<?php

namespace App\Imports;


use Carbon\Carbon;
use GuzzleHttp\Client;
use App\Models\Applicant;
use Maatwebsite\Excel\Row;
use App\Models\ApplicantStatus;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use App\Models\ApplicantContactMethod;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\OnEachRow;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class ApplicantsNewImport implements OnEachRow, WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    private function transformDateTime(string $value, string $format = 'Y-m-d')
    {
        try {
            //$var = preg_split("#/#", $value);
            return Carbon::instance(Date::excelToDateTimeObject($value))->format($format);
        } catch (ErrorException $e) {
            return Carbon::createFromFormat($format, $value);
        }
    }

    protected function formatDateExcel($date)
    { if (gettype($date) === 'double') 
        { 
            $birthday = \PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($date); return $birthday->format('n/j/Y'); 
        } 
        return $date; 
    }

    public function verificaTelefono($id){
        $ApplicantTele=ApplicantContactMethod::where('applicant_id', $id)->get();
        return $ApplicantTele->count();
    }

    public function onRow(Row $row)
    {
        if($row['cis']==NULL){
            exit;
        }
        $conexion = pg_connect("host=localhost dbname=ama6.0 user=postgres password=123");
        $Applicantdata=Applicant::where('government_id', trim($row['cis']))->get();
        //$Applicantdata=Applicant::where('government_id', $row[5])
        //->whereNull('applicants.parent_applicant')->pluck('id'); // busca nro de cedula 
        if((count($Applicantdata)>0)&&($row['cis']!==NULL)){
        echo "<hr/>";
        echo "Cedulas a actualizar : ".$row['cis'];
        Log::info('Cedulas a actualizar: '. $row['cis']);
        echo "<br/>";
        echo "Direccion EXCEL : ".$row['direccion'];
        echo "<br/>";
        echo "Direccion BD : ".$Applicantdata[0]->address;
        echo "<br/>";
        echo "Direccion BD id : ".$Applicantdata[0]->id;
        Log::info('Direccion BD id : '. $Applicantdata[0]->id);
        echo "<br/>";
        echo "Telefono BD : ".$this->verificaTelefono($Applicantdata[0]->id);
        $existphone=$this->verificaTelefono($Applicantdata[0]->id);
        echo "<br/>";
        echo "Telefono EXCEL : ".$row['telefono'];
        echo "<br/>";
        echo "Telefono SI EXISTE : ".$existphone;
        //print_r($Applicantdata);
        print_r($Applicantdata[0]->address);
        //exit;
                if(($Applicantdata[0]->address == NULL) && ($row['direccion'] !== NULL)){  //si no existe ningun registro con este ID
                    //dd($Applicantdata[0]);
                    $direccion=$row['direccion'];
                    $ID=$Applicantdata[0]->id;

                    echo "<hr/>";
                    echo "ACT. a Direccion : ".$row['direccion'];
                    echo "<br/>";
                    echo "ACT a ID : ".$ID;
                   // echo "Fila :". $row->getIndex();
                    echo "<br/>";

                    $sql = "UPDATE applicants SET  address = '$direccion' WHERE id = $ID ";   
                    pg_query( $conexion, $sql );
                    
                }
                   
                if (($existphone == 0) && ($row['telefono'] !== NULL)){
                    $ID=$Applicantdata[0]->id;
                    $telefono=$row['telefono'];
                    $created_at = Carbon::now();
                    $updated_at = Carbon::now(); 

                        $sql2="INSERT INTO applicant_contact_methods (applicant_id, contact_method_id, description, created_at, updated_at) values($ID, 3, '$telefono', '$created_at', '$updated_at')";
                        pg_query( $conexion, $sql2 ); 
                        echo "<hr/>";
                        echo "Actualizar : ".$ID;
                        echo "<br/>";             
                        echo "Actualizar Telfono : ".$telefono;
                        echo "<br/>";             
        
                } 
            } 
                
                
             
    }
 
public function headingRow(): int
{
    return 1;
}
}

