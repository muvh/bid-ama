<?php

namespace App\Imports;


use App\Models\Applicant;
use App\Models\ApplicantStatus;
use Maatwebsite\Excel\Row;
use Maatwebsite\Excel\Concerns\OnEachRow;
use Illuminate\Support\Facades\Auth;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use Maatwebsite\Excel\Concerns\ToModel;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class ApplicantsNewImport implements OnEachRow, WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    private function transformDateTime(string $value, string $format = 'Y-m-d')
    {
        try {
            //$var = preg_split("#/#", $value);
            return Carbon::instance(Date::excelToDateTimeObject($value))->format($format);
        } catch (ErrorException $e) {
            return Carbon::createFromFormat($format, $value);
        }
    }

    protected function formatDateExcel($date)
    { if (gettype($date) === 'double') 
        { 
            $birthday = \PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($date); return $birthday->format('n/j/Y'); 
        } 
        return $date; 
    }
    public function onRow(Row $row)
    {
    
        $conexion = pg_connect("host=localhost dbname=newprodu user=postgres password=123");
                         $Applicantdata=Applicant::where('government_id', trim($row[5]))->get();
                //$Applicantdata=Applicant::where('government_id', $row[5])
                //->whereNull('applicants.parent_applicant')->pluck('id'); // busca nro de cedula 
                
                if( $Applicantdata->count()==0){  //si no existe ningun registro con este ID
                    //dd($Applicantdata[0]);
                    echo "<hr/>";
                    echo "Cedulas a actualizar : ".$row[5];
                    echo "<br/>";
                   // echo "Fila :". $row->getIndex();
                    echo "<br/>";
                    $applicant = new Applicant();
                    $finaval=$row[41] !== NULL ? true: false;
                    $sql = "INSERT INTO applicants (names, state_id, government_id, financial_entity_eval, city_id, financial_entity, construction_entity, address, neighborhood, resolution_number, file_number, origin) "
                    ."VALUES (''$row[4]'', 11, '$row[5]', $finaval, $row[40], $row[39], $row[41], '$row[10]', '$row[11]', '$row[26]', '$row[20]', '$row[42]' )";   
                    pg_query( $conexion, $sql );
                    
                  
                    //dd($test);
               /*      try
                {
                // insert Applicant       
                $applicant = new Applicant();
                $applicant->names=$row[4];
                $applicant->government_id=$row[5];
                $applicant->state_id=11;
                $applicant->financial_entity_eval=($row[41] !== NULL) ? true: false;
                $applicant->city_id=$row[40];
                $applicant->financial_entity=$row[39];
                $applicant->construction_entity=$row[41];
                $applicant->address=$row[10];
                $applicant->neighborhood=$row[11];
                $applicant->resolution_number=$row[26];
                $applicant->file_number=$row[20];
                $applicant->origin=$row[42];
                $applicant->save();
                //dd($applicant);

            } catch (\Exception $exception) {
                dd($exception->getMessage());
            }    */
            //protected $fillable = ['state_id', 'city_id', 'education_level', 'parent_applicant', 'applicant_relationship', 
            //'names', 'last_names', 'birthdate', 'gender', 'government_id', 'marital_status', 'pregnant', 
            //'pregnancy_due_date', 'cadaster', 'property_id', 'occupation', 'monthly_income', 'nationality', 
            //'address', 'neighborhood', 'construction_entity', 'financial_entity', 'resolution_number', 'file_number', 
           // 'financial_entity_eval', 'construction_entity_eval', 'ruc', 'created_by', 'created_at', 'updated_at', 'origin', 'date_register'];


 /*               return new Applicant([
                    'names' => ($row[4] !== NULL) ? strval($row[4]) : NULL,
                    'government_id' => ($row[5] !== NULL) ? strval($row[5]) : NULL, 
                    'state_id' => intval(11),
                    //'financial_entity_eval' => ($row[41] !== NULL) ? true: false,
                    'city_id' => ($row[40] !== NULL) ? intval($row[40]) : NULL,
                    'financial_entity' => intval(($row[39] !== NULL) ? intval($row[39]) : NULL),
                    'construction_entity' => intval(($row[41] !== NULL) ? intval($row[41]) : NULL),
                    'address' => ($row[10] !== NULL) ? strval($row[10]) : NULL,
                    'neighborhood' => ($row[11] !== NULL) ? strval($row[11]) : NULL,
                    'resolution_number' => ($row[26] !== NULL) ? intval($row[26]) : NULL,
                    'file_number' => ($row[20] !== NULL) ? intval($row[20]) : NULL,
                    'origin' => ($row[42] !== NULL) ? strval($row[42]) : NULL//,
                    //'date_register' => ($row[2] !== NULL) ? $row[2] : NULL
                ]); */
                 
                
                if ($row[7] !== NULL){
                    //dd($Applicantdata2[0]);
                    // insert Applicant
                    
                        $sql2="INSERT INTO applicants (names, government_id, parent_applicant, state_id, city_id, applicant_relationship) values('$row[6]','$row[7]', $applicant->id', 11, $row[40], 1)";
                        pg_query( $conexion, $sql2 );
                        
    /*                 $createConyugue = new Applicant();
                    $createConyugue->names = $row[6];
                    $createConyugue->government_id = $row[7];
                    $createConyugue->parent_applicant = $applicant->id;
                    $createConyugue->state_id = 11;
                    $createConyugue->city_id = $row[40];
                    $createConyugue->applicant_relationship = 1;
                    $createConyugue->save(); */
               
        
                } 
                
                
                /*                 $Applicantdata=Applicant::where('government_id', $row[5])->whereNull('created_by')->get();
                
                print_r($Applicantdata);
                exit; */
                //$descrip=(substr($row[25], 0, 200));
/*                 try{
                $createAppStatus = new ApplicantStatus();
                $createAppStatus->applicant_id = $applicant->id;
                $createAppStatus->user_id=Auth::user()->id;
                $createAppStatus->status_id=$row[38];
                $createAppStatus->description=$row[25];
                $createAppStatus->save();
            } catch (\Exception $exception) {
                dd($exception->getMessage());
            }  */

            $sql3="INSERT INTO applicants (applicant_id, user_id, status_id, state_id, description) values($applicant->id,3, $row[38], $row[25])";
            pg_query( $conexion, $sql3 );
                /* $createAppStatus= ApplicantStatus::create([
                    'applicant_id' => $applicant->id,
                    'user_id' => Auth::user()->id,
                    'status_id' => ($row[38] !== NULL) ? $row[38] : NULL,
                    'description' => ($row[25] !== NULL) ? $row[25] : NULL,
                    //'created_at' => $row[2] //Date::excelToDateTimeObject($row[2])->format('Y-m-d'),
                    //'updated_at' => $timestamp1,
                    
                ]);  */
                echo 'Nuevo Registro CI.: '.$row[5];
                echo '<br/>';
                echo 'Registro:'.$applicant->id;
                echo "<hr/>";
                
        
                
       /*      }else{
                // echo 'Ya existe en DB: '.$Applicantdata[0]['government_id'];
                // echo '<br/>';
                
            }
         */
        
        
    
    }
    }
 
public function headingRow(): int
{
    return 1;
}
}

