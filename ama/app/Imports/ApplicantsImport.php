<?php

namespace App\Imports;

use App\Models\Applicant;
use App\Models\ApplicantStatus;
use Maatwebsite\Excel\Row;
use Maatwebsite\Excel\Concerns\OnEachRow;
use Illuminate\Support\Facades\Auth;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class ApplicantsImport implements OnEachRow, WithHeadingRow
{
    /** 
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    private function transformDateTime(string $value, string $format = 'Y-m-d')
    {
        try {
            $var = preg_split("#/#", $value);
            //dd($var);
            return Carbon::instance(Date::excelToDateTimeObject($value))->format($format);
        } catch (\ErrorException $e) {
            return Carbon::createFromFormat($format, $value);
        }
    }
 
    public function onRow(Row $row)
    {
        
        if ($row->getIndex() == 1  || $row->getIndex() == 2) {
        } else {
            //var_dump($row[7]);
           // if(!empty($row[7])){
            if ($row[7] !== NULL){ // si no esta vacio en el EXCEL 
                //dd($row[5]);
                $Applicantdata=Applicant::where('government_id', $row[5])->whereNull('created_by')->get();
                //$Applicantdata=Applicant::where('government_id', '2081414')->whereNull('created_by')->get();
                //dd($Applicantdata->count());
                if( $Applicantdata->count()==0){ 
                    echo "<hr/>";
                    echo "No se actualizo Solicitante: ".$row[5];
                    echo "<br/>";
                    echo "<hr/>";
                }else{

                        $CIApplicantBD=$Applicantdata[0]['government_id']; //CI del soliciante
                        //dd($row[7]);
                        $IDApplicant=$Applicantdata[0]['id']; //id del solicitante del BD
                        $CIConyugueEX=$row[7]; // CI del conyugue del EXCEL
                        //dd($CIApplicantBD);
                        
                                $result=Applicant::where('parent_applicant',$IDApplicant)
                                ->update(['government_id' => "$CIConyugueEX" ]);
                                
                                if($result==1){
                                    echo "Se ha actualizado el CI.:".$CIApplicantBD;
                                    echo "<br/>";
                                    echo "Solicitante: ".$CIApplicantBD." Conyugue con CI BD.:".$CIConyugueEX;
                                    echo "<br/>";
                                    echo "Result: ".$result;
                                    echo "<hr/>";
                                }else{
                                    echo "<hr/>";
                                    echo "No se actualizo Solicitante: ".$CIApplicantBD." Conyugue ".$row[7];
                                    echo "<br/>";
                                    echo "<hr/>";
                                }
                            
                }
            }

           
            //$datos = $this->getData($row[4]);

            //dd($datos);
            /*if (!isset($row[2])) {
                $dt = new \DateTime($row[2]);
                $date = $dt->format('Y-d-m');
            } else {
                $date = '2020-12-12';
            }*/
            //$test = Date::excelToDateTimeObject($row[2])->format('Y-m-d');
            //dd($test);
            //dd($row[2]);
            //$dt = new \DateTime($test);
            //dd($dt);
            //dd(strval($row[0]));

/*             $applicant = Applicant::create([
                'names' => $row[4],
                'government_id' => $row[5],
                'state_id' => '11',
                'financial_entity_eval' => true,
                'city_id' => $row[40],
                'financial_entity' => $row[39],
                'construction_entity' => $row[41],
                'address' => $row[10],
                'neighborhood' => $row[11],
                'resolution_number' => $row[26],
                'file_number' => $row[20],
                'origin' => $row[42],

            ]); */
            //dd(\DateTime::createFromFormat('Y-m-d H:i:s', $row[2]) . ' hola ' . $row[2]);
/*             if ($row[2] !== NULL) {
                // it's a date

                ApplicantStatus::create([
                    'applicant_id' => $applicant->id,
                    'user_id' => Auth::user()->id,
                    'status_id' => 1,
                    'created_at' => $this->transformDateTime($row[2]) //Date::excelToDateTimeObject($row[2])->format('Y-m-d'),
                    //'updated_at' => $timestamp1,

                ]);
            } else {
                ApplicantStatus::create([
                    'applicant_id' => $applicant->id,
                    'user_id' => Auth::user()->id,
                    'status_id' => 1,
                    //'created_at' => $timestamp1,
                    //'updated_at' => $timestamp1,

                ]);
            } */

            //$dateString = $row[2];
            //$timestamp = strtotime(str_replace("/", "-", $dateString));
            /*if ($row[2] == NULL) {
                ApplicantStatus::create([
                    'applicant_id' => $applicant->id,
                    'user_id' => Auth::user()->id,
                    'status_id' => 1,
                    //'created_at' => $timestamp1,
                    //'updated_at' => $timestamp1,

                ]);
            } else {

                if (\DateTime::createFromFormat('Y-m-d H:i:s', $row[2]) !== FALSE) {
                    // it's a date
                  }else{
                    ApplicantStatus::create([
                        'applicant_id' => $applicant->id,
                        'user_id' => Auth::user()->id,
                        'status_id' => 1,
                        //'created_at' => $timestamp1,
                        //'updated_at' => $timestamp1,

                    ]);
                  }*/

            /*try {
                    $date1 = $row[2];
                    $timestamp1 = strtotime($date1);
                } catch (\Throwable $th) {
                    dd($timestamp1);
                }*/

            //echo $timestamp1;
            /*
                ApplicantStatus::create([
                    'applicant_id' => $applicant->id,
                    'user_id' => Auth::user()->id,
                    'status_id' => 1,
                    'created_at' => $timestamp1,
                    //'updated_at' => $timestamp1,

                ]);
            }*/




            /*if ($row[35] == 3) {
                ApplicantStatus::create([
                    'applicant_id' => $applicant->id,
                    'user_id' => Auth::user()->id,
                    'status_id' => 3
                ]);
            }

            if ($row[35] == 14) {
                ApplicantStatus::create([
                    'applicant_id' => $applicant->id,
                    'user_id' => Auth::user()->id,
                    'status_id' => 14
                ]);
            }*/

            /*if ($row[5] == 'N/A') {
            } else {
                Applicant::create([
                    'names' => $row[6],
                    'government_id' => $row[7],
                    'parent_applicant' => $applicant->id,
                    'state_id' => '11',
                    'city_id' => $applicant->city_id,
                    'applicant_relationship' => 1
                ]);
            }*/
        }
    }
    public function headingRow(): int
    {
        return 1;
    }
}