<?php

namespace App\Imports;

use App\Models\Applicant;
use App\Models\ApplicantStatus;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Maatwebsite\Excel\Row;
use Maatwebsite\Excel\Concerns\OnEachRow;
use Illuminate\Support\Facades\Auth;
use PhpOffice\PhpSpreadsheet\Shared\Date;


class ApplicantImport implements OnEachRow
{

    public function getData($cedula)
    {
        $client = new Client();
        //dd('funciona');
        try {
            $url = "http://" . env("URL_ENV", "192.168.202.43:8080") . "/mbohape-core/sii/security";
            $res = $client->request('POST', $url, [
                'connect_timeout' => 10,
                'http_errors' => true,
                'headers' => ['Content-Type' => 'application/json', 'Accept' => 'application/json'],
                'json' => [
                    'username' => 'senavitatconsultas',
                    'password' => 'S3n4vitat'
                ],
            ]);

            //dd('funca');

            if ($res->getStatusCode() == 200) {
                $json = json_decode($res->getBody(), true);
                $token = $json['token'];

                $url = "http://" . env("URL_ENV", "192.168.202.43:8080") . '/frontend-identificaciones/api/persona/obtenerPersonaPorCedula/' . $cedula;
                $res = $client->request('GET', $url, [
                    'connect_timeout' => 10,
                    'http_errors' => true,
                    'headers' => [
                        'Content-Type' => 'application/json',
                        'Accept' => 'application/json',
                        'Authorization' => 'Bearer ' . $token,
                    ],
                ]);

                //dd($res);

                if ($res->getStatusCode() == 200) {
                    $json = json_decode($res->getBody(), true);
                    /*return response()->json([
                            'error' => false,
                            'message' => $json['obtenerPersonaPorNroCedulaResponse']['return']
                        ]);*/
                    //dd
                    return $json['obtenerPersonaPorNroCedulaResponse']['return'];
                } else {
                    /*return response()->json([
                            'error' => true,
                            'message' => 'Error de comunicación en Consulta'
                        ]);*/
                    return [
                        'error' => true
                    ];
                    //dd('error');
                }
            } else {
                /*return response()->json([
                        'error' => true,
                        'message' => 'Error de Autenticación'
                    ]);*/
                //dd('error');
                return [
                    'error' => true
                ];
            }
        } catch (\Exception $exception) {
            /*return response()->json([
                    'error' => true,
                    'message' => 'Error de comunicación'
                ]);*/
            //dd('error');
            return [
                'error' => true
            ];
        }
    }

    private function transformDateTime(string $value, string $format = 'Y-m-d')
    {
        try {
            $var = preg_split("#/#", $value);
            dd($var);
            return Carbon::instance(Date::excelToDateTimeObject($value))->format($format);
        } catch (\ErrorException $e) {
            return Carbon::createFromFormat($format, $value);
        }
    }

    public function onRow(Row $row)
    {
        /*$rowIndex = $row->getIndex();
        $row      = $row->toArray();

        $group = Group::firstOrCreate([
            'name' => $row[1],
        ]);*/
        if ($row->getIndex() == 1) {
        } else {

            //dd('123');
            //$datos = $this->getData($row[4]);

            //dd($datos);
            /*if (!isset($row[2])) {
                $dt = new \DateTime($row[2]);
                $date = $dt->format('Y-d-m');
            } else {
                $date = '2020-12-12';
            }*/
            //$test = Date::excelToDateTimeObject($row[2])->format('Y-m-d');
            //dd($test);
            //dd($row[2]);
            //$dt = new \DateTime($test);
            //dd($dt);
            //dd(strval($row[0]));

            $applicant = Applicant::create([
                'names' => $row[4],
                'government_id' => $row[5],
                'state_id' => '11',
                'financial_entity_eval' => true,
                'city_id' => $row[40],
                'financial_entity' => $row[39],
                'construction_entity' => $row[41],
                'address' => $row[10],
                'neighborhood' => $row[11],
                'resolution_number' => $row[26],
                'file_number' => $row[20],
                'origin' => $row[42],

            ]);
            //dd(\DateTime::createFromFormat('Y-m-d H:i:s', $row[2]) . ' hola ' . $row[2]);
            if ($row[2] !== NULL) {
                // it's a date

                ApplicantStatus::create([
                    'applicant_id' => $applicant->id,
                    'user_id' => Auth::user()->id,
                    'status_id' => 1,
                    'created_at' => $this->transformDateTime($row[2]) //Date::excelToDateTimeObject($row[2])->format('Y-m-d'),
                    //'updated_at' => $timestamp1,

                ]);
            } else {
                ApplicantStatus::create([
                    'applicant_id' => $applicant->id,
                    'user_id' => Auth::user()->id,
                    'status_id' => 1,
                    //'created_at' => $timestamp1,
                    //'updated_at' => $timestamp1,

                ]);
            }

            //$dateString = $row[2];
            //$timestamp = strtotime(str_replace("/", "-", $dateString));
            /*if ($row[2] == NULL) {
                ApplicantStatus::create([
                    'applicant_id' => $applicant->id,
                    'user_id' => Auth::user()->id,
                    'status_id' => 1,
                    //'created_at' => $timestamp1,
                    //'updated_at' => $timestamp1,

                ]);
            } else {

                if (\DateTime::createFromFormat('Y-m-d H:i:s', $row[2]) !== FALSE) {
                    // it's a date
                  }else{
                    ApplicantStatus::create([
                        'applicant_id' => $applicant->id,
                        'user_id' => Auth::user()->id,
                        'status_id' => 1,
                        //'created_at' => $timestamp1,
                        //'updated_at' => $timestamp1,

                    ]);
                  }*/

            /*try {
                    $date1 = $row[2];
                    $timestamp1 = strtotime($date1);
                } catch (\Throwable $th) {
                    dd($timestamp1);
                }*/

            //echo $timestamp1;
            /*
                ApplicantStatus::create([
                    'applicant_id' => $applicant->id,
                    'user_id' => Auth::user()->id,
                    'status_id' => 1,
                    'created_at' => $timestamp1,
                    //'updated_at' => $timestamp1,

                ]);
            }*/




            /*if ($row[35] == 3) {
                ApplicantStatus::create([
                    'applicant_id' => $applicant->id,
                    'user_id' => Auth::user()->id,
                    'status_id' => 3
                ]);
            }

            if ($row[35] == 14) {
                ApplicantStatus::create([
                    'applicant_id' => $applicant->id,
                    'user_id' => Auth::user()->id,
                    'status_id' => 14
                ]);
            }*/

            /*if ($row[5] == 'N/A') {
            } else {
                Applicant::create([
                    'names' => $row[6],
                    'government_id' => $row[7],
                    'parent_applicant' => $applicant->id,
                    'state_id' => '11',
                    'city_id' => $applicant->city_id,
                    'applicant_relationship' => 1
                ]);
            }*/
        }



        /*$group->users()->create([
            'name' => $row[0],
        ]);*/
    }
}


/*
class ApplicantImport implements
{

    public function model(array $row)
    {
        return new Applicant([
            //
            'names' => $row[3]
        ]);
    }

    public function startRow(): int
    {
        return 2;
    }
}
*/
