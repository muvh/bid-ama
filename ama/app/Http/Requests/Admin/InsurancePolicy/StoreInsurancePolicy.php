<?php

namespace App\Http\Requests\Admin\InsurancePolicy;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Gate;
use Illuminate\Validation\Rule;

class StoreInsurancePolicy extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return Gate::allows('admin.insurance-policy.create');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'nro_poliza' => ['nullable', 'string'],
            'concepto' => ['nullable', 'string'],
            'fecha_vigencia' => ['nullable', 'date'],
            'fecha_vencimiento' => ['nullable', 'date'],
            'fecha_desembolso' => ['nullable', 'date'],
            'fecha_envio_cac' => ['nullable', 'date'],
            'importe' => ['nullable', 'string'],
            'applicant_id' => ['nullable', 'int'],
            'insurance_entity_id' => ['nullable', 'int'],
            'construction_entity_id' => ['nullable', 'int'],
            'culminado' => ['nullable', 'boolean'],
            
        ];
    }

    /**
    * Modify input data
    *
    * @return array
    */
    public function getSanitized(): array
    {
        $sanitized = $this->validated();

        //Add your code for manipulation with request data here

        return $sanitized;
    }

    public function getInsuranceEntityId()
    {
        if ($this->has('insurance_entity_id')) {
            return $this->get('insurance_entity_id')['id'];
        }
        return null;
    }
}
