<?php

namespace App\Http\Requests\Admin\InsurancePolicy;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Gate;
use Illuminate\Validation\Rule;

class UpdateInsurancePolicy extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return Gate::allows('admin.insurance-policy.edit', $this->insurancePolicy);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'nro_poliza' => ['sometimes', 'string'],
            'concepto' => ['nullable', 'string'],
            'fecha_vigencia' => ['sometimes', 'date'],
            'fecha_vencimiento' => ['sometimes', 'date'],
            'fecha_desembolso' => ['nullable', 'date'],
            'fecha_envio_cac' => ['nullable', 'date'],
            'importe' => ['nullable', 'int'],
            'applicant_id' => ['sometimes', 'int'],
            'insurance_entity_id' => ['nullable', 'int'],
            'construction_entity_id' => ['sometimes', 'int'],
            'culminado' => ['nullable', 'boolean'],
            
            
        ];
    }

    /**
     * Modify input data
     *
     * @return array
     */
    public function getSanitized(): array
    {
        $sanitized = $this->validated();


        //Add your code for manipulation with request data here

        return $sanitized;
    }


}
