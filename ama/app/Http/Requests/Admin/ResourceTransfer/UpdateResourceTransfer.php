<?php

namespace App\Http\Requests\Admin\ResourceTransfer;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Gate;
use Illuminate\Validation\Rule;

class UpdateResourceTransfer extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return Gate::allows('admin.resource-transfer.edit', $this->resourceTransfer);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'nro_solictud_rt' => ['sometimes', 'string'],
            'fecha_rt' => ['sometimes', 'date'],
            'type_order_id' => ['sometimes', 'string'],
            'construction_entity_id' => ['sometimes', 'string'],
            'nro_nota_bid' => ['sometimes', 'string'],
            
        ];
    }

    /**
     * Modify input data
     *
     * @return array
     */
    public function getSanitized(): array
    {
        $sanitized = $this->validated();


        //Add your code for manipulation with request data here

        return $sanitized;
    }
}
