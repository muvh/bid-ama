<?php

namespace App\Http\Requests\Admin\ResourceTransfer;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Gate;
use Illuminate\Validation\Rule;

class StoreResourceTransfer extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        //return Gate::allows('admin.resource-transfer.create');
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'nro_solictud_rt' => ['required', 'string'],
            'fecha_rt' => ['required', 'date'],
            'type_order_id' => ['nullable', 'string'],
            'construction_entity_id' => ['nullable', 'string'],
            'nro_nota_bid' => ['nullable', 'string'],
            'obs' => ['nullable', 'string'],
            
        ];
    }

    /**
    * Modify input data
    *
    * @return array
    */
    public function getSanitized(): array
    {
        $sanitized = $this->validated();

        //Add your code for manipulation with request data here

        return $sanitized;
    }
    public function getConstructionEntity()
    {
        if ($this->has('construction_entity')) {
            return $this->get('construction_entity')['id'];
        }
        return null;
    }
    public function getTypeOrder()
    {
        if ($this->has('type_order')) {
            return $this->get('type_order')['id'];
        }
        return null;
    }
}
