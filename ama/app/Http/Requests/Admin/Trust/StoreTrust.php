<?php

namespace App\Http\Requests\Admin\Trust;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Gate;
use Illuminate\Validation\Rule;

class StoreTrust extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return Gate::allows('admin.trust.create');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'nro_tr' => ['required', 'string'],
            'fecha_tr' => ['required', 'date'],
            'financial_entity_id' => ['nullable', 'int'],
            
        ];
    }

    /**
    * Modify input data
    *
    * @return array
    */
    public function getSanitized(): array
    {
        $sanitized = $this->validated();

        //Add your code for manipulation with request data here

        return $sanitized;
    }
    public function getFinancialEntity()
    {
        if ($this->has('financial_entity')) {
            return $this->get('financial_entity')['id'];
        }
        return null;
    }
}
