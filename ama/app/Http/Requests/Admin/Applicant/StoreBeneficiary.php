<?php

namespace App\Http\Requests\Admin\Applicant;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Gate;
use Illuminate\Validation\Rule;

class StoreBeneficiary extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return Gate::allows('admin.applicant.create');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [

            'names' => ['required', 'string'],
            'last_names' => ['required', 'string'],
            'birthdate'  => ['required', 'date'],
            'gender' => ['required', 'string'],
            'nationality' => ['required', 'string'],
            'government_id' => ['nullable', 'string'],
            'marital_status' => ['required', 'string'],
           
            'pregnancy_due_date' => ['nullable', 'int'],
            'parent_applicant' => ['nullable', 'int'],
            'ruc' => ['nullable', 'string'],
            'registration_date'  => ['nullable', 'date'],
            'educationlevel' => ['required'],
            'applicantrelationship' => ['sometimes'],
            'diseasecomponents' => ['sometimes', 'array'],
            'disabilitycomponents' => ['sometimes', 'array'],
            'contactcomponents' => ['sometimes', 'array'],

        ];

    }
    public function messages()
{
    return [
        'government_id.required' => 'Debe cargar el documento de identidad.',
        'names.required' => 'Debe cargar el nombre de la persona.',
        'last_names.required' => 'Debe cargar el apellido de la persona.',
        'nationality.required' => 'Debe cargar la nacionalidad de la persona.',
        'gender.required' => 'Debe cargar el genero de la persona.',
        'birthdate.required' => 'Debe cargar la fecha de nacimiento',
        'educationlevel' => 'Debe cargar la fecha de nacimiento',
        'registration_date' => 'Cargar Fecha',
        'marital_status' => 'Cargar Estado Civil',
     
        
    ];
}
    /**
    * Modify input data
    *
    * @return array
    */
    public function getSanitized(): array
    {
        $sanitized = $this->validated();

        //Add your code for manipulation with request data here

        return $sanitized;
    }

    public function getEducationLevelId()
    {
        if ($this->has('educationlevel')) {
            return $this->get('educationlevel')['id'];
        }
        return null;
    }

    public function getApplicantRelationshipId()
    {
        if ($this->has('applicantrelationship')) {
            return $this->get('applicantrelationship')['id'];
        }
        return null;
    }

}
