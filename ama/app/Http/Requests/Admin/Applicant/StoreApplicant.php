<?php

namespace App\Http\Requests\Admin\Applicant;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Gate;
use Illuminate\Validation\Rule;

class StoreApplicant extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        //return Gate::allows('admin.applicant.create');
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [

            'names' => ['nullable', 'string'],
            'last_names' => ['nullable', 'string'],
            'birthdate'  => ['nullable', 'date'],
            'gender' => ['nullable', 'string'],
            'marital_status' => ['required', 'string'],
            'nationality' => ['nullable', 'string'],
            'government_id' => ['required', 'string'],

            'atc_fec_asig'  => ['nullable', 'date'],
            'file_date' => ['nullable', 'string'],
            'resolution_date' => ['nullable', 'string'],
            
            'pregnancy_due_date' => ['nullable', 'int'],
            'parent_applicant' => ['nullable', 'int'],
            'address' => ['nullable', 'string'],
            'neighborhood' => ['nullable', 'string'],
            'city' => ['required'],
            'applicantrelationship' => ['sometimes'],
            'ruc' => ['nullable', 'string'],
            'monthly_income' => ['nullable', 'string'],
            'occupation' => ['nullable', 'string'],
            'padron' => ['nullable', 'string'],
            'property_id' => ['nullable', 'string'],
            'cadaster' => ['nullable', 'string'],
            'registration_date'  => ['nullable', 'date'],

            'familiarsettings'  => ['nullable','array'],
            'diseasecomponents' => ['sometimes', 'array'],
            'disabilitycomponents' => ['sometimes', 'array'],
            'contactcomponents' => ['sometimes', 'array'],
	        'educationlevel' => ['required'],

        ];

    }
    public function messages()
{
    return [
        'government_id.required' => 'Debe cargar el documento de identidad.',
        'names.required' => 'Debe cargar el nombre de la persona.',
        'last_names.required' => 'Debe cargar el apellido de la persona.',
        'nationality.required' => 'Debe cargar la nacionalidad de la persona.',
        'gender.required' => 'Debe cargar el genero de la persona.',
        'birthdate.required' => 'Debe cargar la fecha de nacimiento',
        'address.required' => 'Debe cargar la direccion',
        'city.required' => 'Debe cargar la ciudad',
        'neighborhood.required' => 'Debe cargar el Barrio',
        'ruc' => 'Cargue RUC',
        'registration_date' => 'Cargar Fecha',
        'educationlevel' => 'Debe cargar la Nivel Educacion',
    ];
}
    /**
    * Modify input data
    *
    * @return array
    */
    public function getSanitized(): array
    {
        $sanitized = $this->validated();

        //Add your code for manipulation with request data here

        return $sanitized;
    }

    public function getEducationLevelId()
    {
        if ($this->has('educationlevel')) {
            return $this->get('educationlevel')['id'];
        }
        return null;
    }

    public function getCityId()
    {
        if ($this->has('city')) {
            return $this->get('city')['id'];
        }
        return null;
    }
    public function getFamilarSettingId()
    {
        if ($this->has('familiarsetting')) {
            return $this->get('familiarsetting')['id'];
        }
        return null;
    }
}
