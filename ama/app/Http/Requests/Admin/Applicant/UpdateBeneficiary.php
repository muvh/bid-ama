<?php

namespace App\Http\Requests\Admin\Applicant;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Gate;
use Illuminate\Validation\Rule;

class UpdateBeneficiary extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return Gate::allows('admin.applicant.edit', $this->applicant);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [

            'names' => ['required', 'string'],
            'last_names' => ['required', 'string'],
            'birthdate'  => ['required', 'date'],
            'gender' => ['required', 'string'],
            'nationality' => ['required', 'string'],
            'government_id' => ['required', 'string'],
            'marital_status' => ['required', 'string'],
            'pregnant' => ['sometimes', 'bool'],
            'pregnancy_due_date' => ['nullable', 'int'],
            'parent_applicant' => ['nullable', 'int'],
            'monthly_income' => ['required', 'numeric'],
            'educationlevel' => ['required'],
            'applicantrelationship' => ['sometimes'],
            'occupation' => ['required', 'string'],
            'ruc' => ['nullable', 'string'],
            'registration_date'  => ['nullable', 'date'],
            'diseasecomponents' => ['sometimes', 'array'],
            'disabilitycomponents' => ['sometimes', 'array'],
            'contactcomponents' => ['sometimes', 'array'],

        ];
    }

    /**
     * Modify input data
     *
     * @return array
     */
    public function getSanitized(): array
    {
        $sanitized = $this->validated();


        //Add your code for manipulation with request data here

        return $sanitized;
    }

    public function getEducationLevelId()
    {
        if ($this->has('educationlevel')) {
            return $this->get('educationlevel')['id'];
        }
        return null;
    }

    public function getApplicantRelationshipId()
    {
        if ($this->has('applicantrelationship')) {
            return $this->get('applicantrelationship')['id'];
        }
        return null;
    }

}
