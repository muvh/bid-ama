<?php

namespace App\Http\Requests\Admin\Applicant;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Gate;
use Illuminate\Validation\Rule;

class UpdateApplicant extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return Gate::allows('admin.applicant.edit', $this->applicant);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [

            'names' => ['required', 'string'],
            'last_names' => ['required', 'string'],
            'marital_status' => ['required', 'string'],
            'birthdate'  => ['required', 'date'],
            
            'file_datºe'  => ['nullable', 'date'],
            'resolution_date'  => ['nullable', 'date'],

            'atc_fec_asig'  => ['nullable', 'date'],
            'gender' => ['required', 'string'],
            'nationality' => ['nullable', 'string'],
            'government_id' => ['required', 'string'],
            'pregnant' => ['nullable', 'bool'],
            'pregnancy_due_date' => ['nullable', 'int'],
            'parent_applicant' => ['nullable', 'int'],
            
            'ruc' => ['nullable', 'string'],
            'address' => ['nullable', 'string'],
            'neighborhood' => ['nullable', 'string'],
            'padron' => ['nullable', 'string'],
            'cadaster' => ['nullable', 'string'],
            'property_id' => ['nullable', 'string'],
            'occupation' => ['nullable', 'string'],
            'applicantrelationship' => ['sometimes'],
            'monthly_income' => ['nullable', 'string'],
            'registration_date'  => ['nullable', 'date'],
            
            'familiarsettings'  => ['sometimes', 'array'],
            'diseasecomponents' => ['sometimes', 'array'],
            'disabilitycomponents' => ['sometimes', 'array'],
            'contactcomponents' => ['sometimes', 'array'],
            'educationlevel' => ['sometimes', 'array'],

        ];
    }

    /**
     * Modify input data
     *
     * @return array
     */
    public function messages()
    {
        return [
            'government_id.required' => 'Debe cargar el documento de identidad.',
            'names.required' => 'Debe cargar el nombre de la persona.',
            'marital_status.required' => 'Debe cargar esto civil.',
            'last_names.required' => 'Debe cargar el apellido de la persona.',
            'nationality.required' => 'Debe cargar la nacionalidad de la persona.',
            'gender.required' => 'Debe cargar el genero de la persona.',
            'birthdate.required' => 'Debe cargar la fecha de nacimiento',
            'educationlevel' => 'Debe cargar la fecha de nacimiento',
            'registration_date' => 'Debe cargar la fecha de nacimiento',
         
            
        ];
    }
    public function getSanitized(): array
    {
        $sanitized = $this->validated();


        //Add your code for manipulation with request data here

        return $sanitized;
    }

    public function getEducationLevelId()
    {
        if ($this->has('educationlevel')) {
            return $this->get('educationlevel')['id'];
        }
        return null;
    }

    public function getCityId()
    {
        if ($this->has('city')) {
            return $this->get('city')['id'];
        }
        return null;
    }
    public function getFamilarSettingId()
    {
        if ($this->has('familiarsetting')) {
            return $this->get('familiarsetting')['id'];
        }
        return null;
    }
}
