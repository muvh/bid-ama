<?php

namespace App\Http\Requests\Admin\AccountingRecord;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Gate;
use Illuminate\Validation\Rule;

class UpdateAccountingRecord extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return Gate::allows('admin.accounting-record.edit', $this->accountingRecord);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            //'summary' => ['sometimes', 'string'],
            //'type_movements' => ['sometimes', 'string'],
            //'amount' => ['sometimes'],
            //'balance' => ['sometimes'],
            //'confirmed' => ['sometimes', 'boolean'],
            'resolution' => ['sometimes', 'string'],
            //'registration_date' => ['sometimes', 'date'],

        ];
    }

    /**
     * Modify input data
     *
     * @return array
     */
    public function getSanitized(): array
    {
        $sanitized = $this->validated();


        //Add your code for manipulation with request data here

        return $sanitized;
    }
}
