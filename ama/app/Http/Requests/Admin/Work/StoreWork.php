<?php

namespace App\Http\Requests\Admin\Work;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Gate;
use Illuminate\Validation\Rule;

class StoreWork extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return Gate::allows('admin.work.create');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'fecha_ws' => ['required', 'date'],
            'applicant_id' => ['required'],
            'supervisor_id' => ['required'],
            
        ];
    }

    /**
    * Modify input data
    *
    * @return array
    */
    public function getSanitized(): array
    {
        $sanitized = $this->validated();

        //Add your code for manipulation with request data here

        return $sanitized;
    }
    public function getSupervisorId()
    {
        return $this->get('supervisor')['id'];
    }
    public function getApplicantId()
    {
        return $this->get('applicant')['id'];
    }
    public function getTypeChecksId()
    {
        return $this->get('typechecks')['id'];
    }
}
