<?php

namespace App\Http\Requests\Admin\TransferOrder;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Gate;
use Illuminate\Validation\Rule;

class StoreTransferOrder extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return Gate::allows('admin.transfer-order.create');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'nro_solicitud' => ['required', 'int'],
            'fecha_ot' => ['required', 'date'],
            'importe' => ['required', 'string'],
            'construction_entity_id' => ['required', 'int'],
            'applicant_id' => ['required', 'int'],
            'financial_entity_id' => ['required', 'int'],
        ];
    }
    public function messages()
    {
        return [
            'government_id.required' => 'Debe cargar Cedula de Identidad',
            'nro_solicitud.required' => 'Debe ingresar la Nro. de orden de Transferencia',
            'fecha_ot.required' => 'Debe ingresar la fecha de la orden de Transferencia',
            'importe.required' => 'Falta ingresar el importe de la Orden de Transf.',
            'type_order_id.required' => 'Debe seleccionar el Tipo de Orden de Transf.',
            'construction_entity_id.required' => 'Debe cargar la Empr. Constructora.',
            'applicant_id.required' => 'Debe seleccionar el Postulante',
            'applicant_id.required' => 'Debe cargar la Cooperativa',
        ];
    }
    /**
    * Modify input data
    *
    * @return array
    */
    public function getSanitized(): array
    {
        $sanitized = $this->validated();

        //Add your code for manipulation with request data here

        return $sanitized;
    }

    public function getTypeOrderlId()
    {
        if ($this->has('type_order_id')) {
            return $this->get('type_order_id')['id'];
        }
        return null;
    }
}
