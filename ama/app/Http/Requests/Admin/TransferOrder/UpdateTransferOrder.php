<?php

namespace App\Http\Requests\Admin\TransferOrder;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Gate;
use Illuminate\Validation\Rule;

class UpdateTransferOrder extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return Gate::allows('admin.transfer-order.edit', $this->transferOrder);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'nro_solicitud' => ['sometimes', 'string'],
            'fecha_ot' => ['sometimes', 'date'],
            'importe' => ['sometimes', 'string'],
            'type_order_id' => ['sometimes', 'string'],
            'construction_entity_id' => ['sometimes', 'string'],
            'applicant_id' => ['sometimes', 'string'],
            'financial_entity_id' => ['sometimes', 'string'],
            
        ];
    }

    /**
     * Modify input data
     *
     * @return array
     */
    public function getSanitized(): array
    {
        $sanitized = $this->validated();


        //Add your code for manipulation with request data here

        return $sanitized;
    }
}
