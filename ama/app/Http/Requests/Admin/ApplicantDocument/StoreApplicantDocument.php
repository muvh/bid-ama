<?php

namespace App\Http\Requests\Admin\ApplicantDocument;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Gate;
use Illuminate\Validation\Rule;

class StoreApplicantDocument extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return Gate::allows('admin.applicant-document.create');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'applicant_id' => ['required'],
            'document' => ['required'],
            'received_at' => ['required'],

        ];
    }

    public function getDocumentId()
    {
        if ($this->has('document')) {
            return $this->get('document')['id'];
        }
        return null;
    }

    /**
    * Modify input data
    *
    * @return array
    */
    public function getSanitized(): array
    {
        $sanitized = $this->validated();

        //Add your code for manipulation with request data here

        return $sanitized;
    }
    public function messages()
{
    return [
        'received_at.required' => 'Es necesario agregar una fecha al subir un documento.'
    ];
}
}
