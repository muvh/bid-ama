<?php

namespace App\Http\Requests\Admin\ConstructionEntity;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Gate;
use Illuminate\Validation\Rule;

class UpdateConstructionEntity extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return Gate::allows('admin.construction-entity.edit', $this->constructionEntity);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'name' => ['nullable', 'string'],
            'bank_name' => ['nullable', 'string'],
            'cta_cte' => ['nullable', 'string'],
            'const_ruc' => ['nullable', 'string'],
            'razon_social' => ['nullable', 'string'],
            'repre' => ['nullable', 'string'],
            'nro_ci' => ['nullable', 'string'],
            
        ];
    }

    /**
     * Modify input data
     *
     * @return array
     */
    public function getSanitized(): array
    {
        $sanitized = $this->validated();


        //Add your code for manipulation with request data here

        return $sanitized;
    }
}
