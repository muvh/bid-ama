<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Gate;
use Illuminate\Validation\Rule;

class UpdateApplicantUser extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    /*public function authorize(): bool
    {
        return Gate::allows('admin.applicant.edit', $this->applicant);
    }*/

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [

            'names' => ['required', 'string'],
            'last_names' => ['required', 'string'],
            'birthdate'  => ['required', 'date'],
            //'gender' => ['required', 'string'],
            'nationality' => ['required', 'string'],
            'government_id' => ['required', 'string'],
            'monthly_income' => ['required'],
            'educationlevel' => ['required'],
            'occupation' => ['required'],
            'address' => ['required', 'string'],
            'neighborhood' => ['required', 'string'],
            'city' => ['required'],
            'phone' => ['required'],
            'pregnant' => ['nullable', 'bool'],
            //'pregnancy_due_date' => ['nullable', 'int'],
            //'parent_applicant' => ['nullable', 'int'],
            //'ruc' => ['nullable', 'string'],
            //'applicantrelationship' => ['sometimes'],
            //'diseasecomponents' => ['sometimes', 'array'],
            //'disabilitycomponents' => ['sometimes', 'array'],
            //'contactcomponents' => ['sometimes', 'array'],

        ];
    }

    /**
     * Modify input data
     *
     * @return array
     */
    public function messages()
    {
        return [
            'government_id.required' => 'Debe cargar el documento de identidad.',
            'names.required' => 'Debe cargar el nombre de la persona.',
            'last_names.required' => 'Debe cargar el apellido de la persona.',
            'nationality.required' => 'Debe cargar la nacionalidad de la persona.',
            //'gender.required' => 'Debe cargar el genero de la persona.',
            'birthdate.required' => 'Debe cargar la fecha de nacimiento',
            'phone.required' => 'Debe cargar Telefono Celular',


        ];
    }
    public function getSanitized(): array
    {
        $sanitized = $this->validated();


        //Add your code for manipulation with request data here

        return $sanitized;
    }

    public function getEducationLevelId()
    {
        if ($this->has('educationlevel')) {
            return $this->get('educationlevel')['id'];
        }
        return null;
    }

    public function getCityId()
    {
        if ($this->has('city')) {
            return $this->get('city')['id'];
        }
        return null;
    }
}
