<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Gate;
use Illuminate\Validation\Rule;

class StoreApplicantUser extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    /*public function authorize(): bool
    {
        return Gate::allows('admin.applicant.create');
    }*/

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [

            'names' => ['required', 'string'],
            'last_names' => ['required', 'string'],
            'birthdate'  => ['required', 'date'],
            //'gender' => ['required', 'string'],
            'nationality' => ['required', 'string'],
            'government_id' => ['required', 'string', 'unique:applicants'],
            'occupation' => ['required'],
    
            'address' => ['required', 'string'],
            'neighborhood' => ['required', 'string'],
            'city' => ['required'],
            'educationlevel' => ['required'],
            'monthly_income' => ['required'],
            'phone' => ['required'],
            //'applicantrelationship' => ['sometimes'],
            //'ruc' => ['nullable', 'string'],
            //'diseasecomponents' => ['sometimes', 'array'],
            //'disabilitycomponents' => ['sometimes', 'array'],
            //'contactcomponents' => ['required', 'array', "min:1", "max:3"],

        ];
    }
    public function messages()
    {
        return [
            'government_id.required' => 'Debe cargar el documento de identidad.',
            'government_id.unique' => 'El documento de Identidad ya se encuentra registrado.',
            'names.required' => 'Debe cargar el nombre de la persona.',
            'last_names.required' => 'Debe cargar el apellido de la persona.',
            'nationality.required' => 'Debe cargar la nacionalidad de la persona.',
            //'gender.required' => 'Debe cargar el genero de la persona.',
            'birthdate.required' => 'Debe cargar la fecha de nacimiento',
            'address.required' => 'Debe cargar la direccion',
            'city.required' => 'Debe cargar la ciudad',
            'neighborhood.required' => 'Debe cargar el Barrio',
            'educationlevel.required' => 'Debe cargar Nivel de Educación',
            'monthly_income.required' => 'Debe cargar Ingreso Mensual',
            'occupation.required' => 'Debe cargar Profesión',
            'phone.required' => 'Debe cargar Telefono Celular',
            //'contactcomponents.size:1' => 'Debe cargar al menos el celular',
            //'ruc' => 'Cargue RUC',
        ];
    }
    /**
     * Modify input data
     *
     * @return array
     */
    public function getSanitized(): array
    {
        $sanitized = $this->validated();

        //Add your code for manipulation with request data here

        return $sanitized;
    }

    public function getEducationLevelId()
    {
        if ($this->has('educationlevel')) {
            return $this->get('educationlevel')['id'];
        }
        return null;
    }

    public function getCityId()
    {
        if ($this->has('city')) {
            return $this->get('city')['id'];
        }
        return null;
    }
}
