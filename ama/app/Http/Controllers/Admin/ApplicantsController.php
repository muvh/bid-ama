<?php

namespace App\Http\Controllers\Admin;

use Exception;
use App\Models\City;
use App\Models\User;
use App\Models\Work;
use App\Models\Status;
use GuzzleHttp\Client;
use App\Models\Disease;
use App\Mail\EmailStatus;
use App\Models\Applicant;
use App\Models\TypeCheck;
use Illuminate\View\View;
use App\Models\Disability;
use App\Models\Supervisor;
use App\Mail\MassiveNotify;
use Illuminate\Support\Arr;
use App\Models\DocumentType;
use Illuminate\Http\Request;
use App\Models\ContactMethod;
use Illuminate\Http\Response;
use App\Models\EducationLevel;
use App\Models\WorksTypeCheck;
use App\Models\ApplicantAnswer;
use App\Models\ApplicantStatus;
use App\Models\FamiliarSetting;
use App\Models\FinancialEntity;
use App\Models\ApplicantDisease;
use App\Exports\ApplicantsExport;
use App\Imports\ApplicantsImport;
use App\Models\ApplicantDocument;
use App\Models\AssignmentHistory;
use App\Library\ValidateDocuments;
use App\Models\ConstructionEntity;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use App\Models\ApplicantDisability;
use Illuminate\Support\Facades\Log;

use App\Http\Controllers\Controller;
use App\Imports\ApplicantsNewImport;
use Illuminate\Support\Facades\Auth;


use Illuminate\Support\Facades\Mail;
use Maatwebsite\Excel\Facades\Excel;
use App\Models\ApplicantRelationship;
use App\Models\QuestionnaireTemplate;
use Illuminate\Http\RedirectResponse;
use App\Models\ApplicantContactMethod;
use App\Models\ApplicantQuestionnaire;
use Illuminate\Contracts\View\Factory;
use PhpOffice\PhpSpreadsheet\IOFactory;
use App\Imports\ApplicantStatusesImport;
use PhpOffice\PhpSpreadsheet\Writer\Xls;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use App\Models\QuestionnaireTemplateQuestion;
use Brackets\AdminListing\Facades\AdminListing;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Auth\Access\AuthorizationException;
use App\Http\Requests\Admin\Applicant\IndexEstados;
use PhpOffice\PhpSpreadsheet\Calculation\Financial;
use App\Http\Requests\Admin\Applicant\IndexApplicant;
use App\Http\Requests\Admin\Applicant\StoreApplicant;
use App\Http\Requests\Admin\Applicant\UpdateApplicant;
use App\Http\Requests\Admin\Applicant\DestroyApplicant;
use App\Http\Requests\Admin\Applicant\IndexBeneficiary;
use App\Http\Requests\Admin\Applicant\StoreBeneficiary;
use App\Http\Requests\Admin\Applicant\UpdateBeneficiary;
use App\Http\Requests\Admin\Applicant\BulkDestroyApplicant;
use App\Http\Requests\Admin\ApplicantDocument\IndexApplicantDocument;
use App\Http\Requests\Admin\ApplicantDocument\StoreApplicantDocument;
use App\Http\Requests\Admin\ApplicantDocument\UpdateApplicantDocument;
use App\Models\AdminUser;

class ApplicantsController extends Controller
{

    /* Applicants */

    public function index(IndexApplicant $request)
    {

        $financialEntity = Auth::user()->financial_entity;
        $constructionEntity = Auth::user()->construction_entity;
        //print_r($financialEntity);

        $roles = Auth::user()->getRoleNames();

        $data = AdminListing::create(Applicant::class)->processRequestAndGet(
            // pass the request with params
            $request,

            // set columns to query
            ['id', 'names',  'last_names', 'government_id', 'city_id', 'monthly_income', 'construction_entity', 'financial_entity', 'created_at', 'atc_fec_asig'],

            // set columns to searchIn
            ['id', 'names', 'last_names', 'government_id', 'city_id'],
            function ($query) use ($financialEntity, $constructionEntity, $roles) {
                $query
                    ->whereNull('applicants.parent_applicant')
                    ->whereNotNull('applicants.created_by')
                    ->get();
            }
        );

        if ($request->ajax()) {
            if ($request->has('bulk')) {
                return [
                    'bulkItems' => $data->pluck('id')
                ];
            }
            return ['data' => $data];
        }

        return view('admin.applicant.index', ['data' => $data]);
    }
    public function estados(IndexEstados $request, $status)
    {
        //$this->authorize('admin.applicant.estados', $request);
        $financialEntity = Auth::user()->financial_entity;
        $constructionEntity = Auth::user()->construction_entity;
        //$status = $request->input('status');
        //dd ($status);

        $roles = Auth::user()->getRoleNames();
        $estados = Applicant::whereNull('applicants.parent_applicant')

            ->get();
        $idApplicant = [];
        foreach ($estados as $value) {

            if ($value->statuses->status_id == $status) {
                $idApplicant[] = $value->statuses->applicant_id;
            }
        }


        $data = AdminListing::create(Applicant::class)->processRequestAndGet(
            // pass the request with params
            $request,

            // set columns to query
            ['id', 'names',  'last_names', 'government_id', 'city_id', 'monthly_income', 'construction_entity', 'financial_entity', 'created_at', 'atc_fec_asig'],

            // set columns to searchIn
            ['id', 'names', 'last_names', 'government_id', 'city_id'],
            function ($query) use ($financialEntity, $constructionEntity, $roles, $status, $idApplicant) {

                $query
                    ->whereIn('applicants.id', $idApplicant)

                    ->whereNull('applicants.parent_applicant')
                    ->get();
            }
        );
        //return $data;
        if ($request->ajax()) {
            if ($request->has('bulk')) {
                return [
                    'bulkItems' => $data->pluck('id')
                ];
            }
            return ['data' => $data, 'status' => $status];
        }

        return view('admin.applicant.estados', ['data' => $data, 'status' => $status]);
    }

    public function levels(IndexEstados $request, $steps)
    {
        //return $steps;
        //$this->authorize('admin.applicant.estados', $request);
        $financialEntity = Auth::user()->financial_entity;
        $constructionEntity = Auth::user()->construction_entity;
        //$status = $request->input('status');
        //dd ($status);
        $rs_steps = Status::where('prefix', $steps)->pluck('id')->toArray();
        ///return($rs_steps);
        $roles = Auth::user()->getRoleNames();
        $estados = Applicant::whereNull('applicants.parent_applicant')
            ->get();
        $idApplicant = [];
        foreach ($estados as $value) {
            if (in_array($value->statuses->status_id, $rs_steps)) {
                $idApplicant[] = $value->statuses->applicant_id;
            }
            /*    if ($value->statuses->status_id == $status) {
                $idApplicant[] = $value->statuses->applicant_id;
            } */
        }


        $data = AdminListing::create(Applicant::class)->processRequestAndGet(
            // pass the request with params
            $request,

            // set columns to query
            ['id', 'names',  'last_names', 'government_id', 'gender', 'birthdate', 'city_id', 'monthly_income', 'construction_entity', 'financial_entity', 'created_at', 'atc_fec_asig'],

            // set columns to searchIn
            ['id', 'names', 'last_names', 'government_id', 'city_id'],
            function ($query) use ($idApplicant) {

                $query
                    ->whereIn('applicants.id', $idApplicant)

                    ->whereNull('applicants.parent_applicant')
                    ->get();
            }
        );
        //return $data;
        if ($request->ajax()) {
            if ($request->has('bulk')) {
                return [
                    'bulkItems' => $data->pluck('id')
                ];
            }
            return ['data' => $data, 'steps' => $steps];
        }

        return view('admin.applicant.levels', ['data' => $data, 'steps' => $steps]);
    }
    public function show(Applicant $applicant, Request $request)
    {
        //return $request;
        // $this->authorize('admin.applicant.edit', $applicant);

        $cities = City::all();
        $educationlevels = EducationLevel::all();
        $diseases = Disease::all();
        $disabilities = Disability::all();
        $contactmethods = ContactMethod::all();
        $atcs = ConstructionEntity::orderBy('name', 'asc')->get();;
        $familiarSettings = FamiliarSetting::all();
        $coops = FinancialEntity::all();
        $status_id = ApplicantStatus::where('applicant_id', '=', $applicant->id)->max('status_id');
        $statuslist = ApplicantStatus::where('applicant_id', '=', $applicant->id)->orderBy('id', 'desc')->get();
        //dd(json_encode($statuslist));
        $statu = Status::where('id', '=', $status_id)->pluck('name');
        $statuName = Status::orderBy('name', 'asc')->get();
        $supervisors = Supervisor::whereNotIn('id', [2, 3])->orderBy('name', 'desc')->get();

        $typechecks = TypeCheck::select('id', 'name')
            ->orderBy('name')
            ->get();
        $client = new Client();
        $vinSoli = [];

        //$url = "http://192.168.98.192:8000/api/getexpe/" . $id;
        $url = "http://192.168.98.192:8000/api/getdataexpe/" . $applicant->government_id;
        $res = $client->request('GET', $url, [
            'connect_timeout' => 10,
            'http_errors' => true,
            'headers' => ['Content-Type' => 'application/json', 'Accept' => 'application/json'],
        ]);
        //return $res;

        if ($res->getStatusCode() == 200) {
            $json = json_decode($res->getBody(), true);
            //return $json;
            $vinSoli['postulante'] = $json;
        }

        $aux = [];
        foreach ($applicant->applicantDiseases as $key => $disease) {
            $aux[$key]['id'] = $disease->disease->toArray();
        }
        $saveddiseases = $aux;

        $aux = [];
        foreach ($applicant->applicantDisabilities as $key => $disability) {
            $aux[$key]['id'] = $disability->disability->toArray();
        }
        $saveddisabilities = $aux;

        $aux = [];
        foreach ($applicant->applicantContactMethods as $key => $method) {
            $aux[$key]['id'] = $method->contactMethod->toArray();
            $aux[$key]['description'] = $method->toArray()['description'];
        }
        $savedcontactmethods = $aux;

        $questionnaireID = 1;
        $questions = QuestionnaireTemplate::where('id', $questionnaireID)->with('questionnaireTemplateQuestions')->get();
        $answers = collect([]);
        if ($questions->count() > 0) {
            $answers = ApplicantAnswer::where('applicant_id', $applicant->id)->where('questionnaire_template_id', $questionnaireID)->get();
        }

        $questionnaireID = 2;
        $questions2 = QuestionnaireTemplate::where('id', $questionnaireID)->with('questionnaireTemplateQuestions')->get();
        $answers2 = collect([]);
        if ($questions2->count() > 0) {
            $answers2 = ApplicantAnswer::where('applicant_id', $applicant->id)->where('questionnaire_template_id', $questionnaireID)->get();
        }

        $applicantID = $applicant->id;
        $documentType = 'S';

        $socialDocuments = AdminListing::create(ApplicantDocument::class)->processRequestAndGet(
            $request,
            ['applicant_documents.id', 'document_types.name', 'received_at'],
            ['applicant_documents.id', 'document_types.name'],
            function ($query) use ($applicantID, $documentType) {
                $query
                    ->leftJoin('document_types', 'document_types.id', '=', 'applicant_documents.document_id')
                    ->where('applicant_documents.applicant_id', '=', $applicantID)
                    ->where('document_types.type', '=', $documentType);
            }
        );

        $documentType = 'F';

        $finDocuments = AdminListing::create(ApplicantDocument::class)->processRequestAndGet(
            $request,
            ['applicant_documents.id', 'document_types.name', 'received_at'],
            ['applicant_documents.id', 'document_types.name'],
            function ($query) use ($applicantID, $documentType) {
                $query
                    ->leftJoin('document_types', 'document_types.id', '=', 'applicant_documents.document_id')
                    ->where('applicant_documents.applicant_id', '=', $applicantID)
                    ->where('document_types.type', '=', $documentType);
            }
        );

        $documentType = 'T';

        $techDocuments = AdminListing::create(ApplicantDocument::class)->processRequestAndGet(
            $request,
            ['applicant_documents.id', 'document_types.name', 'received_at'],
            ['applicant_documents.id', 'document_types.name'],
            function ($query) use ($applicantID, $documentType) {
                $query
                    ->leftJoin('document_types', 'document_types.id', '=', 'applicant_documents.document_id')
                    ->where('applicant_documents.applicant_id', '=', $applicantID)
                    ->where('document_types.type', '=', $documentType);
            }
        );

        // create and AdminListing instance for a specific model and
        $beneficiaries = AdminListing::create(Applicant::class)->processRequestAndGet(
            // pass the request with params
            $request,
            // set columns to query
            ['applicants.id', 'names', 'last_names', 'government_id', 'monthly_income', 'applicant_relationship'],
            // set columns to searchIn
            ['applicants.id', 'names', 'last_names', 'government_id', 'applicant_relationships.name'],
            function ($query) use ($applicantID) {
                $query
                    ->leftJoin('applicant_relationships', 'applicant_relationship', '=', 'applicant_relationships.id')
                    ->where('parent_applicant', '=', $applicantID);
            }
        );
        $confirm = new Collection([
            ['id' => '1', 'descri' => 'Aprobado'],
            ['id' => '2', 'descri' => 'Pendiente'],
            ['id' => '3', 'descri' => 'Inactivar']

        ]);
        //dd($vinSoli);
        return view('admin.applicant.show', [
            'applicant' => $applicant,
            'educationlevels' => $educationlevels,
            'diseases' => $diseases,
            'disabilities' => $disabilities,
            'contactmethods' => $contactmethods,
            'cities' => $cities,
            'atcs' => $atcs,
            'familiarSettings' => $familiarSettings,
            'coops' => json_decode($coops),
            'saveddiseases' => json_encode($saveddiseases),
            'saveddisabilities' => json_encode($saveddisabilities),
            'savedcontactmethods' => json_encode($savedcontactmethods),
            'questions' => $questions,
            'answers' => $answers,
            'questions2' => $questions2,
            'answers2' => $answers2,
            'socialDocuments' => $socialDocuments,
            'finDocuments' => $finDocuments,
            'techDocuments' => $techDocuments,
            'beneficiaries' => $beneficiaries,
            'statuse' => $statu,
            'statusName' => $statuName,
            'statuslist' => json_decode($statuslist),
            'confirmations' => json_encode($confirm),
            'vinSoli' => $vinSoli,
            'supervisors' => $supervisors,
            'typechecks' => $typechecks,
        ]);
    }

    public function processFormSearch(IndexEstados $request)
    {

        return redirect('admin/applicants/busqueda-ci/' . $request->government_id);
    }

    public function busquedaci(Request $request, $government_id)
    {
        $government_id = strtoupper($government_id);
        $roles = Auth::user()->getRoleNames();
        //return $request;
        $data = AdminListing::create(Applicant::class)->processRequestAndGet(
            // pass the request with params
            $request,

            // set columns to query
            ['id', 'names',  'last_names', 'government_id', 'city_id', 'monthly_income', 'construction_entity', 'financial_entity', 'created_at', 'atc_fec_asig'],

            // set columns to searchIn
            ['id', 'names', 'last_names', 'government_id', 'city_id'],
            function ($query) use ($government_id) {

                $query->whereNull('applicants.parent_applicant')
                    ->where("names", "like", "%$government_id%")
                    ->orwhere("last_names", "like", "%$government_id%")
                    ->orwhere('applicants.government_id', str_replace('.', '', $government_id))
                    ->get();
            }
        );
        //return $data;
        if ($request->ajax()) {
            if ($request->has('bulk')) {
                return [
                    'bulkItems' => $data->pluck('id')
                ];
            }
            return ['data' => $data, 'government_id' => $government_id];
        }

        return view('admin.applicant.busqueda-ci', ['data' => $data, 'government_id' => $government_id]);
    }
    public function califProyectos(IndexEstados $request, $status)
    {

        $financialEntity = Auth::user()->financial_entity;
        $constructionEntity = Auth::user()->construction_entity;
        //$status = $request->input('status');
        //dd ($status);

        $roles = Auth::user()->getRoleNames();
        $estados = Applicant::whereNull('applicants.parent_applicant')

            ->get();
        $idApplicant = [];
        foreach ($estados as $value) {

            if ($value->statuses->status_id == $status) {
                $idApplicant[] = $value->statuses->applicant_id;
            }
        }


        $data = AdminListing::create(Applicant::class)->processRequestAndGet(
            // pass the request with params
            $request,

            // set columns to query
            ['id', 'names',  'last_names', 'government_id', 'city_id', 'monthly_income', 'construction_entity', 'financial_entity', 'created_at'],

            // set columns to searchIn
            ['id', 'names', 'last_names', 'government_id', 'city_id'],
            function ($query) use ($financialEntity, $constructionEntity, $roles, $status, $idApplicant) {

                $query
                    ->whereIn('applicants.id', $idApplicant)

                    ->whereNull('applicants.parent_applicant')
                    ->get();
            }
        );
        //return $data;
        if ($request->ajax()) {
            if ($request->has('bulk')) {
                return [
                    'bulkItems' => $data->pluck('id')
                ];
            }
            return ['data' => $data, 'status' => $status];
        }

        return view('admin.applicant.calif-proyectos', ['data' => $data, 'status' => $status]);
    }
    public function generacionOp20(IndexEstados $request, $status)
    {

        $financialEntity = Auth::user()->financial_entity;
        $constructionEntity = Auth::user()->construction_entity;
        //$status = $request->input('status');
        //dd ($status);

        $roles = Auth::user()->getRoleNames();
        $estados = Applicant::whereNull('applicants.parent_applicant')

            ->get();
        $idApplicant = [];
        foreach ($estados as $value) {

            if ($value->statuses->status_id == $status) {
                $idApplicant[] = $value->statuses->applicant_id;
            }
        }


        $data = AdminListing::create(Applicant::class)->processRequestAndGet(
            // pass the request with params
            $request,

            // set columns to query
            ['id', 'names',  'last_names', 'government_id', 'city_id', 'monthly_income', 'construction_entity', 'financial_entity', 'created_at'],

            // set columns to searchIn
            ['id', 'names', 'last_names', 'government_id', 'city_id'],
            function ($query) use ($financialEntity, $constructionEntity, $roles, $status, $idApplicant) {

                $query
                    ->whereIn('applicants.id', $idApplicant)

                    ->whereNull('applicants.parent_applicant')
                    ->get();
            }
        );
        //return $data;
        if ($request->ajax()) {
            if ($request->has('bulk')) {
                return [
                    'bulkItems' => $data->pluck('id')
                ];
            }
            return ['data' => $data, 'status' => $status];
        }

        return view('admin.applicant.gen-op-20', ['data' => $data, 'status' => $status]);
    }

    public function processed(IndexApplicant $request)
    {

        $financialEntity = Auth::user()->financial_entity;
        $constructionEntity = Auth::user()->construction_entity;

        $roles = Auth::user()->getRoleNames();

        $data = AdminListing::create(Applicant::class)
            ->processRequestAndGet(
                // pass the request with params
                $request,

                // set columns to query
                ['id', 'names', 'last_names', 'government_id', 'city_id', 'construction_entity', 'financial_entity', 'monthly_income'],

                // set columns to searchIn
                ['id', 'names', 'last_names', 'government_id', 'city_id'],
                function ($query) use ($financialEntity, $constructionEntity, $roles) {
                    $query->whereNotIn('applicants.id', ApplicantStatus::where('status_id', '=', 14)->pluck('applicant_id')->toArray())
                        ->whereNull('applicants.parent_applicant')
                        ->with('financial', 'construction');
                    if (isset($constructionEntity)) {
                        $query
                            ->where('applicants.construction_entity', '=', $constructionEntity);
                    }

                    if (isset($financialEntity)) {
                        $query
                            ->where('applicants.financial_entity', '=', $financialEntity);
                    }
                    /*$query
                    ->selectRaw('applicants.id as id,applicants.names as names,applicants.last_names as last_names,applicants.government_id as government_id,cities.name as city_name,
                    (select count(*) from applicants as beneficiaries where beneficiaries.parent_applicant = applicants.id ) as total_beneficiaries,
                    coalesce(coalesce((select sum(beneficiaries.monthly_income) from applicants as beneficiaries
                    inner join applicant_relationships on beneficiaries.applicant_relationship = applicant_relationships.id
                    where beneficiaries.parent_applicant = applicants.id and compute_income = true ),0) + coalesce(applicants.monthly_income,0),0) as total_monthly_income,
                    construction_entity, financial_entity, construction_entities.name as centityname, financial_entities.name as fentityname,
                    (select statuses."name" as applicant_status from statuses WHERE id= (SELECT max(status_id) from applicant_statuses where applicants."id"= applicant_statuses.applicant_id)),
                    (select statuses."id" as applicant_status_id from statuses WHERE id= (SELECT max(status_id) from applicant_statuses where applicants."id"= applicant_statuses.applicant_id))

                    ')
                    ->leftJoin('cities', 'applicants.city_id', '=', 'cities.id')
                    ->leftJoin('construction_entities', 'applicants.construction_entity', '=', 'construction_entities.id')
                    ->leftJoin('financial_entities', 'applicants.financial_entity', '=', 'financial_entities.id')
                    ->whereNotIn('applicants.id', ApplicantStatus::where('status_id','=',14)->pluck('applicant_id')->toArray())
                    ->whereNull('applicants.parent_applicant')
                    ->with('financial', 'construction');

                if (isset($constructionEntity)) {
                    $query
                        ->where('applicants.construction_entity', '=', $constructionEntity);
                }

                if (isset($financialEntity)) {
                    $query
                        ->where('applicants.financial_entity', '=', $financialEntity);
                }*/
                }
            );

        if ($request->ajax()) {
            if ($request->has('bulk')) {
                return [
                    'bulkItems' => $data->pluck('id')
                ];
            }
            return ['data' => $data];
        }

        return view('admin.applicant.processed', ['data' => $data]);
    }
    public function assignedcoop(IndexApplicant $request)
    {

        $financialEntity = Auth::user()->financial_entity;
        $constructionEntity = Auth::user()->construction_entity;

        $roles = Auth::user()->getRoleNames();

        $data = AdminListing::create(Applicant::class)
            ->processRequestAndGet(
                // pass the request with params
                $request,

                // set columns to query
                ['id', 'names', 'last_names', 'government_id', 'city_id', 'construction_entity', 'financial_entity', 'monthly_income'],

                // set columns to searchIn
                ['id', 'names', 'last_names', 'government_id', 'city_id'],
                function ($query) use ($financialEntity, $constructionEntity, $roles) {
                    $query->whereNotIn('applicants.id', ApplicantStatus::where('status_id', '=', 14)->pluck('applicant_id')->toArray())
                        ->whereNull('applicants.parent_applicant')
                        ->with('financial', 'construction');

                    $query->whereNotNull('applicants.financial_entity');
                    $query->where('applicants.financial_entity_eval', true);
                    $query->where('applicants.construction_entity_eval', false);
                    $query->whereNull('applicants.construction_entity');
                }
            );

        if ($request->ajax()) {
            if ($request->has('bulk')) {
                return [
                    'bulkItems' => $data->pluck('id')
                ];
            }
            return ['data' => $data];
        }

        return view('admin.applicant.assigcoop', ['data' => $data]);
    }
    public function migrated(IndexApplicant $request)
    {

        $financialEntity = Auth::user()->financial_entity;
        $constructionEntity = Auth::user()->construction_entity;

        $roles = Auth::user()->getRoleNames();

        $data = AdminListing::create(Applicant::class)
            ->processRequestAndGet(
                // pass the request with params
                $request,

                // set columns to query
                ['id', 'names', 'last_names', 'government_id', 'city_id', 'construction_entity', 'financial_entity', 'monthly_income'],

                // set columns to searchIn
                ['id', 'names', 'last_names', 'government_id', 'city_id'],
                function ($query) use ($financialEntity, $constructionEntity, $roles) {
                    $query->whereNotIn('applicants.id', ApplicantStatus::where('status_id', '=', 14)->pluck('applicant_id')->toArray())
                        ->whereNull('applicants.parent_applicant')
                        ->with('financial', 'construction');

                    $query->whereNull('applicants.created_by');
                }
            );

        if ($request->ajax()) {
            if ($request->has('bulk')) {
                return [
                    'bulkItems' => $data->pluck('id')
                ];
            }
            return ['data' => $data];
        }

        return view('admin.applicant.migrated', ['data' => $data]);
    }
    public function assignedatc(IndexApplicant $request)
    {

        $financialEntity = Auth::user()->financial_entity;
        $constructionEntity = Auth::user()->construction_entity;

        $roles = Auth::user()->getRoleNames();

        $data = AdminListing::create(Applicant::class)
            ->processRequestAndGet(
                // pass the request with params
                $request,

                // set columns to query
                ['id', 'names', 'last_names', 'government_id', 'city_id', 'construction_entity', 'financial_entity', 'monthly_income'],

                // set columns to searchIn
                ['id', 'names', 'last_names', 'government_id', 'city_id'],
                function ($query) use ($financialEntity, $constructionEntity, $roles) {
                    $query->whereNotIn('applicants.id', ApplicantStatus::where('status_id', '=', 14)->pluck('applicant_id')->toArray())
                        ->whereNull('applicants.parent_applicant')
                        ->with('financial', 'construction');

                    $query->whereNotNull('applicants.construction_entity');
                    //$query->orwhere('applicants.construction_entity_eval', true);
                }
            );

        if ($request->ajax()) {
            if ($request->has('bulk')) {
                return [
                    'bulkItems' => $data->pluck('id')
                ];
            }
            return ['data' => $data];
        }

        return view('admin.applicant.migrated', ['data' => $data]);
    }
    public function recordnumber(IndexApplicant $request)
    {

        $financialEntity = Auth::user()->financial_entity;
        $constructionEntity = Auth::user()->construction_entity;

        $roles = Auth::user()->getRoleNames();

        $data = AdminListing::create(Applicant::class)
            ->processRequestAndGet(
                // pass the request with params
                $request,

                // set columns to query
                ['id', 'names', 'last_names', 'government_id', 'city_id', 'construction_entity', 'financial_entity', 'monthly_income'],

                // set columns to searchIn
                ['id', 'names', 'last_names', 'government_id', 'city_id'],
                function ($query) use ($financialEntity, $constructionEntity, $roles) {
                    $query->whereNotIn('applicants.id', ApplicantStatus::where('status_id', '=', 14)->pluck('applicant_id')->toArray())
                        ->whereNull('applicants.parent_applicant')
                        ->with('financial', 'construction');

                    $query->whereNotNull('applicants.resolution_number');
                }
            );

        if ($request->ajax()) {
            if ($request->has('bulk')) {
                return [
                    'bulkItems' => $data->pluck('id')
                ];
            }
            return ['data' => $data];
        }

        return view('admin.applicant.recordnumber', ['data' => $data]);
    }
    public function disqualified(IndexApplicant $request)
    {

        $financialEntity = Auth::user()->financial_entity;
        $constructionEntity = Auth::user()->construction_entity;

        $roles = Auth::user()->getRoleNames();

        $data = AdminListing::create(Applicant::class)->processRequestAndGet(
            // pass the request with params
            $request,

            // set columns to query
            ['id', 'names', 'last_names', 'government_id', 'city_name', 'total_beneficiaries', 'total_monthly_income', 'construction_entity', 'financial_entity', 'centityname', 'fentityname'],

            // set columns to searchIn
            ['id', 'names', 'last_names', 'government_id', 'cities.name', 'construction_entities.name', 'financial_entities.name'],
            function ($query) use ($financialEntity, $constructionEntity, $roles) {

                $query
                    ->selectRaw('applicants.id as id,applicants.names as names,applicants.last_names as last_names,applicants.government_id as government_id,cities.name as city_name,
                    (select count(*) from applicants as beneficiaries where beneficiaries.parent_applicant = applicants.id ) as total_beneficiaries,
                    coalesce(coalesce((select sum(beneficiaries.monthly_income) from applicants as beneficiaries
                    inner join applicant_relationships on beneficiaries.applicant_relationship = applicant_relationships.id
                    where beneficiaries.parent_applicant = applicants.id and compute_income = true ),0) + coalesce(applicants.monthly_income,0),0) as total_monthly_income,
                    construction_entity, financial_entity, construction_entities.name as centityname, financial_entities.name as fentityname,
                    (select statuses."name" as applicant_status from statuses WHERE id= (SELECT max(status_id) from applicant_statuses where applicants."id"= applicant_statuses.applicant_id)),
                    (select statuses."id" as applicant_status_id from statuses WHERE id= (SELECT max(status_id) from applicant_statuses where applicants."id"= applicant_statuses.applicant_id))

                    ')
                    ->leftJoin('cities', 'applicants.city_id', '=', 'cities.id')
                    ->leftJoin('construction_entities', 'applicants.construction_entity', '=', 'construction_entities.id')
                    ->leftJoin('financial_entities', 'applicants.financial_entity', '=', 'financial_entities.id')
                    ->whereIn('applicants.id', ApplicantStatus::where('status_id', '=', 14)->pluck('applicant_id')->toArray())
                    ->whereNull('applicants.parent_applicant')
                    ->with('financial', 'construction');

                if (isset($constructionEntity)) {
                    $query
                        ->where('applicants.construction_entity', '=', $constructionEntity);
                }

                if (isset($financialEntity)) {
                    $query
                        ->where('applicants.financial_entity', '=', $financialEntity);
                }
            }
        );

        if ($request->ajax()) {
            if ($request->has('bulk')) {
                return [
                    'bulkItems' => $data->pluck('id')
                ];
            }
            return ['data' => $data];
        }

        return view('admin.applicant.disqualified', ['data' => $data]);
    }

    public function export($status)
    {
        return Excel::download(new ApplicantsExport($status), 'applicants.xlsx');
    }

    public function create(Request $request)
    {

        $this->authorize('admin.applicant.create');

        $cities = City::all();
        $educationlevels = EducationLevel::all();
        $diseases = Disease::all();
        $disabilities = Disability::all();
        $contactmethods = ContactMethod::all();

        return view('admin.applicant.create', compact('cities', 'educationlevels', 'diseases', 'disabilities', 'contactmethods'));
    }

    public function store(StoreApplicant $request)
    {
        // $request->government_id;
        $applicants = Applicant::where('government_id', '=', $request->government_id)->get();
        if ($applicants->count() > 0) {
            if ($request->ajax()) {
                return ['notify' => ['type' => 'success', 'title' => 'Atención', 'message' => 'El soliciantes ya se encuentra en la base de datos']];
                //exit;
            }
        }

        // exit;
        /*   return response()->json([
            'error' => true,
            'message' => 'Error de comunicación'
        ]); */
        // Sanitize input
        $sanitized = $request->getSanitized();
        //dd($sanitized);
        //exit;
        $sanitized['city_id'] = $request->getCityId();
        $sanitized['familiar_setting_id'] = NULL;
        $sanitized['education_level'] = $request->getEducationLevelId();
        $sanitized['financial_entity'] = Auth::user()->financial_entity;

        $diseases = $sanitized['diseasecomponents'];
        $disabilities = $sanitized['disabilitycomponents'];
        $contactmethods = $sanitized['contactcomponents'];
        //dd($sanitized);
        // Store the Applicant
        $applicant = Applicant::create($sanitized);

        if ($applicant) {
            try {
                ApplicantStatus::create([
                    'applicant_id' => $applicant->id,
                    'user_id' => Auth::user()->id,
                    'status_id' => 1
                ]);

                foreach ($diseases as $disease) {
                    if (!is_null($disease['id'])) {
                        ApplicantDisease::create(['applicant_id' => $applicant->id, 'disease_id' => $disease['id']['id']]);
                    }
                }

                foreach ($disabilities as $disability) {
                    if (!is_null($disability['id'])) {
                        ApplicantDisability::create(['applicant_id' => $applicant->id, 'disability_id' => $disability['id']['id']]);
                    }
                }

                foreach ($contactmethods as $contactmethod) {
                    if (!is_null($contactmethod['id']) && !is_null($contactmethod['description']) && strlen($contactmethod['description']) > 0) {
                        ApplicantContactMethod::create(['applicant_id' => $applicant->id, 'contact_method_id' => $contactmethod['id']['id'], 'description' => $contactmethod['description']]);
                    }
                }
            } catch (\Exception $exception) {
                dd($exception->getMessage());
            }
        }

        if ($request->ajax()) {
            return ['redirect' => url('admin/applicants/' . $applicant->id . '/edit#benficiarios'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
        }

        return redirect('admin/applicants');
    }

    public function migrations(Applicant $applicant, Request $request)
    {
        return view('admin.applicant.migrations', [
            'data' => $applicant
        ]);
    }

    public function edit(Applicant $applicant, Request $request)
    {
        //return $request;
        $this->authorize('admin.applicant.edit', $applicant);

        $cities = City::all();
        $educationlevels = EducationLevel::all();
        $diseases = Disease::all();
        $disabilities = Disability::all();
        $contactmethods = ContactMethod::all();
        $atcs = ConstructionEntity::orderBy('name', 'asc')->get();;
        $familiarSettings = FamiliarSetting::all();
        $coops = FinancialEntity::all();
        $status_id = ApplicantStatus::where('applicant_id', '=', $applicant->id)->max('status_id');
        $statuslist = ApplicantStatus::where('applicant_id', '=', $applicant->id)->orderBy('id', 'desc')->get();
        //dd(json_encode($statuslist));
        $statu = Status::where('id', '=', $status_id)->pluck('name');
        $statuName = Status::orderBy('name', 'asc')->get();
        //$supervisors = Supervisor::orderBy('name', 'desc')->get();
        $supervisors = Supervisor::whereNotIn('id', [2, 3])->orderBy('name', 'desc')->get();

        $typechecks = TypeCheck::select('id', 'name')
            ->orderBy('name')
            ->get();
        $users = AdminUser::select('id', 'first_name', 'last_name')
            ->whereNotIn('id', [3, 4, 1, 2, 5]) //exluidos
            ->orderBy('first_name')
            ->get();
        $client = new Client();
        $vinSoli = [];
        /*
        //$url = "http://192.168.98.192:8000/api/getexpe/" . $id;
        $url = "http://192.168.98.192:8000/api/getdataexpe/" . $applicant->government_id;
        $res = $client->request('GET', $url, [
            'connect_timeout' => 10,
            'http_errors' => true,
            'headers' => ['Content-Type' => 'application/json', 'Accept' => 'application/json'],
        ]);
        //return $res;

        if ($res->getStatusCode() == 200) {
            $json = json_decode($res->getBody(), true);
            //return $json;
            $vinSoli['postulante'] = $json;
        }
*/
        $aux = [];
        foreach ($applicant->applicantDiseases as $key => $disease) {
            $aux[$key]['id'] = $disease->disease->toArray();
        }
        $saveddiseases = $aux;

        $aux = [];
        foreach ($applicant->applicantDisabilities as $key => $disability) {
            $aux[$key]['id'] = $disability->disability->toArray();
        }
        $saveddisabilities = $aux;

        $aux = [];
        foreach ($applicant->applicantContactMethods as $key => $method) {
            $aux[$key]['id'] = $method->contactMethod->toArray();
            $aux[$key]['description'] = $method->toArray()['description'];
        }
        $savedcontactmethods = $aux;

        $questionnaireID = 1;
        $questions = QuestionnaireTemplate::where('id', $questionnaireID)->with('questionnaireTemplateQuestions')->get();
        $answers = collect([]);
        if ($questions->count() > 0) {
            $answers = ApplicantAnswer::where('applicant_id', $applicant->id)->where('questionnaire_template_id', $questionnaireID)->get();
        }

        $questionnaireID = 2;
        $questions2 = QuestionnaireTemplate::where('id', $questionnaireID)->with('questionnaireTemplateQuestions')->get();
        $answers2 = collect([]);
        if ($questions2->count() > 0) {
            $answers2 = ApplicantAnswer::where('applicant_id', $applicant->id)->where('questionnaire_template_id', $questionnaireID)->get();
        }

        $applicantID = $applicant->id;
        $documentType = 'S';

        $socialDocuments = AdminListing::create(ApplicantDocument::class)->processRequestAndGet(
            $request,
            ['applicant_documents.id', 'document_types.name', 'received_at'],
            ['applicant_documents.id', 'document_types.name'],
            function ($query) use ($applicantID, $documentType) {
                $query
                    ->leftJoin('document_types', 'document_types.id', '=', 'applicant_documents.document_id')
                    ->where('applicant_documents.applicant_id', '=', $applicantID)
                    ->where('document_types.type', '=', $documentType);
            }
        );

        $documentType = 'F';

        $finDocuments = AdminListing::create(ApplicantDocument::class)->processRequestAndGet(
            $request,
            ['applicant_documents.id', 'document_types.name', 'received_at'],
            ['applicant_documents.id', 'document_types.name'],
            function ($query) use ($applicantID, $documentType) {
                $query
                    ->leftJoin('document_types', 'document_types.id', '=', 'applicant_documents.document_id')
                    ->where('applicant_documents.applicant_id', '=', $applicantID)
                    ->where('document_types.type', '=', $documentType);
            }
        );

        $documentType = 'T';

        $techDocuments = AdminListing::create(ApplicantDocument::class)->processRequestAndGet(
            $request,
            ['applicant_documents.id', 'document_types.name', 'received_at'],
            ['applicant_documents.id', 'document_types.name'],
            function ($query) use ($applicantID, $documentType) {
                $query
                    ->leftJoin('document_types', 'document_types.id', '=', 'applicant_documents.document_id')
                    ->where('applicant_documents.applicant_id', '=', $applicantID)
                    ->where('document_types.type', '=', $documentType);
            }
        );

        // create and AdminListing instance for a specific model and
        $beneficiaries = AdminListing::create(Applicant::class)->processRequestAndGet(
            // pass the request with params
            $request,
            // set columns to query
            ['applicants.id', 'names', 'last_names', 'government_id', 'monthly_income', 'applicant_relationship'],
            // set columns to searchIn
            ['applicants.id', 'names', 'last_names', 'government_id', 'applicant_relationships.name'],
            function ($query) use ($applicantID) {
                $query
                    ->leftJoin('applicant_relationships', 'applicant_relationship', '=', 'applicant_relationships.id')
                    ->where('parent_applicant', '=', $applicantID);
            }
        );
        $confirm = new Collection([
            ['id' => '1', 'descri' => 'Aprobado'],
            ['id' => '2', 'descri' => 'Pendiente'],
            ['id' => '3', 'descri' => 'Inactivar']

        ]);
        //dd($vinSoli);
        return view('admin.applicant.edit', [
            'applicant' => $applicant,
            'educationlevels' => $educationlevels,
            'diseases' => $diseases,
            'disabilities' => $disabilities,
            'contactmethods' => $contactmethods,
            'cities' => $cities,
            'atcs' => $atcs,
            'familiarSettings' => $familiarSettings,
            'coops' => json_decode($coops),
            'saveddiseases' => json_encode($saveddiseases),
            'saveddisabilities' => json_encode($saveddisabilities),
            'savedcontactmethods' => json_encode($savedcontactmethods),
            'questions' => $questions,
            'answers' => $answers,
            'questions2' => $questions2,
            'answers2' => $answers2,
            'socialDocuments' => $socialDocuments,
            'finDocuments' => $finDocuments,
            'techDocuments' => $techDocuments,
            'beneficiaries' => $beneficiaries,
            'statuse' => $statu,
            'statusName' => $statuName,
            'statuslist' => json_decode($statuslist),
            'confirmations' => json_encode($confirm),
            'vinSoli' => $vinSoli,
            'supervisors' => $supervisors,
            'typechecks' => $typechecks,
            'users' => $users,
        ]);
    }

    public function addsupervisor(Applicant $applicant, Request $request)
    {
        //return $request;

        /* $sanitized = $request->getSanitized();
        $sanitized ['applicant_id'] =  $request->applicant_id;
        $sanitized ['supervisor_id'] =  $request->supervisor_id->id;
        $sanitized ['fecha_ws'] =  $request->fecha_ws; */
        $trustsave = Work::create([
            'applicant_id' => $request['applicant_id'],
            'supervisor_id' => $request['supervisor_id'],
            'fecha_ws' => $request['fecha_ws']
        ]);

        if ($request->ajax()) {
            return ['redirect' => url('admin/applicants/' . $applicant->id . '/edit'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
        }

        return redirect('admin/applicants/' . $applicant->id . '/edit#informaciones');
    }
    public function addtype(Applicant $applicant, IndexApplicant $request)
    {
        $data = $request->all();
        //return $data;
        $existe = WorksTypeCheck::where('work_id', $request['work_id'])->where('type_check_id', $request['type_check_id'])->get();
        //return $existe->count();
        if ($existe->count() > 0) {
            if ($request->ajax()) {
                //return ['notify' => ['type' => 'success', 'title' => 'Atención', 'message' => 'El soliciante ya fue cargado.']];
                return ['redirect' => url('admin/applicants/' . $applicant->id . '/edit'), 'message' => 'La verificacion ya fue cargada. Favor verificar..!!!'];
            }
        }

        $trustsave = WorksTypeCheck::create([
            'work_id' => $applicant->work[0]->id,
            'fecha_check' => $request['fecha'],
            'type_check_id' => $request['type_check_id'],
            'applicant_id' => $applicant->id
        ]);
        if ($trustsave) {
            if ($request->ajax()) {
                //return $sanitized;
                return ['redirect' => url('admin/applicants/' . $applicant->id . '/edit'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
            }
        }
        return redirect('admin/applicants/' . $applicant->id . '/edit');
        //return ['redirect' => url('admin/trusts/'.$request['trust_id'].'/edit'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
    }

    public function update(UpdateApplicant $request, Applicant $applicant)
    {
        //return $request->all();
        $sanitized = $request->getSanitized();

        $sanitized['city_id'] = $request->getCityId();
        $sanitized['education_level'] = $request->getEducationLevelId();
        $sanitized['familiar_setting_id'] = $request->getFamilarSettingId();

        $diseases = $sanitized['diseasecomponents'];
        $disabilities = $sanitized['disabilitycomponents'];
        $contactmethods = $sanitized['contactcomponents'];
        //dd($sanitized);
        ApplicantDisease::where('applicant_id', $applicant->id)->delete();
        ApplicantDisability::where('applicant_id', $applicant->id)->delete();
        ApplicantContactMethod::where('applicant_id', $applicant->id)->delete();

        // Update changed values Applicant
        if ($applicant->update($sanitized)) {
            foreach ($diseases as $disease) {
                if (!is_null($disease['id'])) {
                    ApplicantDisease::create(['applicant_id' => $applicant->id, 'disease_id' => $disease['id']['id']]);
                }
            }

            foreach ($disabilities as $disability) {
                if (!is_null($disability['id'])) {
                    ApplicantDisability::create(['applicant_id' => $applicant->id, 'disability_id' => $disability['id']['id']]);
                }
            }

            foreach ($contactmethods as $contactmethod) {
                if (!is_null($contactmethod['id']) && !is_null($contactmethod['description']) && strlen($contactmethod['description']) > 0) {
                    ApplicantContactMethod::create(['applicant_id' => $applicant->id, 'contact_method_id' => $contactmethod['id']['id'], 'description' => $contactmethod['description']]);
                }
            }
            return ['notify' => ['type' => 'success', 'title' => 'Actualizaciones', 'message' => 'Se realizo con exito la actualización']];
        }

        if ($request->ajax()) {

            return ['redirect' => url('admin/applicants/' . $applicant->id . '/edit#benficiarios'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
        }

        return redirect('admin/applicants/' . $applicant->id . '/edit#informaciones');
    }

    /*  public function show(Applicant $applicant)
    {
        $this->authorize('admin.applicant.show', $applicant);

        // TODO your code goes here
    } */

    public function destroy(DestroyApplicant $request, Applicant $applicant)
    {
        $applicant->delete();

        if ($request->ajax()) {
            return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
        }

        return redirect()->back();
    }

    public function bulkDestroy(BulkDestroyApplicant $request): Response
    {
        DB::transaction(static function () use ($request) {
            collect($request->data['ids'])
                ->chunk(1000)
                ->each(static function ($bulkChunk) {
                    Applicant::whereIn('id', $bulkChunk)->delete();

                    // TODO your code goes here
                });
        });

        return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
    }

    /* Fin Applicants */


    /* Documents */

    public function indexDocuments(IndexApplicantDocument $request, $id, $type)
    {

        $applicantID = $id;
        $documentType = $type;

        $data = AdminListing::create(ApplicantDocument::class)->processRequestAndGet(
            $request,
            ['applicant_documents.id', 'document_types.name', 'received_at'],
            ['applicant_documents.id', 'document_types.name'],
            function ($query) use ($applicantID, $documentType) {
                $query
                    ->leftJoin('document_types', 'document_types.id', '=', 'applicant_documents.document_id')
                    ->where('applicant_documents.applicant_id', '=', $applicantID)
                    ->where('document_types.type', '=', $documentType);
            }
        );

        if ($request->ajax()) {
            if ($request->has('bulk')) {
                return [
                    'bulkItems' => $data->pluck('id')
                ];
            }
            return ['data' => $data];
        }

        return view('admin.applicant-document.index', ['data' => $data]);
    }

    public function documentCreate(Request $request, $id, $type)
    {

        $this->authorize('admin.applicant-document.create');

        $applicantID = $id;

        $used = ApplicantDocument::leftJoin('document_types', 'document_types.id', '=', 'applicant_documents.document_id')
            ->where('applicant_id', '=', $applicantID)
            ->where('document_types.type', '=', $type)
            ->pluck('document_types.id')->toArray();

        $documents = DocumentType::where('type', '=', $type)
            ->whereNotIn('id', $used)->get();

        return view('admin.applicant-document.create', compact('applicantID', 'documents', 'type'));
    }

    public function storeApplicantDocument(StoreApplicantDocument $request, $id)
    {

        $sanitized = $request->getSanitized();

        $sanitized['document_id'] = $request->getDocumentId();

        $applicantDocument = ApplicantDocument::create($sanitized);

        if ($request->ajax()) {
            return ['redirect' => url('admin/applicants/' . $id . '/edit'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
        }

        return redirect('admin/applicant-documents');
    }

    public function documentUpdate(UpdateApplicantDocument $request, ApplicantDocument $applicantDocument)
    {

        // Sanitize input
        $sanitized = $request->getSanitized();

        // Update changed values ApplicantDocument
        $applicantDocument->update($sanitized);

        if ($request->ajax()) {
            return [
                'redirect' => url('admin/applicant-documents'),
                'message' => trans('brackets/admin-ui::admin.operation.succeeded'),
            ];
        }

        return redirect('admin/applicant-documents');
    }

    /* Fin Documents */

    /* Beneficiary */

    public function indexBeneficiary(IndexBeneficiary $request, $id)
    {

        $applicantID = $id;

        $data = AdminListing::create(Applicant::class)->processRequestAndGet(
            $request,
            // set columns to query
            ['applicants.id', 'names', 'last_names', 'government_id', 'monthly_income', 'applicant_relationship'],
            // set columns to searchIn
            ['applicants.id', 'names', 'last_names', 'government_id', 'applicant_relationships.name'],
            function ($query) use ($applicantID) {
                $query
                    ->leftJoin('applicant_relationships', 'applicant_relationship', '=', 'applicant_relationships.id')
                    ->where('parent_applicant', '=', $applicantID);
            }
        );

        if ($request->ajax()) {
            if ($request->has('bulk')) {
                return [
                    'bulkItems' => $data->pluck('id')
                ];
            }
            return ['data' => $data];
        }

        return view('admin.applicant.index', ['data' => $data]);
    }

    public function createBeneficiary(Request $request, Applicant $applicant)
    {

        $this->authorize('admin.applicant.create');

        $educationlevels = EducationLevel::all();
        $diseases = Disease::all();
        $disabilities = Disability::all();
        $contactmethods = ContactMethod::all();
        $applicantrelationships = ApplicantRelationship::all();

        return view('admin.applicant.create-beneficiary', compact('applicant', 'applicantrelationships', 'educationlevels', 'diseases', 'disabilities', 'contactmethods'));
    }

    public function storeBeneficiary(StoreBeneficiary $request, Applicant $applicant)
    {

        $applicants = Applicant::where('government_id', '=', $request->government_id)->get();
        if ($request->getApplicantRelationshipId() != 2)
            if ($applicants->count() > 0) {
                if ($request->ajax()) {
                    return ['notify' => ['type' => 'Error', 'title' => 'Atención', 'message' => 'La persona ya se encuentra en la base de datos del Proyecto AMA']];
                    //exit;
                }
            }

        $sanitized = $request->getSanitized();

        $sanitized['applicant_relationship'] = $request->getApplicantRelationshipId();
        $sanitized['education_level'] = $request->getEducationLevelId();
        $sanitized['parent_applicant'] = $applicant->id;


        $diseases = $sanitized['diseasecomponents'];
        $disabilities = $sanitized['disabilitycomponents'];
        $contactmethods = $sanitized['contactcomponents'];

        // Store the Applicant
        $beneficiary = Applicant::create($sanitized);

        if ($beneficiary) {
            try {

                foreach ($diseases as $disease) {
                    if (!is_null($disease['id'])) {
                        ApplicantDisease::create(['applicant_id' => $beneficiary->id, 'disease_id' => $disease['id']['id']]);
                    }
                }

                foreach ($disabilities as $disability) {
                    if (!is_null($disability['id'])) {
                        ApplicantDisability::create(['applicant_id' => $beneficiary->id, 'disability_id' => $disability['id']['id']]);
                    }
                }

                foreach ($contactmethods as $contactmethod) {
                    if (!is_null($contactmethod['id']) && !is_null($contactmethod['description']) && strlen($contactmethod['description']) > 0) {
                        ApplicantContactMethod::create(['applicant_id' => $beneficiary->id, 'contact_method_id' => $contactmethod['id']['id'], 'description' => $contactmethod['description']]);
                    }
                }
            } catch (\Exception $exception) {
                dd($exception->getMessage());
            }
        }

        if ($request->ajax()) {
            return ['redirect' => url('admin/applicants/' . $applicant->id . '/edit#benficiarios'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
        }

        return redirect('admin/applicants');
    }

    public function editBeneficiary(Applicant $applicant, Request $request, Applicant $beneficiary)
    {

        $this->authorize('admin.applicant.edit');

        $educationlevels = EducationLevel::all();
        $diseases = Disease::all();
        $disabilities = Disability::all();
        $contactmethods = ContactMethod::all();
        $applicantrelationships = ApplicantRelationship::all();

        $aux = [];
        foreach ($beneficiary->applicantDiseases as $key => $disease) {
            $aux[$key]['id'] = $disease->disease->toArray();
        }
        $saveddiseases = json_encode($aux);

        $aux = [];
        foreach ($beneficiary->applicantDisabilities as $key => $disability) {
            $aux[$key]['id'] = $disability->disability->toArray();
        }
        $saveddisabilities = json_encode($aux);

        $aux = [];
        foreach ($beneficiary->applicantContactMethods as $key => $method) {
            $aux[$key]['id'] = $method->contactMethod->toArray();
            $aux[$key]['description'] = $method->toArray()['description'];
        }
        $savedcontactmethods = json_encode($aux);

        return view('admin.applicant.edit-beneficiary', compact(
            'beneficiary',
            'applicant',
            'educationlevels',
            'diseases',
            'disabilities',
            'contactmethods',
            'applicantrelationships',
            'saveddiseases',
            'saveddisabilities',
            'savedcontactmethods'
        ));
    }

    public function updateBeneficiary(UpdateBeneficiary $request, Applicant $applicant, Applicant $beneficiary)
    {

        $sanitized = $request->getSanitized();

        $sanitized['applicant_relationship'] = $request->getApplicantRelationshipId();
        $sanitized['education_level'] = $request->getEducationLevelId();

        $diseases = $sanitized['diseasecomponents'];
        $disabilities = $sanitized['disabilitycomponents'];
        $contactmethods = $sanitized['contactcomponents'];

        ApplicantDisease::where('applicant_id', $beneficiary->id)->delete();
        ApplicantDisability::where('applicant_id', $beneficiary->id)->delete();
        ApplicantContactMethod::where('applicant_id', $beneficiary->id)->delete();

        // Update changed values Applicant
        if ($beneficiary->update($sanitized)) {
            foreach ($diseases as $disease) {
                if (!is_null($disease['id'])) {
                    ApplicantDisease::create(['applicant_id' => $beneficiary->id, 'disease_id' => $disease['id']['id']]);
                }
            }

            foreach ($disabilities as $disability) {
                if (!is_null($disability['id'])) {
                    ApplicantDisability::create(['applicant_id' => $beneficiary->id, 'disability_id' => $disability['id']['id']]);
                }
            }

            foreach ($contactmethods as $contactmethod) {
                if (!is_null($contactmethod['id']) && !is_null($contactmethod['description']) && strlen($contactmethod['description']) > 0) {
                    ApplicantContactMethod::create(['applicant_id' => $beneficiary->id, 'contact_method_id' => $contactmethod['id']['id'], 'description' => $contactmethod['description']]);
                }
            }
        }

        if ($request->ajax()) {
            return ['redirect' => url('admin/applicants/' . $applicant->id . '/edit#benficiarios'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
        }

        return redirect('admin/applicants');
    }

    /* Fin Beneficiary */

    public function questionnaire(\Illuminate\Http\Request $request, Applicant $applicant, $template)
    {

        $this->authorize('admin.applicant.edit');
        $data = $request->all();
        $questions = $data['questions'];

        ApplicantAnswer::where('applicant_id', $applicant->id)->where('questionnaire_template_id', $template)->delete();

        foreach ($questions as $question) {
            ApplicantAnswer::create([
                'answer' => $question['value'],
                'extended_value' => $question['comment_text'],
                'question_id' => $question['id'],
                'received_at' => $question['received_at_value'],
                'questionnaire_template_id' => $question['template'],
                'applicant_id' => $applicant->id
            ]);
        }


        if ($request->ajax()) {
            return ['notify' => ['type' => 'success', 'title' => 'Atención', 'message' => 'Respuestas actualizadas']];
        }
    }


    public function getIdentificaciones($id)
    {




        $client = new Client();

        try {
            $url = "http://" . env("URL_ENV", "192.168.195.1:8080") . "/mbohape-core/sii/security";
            $res = $client->request('POST', $url, [
                'connect_timeout' => 10,
                'http_errors' => true,
                'headers' => ['Content-Type' => 'application/json', 'Accept' => 'application/json'],
                'json' => [
                    'username' => 'senavitatconsultas',
                    'password' => 'S3n4vitat'
                ],
            ]);
            //192.168.202.43
            //dd($res);
            if ($res->getStatusCode() == 200) {
                $json = json_decode($res->getBody(), true);
                $token = $json['token'];
                //local:10.1.79.7
                //192.168.195.1
                $url = "http://" . env("URL_ENV", "192.168.195.1:8080") . '/frontend-identificaciones/api/persona/obtenerPersonaPorCedula/' . $id;
                $res = $client->request('GET', $url, [
                    'connect_timeout' => 10,
                    'http_errors' => true,
                    'headers' => [
                        'Content-Type' => 'application/json',
                        'Accept' => 'application/json',
                        'Authorization' => 'Bearer ' . $token,
                    ],
                ]);

                if ($res->getStatusCode() == 200) {
                    $json = json_decode($res->getBody(), true);
                    return response()->json([
                        'error' => false,
                        'message' => $json['obtenerPersonaPorNroCedulaResponse']['return']
                    ]);
                } else {
                    return response()->json([
                        'error' => true,
                        'message' => 'Error de comunicación en Consulta'
                    ]);
                }
            } else {
                return response()->json([
                    'error' => true,
                    'message' => 'Error de Autenticación'
                ]);
            }
        } catch (\Exception $exception) {
            dd($exception);
            return response()->json([
                'error' => true,
                'message' => 'Error de comunicación'
            ]);
        }
    }

    public function hadBenefit($id)
    {

        $client = new Client();

        try {
            $url = "http://192.168.98.192:8000/api/getexpe/" . $id;
            $res = $client->request('GET', $url, [
                'connect_timeout' => 10,
                'http_errors' => true,
                'headers' => ['Content-Type' => 'application/json', 'Accept' => 'application/json'],
            ]);

            if ($res->getStatusCode() == 200) {
                $json = json_decode($res->getBody(), true);
                return response()->json([
                    'error' => false,
                    'message' => $json
                ]);
            } else {
                return response()->json([
                    'error' => true,
                    'message' => 'Error de Consulta'
                ]);
            }
        } catch (\Exception $exception) {
            return response()->json([
                'error' => true,
                'message' => 'Error de comunicación'
            ]);
        }
    }

    public function findapplicant($id)
    {


        try {
            $rest = Applicant::whereNull('applicants.parent_applicant')->where('government_id', '=', "$id")->get();

            //return($rest);
            if ($rest->count() > 0) {
                return response()->json([
                    'cod' => 1,
                    'error' => false,
                    'message' => 'El Postulante ya se encuentra en los registros del Proyecto AMA'
                ]);
            }
        } catch (\Exception $exception) {
            return response()->json([
                'error' => true,
                'message' => 'Error de comunicación'
            ]);
        }
    }

    public function findApplicantRecord($id)
    {


        try {
            $rest = Applicant::whereNull('applicants.parent_applicant')->where('government_id', '=', "$id")->first();

            //return($rest);
            if ($rest->count() > 0) {
                return response()->json([
                    'id' => $rest['id'],
                    'error' => false,
                    'nombre' => $rest['names'],
                    'apellido' => $rest['last_names'],
                    'message' => 'El Postulante ya se encuentra en los registros del Proyecto AMA'
                ]);
            } else {
                return response()->json([
                    'cod' => 1,
                    'error' => true,
                    'message' => 'No se encuentra'
                ]);
            }
        } catch (\Exception $exception) {
            return response()->json([
                'error' => true,
                'message' => 'Error de comunicación'
            ]);
        }
    }

    public function confirmAtc(Applicant $applicant, Request $request)
    {
        $typefinancial = 'T';

        $val = new ValidateDocuments($typefinancial, $applicant->id, 4);
        $reply = $val->validateRegular();
        $ResponseValidate = array();
        $ResponseValidate['message'] = '';
        $ResponseValidate['error'] = $reply['error'];
        $ResponseValidate['message'] .= $reply['message'];

        if ($request->ajax() && $ResponseValidate['error'] == 1) {
            return ['notify' => ['type' => 'success', 'title' => 'Atención', 'message' => $ResponseValidate['message']]];
        }
        $ResponseValidate = array();

        $applicant->construction_entity_eval = true;
        $applicant->save();

        if ($request->ajax()) {
            return ['redirect' => url('admin/applicants'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
        }

        return redirect('admin/applicants');
    }
    public function assigAtc(Applicant $applicant, Request $request)
    {

        $applicant->construction_entity = $request->atc['id'];
        if ($applicant->save()) {
            AssignmentHistory::create([
                'applicant_id' => $applicant->id,
                'entity_id' => $request->atc['id'],
                'assignment_date' => date('Y-m-d'),
                'observations' => $request->observations,
                'user_id' => Auth::user()->id,
            ]);
        }


        if ($request->ajax()) {
            return ['redirect' => url('admin/applicants'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
        }

        return redirect('admin/applicants');
    }
    public function assigAtcEdit(Applicant $applicant, Request $request)
    {

        //return ($request);
        //exit;
        $applicant->construction_entity = $request->atc;
        $applicant->atc_fec_asig = $request->fec_atc;
        if ($applicant->save()) {
            ApplicantStatus::create([
                'applicant_id' => $applicant->id,
                'user_id' => Auth::user()->id,
                'status_id' => $applicant->statuses->status_id,
                'description' => 'Se ha realizado la asignacion de ATCs de codigo ' . $request->atc

            ]);
        }



        if ($request->ajax()) {
            return ['redirect' => url('admin/applicants/' . $applicant->id . '/edit#informacion'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
        }

        return redirect('admin/applicants/' . $applicant->id . '/edit#informacion');
    }

    public function assigCoop(Applicant $applicant, Request $request)
    {

        $applicant->financial_entity = $request->coop;
        $applicant->financial_entity_eval = true;

        if ($applicant->save()) {
            if ($request->ajax()) {
                return ['redirect' => url('admin/applicants'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
            }
            return redirect('admin/applicants/' . $applicant->id . '/edit');
        } else {
            return ['redirect' => url('admin/applicants'), 'message' => trans('brackets/admin-ui::admin.operation.error')];
        }
    }

    public function formStatus(Applicant $applicant, Request $request)
    {

        $status = Status::where('id', $request->status)
            ->first();
        if (!isset($request->status)) {
            return redirect('admin/applicants/' . $applicant->id . '/edit')->with('message', 'ERROR: Debe seleccionar un estado para cambiar');
        }

        ApplicantStatus::create([
            'applicant_id' => $applicant->id,
            'user_id' => Auth::user()->id,
            'status_id' => $request->status,
            'description' => $request->obs

        ]);

        if ($applicant->created_by) {
            //return $request->user_notify;
            $user = User::where('id', $applicant->created_by)
                ->first();

            if ($request->user_notify) {

                try {
                    $receiver = $user->name;
                    $objDemo = new \stdClass();
                    $objDemo->name = $applicant->names . ' ' . $applicant->last_names;
                    $objDemo->status = $status->name;
                    $objDemo->sender = 'Equipo AMA';
                    $objDemo->obs = $request->obs;
                    $objDemo->receiver = $receiver;


                    //dd($objDemo);

                    Mail::to($user->email)->send(new EmailStatus($objDemo));
                    //return new EmailStatus($objDemo);
                } catch (\Throwable $th) {
                    dd($th);
                }
            }
        }




        if ($request->ajax()) {
            return ['redirect' => url('admin/applicants'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
        }
        //return ['notify' => ['type' => 'success', 'title' => 'Atención', 'message' => 'Estado Actualizado']];
        return redirect('admin/applicants/' . $applicant->id . '/edit');
    }



    public function confirmAma(Applicant $applicant, Request $request)
    {
        $data = $request->all();

        $applicant->file_number = $data['file_number'];
        $applicant->resolution_number = $data['resolution_number'];
        $applicant->receiving_file_date = date('Y-m-d');
        $applicant->resolution_date = $data['resolution_date'];
        $applicant->file_date = $data['file_date'];

        if ($applicant->save()) {
            return ['notify' => [
                'type' => 'error',
                'title' => 'Atención',
                'message' => 'El Numero de Solicitud ya esta Registrado. No puede duplicarse ',
                'redirect' => url('admin/applicants/' . $applicant->id . '/edit#informacion'),
            ]];
            /* return [

                'redirect' => url('admin/applicants/'.$applicant->id.'/edit#informacion'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')]; */
        }


        // return redirect('admin/applicants'.$applicant->id.'/edit#informacion');
    }
    public function reassigAtc(Applicant $applicant, Request $request)
    {
        $data = $request->all();
        //reassig
        //dd($data['observations']);
        $applicant->file_number = $data['file_number'];
        $applicant->resolution_number = $data['resolution_number'];
        $applicant->receiving_file_date = date('Y-m-d');
        $applicant->atc_fec_asig = date('Y-m-d');
        $applicant->resolution_date = $data['resolution_date'];
        $applicant->file_date = $data['file_date'];
        $applicant->construction_entity = $data['atc']['id'];
        $observations = $data['observations'];
        ApplicantStatus::create([
            'applicant_id' => $applicant->id,
            'user_id' => Auth::user()->id,
            'status_id' => 34, //Reasignaciones
            'description' => $observations,

        ]);

        if ($applicant->save()) {
            return ['redirect' => url('admin/applicants/' . $applicant->id . '/edit#informaciones'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
        }
        if ($request->ajax()) {
            return ['redirect' => url('admin/applicants/' . $applicant->id . '/edit#informaciones'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
        }

        return redirect('admin/applicants/' . $applicant->id . '/edit#informaciones');
    }

    public function  graphic(IndexApplicant $request)
    {
        $roles = Auth::user()->getRoleNames();
        $inscipciones = Applicant::whereNull('applicants.parent_applicant')
            //->whereIn('applicants.id', ApplicantStatus::where('status_id', '=', 1)->pluck('applicant_id')->toArray())0
            ->with('statuses')

            ->get();

        //return $inscipciones;
        //->unique('statuses.status_id');
        $datos_a_Insrpciones = [];

        $id = 1;
        $contador = 0;
        $contadorname = 'Solicitud de Inscripcion';

        $id3 = 3;
        $contador3 = 0;
        $contadorname3 = 'Pre Evaluacion AMA';

        $id14 = 14;
        $contador14 = 0;
        $contadorname14 = 'Descalificado';

        $id15 = 15;
        $contador15 = 0;
        $contadorname15 = 'Cancelado';

        $id17 = 17;
        $contador17 = 0;
        $contadorname17 = '1.1 A distribuir';

        $id18 = 18;
        $contador18 = 0;
        $contadorname18 = '1.2 Pendiente Proyecto ATC';

        $id19 = 19;
        $contador19 = 0;
        $contadorname19 = '1.3 En Corrección de Pyto CT-ATC';

        $id20 = 20;
        $contador20 = 0;
        $contadorname20 = '1.4 Pyto Aprobado para Mesa de Entrada - ATC';

        $id21 = 21;
        $contador21 = 0;
        $contadorname21 = '2.2 En proceso de Calificación Técnica';

        $id22 = 22;
        $contador22 = 0;
        $contadorname22 = '2.1 En proceso de Calificación Social';

        $id40 = 23;
        $contador40 = 0;
        $contadorname40 = '2.3 Pendiente de Informe Viabilidad Financiera';

        $id23 = 23;
        $contador23 = 0;
        $contadorname23 = '2.4 Pendiente Resolución';

        $id24 = 24;
        $contador24 = 0;
        $contadorname24 = '3.1 A iniciar con Resolución';

        $id39 = 39;
        $contador39 = 0;
        $contadorname39 = '3.2 A iniciar Pendiente OP 80%';

        $id25 = 25;
        $contador25 = 0;
        $contadorname25 = '3.2 En ejecución';

        $id26 = 26;
        $contador26 = 0;
        $contadorname26 = '3.3 Culminado';

        $id26 = 26;
        $contador26 = 0;
        $contadorname26 = '3.3 Culminado';

        $id27 = 27;
        $contador27 = 0;
        $contadorname27 = '3.10 Paralizado';

        $id28 = 28;
        $contador28 = 0;
        $contadorname28 = '4.5 Rescindido';

        $id29 = 29;
        $contador29 = 0;
        $contadorname29 = '4.1 Inhabilitado en inscripción';

        $id30 = 30;
        $contador30 = 0;
        $contadorname30 = '4.2 Inhabilitado en postulación';

        $id31 = 31;
        $contador31 = 0;
        $contadorname31 = '4.3 Inhabilitado por ATC';

        $id32 = 32;
        $contador32 = 0;
        $contadorname32 = '4.4 Renuncia';

        $id37 = 37;
        $contador37 = 0;
        $contadorname37 = '1.1 En revisión Coord. Social';

        $id38 = 38;
        $contador38 = 0;
        $contadorname38 = '1.2 A distribuir a ATC Coord. Técnica';

        $id41 = 41;
        $contador41 = 0;
        $contadorname41 = '3.3 A iniciar OP remitido a CAC';

        $id42 = 42;
        $contador42 = 0;
        $contadorname42 = '3.6 Pendiente de OP 20%';

        $id43 = 43;
        $contador43 = 0;
        $contadorname43 = '3.7 Orden de Pago 20% remitido a la CAC';

        $id47 = 47;
        $contador47 = 0;
        $contadorname47 = '3.5 Culminada S/ Reporte de Supervisor';

        $id44 = 44;
        $contador44 = 0;
        $contadorname44 = '3.8 Proceso Concluido';

        $id45 = 45;
        $contador45 = 0;
        $contadorname45 = 'Condicionados';

        $id46 = 46;
        $contador46 = 0;
        $contadorname46 = 'Pendiente Dictamen Jurídico';

        $id48 = 48;
        $contador48 = 0;
        $contadorname48 = 'Lista de Espera';

        $id49 = 49;
        $contador49 = 0;
        $contadorname49 = 'Inhabilitado en Lista de Espera';

        $id50 = 50;
        $contador50 = 0;
        $contadorname50 = '3.1 Pendientes de Pólizas';

        $id33 = 0;
        $contador33 = 0;
        $contadorname33 = 0;

        $dataStatus = [];
        foreach ($inscipciones as $app) {





            try {
                if ($app->statuses->status->id == 1) {
                    $contador = $contador + 1;
                    $contadorname = $app->statuses->status->name;
                    $id = $app->statuses->status->id;
                }
            } catch (\Throwable $th) {
                Log::info('Info log test applicants-id ' . $app->id);
                $applicantError = $app->id;
                Log::info('Info log test applicants-id ' . $app->id);
                $para  = 'fcardozob@gmail.com';
                $título = 'Error en Laravel por STATUS';
                $cabeceras  = 'MIME-Version: 1.0' . "\r\n";
                $cabeceras .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
                $mensaje = '
                    <html>
                    <head>
                    <title>Error AMA</title>
                    </head>
                    <body>
                    <p>Laravel ERROR Mail: ' . $applicantError . '
                    <br> Error:' . $th . '</p>

                    </body>
                    </html>
                    ';
                mail($para, $título, $mensaje, $cabeceras);
            }

            if ($app->statuses->status->id == 3) {
                $contador3 = $contador3 + 1;
                $contadorname3 = $app->statuses->status->name;
                $id3 = $app->statuses->status->id;
            }

            if ($app->statuses->status->id == 14) {
                $contador14 = $contador14 + 1;
                $contadorname14 = $app->statuses->status->name;
                $id14 = $app->statuses->status->id;
            }
            if ($app->statuses->status->id == 15) {
                $contador15 = $contador15 + 1;
                $contadorname15 = $app->statuses->status->name;
                $id15 = $app->statuses->status->id;
            }

            if ($app->statuses->status->id == 17) {
                $contador17 = $contador17 + 1;
                $contadorname17 = $app->statuses->status->name;
                $id17 = $app->statuses->status->id;
            }
            if ($app->statuses->status->id == 18) {
                $contador18 = $contador18 + 1;
                $contadorname18 = $app->statuses->status->name;
                $id18 = $app->statuses->status->id;
            }
            if ($app->statuses->status->id == 19) {
                $contador19 = $contador19 + 1;
                $contadorname19 = $app->statuses->status->name;
                $id19 = $app->statuses->status->id;
            }
            if ($app->statuses->status->id == 20) {
                $contador20 = $contador20 + 1;
                $contadorname20 = $app->statuses->status->name;
                $id20 = $app->statuses->status->id;
            }
            if ($app->statuses->status->id == 21) {
                $contador21 = $contador21 + 1;
                $contadorname21 = $app->statuses->status->name;
                $id21 = $app->statuses->status->id;
            }
            if ($app->statuses->status->id == 22) {
                $contador22 = $contador22 + 1;
                $contadorname22 = $app->statuses->status->name;
                $id22 = $app->statuses->status->id;
            }
            if ($app->statuses->status->id == 23) {
                $contador23 = $contador23 + 1;
                $contadorname23 = $app->statuses->status->name;
                $id23 = $app->statuses->status->id;
            }
            if ($app->statuses->status->id == 24) {
                $contador24 = $contador24 + 1;
                $contadorname24 = $app->statuses->status->name;
                $id24 = $app->statuses->status->id;
            }
            if ($app->statuses->status->id == 25) {
                $contador25 = $contador25 + 1;
                $contadorname25 = $app->statuses->status->name;
                $id25 = $app->statuses->status->id;
            }
            if ($app->statuses->status->id == 26) {
                $contador26 = $contador26 + 1;
                $contadorname26 = $app->statuses->status->name;
                $id26 = $app->statuses->status->id;
            }
            if ($app->statuses->status->id == 27) {
                $contador27 = $contador27 + 1;
                $contadorname27 = $app->statuses->status->name;
                $id27 = $app->statuses->status->id;
            }
            if ($app->statuses->status->id == 28) {
                $contador28 = $contador28 + 1;
                $contadorname28 = $app->statuses->status->name;
                $id28 = $app->statuses->status->id;
            }
            if ($app->statuses->status->id == 29) {
                $contador29 = $contador29 + 1;
                $contadorname29 = $app->statuses->status->name;
                $id29 = $app->statuses->status->id;
            }
            if ($app->statuses->status->id == 30) {
                $contador30 = $contador30 + 1;
                $contadorname30 = $app->statuses->status->name;
                $id30 = $app->statuses->status->id;
            }
            if ($app->statuses->status->id == 31) {
                $contador31 = $contador31 + 1;
                $contadorname31 = $app->statuses->status->name;
                $id31 = $app->statuses->status->id;
            }
            if ($app->statuses->status->id == 32) {
                $contador32 = $contador32 + 1;
                $contadorname32 = $app->statuses->status->name;
                $id32 = $app->statuses->status->id;
            }
            if ($app->statuses->status->id == 33) {
                $contador33 = $contador33 + 1;
                $contadorname33 = $app->statuses->status->name;
                $id33 = $app->statuses->status->id;
            }

            if ($app->statuses->status->id == 39) {
                $contador39 = $contador39 + 1;
                $contadorname39 = $app->statuses->status->name;
                $id39 = $app->statuses->status->id;
            }

            if ($app->statuses->status->id == 37) {
                $contador37 = $contador37 + 1;
                $contadorname37 = $app->statuses->status->name;
                $id37 = $app->statuses->status->id;
            }
            if ($app->statuses->status->id == 38) {
                $contador38 = $contador38 + 1;
                $contadorname38 = $app->statuses->status->name;
                $id38 = $app->statuses->status->id;
            }
            if ($app->statuses->status->id == 40) {
                $contador40 = $contador40 + 1;
                $contadorname40 = $app->statuses->status->name;
                $id40 = $app->statuses->status->id;
            }
            if ($app->statuses->status->id == 41) {
                $contador41 = $contador41 + 1;
                $contadorname41 = $app->statuses->status->name;
                $id41 = $app->statuses->status->id;
            }
            if ($app->statuses->status->id == 42) {
                $contador42 = $contador42 + 1;
                $contadorname42 = $app->statuses->status->name;
                $id42 = $app->statuses->status->id;
            }
            if ($app->statuses->status->id == 43) {
                $contador43 = $contador43 + 1;
                $contadorname43 = $app->statuses->status->name;
                $id43 = $app->statuses->status->id;
            }
            if ($app->statuses->status->id == 44) {
                $contador44 = $contador44 + 1;
                $contadorname44 = $app->statuses->status->name;
                $id44 = $app->statuses->status->id;
            }
            if ($app->statuses->status->id == 45) {
                $contador45 = $contador45 + 1;
                $contadorname45 = $app->statuses->status->name;
                $id45 = $app->statuses->status->id;
            }
            if ($app->statuses->status->id == 46) {
                $contador46 = $contador46 + 1;
                $contadorname46 = $app->statuses->status->name;
                $id46 = $app->statuses->status->id;
            }
            if ($app->statuses->status->id == 47) {
                $contador47 = $contador47 + 1;
                $contadorname47 = $app->statuses->status->name;
                $id47 = $app->statuses->status->id;
            }
            if ($app->statuses->status->id == 48) {
                $contador48 = $contador48 + 1;
                $contadorname48 = $app->statuses->status->name;
                $id48 = $app->statuses->status->id;
            }

            if ($app->statuses->status->id == 49) {
                $contador49 = $contador49 + 1;
                $contadorname49 = $app->statuses->status->name;
                $id49 = $app->statuses->status->id;
            }
            if ($app->statuses->status->id == 50) {
                $contador50 = $contador50 + 1;
                $contadorname50 = $app->statuses->status->name;
                $id50 = $app->statuses->status->id;
            }
        }

        $dataStatus[] = [
            'name' => $contadorname,
            'cont' => $contador,
            'id' => $id
        ];

        $dataStatus[] = [
            'name' => $contadorname3,
            'cont' => $contador3,
            'id' => $id3
        ];

        $dataStatus[] = [
            'name' => $contadorname14,
            'cont' => $contador14,
            'id' => $id14
        ];
        $dataStatus[] = [
            'name' => $contadorname15,
            'cont' => $contador15,
            'id' => $id15
        ];

        $dataStatus[] = [
            'name' => $contadorname17,
            'cont' => $contador17,
            'id' => $id17
        ];

        $dataStatus[] = [
            'name' => $contadorname37,
            'cont' => $contador37,
            'id' => $id37
        ];
        $dataStatus[] = [
            'name' => $contadorname38,
            'cont' => $contador38,
            'id' => $id38
        ];
        $dataStatus[] = [
            'name' => $contadorname18,
            'cont' => $contador18,
            'id' => $id18
        ];
        $dataStatus[] = [
            'name' => $contadorname19,
            'cont' => $contador19,
            'id' => $id19
        ];
        $dataStatus[] = [
            'name' => $contadorname20,
            'cont' => $contador20,
            'id' => $id20
        ];
        $dataStatus[] = [
            'name' => $contadorname22,
            'cont' => $contador22,
            'id' => $id22
        ];
        $dataStatus[] = [
            'name' => $contadorname21,
            'cont' => $contador21,
            'id' => $id21
        ];
        $dataStatus[] = [
            'name' => $contadorname40,
            'cont' => $contador40,
            'id' => $id40
        ];

        $dataStatus[] = [
            'name' => $contadorname23,
            'cont' => $contador23,
            'id' => $id23
        ];

        $dataStatus[] = [
            'name' => $contadorname50,
            'cont' => $contador50,
            'id' => $id50
        ];

        $dataStatus[] = [
            'name' => $contadorname24,
            'cont' => $contador24,
            'id' => $id24
        ];
        $dataStatus[] = [
            'name' => $contadorname39,
            'cont' => $contador39,
            'id' => $id39
        ];
        $dataStatus[] = [
            'name' => $contadorname41,
            'cont' => $contador41,
            'id' => $id41
        ];
        $dataStatus[] = [
            'name' => $contadorname25,
            'cont' => $contador25,
            'id' => $id25
        ];
        $dataStatus[] = [
            'name' => $contadorname47,
            'cont' => $contador47,
            'id' => $id47
        ];
        $dataStatus[] = [
            'name' => $contadorname26,
            'cont' => $contador26,
            'id' => $id26
        ];
        $dataStatus[] = [
            'name' => $contadorname42,
            'cont' => $contador42,
            'id' => $id42
        ];
        $dataStatus[] = [
            'name' => $contadorname43,
            'cont' => $contador43,
            'id' => $id43
        ];



        $dataStatus[] = [
            'name' => $contadorname44,
            'cont' => $contador44,
            'id' => $id44
        ];
        $dataStatus[] = [
            'name' => $contadorname27,
            'cont' => $contador27,
            'id' => $id27
        ];

        $dataStatus[] = [
            'name' => $contadorname29,
            'cont' => $contador29,
            'id' => $id29
        ];
        $dataStatus[] = [
            'name' => $contadorname30,
            'cont' => $contador30,
            'id' => $id30
        ];
        $dataStatus[] = [
            'name' => $contadorname31,
            'cont' => $contador31,
            'id' => $id31
        ];
        $dataStatus[] = [
            'name' => $contadorname32,
            'cont' => $contador32,
            'id' => $id32
        ];
        $dataStatus[] = [
            'name' => $contadorname28,
            'cont' => $contador28,
            'id' => $id28
        ];
        $dataStatus[] = [
            'name' => $contadorname33,
            'cont' => $contador33,
            'id' => $id33
        ];
        $dataStatus[] = [
            'name' => $contadorname45,
            'cont' => $contador45,
            'id' => $id45
        ];
        $dataStatus[] = [
            'name' => $contadorname46,
            'cont' => $contador46,
            'id' => $id46
        ];
        $dataStatus[] = [
            'name' => $contadorname48,
            'cont' => $contador48,
            'id' => $id48
        ];

        $dataStatus[] = [
            'name' => $contadorname49,
            'cont' => $contador49,
            'id' => $id49
        ];



        //View::share('site_settings', $dataStatus);


        //View::share('site_settings', $dataStatus);
        //dd($dataStatus);
        return view('admin.applicant.dashboard', [

            'dataStatus' => $dataStatus,
            'roles' => $roles
        ]);

        // return view('admin.applicant-document.index', ['data' => $data]);
    }
    public function  gerencial(IndexApplicant $request)
    {
        $roles = Auth::user()->getRoleNames();
        $inscipciones = Applicant::whereNull('applicants.parent_applicant')
            //->whereIn('applicants.id', ApplicantStatus::where('status_id', '=', 1)->pluck('applicant_id')->toArray())0
            ->with('statuses')

            ->get();

        //return $inscipciones;
        //->unique('statuses.status_id');
        $datos_a_Insrpciones = [];

        $id = 1;
        $contador = 0;
        $contadorname = 'Solicitud de Inscripcion';

        $id3 = 3;
        $contador3 = 0;
        $contadorname3 = 'Pre Evaluacion AMA';

        $id14 = 14;
        $contador14 = 0;
        $contadorname14 = 'Descalificado';

        $id15 = 15;
        $contador15 = 0;
        $contadorname15 = 'Cancelado';

        $id17 = 17;
        $contador17 = 0;
        $contadorname17 = '1.1 A distribuir';

        $id18 = 18;
        $contador18 = 0;
        $contadorname18 = '1.2 Pendiente Proyecto ATC';

        $id19 = 19;
        $contador19 = 0;
        $contadorname19 = '1.3 En Corrección de Pyto CT-ATC';

        $id20 = 20;
        $contador20 = 0;
        $contadorname20 = '1.4 Pyto Aprobado para Mesa de Entrada - ATC';

        $id21 = 21;
        $contador21 = 0;
        $contadorname21 = '2.2 En proceso de Calificación Técnica';

        $id22 = 22;
        $contador22 = 0;
        $contadorname22 = '2.1 En proceso de Calificación Social';

        $id40 = 40;
        $contador40 = 0;
        $contadorname40 = '2.3 Pendiente de Informe Viabilidad Financiera';

        $id23 = 23;
        $contador23 = 0;
        $contadorname23 = '2.4 Pendiente Resolución';

        $id24 = 24;
        $contador24 = 0;
        $contadorname24 = '3.1 A iniciar con Resolución';

        $id39 = 39;
        $contador39 = 0;
        $contadorname39 = '3.2 A iniciar Pendiente OP 80%';

        $id25 = 25;
        $contador25 = 0;
        $contadorname25 = '3.2 En ejecución';

        $id26 = 26;
        $contador26 = 0;
        $contadorname26 = '3.3 Culminado';

        $id26 = 26;
        $contador26 = 0;
        $contadorname26 = '3.3 Culminado';

        $id27 = 27;
        $contador27 = 0;
        $contadorname27 = '3.9 Paralizado';

        $id28 = 28;
        $contador28 = 0;
        $contadorname28 = '3.10 Rescindido';

        $id29 = 29;
        $contador29 = 0;
        $contadorname29 = '4.1 Inhabilitado en inscripción';

        $id30 = 30;
        $contador30 = 0;
        $contadorname30 = '4.2 Inhabilitado en postulación';

        $id31 = 31;
        $contador31 = 0;
        $contadorname31 = '4.3 Inhabilitado por ATC';

        $id32 = 32;
        $contador32 = 0;
        $contadorname32 = '4.4 Renuncia';

        $id37 = 37;
        $contador37 = 0;
        $contadorname37 = '1.1 En revisión Coord. Social';

        $id38 = 38;
        $contador38 = 0;
        $contadorname38 = '1.2 A distribuir a ATC Coord. Técnica';

        $id50 = 50;
        $contador50 = 0;
        $contadorname50 = '3.1 Pendiente de Polizas';

        $id41 = 41;
        $contador41 = 0;
        $contadorname41 = '3.3 A iniciar OP remitido a CAC';

        $id42 = 42;
        $contador42 = 0;
        $contadorname42 = '3.6 Pendiente de OP 20%';

        $id43 = 43;
        $contador43 = 0;
        $contadorname43 = '3.7 Orden de Pago 20% remitido a la CAC';

        $id44 = 44;
        $contador44 = 0;
        $contadorname44 = '3.8 Proceso Concluido';

        $id47 = 47;
        $contador47 = 0;
        $contadorname47 = '3.8 Proceso Concluido';

        $id33 = 0;
        $contador33 = 0;
        $contadorname33 = 0;

        $dataStatus = [];
        foreach ($inscipciones as $app) {





            try {
                if ($app->statuses->status->id == 1) {
                    $contador = $contador + 1;
                    $contadorname = $app->statuses->status->name;
                    $id = $app->statuses->status->id;
                }
            } catch (\Throwable $th) {
                Log::info('Info log test applicants-id ' . $app->id);
                $applicantError = $app->id;
                Log::info('Info log test applicants-id ' . $app->id);
                $para  = 'fcardozob@gmail.com';
                $título = 'Error en Laravel por STATUS';
                $cabeceras  = 'MIME-Version: 1.0' . "\r\n";
                $cabeceras .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
                $mensaje = '
                    <html>
                    <head>
                    <title>Error AMA</title>
                    </head>
                    <body>
                    <p>Laravel ERROR Mail: ' . $applicantError . '
                    <br> Error:' . $th . '</p>

                    </body>
                    </html>
                    ';
                mail($para, $título, $mensaje, $cabeceras);
            }

            if ($app->statuses->status->id == 3) {
                $contador3 = $contador3 + 1;
                $contadorname3 = $app->statuses->status->name;
                $id3 = $app->statuses->status->id;
            }

            if ($app->statuses->status->id == 14) {
                $contador14 = $contador14 + 1;
                $contadorname14 = $app->statuses->status->name;
                $id14 = $app->statuses->status->id;
            }
            if ($app->statuses->status->id == 15) {
                $contador15 = $contador15 + 1;
                $contadorname15 = $app->statuses->status->name;
                $id15 = $app->statuses->status->id;
            }

            if ($app->statuses->status->id == 17) {
                $contador17 = $contador17 + 1;
                $contadorname17 = $app->statuses->status->name;
                $id17 = $app->statuses->status->id;
            }
            if ($app->statuses->status->id == 18) {
                $contador18 = $contador18 + 1;
                $contadorname18 = $app->statuses->status->name;
                $id18 = $app->statuses->status->id;
            }
            if ($app->statuses->status->id == 19) {
                $contador19 = $contador19 + 1;
                $contadorname19 = $app->statuses->status->name;
                $id19 = $app->statuses->status->id;
            }
            if ($app->statuses->status->id == 20) {
                $contador20 = $contador20 + 1;
                $contadorname20 = $app->statuses->status->name;
                $id20 = $app->statuses->status->id;
            }
            if ($app->statuses->status->id == 21) {
                $contador21 = $contador21 + 1;
                $contadorname21 = $app->statuses->status->name;
                $id21 = $app->statuses->status->id;
            }
            if ($app->statuses->status->id == 22) {
                $contador22 = $contador22 + 1;
                $contadorname22 = $app->statuses->status->name;
                $id22 = $app->statuses->status->id;
            }
            if ($app->statuses->status->id == 23) {
                $contador23 = $contador23 + 1;
                $contadorname23 = $app->statuses->status->name;
                $id23 = $app->statuses->status->id;
            }
            if ($app->statuses->status->id == 24) {
                $contador24 = $contador24 + 1;
                $contadorname24 = $app->statuses->status->name;
                $id24 = $app->statuses->status->id;
            }
            if ($app->statuses->status->id == 25) {
                $contador25 = $contador25 + 1;
                $contadorname25 = $app->statuses->status->name;
                $id25 = $app->statuses->status->id;
            }
            if ($app->statuses->status->id == 26) {
                $contador26 = $contador26 + 1;
                $contadorname26 = $app->statuses->status->name;
                $id26 = $app->statuses->status->id;
            }
            if ($app->statuses->status->id == 27) {
                $contador27 = $contador27 + 1;
                $contadorname27 = $app->statuses->status->name;
                $id27 = $app->statuses->status->id;
            }
            if ($app->statuses->status->id == 28) {
                $contador28 = $contador28 + 1;
                $contadorname28 = $app->statuses->status->name;
                $id28 = $app->statuses->status->id;
            }
            if ($app->statuses->status->id == 29) {
                $contador29 = $contador29 + 1;
                $contadorname29 = $app->statuses->status->name;
                $id29 = $app->statuses->status->id;
            }
            if ($app->statuses->status->id == 30) {
                $contador30 = $contador30 + 1;
                $contadorname30 = $app->statuses->status->name;
                $id30 = $app->statuses->status->id;
            }
            if ($app->statuses->status->id == 31) {
                $contador31 = $contador31 + 1;
                $contadorname31 = $app->statuses->status->name;
                $id31 = $app->statuses->status->id;
            }
            if ($app->statuses->status->id == 32) {
                $contador32 = $contador32 + 1;
                $contadorname32 = $app->statuses->status->name;
                $id32 = $app->statuses->status->id;
            }
            if ($app->statuses->status->id == 33) {
                $contador33 = $contador33 + 1;
                $contadorname33 = $app->statuses->status->name;
                $id33 = $app->statuses->status->id;
            }

            if ($app->statuses->status->id == 39) {
                $contador39 = $contador39 + 1;
                $contadorname39 = $app->statuses->status->name;
                $id39 = $app->statuses->status->id;
            }

            if ($app->statuses->status->id == 37) {
                $contador37 = $contador37 + 1;
                $contadorname37 = $app->statuses->status->name;
                $id37 = $app->statuses->status->id;
            }
            if ($app->statuses->status->id == 38) {
                $contador38 = $contador38 + 1;
                $contadorname38 = $app->statuses->status->name;
                $id38 = $app->statuses->status->id;
            }
            if ($app->statuses->status->id == 40) {
                $contador40 = $contador40 + 1;
                $contadorname40 = $app->statuses->status->name;
                $id40 = $app->statuses->status->id;
            }
            if ($app->statuses->status->id == 41) {
                $contador41 = $contador41 + 1;
                $contadorname41 = $app->statuses->status->name;
                $id41 = $app->statuses->status->id;
            }
            if ($app->statuses->status->id == 42) {
                $contador42 = $contador42 + 1;
                $contadorname42 = $app->statuses->status->name;
                $id42 = $app->statuses->status->id;
            }
            if ($app->statuses->status->id == 43) {
                $contador43 = $contador43 + 1;
                $contadorname43 = $app->statuses->status->name;
                $id43 = $app->statuses->status->id;
            }
            if ($app->statuses->status->id == 44) {
                $contador44 = $contador44 + 1;
                $contadorname44 = $app->statuses->status->name;
                $id44 = $app->statuses->status->id;
            }
            if ($app->statuses->status->id == 47) {
                $contador47 = $contador47 + 1;
                $contadorname47 = $app->statuses->status->name;
                $id47 = $app->statuses->status->id;
            }
            if ($app->statuses->status->id == 50) {
                $contador50 = $contador50 + 1;
                $contadorname50 = $app->statuses->status->name;
                $id50 = $app->statuses->status->id;
            }
        }

        $dataStatus[] = [
            'name' => $contadorname,
            'cont' => $contador,
            'id' => $id
        ];

        $dataStatus[] = [
            'name' => $contadorname3,
            'cont' => $contador3,
            'id' => $id3
        ];

        $dataStatus[] = [
            'name' => $contadorname14,
            'cont' => $contador14,
            'id' => $id14
        ];
        $dataStatus[] = [
            'name' => $contadorname15,
            'cont' => $contador15,
            'id' => $id15
        ];

        $dataStatus[] = [
            'name' => $contadorname17,
            'cont' => $contador17,
            'id' => $id17
        ];

        $dataStatus[] = [
            'name' => $contadorname37,
            'cont' => $contador37,
            'id' => $id37
        ];
        $dataStatus[] = [
            'name' => $contadorname38,
            'cont' => $contador38,
            'id' => $id38
        ];
        $dataStatus[] = [
            'name' => $contadorname18,
            'cont' => $contador18,
            'id' => $id18
        ];
        $dataStatus[] = [
            'name' => $contadorname19,
            'cont' => $contador19,
            'id' => $id19
        ];
        $dataStatus[] = [
            'name' => $contadorname20,
            'cont' => $contador20,
            'id' => $id20
        ];
        $dataStatus[] = [
            'name' => $contadorname22,
            'cont' => $contador22,
            'id' => $id22
        ];
        $dataStatus[] = [
            'name' => $contadorname21,
            'cont' => $contador21,
            'id' => $id21
        ];
        $dataStatus[] = [
            'name' => $contadorname40,
            'cont' => $contador40,
            'id' => $id40
        ];

        $dataStatus[] = [
            'name' => $contadorname23,
            'cont' => $contador23,
            'id' => $id23
        ];
        $dataStatus[] = [
            'name' => $contadorname24,
            'cont' => $contador24,
            'id' => $id24
        ];
        $dataStatus[] = [
            'name' => $contadorname39,
            'cont' => $contador39,
            'id' => $id39
        ];
        $dataStatus[] = [
            'name' => $contadorname41,
            'cont' => $contador41,
            'id' => $id41
        ];
        $dataStatus[] = [
            'name' => $contadorname25,
            'cont' => $contador25,
            'id' => $id25
        ];
        $dataStatus[] = [
            'name' => $contadorname26,
            'cont' => $contador26,
            'id' => $id26
        ];
        $dataStatus[] = [
            'name' => $contadorname42,
            'cont' => $contador42,
            'id' => $id42
        ];
        $dataStatus[] = [
            'name' => $contadorname43,
            'cont' => $contador43,
            'id' => $id43
        ];
        $dataStatus[] = [
            'name' => $contadorname44,
            'cont' => $contador44,
            'id' => $id44
        ];
        $dataStatus[] = [
            'name' => $contadorname27,
            'cont' => $contador27,
            'id' => $id27
        ];
        $dataStatus[] = [
            'name' => $contadorname28,
            'cont' => $contador28,
            'id' => $id28
        ];
        $dataStatus[] = [
            'name' => $contadorname29,
            'cont' => $contador29,
            'id' => $id29
        ];
        $dataStatus[] = [
            'name' => $contadorname30,
            'cont' => $contador30,
            'id' => $id30
        ];
        $dataStatus[] = [
            'name' => $contadorname31,
            'cont' => $contador31,
            'id' => $id31
        ];
        $dataStatus[] = [
            'name' => $contadorname32,
            'cont' => $contador32,
            'id' => $id32
        ];
        $dataStatus[] = [
            'name' => $contadorname33,
            'cont' => $contador33,
            'id' => $id33
        ];
        $dataStatus[] = [
            'name' => $contadorname47,
            'cont' => $contador47,
            'id' => $id47
        ];
        $dataStatus[] = [
            'name' => $contadorname50,
            'cont' => $contador50,
            'id' => $id50
        ];
        //View::share('site_settings', $dataStatus);


        //View::share('site_settings', $dataStatus);
        //dd($dataStatus);
        return view('admin.applicant.gerencial', [

            'dataStatus' => $dataStatus,
            'roles' => $roles
        ]);

        // return view('admin.applicant-document.index', ['data' => $data]);
    }
    public function print(Applicant $applicant, Request $request)
    {

        $this->authorize('admin.applicant.edit', $applicant);

        $cities = City::all();
        $educationlevels = EducationLevel::all();
        $diseases = Disease::all();
        $disabilities = Disability::all();
        $contactmethods = ContactMethod::all();
        $atcs = ConstructionEntity::all();
        $coops = FinancialEntity::all();
        $status_id = ApplicantStatus::where('applicant_id', '=', $applicant->id)->max('status_id');
        $statu = Status::where('id', '=', $status_id)->pluck('name');

        $aux = [];
        foreach ($applicant->applicantDiseases as $key => $disease) {
            $aux[$key]['id'] = $disease->disease->toArray();
        }
        $saveddiseases = $aux;

        $aux = [];
        foreach ($applicant->applicantDisabilities as $key => $disability) {
            $aux[$key]['id'] = $disability->disability->toArray();
        }
        $saveddisabilities = $aux;

        $aux = [];
        foreach ($applicant->applicantContactMethods as $key => $method) {
            $aux[$key]['id'] = $method->contactMethod->toArray();
            $aux[$key]['description'] = $method->toArray()['description'];
        }
        $savedcontactmethods = $aux;

        $questionnaireID = 1;
        $questions = QuestionnaireTemplate::where('id', $questionnaireID)->with('questionnaireTemplateQuestions')->get();
        $answers = collect([]);
        if ($questions->count() > 0) {
            $answers = ApplicantAnswer::where('applicant_id', $applicant->id)->where('questionnaire_template_id', $questionnaireID)->get();
        }

        $questionnaireID = 2;
        $questions2 = QuestionnaireTemplate::where('id', $questionnaireID)->with('questionnaireTemplateQuestions')->get();
        $answers2 = collect([]);
        if ($questions2->count() > 0) {
            $answers2 = ApplicantAnswer::where('applicant_id', $applicant->id)->where('questionnaire_template_id', $questionnaireID)->get();
        }

        $applicantID = $applicant->id;
        $documentType = 'S';

        $socialDocuments = AdminListing::create(ApplicantDocument::class)->processRequestAndGet(
            $request,
            ['applicant_documents.id', 'document_types.name', 'received_at'],
            ['applicant_documents.id', 'document_types.name'],
            function ($query) use ($applicantID, $documentType) {
                $query
                    ->leftJoin('document_types', 'document_types.id', '=', 'applicant_documents.document_id')
                    ->where('applicant_documents.applicant_id', '=', $applicantID)
                    ->where('document_types.type', '=', $documentType);
            }
        );

        $documentType = 'F';

        $finDocuments = AdminListing::create(ApplicantDocument::class)->processRequestAndGet(
            $request,
            ['applicant_documents.id', 'document_types.name', 'received_at'],
            ['applicant_documents.id', 'document_types.name'],
            function ($query) use ($applicantID, $documentType) {
                $query
                    ->leftJoin('document_types', 'document_types.id', '=', 'applicant_documents.document_id')
                    ->where('applicant_documents.applicant_id', '=', $applicantID)
                    ->where('document_types.type', '=', $documentType);
            }
        );

        $documentType = 'T';

        $techDocuments = AdminListing::create(ApplicantDocument::class)->processRequestAndGet(
            $request,
            ['applicant_documents.id', 'document_types.name', 'received_at'],
            ['applicant_documents.id', 'document_types.name'],
            function ($query) use ($applicantID, $documentType) {
                $query
                    ->leftJoin('document_types', 'document_types.id', '=', 'applicant_documents.document_id')
                    ->where('applicant_documents.applicant_id', '=', $applicantID)
                    ->where('document_types.type', '=', $documentType);
            }
        );

        // create and AdminListing instance for a specific model and
        $beneficiaries = AdminListing::create(Applicant::class)->processRequestAndGet(
            // pass the request with params
            $request,
            // set columns to query
            ['applicants.id', 'names', 'last_names', 'government_id', 'monthly_income', 'applicant_relationship'],
            // set columns to searchIn
            ['applicants.id', 'names', 'last_names', 'government_id', 'applicant_relationships.name'],
            function ($query) use ($applicantID) {
                $query
                    ->leftJoin('applicant_relationships', 'applicant_relationship', '=', 'applicant_relationships.id')
                    ->where('parent_applicant', '=', $applicantID);
            }
        );
        $confirm = new Collection([
            ['id' => '1', 'descri' => 'Aprobado'],
            ['id' => '2', 'descri' => 'Pendiente'],
            ['id' => '3', 'descri' => 'Inactivar']

        ]);
        //dd($statu);
        return view('admin.applicant.print', [
            'applicant' => $applicant,
            'educationlevels' => $educationlevels,
            'diseases' => $diseases,
            'disabilities' => $disabilities,
            'contactmethods' => $contactmethods,
            'cities' => $cities,
            'atcs' => $atcs,
            'coops' => json_decode($coops),
            'saveddiseases' => json_encode($saveddiseases),
            'saveddisabilities' => json_encode($saveddisabilities),
            'savedcontactmethods' => json_encode($savedcontactmethods),
            'questions' => $questions,
            'answers' => $answers,
            'questions2' => $questions2,
            'answers2' => $answers2,
            'socialDocuments' => $socialDocuments,
            'finDocuments' => $finDocuments,
            'techDocuments' => $techDocuments,
            'beneficiaries' => $beneficiaries,
            'statuse' => $statu,
            'confirmations' => json_encode($confirm),
        ]);
    }
    public function newimportdata(Applicant $applicant, Request $request)
    {
        //nuevos registros
        return view('admin.applicant.newimportdata');
    }
    public function newstoredata(Request $request)
    {
        //nuevos registros
        $file = $request->file('file');
        Excel::import(new ApplicantsNewImport, $file);
        return "Registros se ha importado correctamente!!";
    }

    public function importdata(Applicant $applicant, Request $request)
    {
        //actualiza CI de Conyugue
        return view('admin.applicant.importdata');
    }
    public function storedata(Request $request)
    {
        //actualiza CI de Conyugue
        $file = $request->file('file');
        Excel::import(new ApplicantsImport, $file);
        return "Registros se ha importado correctamente ApplicantsImport !!";
    }
    public function reminderpn()
    {
        $datedesde = '2021-10-30'; //date('Y-m-d');
        $datehasta = date('2022-01-20');

        $applicant = Applicant::whereNull('applicants.parent_applicant')
            //->whereIn('applicants.id', ApplicantStatus::where('status_id', '=', 1)->pluck('applicant_id')->toArray())0
            ->with('financial', 'construction', 'statuses')
            ->whereNotNull('applicants.created_by')
            ->whereBetween('applicants.created_at', [$datedesde . ' 00:00:00', $datehasta . ' 23:59:59'])

            ->get();
        /*        $applicants=Applicant::where('applicants.government_id', 4955789)
        ->whereNotNull('applicants.created_by')
        ->get();
         */
        //return $applicant ;

        foreach ($applicant as $app) {

            //echo $app->statuses->status->id;
            /*       echo json_encode($app);

            exit(); */


            if ($app->created_by) {
                /*      //return $app;
                if($app->statuses->status_id==1){

                    //echo '<pre>';
                    echo($app->statuses->applicant_id);
                    //echo($app->govermen);
                    //echo '<pre/>';
                    echo ',<br/>';
                }
                //break; */
                if ($app->statuses->status->id == 3) {



                    $user = User::where('id', $app->created_by)->first();

                    try {
                        $receiver = $user->name;
                        $objDemxo = new \stdClass();
                        $objDemo->name = $app->names . ' ' . $app->last_names;
                        $objDemo->sender = 'Equipo AMA';
                        $objDemo->receiver = $receiver;

                        //dd($objDemo);

                        Mail::to($user->email)->send(new MassiveNotify($objDemo));
                        //return new EmailStatus($objDemo);
                    } catch (\Throwable $th) {
                        //dd($th);
                    }

                    ApplicantStatus::create([
                        'applicant_id' => $app->id,
                        'user_id' => Auth::user()->id,
                        'status_id' => 33, //status Notificacion por correo
                        'description' => 'Se ha notificado al usuario por coreo electronico para presentar sus documnetos respaldatorios en fecha: ' . date('d-m-Y')

                    ]);

                    echo 'Se notifico  a:' . $app->government_id . '<br/>';
                } else {
                    echo 'NO se ha notificado porque tiene estado:' . $app->statuses->status->id . ' CI:' . $app->government_id . '<br/>';
                }
            }
        } //end foreach
    }

    public function importestados(Applicant $applicant, Request $request)
    {
        //nuevos registros
        return view('admin.applicant.importstatuses');
    }
    public function estadosstore(Request $request)
    {
        //nuevos registros
        $file = $request->file('file');
        Excel::import(new ApplicantStatusesImport, $file);
        return "Registros se ha importado correctamente!!";
    }
}
