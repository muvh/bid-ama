<?php

namespace App\Http\Controllers\Admin;

use Exception;
use App\Models\Applicant;
use App\Models\TypeOrder;
use Illuminate\View\View;
use App\Models\TransferOrder;
use Illuminate\Http\Response;
use App\Models\InsuranceEntity;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Http\RedirectResponse;
use Illuminate\Contracts\View\Factory;
use Brackets\AdminListing\Facades\AdminListing;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Auth\Access\AuthorizationException;
use App\Http\Requests\Admin\TransferOrder\IndexTransferOrder;
use App\Http\Requests\Admin\TransferOrder\StoreTransferOrder;
use App\Http\Requests\Admin\TransferOrder\UpdateTransferOrder;
use App\Http\Requests\Admin\TransferOrder\DestroyTransferOrder;
use App\Http\Requests\Admin\TransferOrder\BulkDestroyTransferOrder;
use App\Models\InsurancePolicy;
use Luecano\NumeroALetras\NumeroALetras;

class TransferOrdersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param IndexTransferOrder $request
     * @return array|Factory|View
     */
    public function ordentransferencia(TransferOrder $transferOrder)
    {   
        //return $transferOrder;
        $mount=0;
        $textConcepto="";
        if($transferOrder->type_order_id==1){
            $mount=28000000;
            $textConcepto="1er. Desembolso 80% del Costo Total del Proyecto";
        }else{
            $mount=7000000;
            $textConcepto="Pago Final 20% del Costo Total del Proyecto";

        }

        $number=$mount;
        $decimals=2;
        $formatter = new NumeroALetras();
        $numeroTexto=  $formatter->toWords($number, $decimals);
        //$formatter = new NumeroALetras();
       // echo '38230.44 => '. NumeroALetras::convertir(38230.44, 'dólares') .'<br>';

       // echo $formatter->toMoney(10.10, 2, 'SOLES', 'CENTIMOS');

      $pdf = \PDF::LoadView('admin.transfer-order.ordentransferencia', [
        'transferOrder' => $transferOrder,
        'mount'=>$mount,
        'textConcepto'=>$textConcepto
    ]);
       //$pdf->loadHTML('<h1>Test</h1>');
       return $pdf->stream();
   /*      return view('admin.transfer-order.ordentransferencia', [
            'transferOrder' => $transferOrder,
        ]); */
    }
    public function index(IndexTransferOrder $request)
    {
        // create and AdminListing instance for a specific model and
        $data = AdminListing::create(TransferOrder::class)->processRequestAndGet(
            // pass the request with params
            $request,

            // set columns to query
            ['id', 'nro_solicitud', 'fecha_ot', 'importe', 'type_order_id', 'construction_entity_id', 'applicant_id', 'financial_entity_id'],

            // set columns to searchIn
            ['id', 'nro_solicitud']
        );

        if ($request->ajax()) {
            if ($request->has('bulk')) {
                return [
                    'bulkItems' => $data->pluck('id')
                ];
            }
            return ['data' => $data];
        }

        return view('admin.transfer-order.index', ['data' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @throws AuthorizationException
     * @return Factory|View
     */
    public function create($applicantID=0)
    {           
      
        $InsuranceEntities= InsuranceEntity::all();
        $typeOrders=TypeOrder::all();
        return view('admin.transfer-order.create', [
            'typeOrders'=>$typeOrders,
            'InsuranceEntities'=>$InsuranceEntities,
       

        ]);
    }
    public function createwithpoliza($applicantID=0)
    {           
        $TransferOrder='0';
        $app='0';
        if($applicantID!=0){
            $TransferOrder= TransferOrder::where('applicant_id',$applicantID)->get();
        }
       
        $app=Applicant::where('id',$applicantID)->get();
     
       // return $app;
        $InsuranceEntities= InsuranceEntity::all();
        $typeOrders=TypeOrder::all();
        return view('admin.transfer-order.createwithpoliza', [
            'typeOrders'=>$typeOrders,
            'InsuranceEntities'=>$InsuranceEntities,
            'TransferOrder'=>$TransferOrder,
            'app'=>$app

        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreTransferOrder $request
     * @return array|RedirectResponse|Redirector
     */
    public function store(StoreTransferOrder $request)
    {

        
        $applicants=TransferOrder::where('nro_solicitud','=', $request->nro_solicitud)->get();
         //dd($applicants);
         if($applicants->count() >0){
             if ($request->ajax()) {
                 return ['notify' => ['type' => 'error', 'title' => 'Atención', 'message' => 'El Numero de Solicitud ya esta Registrado. No puede duplicarse ']];
                 //exit;
             }
         }
    
       $applicants=TransferOrder::where('applicant_id','=', $request->applicant_id)
       ->where('type_order_id','=', $request->type_order_id)->get();
        //dd($applicants);
        if($applicants->count() > 0){
            if ($request->ajax()) {
                return ['notify' => ['type' => 'error', 'title' => 'Atención', 'message' => 'El solicitante ya cuenta con el mismo tipo de orden']];
                //exit;
            }
        }
   

        $sanitized = $request->getSanitized();
        $sanitized['type_order_id'] = $request->getTypeOrderlId();
        $sanitized['importe'] = str_replace('.', '', $request->importe);

        $transferOrder = TransferOrder::create($sanitized);
        if($transferOrder){
        //create poliza

        }
        if ($request->ajax()) {
            return ['redirect' => url('admin/transfer-orders'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
        }

        return redirect('admin/transfer-orders');
    }

    /**
     * Display the specified resource.
     *
     * @param TransferOrder $transferOrder
     * @throws AuthorizationException
     * @return void
     */
    public function show(TransferOrder $transferOrder)
    {
        $this->authorize('admin.transfer-order.show', $transferOrder);

        // TODO your code goes here
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param TransferOrder $transferOrder
     * @throws AuthorizationException
     * @return Factory|View
     */
    public function findApplicant($government_id)
    {
        $data=Applicant::whereNull('applicants.parent_applicant')
        ->where('government_id', $government_id)
        ->get();
        //return $data->count();

        if($data->count()>0){  //si hay datos retorna
            
            $poliza=InsurancePolicy::where('applicant_id', $data[0]->id)->get();
        return [ 
            'data'=>$data[0],
            'poliza'=>$poliza,
            'cod'=>1
        ];
    }else{
        return response()->json([
            'cod' => 0,
            'data' => "",
            
        ]);

    }
  
    }
    public function edit(TransferOrder $transferOrder)
    {
        $this->authorize('admin.transfer-order.edit', $transferOrder);


        return view('admin.transfer-order.edit', [
            'transferOrder' => $transferOrder,
        ]);
    }
    

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateTransferOrder $request
     * @param TransferOrder $transferOrder
     * @return array|RedirectResponse|Redirector
     */
    public function update(UpdateTransferOrder $request, TransferOrder $transferOrder)
    {
        // Sanitize input
        $sanitized = $request->getSanitized();

        // Update changed values TransferOrder
        $transferOrder->update($sanitized);

        if ($request->ajax()) {
            return [
                'redirect' => url('admin/transfer-orders'),
                'message' => trans('brackets/admin-ui::admin.operation.succeeded'),
            ];
        }

        return redirect('admin/transfer-orders');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param DestroyTransferOrder $request
     * @param TransferOrder $transferOrder
     * @throws Exception
     * @return ResponseFactory|RedirectResponse|Response
     */
    public function destroy(DestroyTransferOrder $request, TransferOrder $transferOrder)
    {
        $transferOrder->delete();

        if ($request->ajax()) {
            return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
        }

        return redirect()->back();
    }

    /**
     * Remove the specified resources from storage.
     *
     * @param BulkDestroyTransferOrder $request
     * @throws Exception
     * @return Response|bool
     */
    public function bulkDestroy(BulkDestroyTransferOrder $request) : Response
    {
        DB::transaction(static function () use ($request) {
            collect($request->data['ids'])
                ->chunk(1000)
                ->each(static function ($bulkChunk) {
                    TransferOrder::whereIn('id', $bulkChunk)->delete();

                    // TODO your code goes here
                });
        });

        return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
    }
}
