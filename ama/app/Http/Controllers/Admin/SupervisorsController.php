<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Supervisor\BulkDestroySupervisor;
use App\Http\Requests\Admin\Supervisor\DestroySupervisor;
use App\Http\Requests\Admin\Supervisor\IndexSupervisor;
use App\Http\Requests\Admin\Supervisor\StoreSupervisor;
use App\Http\Requests\Admin\Supervisor\UpdateSupervisor;
use App\Models\Supervisor;
use Brackets\AdminListing\Facades\AdminListing;
use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;

class SupervisorsController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param IndexSupervisor $request
     * @return array|Factory|View
     */
    public function index(IndexSupervisor $request)
    {
        // create and AdminListing instance for a specific model and
        $data = AdminListing::create(Supervisor::class)->processRequestAndGet(
            // pass the request with params
            $request,

            // set columns to query
            ['id', 'name'],

            // set columns to searchIn
            ['id', 'name']
        );

        if ($request->ajax()) {
            if ($request->has('bulk')) {
                return [
                    'bulkItems' => $data->pluck('id')
                ];
            }
            return ['data' => $data];
        }

        return view('admin.supervisor.index', ['data' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @throws AuthorizationException
     * @return Factory|View
     */
    public function create()
    {
        $this->authorize('admin.supervisor.create');

        return view('admin.supervisor.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreSupervisor $request
     * @return array|RedirectResponse|Redirector
     */
    public function store(StoreSupervisor $request)
    {
        // Sanitize input
        $sanitized = $request->getSanitized();

        // Store the Supervisor
        $supervisor = Supervisor::create($sanitized);

        if ($request->ajax()) {
            return ['redirect' => url('admin/supervisors'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
        }

        return redirect('admin/supervisors');
    }

    /**
     * Display the specified resource.
     *
     * @param Supervisor $supervisor
     * @throws AuthorizationException
     * @return void
     */
    public function show(Supervisor $supervisor)
    {
        $this->authorize('admin.supervisor.show', $supervisor);

        // TODO your code goes here
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Supervisor $supervisor
     * @throws AuthorizationException
     * @return Factory|View
     */
    public function edit(Supervisor $supervisor)
    {
        $this->authorize('admin.supervisor.edit', $supervisor);


        return view('admin.supervisor.edit', [
            'supervisor' => $supervisor,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateSupervisor $request
     * @param Supervisor $supervisor
     * @return array|RedirectResponse|Redirector
     */
    public function update(UpdateSupervisor $request, Supervisor $supervisor)
    {
        // Sanitize input
        $sanitized = $request->getSanitized();

        // Update changed values Supervisor
        $supervisor->update($sanitized);

        if ($request->ajax()) {
            return [
                'redirect' => url('admin/supervisors'),
                'message' => trans('brackets/admin-ui::admin.operation.succeeded'),
            ];
        }

        return redirect('admin/supervisors');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param DestroySupervisor $request
     * @param Supervisor $supervisor
     * @throws Exception
     * @return ResponseFactory|RedirectResponse|Response
     */
    public function destroy(DestroySupervisor $request, Supervisor $supervisor)
    {
        $supervisor->delete();

        if ($request->ajax()) {
            return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
        }

        return redirect()->back();
    }

    /**
     * Remove the specified resources from storage.
     *
     * @param BulkDestroySupervisor $request
     * @throws Exception
     * @return Response|bool
     */
    public function bulkDestroy(BulkDestroySupervisor $request) : Response
    {
        DB::transaction(static function () use ($request) {
            collect($request->data['ids'])
                ->chunk(1000)
                ->each(static function ($bulkChunk) {
                    Supervisor::whereIn('id', $bulkChunk)->delete();

                    // TODO your code goes here
                });
        });

        return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
    }
}
