<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\ApplicantOwner\BulkDestroyApplicantOwner;
use App\Http\Requests\Admin\ApplicantOwner\DestroyApplicantOwner;
use App\Http\Requests\Admin\ApplicantOwner\IndexApplicantOwner;
use App\Http\Requests\Admin\ApplicantOwner\StoreApplicantOwner;
use App\Http\Requests\Admin\ApplicantOwner\UpdateApplicantOwner;
use App\Models\ApplicantOwner;
use Brackets\AdminListing\Facades\AdminListing;
use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;

class ApplicantOwnersController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param IndexApplicantOwner $request
     * @return array|Factory|View
     */
    public function index(IndexApplicantOwner $request)
    {
        // create and AdminListing instance for a specific model and
        $data = AdminListing::create(ApplicantOwner::class)->processRequestAndGet(
            // pass the request with params
            $request,

            // set columns to query
            ['id', 'applicant_id', 'admin_user_id'],

            // set columns to searchIn
            ['id']
        );

        if ($request->ajax()) {
            if ($request->has('bulk')) {
                return [
                    'bulkItems' => $data->pluck('id')
                ];
            }
            return ['data' => $data];
        }

        return view('admin.applicant-owner.index', ['data' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @throws AuthorizationException
     * @return Factory|View
     */
    public function create()
    {
        $this->authorize('admin.applicant-owner.create');

        return view('admin.applicant-owner.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreApplicantOwner $request
     * @return array|RedirectResponse|Redirector
     */
    public function store(StoreApplicantOwner $request)
    {
        //return $request;
        // Sanitize input
        $sanitized = $request->getSanitized();
        //getAdminUserId()
        //$sanitized['admin_user_id'] = $request->getAdminUserId();

        // Store the ApplicantOwner
        $applicantOwner = ApplicantOwner::create($sanitized);

        if ($request->ajax()) {
            return ['redirect' => url('admin/applicants/'.$request->applicant_id.'/edit'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
        }

        return redirect('admin/applicants/'.$request->applicant_id.'/edit');
    }

    /**
     * Display the specified resource.
     *
     * @param ApplicantOwner $applicantOwner
     * @throws AuthorizationException
     * @return void
     */
    public function show(ApplicantOwner $applicantOwner)
    {
        $this->authorize('admin.applicant-owner.show', $applicantOwner);

        // TODO your code goes here
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param ApplicantOwner $applicantOwner
     * @throws AuthorizationException
     * @return Factory|View
     */
    public function edit(ApplicantOwner $applicantOwner)
    {
        $this->authorize('admin.applicant-owner.edit', $applicantOwner);


        return view('admin.applicant-owner.edit', [
            'applicantOwner' => $applicantOwner,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateApplicantOwner $request
     * @param ApplicantOwner $applicantOwner
     * @return array|RedirectResponse|Redirector
     */
    public function update(UpdateApplicantOwner $request, ApplicantOwner $applicantOwner)
    {
        // Sanitize input
        $sanitized = $request->getSanitized();

        // Update changed values ApplicantOwner
        $applicantOwner->update($sanitized);

        if ($request->ajax()) {
            return [
                'redirect' => url('admin/applicant-owners'),
                'message' => trans('brackets/admin-ui::admin.operation.succeeded'),
            ];
        }

        return redirect('admin/applicant-owners');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param DestroyApplicantOwner $request
     * @param ApplicantOwner $applicantOwner
     * @throws Exception
     * @return ResponseFactory|RedirectResponse|Response
     */
    public function destroy(DestroyApplicantOwner $request, ApplicantOwner $applicantOwner)
    {
        $applicantOwner->delete();

        if ($request->ajax()) {
            return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
        }

        return redirect()->back();
    }

    /**
     * Remove the specified resources from storage.
     *
     * @param BulkDestroyApplicantOwner $request
     * @throws Exception
     * @return Response|bool
     */
    public function bulkDestroy(BulkDestroyApplicantOwner $request) : Response
    {
        DB::transaction(static function () use ($request) {
            collect($request->data['ids'])
                ->chunk(1000)
                ->each(static function ($bulkChunk) {
                    ApplicantOwner::whereIn('id', $bulkChunk)->delete();

                    // TODO your code goes here
                });
        });

        return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
    }
}
