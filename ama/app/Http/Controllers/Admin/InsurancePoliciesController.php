<?php

namespace App\Http\Controllers\Admin;

use Exception;
use Validator;
use App\Models\Applicant;
use Illuminate\View\View;
use Illuminate\Http\Response;
use App\Exports\PolizasExport;
use App\Models\ApplicantStatus;
use App\Models\InsuranceEntity;
use App\Models\InsurancePolicy;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Http\RedirectResponse;
use Illuminate\Contracts\View\Factory;
use Brackets\AdminListing\Facades\AdminListing;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Auth\Access\AuthorizationException;
use App\Http\Requests\Admin\InsurancePolicy\IndexInsurancePolicy;
use App\Http\Requests\Admin\InsurancePolicy\StoreInsurancePolicy;
use App\Http\Requests\Admin\InsurancePolicy\UpdateInsurancePolicy;
use App\Http\Requests\Admin\InsurancePolicy\DestroyInsurancePolicy;
use App\Http\Requests\Admin\InsurancePolicy\BulkDestroyInsurancePolicy;

class InsurancePoliciesController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param IndexInsurancePolicy $request
     * @return array|Factory|View
     */
    public function index(IndexInsurancePolicy $request)
    {
        // create and AdminListing instance for a specific model and
        //return $request->search;
        $ci=$request->search;
        //return $ci->count();
        $data = AdminListing::create(InsurancePolicy::class)->processRequestAndGet(
            // pass the request with params
            $request,

            // set columns to query
            ['id', 'nro_poliza', 'concepto', 'fecha_vigencia', 'fecha_vencimiento', 'fecha_desembolso', 'importe', 'applicant_id', 'insurance_entity_id', 'construction_entity_id', 'culminado'],

            // set columns to searchIn
            [],
            function ($query)use($ci){
                if($ci){
                    $query->whereHas('applicant', function ($query)use($ci) {
                        $query->where('government_id', $ci);
                    })->get();
                }
                    
                $query->get();
            }
        );
/*         if($ci){
                    
            $data->whereHas('applicants', function($query2)use ($ci) {
                $query2->where('government_id', $ci);
                
            });
        } */
//return $data;
        if ($request->ajax()) {
            if ($request->has('bulk')) {
                return [
                    'bulkItems' => $data->pluck('id')
                ];
            }
            return ['data' => $data];
        }

        return view('admin.insurance-policy.index', ['data' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @throws AuthorizationException
     * @return Factory|View
     */
    public function export()
    {
        return Excel::download(new PolizasExport(), 'Polizas_'.date('d.m.Y H:i:s').'.xlsx');
    }

    public function create()
    {
        //$this->authorize('admin.insurance-policy.create');
        $applicant=Applicant::whereNull('applicants.parent_applicant')
        ->whereHas('statuses', function($query) {
            $query->where('status_id', 24);
           
         })
        ->OrWhereHas('statuses', function($query) {
            $query->where('status_id', 50);
           
         })
         ->get();
        $seguro=InsuranceEntity::all();
        //return $applicant->count();
        $applicantSelect=[];
        foreach ($applicant as $value) {
            $applicantSelect[]=[
                'id'=> $value->government_id,
                'names' => $value->government_id .' '. $value->names .' ' . $value->last_name 
            ];

         }
         $conceptOption= [
             ['id'=> 1,
             'text'=> 'Anticipo Financiero (100%)'],
            [
                'id' => 2,
                'text'=> 'Fiel Cumplimiento (10%)'

            ] 
            ];
        return view('admin.insurance-policy.create', ['applicant'=>$applicant, 'seguros'=>$seguro,  'conceptOption'=>json_encode($conceptOption), 'applicantSelect'=>json_encode($applicantSelect)]);
    }
    public function createOrdenTra($applicnat_id=0)
    {
        $applican=Applicant::where('id',$applicnat_id)->get();
        $InsuranceEntity = InsuranceEntity::all();
        //return $applican;
        return view('admin.insurance-policy.createOrdenTra', ['applicant'=>$applican,  'InsuranceEntity'=>$InsuranceEntity]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreInsurancePolicy $request
     * @return array|RedirectResponse|Redirector
     */
    public function store(StoreInsurancePolicy $request)
    {   
       //return $request;
        $importe= str_replace( '.', '', $request->importe);
        //return $request->applicant_id;
        $applicants=InsurancePolicy::where('nro_poliza','=', $request->nro_poliza)->where('applicant_id','=', $request->applicant_id)->get();
        if($applicants->count() > 0){
            if ($request->ajax()) {
                return ['notify' => ['type' => 'Error', 'title' => 'Atención', 'message' => 'Nro de Poliza ya se encuentra registrado']];
                //exit;
            }
        }
        $applicants=InsurancePolicy::where('applicant_id','=', $request->applicant_id)->get();
        if($applicants->count() > 2){
            if ($request->ajax()) {
                return ['notify' => ['type' => 'Error', 'title' => 'Atención', 'message' => 'El solicitante ya cuenta con mas de 2 Polizas. Favor Verificar!!!']];
                //exit;
            }
        }
        $applicants=InsurancePolicy::where('applicant_id','=', $request->applicant_id)->where('importe','=', $importe)->get();
        if($applicants->count() > 0){
            if ($request->ajax()) {
                return ['notify' => ['type' => 'Error', 'title' => 'Atención', 'message' => 'El importe ya fue cargado. Favor Verificar!!!']];
                //exit;
            }
        }
        // Sanitize input
        $sanitized = $request->getSanitized();
        $sanitized['importe'] = $importe;
        $sanitized['fecha_desembolso'] = NULL;
       // $sanitized['concepto'] = $request->getConceptoId();
        $insurancePolicy = InsurancePolicy::create($sanitized);

       
        if ($insurancePolicy) {
            ApplicantStatus::create([
                'applicant_id' => $insurancePolicy->applicant->id,
                'user_id' => Auth::user()->id,
                'status_id' => $insurancePolicy->applicant->statuses->status_id,
                'description' => 'Se ha creado nueva Poliza Nro: '. $request->nro_poliza
    
            ]);
        }

        if ($request->ajax()) {
            return ['redirect' => url('admin/insurance-policies/'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
        }

        return redirect('admin/insurance-policies/');
    }

    /**
     * Display the specified resource.
     *
     * @param InsurancePolicy $insurancePolicy
     * @throws AuthorizationException
     * @return void
     */
    public function show(InsurancePolicy $insurancePolicy)
    {
        $this->authorize('admin.insurance-policy.show', $insurancePolicy);

        // TODO your code goes here
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param InsurancePolicy $insurancePolicy
     * @throws AuthorizationException
     * @return Factory|View
     */
    public function edit(InsurancePolicy $insurancePolicy)
    {
        $this->authorize('admin.insurance-policy.edit', $insurancePolicy);


        return view('admin.insurance-policy.edit', [
            'insurancePolicy' => $insurancePolicy,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateInsurancePolicy $request
     * @param InsurancePolicy $insurancePolicy
     * @return array|RedirectResponse|Redirector
     */
    public function update(UpdateInsurancePolicy $request, InsurancePolicy $insurancePolicy)
    {
    
        // Sanitize input
        $sanitized = $request->getSanitized();
        if($request->culminado=='true'){
            $sanitized['culminado']='true';
        }else{
            $sanitized['culminado']='false';

        }
        //return $insurancePolicy;
        // Update changed values InsurancePolicy
        $insurancePolicy->update($sanitized);

        if ($insurancePolicy) {
            ApplicantStatus::create([
                'applicant_id' => $insurancePolicy->applicant->id,
                'user_id' => Auth::user()->id,
                'status_id' => $insurancePolicy->applicant->statuses->status_id,
                'description' => 'Se ha modificado fechas de vigencia y vencimiento de Poliza Nro: '. $request->nro_poliza
    
            ]);
        }
     
       //return  $insurancePolicy ;
        
        if ($request->ajax()) {
            return [
                'redirect' => url('admin/insurance-policies'),
                'message' => trans('brackets/admin-ui::admin.operation.succeeded'),
            ];
        }

        return redirect('admin/insurance-policies');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param DestroyInsurancePolicy $request
     * @param InsurancePolicy $insurancePolicy
     * @throws Exception
     * @return ResponseFactory|RedirectResponse|Response
     */
    public function destroy(DestroyInsurancePolicy $request, InsurancePolicy $insurancePolicy)
    {
        $insurancePolicy->delete();

        if ($request->ajax()) {
            return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
        }

        return redirect()->back();
    }

    /**
     * Remove the specified resources from storage.
     *
     * @param BulkDestroyInsurancePolicy $request
     * @throws Exception
     * @return Response|bool
     */
    public function bulkDestroy(BulkDestroyInsurancePolicy $request) : Response
    {
        DB::transaction(static function () use ($request) {
            collect($request->data['ids'])
                ->chunk(1000)
                ->each(static function ($bulkChunk) {
                    InsurancePolicy::whereIn('id', $bulkChunk)->delete();

                    // TODO your code goes here
                });
        });

        return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
    }
    public function findApplicant($government_id)
    {
        $data=Applicant::whereNull('applicants.parent_applicant')
        ->where('government_id', $government_id)
        ->get();
        //return $data->count();
        //return $data;

        if($data->count()>0){  //si hay datos retorna
            
            return [ 
            'data'=>$data,
            'cod'=>1
        ];
    }else{
        return response()->json([
            'cod' => 0,
            'data' => "",
            
        ]);

        }
        }
}
