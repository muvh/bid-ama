<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\TypeCheck\BulkDestroyTypeCheck;
use App\Http\Requests\Admin\TypeCheck\DestroyTypeCheck;
use App\Http\Requests\Admin\TypeCheck\IndexTypeCheck;
use App\Http\Requests\Admin\TypeCheck\StoreTypeCheck;
use App\Http\Requests\Admin\TypeCheck\UpdateTypeCheck;
use App\Models\TypeCheck;
use Brackets\AdminListing\Facades\AdminListing;
use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;

class TypeChecksController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param IndexTypeCheck $request
     * @return array|Factory|View
     */
    public function index(IndexTypeCheck $request)
    {
        // create and AdminListing instance for a specific model and
        $data = AdminListing::create(TypeCheck::class)->processRequestAndGet(
            // pass the request with params
            $request,

            // set columns to query
            ['id', 'name'],

            // set columns to searchIn
            ['id', 'name']
        );

        if ($request->ajax()) {
            if ($request->has('bulk')) {
                return [
                    'bulkItems' => $data->pluck('id')
                ];
            }
            return ['data' => $data];
        }

        return view('admin.type-check.index', ['data' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @throws AuthorizationException
     * @return Factory|View
     */
    public function create()
    {
        $this->authorize('admin.type-check.create');

        return view('admin.type-check.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreTypeCheck $request
     * @return array|RedirectResponse|Redirector
     */
    public function store(StoreTypeCheck $request)
    {
        // Sanitize input
        $sanitized = $request->getSanitized();

        // Store the TypeCheck
        $typeCheck = TypeCheck::create($sanitized);

        if ($request->ajax()) {
            return ['redirect' => url('admin/type-checks'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
        }

        return redirect('admin/type-checks');
    }

    /**
     * Display the specified resource.
     *
     * @param TypeCheck $typeCheck
     * @throws AuthorizationException
     * @return void
     */
    public function show(TypeCheck $typeCheck)
    {
        $this->authorize('admin.type-check.show', $typeCheck);

        // TODO your code goes here
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param TypeCheck $typeCheck
     * @throws AuthorizationException
     * @return Factory|View
     */
    public function edit(TypeCheck $typeCheck)
    {
        $this->authorize('admin.type-check.edit', $typeCheck);


        return view('admin.type-check.edit', [
            'typeCheck' => $typeCheck,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateTypeCheck $request
     * @param TypeCheck $typeCheck
     * @return array|RedirectResponse|Redirector
     */
    public function update(UpdateTypeCheck $request, TypeCheck $typeCheck)
    {
        // Sanitize input
        $sanitized = $request->getSanitized();

        // Update changed values TypeCheck
        $typeCheck->update($sanitized);

        if ($request->ajax()) {
            return [
                'redirect' => url('admin/type-checks'),
                'message' => trans('brackets/admin-ui::admin.operation.succeeded'),
            ];
        }

        return redirect('admin/type-checks');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param DestroyTypeCheck $request
     * @param TypeCheck $typeCheck
     * @throws Exception
     * @return ResponseFactory|RedirectResponse|Response
     */
    public function destroy(DestroyTypeCheck $request, TypeCheck $typeCheck)
    {
        $typeCheck->delete();

        if ($request->ajax()) {
            return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
        }

        return redirect()->back();
    }

    /**
     * Remove the specified resources from storage.
     *
     * @param BulkDestroyTypeCheck $request
     * @throws Exception
     * @return Response|bool
     */
    public function bulkDestroy(BulkDestroyTypeCheck $request) : Response
    {
        DB::transaction(static function () use ($request) {
            collect($request->data['ids'])
                ->chunk(1000)
                ->each(static function ($bulkChunk) {
                    TypeCheck::whereIn('id', $bulkChunk)->delete();

                    // TODO your code goes here
                });
        });

        return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
    }
}
