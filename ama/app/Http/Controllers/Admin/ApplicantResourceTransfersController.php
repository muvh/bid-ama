<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\ApplicantResourceTransfer\BulkDestroyApplicantResourceTransfer;
use App\Http\Requests\Admin\ApplicantResourceTransfer\DestroyApplicantResourceTransfer;
use App\Http\Requests\Admin\ApplicantResourceTransfer\IndexApplicantResourceTransfer;
use App\Http\Requests\Admin\ApplicantResourceTransfer\StoreApplicantResourceTransfer;
use App\Http\Requests\Admin\ApplicantResourceTransfer\UpdateApplicantResourceTransfer;
use App\Models\ApplicantResourceTransfer;
use Brackets\AdminListing\Facades\AdminListing;
use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;

class ApplicantResourceTransfersController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param IndexApplicantResourceTransfer $request
     * @return array|Factory|View
     */
    public function index(IndexApplicantResourceTransfer $request)
    {
        // create and AdminListing instance for a specific model and
        $data = AdminListing::create(ApplicantResourceTransfer::class)->processRequestAndGet(
            // pass the request with params
            $request,

            // set columns to query
            ['id', 'applicant_id', 'resource_transfer_id'],

            // set columns to searchIn
            ['id']
        );

        if ($request->ajax()) {
            if ($request->has('bulk')) {
                return [
                    'bulkItems' => $data->pluck('id')
                ];
            }
            return ['data' => $data];
        }

        return view('admin.applicant-resource-transfer.index', ['data' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @throws AuthorizationException
     * @return Factory|View
     */
    public function create()
    {
        $this->authorize('admin.applicant-resource-transfer.create');

        return view('admin.applicant-resource-transfer.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreApplicantResourceTransfer $request
     * @return array|RedirectResponse|Redirector
     */
    public function store(StoreApplicantResourceTransfer $request)
    {
        // Sanitize input
        $sanitized = $request->getSanitized();

        // Store the ApplicantResourceTransfer
        $applicantResourceTransfer = ApplicantResourceTransfer::create($sanitized);

        if ($request->ajax()) {
            return ['redirect' => url('admin/applicant-resource-transfers'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
        }

        return redirect('admin/applicant-resource-transfers');
    }

    /**
     * Display the specified resource.
     *
     * @param ApplicantResourceTransfer $applicantResourceTransfer
     * @throws AuthorizationException
     * @return void
     */
    public function show(ApplicantResourceTransfer $applicantResourceTransfer)
    {
        $this->authorize('admin.applicant-resource-transfer.show', $applicantResourceTransfer);

        // TODO your code goes here
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param ApplicantResourceTransfer $applicantResourceTransfer
     * @throws AuthorizationException
     * @return Factory|View
     */
    public function edit(ApplicantResourceTransfer $applicantResourceTransfer)
    {
        $this->authorize('admin.applicant-resource-transfer.edit', $applicantResourceTransfer);


        return view('admin.applicant-resource-transfer.edit', [
            'applicantResourceTransfer' => $applicantResourceTransfer,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateApplicantResourceTransfer $request
     * @param ApplicantResourceTransfer $applicantResourceTransfer
     * @return array|RedirectResponse|Redirector
     */
    public function update(UpdateApplicantResourceTransfer $request, ApplicantResourceTransfer $applicantResourceTransfer)
    {
        // Sanitize input
        $sanitized = $request->getSanitized();

        // Update changed values ApplicantResourceTransfer
        $applicantResourceTransfer->update($sanitized);

        if ($request->ajax()) {
            return [
                'redirect' => url('admin/applicant-resource-transfers'),
                'message' => trans('brackets/admin-ui::admin.operation.succeeded'),
            ];
        }

        return redirect('admin/applicant-resource-transfers');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param DestroyApplicantResourceTransfer $request
     * @param ApplicantResourceTransfer $applicantResourceTransfer
     * @throws Exception
     * @return ResponseFactory|RedirectResponse|Response
     */
    public function destroy(DestroyApplicantResourceTransfer $request, ApplicantResourceTransfer $applicantResourceTransfer)
    {
        $applicantResourceTransfer->delete();

        if ($request->ajax()) {
            return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
        }

        return redirect()->back();
    }

    /**
     * Remove the specified resources from storage.
     *
     * @param BulkDestroyApplicantResourceTransfer $request
     * @throws Exception
     * @return Response|bool
     */
    public function bulkDestroy(BulkDestroyApplicantResourceTransfer $request) : Response
    {
        DB::transaction(static function () use ($request) {
            collect($request->data['ids'])
                ->chunk(1000)
                ->each(static function ($bulkChunk) {
                    ApplicantResourceTransfer::whereIn('id', $bulkChunk)->delete();

                    // TODO your code goes here
                });
        });

        return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
    }
}
