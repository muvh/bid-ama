<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\DocumentType\BulkDestroyDocumentType;
use App\Http\Requests\Admin\DocumentType\DestroyDocumentType;
use App\Http\Requests\Admin\DocumentType\IndexDocumentType;
use App\Http\Requests\Admin\DocumentType\StoreDocumentType;
use App\Http\Requests\Admin\DocumentType\UpdateDocumentType;
use App\Models\DocumentType;
use Brackets\AdminListing\Facades\AdminListing;
use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;

class DocumentTypesController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param IndexDocumentType $request
     * @return array|Factory|View
     */
    public function index(IndexDocumentType $request)
    {
        // create and AdminListing instance for a specific model and
        $data = AdminListing::create(DocumentType::class)->processRequestAndGet(
            // pass the request with params
            $request,

            // set columns to query
            ['id', 'name', 'enabled', 'type'],

            // set columns to searchIn
            ['id', 'name', 'type']
        );

        if ($request->ajax()) {
            if ($request->has('bulk')) {
                return [
                    'bulkItems' => $data->pluck('id')
                ];
            }
            return ['data' => $data];
        }

        return view('admin.document-type.index', ['data' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @throws AuthorizationException
     * @return Factory|View
     */
    public function create()
    {
        $this->authorize('admin.document-type.create');

        return view('admin.document-type.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreDocumentType $request
     * @return array|RedirectResponse|Redirector
     */
    public function store(StoreDocumentType $request)
    {
        // Sanitize input
        $sanitized = $request->getSanitized();

        // Store the DocumentType
        $documentType = DocumentType::create($sanitized);

        if ($request->ajax()) {
            return ['redirect' => url('admin/document-types'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
        }

        return redirect('admin/document-types');
    }

    /**
     * Display the specified resource.
     *
     * @param DocumentType $documentType
     * @throws AuthorizationException
     * @return void
     */
    public function show(DocumentType $documentType)
    {
        $this->authorize('admin.document-type.show', $documentType);

        // TODO your code goes here
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param DocumentType $documentType
     * @throws AuthorizationException
     * @return Factory|View
     */
    public function edit(DocumentType $documentType)
    {
        $this->authorize('admin.document-type.edit', $documentType);


        return view('admin.document-type.edit', [
            'documentType' => $documentType,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateDocumentType $request
     * @param DocumentType $documentType
     * @return array|RedirectResponse|Redirector
     */
    public function update(UpdateDocumentType $request, DocumentType $documentType)
    {
        // Sanitize input
        $sanitized = $request->getSanitized();

        // Update changed values DocumentType
        $documentType->update($sanitized);

        if ($request->ajax()) {
            return [
                'redirect' => url('admin/document-types'),
                'message' => trans('brackets/admin-ui::admin.operation.succeeded'),
            ];
        }

        return redirect('admin/document-types');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param DestroyDocumentType $request
     * @param DocumentType $documentType
     * @throws Exception
     * @return ResponseFactory|RedirectResponse|Response
     */
    public function destroy(DestroyDocumentType $request, DocumentType $documentType)
    {
        $documentType->delete();

        if ($request->ajax()) {
            return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
        }

        return redirect()->back();
    }

    /**
     * Remove the specified resources from storage.
     *
     * @param BulkDestroyDocumentType $request
     * @throws Exception
     * @return Response|bool
     */
    public function bulkDestroy(BulkDestroyDocumentType $request) : Response
    {
        DB::transaction(static function () use ($request) {
            collect($request->data['ids'])
                ->chunk(1000)
                ->each(static function ($bulkChunk) {
                    DocumentType::whereIn('id', $bulkChunk)->delete();

                    // TODO your code goes here
                });
        });

        return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
    }
}
