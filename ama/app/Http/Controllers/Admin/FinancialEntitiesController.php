<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\FinancialEntity\BulkDestroyFinancialEntity;
use App\Http\Requests\Admin\FinancialEntity\DestroyFinancialEntity;
use App\Http\Requests\Admin\FinancialEntity\IndexFinancialEntity;
use App\Http\Requests\Admin\FinancialEntity\StoreFinancialEntity;
use App\Http\Requests\Admin\FinancialEntity\UpdateFinancialEntity;
use App\Models\FinancialEntity;
use Brackets\AdminListing\Facades\AdminListing;
use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;

class FinancialEntitiesController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param IndexFinancialEntity $request
     * @return array|Factory|View
     */
    public function index(IndexFinancialEntity $request)
    {
        // create and AdminListing instance for a specific model and
        $data = AdminListing::create(FinancialEntity::class)->processRequestAndGet(
            // pass the request with params
            $request,

            // set columns to query
            ['id', 'name', 'bank_name', 'address', 'cta_cte', 'const_ruc', 'razon_social'],

            // set columns to searchIn
            ['id', 'name', 'bank_name', 'address', 'cta_cte', 'const_ruc', 'razon_social']
        );

        if ($request->ajax()) {
            if ($request->has('bulk')) {
                return [
                    'bulkItems' => $data->pluck('id')
                ];
            }
            return ['data' => $data];
        }

        return view('admin.financial-entity.index', ['data' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @throws AuthorizationException
     * @return Factory|View
     */
    public function create()
    {
        $this->authorize('admin.financial-entity.create');

        return view('admin.financial-entity.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreFinancialEntity $request
     * @return array|RedirectResponse|Redirector
     */
    public function store(StoreFinancialEntity $request)
    {
        // Sanitize input
        $sanitized = $request->getSanitized();

        // Store the FinancialEntity
        $financialEntity = FinancialEntity::create($sanitized);

        if ($request->ajax()) {
            return ['redirect' => url('admin/financial-entities'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
        }

        return redirect('admin/financial-entities');
    }

    /**
     * Display the specified resource.
     *
     * @param FinancialEntity $financialEntity
     * @throws AuthorizationException
     * @return void
     */
    public function show(FinancialEntity $financialEntity)
    {
        $this->authorize('admin.financial-entity.show', $financialEntity);

        // TODO your code goes here
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param FinancialEntity $financialEntity
     * @throws AuthorizationException
     * @return Factory|View
     */
    public function edit(FinancialEntity $financialEntity)
    {
        $this->authorize('admin.financial-entity.edit', $financialEntity);


        return view('admin.financial-entity.edit', [
            'financialEntity' => $financialEntity,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateFinancialEntity $request
     * @param FinancialEntity $financialEntity
     * @return array|RedirectResponse|Redirector
     */
    public function update(UpdateFinancialEntity $request, FinancialEntity $financialEntity)
    {
        // Sanitize input
        $sanitized = $request->getSanitized();

        // Update changed values FinancialEntity
        $financialEntity->update($sanitized);

        if ($request->ajax()) {
            return [
                'redirect' => url('admin/financial-entities'),
                'message' => trans('brackets/admin-ui::admin.operation.succeeded'),
            ];
        }

        return redirect('admin/financial-entities');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param DestroyFinancialEntity $request
     * @param FinancialEntity $financialEntity
     * @throws Exception
     * @return ResponseFactory|RedirectResponse|Response
     */
    public function destroy(DestroyFinancialEntity $request, FinancialEntity $financialEntity)
    {
        $financialEntity->delete();

        if ($request->ajax()) {
            return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
        }

        return redirect()->back();
    }

    /**
     * Remove the specified resources from storage.
     *
     * @param BulkDestroyFinancialEntity $request
     * @throws Exception
     * @return Response|bool
     */
    public function bulkDestroy(BulkDestroyFinancialEntity $request) : Response
    {
        DB::transaction(static function () use ($request) {
            collect($request->data['ids'])
                ->chunk(1000)
                ->each(static function ($bulkChunk) {
                    FinancialEntity::whereIn('id', $bulkChunk)->delete();

                    // TODO your code goes here
                });
        });

        return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
    }
}
