<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\WorksTypeCheck\BulkDestroyWorksTypeCheck;
use App\Http\Requests\Admin\WorksTypeCheck\DestroyWorksTypeCheck;
use App\Http\Requests\Admin\WorksTypeCheck\IndexWorksTypeCheck;
use App\Http\Requests\Admin\WorksTypeCheck\StoreWorksTypeCheck;
use App\Http\Requests\Admin\WorksTypeCheck\UpdateWorksTypeCheck;
use App\Models\WorksTypeCheck;
use Brackets\AdminListing\Facades\AdminListing;
use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;

class WorksTypeChecksController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param IndexWorksTypeCheck $request
     * @return array|Factory|View
     */
    public function index(IndexWorksTypeCheck $request)
    {
        // create and AdminListing instance for a specific model and
        $data = AdminListing::create(WorksTypeCheck::class)->processRequestAndGet(
            // pass the request with params
            $request,

            // set columns to query
            ['id', 'work_id', 'type_check_id'],

            // set columns to searchIn
            ['id']
        );

        if ($request->ajax()) {
            if ($request->has('bulk')) {
                return [
                    'bulkItems' => $data->pluck('id')
                ];
            }
            return ['data' => $data];
        }

        return view('admin.works-type-check.index', ['data' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @throws AuthorizationException
     * @return Factory|View
     */
    public function create()
    {
        $this->authorize('admin.works-type-check.create');

        return view('admin.works-type-check.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreWorksTypeCheck $request
     * @return array|RedirectResponse|Redirector
     */
    public function store(StoreWorksTypeCheck $request)
    {
        // Sanitize input
        $sanitized = $request->getSanitized();

        // Store the WorksTypeCheck
        $worksTypeCheck = WorksTypeCheck::create($sanitized);

        if ($request->ajax()) {
            return ['redirect' => url('admin/works-type-checks'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
        }

        return redirect('admin/works-type-checks');
    }

    /**
     * Display the specified resource.
     *
     * @param WorksTypeCheck $worksTypeCheck
     * @throws AuthorizationException
     * @return void
     */
    public function show(WorksTypeCheck $worksTypeCheck)
    {
        $this->authorize('admin.works-type-check.show', $worksTypeCheck);

        // TODO your code goes here
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param WorksTypeCheck $worksTypeCheck
     * @throws AuthorizationException
     * @return Factory|View
     */
    public function edit(WorksTypeCheck $worksTypeCheck)
    {
        $this->authorize('admin.works-type-check.edit', $worksTypeCheck);


        return view('admin.works-type-check.edit', [
            'worksTypeCheck' => $worksTypeCheck,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateWorksTypeCheck $request
     * @param WorksTypeCheck $worksTypeCheck
     * @return array|RedirectResponse|Redirector
     */
    public function update(UpdateWorksTypeCheck $request, WorksTypeCheck $worksTypeCheck)
    {
        // Sanitize input
        $sanitized = $request->getSanitized();

        // Update changed values WorksTypeCheck
        $worksTypeCheck->update($sanitized);

        if ($request->ajax()) {
            return [
                'redirect' => url('admin/works-type-checks'),
                'message' => trans('brackets/admin-ui::admin.operation.succeeded'),
            ];
        }

        return redirect('admin/works-type-checks');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param DestroyWorksTypeCheck $request
     * @param WorksTypeCheck $worksTypeCheck
     * @throws Exception
     * @return ResponseFactory|RedirectResponse|Response
     */
    public function destroy(DestroyWorksTypeCheck $request, WorksTypeCheck $worksTypeCheck)
    {
        $worksTypeCheck->delete();

        if ($request->ajax()) {
            return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
        }

        return redirect()->back();
    }

    /**
     * Remove the specified resources from storage.
     *
     * @param BulkDestroyWorksTypeCheck $request
     * @throws Exception
     * @return Response|bool
     */
    public function bulkDestroy(BulkDestroyWorksTypeCheck $request) : Response
    {
        DB::transaction(static function () use ($request) {
            collect($request->data['ids'])
                ->chunk(1000)
                ->each(static function ($bulkChunk) {
                    WorksTypeCheck::whereIn('id', $bulkChunk)->delete();

                    // TODO your code goes here
                });
        });

        return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
    }
}
