<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Certificate\BulkDestroyCertificate;
use App\Http\Requests\Admin\Certificate\DestroyCertificate;
use App\Http\Requests\Admin\Certificate\IndexCertificate;
use App\Http\Requests\Admin\Certificate\StoreCertificate;
use App\Http\Requests\Admin\Certificate\UpdateCertificate;
use App\Models\Certificate;
use App\Models\Applicant;
use Illuminate\Http\Request;
use Brackets\AdminListing\Facades\AdminListing;
use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;

class CertificatesController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param IndexCertificate $request
     * @return array|Factory|View
     */
    public function index(IndexCertificate $request)
    {
        $applicantId =[];
        $applicant = collect([]);
        if($request->search){
            $applicant = Applicant::
            //where('government_id','=',trim($request->search))
            where('names','like', '%' .strtoupper($request->search).'%')
            ->orWhere('last_names','like', '%' .strtoupper($request->search.'%'))
            ->orwhere('government_id','like', '%' .trim($request->search).'%')
            ->get();
            //->toArray();
            //return $applicant;
            foreach ($applicant as $value) {
                $applicantId[]=$value->id;
            }
        }
        //return $applicant;
        //return $applicantId;

        $type = '1';
        // create and AdminListing instance for a specific model and
        $data = AdminListing::create(Certificate::class)
        
        ->processRequestAndGet(
            // pass the request with params
            $request,

            // set columns to query
            ['id', 'fecha_tr','applicant_id','type'],

            // set columns to searchIn
            [],
            function ($query) use ($type, $applicantId, $request) {
                
                $query->where('certificates.type', '=', $type);
                    if($request->search){
                        $query->whereIn('applicant_id', $applicantId);
                    }
            }
        );

        if ($request->ajax()) {
            if ($request->has('bulk')) {
                return [
                    'bulkItems' => $data->pluck('id')
                ];
            }
            return ['data' => $data];
        }
        //return $data;
        return view('admin.certificate.index', ['data' => $data]);
    }

    public function indexCertificate(IndexCertificate $request)
    {
        $applicantId =[];
        $applicant = collect([]);
        if($request->search){
            $applicant = Applicant::
            //where('government_id','=',trim($request->search))
            where('names','like', '%' .strtoupper($request->search).'%')
            ->orWhere('last_names','like', '%' .strtoupper($request->search.'%'))
            ->orwhere('government_id','=',trim($request->search))
            ->get();
            //->toArray();
            //return $applicant;
            foreach ($applicant as $value) {
                $applicantId[]=$value->id;
            }
        }
        $type = '2';
        // create and AdminListing instance for a specific model and
        $data = AdminListing::create(Certificate::class)->processRequestAndGet(
            // pass the request with params
            $request,

            // set columns to query
            ['id', 'fecha_tr','applicant_id','type'],

            // set columns to searchIn
            [],
            function ($query) use ($type, $request, $applicantId) {
                $query
                    ->where('certificates.type', '=', $type);
                    if($request->search){
                        $query->whereIn('applicant_id', $applicantId);
                    }
            }
        );

        if ($request->ajax()) {
            if ($request->has('bulk')) {
                return [
                    'bulkItems' => $data->pluck('id')
                ];
            }
            return ['data' => $data];
        }

        //return $data;

        return view('admin.certificate.indexcertificate', ['data' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @throws AuthorizationException
     * @return Factory|View
     */
    public function create()
    {
        $this->authorize('admin.certificate.create');

        $estados=Applicant::whereNull('applicants.parent_applicant')
    
        ->get();  
        $idApplicant=[];
    foreach ($estados as $value) {
    
        if($value->statuses->status_id==21){
            $idApplicant[]= $value->statuses->applicant_id;
        }
        if($value->statuses->status_id==22){
            $idApplicant[]= $value->statuses->applicant_id;
        }
    }
        $applicants = Applicant::where('parent_applicant', null)
                                //->where('government_id', '>' , 0)
                                ->whereIn('applicants.id', $idApplicant)
                                ->select('id','names', 'last_names', 'government_id')
                                ->orderBy('government_id')
                                ->get();

                                //return dd($applicants);
/*          $applicant=Applicant::whereNull('applicants.parent_applicant')
                                ->whereHas('statuses', function($query) {
                                    $query->where('status_id', 24);
                                   
                                 })
                                ->OrWhereHas('statuses', function($query) {
                                    $query->where('status_id', 39);
                                   
                                 })
                                 ->get(); */
        //return ($applicant);

        return view('admin.certificate.create', compact('applicants'));
    }


    public function createFinalObra()
    {
        $this->authorize('admin.certificate.create');


        $applicants = Applicant::where('parent_applicant', null)
        ->where('government_id', '>' , 0)
        ->wherehas('statuses', function($query){
            $query->where('status_id', 26);
        })
        ->select('id','names', 'last_names', 'government_id')
        ->orderBy('government_id')
        ->get();

        //return ($applicant);

        return view('admin.certificate.createfinalobra', compact('applicants'));
    }


    public function searchApplicants(Request $request){
        //return $request['query'];

        $applicants = Applicant::where('parent_applicant', null)
                      ->select('id','names', 'last_names', 'government_id')
                      ->paginate(50)
                      ->pluck('id', 'names');
                                //->where('government_id', '>' , 0)
                                //->wherehas('')
                                //->select('id','names', 'last_names', 'government_id')
                                //->orderBy('government_id')
                                //->get();
        return $applicants->toJson();
        return [
            'message' => 'success'
        ];
    }

    public function showpdf(Certificate $certificate){
        //require 'vendor/autoload.php';
        /*$applicant=ApplicantResourceTransfer::where('resource_transfer_id',$resourceTransfer->id)->get();
        $resource=ResourceTransfer::where('id',$resourceTransfer->id)->get();

        $mount=0;
        if($resourceTransfer->type_order_id==2){
            $mount= $applicant->count()*7000000;
        }else{
            $mount= $applicant->count()*28000000;

        }
        //return $resourceTransfer;
        $number=$mount;
        $decimals=2;
        $formatter = new NumeroALetras();
        $numeroTexto=  $formatter->toWords($number, $decimals);
       // return  NumeroALetras::convertir(4599.99, 'soles') .'<br>';
         $resourceTransfer->culminado=true;
        $resourceTransfer->save();*/
       //return $resourceTransfer;

       $fecha = $certificate['fecha_tr'];
       $applicant = $certificate['applicant'];

       //return $applicant;
       setlocale(LC_ALL,'es_ES.UTF-8');
       setlocale(LC_TIME,'es_ES');
       \Carbon\Carbon::setLocale('es_ES');
        $pdf = \PDF::LoadView('admin.certificate.showpdf', [
            'resourceTransfer' => $certificate,
            //'numeroTexto'=>$numeroTexto,
            'applicant'=> $applicant,
            'fecha'=> $fecha
        ]);
           //$pdf->loadHTML('<h1>Test</h1>');
           return $pdf->stream();

        //return view('admin.resource-transfer.showfideicomiso', compact('resourceTransfer','applicant','mount', 'numeroTexto'));
    }

    public function finalobrapdf(Certificate $certificate){

       $fecha = $certificate['fecha_tr'];
       $applicant = $certificate['applicant'];

       //return $applicant;
       setlocale(LC_ALL,'es_ES.UTF-8');
       setlocale(LC_TIME,'es_ES');
       \Carbon\Carbon::setLocale('es_ES');
        $pdf = \PDF::LoadView('admin.certificate.finalobrapdf', [
            'resourceTransfer' => $certificate,
            //'numeroTexto'=>$numeroTexto,
            'applicant'=> $applicant,
            'fecha'=> $fecha
        ]);
           //$pdf->loadHTML('<h1>Test</h1>');
           return $pdf->stream();

        //return view('admin.resource-transfer.showfideicomiso', compact('resourceTransfer','applicant','mount', 'numeroTexto'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreCertificate $request
     * @return array|RedirectResponse|Redirector
     */
    public function store(StoreCertificate $request)
    {
        // Sanitize input
        $applicants=Certificate::where('applicant_id','=', $request->getApplicantId())->where('type',1)->get();
        if($applicants->count() > 0){
            if ($request->ajax()) {
                return ['notify' => ['type' => 'success', 'title' => 'Atención', 'message' => 'El soliciantes ya tiene Certificado. Verificar!!']];
                //exit;
            }
        }
        //return $request;

        $sanitized = $request->getSanitized();
        $sanitized ['applicant_id'] =  $request->getApplicantId();
        $sanitized ['type'] = '1';

        //return $sanitized;
        // Store the Certificate
        $certificate = Certificate::create($sanitized);

        if ($request->ajax()) {
            return ['redirect' => url('admin/certificates'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
        }

        return redirect('admin/certificates');
    }

    public function storeFinalObra(StoreCertificate $request)
    {
        // Sanitize input
        $applicants=Certificate::where('applicant_id','=', $request->getApplicantId())->where('type',2)->get();
        if($applicants->count() > 0){
            if ($request->ajax()) {
                return ['notify' => ['type' => 'success', 'title' => 'Atención', 'message' => 'El soliciantes ya tiene Certificado. Verificar!!']];
                //exit;
            }
        }
        //return $request;
        //return $request;

        $sanitized = $request->getSanitized();
        $sanitized ['applicant_id'] =  $request->getApplicantId();
        $sanitized ['type'] = '2';

        //return $sanitized;
        // Store the Certificate
        $certificate = Certificate::create($sanitized);

        if ($request->ajax()) {
            return ['redirect' => url('admin/certificates/certificate'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
        }

        return redirect('admin/certificates');
    }

    /**
     * Display the specified resource.
     *
     * @param Certificate $certificate
     * @throws AuthorizationException
     * @return void
     */
    public function show(Certificate $certificate)
    {
        $this->authorize('admin.certificate.show', $certificate);

        // TODO your code goes here
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Certificate $certificate
     * @throws AuthorizationException
     * @return Factory|View
     */
    public function edit(Certificate $certificate)
    {
        $this->authorize('admin.certificate.edit', $certificate);


        return view('admin.certificate.edit', [
            'certificate' => $certificate,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateCertificate $request
     * @param Certificate $certificate
     * @return array|RedirectResponse|Redirector
     */
    public function update(UpdateCertificate $request, Certificate $certificate)
    {
        // Sanitize input
        $sanitized = $request->getSanitized();

        // Update changed values Certificate
        $certificate->update($sanitized);

        if ($request->ajax()) {
            return [
                'redirect' => url('admin/certificates'),
                'message' => trans('brackets/admin-ui::admin.operation.succeeded'),
            ];
        }

        return redirect('admin/certificates');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param DestroyCertificate $request
     * @param Certificate $certificate
     * @throws Exception
     * @return ResponseFactory|RedirectResponse|Response
     */
    public function destroy(DestroyCertificate $request, Certificate $certificate)
    {
        $certificate->delete();

        if ($request->ajax()) {
            return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
        }

        return redirect()->back();
    }

    /**
     * Remove the specified resources from storage.
     *
     * @param BulkDestroyCertificate $request
     * @throws Exception
     * @return Response|bool
     */
    public function bulkDestroy(BulkDestroyCertificate $request) : Response
    {
        DB::transaction(static function () use ($request) {
            collect($request->data['ids'])
                ->chunk(1000)
                ->each(static function ($bulkChunk) {
                    Certificate::whereIn('id', $bulkChunk)->delete();

                    // TODO your code goes here
                });
        });

        return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
    }
}
