<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\FinancialSummary\BulkDestroyFinancialSummary;
use App\Http\Requests\Admin\FinancialSummary\DestroyFinancialSummary;
use App\Http\Requests\Admin\FinancialSummary\IndexFinancialSummary;
use App\Http\Requests\Admin\FinancialSummary\StoreFinancialSummary;
use App\Http\Requests\Admin\FinancialSummary\UpdateFinancialSummary;
use App\Models\FinancialSummary;
use Brackets\AdminListing\Facades\AdminListing;
use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;

class FinancialSummariesController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param IndexFinancialSummary $request
     * @return array|Factory|View
     */
    public function index(IndexFinancialSummary $request)
    {
        // create and AdminListing instance for a specific model and
        $data = AdminListing::create(FinancialSummary::class)->processRequestAndGet(
            // pass the request with params
            $request,

            // set columns to query
            ['id', 'applicant_id', 'desembolso', 'date'],

            // set columns to searchIn
            ['id', 'desembolso']
        );

        if ($request->ajax()) {
            if ($request->has('bulk')) {
                return [
                    'bulkItems' => $data->pluck('id')
                ];
            }
            return ['data' => $data];
        }

        return view('admin.financial-summary.index', ['data' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @throws AuthorizationException
     * @return Factory|View
     */
    public function create()
    {
        $this->authorize('admin.financial-summary.create');

        return view('admin.financial-summary.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreFinancialSummary $request
     * @return array|RedirectResponse|Redirector
     */
    public function store(StoreFinancialSummary $request)
    {
        // Sanitize input
        $sanitized = $request->getSanitized();

        // Store the FinancialSummary
        $financialSummary = FinancialSummary::create($sanitized);

        if ($request->ajax()) {
            return ['redirect' => url('admin/financial-summaries'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
        }

        return redirect('admin/financial-summaries');
    }

    /**
     * Display the specified resource.
     *
     * @param FinancialSummary $financialSummary
     * @throws AuthorizationException
     * @return void
     */
    public function show(FinancialSummary $financialSummary)
    {
        $this->authorize('admin.financial-summary.show', $financialSummary);

        // TODO your code goes here
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param FinancialSummary $financialSummary
     * @throws AuthorizationException
     * @return Factory|View
     */
    public function edit(FinancialSummary $financialSummary)
    {
        $this->authorize('admin.financial-summary.edit', $financialSummary);


        return view('admin.financial-summary.edit', [
            'financialSummary' => $financialSummary,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateFinancialSummary $request
     * @param FinancialSummary $financialSummary
     * @return array|RedirectResponse|Redirector
     */
    public function update(UpdateFinancialSummary $request, FinancialSummary $financialSummary)
    {
        // Sanitize input
        $sanitized = $request->getSanitized();

        // Update changed values FinancialSummary
        $financialSummary->update($sanitized);

        if ($request->ajax()) {
            return [
                'redirect' => url('admin/financial-summaries'),
                'message' => trans('brackets/admin-ui::admin.operation.succeeded'),
            ];
        }

        return redirect('admin/financial-summaries');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param DestroyFinancialSummary $request
     * @param FinancialSummary $financialSummary
     * @throws Exception
     * @return ResponseFactory|RedirectResponse|Response
     */
    public function destroy(DestroyFinancialSummary $request, FinancialSummary $financialSummary)
    {
        $financialSummary->delete();

        if ($request->ajax()) {
            return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
        }

        return redirect()->back();
    }

    /**
     * Remove the specified resources from storage.
     *
     * @param BulkDestroyFinancialSummary $request
     * @throws Exception
     * @return Response|bool
     */
    public function bulkDestroy(BulkDestroyFinancialSummary $request) : Response
    {
        DB::transaction(static function () use ($request) {
            collect($request->data['ids'])
                ->chunk(1000)
                ->each(static function ($bulkChunk) {
                    FinancialSummary::whereIn('id', $bulkChunk)->delete();

                    // TODO your code goes here
                });
        });

        return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
    }
}
