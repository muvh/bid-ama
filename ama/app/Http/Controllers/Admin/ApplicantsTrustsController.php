<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\ApplicantsTrust\BulkDestroyApplicantsTrust;
use App\Http\Requests\Admin\ApplicantsTrust\DestroyApplicantsTrust;
use App\Http\Requests\Admin\ApplicantsTrust\IndexApplicantsTrust;
use App\Http\Requests\Admin\ApplicantsTrust\StoreApplicantsTrust;
use App\Http\Requests\Admin\ApplicantsTrust\UpdateApplicantsTrust;
use App\Models\ApplicantsTrust;
use Brackets\AdminListing\Facades\AdminListing;
use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;

class ApplicantsTrustsController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param IndexApplicantsTrust $request
     * @return array|Factory|View
     */
    public function index(IndexApplicantsTrust $request)
    {
        // create and AdminListing instance for a specific model and
        $data = AdminListing::create(ApplicantsTrust::class)->processRequestAndGet(
            // pass the request with params
            $request,

            // set columns to query
            ['id', 'applicant_id', 'trust_id'],

            // set columns to searchIn
            ['id']
        );

        if ($request->ajax()) {
            if ($request->has('bulk')) {
                return [
                    'bulkItems' => $data->pluck('id')
                ];
            }
            return ['data' => $data];
        }

        return view('admin.applicants-trust.index', ['data' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @throws AuthorizationException
     * @return Factory|View
     */
    public function create()
    {
        $this->authorize('admin.applicants-trust.create');

        return view('admin.applicants-trust.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreApplicantsTrust $request
     * @return array|RedirectResponse|Redirector
     */
    public function store(StoreApplicantsTrust $request)
    {
        // Sanitize input
        $sanitized = $request->getSanitized();

        // Store the ApplicantsTrust
        $applicantsTrust = ApplicantsTrust::create($sanitized);

        if ($request->ajax()) {
            return ['redirect' => url('admin/applicants-trusts'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
        }

        return redirect('admin/applicants-trusts');
    }

    /**
     * Display the specified resource.
     *
     * @param ApplicantsTrust $applicantsTrust
     * @throws AuthorizationException
     * @return void
     */
    public function show(ApplicantsTrust $applicantsTrust)
    {
        $this->authorize('admin.applicants-trust.show', $applicantsTrust);

        // TODO your code goes here
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param ApplicantsTrust $applicantsTrust
     * @throws AuthorizationException
     * @return Factory|View
     */
    public function edit(ApplicantsTrust $applicantsTrust)
    {
        $this->authorize('admin.applicants-trust.edit', $applicantsTrust);


        return view('admin.applicants-trust.edit', [
            'applicantsTrust' => $applicantsTrust,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateApplicantsTrust $request
     * @param ApplicantsTrust $applicantsTrust
     * @return array|RedirectResponse|Redirector
     */
    public function update(UpdateApplicantsTrust $request, ApplicantsTrust $applicantsTrust)
    {
        // Sanitize input
        $sanitized = $request->getSanitized();

        // Update changed values ApplicantsTrust
        $applicantsTrust->update($sanitized);

        if ($request->ajax()) {
            return [
                'redirect' => url('admin/applicants-trusts'),
                'message' => trans('brackets/admin-ui::admin.operation.succeeded'),
            ];
        }

        return redirect('admin/applicants-trusts');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param DestroyApplicantsTrust $request
     * @param ApplicantsTrust $applicantsTrust
     * @throws Exception
     * @return ResponseFactory|RedirectResponse|Response
     */
    public function destroy(DestroyApplicantsTrust $request, ApplicantsTrust $applicantsTrust)
    {
        $applicantsTrust->delete();

        if ($request->ajax()) {
            return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
        }

        return redirect()->back();
    }

    /**
     * Remove the specified resources from storage.
     *
     * @param BulkDestroyApplicantsTrust $request
     * @throws Exception
     * @return Response|bool
     */
    public function bulkDestroy(BulkDestroyApplicantsTrust $request) : Response
    {
        DB::transaction(static function () use ($request) {
            collect($request->data['ids'])
                ->chunk(1000)
                ->each(static function ($bulkChunk) {
                    ApplicantsTrust::whereIn('id', $bulkChunk)->delete();

                    // TODO your code goes here
                });
        });

        return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
    }
}
