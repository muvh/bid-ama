<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\AccountingRecord\BulkDestroyAccountingRecord;
use App\Http\Requests\Admin\AccountingRecord\DestroyAccountingRecord;
use App\Http\Requests\Admin\AccountingRecord\IndexAccountingRecord;
use App\Http\Requests\Admin\AccountingRecord\StoreAccountingRecord;
use App\Http\Requests\Admin\AccountingRecord\UpdateAccountingRecord;
use App\Http\Requests\Admin\ApplicantRecord\IndexApplicantRecord;
use App\Models\AccountingRecord;
use App\Models\Applicant;
use App\Models\AccountingSummary;
use App\Models\ApplicantRecord;
use App\Models\TypeMovement;
use App\Models\ResourceTransfer;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\AccountingRecordExport;
use App\Exports\ResolutionsExport;

use App\Exports\ApplicantsExport;

use Brackets\AdminListing\Facades\AdminListing;
use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;
use PhpParser\Node\Expr\Empty_;
use PhpParser\Node\Stmt\TryCatch;

class AccountingRecordsController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param IndexAccountingRecord $request
     * @return array|Factory|View
     */
    public function index(IndexAccountingRecord $request)
    {
        // create and AdminListing instance for a specific model and
        $data = AdminListing::create(AccountingRecord::class)->processRequestAndGet(
            // pass the request with params
            $request,

            // set columns to query
            ['id', 'summary', 'type_movements_id', 'amount', 'balance', 'confirmed', 'resolution', 'registration_date'],

            // set columns to searchIn
            ['id', 'summary', 'resolution', 'registration_date']
        );

        if ($request->ajax()) {
            if ($request->has('bulk')) {
                return [
                    'bulkItems' => $data->pluck('id')
                ];
            }
            return ['data' => $data];
        }

        $summary = AccountingSummary::find(1);

        $ingresos = AccountingRecord::where('type_movements_id', 1)->orWhere('type_movements_id', 3)->sum('balance');
        $reserva = AccountingRecord::where('type_movements_id', 2)->where('confirmed', true)->sum('balance');
        $masivo =  AccountingRecord::where('type_movements_id', 5)->sum('balance');
        $egresos = $reserva + $masivo;
        $balance = $ingresos - $egresos;
        $subsidios = floor($balance / 34000000);

        //return $egresos;

        return view('admin.accounting-record.index', ['data' => $data, 'summary' => $summary, 'ingresos' => $ingresos, 'egresos' => $egresos, 'balance' => $balance, 'subsidios' => $subsidios]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @throws AuthorizationException
     * @return Factory|View
     */
    public function create()
    {
        $this->authorize('admin.accounting-record.create');

        $type_movements = TypeMovement::all();

        return view('admin.accounting-record.create', compact('type_movements'));
    }
    public function summary()
    { 
        $summary = AccountingSummary::find(1);
        $record= AccountingRecord::orderBy('id', 'asc')->get();
       // ConstructionEntity::orderBy('name', 'asc')->get();;
       // echo($record->type_movement);
        //echo($record);
       // exit;
        $ingresos = AccountingRecord::where('type_movements_id', 1)->orWhere('type_movements_id', 3)->sum('balance');
        $reserva = AccountingRecord::where('type_movements_id', 2)->where('confirmed', true)->sum('balance');
        $masivo =  AccountingRecord::where('type_movements_id', 5)->sum('balance');
        $egresos = $reserva + $masivo;
        $balance = $ingresos - $egresos;
        $subsidios = floor($balance / 34000000);

        //return $subsidios;
         $pdf = \PDF::LoadView('admin.accounting-record.summary', [
            'ingresos' => $ingresos,
            'egresos'=>$egresos,
            'balance'=> $balance,
            'subsidios'=> $subsidios,
            'summary'=> $summary,
            'record'=> json_decode($record)
        ]);
           //$pdf->loadHTML('<h1>Test</h1>');
           return $pdf->stream(); 
        //return view('admin.accounting-record.index', ['data' => $data, 'summary' => $summary, 'ingresos' => $ingresos, 'egresos' => $egresos, 'balance' => $balance, 'subsidios' => $subsidios]);
    }
    public function reports()
    {
        //$this->authorize('admin.accounting-record.create');

        //$type_movements = TypeMovement::all();
        $summary = AccountingSummary::find(1);

        $ingresos = AccountingRecord::where('type_movements_id', 1)->orWhere('type_movements_id', 3)->sum('balance');
        $egresos = AccountingRecord::where('type_movements_id', 2)->where('confirmed', true)->sum('balance');
        $balance = $ingresos - $egresos;
        $subsidios = floor($balance / 34000000);

        return view('admin.accounting-record.reports', compact('summary', 'egresos', 'balance', 'subsidios', 'ingresos'));
    }


    public function accounting()
    {
        return Excel::download(new AccountingRecordExport(), 'Resumen_Contable_' . date('d.m.Y H:i:s') . '.xlsx');
    }

    public function resolutions()
    {
        /*$value = Applicant::where('resolution_date', '!=', null)
            ->select(DB::raw('extract(year from resolution_date) as resolution_date'), DB::raw("count(*) as total"))->orderBy("resolution_date")->groupBy(DB::raw('extract(year from resolution_date)  '))->get();

        $pendientes = Applicant::whereNull('resolution_number')
            ->whereNotNull('file_number')
            ->select(DB::raw("count(*) as total"))
            ->get();*/


        /*$app = Applicant::query();

        $app = $app->whereNull('parent_applicant');*/

        /*$app = Applicant::query();
        $fecha = '2023-01-01';
        $ochenta = 41;
        $veinte = 44;
        //$app = $app->select('names');
        $app = $app->whereNull('parent_applicant');
        $app = $app->whereHas('statuses', function ($q) use ($fecha) {
            $q->where('created_at', '>', $fecha)->latest('id');
        });
        $app = $app->whereHas('statuses', function ($q) use ($ochenta) {
            $q->where('status_id', '=', $ochenta)->latest('id');
        });
        $app = $app->orWhereHas('statuses', function ($q) use ($veinte) {
            $q->where('status_id', '=', $veinte)->latest('id');
        });
        $scrapper = $app->get();

        return $scrapper;*/




        //return Applicant::whereNull('applicants.parent_applicant')->get();
        /*return $estados = Applicant::whereNull('applicants.parent_applicant')
            ->where('government_id', '=', '1773509')

            ->get();*/

        //return Excel::download(new ApplicantsExport(), 'Resumen_Resoluciones_' . date('d.m.Y H:i:s') . '.xlsx');
        return Excel::download(new ResolutionsExport(), 'Resumen_Resoluciones_' . date('d.m.Y H:i:s') . '.xlsx');
    }

    public function resolutions_old()
    {
        $value = Applicant::where('resolution_date', '!=', null)
            ->select(DB::raw('extract(year from resolution_date) as resolution_date'), DB::raw("count(*) as total"))->orderBy("resolution_date")->groupBy(DB::raw('extract(year from resolution_date)  '))->get();

        $pendientes = Applicant::whereNull('resolution_number')
            ->whereNotNull('file_number')
            ->select(DB::raw("count(*) as total"))
            ->get();



        return $pendientes;

        return Excel::download(new ResolutionsExport(), 'Resumen_Resoluciones_' . date('d.m.Y H:i:s') . '.xlsx');
    }





    /**
     * Store a newly created resource in storage.
     *
     * @param StoreAccountingRecord $request
     * @return array|RedirectResponse|Redirector
     */
    public function store(StoreAccountingRecord $request)
    {
        // Sanitize input
        $sanitized = $request->getSanitized();
        $sanitized['type_movements_id'] = $request->getStateId();

        // Store the AccountingRecord
        //return $sanitized;
        if ($sanitized['type_movements_id'] == 1 or $sanitized['type_movements_id'] == 3) {
            //return "ingreso";

            $summary = AccountingSummary::find(1);
            $income = $summary['income'] + $sanitized['balance'];
            $balance = $summary['balance'] + $sanitized['balance'];
            $accountingRecord = AccountingRecord::create($sanitized);
            AccountingSummary::where('id', 1)->update(['income' => $income, 'balance' => $balance]);
        } else {
            $accountingRecord = AccountingRecord::create($sanitized);
        }
        //$accountingRecord = AccountingRecord::create($sanitized);

        if ($request->ajax()) {
            return ['redirect' => url('admin/accounting-records'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
        }

        return redirect('admin/accounting-records');
    }

    /**
     * Display the specified resource.
     *
     * @param AccountingRecord $accountingRecord
     * @throws AuthorizationException
     * @return void
     */
    public function show(AccountingRecord $accountingRecord, IndexApplicantRecord $request)
    {
        $this->authorize('admin.accounting-record.show', $accountingRecord);

        //return $accountingRecord->beneficiaries->sum('amount');
        $summary = AccountingSummary::find(1);
        $applicantID = $accountingRecord['id'];
        //$estado = $accountingRecord['confirmed'];
        //$estado = $accountingRecord['confirmed'] ? 'true' : 'false';

        //return $estado;
        $data = AdminListing::create(ApplicantRecord::class)->processRequestAndGet(
            // pass the request with params
            $request,

            // set columns to query
            ['id', 'accounting_record_id', 'applicant_id', 'amount'],

            // set columns to searchIn
            ['id'],
            function ($query) use ($applicantID) {
                $query
                    ->where('accounting_record_id', '=', $applicantID);
            }
        );

        if ($request->ajax()) {
            if ($request->has('bulk')) {
                return [
                    'bulkItems' => $data->pluck('id')
                ];
            }
            return ['data' => $data];
        }

        $ingresos = AccountingRecord::where('type_movements_id', 1)->orWhere('type_movements_id', 3)->sum('balance');
        $egresos = AccountingRecord::where('type_movements_id', 2)->where('confirmed', true)->sum('balance');
        $balance = $ingresos - $egresos;
        $subsidios = floor($balance / 34000000);

        return view('admin.accounting-record.show', ['data' => $data, 'accountingRecord' => $accountingRecord, 'summary' => $summary, 'ingresos' => $ingresos, 'egresos' => $egresos, 'balance' => $balance, 'subsidios' => $subsidios]);

        // TODO your code goes here
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param AccountingRecord $accountingRecord
     * @throws AuthorizationException
     * @return Factory|View
     */
    public function edit(AccountingRecord $accountingRecord)
    {
        $this->authorize('admin.accounting-record.edit', $accountingRecord);
        $type_movements = TypeMovement::all();


        return view('admin.accounting-record.edit', [
            'accountingRecord' => $accountingRecord,
            'type_movements' => $type_movements,
        ]);
    }

    public function confirm(AccountingRecord $accountingRecord)
    {
        //$this->authorize('admin.accounting-record.edit', $accountingRecord);

        try {
            $summary = AccountingSummary::find(1);
            $expenses = $summary['expenses'] + $accountingRecord['balance'];
            $balance = $summary['balance'] - $accountingRecord['balance'];
            //$accountingRecord = AccountingRecord::create($sanitized);
            AccountingRecord::where('id', $accountingRecord['id'])->update(['confirmed' => true, 'balance' =>  $accountingRecord->beneficiaries->sum('amount')]);
            //AccountingSummary::where('id', 1)->update(['expenses' => $expenses, 'balance' => $balance]);

            return response(['message' => 'Reserva Confirmada']);
        } catch (\Throwable $th) {
            return response(['message' => $th]);
        }


        /*return view('admin.accounting-record.edit', [
            'accountingRecord' => $accountingRecord,
        ]);*/
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateAccountingRecord $request
     * @param AccountingRecord $accountingRecord
     * @return array|RedirectResponse|Redirector
     */
    public function update(UpdateAccountingRecord $request, AccountingRecord $accountingRecord)
    {
        // Sanitize input
        $sanitized = $request->getSanitized();
        //return $sanitized;
        // Update changed values AccountingRecord
        $accountingRecord->update($sanitized);

        if ($request->ajax()) {
            return [
                'redirect' => url('admin/accounting-records'),
                'message' => trans('brackets/admin-ui::admin.operation.succeeded'),
            ];
        }

        return redirect('admin/accounting-records');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param DestroyAccountingRecord $request
     * @param AccountingRecord $accountingRecord
     * @throws Exception
     * @return ResponseFactory|RedirectResponse|Response
     */
    public function destroy(DestroyAccountingRecord $request, AccountingRecord $accountingRecord)
    {
        $accountingRecord->delete();

        if ($request->ajax()) {
            return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
        }

        return redirect()->back();
    }

    /**
     * Remove the specified resources from storage.
     *
     * @param BulkDestroyAccountingRecord $request
     * @throws Exception
     * @return Response|bool
     */
    public function bulkDestroy(BulkDestroyAccountingRecord $request): Response
    {
        DB::transaction(static function () use ($request) {
            collect($request->data['ids'])
                ->chunk(1000)
                ->each(static function ($bulkChunk) {
                    AccountingRecord::whereIn('id', $bulkChunk)->delete();

                    // TODO your code goes here
                });
        });

        return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
    }
}
