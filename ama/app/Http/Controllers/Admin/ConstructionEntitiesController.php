<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\ConstructionEntity\BulkDestroyConstructionEntity;
use App\Http\Requests\Admin\ConstructionEntity\DestroyConstructionEntity;
use App\Http\Requests\Admin\ConstructionEntity\IndexConstructionEntity;
use App\Http\Requests\Admin\ConstructionEntity\StoreConstructionEntity;
use App\Http\Requests\Admin\ConstructionEntity\UpdateConstructionEntity;
use App\Models\ConstructionEntity;
use Brackets\AdminListing\Facades\AdminListing;
use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;

class ConstructionEntitiesController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param IndexConstructionEntity $request
     * @return array|Factory|View
     */
    public function index(IndexConstructionEntity $request)
    {
        // create and AdminListing instance for a specific model and
        $data = AdminListing::create(ConstructionEntity::class)->processRequestAndGet(
            // pass the request with params
            $request,

            // set columns to query
            ['id', 'name', 'bank_name', 'cta_cte', 'const_ruc', 'razon_social', 'repre', 'nro_ci'],

            // set columns to searchIn
            ['id', 'name', 'bank_name', 'cta_cte', 'const_ruc', 'razon_social', 'repre', 'nro_ci']
        );

        if ($request->ajax()) {
            if ($request->has('bulk')) {
                return [
                    'bulkItems' => $data->pluck('id')
                ];
            }
            return ['data' => $data];
        }

        return view('admin.construction-entity.index', ['data' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @throws AuthorizationException
     * @return Factory|View
     */
    public function create()
    {
        $this->authorize('admin.construction-entity.create');

        return view('admin.construction-entity.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreConstructionEntity $request
     * @return array|RedirectResponse|Redirector
     */
    public function store(StoreConstructionEntity $request)
    {
        // Sanitize input
        $sanitized = $request->getSanitized();
        $existe=ConstructionEntity::where('const_ruc', trim($request->const_ruc))->get();
        if($existe->count()>0){
            if ($request->ajax()) {
                return ['notify' => ['type' => 'success', 'title' => 'Atención', 'message' => 'El RUC ya fue cargada. Favor verificar..!!!']];
                //return ['redirect' => url('admin/construction-entities/create'), 'message' => 'El RUC ya fue cargada. Favor verificar..!!!'];
            }
        }
        //return $ruc;
        // Store the ConstructionEntity
        $constructionEntity = ConstructionEntity::create($sanitized);

        if ($request->ajax()) {
            return ['redirect' => url('admin/construction-entities'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
        }

        return redirect('admin/construction-entities');
    }

    /**
     * Display the specified resource.
     *
     * @param ConstructionEntity $constructionEntity
     * @throws AuthorizationException
     * @return void
     */
    public function show(ConstructionEntity $constructionEntity)
    {
        $this->authorize('admin.construction-entity.show', $constructionEntity);

        // TODO your code goes here
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param ConstructionEntity $constructionEntity
     * @throws AuthorizationException
     * @return Factory|View
     */
    public function edit(ConstructionEntity $constructionEntity)
    {
        $this->authorize('admin.construction-entity.edit', $constructionEntity);


        return view('admin.construction-entity.edit', [
            'constructionEntity' => $constructionEntity,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateConstructionEntity $request
     * @param ConstructionEntity $constructionEntity
     * @return array|RedirectResponse|Redirector
     */
    public function update(UpdateConstructionEntity $request, ConstructionEntity $constructionEntity)
    {
        // Sanitize input
        $sanitized = $request->getSanitized();

        // Update changed values ConstructionEntity
        $constructionEntity->update($sanitized);

        if ($request->ajax()) {
            return [
                'redirect' => url('admin/construction-entities'),
                'message' => trans('brackets/admin-ui::admin.operation.succeeded'),
            ];
        }

        return redirect('admin/construction-entities');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param DestroyConstructionEntity $request
     * @param ConstructionEntity $constructionEntity
     * @throws Exception
     * @return ResponseFactory|RedirectResponse|Response
     */
    public function destroy(DestroyConstructionEntity $request, ConstructionEntity $constructionEntity)
    {
        $constructionEntity->delete();

        if ($request->ajax()) {
            return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
        }

        return redirect()->back();
    }

    /**
     * Remove the specified resources from storage.
     *
     * @param BulkDestroyConstructionEntity $request
     * @throws Exception
     * @return Response|bool
     */
    public function bulkDestroy(BulkDestroyConstructionEntity $request) : Response
    {
        DB::transaction(static function () use ($request) {
            collect($request->data['ids'])
                ->chunk(1000)
                ->each(static function ($bulkChunk) {
                    ConstructionEntity::whereIn('id', $bulkChunk)->delete();

                    // TODO your code goes here
                });
        });

        return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
    }
}
