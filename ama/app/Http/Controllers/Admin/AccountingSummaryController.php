<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\AccountingSummary\BulkDestroyAccountingSummary;
use App\Http\Requests\Admin\AccountingSummary\DestroyAccountingSummary;
use App\Http\Requests\Admin\AccountingSummary\IndexAccountingSummary;
use App\Http\Requests\Admin\AccountingSummary\StoreAccountingSummary;
use App\Http\Requests\Admin\AccountingSummary\UpdateAccountingSummary;
use App\Models\AccountingSummary;
use Brackets\AdminListing\Facades\AdminListing;
use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;

class AccountingSummaryController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param IndexAccountingSummary $request
     * @return array|Factory|View
     */
    public function index(IndexAccountingSummary $request)
    {
        // create and AdminListing instance for a specific model and
        $data = AdminListing::create(AccountingSummary::class)->processRequestAndGet(
            // pass the request with params
            $request,

            // set columns to query
            ['id', 'income', 'expenses', 'balance'],

            // set columns to searchIn
            ['id']
        );

        if ($request->ajax()) {
            if ($request->has('bulk')) {
                return [
                    'bulkItems' => $data->pluck('id')
                ];
            }
            return ['data' => $data];
        }

        return view('admin.accounting-summary.index', ['data' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @throws AuthorizationException
     * @return Factory|View
     */
    public function create()
    {
        $this->authorize('admin.accounting-summary.create');

        return view('admin.accounting-summary.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreAccountingSummary $request
     * @return array|RedirectResponse|Redirector
     */
    public function store(StoreAccountingSummary $request)
    {
        // Sanitize input
        $sanitized = $request->getSanitized();

        // Store the AccountingSummary
        $accountingSummary = AccountingSummary::create($sanitized);

        if ($request->ajax()) {
            return ['redirect' => url('admin/accounting-summaries'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
        }

        return redirect('admin/accounting-summaries');
    }

    /**
     * Display the specified resource.
     *
     * @param AccountingSummary $accountingSummary
     * @throws AuthorizationException
     * @return void
     */
    public function show(AccountingSummary $accountingSummary)
    {
        $this->authorize('admin.accounting-summary.show', $accountingSummary);

        // TODO your code goes here
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param AccountingSummary $accountingSummary
     * @throws AuthorizationException
     * @return Factory|View
     */
    public function edit(AccountingSummary $accountingSummary)
    {
        $this->authorize('admin.accounting-summary.edit', $accountingSummary);


        return view('admin.accounting-summary.edit', [
            'accountingSummary' => $accountingSummary,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateAccountingSummary $request
     * @param AccountingSummary $accountingSummary
     * @return array|RedirectResponse|Redirector
     */
    public function update(UpdateAccountingSummary $request, AccountingSummary $accountingSummary)
    {
        // Sanitize input
        $sanitized = $request->getSanitized();

        // Update changed values AccountingSummary
        $accountingSummary->update($sanitized);

        if ($request->ajax()) {
            return [
                'redirect' => url('admin/accounting-summaries'),
                'message' => trans('brackets/admin-ui::admin.operation.succeeded'),
            ];
        }

        return redirect('admin/accounting-summaries');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param DestroyAccountingSummary $request
     * @param AccountingSummary $accountingSummary
     * @throws Exception
     * @return ResponseFactory|RedirectResponse|Response
     */
    public function destroy(DestroyAccountingSummary $request, AccountingSummary $accountingSummary)
    {
        $accountingSummary->delete();

        if ($request->ajax()) {
            return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
        }

        return redirect()->back();
    }

    /**
     * Remove the specified resources from storage.
     *
     * @param BulkDestroyAccountingSummary $request
     * @throws Exception
     * @return Response|bool
     */
    public function bulkDestroy(BulkDestroyAccountingSummary $request) : Response
    {
        DB::transaction(static function () use ($request) {
            collect($request->data['ids'])
                ->chunk(1000)
                ->each(static function ($bulkChunk) {
                    AccountingSummary::whereIn('id', $bulkChunk)->delete();

                    // TODO your code goes here
                });
        });

        return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
    }
}
