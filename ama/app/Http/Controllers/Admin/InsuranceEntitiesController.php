<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\InsuranceEntity\BulkDestroyInsuranceEntity;
use App\Http\Requests\Admin\InsuranceEntity\DestroyInsuranceEntity;
use App\Http\Requests\Admin\InsuranceEntity\IndexInsuranceEntity;
use App\Http\Requests\Admin\InsuranceEntity\StoreInsuranceEntity;
use App\Http\Requests\Admin\InsuranceEntity\UpdateInsuranceEntity;
use App\Models\InsuranceEntity;
use Brackets\AdminListing\Facades\AdminListing;
use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;

class InsuranceEntitiesController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param IndexInsuranceEntity $request
     * @return array|Factory|View
     */
    public function index(IndexInsuranceEntity $request)
    {
        // create and AdminListing instance for a specific model and
        $data = AdminListing::create(InsuranceEntity::class)->processRequestAndGet(
            // pass the request with params
            $request,

            // set columns to query
            ['id', 'title'],

            // set columns to searchIn
            ['id', 'title']
        );

        if ($request->ajax()) {
            if ($request->has('bulk')) {
                return [
                    'bulkItems' => $data->pluck('id')
                ];
            }
            return ['data' => $data];
        }

        return view('admin.insurance-entity.index', ['data' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @throws AuthorizationException
     * @return Factory|View
     */
    public function create()
    {
        //$this->authorize('admin.insurance-entity.create');

        return view('admin.insurance-entity.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreInsuranceEntity $request
     * @return array|RedirectResponse|Redirector
     */
    public function store(StoreInsuranceEntity $request)
    {
        // Sanitize input
        $sanitized = $request->getSanitized();

        // Store the InsuranceEntity
        $insuranceEntity = InsuranceEntity::create($sanitized);

        if ($request->ajax()) {
            return ['redirect' => url('admin/insurance-entities'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
        }

        return redirect('admin/insurance-entities');
    }

    /**
     * Display the specified resource.
     *
     * @param InsuranceEntity $insuranceEntity
     * @throws AuthorizationException
     * @return void
     */
    public function show(InsuranceEntity $insuranceEntity)
    {
        $this->authorize('admin.insurance-entity.show', $insuranceEntity);

        // TODO your code goes here
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param InsuranceEntity $insuranceEntity
     * @throws AuthorizationException
     * @return Factory|View
     */
    public function edit(InsuranceEntity $insuranceEntity)
    {
        $this->authorize('admin.insurance-entity.edit', $insuranceEntity);


        return view('admin.insurance-entity.edit', [
            'insuranceEntity' => $insuranceEntity,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateInsuranceEntity $request
     * @param InsuranceEntity $insuranceEntity
     * @return array|RedirectResponse|Redirector
     */
    public function update(UpdateInsuranceEntity $request, InsuranceEntity $insuranceEntity)
    {
        // Sanitize input
        $sanitized = $request->getSanitized();

        // Update changed values InsuranceEntity
        $insuranceEntity->update($sanitized);

        if ($request->ajax()) {
            return [
                'redirect' => url('admin/insurance-entities'),
                'message' => trans('brackets/admin-ui::admin.operation.succeeded'),
            ];
        }

        return redirect('admin/insurance-entities');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param DestroyInsuranceEntity $request
     * @param InsuranceEntity $insuranceEntity
     * @throws Exception
     * @return ResponseFactory|RedirectResponse|Response
     */
    public function destroy(DestroyInsuranceEntity $request, InsuranceEntity $insuranceEntity)
    {
        $insuranceEntity->delete();

        if ($request->ajax()) {
            return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
        }

        return redirect()->back();
    }

    /**
     * Remove the specified resources from storage.
     *
     * @param BulkDestroyInsuranceEntity $request
     * @throws Exception
     * @return Response|bool
     */
    public function bulkDestroy(BulkDestroyInsuranceEntity $request) : Response
    {
        DB::transaction(static function () use ($request) {
            collect($request->data['ids'])
                ->chunk(1000)
                ->each(static function ($bulkChunk) {
                    InsuranceEntity::whereIn('id', $bulkChunk)->delete();

                    // TODO your code goes here
                });
        });

        return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
    }
}
