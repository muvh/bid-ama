<?php

namespace App\Http\Controllers\Admin;

use Exception;
use App\Models\Applicant;
use App\Models\TypeOrder;
use Illuminate\View\View;
use Illuminate\Http\Response;
use App\Models\ResourceTransfer;
use App\Models\ConstructionEntity;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Http\RedirectResponse;
use Illuminate\Contracts\View\Factory;
use App\Models\ApplicantResourceTransfer;
use Symfony\Component\HttpFoundation\Request;
use Brackets\AdminListing\Facades\AdminListing;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Auth\Access\AuthorizationException;
use App\Http\Requests\Admin\ResourceTransfer\IndexResourceTransfer;
use App\Http\Requests\Admin\ResourceTransfer\StoreResourceTransfer;
use App\Http\Requests\Admin\ResourceTransfer\UpdateResourceTransfer;
use App\Http\Requests\Admin\ResourceTransfer\DestroyResourceTransfer;
use App\Http\Requests\Admin\ResourceTransfer\BulkDestroyResourceTransfer;
use Luecano\NumeroALetras\NumeroALetras;

class ResourceTransfersController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param IndexResourceTransfer $request
     * @return array|Factory|View
     */
    public function index(IndexResourceTransfer $request)
    {
        // create and AdminListing instance for a specific model and
        $data = AdminListing::create(ResourceTransfer::class)->processRequestAndGet(
            // pass the request with params
            $request,

            // set columns to query
            ['id', 'nro_solictud_rt', 'fecha_rt', 'type_order_id', 'construction_entity_id','culminado', 'nro_nota_bid', 'obs'],

            // set columns to searchIn
            ['id', 'nro_solictud_rt', 'nro_nota_bid']
        );

        if ($request->ajax()) {
            if ($request->has('bulk')) {
                return [
                    'bulkItems' => $data->pluck('id')
                ];
            }
            return ['data' => $data];
        }

        return view('admin.resource-transfer.index', ['data' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @throws AuthorizationException
     * @return Factory|View
     */
    public function create()
    {
        $this->authorize('admin.resource-transfer.create');
        $constructionEntity= ConstructionEntity::all();
        $typeOrder= TypeOrder::all();
        return view('admin.resource-transfer.create', ['constructions'=>$constructionEntity,'typeorders'=>$typeOrder ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreResourceTransfer $request
     * @return array|RedirectResponse|Redirector
     */
    public function store(StoreResourceTransfer $request)
    {
        // Sanitize input
        $existe=ResourceTransfer::where('nro_solictud_rt', $request['nro_solictud_rt'])->get();
        if($existe->count()>0){
            if ($request->ajax()) {
                return ['notify' => ['type' => 'success', 'title' => 'Atención', 'message' => 'El Nro de Solicitid ya se encuentra. Verificar.. No puede repetirse.']];
                //exit;
            }
        }
        $sanitized = $request->getSanitized();
        $sanitized['type_order_id'] = $request->getTypeOrder();
        $sanitized['construction_entity_id'] = $request->getConstructionEntity();
        // Store the ResourceTransfer
        $resourceTransfer = ResourceTransfer::create($sanitized);

        if ($request->ajax()) {
            return ['redirect' => url('admin/resource-transfers/'.$resourceTransfer->id.'/edit'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
        }

        return redirect('admin/resource-transfers');
    }
    public function editresource(StoreResourceTransfer $request, $idresource)
    {
        // Sanitize input
        $resource = ResourceTransfer::find($idresource);
        $data=$request->all();
        //print_r($data);
        //$sanitized = $resource->getSanitized();
        //dd($resource);
        //exit;
        $resource->type_order_id = $data['typeorders'];
        $resource->construction_entity_id = $data['atc'];
        $resource->nro_solictud_rt = $data['nro_solictud_rt'];
        $resource->fecha_rt = $data['fecha_rt'];
        $resource->nro_nota_bid = $data['nro_nota_bid'];
        // Store the ResourceTransfer
        $resource->save();

        if ($request->ajax()) {
            return ['redirect' => url('admin/resource-transfers/'.$resource->id.'/edit'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
        }

        return redirect('admin/resource-transfers/'.$resource->id.'/edit');
    }
    public function addobs(ResourceTransfer $ResourceTransfer, IndexResourceTransfer $request, $rtid=0)
    {
        // Sanitize input
        $data = $request->all();
        //return $rtid; 
        //dd($request);
       // print_r($request);
        //$sanitized = $resource->getSanitized();
        //dd($resource);
        //exit;
        $obsvalue=$request['obs'];
        //return $obsvalue; 
        $affectedRows = ResourceTransfer::where("id", $rtid)->update(["obs" => "$obsvalue"]);
        //$ResourceTransfer->obs = $request['obs'];
        // Store the ResourceTransfer
        //$ResourceTransfer->save();

        if ($request->ajax()) {
            return ['redirect' => url('admin/resource-transfers/'.$rtid.'/edit'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
        }

        return redirect('admin/resource-transfers/'.$rtid.'/edit'); 
    }

    /**
     * Display the specified resource.
     *
     * @param ResourceTransfer $resourceTransfer
     * @throws AuthorizationException
     * @return void
     */
    public function show(ResourceTransfer $resourceTransfer)
    {
        $this->authorize('admin.resource-transfer.show', $resourceTransfer);

        // TODO your code goes here
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param ResourceTransfer $resourceTransfer
     * @throws AuthorizationException
     * @return Factory|View
     */
    public function edit(ResourceTransfer $resourceTransfer)
    {
        $this->authorize('admin.resource-transfer.edit', $resourceTransfer);
        $apprestra=ApplicantResourceTransfer::where('resource_transfer_id', $resourceTransfer->id)
        ->get();
        $typeOrder= TypeOrder::all();
        $constructionEntity= ConstructionEntity::all();
       //return  $apprestra;
        return view('admin.resource-transfer.edit', [
            'resourceTransfer' => $resourceTransfer,
            'apprestra' => $apprestra,
            'typeorders'=>$typeOrder,
            'constructions'=>$constructionEntity
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateResourceTransfer $request
     * @param ResourceTransfer $resourceTransfer
     * @return array|RedirectResponse|Redirector
     */
    public function update(UpdateResourceTransfer $request, ResourceTransfer $resourceTransfer)
    {
        // Sanitize input
        $sanitized = $request->getSanitized();

        // Update changed values ResourceTransfer
        $resourceTransfer->update($sanitized);

        if ($request->ajax()) {
            return [
                'redirect' => url('admin/resource-transfers'),
                'message' => trans('brackets/admin-ui::admin.operation.succeeded'),
            ];
        }

        return redirect('admin/resource-transfers');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param DestroyResourceTransfer $request
     * @param ResourceTransfer $resourceTransfer
     * @throws Exception
     * @return ResponseFactory|RedirectResponse|Response
     */
    public function destroy(DestroyResourceTransfer $request, ResourceTransfer $resourceTransfer)
    {
        $resourceTransfer->delete();

        if ($request->ajax()) {
            return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
        }

        return redirect()->back();
    }

    /**
     * Remove the specified resources from storage.
     *
     * @param BulkDestroyResourceTransfer $request
     * @throws Exception
     * @return Response|bool
     */
    public function bulkDestroy(BulkDestroyResourceTransfer $request) : Response
    {
        DB::transaction(static function () use ($request) {
            collect($request->data['ids'])
                ->chunk(1000)
                ->each(static function ($bulkChunk) {
                    ResourceTransfer::whereIn('id', $bulkChunk)->delete();

                    // TODO your code goes here
                });
        });

        return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
    }
    public function findlistado($id)
    {
        //return $id;
    $data=ApplicantResourceTransfer::where('resource_transfer_id', $id)
    ->get();

    //return $data;

    if($data->count()>0){  //si hay datos retorna
        
        return [ 
            'data'=>$data[0],
            'cod'=>1
        ];
    }else{
        return response()->json([
            'cod' => 0,
            'data' => "",
            
        ]);

    }
  
    }

    public function findApplicant($government_id)
    {
        $data=Applicant::whereNull('applicants.parent_applicant')
        ->where('government_id', $government_id)
        ->get();
        //return $data->count();

        if($data->count()>0){  //si hay datos retorna
            
            $applicant=ApplicantResourceTransfer::where('applicant_id', $data[0]->id)->get();
        return [ 
            'data'=>$data[0],
            'applicant'=>$applicant,
            'cod'=>1
        ];
    }else{
        return response()->json([
            'cod' => 0,
            'data' => "",
            
        ]);

    }
  
    }
    public function addApplicant( ResourceTransfer $resourceTransfer, IndexResourceTransfer $request)
    {
        $data = $request->all();
        //return $request;
        $existe=ApplicantResourceTransfer::where('applicant_id', $request['applicant_id'])->get();
        if($existe->count()>0){
            if ($request->ajax()) {
                return ['notify' => ['type' => 'success', 'title' => 'Atención', 'message' => 'El soliciante ya fue cargado.']];
                //exit;
            }
        }

        $trustsave = ApplicantResourceTransfer::create([
            'resource_transfer_id' => $request['resource_transfer_id'],
            'applicant_id' => $request['applicant_id']
        ]);
        if($trustsave){
             if ($request->ajax()) {
                //return $sanitized;
                return ['redirect' => url('admin/resource-transfers/'.$request['resource_transfer_id'].'/edit'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
            } 
        }
        return redirect('admin/resource-transfers/'.$request['resource_transfer_id'].'/edit');
        //return ['redirect' => url('admin/trusts/'.$request['trust_id'].'/edit'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
    }
    public function showfideicomiso(ResourceTransfer $resourceTransfer){
        //require 'vendor/autoload.php';
        $applicant=ApplicantResourceTransfer::where('resource_transfer_id',$resourceTransfer->id)->get();
        $resource=ResourceTransfer::where('id',$resourceTransfer->id)->get();
       
        $mount=0;
        if($resourceTransfer->type_order_id==2){
            $mount= $applicant->count()*7000000;
        }else{
            $mount= $applicant->count()*28000000;

        }
        //return $resourceTransfer;
        $number=$mount;
        $decimals=2;
        $formatter = new NumeroALetras();
        $numeroTexto=  $formatter->toWords($number, $decimals);
       // return  NumeroALetras::convertir(4599.99, 'soles') .'<br>';
         $resourceTransfer->culminado=true;
        $resourceTransfer->save(); 
       //return $resourceTransfer;
        $pdf = \PDF::LoadView('admin.resource-transfer.showfideicomiso', [
            'resourceTransfer' => $resourceTransfer,
            'numeroTexto'=>$numeroTexto,
            'applicant'=> $applicant,
            'mount'=> $mount
        ]);
           //$pdf->loadHTML('<h1>Test</h1>');
           return $pdf->stream();

        //return view('admin.resource-transfer.showfideicomiso', compact('resourceTransfer','applicant','mount', 'numeroTexto'));
    }
    public function showAutorizando(ResourceTransfer $resourceTransfer){
        //require 'vendor/autoload.php';
        $applicant=ApplicantResourceTransfer::where('resource_transfer_id',$resourceTransfer->id)->get();
       
       //return $resourceTransfer;
        $pdf = \PDF::LoadView('admin.resource-transfer.showAutorizando', [
            'resourceTransfer' => $resourceTransfer,
            'applicant' => $applicant,
        ]);
           //$pdf->loadHTML('<h1>Test</h1>');
           return $pdf->stream();

        //return view('admin.resource-transfer.showfideicomiso', compact('resourceTransfer','applicant','mount', 'numeroTexto'));
    }
}
