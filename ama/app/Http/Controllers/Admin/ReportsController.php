<?php

namespace App\Http\Controllers\Admin;

use DB;
use Carbon\Carbon;
use App\Models\City;
use App\Models\Status;
use GuzzleHttp\Client;
use App\Charts\MyChart;
use App\Charts\AmaChart;
use App\Models\Applicant;
use Illuminate\Http\Request;
use App\Charts\AmaChartStatus;
use App\Exports\InvoicesExport;
use App\Models\ApplicantStatus;
use App\Models\FamiliarSetting;
use App\Models\FinancialEntity;
use App\Models\ConstructionEntity;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\InvoicesExportEtapas;
use App\Exports\ApplicantsByResolution;
use App\Exports\ApplicantsByResolutionSocial;
use App\Http\Requests\Admin\Report\CoopReport;
use App\Http\Requests\Admin\Report\IndexReport;
use Brackets\AdminListing\Facades\AdminListing;
use App\Http\Requests\Admin\Report\SocialReport;

class ReportsController extends Controller
{

    public function applicantsByResolutionExport($resolution_number)
    {
        return Excel::download(new ApplicantsByResolution($resolution_number), 'applicants.xlsx');
    }
    public function applicantsByResolutionSocialExport($dateto, $datefrom, $status)
    {
        return Excel::download(new ApplicantsByResolutionSocial($dateto, $datefrom, $status), date("Ymd") . '_report_social.xlsx');
    }
    public function applicantsCoopStatusExport($dateto, $datefrom, $dispatched)
    {
        return Excel::download(new applicantsCoopStatusExport($dateto, $datefrom, $dispatched), date("Ymd") . '_report_social.xlsx');
    }

    public function applicantsByResolution(IndexReport $request)
    {

        $data = $request->all();

        $resolution = $data['search'] ?? 0;

        // create and AdminListing instance for a specific model and
        $data = AdminListing::create(Applicant::class)->processRequestAndGet(
            //lala 1
            $request,

            ['id', 'file_number', 'created_at', 'names', 'government_id', 'marital_status',  'construction_entity', 'financial_entity', 'resolution_number', 'city_id'],
            ['resolution_number'],

            function ($query) use ($resolution) {

                $query->whereNull('applicants.parent_applicant');
                $query->where('resolution_number', '=', $resolution);
                $query->get();
            }

        );

        if ($request->ajax()) {
            return ['data' => $data, 'resolution' => $resolution];
        }


        return view('admin.report.index', ['data' => $data, 'resolution' => $resolution]);
    }
    public function applicantsSearchNames(IndexReport $request)
    {

        $data = $request->all();

        $names = $data['search'] ?? '0';
        $names = strtoupper($names);

        // create and AdminListing instance for a specific model and
        $data = AdminListing::create(Applicant::class)->processRequestAndGet(

            $request,
            ['id', 'names',  'last_names', 'government_id', 'city_id', 'monthly_income', 'construction_entity', 'financial_entity', 'created_at'],

            // set columns to searchIn
            ['id', 'names', 'last_names', 'government_id', 'city_id'],

            function ($query) use ($names) {
                $query->select('id', 'names', 'last_names', 'parent_applicant', 'government_id', 'registration_date',  'city_id');
                $query->whereNull('applicants.parent_applicant')

                    ->where("names", "like", "$names%")
                    ->orwhere("last_names", "like", "$names%")
                ; //->with('status');
                //->whereNull('applicants.parent_applicant')->where('resolution_number', '=', $resolution);
            }

        );

        if ($request->ajax()) {
            return ['data' => $data, 'names' => $names];
        }


        return view('admin.report.search-names', ['data' => $data, 'names' => $names]);
    }
    public function applicantsByCoop(CoopReport $request)
    {

        $data = $request->all();

        $codCoop = $data['coop'] ?? 0;
        $coops = FinancialEntity::all(); // coop report
        //print_r($coops->name);
        $labels = FinancialEntity::all()->pluck('name'); // coop label chart

        $statusData = Status::selectRaw('distinct (statuses.name),
       (select COUNT(applicant_statuses.status_id) from applicant_statuses where applicant_statuses.status_id = statuses.id) as sum')
            ->leftJoin('applicant_statuses', 'applicant_statuses.status_id', '=', 'statuses.id')->pluck('sum', 'statuses.name');

        //return $statusData;

        $statusChart = new AmaChartStatus;
        $statusChart->labels($statusData->keys());
        for ($i = 0; $i <= count($statusData); $i++) {
            $colours2[] = '#' . substr(str_shuffle('ABCDEF0123456789'), 0, 6);
        }
        $statusChart->dataset('Solicitantes por Cooperativa', 'pie', $statusData->values())
            ->backgroundColor($colours2);


        $data = AdminListing::create(Applicant::class)->processRequestAndGet(
            $request,

            ['id', 'file_number', 'admission_data', 'applicant_full_name', 'government_id', 'spouse_data', 'marital_status', 'total_monthly_income', 'construction_entity', 'financial_entity', 'centityname', 'fentityname', 'city_name', 'resolution_number'],
            ['resolution_number'],
            function ($query) use ($codCoop) {
                $query
                    ->selectRaw("ROW_NUMBER () OVER (ORDER BY applicants.id) as id,applicants.file_number as file_number,applicants.created_at as admission_data,
                    applicants.names||' '||applicants.last_names as applicant_full_name, applicants.government_id as government_id,
                    (select b.names||' '||b.last_names||'-'||b.government_id from applicants b where b.parent_applicant = applicants.id and b.applicant_relationship = 1 ) as spouse_data, applicants.marital_status as marital_status,
                    coalesce(coalesce((select sum(beneficiaries.monthly_income) from applicants as beneficiaries
                    inner join applicant_relationships on beneficiaries.applicant_relationship = applicant_relationships.id
                    where beneficiaries.parent_applicant = applicants.id and compute_income = true ),0) + coalesce(applicants.monthly_income,0),0) as total_monthly_income,
                    construction_entity, financial_entity, construction_entities.name as centityname, financial_entities.name as fentityname, cities.name as city_name, applicants.resolution_number
                    ")
                    ->leftJoin('cities', 'applicants.city_id', '=', 'cities.id')
                    ->leftJoin('construction_entities', 'applicants.construction_entity', '=', 'construction_entities.id')
                    ->leftJoin('financial_entities', 'applicants.financial_entity', '=', 'financial_entities.id')
                    ->whereNull('applicants.parent_applicant')
                    ->where('financial_entity', '=', $codCoop);
            }
        );

        if ($request->ajax()) {
            if ($request->has('bulk')) {
                return [
                    'bulkItems' => $data->pluck('id')
                ];
            }
            return ['data' => $data, 'codCoop' => $codCoop,  'coops' => json_decode($coops)];
        }

        return view('admin.report.coop-report', ['data' => $data, 'codCoop' => $codCoop, 'statusChart' => $statusChart, 'labels' => $labels, 'coops' => json_decode($coops)]);
    }


    public function reportEtapas()
    {

        $coops = FinancialEntity::orderBy('name', 'asc')->get();
        $atc = ConstructionEntity::orderBy('name', 'asc')->get();
        $status = Status::orderBy('name', 'asc')->get();
        $configfamily = FamiliarSetting::orderBy('name', 'asc')->get();
        $municipios = City::orderBy('name', 'asc')->get();
        /*$estados=Applicant::whereNull('applicants.parent_applicant')->get();
        $coops=FinancialEntity::all();
        $municipios=City::all();*/

        return view('admin.report.reportEtapas', compact('coops', 'atc', 'status', 'configfamily', 'municipios'));
    }
    public function filtrosEtapas(Request $request)
    {


        /*$atcic= ConstructionEntity::find($request->input('atc'));
        $coopic= FinancialEntity::find($request->input('coop'));
        $muniic=$request->input('muni');
        $configic=$request->input('config');
        $estadoic=$request->input('estado');*/




        /*$filtros = [
            'atc' => $atcic,
            'coop' => $coopic,
            /*'muni' => $atcic['name'],
            'config' => $atcic['name'],
            'estado' => $atcic['name']
        ];*/

        /*$coops=FinancialEntity::all();
        $atc = ConstructionEntity::all();
        $status = Status::all();
        $configfamily = FamiliarSetting::all();
        $municipios=City::all();*/

        //return $request;
        $atc = "";
        $coop = "";
        $muni = "";
        $config = "";
        $estado = "";

        $app = Applicant::query();

        $app = $app->whereNull('parent_applicant');

        if ($request->input('atc')) {
            $app = $app->where('construction_entity', $request->input('atc'));
            $atcic = ConstructionEntity::find($request->input('atc'));
            $atc = $atcic ? $atcic['name'] : 'N/A';
        }

        if ($request->input('coop')) {
            $app = $app->where('financial_entity', $request->input('coop'));
            $coopic = FinancialEntity::find($request->input('coop'));
            $coop = $coopic ? $coopic['name'] : 'N/A';
        }

        if ($request->input('muni')) {
            $app = $app->where('city_id', $request->input('muni'));
            $muniic = City::find($request->input('muni'));
            $muni = $muniic ? $muniic['name'] : 'N/A';
        }

        if ($request->input('config')) {
            $app = $app->where('familiar_setting_id', $request->input('config'));
            $configic = FamiliarSetting::find($request->input('config'));
            $config = $configic ? $configic['name'] : 'N/A';
        }
        //$etapas=$request->input('nameetapas');
        //return $etapas;
        if ($request->input('nameetapas')) {
            $datain = $request->input('nameetapas');
            $stringEtapa = '';
            if (in_array("INS", $datain)) {
                $stringEtapa .= "INSCRIPCIONES";
            }
            if (in_array("POS", $datain)) {
                $stringEtapa .= " - POSTULACIONES";
            }
            if (in_array("ADJ", $datain)) {
                $stringEtapa .= " - ADJUDICACIONES";
            }
            if (in_array("ING", $datain)) {
                $stringEtapa .= " - INHABILITACIONES";
            }
            //return $datain;
            $rsdatain = Status::whereIn('prefix', $datain)->pluck('id')->toArray();
            //return $rsdatain;

            //$app->with('statuses')->where('statuses.status_id', $request->input('estado'));
            $app = $app->whereHas('statuses', function ($q) use ($rsdatain) {
                $q->whereIn('status_id', $rsdatain);
            });
            //$estadoic = Status::find($request->input('estado'));
            $estado = $stringEtapa ? $stringEtapa : 'N/A';
        }

        $scrapper = $app->get();
        //return $scrapper;
        return Excel::download(new InvoicesExportEtapas($scrapper, $atc, $coop, $muni, $config, $estado), 'reporte_ama.xlsx');

        //return view('admin.report.report',compact('coops','atc','status','configfamily','municipios','atcic','coopic','muniic','configic','estadoic'));
    }
    public function report()
    {

        $coops = FinancialEntity::orderBy('name', 'asc')->get();
        $atc = ConstructionEntity::orderBy('name', 'asc')->get();
        $status = Status::orderBy('name', 'asc')->get();
        $configfamily = FamiliarSetting::orderBy('name', 'asc')->get();
        $municipios = City::orderBy('name', 'asc')->get();
        /*$estados=Applicant::whereNull('applicants.parent_applicant')->get();
        $coops=FinancialEntity::all();
        $municipios=City::all();*/

        return view('admin.report.report', compact('coops', 'atc', 'status', 'configfamily', 'municipios'));
    }

    public function filtros(Request $request)
    {

        /*$atcic= ConstructionEntity::find($request->input('atc'));
        $coopic= FinancialEntity::find($request->input('coop'));
        $muniic=$request->input('muni');
        $configic=$request->input('config');
        $estadoic=$request->input('estado');*/




        /*$filtros = [
            'atc' => $atcic,
            'coop' => $coopic,
            /*'muni' => $atcic['name'],
            'config' => $atcic['name'],
            'estado' => $atcic['name']
        ];*/

        /*$coops=FinancialEntity::all();
        $atc = ConstructionEntity::all();
        $status = Status::all();
        $configfamily = FamiliarSetting::all();
        $municipios=City::all();*/

        //return $request;
        $atc = "";
        $coop = "";
        $muni = "";
        $config = "";
        $estado = "";

        $app = Applicant::query();

        $app = $app->whereNull('parent_applicant');

        if ($request->input('atc')) {
            $app = $app->where('construction_entity', $request->input('atc'));
            $atcic = ConstructionEntity::find($request->input('atc'));
            $atc = $atcic ? $atcic['name'] : 'N/A';
        }

        if ($request->input('coop')) {
            $app = $app->where('financial_entity', $request->input('coop'));
            $coopic = FinancialEntity::find($request->input('coop'));
            $coop = $coopic ? $coopic['name'] : 'N/A';
        }

        if ($request->input('muni')) {
            $app = $app->where('city_id', $request->input('muni'));
            $muniic = City::find($request->input('muni'));
            $muni = $muniic ? $muniic['name'] : 'N/A';
        }

        if ($request->input('config')) {
            $app = $app->where('familiar_setting_id', $request->input('config'));
            $configic = FamiliarSetting::find($request->input('config'));
            $config = $configic ? $configic['name'] : 'N/A';
        }

        if ($request->input('estado')) {
            //$app->with('statuses')->where('statuses.status_id', $request->input('estado'));
            $app = $app->whereHas('statuses', function ($q) use ($request) {
                $q->where('status_id', '=', $request->input('estado'))->latest('id');
            });
            $estadoic = Status::find($request->input('estado'));
            $estado = $estadoic ? $estadoic['name'] : 'N/A';
        }

        $scrapper = $app->with('reportDisabilities', 'reportDiseases', 'applicantStatuses')->get();
        //return $scrapper;
        return Excel::download(new InvoicesExport($scrapper, $atc, $coop, $muni, $config, $estado), 'reporte_ama.xlsx');

        //return view('admin.report.report',compact('coops','atc','status','configfamily','municipios','atcic','coopic','muniic','configic','estadoic'));
    }

    //public function

    public function atcByStatus(CoopReport $request, $status = 0, $entity = 0, $radio = 0, $config = 0)
    {
        //return $entity;
        $ConsEntity = ConstructionEntity::all();
        $statusName = Status::all();
        $configfamily = FamiliarSetting::all();
        $estados = Applicant::whereNull('applicants.parent_applicant')->get();
        $coops = FinancialEntity::all();
        $municipios = City::all();

        $idApplicant = [];

        foreach ($estados as $value) {

            if ($value->statuses->status_id == $status) {
                $idApplicant[] = $value->statuses->applicant_id;
            }
        }
        $data = AdminListing::create(Applicant::class)->processRequestAndGet(

            $request,

            ['id', 'names',  'last_names', 'government_id', 'city_id', 'monthly_income', 'construction_entity', 'financial_entity', 'created_at', 'atc_fec_asig', 'familiar_setting_id'],
            ['resolution_number'],

            function ($query) use ($idApplicant, $entity, $radio, $config) {

                $query->whereNull('applicants.parent_applicant');
                $query->with('applicantGetCellphone', 'city');
                // $query->whereRaw('DATE_PART("day", now() - applicants.atc_fec_asig)');


                //$query->whereBetween('applicants.created_at', [$dateto . ' 00:00:00', $datefrom . ' 23:59:59']);
                if ($radio == 1) { //por ATCs
                    $query->where('construction_entity', $entity);
                    $query->whereIn('applicants.id', $idApplicant);
                }
                if ($radio == 2) { //por Coops
                    $query->where('financial_entity', $entity);
                    $query->whereIn('applicants.id', $idApplicant);
                }
                if ($radio == 3) { //por municipios
                    $query->where('city_id', $entity);
                    $query->whereIn('applicants.id', $idApplicant);
                }
                if ($radio == 4) { //por Config. Familiar
                    $query->where('familiar_setting_id', $config);
                }

                //$query->paginate(20);
            }
        );

        //return $data;
        if ($request->ajax()) {
            if ($request->has('bulk')) {
                return [
                    'bulkItems' => $data->pluck('id')
                ];
            }
            return ['data' => $data, 'paginator' => ''];
        }

        return view('admin.report.atcByStatus', ['data' => $data, 'statusName' => $statusName, 'ConsEntity' => $ConsEntity, 'coops' => $coops, 'municipios' => $municipios, 'configfamily' => $configfamily]);
    }

    public function applicantsByResolutionSocial(SocialReport $request)
    {

        $data = $request->all();
        //return $data;
        //return $data['params']['dateto'];


        //$resolution = $data['search'] ?? 0;
        $dateto = $data['dateto'] ?? date('Y-m') . '-01';
        $datefrom = $data['datefrom'] ?? date('Y-m-d');
        $status = $data['status'] ?? 0;
        $statuName = Status::all();



        $estados = Applicant::whereNull('applicants.parent_applicant')

            ->get();
        $idApplicant = [];
        foreach ($estados as $value) {

            if ($value->statuses->status_id == $status) {
                $idApplicant[] = $value->statuses->applicant_id;
            }
        }
        //return $status;
        //$date_from = $data['date_from'] ?? '';
        //if()
        // create and AdminListing instance for a specific model and
        $data = AdminListing::create(Applicant::class)->processRequestAndGet(

            $request,

            ['id', 'file_number', 'neighborhood', 'address', 'names', 'last_names', 'government_id',  'marital_status', 'construction_entity', 'financial_entity',   'resolution_number', 'created_at'],
            ['resolution_number'],

            function ($query) use ($dateto, $datefrom, $status, $idApplicant) {

                $query->whereNull('applicants.parent_applicant');
                if ($status != 0) {
                    $query->whereIn('applicants.id', ApplicantStatus::where('status_id', '=', $status)->latest()->pluck('applicant_id')->toArray());
                }
                $query->with('applicantGetCellphone', 'city');
                $query->whereBetween('applicants.created_at', [$dateto . ' 00:00:00', $datefrom . ' 23:59:59']);

                $query->get();
            }

        );

        if ($request->ajax()) {
            return $data;
            if ($request->has('bulk')) {
                return [
                    'bulkItems' => $data->pluck('id')
                ];
            }
            return ['data' => $data, 'dateto' => $dateto, 'datefrom' => $datefrom, 'statusName' => $statuName, 'status' => $status];
        }

        return view('admin.report.social', ['data' => $data, 'dateto' => $dateto, 'datefrom' => $datefrom, 'statusName' => $statuName, 'status' => $status]);
    }
    public function applicantsCoopStatus(SocialReport $request)
    {

        $data = $request->all();
        // return date('Y-m-d');
        if ($request->ajax()) {
            return $request;
        }
        //return $request;

        //$resolution = $data['search'] ?? 0;
        $dateto = $data['date_to'] ?? date('Y-m') . '-01';
        $datefrom = $data['date_from'] ?? date('Y-m-d');
        $send = $request->dispatched ?? '';
        $entity = Auth::user()->construction_entity;
        $typeReport = $data['type_report'];


        $financialEntity = Auth::user()->financial_entity;
        //$date_from = $data['date_from'] ?? '';
        //if()
        // create and AdminListing instance for a specific model and
        $data = AdminListing::create(Applicant::class)->processRequestAndGet(

            $request,

            ['id', 'file_number', 'admission_data', 'applicant_full_name', 'government_id', 'spouse_data', 'marital_status', 'total_monthly_income', 'construction_entity', 'financial_entity', 'centityname', 'fentityname', 'city_name', 'resolution_number'],
            ['resolution_number'],

            function ($query) use ($dateto, $datefrom, $send, $financialEntity, $typeReport, $entity) {
                $start = Carbon::parse($dateto);
                $end = Carbon::parse($datefrom);
                $query
                    ->selectRaw("ROW_NUMBER () OVER (ORDER BY applicants.id) as id,
                    applicants.id as applicantsid,
                    applicants.file_number as file_number,
                    applicants.created_at as admission_data,
                    applicants.names||' '||applicants.last_names as applicant_full_name,
                    applicants.government_id as government_id,
                    (select b.names||' '||b.last_names||'-'||b.government_id from applicants b where b.parent_applicant = applicants.id and b.applicant_relationship = 1 ) as spouse_data,
                    applicants.marital_status as marital_status,
                    applicants.address as address,
                    applicants.neighborhood as neighborhood,
                    -- applicants.financial_entity_eval as dispatched,
                    (CASE
                    WHEN financial_entity_eval = true THEN 'Enviado'
                    WHEN financial_entity_eval = false THEN 'Pendiente'
                    END ) as dispatched,
                    -- applicants.financial_entity_eval as dispatched,
                    (CASE
                    WHEN construction_entity_eval = true THEN 'Enviado'
                    WHEN construction_entity_eval = false THEN 'Pendiente'
                    END ) as dispatched2,
                    (select ap.description from applicant_statuses ap where ap.applicant_id= applicants.id and  ap.status_id=1 ) as description,
                    coalesce((select sum(beneficiaries.monthly_income) from applicants as beneficiaries
                    inner join applicant_relationships on beneficiaries.applicant_relationship = applicant_relationships.id
                    where beneficiaries.parent_applicant = applicants.id and compute_income = true ) + coalesce(applicants.monthly_income,0),0) as total_monthly_income,
                    construction_entity,
                    financial_entity,
                    construction_entities.name as centityname,
                    financial_entities.name as fentityname,
                    cities.name as city_name,
                    applicants.resolution_number
                    ")
                    ->leftJoin('cities', 'applicants.city_id', '=', 'cities.id')
                    ->leftJoin('construction_entities', 'applicants.construction_entity', '=', 'construction_entities.id')
                    ->leftJoin('financial_entities', 'applicants.financial_entity', '=', 'financial_entities.id');
                //->whereNull('applicants.parent_applicant');



                if ($typeReport == 'coop') {
                    $query
                        ->where('applicants.financial_entity', '=', $financialEntity);
                    if ($send) {
                        $query->where('applicants.financial_entity_eval', $send);
                    }
                }
                if ($typeReport == 'atc') {
                    $query
                        ->where('applicants.construction_entity', '=', $entity)
                        ->whereNotNull('applicants.file_number')
                        ->whereNotNull('applicants.resolution_number');
                    if ($send) {
                        $query->where('applicants.construction_entity_eval', $send);
                    }
                }

                if (isset($data['date_to'])) {
                    $query->whereBetween('applicants.created_at', [$dateto . ' 00:00:00', $datefrom . ' 23:59:59']);
                } else {
                    $query->whereBetween('applicants.created_at', [$start->format('Y-m-d') . ' 00:00:00', $end->format('Y-m-d') . ' 23:59:59']);
                }
            }

        );

        if ($request->ajax()) {
            //return $request->all();
            return ['data' => $data, 'date_to' => $dateto, 'date_from' => $datefrom];
        }


        return view('admin.report.coop-status', ['data' => $data, 'date_to' => Carbon::parse($dateto)->format('d-m-Y'), 'date_from' => Carbon::parse($datefrom)->format('d-m-Y')]);
    }
    public function searchci($government_id = '', SocialReport $request, $statuName = '')
    {
        $data = $request->all();
        //$txtci = $government_id ?? 0;
        $data = AdminListing::create(Applicant::class)->processRequestAndGet(
            // pass the request with params
            $request,

            // set columns to query
            ['id', 'names',  'last_names', 'address', 'government_id', 'city_id', 'monthly_income', 'construction_entity', 'financial_entity', 'created_at', 'registration_date'],

            // set columns to searchIn
            ['id', 'names', 'last_names', 'government_id', 'city_id'],
            function ($query) use ($government_id) {

                $query->where('government_id', $government_id);
                $query->whereNull('applicants.parent_applicant');
                $query->paginate(24);
            }
        );
        //return $data;
        if ($request->ajax()) {
            if ($request->has('bulk')) {
                return [
                    'bulkItems' => $data->pluck('id')
                ];
            }
            return ['items' => $data];
        }

        return view('admin.report.searchci', ['items' => $data]);
    }
    public function verificacionInstitucional($government_id = '', SocialReport $request, $statuName = '')
    {
        $vinSoli = [];
        if ($request->ajax()) {
            $client = new Client();

            $url = "http://192.168.98.192:8000/api/getexpe/" . $government_id;
            //$url = "http://192.168.98.192:8000/api/getdataexpe/" . $government_id;
            $res = $client->request('GET', $url, [
                'connect_timeout' => 10,
                'http_errors' => true,
                'headers' => ['Content-Type' => 'application/json', 'Accept' => 'application/json'],
            ]);
            //return $res;

            if ($res->getStatusCode() == 200) {
                $json = json_decode($res->getBody(), true);
                //return $json;
                $ama = Applicant::whereNull('applicants.parent_applicant')->where('government_id', '=', "$government_id")->get();
                $amaconyugue = Applicant::whereNotNull('applicants.parent_applicant')->where('government_id', '=', "$government_id")->get();
                $vinSoli['postulante'] = $json;
                $vinSoli['ama'] = $ama->count() > 0 ? '* Se encuentra en la Base de Datos AMA como Postulante' : '';
                $vinSoli['amaconyugue'] = $amaconyugue->count() > 0 ? '* Se encuentra En grupo Familiar en Base de Datos AMA' : '';
            }
            return $vinSoli;
        }


        return view('admin.report.verificacion', ['items' => json_encode($vinSoli)]);
    }



    public function listejec(Request $request, $year = '')
    {

        // $data = Applicant::whereMonth('file_date', 9)->count()->get();
        $YY = ($year == '') ? 2019 : $year;



        $var[2019]['09'] = "12";
        $var[2019]['10'] = "12";
        $var[2019]['11'] = "12";
        $var[2019]['12'] = "12";
        //160 total

        $var[2020]['01']  = "24";
        $var[2020]['02']  = "24";
        $var[2020]['03']  = "24";
        $var[2020]['04']  = "24";
        $var[2020]['05']  = "24";
        $var[2020]['06']  = "24";
        $var[2020]['07']  = "24";
        $var[2020]['08']  = "24";
        $var[2020]['09']  = "24";
        $var[2020]['10']  = "24";
        $var[2020]['11']  = "24";
        $var[2020]['12']  = "27";
        //

        $var[2021]['01']  = "30";
        $var[2021]['02']  = "30";
        $var[2021]['03']  = "30";
        $var[2021]['04']  = "30";
        $var[2021]['05']  = "30";
        $var[2021]['06']  = "30";
        $var[2021]['07']  = "30";
        $var[2021]['08']  = "30";
        $var[2021]['09']  = "30";
        $var[2021]['10']  = "30";
        $var[2021]['11']  = "30";
        $var[2021]['12']  = "32";

        $var[2022]['01']  = "0";
        $var[2022]['02']  = "30";
        $var[2022]['03']  = "45";
        $var[2022]['04']  = "45";
        $var[2022]['05']  = "55";
        $var[2022]['06']  = "65";
        $var[2022]['07']  = "75";
        $var[2022]['08']  = "75";
        $var[2022]['09']  = "75";
        $var[2022]['10']  = "75";
        $var[2022]['11']  = "85";
        $var[2022]['12']  = "85";

        $var[2023]['01']  = "60";
        $var[2023]['02']  = "65";
        $var[2023]['03']  = "80";
        $var[2023]['04']  = "80";
        $var[2023]['05']  = "85";
        $var[2023]['06']  = "85";
        $var[2023]['07']  = "85";
        $var[2023]['08']  = "0";
        $var[2023]['09']  = "0";
        $var[2023]['10']  = "0";
        $var[2023]['11']  = "0";
        $var[2023]['12']  = "0";

        $var[2024]['01']  = "0";
        $var[2024]['02']  = "0";
        $var[2024]['03']  = "0";
        $var[2024]['04']  = "0";
        $var[2024]['05']  = "0";
        $var[2024]['06']  = "0";
        $var[2024]['07']  = "0";
        $var[2024]['08']  = "0";
        $var[2024]['09']  = "0";
        $var[2024]['10']  = "0";
        $var[2024]['11']  = "0";
        $var[2024]['12']  = "0";




        $data = Applicant::select(
            DB::raw('count(*) as total'),
            DB::raw("to_char(resolution_date::date,'MM' ) as months"),
            //DB::raw("to_char(file_date::date,'Month' ) as linetime"),
            DB::raw("CASE
        when trim(to_char(resolution_date::date,'Month' ) ) = 'January' then 'ENERO'
        when trim(to_char(resolution_date::date,'Month' ) ) = 'February' then 'FEBRERO'
        when trim(to_char(resolution_date::date,'Month' ) ) = 'March' then 'MARZO'
        when trim(to_char(resolution_date::date,'Month' ) ) = 'April' then 'ABRIL'
        when trim(to_char(resolution_date::date,'Month' ) ) = 'May' then 'MAYO'
        when trim(to_char(resolution_date::date,'Month' ) ) = 'June' then 'JUNIO'
        when trim(to_char(resolution_date::date,'Month' ) ) = 'July' then 'JULIO'
        when trim(to_char(resolution_date::date,'Month' ) ) = 'August' then 'AGOSTO'
        when trim(to_char(resolution_date::date,'Month' ) ) = 'September' then 'SETIEMBRE'
        when trim(to_char(resolution_date::date,'Month' ) ) = 'October' then 'OCTUBRE'
        when trim(to_char(resolution_date::date,'Month' ) ) = 'November' then 'NOVIEMBRE'
        when trim(to_char(resolution_date::date,'Month' ) ) = 'December' then 'DICIEMBRE'
        END AS  linetime")
        )

            ->whereYear('resolution_date', $YY)
            ->groupBy('months', 'linetime')
            ->orderBy('months', 'ASC')
            ->get();
        $arrayData = [];
        $i = 1;
        //return $var['2022'];

        foreach ($data->toArray() as $item) {

            $itemsMonths = $item['months'];
            $arrayData[] = [
                'total' => $item['total'],
                'obj' => $var[$YY]["$itemsMonths"],
                'linetime' => $item['linetime']

            ];
            $i++;
        }

        //dd($arrayData);

        $dataset = $data->pluck('total');
        $labels = $data->pluck('linetime');

        $collection = collect($var[$YY]);
        $totalProyeccion = $collection->values()->sum();

        $totalAdj = $dataset->sum();
        //return $totalAdj;
        $collection2 = collect();
        if ($YY == '2022') {
            unset($var['2022']['01']);
            unset($var['2022']['02']);
            $collection2 = collect($var[$YY]);
            //eturn $collection2;
            //return $collection->values();
        } else {
            $collection = collect($var[$YY]);
        }

        if ($request->ajax()) {
            if ($request->has('bulk')) {
                return [
                    'bulkItems' => $data->pluck('id')
                ];
            }
            return ['data' => $arrayData, 'labels' => $labels, 'dataset' => $dataset, 'dataObj' => ($YY == '2022' ? $collection2->values() : $collection->values()), 'totalAdj' => $totalAdj, 'totalProyeccion' => $totalProyeccion];
        }

        return view('admin.report.list-ejecucion', ['data' => $arrayData, 'yy' => $YY,  'dataObj' => $collection->values(), 'totalAdj' => $totalAdj]);
    }

    public function asignacionesAtc(Request $request)
    {
        //dd(phpinfo());
        //ATC count
        $atcCount = ConstructionEntity::groupBy('construction_entities.name')
            ->selectRaw('construction_entities.name, COUNT(*) as total')
            ->leftJoin('applicants', 'applicants.construction_entity', '=', 'construction_entities.id')->pluck('total', 'construction_entities.name');

        $atcCountChart = new AmaChartStatus;
        $atcCountChart->labels($atcCount->keys());
        for ($i = 0; $i <= count($atcCount); $i++) {
            $colours[] = '#' . substr(str_shuffle('ABCDEF0123456789'), 0, 6);
        }
        $atcCountChart->dataset('Atc Asignados', 'bar', $atcCount->values())
            ->backgroundColor($colours);
        //return $atcCount;


        if ($request->ajax()) {
            /*  if ($request->has('bulk')) {
                 return [
                     'bulkItems' => $data->pluck('id')
                 ];
             } */
            /* return [   'coops' => json_decode($coops)]; */
        }

        return view('admin.report.asignaciones_atc', ['atcCountChart' => $atcCountChart,]);
    }
}
