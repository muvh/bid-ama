<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\ApplicantRecord\BulkDestroyApplicantRecord;
use App\Http\Requests\Admin\ApplicantRecord\DestroyApplicantRecord;
use App\Http\Requests\Admin\ApplicantRecord\IndexApplicantRecord;
use App\Http\Requests\Admin\ApplicantRecord\StoreApplicantRecord;
use App\Http\Requests\Admin\ApplicantRecord\UpdateApplicantRecord;
use App\Models\ApplicantRecord;
use App\Models\AccountingRecord;
use Brackets\AdminListing\Facades\AdminListing;
use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;

class ApplicantRecordsController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param IndexApplicantRecord $request
     * @return array|Factory|View
     */
    public function index(IndexApplicantRecord $request)
    {
        // create and AdminListing instance for a specific model and
        $data = AdminListing::create(ApplicantRecord::class)->processRequestAndGet(
            // pass the request with params
            $request,

            // set columns to query
            ['id', 'accounting_record_id', 'applicant_id', 'amount'],

            // set columns to searchIn
            ['id']
        );

        if ($request->ajax()) {
            if ($request->has('bulk')) {
                return [
                    'bulkItems' => $data->pluck('id')
                ];
            }
            return ['data' => $data];
        }

        return view('admin.applicant-record.index', ['data' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @throws AuthorizationException
     * @return Factory|View
     */
    public function create(AccountingRecord $accountingRecord)
    {
        $this->authorize('admin.applicant-record.create');

        return view('admin.applicant-record.create', compact('accountingRecord'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreApplicantRecord $request
     * @return array|RedirectResponse|Redirector
     */
    public function store(StoreApplicantRecord $request)
    {
        // Sanitize input
        $sanitized = $request->getSanitized();
        //return $sanitized;


        $summary = AccountingRecord::find($sanitized['accounting_record_id']);
        //return $summary;
        $balance = $summary['balance'] + $sanitized['amount'];
        //return $balance;
        $applicantRecord = ApplicantRecord::create($sanitized);
        //AccountingRecord::where('id', $sanitized['accounting_record_id'])->update(['balance' => $balance]);


        if ($request->ajax()) {
            return ['redirect' => url('admin/accounting-records/' . $sanitized['accounting_record_id'] . '/show'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
        }

        return redirect('admin/applicant-records');
    }

    /**
     * Display the specified resource.
     *
     * @param ApplicantRecord $applicantRecord
     * @throws AuthorizationException
     * @return void
     */
    public function show(ApplicantRecord $applicantRecord)
    {
        $this->authorize('admin.applicant-record.show', $applicantRecord);

        // TODO your code goes here
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param ApplicantRecord $applicantRecord
     * @throws AuthorizationException
     * @return Factory|View
     */
    public function edit(ApplicantRecord $applicantRecord)
    {
        $this->authorize('admin.applicant-record.edit', $applicantRecord);


        return view('admin.applicant-record.edit', [
            'applicantRecord' => $applicantRecord,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateApplicantRecord $request
     * @param ApplicantRecord $applicantRecord
     * @return array|RedirectResponse|Redirector
     */
    public function update(UpdateApplicantRecord $request, ApplicantRecord $applicantRecord)
    {
        // Sanitize input
        $sanitized = $request->getSanitized();

        // Update changed values ApplicantRecord
        $applicantRecord->update($sanitized);

        if ($request->ajax()) {
            return [
                'redirect' => url('admin/applicant-records'),
                'message' => trans('brackets/admin-ui::admin.operation.succeeded'),
            ];
        }

        return redirect('admin/applicant-records');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param DestroyApplicantRecord $request
     * @param ApplicantRecord $applicantRecord
     * @throws Exception
     * @return ResponseFactory|RedirectResponse|Response
     */
    public function destroy(DestroyApplicantRecord $request, ApplicantRecord $applicantRecord)
    {




        $applicantRecord->delete();
        $total = AccountingRecord::find($applicantRecord['accounting_record_id']);
        return $total->beneficiaries->sum('amount');

        if ($request->ajax()) {
            return response(['data' => $total]);
        }

        return redirect()->back();
    }

    /**
     * Remove the specified resources from storage.
     *
     * @param BulkDestroyApplicantRecord $request
     * @throws Exception
     * @return Response|bool
     */
    public function bulkDestroy(BulkDestroyApplicantRecord $request): Response
    {
        DB::transaction(static function () use ($request) {
            collect($request->data['ids'])
                ->chunk(1000)
                ->each(static function ($bulkChunk) {
                    ApplicantRecord::whereIn('id', $bulkChunk)->delete();

                    // TODO your code goes here
                });
        });

        return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
    }
}
