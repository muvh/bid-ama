<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\WorksDatum\BulkDestroyWorksDatum;
use App\Http\Requests\Admin\WorksDatum\DestroyWorksDatum;
use App\Http\Requests\Admin\WorksDatum\IndexWorksDatum;
use App\Http\Requests\Admin\WorksDatum\StoreWorksDatum;
use App\Http\Requests\Admin\WorksDatum\UpdateWorksDatum;
use App\Models\WorksDatum;
use App\Models\Applicant;
use Brackets\AdminListing\Facades\AdminListing;
use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;

class WorksDataController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param IndexWorksDatum $request
     * @return array|Factory|View
     */
    public function index(IndexWorksDatum $request)
    {
        // create and AdminListing instance for a specific model and
        /* $data = AdminListing::create(WorksDatum::class)->processRequestAndGet(
            // pass the request with params
            $request,

            // set columns to query
            ['id', 'applicant_id', 'start_work', 'latitude', 'longitude'],

            // set columns to searchIn
            ['id', 'latitude', 'longitude']
        );

        if ($request->ajax()) {
            if ($request->has('bulk')) {
                return [
                    'bulkItems' => $data->pluck('id')
                ];
            }
            return ['data' => $data];
        }*/
        //$data = array("position" => array("lat" => 10.0, "lng" => 10.0), "position" => array("lat" => 11.0, "lng" => 1.0);

        //return WorksDatum::all();

        /*{
                    position: {
                    lat: 10.0,
                    lng: 10.0
                    }
                }, {
                    position: {
                    lat: 11.0,
                    lng: 11.0
                    }
                }*/

        return view('admin.works-datum.index');
    }

    public function indexapi()
    {
        return WorksDatum::all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @throws AuthorizationException
     * @return Factory|View
     */
    public function create(Applicant $applicant)
    {
        $this->authorize('admin.works-datum.create');

        return view('admin.works-datum.create', compact('applicant'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreWorksDatum $request
     * @return array|RedirectResponse|Redirector
     */
    public function store(StoreWorksDatum $request)
    {
        // Sanitize input
        $sanitized = $request->getSanitized();

        // Store the WorksDatum
        $worksDatum = WorksDatum::create($sanitized);

        if ($request->ajax()) {
            return ['redirect' => url('admin/applicants/' . $sanitized['applicant_id'] . '/showvisit'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
        }

        return redirect('admin/works-data');
    }

    /**
     * Display the specified resource.
     *
     * @param WorksDatum $worksDatum
     * @throws AuthorizationException
     * @return void
     */
    public function show(WorksDatum $worksDatum)
    {
        $this->authorize('admin.works-datum.show', $worksDatum);

        // TODO your code goes here
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param WorksDatum $worksDatum
     * @throws AuthorizationException
     * @return Factory|View
     */
    public function edit(WorksDatum $worksDatum)
    {
        $this->authorize('admin.works-datum.edit', $worksDatum);


        return view('admin.works-datum.edit', [
            'worksDatum' => $worksDatum,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateWorksDatum $request
     * @param WorksDatum $worksDatum
     * @return array|RedirectResponse|Redirector
     */
    public function update(UpdateWorksDatum $request, WorksDatum $worksDatum)
    {
        // Sanitize input
        $sanitized = $request->getSanitized();

        // Update changed values WorksDatum
        $worksDatum->update($sanitized);

        if ($request->ajax()) {
            return [
                'redirect' => url('admin/applicants/' . $sanitized['applicant_id'] . '/showvisit'),
                'message' => trans('brackets/admin-ui::admin.operation.succeeded'),
            ];
        }

        return redirect('admin/works-data');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param DestroyWorksDatum $request
     * @param WorksDatum $worksDatum
     * @throws Exception
     * @return ResponseFactory|RedirectResponse|Response
     */
    public function destroy(DestroyWorksDatum $request, WorksDatum $worksDatum)
    {
        $worksDatum->delete();

        if ($request->ajax()) {
            return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
        }

        return redirect()->back();
    }

    /**
     * Remove the specified resources from storage.
     *
     * @param BulkDestroyWorksDatum $request
     * @throws Exception
     * @return Response|bool
     */
    public function bulkDestroy(BulkDestroyWorksDatum $request): Response
    {
        DB::transaction(static function () use ($request) {
            collect($request->data['ids'])
                ->chunk(1000)
                ->each(static function ($bulkChunk) {
                    WorksDatum::whereIn('id', $bulkChunk)->delete();

                    // TODO your code goes here
                });
        });

        return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
    }
}
