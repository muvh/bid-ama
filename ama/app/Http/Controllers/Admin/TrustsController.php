<?php

namespace App\Http\Controllers\Admin;

use Exception;
use App\Models\Trust;
use App\Models\Applicant;
use Illuminate\View\View;
use Illuminate\Http\Response;
use App\Models\ConstructionEntity;
use App\Models\FinancialEntity;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Http\RedirectResponse;
use Illuminate\Contracts\View\Factory;
use App\Http\Requests\Admin\Trust\IndexTrust;
use App\Http\Requests\Admin\Trust\StoreTrust;
use Symfony\Component\HttpFoundation\Request;
use App\Http\Requests\Admin\Trust\UpdateTrust;
use App\Http\Requests\Admin\Trust\DestroyTrust;
use Brackets\AdminListing\Facades\AdminListing;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Auth\Access\AuthorizationException;
use App\Http\Requests\Admin\Trust\BulkDestroyTrust;
use App\Models\ApplicantsTrust;
use Luecano\NumeroALetras\NumeroALetras;

class TrustsController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param IndexTrust $request
     * @return array|Factory|View
     */
    public function index(IndexTrust $request)
    {
        // create and AdminListing instance for a specific model and
        $data = AdminListing::create(Trust::class)->processRequestAndGet(
            // pass the request with params
            $request,

            // set columns to query
            ['id', 'nro_tr', 'fecha_tr', 'financial_entity_id','culminado'],

            // set columns to searchIn
            ['id', 'nro_tr']
        );

        if ($request->ajax()) {
            if ($request->has('bulk')) {
                return [
                    'bulkItems' => $data->pluck('id')
                ];
            }
            return ['data' => $data];
        }

        return view('admin.trust.index', ['data' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @throws AuthorizationException
     * @return Factory|View
     */
    public function create()
    {
        $this->authorize('admin.trust.create');
        $financialEntity= FinancialEntity::all();

        return view('admin.trust.create', ['financial'=>$financialEntity]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreTrust $request
     * @return array|RedirectResponse|Redirector
     */
    public function store(StoreTrust $request)
    {
        //return $request;
        // Sanitize input
        $existe=Trust::where('nro_tr', $request['nro_tr'])->get();
        if($existe->count()>0){
            if ($request->ajax()) {
                return ['notify' => ['type' => 'success', 'title' => 'Atención', 'message' => 'El Nro de Solicitid ya se encuentra. Verificar.. No puede repetirse.']];
                //exit;
            }
        }

        $sanitized = $request->getSanitized();
        
        $sanitized['financial_entity_id'] = $request->getFinancialEntity();
        // Store the Trust
        //return $sanitized;
        $trust = Trust::create($sanitized);

        if ($request->ajax()) {
            return ['redirect' => url('admin/trusts/'.$trust->id.'/edit'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
        }

        return redirect('admin/trusts');
    }

    /**
     * Display the specified resource.
     *
     * @param Trust $trust
     * @throws AuthorizationException
     * @return void
     */
    public function show(Trust $trust)
    {
        $this->authorize('admin.trust.show', $trust);

        // TODO your code goes here
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Trust $trust
     * @throws AuthorizationException
     * @return Factory|View
     */
    public function showfideicomiso(Trust $trust){
        //require 'vendor/autoload.php';
        $applicant=ApplicantsTrust::where('trust_id',$trust->id)->get();
        $mount= $applicant->count()*34000000;

        $number=$mount;
        $decimals=2;
        $formatter = new NumeroALetras();
        $numeroTexto=  $formatter->toWords($number, $decimals);
       // return  NumeroALetras::convertir(4599.99, 'soles') .'<br>';
        $trust->culminado=true;
        $trust->save();

        $pdf = \PDF::LoadView('admin.trust.showfideicomiso', [
            'trust' => $trust,
            'numeroTexto'=>$numeroTexto,
            'applicant'=> $applicant,
            'mount'=> $mount
        ]);
           //$pdf->loadHTML('<h1>Test</h1>');
           return $pdf->stream();

        //return view('admin.trust.showfideicomiso', compact('trust'));
    }

    public function edit(Trust $trust)
    {
        $this->authorize('admin.trust.edit', $trust);
        $data=ApplicantsTrust::where('trust_id', $trust->id)
        ->get();
        //return $data;
        return view('admin.trust.edit', [
            'trust' => $trust,
            'ApplicantsTrust' => $data,
        ]);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param UpdateTrust $request
     * @param Trust $trust
     * @return array|RedirectResponse|Redirector
     */
    public function update(UpdateTrust $request, Trust $trust)
    {
        // Sanitize input
        $sanitized = $request->getSanitized();

        // Update changed values Trust
        $trust->update($sanitized);

        if ($request->ajax()) {
            return [
                'redirect' => url('admin/trusts'),
                'message' => trans('brackets/admin-ui::admin.operation.succeeded'),
            ];
        }

        return redirect('admin/trusts');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param DestroyTrust $request
     * @param Trust $trust
     * @throws Exception
     * @return ResponseFactory|RedirectResponse|Response
     */
    public function destroy(DestroyTrust $request, Trust $trust)
    {
        $trust->delete();

        if ($request->ajax()) {
            return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
        }

        return redirect()->back();
    }

    /**
     * Remove the specified resources from storage.
     *
     * @param BulkDestroyTrust $request
     * @throws Exception
     * @return Response|bool
     */
    public function findApplicant($government_id)
    {
        $data=Applicant::whereNull('applicants.parent_applicant')
        ->where('government_id', $government_id)
        ->get();
        //return $data->count();
        $trust=ApplicantsTrust::where('applicant_id', $data[0]->id)->get();
        //return $trust->count();

        if($data->count()>0 ){  //si hay datos retorna
            
        return [ 
            'data'=>$data[0],
            'trust'=>$trust->count(),
            'cod'=>$trust->count()>0?2:1,
        ];
    }else{
        return response()->json([
            'cod' => 0,
            'data' => "",
            'trust'=>$trust->count(),
            
        ]);

    }
    }

    public function addApplicant(Trust $trust, Request $request)
    {
        $data = $request->all();
        //return $request;
        $existe=ApplicantsTrust::where('applicant_id', $request['applicant_id'])->get();
        if($existe->count()>0){
            if ($request->ajax()) {
                return ['notify' => ['type' => 'success', 'title' => 'Atención', 'message' => 'El soliciante ya fue cargado.']];
                //exit;
            }
        }

        $trustsave = ApplicantsTrust::create([
            'trust_id' => $request['trust_id'],
            'applicant_id' => $request['applicant_id']
        ]);
        if($trustsave){
             if ($request->ajax()) {
                //return $sanitized;
                return ['redirect' => url('admin/trusts/'.$request['trust_id'].'/edit'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
            } 
        }
        return redirect('admin/trusts/'.$request['trust_id'].'/edit');
        //return ['redirect' => url('admin/trusts/'.$request['trust_id'].'/edit'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
    }
    
    public function findlistado($id)
    {
        //return $id;
    $data=ApplicantsTrust::where('trust_id', $id)
    ->get();

    //return $data;

    if($data->count()>0){  //si hay datos retorna
        
        return [ 
            'data'=>$data[0],
            'cod'=>1
        ];
    }else{
        return response()->json([
            'cod' => 0,
            'data' => "",
            
        ]);

    }
  
    }

    public function bulkDestroy(BulkDestroyTrust $request) : Response
    {
        DB::transaction(static function () use ($request) {
            collect($request->data['ids'])
                ->chunk(1000)
                ->each(static function ($bulkChunk) {
                    Trust::whereIn('id', $bulkChunk)->delete();

                    // TODO your code goes here
                });
        });

        return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
    }
}
