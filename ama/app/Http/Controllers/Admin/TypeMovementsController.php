<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\TypeMovement\BulkDestroyTypeMovement;
use App\Http\Requests\Admin\TypeMovement\DestroyTypeMovement;
use App\Http\Requests\Admin\TypeMovement\IndexTypeMovement;
use App\Http\Requests\Admin\TypeMovement\StoreTypeMovement;
use App\Http\Requests\Admin\TypeMovement\UpdateTypeMovement;
use App\Models\TypeMovement;
use Brackets\AdminListing\Facades\AdminListing;
use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;

class TypeMovementsController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param IndexTypeMovement $request
     * @return array|Factory|View
     */
    public function index(IndexTypeMovement $request)
    {
        // create and AdminListing instance for a specific model and
        $data = AdminListing::create(TypeMovement::class)->processRequestAndGet(
            // pass the request with params
            $request,

            // set columns to query
            ['id', 'name'],

            // set columns to searchIn
            ['id', 'name']
        );

        if ($request->ajax()) {
            if ($request->has('bulk')) {
                return [
                    'bulkItems' => $data->pluck('id')
                ];
            }
            return ['data' => $data];
        }

        return view('admin.type-movement.index', ['data' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @throws AuthorizationException
     * @return Factory|View
     */
    public function create()
    {
        $this->authorize('admin.type-movement.create');

        return view('admin.type-movement.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreTypeMovement $request
     * @return array|RedirectResponse|Redirector
     */
    public function store(StoreTypeMovement $request)
    {
        // Sanitize input
        $sanitized = $request->getSanitized();

        // Store the TypeMovement
        $typeMovement = TypeMovement::create($sanitized);

        if ($request->ajax()) {
            return ['redirect' => url('admin/type-movements'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
        }

        return redirect('admin/type-movements');
    }

    /**
     * Display the specified resource.
     *
     * @param TypeMovement $typeMovement
     * @throws AuthorizationException
     * @return void
     */
    public function show(TypeMovement $typeMovement)
    {
        $this->authorize('admin.type-movement.show', $typeMovement);

        // TODO your code goes here
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param TypeMovement $typeMovement
     * @throws AuthorizationException
     * @return Factory|View
     */
    public function edit(TypeMovement $typeMovement)
    {
        $this->authorize('admin.type-movement.edit', $typeMovement);


        return view('admin.type-movement.edit', [
            'typeMovement' => $typeMovement,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateTypeMovement $request
     * @param TypeMovement $typeMovement
     * @return array|RedirectResponse|Redirector
     */
    public function update(UpdateTypeMovement $request, TypeMovement $typeMovement)
    {
        // Sanitize input
        $sanitized = $request->getSanitized();

        // Update changed values TypeMovement
        $typeMovement->update($sanitized);

        if ($request->ajax()) {
            return [
                'redirect' => url('admin/type-movements'),
                'message' => trans('brackets/admin-ui::admin.operation.succeeded'),
            ];
        }

        return redirect('admin/type-movements');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param DestroyTypeMovement $request
     * @param TypeMovement $typeMovement
     * @throws Exception
     * @return ResponseFactory|RedirectResponse|Response
     */
    public function destroy(DestroyTypeMovement $request, TypeMovement $typeMovement)
    {
        $typeMovement->delete();

        if ($request->ajax()) {
            return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
        }

        return redirect()->back();
    }

    /**
     * Remove the specified resources from storage.
     *
     * @param BulkDestroyTypeMovement $request
     * @throws Exception
     * @return Response|bool
     */
    public function bulkDestroy(BulkDestroyTypeMovement $request) : Response
    {
        DB::transaction(static function () use ($request) {
            collect($request->data['ids'])
                ->chunk(1000)
                ->each(static function ($bulkChunk) {
                    TypeMovement::whereIn('id', $bulkChunk)->delete();

                    // TODO your code goes here
                });
        });

        return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
    }
}
