<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Visit\BulkDestroyVisit;
use App\Http\Requests\Admin\Visit\DestroyVisit;
use App\Http\Requests\Admin\Visit\IndexVisit;
use App\Http\Requests\Admin\Visit\StoreVisit;
use App\Http\Requests\Admin\Visit\UpdateVisit;
use App\Models\Visit;
use App\Models\Applicant;
use Brackets\AdminListing\Facades\AdminListing;
use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;

class VisitsController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param IndexVisit $request
     * @return array|Factory|View
     */
    public function index(IndexVisit $request)
    {
        // create and AdminListing instance for a specific model and
        $data = AdminListing::create(Applicant::class)->processRequestAndGet(
            // pass the request with params
            $request,

            // set columns to query
            //['id', 'applicant_id', 'visit_number', 'advance', 'latitude', 'longitude', 'visit_date'],
            ['id', 'names',  'last_names', 'government_id', 'city_id', 'monthly_income', 'construction_entity', 'financial_entity', 'created_at', 'atc_fec_asig'],

            // set columns to searchIn
            //['id', 'visit_number', 'advance', 'latitude', 'longitude']
            ['id', 'names', 'last_names', 'government_id', 'city_id'],
        );

        if ($request->ajax()) {
            if ($request->has('bulk')) {
                return [
                    'bulkItems' => $data->pluck('id')
                ];
            }
            return ['data' => $data];
        }

        return view('admin.visit.index', ['data' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @throws AuthorizationException
     * @return Factory|View
     */
    public function create()
    {
        $this->authorize('admin.visit.create');

        return view('admin.visit.create');
    }

    public function createvisit(Applicant $applicant)
    {
        //$this->authorize('admin.visit.create');
        //return 'create visita';
        return view('admin.visit.create', compact('applicant'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreVisit $request
     * @return array|RedirectResponse|Redirector
     */
    public function store(StoreVisit $request)
    {
        // Sanitize input
        //return $request;

        $sanitized = $request->getSanitized();

        // return $sanitized;

        // Store the Visit
        $visit = Visit::create($sanitized);

        if ($request->ajax()) {
            return ['redirect' => url('admin/applicants/' . $sanitized['applicant_id'] . '/showvisit'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
        }

        return redirect('admin/visits');
    }

    /**
     * Display the specified resource.
     *
     * @param Visit $visit
     * @throws AuthorizationException
     * @return void
     */
    public function show(Applicant $applicant, IndexVisit $request)
    {
        //$this->authorize('admin.visit.show', $applicant);
        $projectid = $applicant->id;

        $visitas =  Visit::where('applicant_id', $applicant->id)->orderBy('created_at')->get();
        $avances = $applicant->visits->pluck('advance');

        if ($applicant->visits->count() > 0) {
            $latlong = $applicant->visits;
            $latitude = $latlong[0]['latitude'];
            $longitude = $latlong[0]['longitude'];
            $aux = Visit::where('applicant_id', $applicant->id)
                //->select('advance')
                ->orderBy('id', 'desc')->first();
            $avance = $aux['advance'];
        } else {
            $latitude = '-25.2949068';
            $longitude = '-57.6087548';
            $avance  = 0;
        }

        //return $latlong[0]['latitude'];

        $data = AdminListing::create(Visit::class)->processRequestAndGet(
            // pass the request with params
            $request,

            // set columns to query
            ['id', 'applicant_id', 'visit_number', 'advance', 'visit_date'],

            // set columns to searchIn
            ['id', 'visit_number', 'advance'],
            function ($query) use ($projectid) {
                $query
                    ->where('visits.applicant_id', '=', $projectid)
                    ->orderByRaw('CAST(visit_number AS INTEGER)');
                //->orderBy('visit_number');
            }
        );

        //return 'show';
        return view('admin.visit.show', compact('applicant', 'visitas', 'avances', 'data', 'latitude', 'longitude', 'avance'));

        // TODO your code goes here
    }

    public function showVisit(Visit $visit)
    {
        //$this->authorize('admin.visit.show', $visit);
        //return $visit->getMedia('visit_gallery');
        $datos = [];
        $applicant = Applicant::find($visit->applicant_id);




        foreach ($visit->getMedia('visit_gallery') as $key => $value) {
            array_push($datos, 'storage/' . $value->getUrl());
        }

        //return $datos;
        // TODO your code goes here
        return view('admin.visit.showvisit', compact('datos', 'visit', 'applicant'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Visit $visit
     * @throws AuthorizationException
     * @return Factory|View
     */
    public function edit(Visit $visit)
    {
        $this->authorize('admin.visit.edit', $visit);


        return view('admin.visit.edit', [
            'visit' => $visit,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateVisit $request
     * @param Visit $visit
     * @return array|RedirectResponse|Redirector
     */
    public function update(UpdateVisit $request, Visit $visit)
    {
        // Sanitize input
        $sanitized = $request->getSanitized();

        // Update changed values Visit
        $visit->update($sanitized);

        if ($request->ajax()) {
            return [
                'redirect' => url('admin/applicants/' . $sanitized['applicant_id'] . '/showvisit'),
                'message' => trans('brackets/admin-ui::admin.operation.succeeded'),
            ];
        }

        return redirect('admin/applicants/' . $sanitized['applicant_id'] . '/showvisit');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param DestroyVisit $request
     * @param Visit $visit
     * @throws Exception
     * @return ResponseFactory|RedirectResponse|Response
     */
    public function destroy(DestroyVisit $request, Visit $visit)
    {
        $visit->delete();

        if ($request->ajax()) {
            return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
        }

        return redirect()->back();
    }

    /**
     * Remove the specified resources from storage.
     *
     * @param BulkDestroyVisit $request
     * @throws Exception
     * @return Response|bool
     */
    public function bulkDestroy(BulkDestroyVisit $request): Response
    {
        DB::transaction(static function () use ($request) {
            collect($request->data['ids'])
                ->chunk(1000)
                ->each(static function ($bulkChunk) {
                    Visit::whereIn('id', $bulkChunk)->delete();

                    // TODO your code goes here
                });
        });

        return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
    }
}
