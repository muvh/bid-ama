<?php

namespace App\Http\Controllers\Admin;

use Exception;
use App\Models\Work;
use App\Models\Applicant;
use App\Models\TypeCheck;
use Illuminate\View\View;
use App\Models\Supervisor;
use Illuminate\Http\Response;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Http\RedirectResponse;
use Illuminate\Contracts\View\Factory;
use App\Http\Requests\Admin\Work\IndexWork;
use App\Http\Requests\Admin\Work\StoreWork;
use App\Http\Requests\Admin\Work\UpdateWork;
use App\Http\Requests\Admin\Work\DestroyWork;
use Brackets\AdminListing\Facades\AdminListing;
use App\Http\Requests\Admin\Work\BulkDestroyWork;
use App\Models\WorksTypeCheck;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Auth\Access\AuthorizationException;

class WorksController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param IndexWork $request
     * @return array|Factory|View
     */
    public function index(IndexWork $request)
    {
        // create and AdminListing instance for a specific model and
        $data = AdminListing::create(Work::class)->processRequestAndGet(
            // pass the request with params
            $request,

            // set columns to query
            ['id', 'fecha_ws', 'applicant_id', 'supervisor_id'],

            // set columns to searchIn
            ['id']
        );

        if ($request->ajax()) {
            if ($request->has('bulk')) {
                return [
                    'bulkItems' => $data->pluck('id')
                ];
            }
            return ['data' => $data];
        }

        return view('admin.work.index', ['data' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @throws AuthorizationException
     * @return Factory|View
     */
    public function create()
    {
        $this->authorize('admin.work.create');
        $supervisors=Supervisor::select('id','name')
        ->orderBy('name')
        ->get();
        $applicants = Applicant::where('parent_applicant', null)
        ->where('government_id', '>' , 0)
        ->whereNotNull('file_number')
        //->wherehas('')
        ->select('id','names', 'last_names', 'government_id')
        ->orderBy('government_id')
        ->get();
       // return $supervisors;
        return view('admin.work.create', compact('supervisors', 'applicants'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreWork $request
     * @return array|RedirectResponse|Redirector
     */
    public function store(StoreWork $request)
    {
        // Sanitize input
        //return $request;

        $sanitized = $request->getSanitized();
        $sanitized ['applicant_id'] =  $request->getApplicantId();
        $sanitized ['supervisor_id'] =  $request->getSupervisorId();

        // Store the Work
        $work = Work::create($sanitized);

        if ($request->ajax()) {
            return ['redirect' => url('admin/works'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
        }

        return redirect('admin/works');
    }

    /**
     * Display the specified resource.
     *
     * @param Work $work
     * @throws AuthorizationException
     * @return void
     */
    public function show(Work $work)
    {
        $this->authorize('admin.work.show', $work);

        // TODO your code goes here
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Work $work
     * @throws AuthorizationException
     * @return Factory|View
     */
    public function edit(Work $work)
    {
        $this->authorize('admin.work.edit', $work);
        $typechecks=TypeCheck::select('id','name')
        ->orderBy('name')
        ->get();
        //return $work;
        $worksTypeCheck=WorksTypeCheck::where('work_id', $work->id)
        ->get();
        return view('admin.work.edit', [
            'work' => $work, 
            'typechecks'=>$typechecks,
            'worksTypeCheck'=>$worksTypeCheck
        ]);
    }
    public function addtype(Work $work, IndexWork $request)
    {
        $data = $request->all();
        return $data;
        $existe=WorksTypeCheck::where('work_id', $request['work_id'])->where('type_check_id', $request['type_check_id']) ->get();
        //return $existe->count();
        if($existe->count()>1){
            if ($request->ajax()) {
                //return ['notify' => ['type' => 'success', 'title' => 'Atención', 'message' => 'El soliciante ya fue cargado.']];
                return ['redirect' => url('admin/works/'.$request['work_id'].'/edit'), 'message' => 'La verificacion ya fue cargada. Favor verificar..!!!'];
            }
        }

        $trustsave = WorksTypeCheck::create([
            'work_id' => $request['work_id'],
            'fecha_check' => $request['fecha'],
            'type_check_id' => $request['type_check_id'],
            'applicant_id' => $request['applicant_id']
        ]);
        if($trustsave){
             if ($request->ajax()) {
                //return $sanitized;
                return ['redirect' => url('admin/works/'.$request['work_id'].'/edit'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
            } 
        }
        return redirect('admin/works/'.$request['work_id'].'/edit');
        //return ['redirect' => url('admin/trusts/'.$request['trust_id'].'/edit'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
    }
    /**
     * Update the specified resource in storage.
     *
     * @param UpdateWork $request
     * @param Work $work
     * @return array|RedirectResponse|Redirector
     */
    public function update(UpdateWork $request, Work $work)
    {
        // Sanitize input
        $sanitized = $request->getSanitized();

        // Update changed values Work
        $work->update($sanitized);

        if ($request->ajax()) {
            return [
                'redirect' => url('admin/works'),
                'message' => trans('brackets/admin-ui::admin.operation.succeeded'),
            ];
        }

        return redirect('admin/works');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param DestroyWork $request
     * @param Work $work
     * @throws Exception
     * @return ResponseFactory|RedirectResponse|Response
     */
    public function destroy(DestroyWork $request, Work $work)
    {
        $work->delete();

        if ($request->ajax()) {
            return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
        }

        return redirect()->back();
    }

    /**
     * Remove the specified resources from storage.
     *
     * @param BulkDestroyWork $request
     * @throws Exception
     * @return Response|bool
     */
    public function bulkDestroy(BulkDestroyWork $request) : Response
    {
        DB::transaction(static function () use ($request) {
            collect($request->data['ids'])
                ->chunk(1000)
                ->each(static function ($bulkChunk) {
                    Work::whereIn('id', $bulkChunk)->delete();

                    // TODO your code goes here
                });
        });

        return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
    }
}
