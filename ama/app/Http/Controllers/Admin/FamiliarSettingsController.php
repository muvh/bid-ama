<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\FamiliarSetting\BulkDestroyFamiliarSetting;
use App\Http\Requests\Admin\FamiliarSetting\DestroyFamiliarSetting;
use App\Http\Requests\Admin\FamiliarSetting\IndexFamiliarSetting;
use App\Http\Requests\Admin\FamiliarSetting\StoreFamiliarSetting;
use App\Http\Requests\Admin\FamiliarSetting\UpdateFamiliarSetting;
use App\Models\FamiliarSetting;
use Brackets\AdminListing\Facades\AdminListing;
use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;

class FamiliarSettingsController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param IndexFamiliarSetting $request
     * @return array|Factory|View
     */
    public function index(IndexFamiliarSetting $request)
    {
        // create and AdminListing instance for a specific model and
        $data = AdminListing::create(FamiliarSetting::class)->processRequestAndGet(
            // pass the request with params
            $request,

            // set columns to query
            ['id', 'name'],

            // set columns to searchIn
            ['id', 'name']
        );

        if ($request->ajax()) {
            if ($request->has('bulk')) {
                return [
                    'bulkItems' => $data->pluck('id')
                ];
            }
            return ['data' => $data];
        }

        return view('admin.familiar-setting.index', ['data' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @throws AuthorizationException
     * @return Factory|View
     */
    public function create()
    {
        //$this->authorize('admin.familiar-setting.create');

        return view('admin.familiar-setting.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreFamiliarSetting $request
     * @return array|RedirectResponse|Redirector
     */
    public function store(StoreFamiliarSetting $request)
    {
        // Sanitize input
        $sanitized = $request->getSanitized();

        // Store the FamiliarSetting
        $familiarSetting = FamiliarSetting::create($sanitized);

        if ($request->ajax()) {
            return ['redirect' => url('admin/familiar-settings'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
        }

        return redirect('admin/familiar-settings');
    }

    /**
     * Display the specified resource.
     *
     * @param FamiliarSetting $familiarSetting
     * @throws AuthorizationException
     * @return void
     */
    public function show(FamiliarSetting $familiarSetting)
    {
        $this->authorize('admin.familiar-setting.show', $familiarSetting);

        // TODO your code goes here
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param FamiliarSetting $familiarSetting
     * @throws AuthorizationException
     * @return Factory|View
     */
    public function edit(FamiliarSetting $familiarSetting)
    {
        //$this->authorize('admin.familiar-setting.edit', $familiarSetting);


        return view('admin.familiar-setting.edit', [
            'familiarSetting' => $familiarSetting,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateFamiliarSetting $request
     * @param FamiliarSetting $familiarSetting
     * @return array|RedirectResponse|Redirector
     */
    public function update(UpdateFamiliarSetting $request, FamiliarSetting $familiarSetting)
    {
        // Sanitize input
        $sanitized = $request->getSanitized();

        // Update changed values FamiliarSetting
        $familiarSetting->update($sanitized);

        if ($request->ajax()) {
            return [
                'redirect' => url('admin/familiar-settings'),
                'message' => trans('brackets/admin-ui::admin.operation.succeeded'),
            ];
        }

        return redirect('admin/familiar-settings');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param DestroyFamiliarSetting $request
     * @param FamiliarSetting $familiarSetting
     * @throws Exception
     * @return ResponseFactory|RedirectResponse|Response
     */
    public function destroy(DestroyFamiliarSetting $request, FamiliarSetting $familiarSetting)
    {
        $familiarSetting->delete();

        if ($request->ajax()) {
            return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
        }

        return redirect()->back();
    }

    /**
     * Remove the specified resources from storage.
     *
     * @param BulkDestroyFamiliarSetting $request
     * @throws Exception
     * @return Response|bool
     */
    public function bulkDestroy(BulkDestroyFamiliarSetting $request) : Response
    {
        DB::transaction(static function () use ($request) {
            collect($request->data['ids'])
                ->chunk(1000)
                ->each(static function ($bulkChunk) {
                    FamiliarSetting::whereIn('id', $bulkChunk)->delete();

                    // TODO your code goes here
                });
        });

        return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
    }
}
