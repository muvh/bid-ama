<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\TypeOrder\BulkDestroyTypeOrder;
use App\Http\Requests\Admin\TypeOrder\DestroyTypeOrder;
use App\Http\Requests\Admin\TypeOrder\IndexTypeOrder;
use App\Http\Requests\Admin\TypeOrder\StoreTypeOrder;
use App\Http\Requests\Admin\TypeOrder\UpdateTypeOrder;
use App\Models\TypeOrder;
use Brackets\AdminListing\Facades\AdminListing;
use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;

class TypeOrdersController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param IndexTypeOrder $request
     * @return array|Factory|View
     */
    public function index(IndexTypeOrder $request)
    {
        // create and AdminListing instance for a specific model and
        $data = AdminListing::create(TypeOrder::class)->processRequestAndGet(
            // pass the request with params
            $request,

            // set columns to query
            ['id', 'title'],

            // set columns to searchIn
            ['id', 'title']
        );

        if ($request->ajax()) {
            if ($request->has('bulk')) {
                return [
                    'bulkItems' => $data->pluck('id')
                ];
            }
            return ['data' => $data];
        }

        return view('admin.type-order.index', ['data' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @throws AuthorizationException
     * @return Factory|View
     */
    public function create()
    {
        //$this->authorize('admin.type-order.create');

        return view('admin.type-order.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreTypeOrder $request
     * @return array|RedirectResponse|Redirector
     */
    public function store(StoreTypeOrder $request)
    {
        // Sanitize input
        $sanitized = $request->getSanitized();

        // Store the TypeOrder
        $typeOrder = TypeOrder::create($sanitized);

        if ($request->ajax()) {
            return ['redirect' => url('admin/type-orders'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
        }

        return redirect('admin/type-orders');
    }

    /**
     * Display the specified resource.
     *
     * @param TypeOrder $typeOrder
     * @throws AuthorizationException
     * @return void
     */
    public function show(TypeOrder $typeOrder)
    {
        $this->authorize('admin.type-order.show', $typeOrder);

        // TODO your code goes here
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param TypeOrder $typeOrder
     * @throws AuthorizationException
     * @return Factory|View
     */
    public function edit(TypeOrder $typeOrder)
    {
        $this->authorize('admin.type-order.edit', $typeOrder);


        return view('admin.type-order.edit', [
            'typeOrder' => $typeOrder,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateTypeOrder $request
     * @param TypeOrder $typeOrder
     * @return array|RedirectResponse|Redirector
     */
    public function update(UpdateTypeOrder $request, TypeOrder $typeOrder)
    {
        // Sanitize input
        $sanitized = $request->getSanitized();

        // Update changed values TypeOrder
        $typeOrder->update($sanitized);

        if ($request->ajax()) {
            return [
                'redirect' => url('admin/type-orders'),
                'message' => trans('brackets/admin-ui::admin.operation.succeeded'),
            ];
        }

        return redirect('admin/type-orders');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param DestroyTypeOrder $request
     * @param TypeOrder $typeOrder
     * @throws Exception
     * @return ResponseFactory|RedirectResponse|Response
     */
    public function destroy(DestroyTypeOrder $request, TypeOrder $typeOrder)
    {
        $typeOrder->delete();

        if ($request->ajax()) {
            return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
        }

        return redirect()->back();
    }

    /**
     * Remove the specified resources from storage.
     *
     * @param BulkDestroyTypeOrder $request
     * @throws Exception
     * @return Response|bool
     */
    public function bulkDestroy(BulkDestroyTypeOrder $request) : Response
    {
        DB::transaction(static function () use ($request) {
            collect($request->data['ids'])
                ->chunk(1000)
                ->each(static function ($bulkChunk) {
                    TypeOrder::whereIn('id', $bulkChunk)->delete();

                    // TODO your code goes here
                });
        });

        return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
    }
}
