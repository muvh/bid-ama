<?php

namespace App\Http\Controllers;

use App\Http\Requests\IndexTracking;
use App\Models\ApplicantStatus;
use App\Models\Applicant;
use Brackets\AdminListing\AdminListing;


class TrackingController extends Controller
{

    public function index(IndexTracking $request)
    {

        $applicantId = 0;
        $applicant = collect([]);
        //str_replace('.','',$government_id) )$government_id) )
        if($request->search){
            $applicant = Applicant::where('government_id','=', str_replace('.','',$request->search))
            ->orWhere('file_number','=',$request->search)->first();
            $applicantId = $applicant->id ?? 0;
        }

        $data = AdminListing::create(ApplicantStatus::class)
            ->attachPagination($request->currentPage, 55)
            ->attachOrdering($request->orderBy, $request->created_at ?? 'desc')
            ->modifyQuery(function($query) use ($applicantId){
                $query->where('applicant_id', '=', $applicantId);
            })
            ->get();
           
        if ($request->ajax()) {
            return ['data' => $data, 'applicant' => $applicant];
        }

        if(count($data)>0){
            dd($data);
        }
      
        return view('tracking', ['data' => $data, 'applicant' => $applicant]);

    }

}
