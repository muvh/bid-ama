<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Mail\DemoEmail;
use Illuminate\Support\Facades\Mail;

class MailController extends Controller
{
    public function send()
    {

        $objDemo = new \stdClass();
        $objDemo->demo_one = 'PEDRO ALEJANDRO ACOSTA MELO';
        $objDemo->demo_two = 'MARIA ALEJANDRA DURE GONZALEZ';
        $objDemo->sender = 'Equipo AMA';
        $objDemo->receiver = 'Pedro Acosta';

        //dd($objDemo);

        Mail::to("postulacionamamuvh@gmail.com")->send(new DemoEmail($objDemo));
    }
}
