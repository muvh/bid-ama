<?php


namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Laravel\Socialite\Facades\Socialite;
use Laravel\Socialite\Contracts\User as ProviderUser;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class SocialAuthFacebookController extends Controller
{
    /**
     * Create a redirect method to facebook api.
     *
     * @return void
     */
    public function redirect()
    {
        return Socialite::driver('facebook')->redirect();
    }

    /**
     * Return a callback method from facebook api.
     *
     * @return callback URL from facebook
     */
    public function callback()
    {
        //$user = Socialite::driver('facebook')->user();
        //dd($user);
        try {
            //create a user using socialite driver google
            $user = Socialite::driver('facebook')->user();
            // if the user exits, use that user and login
            $finduser = User::where('facebook_id', $user->id)->first();
            if ($finduser) {
                //if the user exists, login and show dashboard
                //dd('ya entro');
                Auth::login($finduser);
                return redirect('/home');
            } else {

                $finduser = User::where('email', $user->email)->first();

                if ($finduser) {
                    //dd('Correo ya se registro con otra red social');
                    return redirect('login')->withErrors(['Correo ya se registro con otra red social']);
                } else {
                    $newUser = User::create([
                        'name' => $user->name,
                        'email' => $user->email,
                        'facebook_id' => $user->id,
                        'password' => encrypt('')
                    ]);

                    $newUser->save();
                    //login as the new user
                    Auth::login($newUser);
                    // go to the dashboard
                    return redirect('/home');
                }
            }
            //catch exceptions
        } catch (Exception $e) {
            dd($e->getMessage());
        }
    }
    public function handleGoogleCallback()
    {
        try {
            //create a user using socialite driver google
            $user = Socialite::driver('google')->user();
            // if the user exits, use that user and login
            $finduser = User::where('google_id', $user->id)->first();
            if ($finduser) {
                //if the user exists, login and show dashboard
                Auth::login($finduser);
                return redirect('/home');
            } else {
                //user is not yet created, so create first
                $newUser = User::create([
                    'name' => $user->name,
                    'email' => $user->email,
                    'google_id' => $user->id,
                    'password' => encrypt('')
                ]);
                //every user needs a team for dashboard/jetstream to work.
                //create a personal team for the user
                /*$newTeam = Team::forceCreate([
                    'user_id' => $newUser->id,
                    'name' => explode(' ', $user->name, 2)[0]."'s Team",
                    'personal_team' => true,
                ]);
                // save the team and add the team to the user.
                $newTeam->save();
                $newUser->current_team_id = $newTeam->id;*/
                $newUser->save();
                //login as the new user
                Auth::login($newUser);
                // go to the dashboard
                return redirect('/home');
            }
            //catch exceptions
        } catch (Exception $e) {
            dd($e->getMessage());
        }
    }
}
