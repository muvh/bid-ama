<?php
namespace App\Library;

use App\Http\Controllers\Admin\ApplicantRelationshipsController;
use App\Models\ApplicantDocument;
use App\Models\DocumentType;
use App\Models\Applicant;
use App\Models\ApplicantRelationship;
use App\Models\ApplicantDisease;
use App\Models\ApplicantDisability;

class ValidateDocuments
{
    public $type="S";
    public $applicantID=53;
    public $sort=0;

    function __construct(string $type, int $applicantID, int $sort )
    {
        $this->type=$type;
        $this->applicantID=$applicantID;
        $this->sort=$sort;
    }
    
    public function __set($key, $value){
        $this->$key=$value;
    }

    public function validateRegular()
    {
        $message="";
        $error=0;
        $data= array();
        $document = ApplicantDocument::leftJoin('document_types', 'document_types.id', '=', 'applicant_documents.document_id')
        ->where('applicant_id','=',$this->applicantID)
        ->where('document_types.type','=',$this->type)
        ->pluck('document_types.id')->toArray();
        //SOCIAL
        if($this->sort==1){
            //obligatorios: 1,3,4,6, 
            $validations = array(1,3,4,6);
            foreach ($validations as $value){ 
                 if(!in_array($value,$document)){
                    $message.=" <br> * ".$this->documents($value);
                    $error=1;
                } 
            }
        }
        //FINANCIAL
        if($this->sort==2){
            $validations = array(14,15);
            foreach ($validations as $value){ 
                 if(!in_array($value,$document)){
                    $message.=" <br> * ".$this->documents($value);
                    $error=1;
                } 
            }
        //opcional
        if($this->sort==3){
            $applicant=Applicant::where('id','=',$this->applicantID)->get('marital_status')->toArray();
            $applicantNationality=Applicant::where('id','=',$this->applicantID)->get('nationality')->toArray();
            $parentRelationship=Applicant::where('parent_applicant',$this->applicantID)->get()->toArray();
            $computeIncome=ApplicantRelationship::where('compute_income', true)
            ->pluck('id')
            ->toArray();
            $validations = array(16=>'CA', 17=>'CO', 19=>'VI', 18=>'DI',20 =>'PARAGUAYA', 21=>'fsosten', 22=>'ApplicantDisability', 23=>'ApplicantDisease', 24=>'pregnant');
            foreach ($validations as $clave => $valor){ 
                if(trim($applicant[0]['marital_status'])==$valor){
                    if(!in_array($clave, $document)){
                        $message.=" <br> * ".$this->documents($clave);
                        $error=1;
                    }
                
                }
                if(($clave==20) && trim($applicantNationality[0]['nationality'])!=$valor){
                    if(!in_array($clave, $document)){
                        $message.=" <br> * ".$this->documents($clave);
                        $error=1;
                    }
                }
                if($clave==21){
                    foreach($parentRelationship as $val){
                            foreach($computeIncome as $income){
                                if($income>1 && $income==$val['relationship']['id']){
                                    $message.=" <br> * ".$this->documents($clave);
                                    $message.=" <br> * ".$this->documents(4);
                                    $error=1;  
                                }
                            }
                    }
                }
                if($clave==23){
                    $disease=ApplicantDisease::where('applicant_id',$this->applicantID)->get();
                    if($disease->count()>0){
                        if(!in_array($clave, $document)){
                            $message.=" <br> * ".$this->documents($clave);
                            $error=1;
                            }
                        }
                    }
                if($clave==22){       
                    $disability=ApplicantDisability::where('applicant_id',$this->applicantID)->get();
                    if($disability->count()>0){
                        if(!in_array($clave, $document)){
                            $message.=" <br> * ".$this->documents($clave);
                            $error=1;
                        }
                    }
                }
                if($clave==24){       
                    $pregnant=Applicant::where('id',$this->applicantID)->get()->toArray();
                    if($pregnant[0]['pregnant']==true){
                        if(!in_array($clave, $document)){
                            $message.=" <br> * ".$this->documents($clave);
                            $error=1;
                        }
                    }
                }
            }

        }
        //technical
        if($this->sort==4){
            $validations = array(8,9,10,11,12,13);
            foreach ($validations as $value){ 
                 if(!in_array($value,$document)){
                    $message.=" <br> * ".$this->documents($value);
                    $error=1;
                    
                } 
            }
        }
        $data['error']=$error;    
        $data['message']=$message;
        return $data;

    }
    public function documents($cod){

        $documents = DocumentType::where('id' , '=' , $cod)
        ->pluck('document_types.name')->toArray();
        return $documents[0];

    }
        
}