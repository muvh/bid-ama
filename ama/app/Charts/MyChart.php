<?php

namespace App\Charts;

use ConsoleTVs\Charts\Classes\Chartjs\Chart;

class MyChart extends Chart
{
    /**
     * Initializes the chart.
     *
     * @return void
     */
 


    public $count;
    public $labels; 
    public $dataset;

    public function __construct($count, $labels, $dataset)
    {
        $this->count=$count;
        $this->labels=$labels;
        $this->dataset=$dataset;
        parent::__construct();
    }
    public function my_chart(){
        $chart = new Chart;
        //for ($i = 0; $i <= count($atcCount); $i++) {
        for ($i = 0; $i <= $this->count; $i++) {
            $colours[] = '#' . substr(str_shuffle('ABCDEF0123456789'), 0, 6);
        }
  
     
        $borderColors = [
            "rgba(255, 99, 132, 1.0)",
            "rgba(22,160,133, 1.0)",
            "rgba(255, 205, 86, 1.0)",
            "rgba(51,105,232, 1.0)",
            "rgba(244,67,54, 1.0)",
            "rgba(34,198,246, 1.0)",
            "rgba(153, 102, 255, 1.0)",
            "rgba(255, 159, 64, 1.0)",
            "rgba(233,30,99, 1.0)",
            "rgba(205,220,57, 1.0)"
        ];
        $fillColors = [
            "rgba(255, 99, 132, 0.2)",
            "rgba(22,160,133, 0.2)",
            "rgba(255, 205, 86, 0.2)",
            "rgba(51,105,232, 0.2)",
            "rgba(244,67,54, 0.2)",
            "rgba(34,198,246, 0.2)",
            "rgba(153, 102, 255, 0.2)",
            "rgba(255, 159, 64, 0.2)",
            "rgba(233,30,99, 0.2)",
            "rgba(205,220,57, 0.2)"

        ];
        $chart->labels($this->labels);

        $chart->dataset('Adjudicaciones por mes', 'bar', $this->dataset)
        ->color($borderColors)
        ->backgroundcolor($fillColors);
 
        return $chart;
    }
}
