<?php

namespace App\Exports;

use App\Models\Applicant;
use App\Models\ApplicantStatus;
use App\Models\AccountingRecord;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Carbon\Carbon;

class AccountingRecordExport implements FromView, WithMapping, WithHeadings
{
    /**
     * @return \Illuminate\Support\Collection
     */
    //private $status;
    function __construct()
    {

        //$this->status = $status;
    }


    public function view(): View
    {

        $ingresos = AccountingRecord::where('type_movements_id', 1)->orWhere('type_movements_id', 3)->sum('balance');
        $egresos = AccountingRecord::where('type_movements_id', 2)->where('confirmed', true)->sum('balance');
        $balance = $ingresos - $egresos;
        $subsidios = floor($balance / 34000000);

        return view('admin.accounting-record.reports.accounting-reports', [
            'invoices' => AccountingRecord::all(),
            'ingresos' => $ingresos,
            'egresos' => $egresos,
            'balance' => $balance,
            'subsidios' => $subsidios,
        ]);
    }

    /*public function collection()
    {
        return AccountingRecord::all();
    }*/

    public function headings(): array
    {
        return [
            //'ID',
            'Descripcion',
            'Tipo de Movimiento',
            'Cantidad',
            'Balance',
            'Resolucion',
            'Fecha Registro',
            //'Aseguradora',

        ];
    }

    /**
     * @param Applicant $applicant
     * @return array
     *
     */
    public function map($insurance): array
    {
        //$date = Carbon::parse($insurance->created_at);
        return [
            //$insurance->id,
            $insurance->summary,
            $insurance->typeMovement['name'],
            $insurance->beneficiaries_count > 0 ? $insurance->beneficiaries_count : 'N/A',
            $insurance->balance,
            $insurance->resolution,
            $insurance->registration_date,
            //is_null($insurance->insurance_entity->title)?'':$insurance->insurance_entity->title,
            /*$insurance->construction->name,
            $insurance->importe*/

        ];
    }
}
