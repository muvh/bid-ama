<?php

namespace App\Exports;

use Carbon\Carbon;
use App\Models\Status;
use App\Models\Applicant;
use App\Models\ApplicantStatus;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use Maatwebsite\Excel\Concerns\WithColumnWidths;

class ApplicantsExport implements FromCollection, WithMapping, WithHeadings, WithStyles, WithColumnWidths
{
    /**
     * @return \Illuminate\Support\Collection
     */
    private $status;
    function __construct($status)
    {

        $this->status = $status;
    }



    public function collection()
    {
        $estados = Applicant::whereNull('applicants.parent_applicant')

            ->get();
        $idApplicant = [];
        foreach ($estados as $value) {
            if (strlen($this->status) > 2) {
                $rs_steps = Status::where('prefix', $this->status)->pluck('id')->toArray();
                if (in_array($value->statuses->status_id, $rs_steps)) {
                    $idApplicant[] = $value->statuses->applicant_id;
                }
            } else {

                if ($value->statuses->status_id == $this->status) {
                    $idApplicant[] = $value->statuses->applicant_id;
                }
            }
        }
        return Applicant::whereIn('applicants.id', $idApplicant)

            ->whereNull('applicants.parent_applicant')
            ->with('reportDisabilities', 'reportDiseases', 'applicantStatuses')
            ->get();
    }

    public function headings(): array
    {
        return [
            'ID',
            'Nombres',
            'Apellidos',
            'Documento',
            'Genero',
            'Edad',
            'Ciudad',
            'Dirección',
            'Telefono',
            'Beneficiarios',
            'Ingreso Familiar',
            'Fecha de Inscripción',
            'ATC',
            'Fecha Asig ATC',
            'Cooperativa',
            'Estatdo',
            '80% Enviado CAC',
            '20% Enviado CAC',
            'Nro. Resolución',
            'Discapacidad',
            'Enfermedad',
            'Observaciones'
        ];
    }

    /**
     * @param Applicant $applicant
     * @return array
     *
     */
    public function map($applicant): array
    {
        $date = Carbon::parse($applicant->created_at);
        $nacimiento = Carbon::parse($applicant->birthdate);
        $actual = Carbon::now();
        $obs = '';

        foreach ($applicant->applicantStatuses as $item) {
            $obs = $obs . 'Fecha: ' . $item['created_at'] . ' Descripcion: ' . $item['description']  . PHP_EOL;
        }

        return [
            $applicant->id,
            $applicant->names,
            $applicant->last_names,
            $applicant->government_id,
            $applicant->gender,
            Carbon::parse($applicant->birthdate)->age,
            $applicant->city ? $applicant->city->name : '',
            $applicant->address ? $applicant->address : '',
            $applicant->applicantGetCellphone ? $applicant->applicantGetCellphone->description : '',
            $applicant->beneficiaries->count() + 1,
            intval($applicant->monthly_income) +
                ($applicant->conyuge ?
                    intval($applicant->conyuge->monthly_income) :
                    intval($applicant->monthly_income)),
            $date->format('d/m/Y'),
            $applicant->construction ? $applicant->construction->name : '',
            $applicant->atc_fec_asig ? Carbon::parse($applicant->atc_fec_asig)->format('d/m/Y') : '',
            $applicant->financial ? $applicant->financial->name : '',
            $applicant->statuses->status->name ? $applicant->statuses->status->name : '',
            $applicant->enviocac ? Carbon::parse($applicant->enviocac->created_at)->format('d/m/Y') : '', //$applicant->insurance_policy ? $applicant->insurance_policy:'',
            $applicant->envio20cac ? Carbon::parse($applicant->envio20cac->created_at)->format('d/m/Y') : '', //$applicant->insurance_policy ? $applicant->insurance_policy:'',
            $applicant->resolution_number ? $applicant->resolution_number : '',
            $applicant->reportDisabilities ? $applicant->reportDisabilities->disability->name : '',
            $applicant->reportDiseases ? $applicant->reportDiseases->disease->name : '',
            //$applicant->id,
            //$applicant->id,
            $obs,
            // (item.monthly_income ? parseInt(item.monthly_income)  : 0 ) + (item.conyuge ? parseInt(item.conyuge.monthly_income ? item.conyuge.monthly_income : 0) : 0)
        ];
    }

    public function styles(Worksheet $sheet)
    {
        return [
            'V' => ['alignment' => ['wrapText' => true]],
        ];
    }

    public function columnWidths(): array
    {
        return [
            'V' => 300,

        ];
    }
}
