<?php

namespace App\Exports;

use App\Models\Applicant;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithDrawings;
use PhpOffice\PhpSpreadsheet\Worksheet\Drawing;
use Maatwebsite\Excel\Concerns\WithCustomStartCell;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use Carbon\Carbon;

class ApplicantsByResolution implements FromCollection, WithMapping, WithHeadings, WithDrawings, WithCustomStartCell, ShouldAutoSize, WithEvents
{

    private $resolution_number;

    function __construct($resolution_number) {
        $this->resolution_number = $resolution_number;
    }

    public function startCell(): string
    {
        return 'A6';
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class    => function(AfterSheet $event) {
                $cellRange = 'A6:L6'; // All headers
                $event->sheet->getDelegate()->getStyle($cellRange)->getFont()->setSize(12);

                $event->sheet->getDelegate()->getStyle($cellRange)
                    ->getFill()
                    ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                    ->getStartColor()
                    ->setRGB('FFFF02');

                $event->sheet->getColumnDimension('K')->setAutoSize(false);
                $event->sheet->getColumnDimension('J')->setAutoSize(false);
                $event->sheet->getColumnDimension('L')->setAutoSize(false);
                $event->sheet->getColumnDimension('k')->setWidth(20);
                $event->sheet->getColumnDimension('J')->setWidth(20);
                $event->sheet->getColumnDimension('L')->setWidth(20);

                $lastRow = $event->sheet->getHighestRow();

                $event->sheet->getDelegate()->getStyle('J6:L'.$lastRow)
                    ->getAlignment()->setWrapText(true);

                $styleArray = array(
                    'alignment' => array(
                        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT,
                        'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                    )
                );
                $event->sheet->getDelegate()->getStyle('A6:L'.$lastRow)->applyFromArray($styleArray);


            },
        ];
    }

    public function drawings()
    {
        $drawing = new Drawing();
        $drawing->setName('');
        $drawing->setDescription('');
        $drawing->setPath(public_path('/images/logo.jpg'));
        $drawing->setHeight(90);
        $drawing->setCoordinates('A1');

        $drawing2 = new Drawing();
        $drawing2->setName('');
        $drawing2->setDescription('');
        $drawing2->setPath(public_path('/images/gobierno.png'));
        $drawing2->setHeight(90);
        $drawing2->setCoordinates('G1');

        $drawing3 = new Drawing();
        $drawing3->setName('');
        $drawing3->setDescription('');
        $drawing3->setPath(public_path('/images/paraguay-de-la-gente.png'));
        $drawing3->setHeight(90);
        $drawing3->setCoordinates('L1');

        return [$drawing, $drawing2, $drawing3];

    }

    public function collection()
    {

        return Applicant::selectRaw("ROW_NUMBER () OVER (ORDER BY applicants.id) as id,applicants.file_number as file_number,applicants.created_at::timestamp as admission_data,
                    applicants.names||' '||applicants.last_names as applicant_full_name, applicants.government_id as government_id,
                    (select b.names||' '||b.last_names||'-'||b.government_id from applicants b where b.parent_applicant = applicants.id and b.applicant_relationship = 1 ) as spouse_data, applicants.marital_status as marital_status,
                    coalesce(coalesce((select sum(beneficiaries.monthly_income) from applicants as beneficiaries
                    inner join applicant_relationships on beneficiaries.applicant_relationship = applicant_relationships.id
                    where beneficiaries.parent_applicant = applicants.id and compute_income = true ),0) + coalesce(applicants.monthly_income,0),0) as total_monthly_income,
                    construction_entity, financial_entity, construction_entities.name as centityname, financial_entities.name as fentityname, cities.name as city_name, applicants.resolution_number
                    ")
            ->leftJoin('cities', 'applicants.city_id', '=', 'cities.id')
            ->leftJoin('construction_entities', 'applicants.construction_entity', '=', 'construction_entities.id')
            ->leftJoin('financial_entities', 'applicants.financial_entity', '=', 'financial_entities.id')
            ->whereNull('applicants.parent_applicant')->where('resolution_number','=', $this->resolution_number)->get();

    }

    public function headings(): array
    {

        return [
            'Nro',
            'Expediente',
            'Ingreso',
            'Beneficiario',
            'CI Nro',
            'Conyuge',
            'CI Nro',
            'Estado Civil',
            'Ingreso',
            'ATC',
            'Coop.',
            'Ciudad',
        ];

    }

    public function map($applicant): array
    {
        $date = Carbon::parse($applicant->admission_data);

        return [
            $applicant->id,
            $applicant->file_number,
            $date->format('d-m-Y'),
            $applicant->applicant_full_name,
            $applicant->government_id,
            $applicant->spouse_data ? (explode('-',$applicant->spouse_data))[0] : '',
            $applicant->spouse_data ? (explode('-',$applicant->spouse_data))[1] : '',
            $applicant->marital_status,
            $applicant->total_monthly_income,
            $applicant->centityname,
            $applicant->fentityname,
            $applicant->city_name,
        ];
    }

}
