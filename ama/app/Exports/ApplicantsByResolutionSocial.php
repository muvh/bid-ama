<?php

namespace App\Exports;

use App\Models\Applicant;
use App\Models\ApplicantStatus;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithDrawings;
use PhpOffice\PhpSpreadsheet\Worksheet\Drawing;
use Maatwebsite\Excel\Concerns\WithCustomStartCell;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use Carbon\Carbon;

class ApplicantsByResolutionSocial implements FromCollection, WithMapping, WithHeadings, WithDrawings, WithCustomStartCell, ShouldAutoSize, WithEvents
{


    private $dateto;
    private $datefrom;
    private $status;

    function __construct($dateto, $datefrom, $status) {
        $this->dateto = $dateto;
        $this->datefrom = $datefrom;
        $this->status = $status;
    }

    public function startCell(): string
    {
        return 'A6';
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class    => function(AfterSheet $event) {
                $cellRange = 'A6:T6'; // All headers
                $event->sheet->getDelegate()->getStyle($cellRange)->getFont()->setSize(12);

                $event->sheet->getDelegate()->getStyle($cellRange)
                    ->getFill()
                    ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                    ->getStartColor()
                    ->setRGB('FFFF02');

                $event->sheet->getColumnDimension('K')->setAutoSize(false);
                $event->sheet->getColumnDimension('J')->setAutoSize(false);
                $event->sheet->getColumnDimension('L')->setAutoSize(false);
                $event->sheet->getColumnDimension('k')->setWidth(20);
                $event->sheet->getColumnDimension('J')->setWidth(20);
                $event->sheet->getColumnDimension('L')->setWidth(20);

                $lastRow = $event->sheet->getHighestRow();

                $event->sheet->getDelegate()->getStyle('J6:T'.$lastRow)
                    ->getAlignment()->setWrapText(true);

                $styleArray = array(
                    'alignment' => array(
                        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT,
                        'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                    )
                );
                $event->sheet->getDelegate()->getStyle('A6:T'.$lastRow)->applyFromArray($styleArray);


            },
        ];
    }

    public function drawings()
    {
        $drawing = new Drawing();
        $drawing->setName('');
        $drawing->setDescription('');
        $drawing->setPath(public_path('/images/logo.jpg'));
        $drawing->setHeight(90);
        $drawing->setCoordinates('A1');

        $drawing2 = new Drawing();
        $drawing2->setName('');
        $drawing2->setDescription('');
        $drawing2->setPath(public_path('/images/gobierno.png'));
        $drawing2->setHeight(90);
        $drawing2->setCoordinates('G1');

        $drawing3 = new Drawing();
        $drawing3->setName('');
        $drawing3->setDescription('');
        $drawing3->setPath(public_path('/images/paraguay-de-la-gente.png'));
        $drawing3->setHeight(90);
        $drawing3->setCoordinates('L1');

        return [$drawing, $drawing2, $drawing3];

    }

    public function collection()
    {

        if($this->status!=0){
            return Applicant::whereNull('applicants.parent_applicant')
            ->whereIn('applicants.id', ApplicantStatus::where('status_id', '=', $this->status)->latest()->pluck('applicant_id')->toArray())
                
            //->whereIn('applicants.id', ApplicantStatus::where('status_id', '=', $this->status)->pluck('applicant_id')->toArray())
                ->with('applicantGetCellphone','city', 'conyuge', 'financial', 'construction')
                ->whereBetween('applicants.created_at', [$this->dateto . ' 00:00:00', $this->datefrom . ' 23:59:59'])
                ->get();
            }else{
            return Applicant::whereNull('applicants.parent_applicant')
               
                ->with('applicantGetCellphone','city', 'conyuge', 'financial', 'construction')
                ->whereBetween('applicants.created_at', [$this->dateto . ' 00:00:00', $this->datefrom . ' 23:59:59'])
                ->get();

        }


/*
        return Applicant::
/*         selectRaw("ROW_NUMBER () OVER (ORDER BY applicants.id) as id,applicants.file_number as file_number,applicants.created_at::timestamp as admission_data,
                    applicants.names||' '||applicants.last_names as applicant_full_name, applicants.government_id as government_id,
                    (select b.names||' '||b.last_names||'-'||b.government_id from applicants b where b.parent_applicant = applicants.id and b.applicant_relationship = 1 ) as spouse_data, applicants.marital_status as marital_status,
                    coalesce((select sum(beneficiaries.monthly_income) from applicants as beneficiaries
                    inner join applicant_relationships on beneficiaries.applicant_relationship = applicant_relationships.id
                    where beneficiaries.parent_applicant = applicants.id and compute_income = true ) + coalesce(applicants.monthly_income,0),0) as total_monthly_income,
                    construction_entity, financial_entity, construction_entities.name as centityname, financial_entities.name as fentityname, cities.name as city_name, applicants.resolution_number
                    ")
            ->leftJoin('cities', 'applicants.city_id', '=', 'cities.id')
            ->leftJoin('construction_entities', 'applicants.construction_entity', '=', 'construction_entities.id')
            ->leftJoin('financial_entities', 'applicants.financial_entity', '=', 'financial_entities.id')
            ->whereNull('applicants.parent_applicant')->where('resolution_number','=', $this->resolution_number)->get(); */
/*
            selectRaw("ROW_NUMBER () OVER (ORDER BY applicants.id) as id,applicants.file_number as file_number,applicants.created_at as admission_data,
                    applicants.names||' '||applicants.last_names as applicant_full_name, applicants.government_id as government_id,
                    (select b.names||' '||b.last_names||'-'||b.government_id from applicants b where b.parent_applicant = applicants.id and b.applicant_relationship = 1 ) as spouse_data, applicants.marital_status as marital_status, 
                    applicants.address as address, 
                    applicants.neighborhood as neighborhood,
                    applicants.receiving_file_date as  receiving_file_date,
                    (select description from applicant_contact_methods WHERE applicant_id= applicants.id  ) as telefono,
                    (select ap.description from applicant_statuses ap where ap.applicant_id= applicants.id and  ap.status_id=1 ) as description,
                    (select  case when aa.answer = '1' then 'SI' else 'NO' end as social_evaluation
                    from applicant_answers aa 
                    where aa.applicant_id=applicants.id  
                    and aa.question_id=7) as social_evaluation,
                    (SELECT max(ah.assignment_date) from assignment_histories as ah where ah.applicant_id=applicants.id) as assignment_date,
                    (select  case when aa.answer = '1' then 'SI' else 'NO' end as answer_status
                    from applicant_answers aa 
                    where aa.applicant_id=applicants.id  
                    and aa.question_id=15) as social_rating,
                    coalesce((select sum(beneficiaries.monthly_income) from applicants as beneficiaries
                    inner join applicant_relationships on beneficiaries.applicant_relationship = applicant_relationships.id
                    where beneficiaries.parent_applicant = applicants.id and compute_income = true ) + coalesce(applicants.monthly_income,0),0) as total_monthly_income,
                    construction_entity, financial_entity, construction_entities.name as centityname, financial_entities.name as fentityname, cities.name as city_name, applicants.resolution_number
                    ")
                    ->leftJoin('cities', 'applicants.city_id', '=', 'cities.id')
                    ->leftJoin('construction_entities', 'applicants.construction_entity', '=', 'construction_entities.id')
                    ->leftJoin('financial_entities', 'applicants.financial_entity', '=', 'financial_entities.id')
                    ->whereNull('applicants.parent_applicant')
                    ->whereBetween('applicants.created_at',[$this->dateto.' 00:00:00', $this->datefrom.' 23:59:59'])->get();

    */
                }

    public function headings(): array
    {

        return [
            'Nro',
            'Coop.',
            'Ingreso',
            'Beneficiario',
            'CI Nro',
            'Conyuge',
            'CI Nro',
            'Estado Civil',
            'Dirección',
            'Barrio',
            'Municipio',
            'ATC Asignada',
            'Nro. Expediente',
            'Fecha ingreso Expediente',
            'Evaluacion Social',
            'Calific. Social',
            'Fecha de asignacion ATC',
            'Observación',
            'Telefono',
            'Ingreso',
        ];

    }

    public function map($applicant): array
    {
        $date = Carbon::parse($applicant->created_at);
        $date2 = Carbon::parse($applicant->receiving_file_date);
        $date3 = Carbon::parse($applicant->assignment_date);
        //print_r($applicant);
        return [
            $applicant->id,
            $applicant->financial->name,
            $date->format('d-m-Y'),
            $applicant->names .' '.$applicant->last_names,
            $applicant->government_id,
            $applicant->conyuge?$applicant->conyuge->names:'',
            $applicant->conyuge?$applicant->conyuge->government_id:'',
            $applicant->marital_status,
            $applicant->address,
            $applicant->neighborhood,
            $applicant->city?$applicant->city->name:'',
            $applicant->construction?$applicant->construction->name:'',
            $applicant->file_number,
            $applicant->receiving_file_date, //Fecha ingreso Expediente
            $applicant->social_evaluation,
            $applicant->social_rating,
            $applicant->assignment_date,
            $applicant->description,
            $applicant->applicantGetCellphone ? $applicant->applicantGetCellphone->description:'',
            intval($applicant->monthly_income) + 
            ($applicant->conyuge ? 
            intval($applicant->conyuge->monthly_income):
            intval($applicant->monthly_income)),
        ];
    }

}
