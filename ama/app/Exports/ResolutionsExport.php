<?php

namespace App\Exports;

use App\Models\Applicant;
use App\Models\ResourceTransfer;
use App\Models\AccountingRecord;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Carbon\Carbon;

class ResolutionsExport implements FromView, WithMapping, WithHeadings
{
    /**
     * @return \Illuminate\Support\Collection
     */
    //private $status;
    function __construct()
    {

        //$this->status = $status;
    }


    public function view(): View
    {

        $users = ResourceTransfer::where('fecha_rt', '>=', '2023-01-01')->get();

        return view('admin.accounting-record.reports.resolutions', [
            'invoices' => $users,

        ]);
    }

    /*public function collection()
    {
        return AccountingRecord::all();
    }*/

    public function headings(): array
    {
        return [
            //'ID',
            'Descripcion',
            'Tipo de Movimiento',
            'Cantidad',
            'Balance',
            'Resolucion',
            'Fecha Registro',
            //'Aseguradora',

        ];
    }

    /**
     * @param Applicant $applicant
     * @return array
     *
     */
    public function map($insurance): array
    {
        //$date = Carbon::parse($insurance->created_at);
        return [
            //$insurance->id,
            $insurance->summary,
            $insurance->typeMovement['name'],
            $insurance->beneficiaries_count > 0 ? $insurance->beneficiaries_count : 'N/A',
            $insurance->balance,
            $insurance->resolution,
            $insurance->registration_date,
            //is_null($insurance->insurance_entity->title)?'':$insurance->insurance_entity->title,
            /*$insurance->construction->name,
            $insurance->importe*/

        ];
    }
}
