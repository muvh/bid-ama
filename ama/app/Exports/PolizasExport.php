<?php

namespace App\Exports;

use App\Models\Applicant;
use App\Models\ApplicantStatus;
use App\Models\InsurancePolicy;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Carbon\Carbon;

class PolizasExport implements FromCollection, WithMapping, WithHeadings
{
    /**
     * @return \Illuminate\Support\Collection
     */
    //private $status;
    function __construct() {

        //$this->status = $status;
    }
    public function collection()
    {
        return InsurancePolicy::
            where('culminado', false)
            ->get();
    }

    public function headings(): array
    {
        return [
            'ID',
            'Nro poliza',
            'Concepto',
            'Nro CI',
            'Postulante',
            'Fecha Vigencia',
            'Fecha Vencimiento',
            //'Aseguradora',
            'ATC',
            'Importe',
      

        ];
    }

    /**
     * @param Applicant $applicant
     * @return array
     *
     */
    public function map($insurance): array
    {
        $date = Carbon::parse($insurance->created_at);
        return [
            $insurance->id,
            $insurance->nro_poliza,
            $insurance->concepto,
            $insurance->applicant->government_id,
            $insurance->applicant->names.' '. $insurance->applicant->last_names,
            $insurance->fecha_vigencia,
            $insurance->fecha_vencimiento,
            //is_null($insurance->insurance_entity->title)?'':$insurance->insurance_entity->title,
            $insurance->construction->name,
            $insurance->importe
            
        ];
    } 
}
