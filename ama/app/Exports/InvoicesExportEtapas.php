<?php

namespace App\Exports;

use App\Models\City;
//use App\Models\Call;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Carbon\Carbon;

class InvoicesExportEtapas implements FromView
{

    public function __construct(object $call, string $atc, string $coop, string $muni, string $config, string $estado)
    {
        $this->atc = $atc;
        $this->coop = $coop;
        $this->muni = $muni;
        $this->config = $config;
        $this->estado = $estado;
        $this->invoices = $call;
        //$this->row = '0';
    }

    public function view(): View
    {
        /*$invoices = Application::where('call_id', $this->call)
                    ->get()
                    ->sortByDesc('statuses.status_id');
        $call = Call::find($this->call);*/
        //$invoices = City::all();

        return view('admin.report.reporteExcelEtapas', [
            'invoices' => $this->invoices,
            'atc' => $this->atc,
            'coop' => $this->coop,
            'muni' => $this->muni,
            'config' => $this->config,
            'estado' => $this->estado
        ]);
    }

}
