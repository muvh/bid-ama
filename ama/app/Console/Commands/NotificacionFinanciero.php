<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use App\Models\Applicant;
use App\Mail\NotifFinanciero;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;

class NotificacionFinanciero extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'not:fin';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Postulantes con CAC notificadas';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        /*En estado Ejecutado pero 30 sin cargar fecha de ejecucion */
        $applicantsSCulminar = [];
        $result = Applicant::whereNull('applicants.parent_applicant')->get();
        $idApplicantEJe = [];
        $mayor15dias = 0;
        $mayor30dias = 0;
        foreach ($result as $value) {
            if ($value->statuses->status_id == 41) {
                $idApplicantEJe[] = $value->statuses->applicant_id;
            }
        }
        $estadoEnEjecucion = Applicant::whereNull('applicants.parent_applicant')
            ->whereIn('applicants.id', $idApplicantEJe)
            ->get();

        foreach ($estadoEnEjecucion as $value) {
            $fecha1 = date_create();
            $fecha2 = date_create(Carbon::parse($value->enviocac->created_at)->format('Y-m-d'));
            $dias = date_diff($fecha2, $fecha1)->format('%R%a');
            //Storage::append("archivo.txt", $value);

            if ($dias >= 14 && is_null($value->finobras)) {
                $applicantsSCulminar[] = [
                    'id' => $value->id,
                    'ci' => $value->government_id,
                    'titular' => $value->names . " " . $value->last_names,
                    'fecha_estado' => Carbon::parse($value->enviocac->created_at)->format('Y-m-d'),
                    'atc' => $value->construction->name,
                    'coop' => $value->financial->name,
                    'estado' => $value->statuses->status->name,
                    'dias' => $dias,
                ];
                $mayor30dias++;
            }
        }

        /*alerta de obras sin ejecucion */
        $applicantsSEjecucion = [];
        $idApplicant = [];
        foreach ($result as $value) {
            if ($value->statuses->status_id == 43) {
                $idApplicant[] = $value->statuses->applicant_id;
            }
        }
        $adjudicados = Applicant::whereNull('applicants.parent_applicant')
            ->whereIn('applicants.id', $idApplicant)
            ->get();

        foreach ($adjudicados as $value) {
            $fecha1 = date_create();
            $fecha2 = date_create(Carbon::parse($value->envio20cac->created_at)->format('Y-m-d'));
            $dias = date_diff($fecha2, $fecha1)->format('%R%a');
            //Storage::append("archivo.txt", $dias);


            if ($dias >= 30) {
                $applicantsSEjecucion[] = [
                    'id' => $value->id,
                    'ci' => $value->government_id,
                    'titular' => $value->names . " " . $value->last_names,
                    'fecha_estado' => Carbon::parse($value->envio20cac->created_at)->format('Y-m-d'),
                    'atc' => $value->construction->name,
                    'coop' => $value->financial->name,
                    'estado' => $value->statuses->status->name,
                    'dias' => $dias,
                ];
                $mayor15dias++;
            }
        }



        $objDemo = new \stdClass();
        //$objDemo->name = 'TEST';
        $objDemo->applicantsSCulminar = $applicantsSCulminar;
        $objDemo->applicantsSEjecucion = $applicantsSEjecucion;
        $objDemo->mayor15dias = $mayor15dias;
        $objDemo->mayor30dias = $mayor30dias;
        $objDemo->sender = 'Equipo AMA';
        //$objDemo->obs = 'TEST';
        $objDemo->receiver = 'Pedro';
        Mail::to('pacosta@muvh.gov.py')->send(new NotifFinanciero($objDemo));
        sleep(10);
        /*Mail::to('jcorbeta@muvh.gov.py')->send(new EjecucionesObras($objDemo));
        sleep(10);
        Mail::to('saruiz@muvh.gov.py')->send(new EjecucionesObras($objDemo));
        sleep(10);
        Mail::to('mpaez@muvh.gov.py')->send(new EjecucionesObras($objDemo));
        sleep(10);
        Mail::to('financierobid@gmail.com')->send(new EjecucionesObras($objDemo));*/
    }
}
