<?php

namespace App\Console\Commands;

use App\Mail\EmailStatus;
use App\Models\Applicant;
use App\Mail\ExpirationNotify;
use App\Models\ApplicantStatus;
use App\Models\InsurancePolicy;
use Illuminate\Console\Command;
use PhpParser\Node\Expr\FuncCall;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\Admin\InsurancePolicy\UpdateInsurancePolicy;

class TestEmail extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'test:email';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Envio de correo electronico con alertas';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

         $control1 = InsurancePolicy::where('culminado','=', false)
                ->get();
                Log::info('Info 1 log Principal '. $control1->count());
                foreach ($control1 as $key => $value) {
                    $ctrApplicant=ApplicantStatus::where('applicant_id',$value->applicant->id)->get();
                   // Log::info('Info log test applicants-id '. $ctrApplicant->count());
                   $idApplicant=$value->applicant->id;
                   Log::info('Info 2 log test applicants-id '. $idApplicant);
                   foreach ($ctrApplicant as $key => $val) {
                       
                        Log::info('Info 3 log test applicants-id '. $val->status_id);
                        if($val->status_id==26 || $val->status_id==47 ){
                            
                        $fecha1 = date_create();
                        $fecha2 = date_create($value['fecha_vencimiento']);
                        $dias = date_diff($fecha1, $fecha2)->format('%R%a');
                        if ($dias <= 0 ) {
                           $res=InsurancePolicy::where('applicant_id',$idApplicant)
                           ->update(['culminado'=>true]);
               
                           Log::info('Info 4 log test UPDATE '. $val->applicant_id);
                           Log::info('Info 5 log RESULTADO-> '. $res);
                       }
                    }

                   }
                    
                        //Log::info('Info log test applicants-id '. $ctrApplicant);

                    
                }

        $polizas = [];
        $result = InsurancePolicy::where('culminado','!=', true)
                ->get();


        foreach ($result as $key => $value) {
            $fecha1 = date_create();
            $fecha2 = date_create($value['fecha_vencimiento']);
            $dias = date_diff($fecha1, $fecha2)->format('%R%a');
            //Storage::append("archivo.txt", $dias);

            if ($dias <= 60 ) {
                $polizas[] = [
                    'id' => $value->id,
                    'ci' => $value->applicant->government_id,
                    'titular' => $value->applicant->names." ".$value->applicant->last_names,
                    'poliza' => $value['nro_poliza'],
                    'atc'=> $value->applicant->construction->name,
                    'coop'=> $value->applicant->financial->name,
                    'concepto' => $value['concepto'],
                    'vencimiento' => $value['fecha_vencimiento'],
                    'dias' => $value['dias'],
                ];
            }
        }

        $objDemo = new \stdClass();
        //$objDemo->name = 'TEST';
        $objDemo->polizas = $polizas;
        $objDemo->sender = 'Equipo AMA';
        //$objDemo->obs = 'TEST';
        $objDemo->receiver = 'SANDRA';
        Mail::to('fcardozo@muvh.gov.py')->send(new ExpirationNotify($objDemo));
        sleep(10);
        Mail::to('emaciel@muvh.gov.py')->send(new ExpirationNotify($objDemo));
        sleep(10);
        Mail::to('eledezma@muvh.gov.py')->send(new ExpirationNotify($objDemo));
        sleep(10);
        Mail::to('saruiz@muvh.gov.py')->send(new ExpirationNotify($objDemo));
        sleep(10);
        Mail::to('ganazco@muvh.gov.py')->send(new ExpirationNotify($objDemo));
 
    }

}
