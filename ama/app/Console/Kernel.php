<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
        Commands\TestEmail::class,
        Commands\opsinejecucion::class,
        Commands\NotificacionFinanciero::class
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        //solo los primeros dias de cada mes del 01 al 05 Polizas
        if(date("d")<=7){
            $schedule->command('test:email');
        }
        //->monthlyOn(2, '8:00');

        //cada lunes seguimiento de obras
       $schedule->command('op:ej');
        //->weekly();
        //          ->hourly();
        /*
        con el putty ejecutar en el servidos el siguiente comando
        * * * * * cd /www/html/bid-ama/ama && php artisan schedule:run >> /dev/null 2>&1
        */
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__ . '/Commands');

        require base_path('routes/console.php');
    }
}
