<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Brackets\Media\HasMedia\ProcessMediaTrait;
use Brackets\Media\HasMedia\AutoProcessMediaTrait;
use Brackets\Media\HasMedia\HasMediaCollectionsTrait;
use Brackets\Media\HasMedia\HasMediaThumbsTrait;
use Spatie\MediaLibrary\Models\Media;
use Spatie\MediaLibrary\HasMedia\HasMedia;

class Visit extends Model implements HasMedia
{

    use ProcessMediaTrait;
    use AutoProcessMediaTrait;
    use HasMediaCollectionsTrait;
    use HasMediaThumbsTrait;


    protected $fillable = [
        'applicant_id',
        'visit_number',
        'advance',
        'latitude',
        'longitude',
        'visit_date',
        'footnote',

    ];


    protected $dates = [
        'visit_date',
        'created_at',
        'updated_at',

    ];

    protected $appends = ['resource_url'];

    /* ************************ ACCESSOR ************************* */

    public function getResourceUrlAttribute()
    {
        return url('/admin/visits/' . $this->getKey());
    }

    /*public function registerMediaCollections()
    {
        $this->addMediaCollection('visit_gallery');
    }*/

    function registerMediaCollections()
    {
        $this->addMediaCollection('visit_gallery')
            ->accepts('image/*')
            ->maxFilesize(2 * 2048 * 2048)
            ->maxNumberOfFiles(10);
        //->minNumberOfFiles(10);
    }

    public function registerMediaConversions(Media $media = null)
    {
        $this->autoRegisterThumb200();
    }
}
