<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WorksDatum extends Model
{
    protected $fillable = [
        'applicant_id',
        'start_work',
        'latitude',
        'longitude',
    
    ];
    
    
    protected $dates = [
        'start_work',
        'created_at',
        'updated_at',
    
    ];
    
    protected $appends = ['resource_url'];

    /* ************************ ACCESSOR ************************* */

    public function getResourceUrlAttribute()
    {
        return url('/admin/works-data/'.$this->getKey());
    }
}
