<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property integer $entity_id
 * @property integer $applicant_id
 * @property string $assignment_date
 * @property string $observations
 * @property string $created_at
 * @property string $updated_at
 * @property Applicant $applicant
 * @property ConstructionEntity $constructionEntity
 */
class AssignmentHistory extends Model
{
    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $primaryKey = 'id';
    protected $keyType = 'integer';
    /**
     * @var array
     */
    protected $fillable = [ 'entity_id', 'user_id', 'applicant_id',  'assignment_date', 'observations', 'created_at', 'updated_at'];

    protected $with = ['applicant','constructionEntity'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function applicant()
    {
       
        return $this->belongsTo('App\Models\Applicant');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function constructionEntity()
    {
        //return $this->belongsTo('App\Models\ConstructionEntity');
        return $this->belongsTo('App\Models\ConstructionEntity');
    }


}
