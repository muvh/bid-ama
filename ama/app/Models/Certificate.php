<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Certificate extends Model
{
    protected $fillable = [
        'fecha_tr',
        'applicant_id',
        'type'

    ];


    protected $dates = [
        'fecha_tr',
        'created_at',
        'updated_at',

    ];

    protected $appends = ['resource_url'];
    protected $with = ['applicant'];

    /* ************************ ACCESSOR ************************* */

    public function getResourceUrlAttribute()
    {
        return url('/admin/certificates/'.$this->getKey());
    }

    public function applicant() {
        return $this->belongsTo('App\Models\Applicant');
    }
}
