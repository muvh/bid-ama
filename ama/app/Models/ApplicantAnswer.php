<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property integer $applicant_questionnaire_id
 * @property string $answer
 * @property string $extended_value
 * @property string $created_at
 * @property string $updated_at
 * @property ApplicantQuretionnaire $applicantQuretionnaire
 */
class ApplicantAnswer extends Model
{

    protected $primaryKey = null;
    public $incrementing = false;

    /**
     * @var array
     */
    protected $fillable = ['answer', 'extended_value', 'created_at', 'updated_at', 'question_id', 'received_at', 'questionnaire_template_id', 'applicant_id'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function questionnaireTemplate()
    {
        return $this->belongsTo('App\Models\QuestionnaireTemplate', 'questionnaire_template_id');
    }

    public function questionnaireTemplateQuestion()
    {
        return $this->belongsTo('App\Models\QuestionnaireTemplateQuestion', 'question_id');
    }

    public function applicant()
    {
        return $this->belongsTo('App\Models\Applicant', 'applicant_id');
    }
}
