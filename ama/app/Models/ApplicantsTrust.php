<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ApplicantsTrust extends Model
{
    protected $fillable = [
        'applicant_id',
        'trust_id',
    
    ];
    
    
    protected $dates = [
        'created_at',
        'updated_at',
    
    ];
    protected $with = ['applicant'];
    protected $appends = ['resource_url'];

    /* ************************ ACCESSOR ************************* */
    public function applicant()
    {
        return $this->belongsTo('App\Models\Applicant');
    }

    public function trust()
    {
              return $this->belongsTo('App\Models\Trust', 'trust_id')->withDefault([
            'name' => '',
        ]); 
    }



    public function getResourceUrlAttribute()
    {
        return url('/admin/applicants-trusts/'.$this->getKey());
    }
}
