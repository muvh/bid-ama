<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AccountingSummary extends Model
{
    protected $table = 'accounting_summary';

    protected $fillable = [
        'income',
        'expenses',
        'balance',
    
    ];
    
    
    protected $dates = [
        'created_at',
        'updated_at',
    
    ];
    
    protected $appends = ['resource_url'];

    /* ************************ ACCESSOR ************************* */

    public function getResourceUrlAttribute()
    {
        return url('/admin/accounting-summaries/'.$this->getKey());
    }
}
