<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ApplicantOwner extends Model
{
    protected $fillable = [
        'applicant_id',
        'admin_user_id',
    
    ];
    
    protected $with = ['admin_user'];
    protected $dates = [
        'created_at',
        'updated_at',
    
    ];
    
    protected $appends = ['resource_url'];

    /* ************************ ACCESSOR ************************* */

    public function getResourceUrlAttribute()
    {
        return url('/admin/applicant-owners/'.$this->getKey());
    }

    public function admin_user()
    {
        //return $this->hasOne(AdminUser::class, 'id', 'user_id');
        return $this->belongsTo('App\Models\AdminUser', 'admin_user_id', 'id');
    }
}
