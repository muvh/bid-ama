<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Brackets\Media\HasMedia\ProcessMediaTrait;
use Brackets\Media\HasMedia\AutoProcessMediaTrait;
use Brackets\Media\HasMedia\HasMediaCollectionsTrait;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\Models\Media;
use Brackets\Media\HasMedia\HasMediaThumbsTrait;

/**
 * @property int $id
 * @property integer $applicant_id
 * @property integer $document_id
 * @property string $created_at
 * @property string $updated_at
 * @property Applicant $applicant
 * @property DocumentType $documentType
 */
class ApplicantDocument extends Model implements HasMedia
{

    use ProcessMediaTrait;
    use AutoProcessMediaTrait;
    use HasMediaCollectionsTrait;
    use HasMediaThumbsTrait;

    public function registerMediaCollections() {
        $this->addMediaCollection('supporting-documents')
            ->maxNumberOfFiles(10)
            ->maxFilesize(30*1024*1024) // Set the file size limit
            ->accepts('application/pdf');

   
    }

    public function registerMediaConversions(Media $media = null)
    {
        $this->autoRegisterThumb200();
        $this->autoRegisterThumb150();
    }

    /**
     * Auto register thumb overridden
     */
    public function autoRegisterThumb200()
    {
        $this->getMediaCollections()->filter->isImage()->each(function ($mediaCollection) {
            $this->addMediaConversion('thumb_200')
                ->width(200)
                ->height(200)
                ->fit('crop', 200, 200)
                ->optimize()
                ->performOnCollections($mediaCollection->getName())
                ->nonQueued();
        });
    }

    public function autoRegisterThumb150()
    {
        $this->getMediaCollections()->filter->isImage()->each(function ($mediaCollection) {
            $this->addMediaConversion('thumb_150')
                ->width(150)
                ->height(150)
                ->fit('crop', 150, 150)
                ->optimize()
                ->performOnCollections($mediaCollection->getName())
                ->nonQueued();
        });
    }

    /**
     * @var array
     */
    protected $fillable = ['applicant_id', 'document_id', 'created_at', 'updated_at', 'received_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function applicant()
    {
        return $this->belongsTo('App\Models\Applicant');
    }

    protected $with = ['document'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function document()
    {
        return $this->belongsTo('App\Models\DocumentType', 'document_id');
    }

    protected $appends = ['resource_url', 'document_url'];

    /* ************************ ACCESSOR ************************* */

    public function getResourceUrlAttribute()
    {
        return url('/admin/applicant-documents/'.$this->getKey());
    }

    public function getDocumentUrlAttribute()
    {
        $mediaItems = $this->getMedia('supporting-documents');
        $publicUrl = isset($mediaItems[0]) ? url($mediaItems[0]->getUrl()): '';
        return $publicUrl;
    }



}
