<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ResourceTransfer extends Model
{
    protected $fillable = [
        'nro_solictud_rt',
        'fecha_rt',
        'type_order_id',
        'construction_entity_id',
        'culminado',
        'nro_nota_bid',
        'obs'

    ];


    protected $dates = [
        'fecha_rt',
        'created_at',
        'updated_at',

    ];
    protected $with = ['typeorder', 'construction', /*'applicantresource'*/];
    protected $appends = ['resource_url'];

    /* ************************ ACCESSOR ************************* */
    public function typeorder()
    {
        return $this->belongsTo('App\Models\TypeOrder', 'type_order_id')->withDefault([
            'title' => '',
        ]);
    }
    public function construction()
    {
        return $this->belongsTo('App\Models\ConstructionEntity', 'construction_entity_id')->withDefault([
            'name' => '',
        ]);
    }

    public function applicantresource()
    {
        return $this->belongsTo('App\Models\ApplicantResourceTransfer', 'id', 'resource_transfer_id');
    }

    public function getResourceUrlAttribute()
    {
        return url('/admin/resource-transfers/' . $this->getKey());
    }
}
