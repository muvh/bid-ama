<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Trust extends Model
{
    protected $fillable = [
        'nro_tr',
        'fecha_tr',
        'financial_entity_id',
        'culminado'
    
    ];
    
    protected $with = ['financial'];
    protected $dates = [
        'fecha_tr',
        'created_at',
        'updated_at',
    
    ];
    
    protected $appends = ['resource_url'];
    public function financial()
    {
        return $this->belongsTo('App\Models\FinancialEntity', 'financial_entity_id')->withDefault([
            'name' => '',
        ]);
    }



    /* ************************ ACCESSOR ************************* */

    public function getResourceUrlAttribute()
    {
        return url('/admin/trusts/'.$this->getKey());
    }
}
