<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ApplicantResourceTransfer extends Model
{
    protected $fillable = [
        'applicant_id',
        'resource_transfer_id',
    
    ];
    
    
    protected $dates = [
        'created_at',
        'updated_at',
    
    ];
    
    protected $appends = ['resource_url'];
    protected $with = ['applicant'];

    /* ************************ ACCESSOR ************************* */
    public function applicant()
    {
        return $this->belongsTo('App\Models\Applicant');
    }
    public function getResourceUrlAttribute()
    {
        return url('/admin/applicant-resource-transfers/'.$this->getKey());
    }
}
