<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ConstructionEntity extends Model
{
    protected $fillable = [
        'name',
        'bank_name',
        'cta_cte',
        'const_ruc',
        'razon_social',
        'repre',
        'nro_ci',
    
    ];
    
    
    protected $dates = [
        'created_at',
        'updated_at',
    
    ];
    
    protected $appends = ['resource_url'];

    /* ************************ ACCESSOR ************************* */
    public function adminUsers()
    {
        return $this->hasMany('App\Models\AdminUser', 'construction_entity');
    }

    public function getResourceUrlAttribute()
    {
        return url('/admin/construction-entities/'.$this->getKey());
    }
}
