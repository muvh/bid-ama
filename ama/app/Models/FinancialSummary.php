<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FinancialSummary extends Model
{
    protected $fillable = [
        'applicant_id',
        'desembolso',
        'date',
    
    ];
    
    
    protected $dates = [
        'date',
        'created_at',
        'updated_at',
    
    ];
    
    protected $appends = ['resource_url'];

    /* ************************ ACCESSOR ************************* */

    public function getResourceUrlAttribute()
    {
        return url('/admin/financial-summaries/'.$this->getKey());
    }
}
