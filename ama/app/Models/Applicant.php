<?php

namespace App\Models;

use Carbon\Carbon;
use App\Models\City;
use App\Models\Visit;
use Illuminate\Database\Eloquent\Model;


/**
 * @property integer $id
 * @property integer $state_id
 * @property integer $city_id
 * @property integer $education_level
 * @property integer $parent_applicant
 * @property integer $applicant_relationship
 * @property string $names
 * @property string $last_names
 * @property date $birthdate
 * @property date $registration_date
 * @property string $gender
 * @property string $government_id
 * @property string $marital_status
 * @property boolean $pregnant
 * @property integer $pregnancy_due_date
 * @property string $cadaster
 * @property string $property_id
 * @property string $occupation
 * @property float $monthly_income
 * @property State $state
 * @property City $city
 * @property EducationLevel $educationLevel
 * @property ApplicantRelationship $applicantRelationship
 * @property Applicant $applicant
 * @property ApplicantContactMethod[] $applicantContactMethods
 * @property ApplicantDisability[] $applicantDisabilities
 * @property ApplicantDisease[] $applicantDiseases
 * @property ApplicantQuestionnaire[] $applicantQuestionnaires
 * @property ApplicantStatus[] $applicantStatuses
 */
class Applicant extends Model
{

    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'integer';
    //protected $guarded = [];

    /**
     * @var array
     */
    protected $fillable = [
        'state_id',
        'city_id',
        'education_level',
        'parent_applicant',
        'applicant_relationship',
        'names',
        'last_names',
        'birthdate',
        'gender',
        'government_id',
        'marital_status',
        'pregnant',
        'pregnancy_due_date',
        'cadaster',
        'property_id',
        'occupation',
        'monthly_income',
        'nationality',
        'address',
        'neighborhood',
        'construction_entity',
        'financial_entity',
        'resolution_number',
        'file_number',
        'financial_entity_eval',
        'construction_entity_eval',
        'ruc',
        'created_by',
        'created_at',
        'updated_at',
        'origin',
        'registration_date',
        'atc_fec_asig',
        'padron',
        'resolution_date',
        'file_date',
        'familiar_setting_id'
    ];

    protected $appends = ['resource_url', 'full_name', 'birthdate'];
    protected $with = ['financial', 'construction', 'workdata', 'visit', 'city', 'educationlevel', 'atc', 'statuses', 'beneficiaries', 'conyuge', 'insurancePolicy', 'applicantsTrust', 'enviocac', 'envio20cac', 'work', 'finobras', 'enejecucion', 'relationship', 'applicantGetCellphone'];
    //protected $withCount = ['beneficiaries'];
    public function getFullNameAttribute()
    {
        return "{$this->names} {$this->last_names}";
    }

    public function getBirthdateAttribute()
    {
        return isset($this->attributes['birthdate']) ? (new Carbon($this->attributes['birthdate']))->format('Y-m-d') : null;
    }
    /*public function setAddressAttribute()
    {
        return $this->attributes['address'] = mb_strtoupper($this->address);
        //return ucfirst($this->address);
    }*/

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function state()
    {
        return $this->belongsTo('App\Models\State');
    }

    public function visits()
    {
        return $this->hasMany(Visit::class, 'applicant_id')->orderByRaw('CAST(visit_number AS INTEGER)');
    }

    public function conyuge()
    {
        return $this->hasOne('App\Models\Applicant', 'parent_applicant')->where('applicant_relationship', 1);
    }

    public function beneficiaries()
    {
        return $this->hasMany('App\Models\Applicant', 'parent_applicant');
        //return $this->hasMany('App\Models\Applicant')->where('parent_applicant', $this->id)->count();
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function city()
    {
        return $this->belongsTo(City::class, 'city_id')->withDefault([
            'name' => '',
        ]);
        //return $this->belongsTo('App\Models\City');
        //return $this->hasMany(City::class, 'id');
        //return $this->belongsTo('App\Models\Applicant');
    }
    public function familiarSetting()
    {
        /*return $this->belongsTo(City::class, 'city_id', 'id')->withDefault([
            'name' => '',
        ]);;*/
        return $this->belongsTo('App\Models\FamiliarSetting');
    }

    public function statuses()
    {
        return $this->hasOne('App\Models\ApplicantStatus')->latest('id');
    }

    public function visit()
    {
        return $this->hasOne('App\Models\Visit')->latest('id');
    }

    public function workdata()
    {
        return $this->hasOne('App\Models\WorksDatum');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function educationlevel()
    {
        return $this->belongsTo('App\Models\EducationLevel', 'education_level');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function applicant()
    {
        return $this->belongsTo('App\Models\Applicant', 'parent_applicant', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function applicantContactMethods()
    {
        return $this->hasMany('App\Models\ApplicantContactMethod');
    }

    public function applicantGetCellphone()
    {
        return $this->hasOne('App\Models\ApplicantContactMethod')->where('contact_method_id', 3);
        //return $this->hasOne('App\Models\Applicant', 'parent_applicant')->where('applicant_relationship', 1);
        //return '3';
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function applicantDisabilities()
    {
        return $this->hasMany('App\Models\ApplicantDisability');
    }

    public function reportDisabilities()
    {
        return $this->hasOne('App\Models\ApplicantDisability')->latest();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function applicantDiseases()
    {
        return $this->hasMany('App\Models\ApplicantDisease');
    }

    public function reportDiseases()
    {
        return $this->hasOne('App\Models\ApplicantDisease')->latest();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function applicantQuestionnaires()
    {
        return $this->hasMany('App\Models\ApplicantQuestionnaire');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function applicantStatuses()
    {
        return $this->hasMany('App\Models\ApplicantStatus');
    }

    public function insurancePolicy()
    {
        return $this->belongsTo('App\Models\InsurancePolicy');
        //return $this->belongsTo('App\Models\InsurancePolicy', 'applicant_id');
        //return $this->hasMany('App\Models\InsurancePolicy', 'applicant_id', 'applicant_id');
    }
    public function trust()
    {
        return $this->hasMany('App\Models\ApplicantsTrust');
    }
    public function relationship()
    {
        return $this->belongsTo('App\Models\ApplicantRelationship', 'applicant_relationship');
    }
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */

    /*    public function applicantrelationship()
    {
        //return $this->belongsTo('App\Models\ApplicantRelationship', 'applicant_relationship');
        return $this->belongsTo('App\Models\ApplicantRelationship', 'applicant_relationship')->withDefault([
            'name' => '',
        ]);
    } */
    public function applicantsTrust()
    {
        return $this->belongsTo('App\Models\ApplicantsTrust', 'applicant_id');
    }


    public function financial()
    {
        return $this->belongsTo('App\Models\FinancialEntity', 'financial_entity')->withDefault([
            'name' => '',
        ]);
    }
    public function enviocac()
    { //reporte de applicants.estados
        return $this->hasOne('App\Models\ApplicantStatus')->where('status_id', 41)->latest('id');
        //return $this->hasOne('App\Models\ApplicantContactMethod')->where('contact_method_id', 3);
    }
    public function envio20cac()
    { //reporte de applicants.estados
        return $this->hasOne('App\Models\ApplicantStatus')->where('status_id', 43)->latest('id');
        //return $this->hasOne('App\Models\ApplicantContactMethod')->where('contact_method_id', 3);
    }
    public function enejecucion()
    {
        return $this->hasOne('App\Models\ApplicantStatus')->where('status_id', 25);
        //return $this->hasOne('App\Models\ApplicantContactMethod')->where('contact_method_id', 3);
    }

    public function work()
    {
        return $this->hasMany('App\Models\Work');
    }

    public function construction()
    {
        return $this->belongsTo('App\Models\ConstructionEntity', 'construction_entity')->withDefault([
            'name' => '',
        ]);
    }

    public function atc()
    {
        return $this->belongsTo('App\Models\ConstructionEntity', 'construction_entity', 'id');
    }
    public function finobras()
    {
        ///return $this->hasOne('App\Models\WorksTypeCheck', 'work_id')->where('type_check_id', 1);
        return $this->hasOne('App\Models\WorksTypeCheck')->where('type_check_id', 2);
        //return $this->hasOne('App\Models\WorksTypeCheck')->where('type_check_id', 3)->latest('id');
        // return $this->hasMany('App\Models\Work');
    }
    /* ************************ ACCESSOR ************************* */

    public function getResourceUrlAttribute()
    {
        return url('/admin/applicants/' . $this->getKey());
    }
    public function getDateRegistertAttribute()
    {
        return isset($this->attributes['date_register']) ? (new Carbon($this->attributes['date_register']))->format('Y-m-d') : null;
    }
}
