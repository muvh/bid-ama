<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WorksTypeCheck extends Model
{
    protected $fillable = [
        'work_id',
        'applicant_id',
        'type_check_id',
        'fecha_check',
    
    ];
    protected $with = ['typecheck'];
    
    protected $dates = [
        'created_at',
        'updated_at',
    
    ];
    
    protected $appends = ['resource_url'];
    public function typecheck()
    {
        return $this->belongsTo('App\Models\TypeCheck', 'type_check_id')->withDefault([
            'names' => '',
        ]);
        //return $this->belongsTo('App\Models\FinancialEntity', 'education_level');
    }



    /* ************************ ACCESSOR ************************* */

    public function getResourceUrlAttribute()
    {
        return url('/admin/works-type-checks/'.$this->getKey());
    }
}

