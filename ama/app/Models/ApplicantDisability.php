<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property integer $applicant_id
 * @property integer $disability_id
 * @property string $created_at
 * @property string $updated_at
 * @property Disability $disability
 * @property Applicant $applicant
 */
class ApplicantDisability extends Model
{

    protected $primaryKey = null;
    public $incrementing = false;

    /**
     * @var array
     */
    protected $fillable = ['applicant_id', 'disability_id', 'created_at', 'updated_at'];
    protected $with = ['disability'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function disability()
    {
        return $this->belongsTo('App\Models\Disability');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function applicant()
    {
        return $this->belongsTo('App\Models\Applicant');
    }
}
