<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ApplicantRecord extends Model
{
    protected $fillable = [
        'accounting_record_id',
        'applicant_id',
        'amount',

    ];


    protected $dates = [
        'created_at',
        'updated_at',

    ];

    protected $appends = ['resource_url'];
    protected $with = ['applicant'];

    /* ************************ ACCESSOR ************************* */

    public function getResourceUrlAttribute()
    {
        return url('/admin/applicant-records/' . $this->getKey());
    }

    public function applicant()
    {
        return $this->belongsTo('App\Models\Applicant', 'applicant_id');
    }
}
