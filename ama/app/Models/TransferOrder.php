<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TransferOrder extends Model
{
    protected $fillable = [
        'nro_solicitud',
        'fecha_ot',
        'importe',
        'type_order_id',
        'construction_entity_id',
        'applicant_id',
        'financial_entity_id',
    
    ];
    protected $with = ['financial', 'construction', 'applicant', 'typeorder'];

    public function applicant()
    {
        return $this->belongsTo('App\Models\Applicant', 'applicant_id')->withDefault([
            'names' => '',
        ]);
        //return $this->belongsTo('App\Models\FinancialEntity', 'education_level');
    }
    public function financial()
    {
        return $this->belongsTo('App\Models\FinancialEntity', 'financial_entity_id')->withDefault([
            'name' => '',
        ]);
        //return $this->belongsTo('App\Models\FinancialEntity', 'education_level');
    }
    public function typeorder()
    {
        return $this->belongsTo('App\Models\TypeOrder', 'type_order_id')->withDefault([
            'name' => '',
        ]);
        //return $this->belongsTo('App\Models\FinancialEntity', 'education_level');
    }
    
    public function construction()
    {
        return $this->belongsTo('App\Models\ConstructionEntity', 'construction_entity_id')->withDefault([
            'name' => '',
        ]);
    }

    protected $dates = [
        'fecha_ot',
        'created_at',
        'updated_at',
    
    ];
    
    protected $appends = ['resource_url'];

    /* ************************ ACCESSOR ************************* */

    public function getResourceUrlAttribute()
    {
        return url('/admin/transfer-orders/'.$this->getKey());
    }
}
