<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;

use Illuminate\Database\Eloquent\Model;

class AccountingRecord extends Model
{
    protected $table = 'accounting_records';

    protected $fillable = [
        'summary',
        'type_movements_id',
        'amount',
        'balance',
        'confirmed',
        'resolution',
        'registration_date',

    ];

    protected $casts = [
        'confirmed' => 'boolean',
    ];


    protected $dates = [
        'registration_date',
        'created_at',
        'updated_at',

    ];
    
    protected $withCount = ['beneficiaries'];
    protected $with = ['typeMovement','beneficiaries'];
    protected $appends = ['resource_url'];

    /* ************************ ACCESSOR ************************* */

    public function getResourceUrlAttribute()
    {
        return url('/admin/accounting-records/' . $this->getKey());
    }

    public function beneficiaries()
    {
        return $this->hasMany('App\Models\ApplicantRecord', 'accounting_record_id');
        //return $this->hasMany('App\Models\Applicant')->where('parent_applicant', $this->id)->count();
    }

    public function balance()
    {
        return $this->hasMany('App\Models\ApplicantRecord', 'accounting_record_id');
        //return $this->hasMany('App\Models\Applicant')->where('parent_applicant', $this->id)->count();
    }

    public function typeMovement()
    {
        //return $this->belongsTo('App\Models\TypeMovement', 'type_movements_id');
        return $this->belongsTo('App\Models\TypeMovement', 'type_movements_id',);;
    }
}
