<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class InsurancePolicy extends Model
{
    protected $fillable = [
        'nro_poliza',
        'concepto',
        'fecha_vigencia',
        'fecha_vencimiento',
        'fecha_desembolso',
        'importe',
        'applicant_id',
        'insurance_entity_id',
        'construction_entity_id',
        'fecha_envio_cac',
        'culminado',
    
    ];
    
    
    protected $dates = [
        'fecha_vigencia',
        'fecha_vencimiento',
        'fecha_desembolso',
       
        'created_at',
        'updated_at',
    
    ];
    protected $with = ['insuranceEntity','construction', 'applicant'];
    protected $appends = ['resource_url', 'dias'];

    /* ************************ ACCESSOR ************************* */
    public function insuranceEntity()
    {
        return $this->belongsTo(InsuranceEntity::class, 'insurance_entity_id', 'id')->withDefault([
            'title' => '',
        ]);
        //return $this->belongsTo('App\Models\InsuranceEntity');
    }
    public function construction()
    {
        return $this->belongsTo('App\Models\ConstructionEntity', 'construction_entity_id')->withDefault([
            'name' => '',
        ]);
    }
    public function applicant()
    {
        return $this->belongsTo('App\Models\Applicant', 'applicant_id')->withDefault([
            'names' => '',
        ]);
    }

    public function getResourceUrlAttribute() {
        return url('/admin/insurance-policies/'.$this->getKey());
    }

    public function getDiasAttribute(){
        $fecha1 = date_create($this->fecha_vencimiento);
        $fecha2 = date_create(Carbon::now());
        $dias = date_diff($fecha2, $fecha1)->format('%R%a');
        return $dias.' Días';
    }


}
