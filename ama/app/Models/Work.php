<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Work extends Model
{
    protected $fillable = [
        'fecha_ws',
        'applicant_id',
        'supervisor_id',
    
    ];
    
    
    protected $dates = [
        'fecha_ws',
        'created_at',
        'updated_at',
    
    ];
    
    protected $appends = ['resource_url'];
    protected $with = [
       // 'applicant', 
        'supervisor','obrasfecha'];
    /* ************************ ACCESSOR ************************* */
 /*    public function applicant()
    {
        return $this->belongsTo('App\Models\Applicant', 'applicant_id')->withDefault([
            'names' => '',
        ]);
        //return $this->belongsTo('App\Models\FinancialEntity', 'education_level');
    } */
    public function supervisor()
    {
        return $this->belongsTo('App\Models\Supervisor', 'supervisor_id')->withDefault([
            'names' => '',
        ]);
        //return $this->belongsTo('App\Models\FinancialEntity', 'education_level');
    }
    public function obrasfecha()
    {
      /*   return $this->belongsTo('App\Models\WorksTypeCheck', 'work_id')->withDefault([
            'names' => '',
        ]); */
        //return $this->belongsTo('App\Models\WorksTypeCheck', 'work_id');
        return $this->hasMany('App\Models\WorksTypeCheck');
    }
    public function getResourceUrlAttribute()
    {
        return url('/admin/works/'.$this->getKey());
    }
}
