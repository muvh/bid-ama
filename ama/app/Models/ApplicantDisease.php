<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property integer $applicant_id
 * @property integer $disease_id
 * @property string $created_at
 * @property string $updated_at
 * @property Disease $disease
 * @property Applicant $applicant
 */
class ApplicantDisease extends Model
{

    protected $primaryKey = null;
    public $incrementing = false;

    /**
     * @var array
     */
    protected $fillable = ['applicant_id', 'disease_id', 'created_at', 'updated_at'];
    protected $with = ['disease'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function disease()
    {
        return $this->belongsTo('App\Models\Disease');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function applicant()
    {
        return $this->belongsTo('App\Models\Applicant');
    }
}
