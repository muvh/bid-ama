<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property integer $applicant_id
 * @property integer $status_id
 * @property int $user_id
 * @property string $created_at
 * @property string $updated_at
 * @property string description
 * @property Applicant $applicant
 * @property Status $status
 */
class ApplicantStatus extends Model
{
    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['applicant_id', 'status_id', 'user_id', 'created_at', 'updated_at', 'description'];

    protected $with = ['status', 'user', 'admin_user'];

    protected $guarded = [];
    //protected $dates = ['created_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function applicant()
    {
        return $this->belongsTo('App\Models\Applicant');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function status()
    {
        return $this->belongsTo('App\Models\Status');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\AdminUser', 'user_id', 'id');
    }
    public function admin_user()
    {
        return $this->hasOne(AdminUser::class, 'id', 'user_id');
        return $this->belongsTo('App\Models\AdminUser', 'user_id', 'id');
    }

}
