<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
/**
 * @property integer $id
 * */
class Status extends Model
{
    protected $fillable = [
        'name' , 
        'prefix'
    ];

    protected $dates = [
        'created_at',
        'updated_at',
    ];
    protected $keyType = 'integer';
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */

    protected $appends = ['resource_url'];

    /* ************************ ACCESSOR ************************* */

    public function getResourceUrlAttribute()
    {
        return url('/admin/statuses/'.$this->getKey());
    }

    public function getmenu(){
        
        return $this->orderby('prefix', 'ASC')
        ->get()
        ->toArray();
    }

}
