<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FinancialEntity extends Model
{
    protected $fillable = [
        'name',
        'bank_name',
        'address',
        'cta_cte',
        'const_ruc',
        'razon_social',
    
    ];
    
    
    protected $dates = [
        'created_at',
        'updated_at',
    
    ];
    
    protected $appends = ['resource_url'];
    public function adminUsers()
    {
        return $this->hasMany('App\Models\AdminUser', 'financial_entity');
    }

    /* ************************ ACCESSOR ************************* */

    public function getResourceUrlAttribute()
    {
        return url('/admin/financial-entities/'.$this->getKey());
    }
}
