<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\CertificatesController;

Route::get('/', function () {
    return view('auth.requisitos');
});

Auth::routes();


Route::get('/home', 'HomeController@index')->name('home');
Route::get('/obras', 'Admin\WorksDataController@indexapi')->name('home');

//Route::get('/applicants/search', [App\Http\Controllers\Admin\CertificatesController::class,'searchApplicants']);




/* Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(static function () {
    Route::get('admin', function () {
        return view('admin.admin-home');
    });
}); */

/* Auto-generated admin routes */
Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(static function () {
    Route::prefix('admin')->namespace('Admin')->name('admin/')->group(static function () {
        Route::prefix('admin-users')->name('admin-users/')->group(static function () {
            Route::get('/',                                             'AdminUsersController@index')->name('index');
            Route::get('/create',                                       'AdminUsersController@create')->name('create');
            Route::post('/',                                            'AdminUsersController@store')->name('store');
            Route::get('/{adminUser}/edit',                             'AdminUsersController@edit')->name('edit');
            Route::post('/{adminUser}',                                 'AdminUsersController@update')->name('update');
            Route::delete('/{adminUser}',                               'AdminUsersController@destroy')->name('destroy');
            Route::get('/{adminUser}/resend-activation',                'AdminUsersController@resendActivationEmail')->name('resendActivationEmail');
        });
    });
});

/* Auto-generated admin routes */
Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(static function () {
    Route::prefix('admin')->namespace('Admin')->name('admin/')->group(static function () {
        Route::get('/profile',                                      'ProfileController@editProfile')->name('edit-profile');
        Route::post('/profile',                                     'ProfileController@updateProfile')->name('update-profile');
        Route::get('/password',                                     'ProfileController@editPassword')->name('edit-password');
        Route::post('/password',                                    'ProfileController@updatePassword')->name('update-password');
        Route::get('/',                                             'ApplicantsController@gerencial')->name('gerencial');
    });
});

/* Auto-generated admin routes */
Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(static function () {
    Route::prefix('admin')->namespace('Admin')->name('admin/')->group(static function () {
        Route::prefix('cities')->name('cities/')->group(static function () {
            Route::get('/',                                             'CitiesController@index')->name('index');
            Route::get('/create',                                       'CitiesController@create')->name('create');
            Route::post('/',                                            'CitiesController@store')->name('store');
            Route::get('/{city}/edit',                                  'CitiesController@edit')->name('edit');
            Route::post('/bulk-destroy',                                'CitiesController@bulkDestroy')->name('bulk-destroy');
            Route::post('/{city}',                                      'CitiesController@update')->name('update');
            Route::delete('/{city}',                                    'CitiesController@destroy')->name('destroy');
        });
    });
});

/* Auto-generated admin routes */
Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(static function () {
    Route::prefix('admin')->namespace('Admin')->name('admin/')->group(static function () {
        Route::prefix('contact-methods')->name('contact-methods/')->group(static function () {
            Route::get('/',                                             'ContactMethodsController@index')->name('index');
            Route::get('/create',                                       'ContactMethodsController@create')->name('create');
            Route::post('/',                                            'ContactMethodsController@store')->name('store');
            Route::get('/{contactMethod}/edit',                         'ContactMethodsController@edit')->name('edit');
            Route::post('/bulk-destroy',                                'ContactMethodsController@bulkDestroy')->name('bulk-destroy');
            Route::post('/{contactMethod}',                             'ContactMethodsController@update')->name('update');
            Route::delete('/{contactMethod}',                           'ContactMethodsController@destroy')->name('destroy');
        });
    });
});

/* Auto-generated admin routes */
Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(static function () {
    Route::prefix('admin')->namespace('Admin')->name('admin/')->group(static function () {
        Route::prefix('disabilities')->name('disabilities/')->group(static function () {
            Route::get('/',                                             'DisabilitiesController@index')->name('index');
            Route::get('/create',                                       'DisabilitiesController@create')->name('create');
            Route::post('/',                                            'DisabilitiesController@store')->name('store');
            Route::get('/{disability}/edit',                            'DisabilitiesController@edit')->name('edit');
            Route::post('/bulk-destroy',                                'DisabilitiesController@bulkDestroy')->name('bulk-destroy');
            Route::post('/{disability}',                                'DisabilitiesController@update')->name('update');
            Route::delete('/{disability}',                              'DisabilitiesController@destroy')->name('destroy');
        });
    });
});

/* Auto-generated admin routes */
Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(static function () {
    Route::prefix('admin')->namespace('Admin')->name('admin/')->group(static function () {
        Route::prefix('diseases')->name('diseases/')->group(static function () {
            Route::get('/',                                             'DiseasesController@index')->name('index');
            Route::get('/create',                                       'DiseasesController@create')->name('create');
            Route::post('/',                                            'DiseasesController@store')->name('store');
            Route::get('/{disease}/edit',                               'DiseasesController@edit')->name('edit');
            Route::post('/bulk-destroy',                                'DiseasesController@bulkDestroy')->name('bulk-destroy');
            Route::post('/{disease}',                                   'DiseasesController@update')->name('update');
            Route::delete('/{disease}',                                 'DiseasesController@destroy')->name('destroy');
        });
    });
});

/* Auto-generated admin routes */
Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(static function () {
    Route::prefix('admin')->namespace('Admin')->name('admin/')->group(static function () {
        Route::prefix('education-levels')->name('education-levels/')->group(static function () {
            Route::get('/',                                             'EducationLevelsController@index')->name('index');
            Route::get('/create',                                       'EducationLevelsController@create')->name('create');
            Route::post('/',                                            'EducationLevelsController@store')->name('store');
            Route::get('/{educationLevel}/edit',                        'EducationLevelsController@edit')->name('edit');
            Route::post('/bulk-destroy',                                'EducationLevelsController@bulkDestroy')->name('bulk-destroy');
            Route::post('/{educationLevel}',                            'EducationLevelsController@update')->name('update');
            Route::delete('/{educationLevel}',                          'EducationLevelsController@destroy')->name('destroy');
        });
    });
});

/* Auto-generated admin routes */
Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(static function () {
    Route::prefix('admin')->namespace('Admin')->name('admin/')->group(static function () {
        Route::prefix('states')->name('states/')->group(static function () {
            Route::get('/',                                             'StatesController@index')->name('index');
            Route::get('/create',                                       'StatesController@create')->name('create');
            Route::post('/',                                            'StatesController@store')->name('store');
            Route::get('/{state}/edit',                                 'StatesController@edit')->name('edit');
            Route::post('/bulk-destroy',                                'StatesController@bulkDestroy')->name('bulk-destroy');
            Route::post('/{state}',                                     'StatesController@update')->name('update');
            Route::delete('/{state}',                                   'StatesController@destroy')->name('destroy');
        });
    });
});

/* Auto-generated admin routes */
Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(static function () {
    Route::prefix('admin')->namespace('Admin')->name('admin/')->group(static function () {
        Route::prefix('statuses')->name('statuses/')->group(static function () {
            Route::get('/',                                             'StatusesController@index')->name('index');
            Route::get('/create',                                       'StatusesController@create')->name('create');
            Route::post('/',                                            'StatusesController@store')->name('store');
            Route::get('/{status}/edit',                                'StatusesController@edit')->name('edit');
            Route::post('/bulk-destroy',                                'StatusesController@bulkDestroy')->name('bulk-destroy');
            Route::post('/{status}',                                    'StatusesController@update')->name('update');
            Route::delete('/{status}',                                  'StatusesController@destroy')->name('destroy');
        });
    });
});

/* Auto-generated admin routes */
Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(static function () {
    Route::prefix('admin')->namespace('Admin')->name('admin/')->group(static function () {
        Route::prefix('questionnaire-types')->name('questionnaire-types/')->group(static function () {
            Route::get('/',                                             'QuestionnaireTypesController@index')->name('index');
            Route::get('/create',                                       'QuestionnaireTypesController@create')->name('create');
            Route::post('/',                                            'QuestionnaireTypesController@store')->name('store');
            Route::get('/{questionnaireType}/edit',                     'QuestionnaireTypesController@edit')->name('edit');
            Route::post('/bulk-destroy',                                'QuestionnaireTypesController@bulkDestroy')->name('bulk-destroy');
            Route::post('/{questionnaireType}',                         'QuestionnaireTypesController@update')->name('update');
            Route::delete('/{questionnaireType}',                       'QuestionnaireTypesController@destroy')->name('destroy');
        });
    });
});

/* Auto-generated admin routes */
Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(static function () {
    Route::prefix('admin')->namespace('Admin')->name('admin/')->group(static function () {
        Route::prefix('applicant-relationships')->name('applicant-relationships/')->group(static function () {
            Route::get('/',                                             'ApplicantRelationshipsController@index')->name('index');
            Route::get('/create',                                       'ApplicantRelationshipsController@create')->name('create');
            Route::post('/',                                            'ApplicantRelationshipsController@store')->name('store');
            Route::get('/{applicantRelationship}/edit',                 'ApplicantRelationshipsController@edit')->name('edit');
            Route::post('/bulk-destroy',                                'ApplicantRelationshipsController@bulkDestroy')->name('bulk-destroy');
            Route::post('/{applicantRelationship}',                     'ApplicantRelationshipsController@update')->name('update');
            Route::delete('/{applicantRelationship}',                   'ApplicantRelationshipsController@destroy')->name('destroy');
        });
    });
});

/* Auto-generated admin routes */
Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(static function () {
    Route::prefix('admin')->namespace('Admin')->name('admin/')->group(static function () {
        Route::prefix('applicants')->name('applicants/')->group(static function () {
            Route::get('/',                                             'ApplicantsController@index')->name('index');
            Route::get('/processed',                                    'ApplicantsController@processed')->name('processed');
            Route::get('/estados/{status}',                            'ApplicantsController@estados')->name('estados');
            Route::get('/levels/{status}',                            'ApplicantsController@levels')->name('levels');
            Route::get('/busqueda-ci/{government_id}',                      'ApplicantsController@busquedaci')->name('busqueda-ci');
            Route::post('/processFormSearch',                      'ApplicantsController@processFormSearch')->name('busqueda-ci');

            Route::get('/calif-proyectos/{status}',               'ApplicantsController@califProyectos')->name('califProyectos');
            Route::get('/generacion-op-20/{status}',               'ApplicantsController@generacionOp20')->name('generacionOp20');
            Route::get('/migrated',                                     'ApplicantsController@migrated')->name('migrated');
            Route::get('/assignedatc',                                   'ApplicantsController@assignedatc')->name('assignedatc');
            Route::get('/assignedcoop',                                  'ApplicantsController@assignedcoop')->name('assignedcoop');
            Route::get('/recordnumber',                                  'ApplicantsController@recordnumber')->name('recordnumber');
            Route::get('/disqualified',                                  'ApplicantsController@disqualified')->name('disqualified');
            Route::get('/beneficiary/{id}',                             'ApplicantsController@indexBeneficiary')->name('index-beneficiary');
            Route::get('/documents/{id}/{type}',                        'ApplicantsController@indexDocuments')->name('index-documents');
            Route::get('/documents/create/{id}/{type}',                 'ApplicantsController@documentCreate')->name('document-create');
            Route::post('/documents/{id}/{type}/edit',                  'ApplicantsController@documentUpdate')->name('document-update');
            Route::post('/documents/create/{id}/{type}',                'ApplicantsController@storeApplicantDocument')->name('document-store');
            Route::get('/create',                                       'ApplicantsController@create')->name('create');
            Route::get('/export/{status}',                                       'ApplicantsController@export')->name('export');
            Route::get('/graphic',                                      'ApplicantsController@graphic')->name('graphic');
            Route::get('/gerencial',                                      'ApplicantsController@gerencial')->name('gerencial');
            Route::post('/',                                            'ApplicantsController@store')->name('store');
            Route::get('/{applicant}/edit',                                          'ApplicantsController@edit')->name('edit');
            Route::get('/{applicant}/migrations',                       'ApplicantsController@migrations')->name('migrations');
            Route::get('/{applicant}/show',                             'ApplicantsController@show')->name('show');
            Route::get('/import',                                       'ApplicantsController@import')->name('import');
            Route::get('/reminderpn',                                   'ApplicantsController@reminderpn')->name('reminderpn');
            Route::get('/importdata',                                   'ApplicantsController@importdata')->name('importdata');
            Route::post('/importdata',                                  'ApplicantsController@storedata')->name('importdata');
            Route::get('/newimportdata',                                'ApplicantsController@newimportdata')->name('newimportdata');
            Route::post('/newstoredata',                                'ApplicantsController@newstoredata')->name('newstoredata');
            Route::post('/storedata',                                   'ApplicantsController@storedata')->name('storedata');
            Route::get('/{applicant}/create-beneficiary',               'ApplicantsController@createBeneficiary')->name('create-beneficiary');
            Route::post('/{applicant}/create-beneficiary',              'ApplicantsController@storeBeneficiary')->name('store-beneficiary');
            Route::get('/{applicant}/edit-beneficiary/{beneficiary}',   'ApplicantsController@editBeneficiary')->name('edit-beneficiary');
            Route::post('/{applicant}/edit-beneficiary/{beneficiary}',  'ApplicantsController@updateBeneficiary')->name('update-beneficiary');

            Route::get('/{applicant}/showvisit',                         'VisitsController@show');
            Route::get('/{applicant}/visits/create',                      'VisitsController@createvisit');

            Route::get('/{applicant}/workcreate',                           'WorksDataController@create');
            //Route::get('/{applicant}/workedit',                             'WorksDataController@edit');
            //Route::get('/{applicant}/{visit}/syncimage',                 'VisitsController@syncimage')->name('syncimage');

            Route::post('/{applicant}/applicants-questionnaire/{template}',        'ApplicantsController@questionnaire');

            Route::post('/bulk-destroy',                                'ApplicantsController@bulkDestroy')->name('bulk-destroy');
            Route::post('/{applicant}',                                 'ApplicantsController@update')->name('update');
            Route::delete('/{applicant}',                               'ApplicantsController@destroy')->name('destroy');
            Route::get('/{id}/identificaciones',                        'ApplicantsController@getIdentificaciones')->name('identificaciones');
            Route::get('/{id}/hadbenefit',                              'ApplicantsController@hadBenefit')->name('hadbenefit');
            Route::get('/{id}/findapplicant',                              'ApplicantsController@findapplicant')->name('findapplicant');
            Route::get('/{id}/findapplicantrecord',                        'ApplicantsController@findApplicantRecord')->name('findapplicant');
            Route::get('/{applicant}/print',                                 'ApplicantsController@print')->name('print');

            Route::post('/{applicant}/confirm-coop',                           'ApplicantsController@confirmCoop');
            Route::post('/{applicant}/confirm-ama',                            'ApplicantsController@confirmAma');
            Route::post('/{applicant}/confirm-atc',                            'ApplicantsController@confirmAtc');

            Route::post('/{applicant}/form-status',                            'ApplicantsController@formStatus');
            Route::post('/{applicant}/assig-coop',                             'ApplicantsController@assigCoop');
            Route::post('/addsupervisor/{applicant}',                             'ApplicantsController@addsupervisor');
            Route::post('/addtype/{applicant}',                                            'ApplicantsController@addtype');
            Route::post('/estadosstore',                                'ApplicantsController@estadosstore')->name('estadosstore');
            Route::get('/importestados',                                'ApplicantsController@importestados')->name('importestados');
            Route::post('/{applicant}/assig-atc-edit',                             'ApplicantsController@assigAtcEdit');
            Route::post('/{applicant}/reassig-atc',                             'ApplicantsController@reassigAtc');
        });
    });
});


/* Auto-generated admin routes */
Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(static function () {
    Route::prefix('admin')->namespace('Admin')->name('admin/')->group(static function () {
        Route::prefix('questionnaire-templates')->name('questionnaire-templates/')->group(static function () {
            Route::get('/',                                             'QuestionnaireTemplatesController@index')->name('index');
            Route::get('/create',                                       'QuestionnaireTemplatesController@create')->name('create');
            Route::post('/',                                            'QuestionnaireTemplatesController@store')->name('store');
            Route::get('/{questionnaireTemplate}/edit',                 'QuestionnaireTemplatesController@edit')->name('edit');
            Route::post('/bulk-destroy',                                'QuestionnaireTemplatesController@bulkDestroy')->name('bulk-destroy');
            Route::post('/{questionnaireTemplate}',                     'QuestionnaireTemplatesController@update')->name('update');
            Route::delete('/{questionnaireTemplate}',                   'QuestionnaireTemplatesController@destroy')->name('destroy');
        });
    });
});


/* Auto-generated admin routes */
Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(static function () {
    Route::prefix('admin')->namespace('Admin')->name('admin/')->group(static function () {
        Route::prefix('document-types')->name('document-types/')->group(static function () {
            Route::get('/',                                             'DocumentTypesController@index')->name('index');
            Route::get('/create',                                       'DocumentTypesController@create')->name('create');
            Route::post('/',                                            'DocumentTypesController@store')->name('store');
            Route::get('/{documentType}/edit',                          'DocumentTypesController@edit')->name('edit');
            Route::post('/bulk-destroy',                                'DocumentTypesController@bulkDestroy')->name('bulk-destroy');
            Route::post('/{documentType}',                              'DocumentTypesController@update')->name('update');
            Route::delete('/{documentType}',                            'DocumentTypesController@destroy')->name('destroy');
        });
    });
});

/* Auto-generated admin routes */
Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(static function () {
    Route::prefix('admin')->namespace('Admin')->name('admin/')->group(static function () {
        Route::prefix('applicant-documents')->name('applicant-documents/')->group(static function () {
            Route::get('/',                                             'ApplicantDocumentsController@index')->name('index');
            Route::get('/create',                                       'ApplicantDocumentsController@create')->name('create');
            Route::post('/',                                            'ApplicantDocumentsController@store')->name('store');
            Route::get('/{applicantDocument}/edit',                     'ApplicantDocumentsController@edit')->name('edit');
            Route::post('/bulk-destroy',                                'ApplicantDocumentsController@bulkDestroy')->name('bulk-destroy');
            Route::post('/{applicantDocument}',                         'ApplicantDocumentsController@update')->name('update');
            Route::delete('/{applicantDocument}',                       'ApplicantDocumentsController@destroy')->name('destroy');
        });
    });
});


/* Auto-generated admin routes */
Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(static function () {
    Route::prefix('admin')->namespace('Admin')->name('admin/')->group(static function () {
        Route::prefix('construction-entities')->name('construction-entities/')->group(static function () {
            Route::get('/',                                             'ConstructionEntitiesController@index')->name('index');
            Route::get('/create',                                       'ConstructionEntitiesController@create')->name('create');
            Route::post('/',                                            'ConstructionEntitiesController@store')->name('store');
            Route::get('/{constructionEntity}/edit',                    'ConstructionEntitiesController@edit')->name('edit');
            Route::post('/bulk-destroy',                                'ConstructionEntitiesController@bulkDestroy')->name('bulk-destroy');
            Route::post('/{constructionEntity}',                        'ConstructionEntitiesController@update')->name('update');
            Route::delete('/{constructionEntity}',                      'ConstructionEntitiesController@destroy')->name('destroy');
        });
    });
});

/* Auto-generated admin routes */
Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(static function () {
    Route::prefix('admin')->namespace('Admin')->name('admin/')->group(static function () {
        Route::prefix('financial-entities')->name('financial-entities/')->group(static function () {
            Route::get('/',                                             'FinancialEntitiesController@index')->name('index');
            Route::get('/create',                                       'FinancialEntitiesController@create')->name('create');
            Route::post('/',                                            'FinancialEntitiesController@store')->name('store');
            Route::get('/{financialEntity}/edit',                       'FinancialEntitiesController@edit')->name('edit');
            Route::post('/bulk-destroy',                                'FinancialEntitiesController@bulkDestroy')->name('bulk-destroy');
            Route::post('/{financialEntity}',                           'FinancialEntitiesController@update')->name('update');
            Route::delete('/{financialEntity}',                         'FinancialEntitiesController@destroy')->name('destroy');
        });
    });
});

Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(static function () {
    Route::prefix('admin')->namespace('Admin')->name('admin/')->group(static function () {
        Route::prefix('reports')->name('reports/')->group(static function () {
            Route::get('/applicants-by-resolution',                                                 'ReportsController@applicantsByResolution')->name('applicants-by-resolution');
            Route::get('/applicants-by-resolution/{resolution}',                                    'ReportsController@applicantsByResolution')->name('applicants-by-resolution');
            Route::get('/search-names',                                                             'ReportsController@applicantsSearchNames')->name('applicantsSearchNames');
            Route::get('/applicants-by-resolution/export/{resolution}',                             'ReportsController@applicantsByResolutionExport')->name('applicants-by-resolution');
            Route::get('/verificacion-institucional',                                               'ReportsController@verificacionInstitucional')->name('verificacionInstitucional');
            Route::get('/verificacion-institucional/{governmentid}',                                               'ReportsController@verificacionInstitucional')->name('verificacionInstitucional');
            Route::get('/searchci',                                                                 'ReportsController@searchci')->name('searchci');
            Route::get('/searchci/{governmentid}',                                                  'ReportsController@searchci')->name('searchci');
            Route::get('/applicants-by-resolution-social',                                          'ReportsController@applicantsByResolutionSocial')->name('applicants-by-resolution-social');
            Route::post('/applicants-by-resolution-social',                                         'ReportsController@applicantsByResolutionSocial')->name('applicantsByResolutionSocial');
            Route::get('/applicants-by-resolution-social/export/{date_to}/{date_from}/{Status}',    'ReportsController@applicantsByResolutionSocialExport')->name('applicants-by-resolution-social-export');
            Route::get('/applicants-coop-status',                                                   'ReportsController@applicantsCoopStatus')->name('applicantsCoopStatus');
            Route::get('/applicants-coop-status/export/{date_to}/{date_from}',                      'ReportsController@applicantsCoopStatusExport')->name('applicantsCoopStatusExport');
            Route::get('/applicants-by-coop',                                                       'ReportsController@applicantsByCoop')->name('applicants-by-coop');
            Route::post('/listado-ejecuciones/{year}',                                              'ReportsController@listejec')->name('listejec');
            Route::get('/listado-ejecuciones',                                                      'ReportsController@listejec')->name('listejec');
            Route::get('/asignaciones_atc',                                                         'ReportsController@asignacionesAtc')->name('asignacionesatc');
            Route::get('/atc-by-status',                                                            'ReportsController@atcByStatus')->name('atcByStatus');
            Route::get('/report',                                                                   'ReportsController@report');
            Route::post('/filtros',                                                                 'ReportsController@filtros');
            Route::get('/reportEtapas',                                                             'ReportsController@reportEtapas');
            Route::post('/filtrosEtapas',                                                           'ReportsController@filtrosEtapas');
            Route::get('/atc-by-status/{status}/{entity}/{radio}/{config}',                         'ReportsController@atcByStatus')->name('atcByStatus');
            Route::get('/atc-by-status/export/{status}/{entity}/{radio}/{config}',                         'ReportsController@atcByStatusExport')->name('atcByStatus');
        });
    });
});

Route::get('/application-tracking', 'TrackingController@index')->name('tracking');
Route::get('auth/google', 'LoginController@redirectToGoogle')->name('auth.google');
Route::get('auth/google/callback', 'LoginController@handleGoogleCallback');

//facebook login
Route::get('/redirectfb', 'SocialAuthFacebookController@redirect');
Route::get('/callbackfb', 'SocialAuthFacebookController@callback');

//politica
Route::get('policy', function () {
    return view('policy');
});

Route::get('terms', function () {
    return view('terms');
});

Auth::routes();

Route::get('/home', 'HomeController@index');
Route::get('/create', 'HomeController@create');
Route::post('/store', 'HomeController@store');

Route::get('/createconyuge', 'HomeController@createconyuge');
Route::post('/storeconyuge', 'HomeController@storeconyuge');

Route::get('/{applicant}/edit', 'HomeController@edit');
Route::get('/{applicant}/editconyuge', 'HomeController@editconyuge');

Route::post('/{applicant}/update', 'HomeController@update');
Route::post('/{applicant}/updateconyuge', 'HomeController@updateconyuge');

//Documentos
Route::get('/documents/create/{id}/{type}', 'HomeController@documentCreate');
Route::post('/documents/create/{id}/{type}', 'HomeController@storeApplicantDocument');
Route::get('/getDocuments', 'HomeController@getDocuments');
//Borrar Documentos
Route::delete('delete/{document}', 'HomeController@destroy');

Route::get('/transition', 'HomeController@transition');
Route::get('/transitionsave', 'HomeController@storeTransition');

Route::get('/{id}/findapplicant', 'Admin\ApplicantsController@findapplicant');
Route::get('/{id}/hadbenefit', 'Admin\ApplicantsController@hadBenefit');
Route::get('/{id}/identificaciones', 'Admin\ApplicantsController@getIdentificaciones');


//Mail
Route::get('mail/send', 'MailController@send');


/* Auto-generated admin routes */
Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(static function () {
    Route::prefix('admin')->namespace('Admin')->name('admin/')->group(static function () {
        Route::prefix('financial-summaries')->name('financial-summaries/')->group(static function () {
            Route::get('/',                                             'FinancialSummariesController@index')->name('index');
            Route::get('/create',                                       'FinancialSummariesController@create')->name('create');
            Route::post('/',                                            'FinancialSummariesController@store')->name('store');
            Route::get('/{financialSummary}/edit',                      'FinancialSummariesController@edit')->name('edit');
            Route::post('/bulk-destroy',                                'FinancialSummariesController@bulkDestroy')->name('bulk-destroy');
            Route::post('/{financialSummary}',                          'FinancialSummariesController@update')->name('update');
            Route::delete('/{financialSummary}',                        'FinancialSummariesController@destroy')->name('destroy');
        });
    });
});

/* Auto-generated admin routes */
Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(static function () {
    Route::prefix('admin')->namespace('Admin')->name('admin/')->group(static function () {
        Route::prefix('familiar-settings')->name('familiar-settings/')->group(static function () {
            Route::get('/',                                             'FamiliarSettingsController@index')->name('index');
            Route::get('/create',                                       'FamiliarSettingsController@create')->name('create');
            Route::post('/',                                            'FamiliarSettingsController@store')->name('store');
            Route::get('/{familiarSetting}/edit',                       'FamiliarSettingsController@edit')->name('edit');
            Route::post('/bulk-destroy',                                'FamiliarSettingsController@bulkDestroy')->name('bulk-destroy');
            Route::post('/{familiarSetting}',                           'FamiliarSettingsController@update')->name('update');
            Route::delete('/{familiarSetting}',                         'FamiliarSettingsController@destroy')->name('destroy');
        });
    });
});

/* Auto-generated admin routes */
Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(static function () {
    Route::prefix('admin')->namespace('Admin')->name('admin/')->group(static function () {
        Route::prefix('type-orders')->name('type-orders/')->group(static function () {
            Route::get('/',                                             'TypeOrdersController@index')->name('index');
            Route::get('/create',                                       'TypeOrdersController@create')->name('create');
            Route::post('/',                                            'TypeOrdersController@store')->name('store');
            Route::get('/{typeOrder}/edit',                             'TypeOrdersController@edit')->name('edit');
            Route::post('/bulk-destroy',                                'TypeOrdersController@bulkDestroy')->name('bulk-destroy');
            Route::post('/{typeOrder}',                                 'TypeOrdersController@update')->name('update');
            Route::delete('/{typeOrder}',                               'TypeOrdersController@destroy')->name('destroy');
        });
    });
});

/* Auto-generated admin routes */
Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(static function () {
    Route::prefix('admin')->namespace('Admin')->name('admin/')->group(static function () {
        Route::prefix('transfer-orders')->name('transfer-orders/')->group(static function () {
            Route::get('/',                                             'TransferOrdersController@index')->name('index');
            Route::get('/create',                                       'TransferOrdersController@create')->name('create');
            Route::get('/{applicant_id}/createwithpoliza',              'TransferOrdersController@createwithpoliza')->name('create');
            Route::get('/create',                                       'TransferOrdersController@create')->name('create');
            Route::post('/',                                            'TransferOrdersController@store')->name('store');
            Route::get('/{transferOrder}/edit',                         'TransferOrdersController@edit')->name('edit');
            Route::post('/bulk-destroy',                                'TransferOrdersController@bulkDestroy')->name('bulk-destroy');
            Route::post('/{transferOrder}',                             'TransferOrdersController@update')->name('update');
            Route::delete('/{transferOrder}',                           'TransferOrdersController@destroy')->name('destroy');
            Route::get('/{government_id}/find-applicant',                'TransferOrdersController@findApplicant')->name('findApplicant');
            Route::get('/{transferOrder}/ordentransferencia',           'TransferOrdersController@ordentransferencia')->name('orden.pdf');
        });
    });
});

/* Auto-generated admin routes */
Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(static function () {
    Route::prefix('admin')->namespace('Admin')->name('admin/')->group(static function () {
        Route::prefix('insurance-entities')->name('insurance-entities/')->group(static function () {
            Route::get('/',                                             'InsuranceEntitiesController@index')->name('index');
            Route::get('/create',                                       'InsuranceEntitiesController@create')->name('create');
            Route::post('/',                                            'InsuranceEntitiesController@store')->name('store');
            Route::get('/{insuranceEntity}/edit',                       'InsuranceEntitiesController@edit')->name('edit');
            Route::post('/bulk-destroy',                                'InsuranceEntitiesController@bulkDestroy')->name('bulk-destroy');
            Route::post('/{insuranceEntity}',                           'InsuranceEntitiesController@update')->name('update');
            Route::delete('/{insuranceEntity}',                         'InsuranceEntitiesController@destroy')->name('destroy');
        });
    });
});

/* Auto-generated admin routes */
Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(static function () {
    Route::prefix('admin')->namespace('Admin')->name('admin/')->group(static function () {
        Route::prefix('insurance-policies')->name('insurance-policies/')->group(static function () {
            Route::get('/',                                             'InsurancePoliciesController@index')->name('index');
            Route::get('{applicant_id}/createOrdenTra',                 'InsurancePoliciesController@createOrdenTra')->name('createOrdenTra');
            Route::get('/create',                                                    'InsurancePoliciesController@create')->name('create');
            Route::get('/export',                                       'InsurancePoliciesController@export')->name('export');
            Route::post('/',                                            'InsurancePoliciesController@store')->name('store');
            Route::get('/{insurancePolicy}/edit',                       'InsurancePoliciesController@edit')->name('edit');
            Route::get('/{government_id}/find-applicant',               'InsurancePoliciesController@findApplicant')->name('findApplicant');
            Route::post('/bulk-destroy',                                'InsurancePoliciesController@bulkDestroy')->name('bulk-destroy');
            Route::post('/{insurancePolicy}',                           'InsurancePoliciesController@update')->name('update');
            Route::delete('/{insurancePolicy}',                         'InsurancePoliciesController@destroy')->name('destroy');
        });
    });
});

/* Auto-generated admin routes */
Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(static function () {
    Route::prefix('admin')->namespace('Admin')->name('admin/')->group(static function () {
        Route::prefix('construction-entities')->name('construction-entities/')->group(static function () {
            Route::get('/',                                             'ConstructionEntitiesController@index')->name('index');
            Route::get('/create',                                       'ConstructionEntitiesController@create')->name('create');
            Route::post('/',                                            'ConstructionEntitiesController@store')->name('store');
            Route::get('/{constructionEntity}/edit',                    'ConstructionEntitiesController@edit')->name('edit');
            Route::post('/bulk-destroy',                                'ConstructionEntitiesController@bulkDestroy')->name('bulk-destroy');
            Route::post('/{constructionEntity}',                        'ConstructionEntitiesController@update')->name('update');
            Route::delete('/{constructionEntity}',                      'ConstructionEntitiesController@destroy')->name('destroy');
        });
    });
});

/* Auto-generated admin routes */
Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(static function () {
    Route::prefix('admin')->namespace('Admin')->name('admin/')->group(static function () {
        Route::prefix('financial-entities')->name('financial-entities/')->group(static function () {
            Route::get('/',                                             'FinancialEntitiesController@index')->name('index');
            Route::get('/create',                                       'FinancialEntitiesController@create')->name('create');
            Route::post('/',                                            'FinancialEntitiesController@store')->name('store');
            Route::get('/{financialEntity}/edit',                       'FinancialEntitiesController@edit')->name('edit');
            Route::post('/bulk-destroy',                                'FinancialEntitiesController@bulkDestroy')->name('bulk-destroy');
            Route::post('/{financialEntity}',                           'FinancialEntitiesController@update')->name('update');
            Route::delete('/{financialEntity}',                         'FinancialEntitiesController@destroy')->name('destroy');
        });
    });
});

/* Auto-generated admin routes */
Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(static function () {
    Route::prefix('admin')->namespace('Admin')->name('admin/')->group(static function () {
        Route::prefix('trusts')->name('trusts/')->group(static function () {
            Route::get('/',                                             'TrustsController@index')->name('index');
            Route::get('/create',                                       'TrustsController@create')->name('create');
            Route::post('/',                                            'TrustsController@store')->name('store');
            Route::post('/add-applicant/{trust}',                       'TrustsController@addApplicant')->name('addApplicant');
            Route::get('/{government_id}/find-applicant',               'TrustsController@findApplicant')->name('findApplicant');
            Route::get('/{trust}/edit',                                 'TrustsController@edit')->name('edit');
            Route::get('/{trust}/showfideicomiso',                      'TrustsController@showfideicomiso')->name('showfideicomiso');
            Route::get('/find-listado/{trust}',                         'TrustsController@findlistado')->name('findlistado');
            Route::post('/bulk-destroy',                                'TrustsController@bulkDestroy')->name('bulk-destroy');
            Route::post('/{trust}',                                     'TrustsController@update')->name('update');
            Route::delete('/{trust}',                                   'TrustsController@destroy')->name('destroy');
        });
    });
});

/* Auto-generated admin routes */
Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(static function () {
    Route::prefix('admin')->namespace('Admin')->name('admin/')->group(static function () {
        Route::prefix('applicant-trusts')->name('applicant-trusts/')->group(static function () {
            Route::get('/',                                             'ApplicantTrustsController@index')->name('index');
            Route::get('/create',                                       'ApplicantTrustsController@create')->name('create');
            Route::post('/',                                            'ApplicantTrustsController@store')->name('store');
            Route::get('/{applicantTrust}/edit',                        'ApplicantTrustsController@edit')->name('edit');
            Route::post('/bulk-destroy',                                'ApplicantTrustsController@bulkDestroy')->name('bulk-destroy');
            Route::post('/{applicantTrust}',                            'ApplicantTrustsController@update')->name('update');
            Route::delete('/{applicantTrust}',                          'ApplicantTrustsController@destroy')->name('destroy');
        });
    });
});

/* Auto-generated admin routes */
Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(static function () {
    Route::prefix('admin')->namespace('Admin')->name('admin/')->group(static function () {
        Route::prefix('applicants-trusts')->name('applicants-trusts/')->group(static function () {
            Route::get('/',                                             'ApplicantsTrustsController@index')->name('index');
            Route::get('/create',                                       'ApplicantsTrustsController@create')->name('create');
            Route::post('/',                                            'ApplicantsTrustsController@store')->name('store');
            Route::get('/{applicantsTrust}/edit',                       'ApplicantsTrustsController@edit')->name('edit');
            Route::post('/bulk-destroy',                                'ApplicantsTrustsController@bulkDestroy')->name('bulk-destroy');
            Route::post('/{applicantsTrust}',                           'ApplicantsTrustsController@update')->name('update');
            Route::delete('/{applicantsTrust}',                         'ApplicantsTrustsController@destroy')->name('destroy');
        });
    });
});

/* Auto-generated admin routes */
Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(static function () {
    Route::prefix('admin')->namespace('Admin')->name('admin/')->group(static function () {
        Route::prefix('resource-transfers')->name('resource-transfers/')->group(static function () {
            Route::get('/',                                             'ResourceTransfersController@index')->name('index');
            Route::get('/create',                                       'ResourceTransfersController@create')->name('create');
            Route::post('/',                                            'ResourceTransfersController@store')->name('store');
            Route::post('/add-applicant/{resourceTransfer}',            'ResourceTransfersController@addApplicant')->name('addApplicant');
            Route::post('/addobs/{resourceTransfer}',                  'ResourceTransfersController@addobs')->name('addobs');
            Route::get('/{resourceTransfer}/edit',                      'ResourceTransfersController@edit')->name('edit');
            Route::post('/editresource/{resourceTransfer}',             'ResourceTransfersController@editresource')->name('editresource');
            Route::get('/{resourceTransfer}/showfideicomiso',           'ResourceTransfersController@showfideicomiso')->name('showfideicomiso');
            Route::get('/{resourceTransfer}/show-autorizando',          'ResourceTransfersController@showAutorizando')->name('showAutorizando');
            Route::get('/{government_id}/find-applicant',               'ResourceTransfersController@findApplicant')->name('findApplicant');
            Route::get('/find-listado/{resourceTransfer}',              'ResourceTransfersController@findlistado')->name('findlistado');
            Route::post('/bulk-destroy',                                'ResourceTransfersController@bulkDestroy')->name('bulk-destroy');
            Route::post('/{resourceTransfer}',                          'ResourceTransfersController@update')->name('update');
            Route::delete('/{resourceTransfer}',                        'ResourceTransfersController@destroy')->name('destroy');
        });
    });
});

/* Auto-generated admin routes */
Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(static function () {
    Route::prefix('admin')->namespace('Admin')->name('admin/')->group(static function () {
        Route::prefix('applicant-resource-transfers')->name('applicant-resource-transfers/')->group(static function () {
            Route::get('/',                                             'ApplicantResourceTransfersController@index')->name('index');
            Route::get('/create',                                       'ApplicantResourceTransfersController@create')->name('create');
            Route::post('/add-applicant/{applicantResourceTransfer}',   'ApplicantResourceTransfersController@addApplicant')->name('addApplicant');
            Route::post('/',                                            'ApplicantResourceTransfersController@store')->name('store');
            Route::get('/{applicantResourceTransfer}/edit',             'ApplicantResourceTransfersController@edit')->name('edit');
            Route::post('/bulk-destroy',                                'ApplicantResourceTransfersController@bulkDestroy')->name('bulk-destroy');
            Route::post('/{applicantResourceTransfer}',                 'ApplicantResourceTransfersController@update')->name('update');
            Route::delete('/{applicantResourceTransfer}',               'ApplicantResourceTransfersController@destroy')->name('destroy');
        });
    });
});

/* Auto-generated admin routes */
Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(static function () {
    Route::prefix('admin')->namespace('Admin')->name('admin/')->group(static function () {
        Route::prefix('construction-entities')->name('construction-entities/')->group(static function () {
            Route::get('/',                                             'ConstructionEntitiesController@index')->name('index');
            Route::get('/create',                                       'ConstructionEntitiesController@create')->name('create');
            Route::post('/',                                            'ConstructionEntitiesController@store')->name('store');
            Route::get('/{constructionEntity}/edit',                    'ConstructionEntitiesController@edit')->name('edit');
            Route::post('/bulk-destroy',                                'ConstructionEntitiesController@bulkDestroy')->name('bulk-destroy');
            Route::post('/{constructionEntity}',                        'ConstructionEntitiesController@update')->name('update');
            Route::delete('/{constructionEntity}',                      'ConstructionEntitiesController@destroy')->name('destroy');
        });
    });
});

/* Auto-generated admin routes */
Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(static function () {
    Route::prefix('admin')->namespace('Admin')->name('admin/')->group(static function () {
        Route::prefix('certificates')->name('certificates/')->group(static function () {
            Route::get('/',                                             'CertificatesController@index')->name('index');
            Route::get('/certificate',                                  'CertificatesController@indexCertificate')->name('index');
            Route::get('/create',                                       'CertificatesController@create')->name('create');
            Route::get('/createfinalobra',                              'CertificatesController@createFinalObra')->name('create');
            Route::post('/',                                            'CertificatesController@store')->name('store');
            Route::post('/finalobra',                                   'CertificatesController@storeFinalObra')->name('store');
            Route::get('/{certificate}/edit',                           'CertificatesController@edit')->name('edit');
            Route::get('/{certificate}/showpdf',                        'CertificatesController@showpdf')->name('showpdf');
            Route::get('/{certificate}/finalobrapdf',                   'CertificatesController@finalobrapdf');
            Route::get('/applicants/search',                            'CertificatesController@searchApplicants');
            //Route::get('/applicants/search', [App\Http\Controllers\Admin\CertificatesController::class,'searchApplicants']);
            Route::post('/bulk-destroy',                                'CertificatesController@bulkDestroy')->name('bulk-destroy');
            Route::post('/{certificate}',                               'CertificatesController@update')->name('update');
            Route::delete('/{certificate}',                             'CertificatesController@destroy')->name('destroy');
        });
    });
});


/* Auto-generated admin routes */
Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(static function () {
    Route::prefix('admin')->namespace('Admin')->name('admin/')->group(static function () {
        Route::prefix('supervisors')->name('supervisors/')->group(static function () {
            Route::get('/',                                             'SupervisorsController@index')->name('index');
            Route::get('/create',                                       'SupervisorsController@create')->name('create');
            Route::post('/',                                            'SupervisorsController@store')->name('store');
            Route::get('/{supervisor}/edit',                            'SupervisorsController@edit')->name('edit');
            Route::post('/bulk-destroy',                                'SupervisorsController@bulkDestroy')->name('bulk-destroy');
            Route::post('/{supervisor}',                                'SupervisorsController@update')->name('update');
            Route::delete('/{supervisor}',                              'SupervisorsController@destroy')->name('destroy');
        });
    });
});

/* Auto-generated admin routes */
Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(static function () {
    Route::prefix('admin')->namespace('Admin')->name('admin/')->group(static function () {
        Route::prefix('type-checks')->name('type-checks/')->group(static function () {
            Route::get('/',                                             'TypeChecksController@index')->name('index');
            Route::get('/create',                                       'TypeChecksController@create')->name('create');
            Route::post('/',                                            'TypeChecksController@store')->name('store');
            Route::get('/{typeCheck}/edit',                             'TypeChecksController@edit')->name('edit');
            Route::post('/bulk-destroy',                                'TypeChecksController@bulkDestroy')->name('bulk-destroy');
            Route::post('/{typeCheck}',                                 'TypeChecksController@update')->name('update');
            Route::delete('/{typeCheck}',                               'TypeChecksController@destroy')->name('destroy');
        });
    });
});

/* Auto-generated admin routes */
Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(static function () {
    Route::prefix('admin')->namespace('Admin')->name('admin/')->group(static function () {
        Route::prefix('works')->name('works/')->group(static function () {
            Route::get('/',                                             'WorksController@index')->name('index');
            Route::get('/create',                                       'WorksController@create')->name('create');
            Route::post('/add-type/{work}',                             'WorksController@addtype')->name('addtype');
            Route::post('/',                                            'WorksController@store')->name('store');
            Route::get('/{work}/edit',                                  'WorksController@edit')->name('edit');
            Route::post('/bulk-destroy',                                'WorksController@bulkDestroy')->name('bulk-destroy');
            Route::post('/{work}',                                      'WorksController@update')->name('update');
            Route::delete('/{work}',                                    'WorksController@destroy')->name('destroy');
        });
    });
});

/* Auto-generated admin routes */
Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(static function () {
    Route::prefix('admin')->namespace('Admin')->name('admin/')->group(static function () {
        Route::prefix('works-type-checks')->name('works-type-checks/')->group(static function () {
            Route::get('/',                                             'WorksTypeChecksController@index')->name('index');
            Route::get('/create',                                       'WorksTypeChecksController@create')->name('create');
            Route::post('/',                                            'WorksTypeChecksController@store')->name('store');
            Route::get('/{worksTypeCheck}/edit',                        'WorksTypeChecksController@edit')->name('edit');
            Route::post('/bulk-destroy',                                'WorksTypeChecksController@bulkDestroy')->name('bulk-destroy');
            Route::post('/{worksTypeCheck}',                            'WorksTypeChecksController@update')->name('update');
            Route::delete('/{worksTypeCheck}',                          'WorksTypeChecksController@destroy')->name('destroy');
        });
    });
});

/* Auto-generated admin routes */
Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(static function () {
    Route::prefix('admin')->namespace('Admin')->name('admin/')->group(static function () {
        Route::prefix('feedback')->name('feedback/')->group(static function () {
            Route::get('/',                                             'FeedbacksController@index')->name('index');
            Route::get('/create',                                       'FeedbacksController@create')->name('create');
            Route::post('/',                                            'FeedbacksController@store')->name('store');
            Route::get('/{feedback}/edit',                              'FeedbacksController@edit')->name('edit');
            Route::get('/{feedback}/show',                              'FeedbacksController@show')->name('show');
            Route::post('/bulk-destroy',                                'FeedbacksController@bulkDestroy')->name('bulk-destroy');
            Route::post('/{feedback}',                                  'FeedbacksController@update')->name('update');
            Route::delete('/{feedback}',                                'FeedbacksController@destroy')->name('destroy');
        });
    });
});

/* Auto-generated admin routes */
Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(static function () {
    Route::prefix('admin')->namespace('Admin')->name('admin/')->group(static function () {
        Route::prefix('visits')->name('visits/')->group(static function () {
            Route::get('/',                                             'VisitsController@index')->name('index');
            Route::get('/{visit}/show',                                 'VisitsController@showvisit');
            Route::get('/create',                                       'VisitsController@create')->name('create');
            Route::post('/',                                            'VisitsController@store')->name('store');
            Route::get('/{visit}/edit',                                 'VisitsController@edit')->name('edit');
            Route::post('/bulk-destroy',                                'VisitsController@bulkDestroy')->name('bulk-destroy');
            Route::post('/{visit}',                                     'VisitsController@update')->name('update');
            Route::delete('/{visit}',                                   'VisitsController@destroy')->name('destroy');
        });
    });
});


/* Auto-generated admin routes */
Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(static function () {
    Route::prefix('admin')->namespace('Admin')->name('admin/')->group(static function () {
        Route::prefix('works-data')->name('works-data/')->group(static function () {
            Route::get('/',                                             'WorksDataController@index')->name('index');
            Route::get('/create',                                       'WorksDataController@create')->name('create');
            Route::post('/',                                            'WorksDataController@store')->name('store');
            Route::get('/{worksDatum}/edit',                            'WorksDataController@edit')->name('edit');
            Route::post('/bulk-destroy',                                'WorksDataController@bulkDestroy')->name('bulk-destroy');
            Route::post('/{worksDatum}',                                'WorksDataController@update')->name('update');
            Route::delete('/{worksDatum}',                              'WorksDataController@destroy')->name('destroy');
        });
    });
});


/* Auto-generated admin routes */
Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(static function () {
    Route::prefix('admin')->namespace('Admin')->name('admin/')->group(static function () {
        Route::prefix('accounting-records')->name('accounting-records/')->group(static function () {
            Route::get('/',                                             'AccountingRecordsController@index')->name('index');
            Route::get('/create',                                       'AccountingRecordsController@create')->name('create');
            Route::get('/reports',                                      'AccountingRecordsController@reports')->name('reports');
            Route::get('/reports/accounting',                           'AccountingRecordsController@accounting')->name('accounting');
            Route::get('/reports/resolutions',                           'AccountingRecordsController@resolutions')->name('resolutions');

            Route::get('/{accountingRecord}/applicant-record/create',   'ApplicantRecordsController@create');
            Route::post('/',                                            'AccountingRecordsController@store')->name('store');
            Route::get('/{accountingRecord}/show',                      'AccountingRecordsController@show');
            Route::post('/{accountingRecord}/confirm',                  'AccountingRecordsController@confirm');
            Route::get('/summary',                                       'AccountingRecordsController@summary')->name('summary');
            Route::get('/{accountingRecord}/edit',                      'AccountingRecordsController@edit')->name('edit');
            Route::post('/bulk-destroy',                                'AccountingRecordsController@bulkDestroy')->name('bulk-destroy');
            Route::post('/{accountingRecord}',                          'AccountingRecordsController@update')->name('update');
            Route::delete('/{accountingRecord}',                        'AccountingRecordsController@destroy')->name('destroy');
        });
    });
});

/* Auto-generated admin routes */
Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(static function () {
    Route::prefix('admin')->namespace('Admin')->name('admin/')->group(static function () {
        Route::prefix('applicant-records')->name('applicant-records/')->group(static function () {
            Route::get('/',                                             'ApplicantRecordsController@index')->name('index');
            Route::get('/create',                                       'ApplicantRecordsController@create')->name('create');
            Route::post('/',                                            'ApplicantRecordsController@store')->name('store');
            Route::get('/{applicantRecord}/edit',                       'ApplicantRecordsController@edit')->name('edit');
            Route::post('/bulk-destroy',                                'ApplicantRecordsController@bulkDestroy')->name('bulk-destroy');
            Route::post('/{applicantRecord}',                           'ApplicantRecordsController@update')->name('update');
            Route::delete('/{applicantRecord}',                         'ApplicantRecordsController@destroy')->name('destroy');
        });
    });
});

/* Auto-generated admin routes */
Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(static function () {
    Route::prefix('admin')->namespace('Admin')->name('admin/')->group(static function () {
        Route::prefix('accounting-summaries')->name('accounting-summaries/')->group(static function () {
            Route::get('/',                                             'AccountingSummaryController@index')->name('index');
            Route::get('/create',                                       'AccountingSummaryController@create')->name('create');
            Route::post('/',                                            'AccountingSummaryController@store')->name('store');
            Route::get('/{accountingSummary}/edit',                     'AccountingSummaryController@edit')->name('edit');
            Route::post('/bulk-destroy',                                'AccountingSummaryController@bulkDestroy')->name('bulk-destroy');
            Route::post('/{accountingSummary}',                         'AccountingSummaryController@update')->name('update');
            Route::delete('/{accountingSummary}',                       'AccountingSummaryController@destroy')->name('destroy');
        });
    });
});


/* Auto-generated admin routes */
Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(static function () {
    Route::prefix('admin')->namespace('Admin')->name('admin/')->group(static function () {
        Route::prefix('type-movements')->name('type-movements/')->group(static function () {
            Route::get('/',                                             'TypeMovementsController@index')->name('index');
            Route::get('/create',                                       'TypeMovementsController@create')->name('create');
            Route::post('/',                                            'TypeMovementsController@store')->name('store');
            Route::get('/{typeMovement}/edit',                          'TypeMovementsController@edit')->name('edit');
            Route::post('/bulk-destroy',                                'TypeMovementsController@bulkDestroy')->name('bulk-destroy');
            Route::post('/{typeMovement}',                              'TypeMovementsController@update')->name('update');
            Route::delete('/{typeMovement}',                            'TypeMovementsController@destroy')->name('destroy');
        });
    });
});
