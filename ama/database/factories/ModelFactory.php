<?php

/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(Brackets\AdminAuth\Models\AdminUser::class, function (Faker\Generator $faker) {
    return [
        'first_name' => $faker->firstName,
        'last_name' => $faker->lastName,
        'email' => $faker->email,
        'password' => bcrypt($faker->password),
        'remember_token' => null,
        'activated' => true,
        'forbidden' => $faker->boolean(),
        'language' => 'en',
        'deleted_at' => null,
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,

    ];
});/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\City::class, static function (Faker\Generator $faker) {
    return [
        'name' => $faker->firstName,
        'state_id' => $faker->sentence,
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,


    ];
});
/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\ContactMethod::class, static function (Faker\Generator $faker) {
    return [
        'name' => $faker->firstName,
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,


    ];
});
/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\Disability::class, static function (Faker\Generator $faker) {
    return [
        'name' => $faker->firstName,
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,


    ];
});
/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\Disease::class, static function (Faker\Generator $faker) {
    return [
        'name' => $faker->firstName,
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,


    ];
});
/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\EducationLevel::class, static function (Faker\Generator $faker) {
    return [
        'name' => $faker->firstName,
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,


    ];
});
/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\State::class, static function (Faker\Generator $faker) {
    return [
        'name' => $faker->firstName,
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,


    ];
});
/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\Status::class, static function (Faker\Generator $faker) {
    return [
        'name' => $faker->firstName,
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,


    ];
});
/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\QuestionnaireType::class, static function (Faker\Generator $faker) {
    return [
        'name' => $faker->firstName,
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,


    ];
});
/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\ApplicantRelationship::class, static function (Faker\Generator $faker) {
    return [
        'name' => $faker->firstName,
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,


    ];
});
/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\Applicant::class, static function (Faker\Generator $faker) {
    return [
        'names' => $faker->sentence,
        'last_names' => $faker->sentence,
        'birthdate' => $faker->dateTime,
        'gender' => $faker->sentence,
        'state_id' => $faker->sentence,
        'city_id' => $faker->sentence,
        'education_level' => $faker->sentence,
        'government_id' => $faker->sentence,
        'marital_status' => $faker->sentence,
        'pregnant' => $faker->boolean(),
        'pregnancy_due_date' => $faker->sentence,
        'parent_applicant' => $faker->sentence,
        'applicant_relationship' => $faker->sentence,
        'cadaster' => $faker->sentence,
        'property_id' => $faker->sentence,
        'occupation' => $faker->sentence,
        'monthly_income' => $faker->randomNumber(5),


    ];
});
/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\QuestionnaireTemplate::class, static function (Faker\Generator $faker) {
    return [
        'name' => $faker->firstName,
        'enabled' => $faker->boolean(),
        'questionnaire_type_id' => $faker->sentence,
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,


    ];
});
/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\DocumentType::class, static function (Faker\Generator $faker) {
    return [
        'name' => $faker->firstName,
        'enabled' => $faker->boolean(),
        'type' => $faker->sentence,
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,
        
        
    ];
});
/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\ApplicantDocument::class, static function (Faker\Generator $faker) {
    return [
        'applicant_id' => $faker->sentence,
        'document_id' => $faker->sentence,
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,
        
        
    ];
});
/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\ConstructionEntity::class, static function (Faker\Generator $faker) {
    return [
        'name' => $faker->firstName,
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,
        
        
    ];
});
/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\FinancialEntity::class, static function (Faker\Generator $faker) {
    return [
        'name' => $faker->firstName,
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,
        
        
    ];
});
/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\Status::class, static function (Faker\Generator $faker) {
    return [
        'name' => $faker->firstName,
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,
        'applicant' => $faker->boolean(),
        
        
    ];
});
/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\FinancialSummary::class, static function (Faker\Generator $faker) {
    return [
        'applicant_id' => $faker->sentence,
        'desembolso' => $faker->sentence,
        'date' => $faker->date(),
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,
        
        
    ];
});
/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\FamiliarSetting::class, static function (Faker\Generator $faker) {
    return [
        'name' => $faker->firstName,
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,
        
        
    ];
});
/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\TypeOrder::class, static function (Faker\Generator $faker) {
    return [
        'title' => $faker->sentence,
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,
        
        
    ];
});
/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\TransferOrder::class, static function (Faker\Generator $faker) {
    return [
        'nro_solicitud' => $faker->sentence,
        'fecha_ot' => $faker->date(),
        'importe' => $faker->sentence,
        'type_order_id' => $faker->sentence,
        'construction_entity_id' => $faker->sentence,
        'applicant_id' => $faker->sentence,
        'financial_entity_id' => $faker->sentence,
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,
        
        
    ];
});
/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\InsuranceEntity::class, static function (Faker\Generator $faker) {
    return [
        'title' => $faker->sentence,
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,
        
        
    ];
});
/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\InsurancePolicy::class, static function (Faker\Generator $faker) {
    return [
        'nro_poliza' => $faker->sentence,
        'concepto' => $faker->sentence,
        'fecha_vigencia' => $faker->date(),
        'fecha_vencimiento' => $faker->date(),
        'fecha_desembolso' => $faker->date(),
        'importe' => $faker->sentence,
        'applicant_id' => $faker->sentence,
        'insurance_entity_id' => $faker->sentence,
        'construction_entity_id' => $faker->sentence,
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,
        
        
    ];
});
/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\ConstructionEntity::class, static function (Faker\Generator $faker) {
    return [
        'name' => $faker->firstName,
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,
        'bank_name' => $faker->sentence,
        'cta_cte' => $faker->sentence,
        'const_ruc' => $faker->sentence,
        
        
    ];
});
/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\FinancialEntity::class, static function (Faker\Generator $faker) {
    return [
        'name' => $faker->firstName,
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,
        'bank_name' => $faker->sentence,
        'address' => $faker->sentence,
        'cta_cte' => $faker->sentence,
        'const_ruc' => $faker->sentence,
        
        
    ];
});
/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\Trust::class, static function (Faker\Generator $faker) {
    return [
        'nro_tr' => $faker->sentence,
        'fecha_tr' => $faker->date(),
        'construction_entity_id' => $faker->sentence,
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,
        
        
    ];
});
/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\ApplicantTrust::class, static function (Faker\Generator $faker) {
    return [
        
        
    ];
});
/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\ApplicantsTrust::class, static function (Faker\Generator $faker) {
    return [
        'applicant_id' => $faker->sentence,
        'trust_id' => $faker->sentence,
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,
        
        
    ];
});
/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\ResourceTransfer::class, static function (Faker\Generator $faker) {
    return [
        'nro_solictud_rt' => $faker->sentence,
        'fecha_rt' => $faker->date(),
        'type_order_id' => $faker->sentence,
        'construction_entity_id' => $faker->sentence,
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,
        
        
    ];
});
/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\ApplicantResourceTransfer::class, static function (Faker\Generator $faker) {
    return [
        'applicant_id' => $faker->sentence,
        'resource_transfer_id' => $faker->sentence,
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,
        
        
    ];
});
/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\ConstructionEntity::class, static function (Faker\Generator $faker) {
    return [
        'name' => $faker->firstName,
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,
        'bank_name' => $faker->sentence,
        'cta_cte' => $faker->sentence,
        'const_ruc' => $faker->sentence,
        'razon_social' => $faker->sentence,
        'repre' => $faker->sentence,
        'nro_ci' => $faker->sentence,
        
        
    ];
});
/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\Certificate::class, static function (Faker\Generator $faker) {
    return [
        'fecha_tr' => $faker->date(),
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,
        
        
    ];
});
/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\Supervisor::class, static function (Faker\Generator $faker) {
    return [
        'name' => $faker->firstName,
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,
        
        
    ];
});
/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\TypeCheck::class, static function (Faker\Generator $faker) {
    return [
        'name' => $faker->firstName,
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,
        
        
    ];
});
/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\Work::class, static function (Faker\Generator $faker) {
    return [
        'fecha_ws' => $faker->date(),
        'applicant_id' => $faker->sentence,
        'supervisor_id' => $faker->sentence,
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,
        
        
    ];
});
/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\WorksTypeCheck::class, static function (Faker\Generator $faker) {
    return [
        'work_id' => $faker->sentence,
        'type_check_id' => $faker->sentence,
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,
        
        
    ];
});
/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\Feedback::class, static function (Faker\Generator $faker) {
    return [
        'type' => $faker->sentence,
        'message' => $faker->text(),
        'user_info' => $faker->text(),
        'reviewed' => $faker->boolean(),
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,
        
        
    ];
});
/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\Visit::class, static function (Faker\Generator $faker) {
    return [
        'applicant_id' => $faker->sentence,
        'visit_number' => $faker->sentence,
        'advance' => $faker->sentence,
        'latitude' => $faker->sentence,
        'longitude' => $faker->sentence,
        'visit_date' => $faker->dateTime,
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,
        
        
    ];
});
/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\WorksDatum::class, static function (Faker\Generator $faker) {
    return [
        'applicant_id' => $faker->sentence,
        'start_work' => $faker->dateTime,
        'latitude' => $faker->sentence,
        'longitude' => $faker->sentence,
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,
        
        
    ];
});
/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\AccountingRecord::class, static function (Faker\Generator $faker) {
    return [
        'summary' => $faker->sentence,
        'type_movements_id' => $faker->sentence,
        'amount' => $faker->sentence,
        'balance' => $faker->sentence,
        'confirmed' => $faker->boolean(),
        'resolution' => $faker->sentence,
        'registration_date' => $faker->date(),
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,
        '0' => $faker->dateTime,
        
        
    ];
});
/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\ApplicantRecord::class, static function (Faker\Generator $faker) {
    return [
        'accounting_record_id' => $faker->sentence,
        'applicant_id' => $faker->sentence,
        'amount' => $faker->sentence,
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,
        
        
    ];
});
/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\AccountingSummary::class, static function (Faker\Generator $faker) {
    return [
        'income' => $faker->sentence,
        'expenses' => $faker->sentence,
        'balance' => $faker->sentence,
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,
        
        
    ];
});
/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\TypeMovement::class, static function (Faker\Generator $faker) {
    return [
        'name' => $faker->firstName,
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,
        
        
    ];
});
