<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTrustsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trusts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nro_tr');
            $table->date('fecha_tr');
            $table->unsignedBigInteger('financial_entity_id');
            $table->foreign('financial_entity_id')->references('id')->on('financial_entities');
            $table->boolean('culminado')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trusts');
    }
}
