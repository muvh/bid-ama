<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFamiliarSettingIdToApplicantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('applicants', function (Blueprint $table) {
            //$table->integer('familar_setting_id')->nullable();
            //$table->foreign('familar_setting_id')->references('id')->on('familar_settings')->onDelete('cascade');
            $table->bigInteger('familiar_setting_id')->unsigned()->nullable();
            $table->foreign('familiar_setting_id')->references('id')->on('familiar_settings');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('applicants', function (Blueprint $table) {
            //
        });
    }
}
