<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransferOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transfer_orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('nro_solicitud');
            $table->date('fecha_ot');
            $table->bigInteger('importe');

            $table->unsignedBigInteger('type_order_id');
            $table->foreign('type_order_id')->references('id')->on('type_orders');

            $table->unsignedBigInteger('construction_entity_id');
            $table->foreign('construction_entity_id')->references('id')->on('construction_entities');
            
            $table->unsignedBigInteger('applicant_id');
            $table->foreign('applicant_id')->references('id')->on('applicants');
            
            $table->unsignedBigInteger('financial_entity_id');
            $table->foreign('financial_entity_id')->references('id')->on('financial_entities');
            $table->timestamps();
        });

        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transfer_orders');
    }
}
