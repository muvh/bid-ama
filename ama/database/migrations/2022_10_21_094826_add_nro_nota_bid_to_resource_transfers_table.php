<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddNroNotaBidToResourceTransfersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('resource_transfers', function (Blueprint $table) {
            //nro_nota_bid
            $table->string('nro_nota_bid')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('resource_transfers', function (Blueprint $table) {
            Schema::dropIfExists('nro_nota_bid');
        });
    }
}
