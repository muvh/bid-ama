<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAccountingRecordsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('accounting_records', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('summary');
            $table->unsignedBigInteger('type_movements_id');
            $table->foreign('type_movements_id')->references('id')->on('type_movements');
            $table->bigInteger('amount');
            $table->bigInteger('balance');
            $table->boolean('confirmed');
            $table->string('resolution');
            $table->date('registration_date');
            $table->timestamps();
            //$table->softDeletes(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('accounting_records');
    }
}
