<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddBancoToFinancialEntitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('financial_entities', function (Blueprint $table) {
            $table->string('bank_name')->nullable();
            $table->string('address')->nullable();
            $table->string('cta_cte')->nullable();
            $table->string('const_ruc')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('financial_entities', function (Blueprint $table) {
            $table->dropColumn('bank_name');
            $table->dropColumn('address');
            $table->dropColumn('cta_cte');
            $table->dropColumn('const_ruc');
        });
    }
}
