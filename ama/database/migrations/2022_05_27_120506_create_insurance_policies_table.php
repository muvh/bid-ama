<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInsurancePoliciesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('insurance_policies', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nro_poliza');
            $table->string('concepto');
            $table->date('fecha_vigencia');
            $table->date('fecha_vencimiento');
            $table->date('fecha_desembolso');
            $table->bigInteger('importe');
            $table->boolean('culminado')->default(0);
            $table->unsignedBigInteger('applicant_id');
            $table->foreign('applicant_id')->references('id')->on('applicants');
            
            $table->unsignedBigInteger('insurance_entity_id');
            $table->foreign('insurance_entity_id')->references('id')->on('insurance_entities');
            
            $table->unsignedBigInteger('construction_entity_id');
            $table->foreign('construction_entity_id')->references('id')->on('construction_entities');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('insurance_policies');
    }
}
