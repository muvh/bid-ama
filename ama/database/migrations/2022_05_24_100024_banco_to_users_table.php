<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class BancoToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('construction_entities', function (Blueprint $table) {
            $table->string('bank_name')->nullable();
            $table->string('cta_cte')->nullable();
            $table->string('const_ruc')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('construction_entities', function (Blueprint $table) {
            $table->dropColumn('bank_name');
            $table->dropColumn('cta_cte');
            $table->dropColumn('const_ruc');
        });
    }
}
