<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFinancialEntityIdToTrustsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trusts', function (Blueprint $table) {
            
            $table->unsignedBigInteger('financial_entity_id');
            $table->foreign('financial_entity_id')->references('id')->on('financial_entities');
            $table->dropColumn('construction_entity_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('trusts', function (Blueprint $table) {
            //
        });
    }
}
