<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateResourceTransfersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('resource_transfers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nro_solictud_rt');
            $table->date('fecha_rt');
            $table->unsignedBigInteger('type_order_id');
            $table->foreign('type_order_id')->references('id')->on('type_orders');
            $table->unsignedBigInteger('construction_entity_id');
            $table->foreign('construction_entity_id')->references('id')->on('construction_entities');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('resource_transfers');
    }
}
