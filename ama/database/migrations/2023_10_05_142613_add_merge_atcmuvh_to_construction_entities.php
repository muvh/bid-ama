<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddMergeAtcmuvhToConstructionEntities extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('construction_entities', function (Blueprint $table) {
            $table->string('merge_atcmuvh')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('construction_entities', function (Blueprint $table) {
            Schema::dropIfExists('merge_atcmuvh');
        });
    }
}
