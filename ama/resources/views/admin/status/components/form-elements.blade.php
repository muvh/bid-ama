<div class="form-group row align-items-center" :class="{'has-danger': errors.has('name'), 'has-success': fields.name && fields.name.valid }">
    <label for="name" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.status.columns.name') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.name" v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('name'), 'form-control-success': fields.name && fields.name.valid}" id="name" name="name" placeholder="{{ trans('admin.status.columns.name') }}">
        <div v-if="errors.has('name')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('name') }}</div>
    </div>
</div>

<div class="form-check row" :class="{'has-danger': errors.has('applicant'), 'has-success': fields.applicant && fields.applicant.valid }">
    <div class="ml-md-auto" :class="isFormLocalized ? 'col-md-8' : 'col-md-10'">
        <input class="form-check-input" id="applicant" type="checkbox" v-model="form.applicant" v-validate="''" data-vv-name="applicant"  name="applicant_fake_element">
        <label class="form-check-label" for="applicant">
            {{ trans('admin.status.columns.applicant') }}
        </label>
        <input type="hidden" name="applicant" :value="form.applicant">
        <div v-if="errors.has('applicant')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('applicant') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('prefix'), 'has-success': fields.prefix && fields.prefix.valid }">
    <label for="prefix" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.status.columns.prefix') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.prefix" min="1" max="3" v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('prefix'), 'form-control-success': fields.prefix && fields.prefix.valid}" id="prefix" name="prefix" placeholder="{{ trans('admin.status.columns.prefix') }}">
        <div v-if="errors.has('prefix')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('prefix') }}</div>
    </div>
</div>


