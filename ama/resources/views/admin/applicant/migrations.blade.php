@extends('brackets/admin-ui::admin.layout.default')

@section('title', trans('admin.applicant.actions.index'))
&nbsp;
@section('body')
@php
    $close='<i style="color: red; font-size: 54px" class="fa fa-times" aria-hidden="true"></i>';
    $open='&nbsp; &nbsp; &nbsp; <i style="color: green; font-size: 54px" class="fa fa-check" aria-hidden="true"></i>';
   
@endphp
    @php($roles = Auth::user()->getRoleNames())

    <applicant-listing
        :data="{{ $data->toJson() }}"
        :url="'{{ url('admin/applicants/migrated') }}'"
        inline-template>

        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="card-header">
                        <i class="fa fa-align-justify"></i> {{ trans('admin.applicant.actions.index') }}
                        @php($roles = Auth::user()->getRoleNames())
                        @if($roles->contains('coop') || $roles->contains('ama'))
                          {{--   <a class="btn btn-primary btn-spinner btn-sm pull-right m-b-0" href="{{ url('admin/applicants/create') }}" role="button"><i class="fa fa-plus"></i>&nbsp; {{ trans('admin.applicant.actions.create') }}</a> --}}
                            <a style="margin: 0px 20px 0px 20px" class="btn btn-primary btn-sm pull-right m-b-0" href="{{ url('admin/applicants/export') }}" role="button"><i class="fa fa-download"></i>&nbsp; {{ trans('admin.applicant.actions.export') }}</a>
                        @endif
                    </div>

                    <div class="card-body" v-cloak>
                        <div class="card-block">
                            <h3>Verificación de datos para migración</h3>
                            <div class="row">
                             <div class="col-sm-6 col-lg-5">
                                <center>
                                <table  class="table">
                                    <tbody>
                                    <tr>
                                    <td>&nbsp;Nro.Expediente&nbsp;</td>
                                    <td>{!! $data->file_number ? $data->file_number.$open  :  ($close)!!} </td>
                                    
                                    </tr>
                                    <tr>
                                    <td>fecha resolucion</td>
                                    <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                    <td>Expediente</td>
                                    <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                    <td>Feha de expediente</td>
                                    <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                    <td>ATC</td>
                                    <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                    <td>Cooperativa</td>
                                    <td>&nbsp;</td>
                                    </tr>
                                    </tbody>
                                    </table>  
                                </center>
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </applicant-listing>

@endsection
