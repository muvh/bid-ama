@extends('brackets/admin-ui::admin.layout.default')

@section('title', trans('admin.applicant.actions.create'))

@section('body')

    @php($roles = Auth::user()->getRoleNames())

    <div class="container-xl">
    <div class="row justify-content-center">
    <div class="col-md-8">
    <div class="card">
        <div class="card-header">
Importar datos
        </div>
        <div class="card-body">

        
        <form action="/admin/applicants/storedata" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <input type="file" name="file" />
                <button type="submit" class="btn btn-primary">Importar</button>
            </div>
        </form>
        <div class="alert-success text-center">
            Solo Actualiza el CI del Conyugue si el CI es igual al Postulante
        </div>
    </div>
    </div>
    </div>
    </div>
    </div>

@endsection
