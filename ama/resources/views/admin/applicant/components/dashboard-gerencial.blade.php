<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css" integrity="sha512-KfkfwYDsLkIlwQp6LFnl8zNdLGxu9YAA1QvwINks4PhcElQSvqcyVLLD9aMhXd13uQjoXtEKNosOWaZqXgel0g==" crossorigin="anonymous" referrerpolicy="no-referrer" />
<style>
    a .link-new i{
        color: #fff !important;
    }
    a .link-new:link{
        color: #fff !important;
    }
</style>
@php
    $totalPa=0;
    $totalPda=0;
    $totalPca=0;
    $totalPrm=0;
    $totalOini=0;
    $totalOeje=0;
    $totalOcu=0;

@endphp
@foreach ($dataStatus as $item)

@if($item['id']==37
||$item['id']==18
||$item['id']==19
||$item['id']==20)
      @php
          $totalPa=$totalPa+$item['cont'];
          
      @endphp
@endif
@if($item['id']==38)           
    @php
        $totalPda=$totalPda+$item['cont'];
    @endphp
@endif
@if($item['id']==22 ||  $item['id']==21 || $item['id']==40)             
    @php
        $totalPca=$totalPca+$item['cont'];
    @endphp
@endif
@if($item['id']==34 )           
    @php
        $totalAdj=$totalAdj+$item['cont'];
    @endphp
@endif
@if($item['id']==23 )           
    @php
        $totalPrm=$totalPrm+$item['cont'];
    @endphp
@endif
@if($item['id']==24 || $item['id']==39 || $item['id']==41 || $item['id']==50 )           
    @php
        $totalOini=$totalOini+$item['cont'];
    @endphp
@endif
@if($item['id']==25 )           
    @php
        $totalOeje=$totalOeje+$item['cont'];
    @endphp
@endif
@if( $item['id']==47 || $item['id']==26 || $item['id']==42 || $item['id']==43 || $item['id']==44 || $item['id']==27  )          
    @php
        $totalOcu=$totalOcu+$item['cont'];
    @endphp
@endif
@endforeach
<div class="col">

</div>
    <div class="row">
      <div class="col-lg-4">
      </div>

        <div class="col-lg-4">
          <h2 class="text-center">Informes Gerenciales</h2>
            <button v-tooltip="'Suma de sgtes estados: 1.1, 1.3, 1.4 y 1.5'" title="Suma de sgtes estados: 1.1, 1.1, 1.3, 1.4 y 1.5" class="btn btn-lg btn-block btn-square btn-warning rounded" type="button">En proceso de Análisis <br>
                <span class="badge badge-primary badge-pill">{{$totalPa}}</span>    
            </button>          
           <button v-tooltip="'Suma de sgtes estados: 1.2'" title="Suma de sgtes estados: 1.2" class="btn btn-lg btn-block btn-square btn-warning rounded" type="button">Pendientes de Distribucion a ATC <br>
                <span class="badge badge-primary badge-pill">{{$totalPda}}</span>    
            </button>
            <button v-tooltip="'Suma de sgtes estados: 2.1, 2.2, 2.3'" title="Suma de sgtes estados: 2.1, 2.2, 2.3" class="btn btn-lg btn-block btn-square btn-warning rounded" type="button">En Proceso de Calificación <br>
                <span class="badge badge-primary badge-pill">{{$totalPca}}</span>    
            </button>
            <button v-tooltip="'Suma de sgtes estados: 2.4'" title="Suma de sgtes estados: 2.4"  class="btn btn-lg btn-block btn-square btn-warning rounded" type="button">Pendiente de Resolucion MUVH <br>
                <span class="badge badge-primary badge-pill">{{$totalPrm}}</span>    
            </button>
            <button v-tooltip="'Suma de sgtes estados: 3.1, 3.2, 3.3, 3.4'" title="Suma de sgtes estados: 3.1, 3.2, 3.3, 3.4" class="btn btn-lg btn-block btn-square btn-warning" type="button">Obras a Iniciar <br>
                <span class="badge badge-primary badge-pill">{{$totalOini}}</span>    
            </button>
            <button v-tooltip="'Suma de sgtes estados: 3.5'" title="Suma de sgtes estados: 3.5" class="btn btn-lg btn-block btn-square btn-warning rounded" type="button">Obras en Ejecución <br>
                <span class="badge badge-primary badge-pill">{{$totalOeje}}</span>    
            </button>
            <button v-tooltip="'Suma de sgtes estados: 3.6, 3.7, 3.8, 3.9 y 3.10'"   title="Suma de sgtes estados: 3.6, 3.7, 3.8, 3.9 y 3.10" class="btn btn-lg btn-block btn-square btn-warning rounded" type="button">Obras Culminadas <br>
            <span class="badge badge-primary badge-pill">{{$totalOcu}}</span>    
        </button>
    <h2 style="text-align: center">Total {{$totalPa+$totalPda+$totalPca+$totalPrm+$totalOini+$totalOeje+$totalOcu}}</h2>
        </div>
      

    </div>
    <div class="row">
      <div class="col-lg-12 card text-white bg-success text-center">
        <blockquote class="card-bodyquote rounded">
        <h2 class="text-center">Accesos directos</h2>
        </blockquote>
      </div>
</div>
      <div class="row">
      <div class="col-lg-3">
           
        <button v-tooltip="'Nuevo Solicitante'"  title="Nuevo Solicitante" class="btn btn-lg btn-block btn-square btn-success rounded" type="button">
          <a class="link-new" href="/admin/applicants/create">   
            {{-- create new Postulante --}}
            <i class="fa-solid fa-file-circle-plus" style="font-size: 50px; color: #fff !important;"></i>
            </a>  
        </button>
        <button v-tooltip="'Cooperativas'" title="Cooperativas" type="button" class="btn btn-lg btn-block btn-square btn-success rounded" data-toggle="modal" data-target="#exampleModal4">
            <i class="fa-solid fa-building-columns" style="font-size: 50px"></i>
        </button>
      
    </div>
    <div class="col-lg-3">
        <button v-tooltip="'Postulantes'" title="Postulantes" type="button" class="btn btn-lg btn-block btn-square btn-success rounded" data-toggle="modal" data-target="#exampleModal6">
            <i class="nav-icon icon-user" style="font-size: 50px"></i>
            
        </button>
        <button v-tooltip="'Listado de Graficos'" title="Listado de Graficos" type="button" class="btn btn-lg btn-block btn-square btn-success rounded" data-toggle="modal" data-target="#exampleModal">
            <i class="fa-solid fa-chart-pie" style="font-size: 50px"></i>
            
        </button>
       
    </div>
    <div class="col-lg-3">
        
        <button v-tooltip="'ATC'" title="ATC" type="button" class="btn btn-lg btn-block btn-square btn-success rounded" data-toggle="modal" data-target="#exampleModal5">
            
            <i class="fa-solid fa-house-circle-check"style="font-size: 50px"></i>
        </button>
        <button v-tooltip="'Financieros'" title="Financieros" type="button" class="btn btn-lg btn-block btn-square btn-success rounded" data-toggle="modal" data-target="#exampleModal3">
            <i class="fa-solid fa-money-bill-wave" style="font-size: 50px"></i>
        </button>
    </div>
    
    <div class="col-lg-3">
           
      
      <button v-tooltip="'Listado de Busquedas'" title="Listado de Busquedas" type="button" class="btn btn-lg btn-block btn-square btn-success rounded" data-toggle="modal" data-target="#exampleModal2">
          <i class="fa-solid fa-magnifying-glass" style="font-size: 50px"></i>
      </button>
      <button v-tooltip="'Listado de estados'" title="Listado de estados" type="button"   class="btn btn-lg btn-block btn-square btn-success rounded">
        {{-- <i class="fa-solid fa-list-ol" style="font-size: 50px"></i> --}}
        <a class="link-new" href="/admin/applicants/graphic">   
        <i class="fa-solid fa-list-check" style="font-size: 50px; color: #fff !important;"></i>
        </a>
      </button>
  </div>
    </div>
</div>

 <!-- Modal 1 Charpie -->
 <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Graficos</h5>     
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>      
  
    <div class="list-group">
        <a href="/admin/reports/applicants-by-coop" class="list-group-item list-group-item-action"><i class="fa-solid fa-arrow-right-to-bracket"></i> Grafico por Estados</a>
        <a href="/admin/reports/listado-ejecuciones" class="list-group-item list-group-item-action"><i class="fa-solid fa-arrow-right-to-bracket"></i> Grafico de Ejecuciones</a>
        <a href="/admin/reports/asignaciones_atc" class="list-group-item list-group-item-action"><i class="fa-solid fa-arrow-right-to-bracket"></i> Grafico de ATCs</a>

      </div>


      </div>
    </div>
  </div>
   <!-- FIN Modal 1 Charpie -->
 <!-- Modal 2 SEARCH -->
 <div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Busquedas</h5>     
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>      
  
    <div class="list-group">
        <a href="/admin/reports/searchci" class="list-group-item list-group-item-action"><i class="fa-solid fa-arrow-right-to-bracket"></i> Buscar por CI</a>
        <a href="/admin/reports/search-names" class="list-group-item list-group-item-action"><i class="fa-solid fa-arrow-right-to-bracket"></i> Buscar por Nombres</a>
        <a href="/admin/reports/atc-by-status" class="list-group-item list-group-item-action"><i class="fa-solid fa-arrow-right-to-bracket"></i> Buscar por Secciones</a>
        <a href="/admin/reports/applicants-by-resolution" class="list-group-item list-group-item-action"><i class="fa-solid fa-arrow-right-to-bracket"></i> Buscar por Resolucion</a>
        <a href="/admin/reports/verificacion-institucional" class="list-group-item list-group-item-action"><i class="fa-solid fa-arrow-right-to-bracket"></i> Verificación Institucional por CI</a>
      </div>
      </div>
    </div>
  </div>
   <!-- FIN Modal 2 SEARCH -->
 <!-- Modal 3 SEARCH -->
 <div class="modal fade" id="exampleModal3" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Financieros</h5>     
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>      
  
    <div class="list-group">
        <a href="/admin/trusts" class="list-group-item list-group-item-action"><i class="fa-solid fa-arrow-right-to-bracket"></i> AFD</a>
        <a href="/admin/resource-transfers" class="list-group-item list-group-item-action"><i class="fa-solid fa-arrow-right-to-bracket"></i> Pagos 80 y 20</a>
        <a href="/admin/insurance-policies" class="list-group-item list-group-item-action"><i class="fa-solid fa-arrow-right-to-bracket"></i> Poliza</a>
      </div>
      </div>
    </div>
  </div>
   <!-- FIN Modal 3 SEARCH -->
 <!-- Modal 4 SEARCH -->
 <div class="modal fade" id="exampleModal4" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Cooperativas</h5>     
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>      
  
    <div class="list-group">
        <a href="/admin/fiancial-entities/create" class="list-group-item list-group-item-action"><i class="fa-solid fa-arrow-right-to-bracket"></i> Nueva Cooperativa</a>
        <a href="/admin/reports/applicants-by-coop#detalles-por-cooperativas" class="list-group-item list-group-item-action"><i class="fa-solid fa-arrow-right-to-bracket"></i> Detalles por Cooperativas</a>
        <a href="/admin/reports/atc-by-status" class="list-group-item list-group-item-action"><i class="fa-solid fa-arrow-right-to-bracket"></i> Buscar por Cooperativas</a>

      </div>
      </div>
    </div>
  </div>
   <!-- FIN Modal 4 SEARCH -->
 <!-- Modal 5 SEARCH -->
 <div class="modal fade" id="exampleModal5" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">ATC</h5>     
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>      
  
    <div class="list-group">

        <a href="/admin/construction-entities" class="list-group-item list-group-item-action"><i class="fa-solid fa-arrow-right-to-bracket"></i> Nueva ATC</a>
        <a href="/admin/reports/atc-by-status" class="list-group-item list-group-item-action"><i class="fa-solid fa-arrow-right-to-bracket"></i> Buscar por ATC</a>

      </div>
      </div>
    </div>
  </div>
   <!-- FIN Modal 5 SEARCH -->
 <!-- Modal 6 SEARCH -->
 <div class="modal fade" id="exampleModal6" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Postulantes</h5>     
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>      
  
    <div class="list-group">

        <a href="/admin/reports/searchci" class="list-group-item list-group-item-action"><i class="fa-solid fa-arrow-right-to-bracket"></i> Buscar por CI</a>
        <a href="/admin/reports/search-names" class="list-group-item list-group-item-action"><i class="fa-solid fa-arrow-right-to-bracket"></i> Buscar por Nombres</a>
        <a href="/admin/reports/applicants-by-resolution" class="list-group-item list-group-item-action"><i class="fa-solid fa-arrow-right-to-bracket"></i> Buscar por Resoluciones</a>

      </div>
      </div>
    </div>
  </div>
   <!-- FIN Modal 5 SEARCH -->

