@php
$style=(!empty($applicant->file_number)) ? 'disabled="disabled"': '';
$style2=(!empty($applicant->resolution_number)) ? 'disabled="disabled"': '';


@endphp
    
<div class="row">
    <div class="form-group col-sm-3">
        <label for="file_number" >Nro. Expediente</label>
        <input  type="text" v-model="form.file_number" v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('file_number'), 'form-control-success': fields.file_number && fields.file_number.valid}" id="file_number" name="file_number" placeholder="Nro. Expendiente">
        <div v-if="errors.has('file_number')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('file_number') }}</div>
    </div>
    <div class="form-group col-sm-3">
        <label for="file_number" >Fecha de Expediente</label>
        <datetime 
            v-model="form.file_date" 
            :config="datePickerConfig" 
           
            class="flatpickr" 
          
            :class="{'form-control-danger': errors.has('file_date'), 'form-control-success': fields.file_date && fields.file_date.valid}" 
            id="file_date" name="file_date" 
            placeholder="{{ trans('brackets/admin-ui::admin.forms.select_date_and_time') }}">
        </datetime>
        <div v-if="errors.has('file_date')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('file_date') }}</div>
    </div>


</div>
<div class="row">
    <div class="form-group col-sm-3">
        <label for="resolution_number" >Nro. Resolucion</label>
        <input  type="text" v-model="form.resolution_number" v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('resolution_number'), 'form-control-success': fields.resolution_number && fields.resolution_number.valid}" id="resolution_number" name="resolution_number" placeholder="Nro. Resolución">
        <div v-if="errors.has('resolution_number')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('resolution_number') }}</div>
    </div>
    <div class="form-group col-sm-3">
        <label for="resolution_date" >Fecha de Resolucion</label>
        <datetime 
            v-model="form.resolution_date" 
            :config="datePickerConfig" 
            
            class="flatpickr" 
          
            :class="{'form-control-danger': errors.has('resolution_date'), 'form-control-success': fields.resolution_date && fields.resolution_date.valid}" 
            id="resolution_date" name="resolution_date" 
            placeholder="{{ trans('brackets/admin-ui::admin.forms.select_date_and_time') }}">
        </datetime>
        <div v-if="errors.has('resolution_date')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('resolution_date') }}</div>
    </div>
</div>


<div class="row">
    <div class="form-group col-sm-6">
        <label for="atc">ATC</label>
        <multiselect
        
            v-model="form.atc"
            :options="atcs"
            :multiple="false"
            track-by="id"
            label="name"
            :taggable="true"
            tag-placeholder=""
            placeholder="{{ trans('admin.applicant.actions.search')  }}">
        </multiselect>

        <div v-if="errors.has('atc')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('atc') }}</div>
    </div>
</div>
<div class="row">
    <div class="form-group col-lg-6">
        <label for="observations" >Observaciones</label>
        <textarea v-model="form.observations" class="form-control"  id="form.observations" name="form.observations" id="" rows="3"></textarea>
        {{-- <input  type="text" v-model="form.file_number" v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('file_number'), 'form-control-success': fields.file_number && fields.file_number.valid}" id="file_number" name="file_number" placeholder="Nro. Expendiente"> --}}
        <div v-if="errors.has('observations')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('observations') }}</div>
    </div>
</div>

