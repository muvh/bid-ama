<div class="card">
    <div class="card-header">
        <i class="fa fa-check"></i>   {{ trans('admin.disease.title') }}
    </div>
    <div class="card-block">
        <div v-for="(disease, index) in form.diseasecomponents">
            <div class="row">
                <div class="form-group col-sm-12">
                    <div class="input-group" style="display: flex">

                        <multiselect
                            name="diseases[][id]"
                            v-model="disease.id"
                            :options="diseases"
                            :multiple="false"
                           
                            track-by="id"
                            label="name"
                            :taggable="true"
                            tag-placeholder=""
                            placeholder="{{ trans('admin.applicant.actions.search')  }}">
                        </multiselect>

                        <span class="input-group-append">
                            <button class="btn btn-danger" type="button" @click="deleteDisease(index)" ><i class="fa fa-minus"></i>&nbsp;{{ trans('brackets/admin-ui::admin.btn.delete') }}</button>
                        </span>

                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="form-group col-sm-12" style="text-align: right">
                <button type="button" class="btn btn-success" @click="addNewDisease()">
                    <i class="fa fa-plus"></i>
                    {{ trans('brackets/admin-ui::admin.btn.add') }}
                </button>
            </div>
        </div>
    </div>
</div>
