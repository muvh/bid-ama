@php
    $totalInscri=0;
    $totalPost=0;
    $totalAdj=0;
    $totalinha=0;
    $totalOtros=0;
    $totalCondi=0;
    $totalPEJ=0;
@endphp
@foreach ($dataStatus as $item)
@if($item['id']>17 && $item['id']<21 || $item['id']==37 || $item['id']==38 )
      @php
          $totalInscri=$totalInscri+$item['cont'];
          
      @endphp
@endif
@if($item['id']>20 && $item['id']<24 || $item['id']==40 )           
    @php
        $totalPost=$totalPost+$item['cont'];
        
    @endphp
@endif
@if($item['id']>23 && $item['id']<28  || ($item['id']==39) || ($item['id']==41)|| ($item['id']==42)|| ($item['id']==43)|| ($item['id']==44) || ($item['id']==47))           
    @php
        $totalAdj=$totalAdj+$item['cont'];
       
    @endphp
@endif
@if($item['id']==34 )           
    @php
        $totalAdj=$totalAdj+$item['cont'];
    @endphp
@endif
@if($item['id']>28 && $item['id']<33 ||$item['id']==28 )           
    @php
        $totalinha=$totalinha+$item['cont'];
    @endphp
@endif
@if($item['id']>0 && $item['id']<17 ||  $item['id']==33)             
    @php
        $totalOtros=$totalOtros+$item['cont'];
    @endphp
@endif
@if($item['id']==45)             
    @php
        $totalCondi=$totalCondi+$item['cont'];
    @endphp
@endif
@if($item['id']==46)             
    @php
        $totalPEJ=$totalPEJ+$item['cont'];
    @endphp
@endif
@endforeach
<div class="col">
    <div class="card text-white bg-primary text-center">
        <div class="card-body">
        <blockquote class="card-bodyquote rounded">
        <h2>Listado detallado de Estados</h2>
        </blockquote>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-3">
           {{-- bloque 1 --}}
           {{-- {{dd($dataStatus)}} --}}
           
            <button  class="btn btn-lg btn-block btn-square btn-secondary rounded" type="button">Pre - Inscripciones
                <span class="badge badge-primary badge-pill">{{$totalInscri}}</span>    
            </button>
            <br>
           <ul class="list-group">
            @foreach ($dataStatus as $item)
            @if($item['id']>17 && $item['id']<21 || $item['id']==37 || $item['id']==38 )
                
            @php
            $id=$item['id'];
        @endphp
           
               <li class="list-group-item d-flex justify-content-between align-items-center">
            <a class="nav-link"     
                   href="{{ route('admin/applicants/estados', ['status'=>$id]) }}">
                   {{$item['name']}}
                   <span class="badge badge-primary badge-pill">{{$item['cont']}}</span>
               </a>
                 </li>
           @endif
            @endforeach
        </ul>
<br>
        {{-- Condicionados --}}
        <ul class="list-group btn btn-lg ">
            @foreach ($dataStatus as $item)
            @if( $item['id']==45)       
                
            @php
            $id=$item['id'];
        @endphp
           
               <li class="list-group-item d-flex justify-content-between align-items-center ">
                   <a class="nav-link btn-block btn-square btn-warning rounded"   
                   href="{{ route('admin/applicants/estados', ['status'=>$id]) }}">
                   {{$item['name']}}
                   <span class="badge badge-primary badge-pill">{{$item['cont']}}</span>
               </a>
                 </li>
           @endif
            @endforeach
        </ul>

        {{-- PEJ --}}
        <ul class="list-group btn btn-lg ">
            @foreach ($dataStatus as $item)
            @if( $item['id']==46)       
                
            @php
            $id=$item['id'];
        @endphp
           
               <li class="list-group-item d-flex justify-content-between align-items-center ">
                   <a class="nav-link btn-block btn-square btn-warning rounded"   
                   href="{{ route('admin/applicants/estados', ['status'=>$id]) }}">
                   {{$item['name']}}
                   <span class="badge badge-primary badge-pill">{{$item['cont']}}</span>
               </a>
                 </li>
           @endif
            @endforeach
        </ul>
        
        </div>

   
        <div class="col-lg-3">
           {{-- bloque 1 --}}
           {{-- {{dd($dataStatus)}} --}}
           <button  class="btn btn-lg btn-block btn-square btn-success rounded" type="button">Postulaciones
            <span class="badge badge-primary badge-pill">{{$totalPost}}</span>    
        </button>
        <br>

           <ul class="list-group">
            @foreach ($dataStatus as $item)
            @if(($item['id']>20 && $item['id']<24) || $item['id']==40 )
                
            @php
            $id=$item['id'];
        @endphp
           
               <li class="list-group-item d-flex justify-content-between align-items-center">
                   <a class="nav-link"   
                   href="{{ route('admin/applicants/estados', ['status'=>$id]) }}">
                   {{$item['name']}}
                   <span class="badge badge-primary badge-pill">{{$item['cont']}}</span>
               </a>
                 </li>
           @endif
            @endforeach
        </ul>
        </div>
        <div class="col-lg-3">
           {{-- bloque 1 --}}
           {{-- {{dd($dataStatus)}} --}}
           <button  class="btn btn-lg btn-block btn-square btn-warning rounded" type="button">Adjudicaciones
            <span class="badge badge-primary badge-pill">{{$totalAdj}}</span>    
        </button>
        <br>

           <ul class="list-group">
            @foreach ($dataStatus as $item)
                @if(($item['id']>23 && $item['id']<28) || ($item['id']==39)  || ($item['id']==41)|| ($item['id']==42)|| ($item['id']==43)|| ($item['id']==44)  || ($item['id']==47))     
                
                @php
                $id=$item['id'];
            @endphp
               
                   <li class="list-group-item d-flex justify-content-between align-items-center">
                       <a class="nav-link"   
                       href="{{ route('admin/applicants/estados', ['status'=>$id]) }}">
                       {{$item['name']}}
                       <span class="badge badge-primary badge-pill">{{$item['cont']}}</span>
                   </a>
                     </li>
               @endif
            @endforeach
        </ul>
        </div>
        <div class="col-lg-3">
           {{-- bloque 1 --}}
           {{-- {{dd($dataStatus)}} --}}
           <button  class="btn btn-lg btn-block btn-square btn-danger rounded" type="button">Inhabilitaciones
            <span class="badge badge-primary badge-pill">{{$totalinha}}</span>    
        </button>
        <br>
        <ul class="list-group">
            @foreach ($dataStatus as $item)
            @if($item['id']>28 && $item['id']<33 || $item['id']==28)       
                
            @php
            $id=$item['id'];
        @endphp
           
               <li class="list-group-item d-flex justify-content-between align-items-center">
                   <a class="nav-link"   
                   href="{{ route('admin/applicants/estados', ['status'=>$id]) }}">
                   {{$item['name']}}
                   <span class="badge badge-primary badge-pill">{{$item['cont']}}</span>
               </a>
                 </li>
           @endif
            @endforeach
        </ul>
        </div>
        
        </div>



        </div>



