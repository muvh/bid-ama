<template v-if="showrelationships === true">
    <div class="row" >
        <div class="form-group col-sm-6">
            <label for="applicant_relationship">{{ trans('admin.applicant.columns.applicant_relationship') }}</label>

            <multiselect
                v-model="form.applicantrelationship"
                :options="applicantrelationships"
                :multiple="false"
                track-by="id"
                label="name"
                :taggable="true"
                tag-placeholder=""
                placeholder="{{ trans('admin.applicant.actions.search')  }}">
            </multiselect>

            <div v-if="errors.has('applicant_relationship')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('applicant_relationship') }}</div>
        </div>
    </div>
</template>

<div class="row">
    <div class="form-group col-sm-3">
        <label for="government_id" >{{ trans('admin.applicant.columns.government_id') }}</label>
        <div class="input-group mb-3">
        <input type="text" v-model="form.government_id" v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('government_id'), 'form-control-success': fields.government_id && fields.government_id.valid}" id="government_id" name="government_id" placeholder="{{ trans('admin.applicant.columns.government_id') }}">
        <div v-if="errors.has('government_id')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('government_id') }}</div>
        <div class="input-group-append">
            @if(Request::path()=='admin/applicants/create')
            <button  @click="findData" class="btn btn-primary" type="button"><i class="fa fa-random" aria-hidden="true"></i></button>
            @else
            <button @click="findDataIdentifi" class="btn btn-info" type="button"><i class="fa fa-plus" aria-hidden="true"></i></button>
            @endif
            <div id="ver" style="display: none;"><img width="40px" src="/images/loading.gif"></div>
          </div>
          </div>
    </div>
    <div class="form-group col-sm-4">
        <label for="names">{{ trans('admin.applicant.columns.names') }}</label>
        <input  type="text" v-model="form.names" v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('names'), 'form-control-success': fields.names && fields.names.valid}" id="names" name="names" placeholder="{{ trans('admin.applicant.columns.names') }}">
        <div v-if="errors.has('names')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('names') }}</div>
    </div>
    <div class="form-group col-sm-4">
        <label for="last_names" >{{ trans('admin.applicant.columns.last_names') }}</label>
        <input  type="text" v-model="form.last_names" v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('last_names'), 'form-control-success': fields.last_names && fields.last_names.valid}" id="last_names" name="last_names" placeholder="{{ trans('admin.applicant.columns.last_names') }}">
        <div v-if="errors.has('last_names')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('last_names') }}</div>
    </div>
</div>

<div class="row">
    <div class="form-group col-sm-3">
        <label for="nationality" >{{ trans('admin.applicant.columns.nationality') }}</label>
        <input  type="text" v-model="form.nationality" v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('nationality'), 'form-control-success': fields.nationality && fields.nationality.valid}" id="nationality" name="nationality" placeholder="{{ trans('admin.applicant.columns.nationality') }}">
        <div v-if="errors.has('nationality')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('nationality') }}</div>
    </div>
    <div class="form-group col-sm-3">
        <label for="marital_status" >{{ trans('admin.applicant.columns.marital_status') }}</label>
       {{--  {{$applicant->beneficiaries}} --}}
        <select class="form-control" v-model="form.marital_status" name="marital_status" id="">
            <option value="SO">SOLTERO/A</option>
            <option value="CA">CASADO/A</option>
            <option value="SE">SEPARADO/A</option>
            <option value="DI">DIVORCIADO/A</option>
            <option value="CU">CONCUBINADO/A</option>
            <option value="VI">VIUDO/A</option>
        </select>
        <div v-if="errors.has('marital_status')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('marital_status') }}</div>
    </div>
    <div class="form-group col-sm-3">
        <label for="gender">{{ trans('admin.applicant.columns.gender') }}</label>
        <input  type="text" v-model="form.gender" v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('gender'), 'form-control-success': fields.gender && fields.gender.valid}" id="gender" name="gender" placeholder="{{ trans('admin.applicant.columns.gender') }}">
        <div v-if="errors.has('gender')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('gender') }}</div>
    </div>
    <div class="form-group col-sm-3">
        <label for="birthdate">{{ trans('admin.applicant.columns.birthdate') }}</label>
        <div class="input-group input-group--custom">
            <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
            <datetime  
            v-model="form.birthdate" 
            :config="datePickerConfig" 
            
            class="flatpickr" 
            :class="{'form-control-danger': errors.has('birthdate'), 'form-control-success': fields.birthdate && fields.birthdate.valid}"
             id="birthdate" 
             name="birthdate" 
             placeholder="{{ trans('brackets/admin-ui::admin.forms.select_date_and_time') }}"></datetime>
        </div>
        <div v-if="errors.has('birthdate')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('birthdate') }}</div>
    </div>
</div>

<template v-if="form.gender === 'F'">
<div class="row">
    <div class="form-group col-sm-2">
        <label for="pregnant">{{ trans('admin.applicant.columns.pregnant') }}</label>
        <select v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('pregnant'), 'form-control-success': fields.pregnant && fields.pregnant.valid}" v-model="form.pregnant" name="pregnant" id="pregnant">
            <option v-for="option in pregnantOption" 
            :selected="option.value == false"
            :value="option.value">
            @{{ option.text }}
        </option>
        </select>
        <div v-if="errors.has('pregnant')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('pregnant') }}</div>
    </div>
    <div class="form-group col-sm-4">
        <label for="pregnancy_due_date">{{ trans('admin.applicant.columns.pregnancy_due_date') }}</label>
        <input type="text" v-model="form.pregnancy_due_date" v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('pregnancy_due_date'), 'form-control-success': fields.pregnancy_due_date && fields.pregnancy_due_date.valid}" id="pregnancy_due_date" name="pregnancy_due_date" placeholder="{{ trans('admin.applicant.columns.pregnancy_due_date') }}">
        <div v-if="errors.has('pregnancy_due_date')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('pregnancy_due_date') }}</div>
    </div>
</div>
</template>

<div class="row">
    <div class="form-group col-sm-3">
        <label for="education_level">{{ trans('admin.applicant.columns.education_level') }}</label>

        <multiselect
            v-model="form.educationlevel"
            :options="educationlevels"
            :multiple="false"
            track-by="id"
            label="name"
            :taggable="true"
            tag-placeholder=""
            placeholder="{{ trans('admin.applicant.actions.search')  }}">
        </multiselect>

        <div v-if="errors.has('education_level')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('education_level') }}</div>
    </div>
    <div class="form-group col-sm-3">
        <label for="occupation">{{ trans('admin.applicant.columns.occupation') }}</label>
        <input type="text" v-model="form.occupation" v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('occupation'), 'form-control-success': fields.occupation && fields.occupation.valid}" id="occupation" name="occupation" placeholder="{{ trans('admin.applicant.columns.occupation') }}">
        <div v-if="errors.has('occupation')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('occupation') }}</div>
    </div>
    <div class="form-group col-sm-3">
        <label for="monthly_income">{{ trans('admin.applicant.columns.monthly_income') }}</label>
        <input type="text" v-model="form.monthly_income" v-validate="'decimal'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('monthly_income'), 'form-control-success': fields.monthly_income && fields.monthly_income.valid}" id="monthly_income" name="monthly_income" placeholder="{{ trans('admin.applicant.columns.monthly_income') }}">
        <div v-if="errors.has('monthly_income')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('monthly_income') }}</div>
    </div>
    <div class="form-group col-sm-3">
        <label for="ruc">{{ trans('admin.applicant.columns.ruc') }}</label>
        <input type="text" v-model="form.ruc" v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('ruc'), 'form-control-success': fields.ruc && fields.ruc.valid}" id="ruc" name="ruc" placeholder="{{ trans('admin.applicant.columns.ruc') }}">
        <div v-if="errors.has('ruc')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('ruc') }}</div>
    </div>
</div>

