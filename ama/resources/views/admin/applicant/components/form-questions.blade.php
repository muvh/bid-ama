<template v-for="question in form.questions">

    <div class="row">
        <div class="form-group col-sm-12">
            <div>@{{ question.text  }}</div>
            <div v-if="errors.has('government_id')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('government_id') }}</div>
        </div>
    </div>

    <div class="row">

        <template v-if="question.type === 'B'">

            <div class="form-group col-sm-1">
                <select class="form-control" name="question[][value]" v-model="question.value">
                    <option v-bind:value="boolean.value" v-for="boolean in form.booleans">@{{ boolean.text }}</option>
                </select>
            </div>

            <template v-if="question.comment === true">
                <div v-bind:class="{'form-group col-sm-8': question.received_at,  'form-group col-sm-11': !question.received_at}">
                    <input type="text" v-model="question.comment_text" class="form-control" name="comment" :placeholder="[[question.comment_placeholder]]">
                </div>
            </template>

            <template v-if="question.received_at === true">
                <div class="form-group col-sm-3">
                    <datetime v-model="question.received_at_value" :config="datePickerConfig" v-validate="'date_format:yyyy-MM-dd'" class="flatpickr" :class="{'form-control-danger': errors.has('received_at'), 'form-control-success': fields.received_at && fields.received_at.valid}" id="received_at" name="received_at" placeholder="{{ trans('brackets/admin-ui::admin.forms.select_date_and_time') }}"></datetime>
                </div>
            </template>


        </template>

        <template v-if="question.type === 'V'">

            <div class="form-group col-sm-5">
                <select class="form-control" name="question[][value]" v-model="question.value">
                    <option v-bind:value="item" v-for="item in question.values">
                        @{{ item }}
                    </option>
                </select>
            </div>

            <template v-if="question.comment === true">
                <div v-bind:class="{'form-group col-sm-4': question.received_at,  'form-group col-sm-7': !question.received_at}">
                    <input type="text" v-model="question.comment_text" class="form-control" name="comment" :placeholder="[[question.comment_placeholder]]">
                </div>
            </template>

            <template v-if="question.received_at === true">
                <div class="form-group col-sm-3">
                    <datetime v-model="question.received_at_value" :config="datePickerConfig" v-validate="'date_format:yyyy-MM-dd'" class="flatpickr" :class="{'form-control-danger': errors.has('received_at'), 'form-control-success': fields.received_at && fields.received_at.valid}" id="received_at" name="received_at" placeholder="{{ trans('brackets/admin-ui::admin.forms.select_date_and_time') }}"></datetime>
                </div>
            </template>

        </template>

        <template v-if="question.type === 'T'">

            <template v-if="question.comment === true && question.received_at === true">
                <div class="form-group col-sm-5">
                    <input type="text" v-model="question.value" class="form-control" name="comment" :placeholder="[[question.extended_placeholder]]">
                </div>
            </template>

            <template v-else-if="question.comment === true && question.received_at === false">
                <div class="form-group col-sm-5">
                    <input type="text" v-model="question.value" class="form-control" name="comment" :placeholder="[[question.extended_placeholder]]">
                </div>
            </template>

            <template v-else-if="question.comment === false && question.received_at === true">
                <div class="form-group col-sm-9">
                    <input type="text" v-model="question.value" class="form-control" name="comment" :placeholder="[[question.extended_placeholder]]">
                </div>
            </template>

            <template v-else>
                <div class="form-group col-sm-12">
                    <input type="text" v-model="question.value" class="form-control" name="comment" :placeholder="[[question.extended_placeholder]]">
                </div>
            </template>

            <template v-if="question.comment === true">
                <div v-bind:class="{'form-group col-sm-4': question.received_at,  'form-group col-sm-7': !question.received_at}">
                    <input type="text" v-model="question.comment_text" class="form-control" name="comment" :placeholder="[[question.comment_placeholder]]">
                </div>
            </template>

            <template v-if="question.received_at === true">
                <div class="form-group col-sm-3">
                    <datetime v-model="question.received_at_value" :config="datePickerConfig" v-validate="'date_format:yyyy-MM-dd'" class="flatpickr" :class="{'form-control-danger': errors.has('received_at'), 'form-control-success': fields.received_at && fields.received_at.valid}" id="received_at" name="received_at" placeholder="{{ trans('brackets/admin-ui::admin.forms.select_date_and_time') }}"></datetime>
                </div>
            </template>

        </template>

    </div>
    <hr>
</template>












