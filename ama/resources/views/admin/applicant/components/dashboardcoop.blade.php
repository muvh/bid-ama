<div class="col">
    <div class="card text-white bg-primary text-center">
        <div class="card-body">
        <blockquote class="card-bodyquote">
        <h2>Bienvenido {{ $user->first_name }}</h2>
        <p>Seleccione una de las siguientes opciones para continuar</p>
        <footer>
            <div class="row align-items-center">
                <div class="col-6 col-sm-4 col-md-2 col-xl mb-3 mb-xl-0">
                    <a href="/admin/applicants"><button  class="btn btn-lg btn-block btn-square btn-secondary" type="button">Postulantes</button></a>
                </div>
                <div class="col-6 col-sm-4 col-md-2 col-xl mb-3 mb-xl-0">
                <a href="/admin/reports/applicants-coop-status?type_report=coop"><button class="btn btn-lg btn-block btn-square btn-lg btn-success" type="button">Reportes</button></a>
                </div>
                <div class="col-6 col-sm-4 col-md-2 col-xl mb-3 mb-xl-0">
                <a href="/admin/applicants/processed"><button class="btn btn-lg btn-block btn-square btn-warning" type="button">{{ trans('admin.applicant-processed.title') }}</button></a>
                </div>
                <div class="col-6 col-sm-4 col-md-2 col-xl mb-3 mb-xl-0">
                <button class="btn btn-lg btn-block btn-square btn-danger" type="button">{{ trans('admin.applicant.rejected') }}</button>
                </div>
                </div>
        </footer>
        </blockquote>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6">
            <applicant-listing
        :data="{{ $data->toJson() }}"
        :url="'{{ url('admin/applicants') }}'"
        inline-template>

        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="card-header">
                        <i class="fa fa-align-justify"></i> {{ trans('admin.applicant.actions.index') }}
                        <a class="btn btn-primary btn-spinner btn-sm pull-right m-b-0" href="{{ url('admin/applicants/create') }}" role="button"><i class="fa fa-plus"></i>&nbsp; {{ trans('admin.applicant.actions.create') }}</a>
                    </div>

                    <div class="card-body" v-cloak>
                        <div class="card-block">
                            <form @submit.prevent="">
                                <div class="row justify-content-md-between">
                                    <div class="col col-lg-7 col-xl-5 form-group">
                                        <div class="input-group">
                                            <input class="form-control" placeholder="{{ trans('brackets/admin-ui::admin.placeholder.search') }}" v-model="search" @keyup.enter="filter('search', $event.target.value)" />
                                            <span class="input-group-append">
                                                <button type="button" class="btn btn-primary" @click="filter('search', search)"><i class="fa fa-search"></i>&nbsp; {{ trans('brackets/admin-ui::admin.btn.search') }}</button>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="col-sm-auto form-group ">
                                        <select class="form-control" v-model="pagination.state.per_page">

                                            <option value="10">10</option>
                                            <option value="25">25</option>
                                            <option value="100">100</option>
                                        </select>
                                    </div>
                                </div>
                            </form>

                            <table class="table table-hover table-listing">
                                <thead>
                                    <tr>
                                        <!-- <th is='sortable' :column="'id'">{{ trans('admin.applicant.columns.id') }}</th> -->
                                        <th is='sortable' :column="'names'">{{ trans('admin.applicant.columns.names') }}</th>
                                        <th is='sortable' :column="'last_names'">{{ trans('admin.applicant.columns.last_names') }}</th>
                                        <th is='sortable' :column="'government_id'" style="text-align: right">{{ trans('admin.applicant.columns.government_id') }}</th>
                                        <th is='sortable' :column="'city_name'">{{ trans('admin.applicant.columns.city_id') }}</th>
                                        <!-- <th is='sortable' :column="'total_beneficiaries'">{{ trans('admin.applicant.columns.beneficiaries_short') }}</th> -->
                                        <th is='sortable' :column="'total_monthly_income'" style="text-align: right">{{ trans('admin.applicant.columns.sum') }}</th>
                                        @if($roles->contains('ama'))
                                            <th is='sortable' :column="'fentityname'">{{ trans('admin.applicant.columns.financial_entity') }}</th>
                                            <th is='sortable' :column="'centityname'">{{ trans('admin.applicant.columns.construction_entity') }}</th>
                                        @endif

                                        <th></th>
                                    </tr>
                                    <tr v-show="(clickedBulkItemsCount > 0) || isClickedAll">
                                        <td class="bg-bulk-info d-table-cell text-center" colspan="20">
                                            <span class="align-middle font-weight-light text-dark">{{ trans('brackets/admin-ui::admin.listing.selected_items') }} @{{ clickedBulkItemsCount }}.  <a href="#" class="text-primary" @click="onBulkItemsClickedAll('/admin/applicants')" v-if="(clickedBulkItemsCount < pagination.state.total)"> <i class="fa" :class="bulkCheckingAllLoader ? 'fa-spinner' : ''"></i> {{ trans('brackets/admin-ui::admin.listing.check_all_items') }} @{{ pagination.state.total }}</a> <span class="text-primary">|</span> <a
                                                        href="#" class="text-primary" @click="onBulkItemsClickedAllUncheck()">{{ trans('brackets/admin-ui::admin.listing.uncheck_all_items') }}</a>  </span>

                                            <span class="pull-right pr-2">
                                                <button class="btn btn-sm btn-danger pr-3 pl-3" @click="bulkDelete('/admin/applicants/bulk-destroy')">{{ trans('brackets/admin-ui::admin.btn.delete') }}</button>
                                            </span>

                                        </td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr v-for="(item, index) in collection" :key="item.id" :class="bulkItems[item.id] ? 'bg-bulk' : ''">
                                        <!-- <td>@{{ item.id }}</td> -->
                                        <td>@{{ item.names }}</td>
                                        <td>@{{ item.last_names }}</td>
                                        <td style="text-align: right">@{{ item.government_id | numberFormat }}</td>
                                        <td>@{{ item.city_name }}</td>
                                        <!-- <td style="text-align: center">@{{ item.total_beneficiaries }}</td> -->
                                        <td style="text-align: right">@{{ item.total_monthly_income | numberFormat }}</td>
                                        @if($roles->contains('ama'))
                                            <td v-tooltip="item.financial.name">@{{ item.financial ? item.financial.name : '' | limitCharecters(10) }}</td>
                                            <td v-tooltip="item.construction.name">@{{ item.construction.name ? item.construction.name : '' | limitCharecters(10) }}</td>
                                        @endif
                                        <td>
                                            <div class="row no-gutters">
                                                <div class="col-auto">
                                                    <a class="btn btn-sm btn-spinner btn-info" :href="item.resource_url + '/edit'" title="{{ trans('brackets/admin-ui::admin.btn.edit') }}" role="button"><i class="fa fa-edit"></i></a>
                                                </div>
                                                <form class="col" @submit.prevent="deleteItem(item.resource_url)">
                                                    <button type="submit" class="btn btn-sm btn-danger" title="{{ trans('brackets/admin-ui::admin.btn.delete') }}"><i class="fa fa-trash-o"></i></button>
                                                </form>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>

                            <div class="row" v-if="pagination.state.total > 0">
                                <div class="col-sm">
                                    <span class="pagination-caption">{{ trans('brackets/admin-ui::admin.pagination.overview') }}</span>
                                </div>
                                <div class="col-sm-auto">
                                    <pagination></pagination>
                                </div>
                            </div>

                            <div class="no-items-found" v-if="!collection.length > 0">
                                <i class="icon-magnifier"></i>
                                <h3>{{ trans('brackets/admin-ui::admin.index.no_items') }}</h3>
                                <p>{{ trans('brackets/admin-ui::admin.index.try_changing_items') }}</p>
                                <a class="btn btn-primary btn-spinner" href="{{ url('admin/applicants/create') }}" role="button"><i class="fa fa-plus"></i>&nbsp; {{ trans('admin.applicant.actions.create') }}</a>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </applicant-listing>
        </div>

        <div class="col-lg-6">
            <applicant-listing
            :data="{{ $dataprocessed->toJson() }}"
            :url="'{{ url('admin/applicants/processed') }}'"
            inline-template>

            <div class="row">
                <div class="col">
                    <div class="card">
                        <div class="card-header">
                            <i class="fa fa-align-justify"></i> {{ trans('admin.applicant-processed.title') }}
                                <a class="btn btn-primary btn-spinner btn-sm pull-right m-b-0" href="{{ url('admin/applicants/create') }}" role="button"><i class="fa fa-plus"></i>&nbsp; {{ trans('admin.applicant.actions.create') }}</a>
                        </div>
                        <div class="card-body" v-cloak>
                            <div class="card-block">
                                <form @submit.prevent="">
                                    <div class="row justify-content-md-between">
                                        <div class="col col-lg-7 col-xl-5 form-group">
                                            <div class="input-group">
                                                <input class="form-control" placeholder="{{ trans('brackets/admin-ui::admin.placeholder.search') }}" v-model="search" @keyup.enter="filter('search', $event.target.value)" />
                                                <span class="input-group-append">
                                                    <button type="button" class="btn btn-primary" @click="filter('search', search)"><i class="fa fa-search"></i>&nbsp; {{ trans('brackets/admin-ui::admin.btn.search') }}</button>
                                                </span>
                                            </div>
                                        </div>
                                        <div class="col-sm-auto form-group ">
                                            <select class="form-control" v-model="pagination.state.per_page">

                                                <option value="10">10</option>
                                                <option value="25">25</option>
                                                <option value="100">100</option>
                                            </select>
                                        </div>
                                    </div>
                                </form>

                                <table class="table table-hover table-listing">
                                    <thead>
                                        <tr>
                                            <!-- <th is='sortable' :column="'id'">{{ trans('admin.applicant.columns.id') }}</th> -->
                                            <th is='sortable' :column="'names'">{{ trans('admin.applicant.columns.names') }}</th>
                                            <!-- <th is='sortable' :column="'last_names'">{{ trans('admin.applicant.columns.last_names') }}</th> -->
                                            <th is='sortable' :column="'government_id'" style="text-align: right">{{ trans('admin.applicant.columns.government_id') }}</th>
                                            <th is='sortable' :column="'city_name'">{{ trans('admin.applicant.columns.city_id') }}</th>
                                            <!-- <th is='sortable' :column="'total_beneficiaries'">{{ trans('admin.applicant.columns.beneficiaries_short') }}</th> -->
                                            <th is='sortable' :column="'total_monthly_income'" style="text-align: right">{{ trans('admin.applicant.columns.sum') }}</th>
                                            @if($roles->contains('ama'))
                                                <th is='sortable' :column="'fentityname'">{{ trans('admin.applicant.columns.financial_entity') }}</th>
                                                <th is='sortable' :column="'centityname'">{{ trans('admin.applicant.columns.construction_entity') }}</th>
                                            @endif

                                            <!-- <th></th> -->
                                        </tr>
                                        <tr v-show="(clickedBulkItemsCount > 0) || isClickedAll">
                                            <td class="bg-bulk-info d-table-cell text-center" colspan="20">
                                                <span class="align-middle font-weight-light text-dark">{{ trans('brackets/admin-ui::admin.listing.selected_items') }} @{{ clickedBulkItemsCount }}.  <a href="#" class="text-primary" @click="onBulkItemsClickedAll('/admin/applicants')" v-if="(clickedBulkItemsCount < pagination.state.total)"> <i class="fa" :class="bulkCheckingAllLoader ? 'fa-spinner' : ''"></i> {{ trans('brackets/admin-ui::admin.listing.check_all_items') }} @{{ pagination.state.total }}</a> <span class="text-primary">|</span> <a
                                                            href="#" class="text-primary" @click="onBulkItemsClickedAllUncheck()">{{ trans('brackets/admin-ui::admin.listing.uncheck_all_items') }}</a>  </span>

                                                <span class="pull-right pr-2">
                                                    <button class="btn btn-sm btn-danger pr-3 pl-3" @click="bulkDelete('/admin/applicants/bulk-destroy')">{{ trans('brackets/admin-ui::admin.btn.delete') }}</button>
                                                </span>

                                            </td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr v-for="(item, index) in collection" :key="item.id" :class="bulkItems[item.id] ? 'bg-bulk' : ''">
                                            <!-- <td>@{{ item.id }}</td> -->
                                            <td>@{{ item.names }} @{{ item.last_names }}</td>
                                            <!-- <td>@{{ item.last_names }}</td> -->
                                            <td style="text-align: right">@{{ item.government_id | numberFormat }}</td>
                                            <td>@{{ item.city_name }}</td>
                                            <!-- <td style="text-align: center">@{{ item.total_beneficiaries }}</td> -->
                                            <td style="text-align: right">@{{ item.total_monthly_income | numberFormat }}</td>
                                            @if($roles->contains('ama'))
                                                <td v-tooltip="item.financial.name">@{{ item.financial ? item.financial.name : '' | limitCharecters(10) }}</td>
                                                <td v-tooltip="item.construction.name">@{{ item.construction.name ? item.construction.name : '' | limitCharecters(10) }}</td>
                                            @endif
                                            <!--<td>
                                                <div class="row no-gutters">
                                                    <div class="col-auto">
                                                        <a class="btn btn-sm btn-spinner btn-info" :href="item.resource_url + '/edit'" title="{{ trans('brackets/admin-ui::admin.btn.edit') }}" role="button"><i class="fa fa-edit"></i></a>
                                                    </div>
                                                    <form class="col" @submit.prevent="deleteItem(item.resource_url)">
                                                        <button type="submit" class="btn btn-sm btn-danger" title="{{ trans('brackets/admin-ui::admin.btn.delete') }}"><i class="fa fa-trash-o"></i></button>
                                                    </form>
                                                </div>
                                            </td> -->
                                        </tr>
                                    </tbody>
                                </table>

                                <div class="row" v-if="pagination.state.total > 0">
                                    <div class="col-sm">
                                        <span class="pagination-caption">{{ trans('brackets/admin-ui::admin.pagination.overview') }}</span>
                                    </div>
                                    <div class="col-sm-auto">
                                        <pagination></pagination>
                                    </div>
                                </div>

                                <div class="no-items-found" v-if="!collection.length > 0">
                                    <i class="icon-magnifier"></i>
                                    <h3>{{ trans('brackets/admin-ui::admin.index.no_items') }}</h3>
                                    <p>{{ trans('brackets/admin-ui::admin.index.try_changing_items') }}</p>
                                    <a class="btn btn-primary btn-spinner" href="{{ url('admin/applicants/create') }}" role="button"><i class="fa fa-plus"></i>&nbsp; {{ trans('admin.applicant.actions.create') }}</a>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </applicant-listing>
        </div>

        </div>

</div>

