@php
    $totalInscri=0;
    $totalPost=0;
    $totalAdj=0;
    $totalinha=0;
    $totalOtros=0;
    $totalCondi=0;
    $totalPEJ=0;
    $totalLE=0;
    $totalILE=0;
@endphp
@foreach ($dataStatus as $item)
@if($item['id']>17 && $item['id']<21 || $item['id']==37 || $item['id']==38 )
      @php
          $totalInscri=$totalInscri+$item['cont'];
          
      @endphp
@endif
@if($item['id']>20 && $item['id']<24 || $item['id']==40 )           
    @php
        $totalPost=$totalPost+$item['cont'];
        
    @endphp
@endif
@if($item['id']>23 && $item['id']<28  || ($item['id']==39) || ($item['id']==41)|| ($item['id']==42)|| ($item['id']==43)|| ($item['id']==44) || ($item['id']==47) || ($item['id']==50))           
    @php
        $totalAdj=$totalAdj+$item['cont'];
       
    @endphp
@endif
@if($item['id']==34 )           
    @php
        $totalAdj=$totalAdj+$item['cont'];
    @endphp
@endif
@if($item['id']>28 && $item['id']<33 ||$item['id']==28 )           
    @php
        $totalinha=$totalinha+$item['cont'];
    @endphp
@endif
@if($item['id']>0 && $item['id']<17 ||  $item['id']==33)             
    @php
        $totalOtros=$totalOtros+$item['cont'];
    @endphp
@endif
@if($item['id']==45)             
    @php
        $totalCondi=$totalCondi+$item['cont'];
    @endphp
@endif
@if($item['id']==46)             
    @php
        $totalPEJ=$totalPEJ+$item['cont'];
    @endphp
@endif
@endforeach
<div class="col">
    <div class="card text-white bg-primary text-center">
        <div class="card-body">
        <blockquote class="card-bodyquote rounded">
        <h2>Listado detallado de Estados</h2>
        </blockquote>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-3">
           {{-- bloque 1 --}}
           {{-- {{dd($dataStatus)}} --}}
           
            <button  class="btn btn-lg btn-block btn-square btn-secondary rounded" type="button">
                <a target="_blank" href="/admin/applicants/levels/INS">
                     Pre - Inscripciones
                <span class="badge badge-primary badge-pill">{{$totalInscri}}</span>   <span> <i class="nav-icon icon-flag"></i></span>
            </a>
            </button>
            <br>
           <ul class="list-group">
            @foreach ($dataStatus as $item)
            @if($item['id']>17 && $item['id']<21 || $item['id']==37 || $item['id']==38 )
                
            @php
            $id=$item['id'];
        @endphp
           
               <li class="list-group-item d-flex justify-content-between align-items-center">
            <a class="nav-link"     
                   href="{{ route('admin/applicants/estados', ['status'=>$id]) }}">
                   {{$item['name']}}
                   <span class="badge badge-primary badge-pill">{{$item['cont']}}</span>
               </a>
                 </li>
           @endif
            @endforeach
        </ul>
<br>
        {{-- Condicionados --}}
        <ul class="list-group btn btn-lg ">
            @foreach ($dataStatus as $item)
            @if( $item['id']==45)       
                
            @php
            $id=$item['id'];
        @endphp
           
               <li class="list-group-item d-flex justify-content-between align-items-center ">
                   <a class="nav-link btn-block btn-square btn-warning rounded"   
                   href="{{ route('admin/applicants/estados', ['status'=>$id]) }}">
                   {{$item['name']}}
                   <span class="badge badge-primary badge-pill">{{$item['cont']}}</span>
               </a>
                 </li>
           @endif
            @endforeach
        </ul>

        {{-- PEJ --}}
        <ul class="list-group btn btn-lg ">
            @foreach ($dataStatus as $item)
            @if( $item['id']==46)       
                
            @php
            $id=$item['id'];
        @endphp
           
               <li class="list-group-item d-flex justify-content-between align-items-center ">
                   <a class="nav-link btn-block btn-square btn-warning rounded"   
                   href="{{ route('admin/applicants/estados', ['status'=>$id]) }}">
                   {{$item['name']}}
                   <span class="badge badge-primary badge-pill">{{$item['cont']}}</span>
               </a>
                 </li>
           @endif
            @endforeach
        </ul>
        {{-- LE --}}
        <ul class="list-group btn btn-lg ">
            @foreach ($dataStatus as $item)
            @if( $item['id']==48)       
                
            @php
            $id=$item['id'];
        @endphp
           
               <li class="list-group-item d-flex justify-content-between align-items-center ">
                   <a class="nav-link btn-block btn-square btn-warning rounded"   
                   href="{{ route('admin/applicants/estados', ['status'=>$id]) }}">
                   {{$item['name']}}
                   <span class="badge badge-primary badge-pill">{{$item['cont']}}</span>
               </a>
                 </li>
           @endif
            @endforeach
        </ul>
        {{-- ILE --}}
        <ul class="list-group btn btn-lg ">
            @foreach ($dataStatus as $item)
            @if( $item['id']==49)       
                
            @php
            $id=$item['id'];
        @endphp
           
               <li class="list-group-item d-flex justify-content-between align-items-center ">
                   <a class="nav-link btn-block btn-square btn-danger rounded"   
                   href="{{ route('admin/applicants/estados', ['status'=>$id]) }}">
                   {{$item['name']}}
                   <span class="badge badge-primary badge-pill">{{$item['cont']}}</span>
               </a>
                 </li>
           @endif
            @endforeach
        </ul>
        
        </div>

   
        <div class="col-lg-3">
           {{-- bloque 1 --}}
           {{-- {{dd($dataStatus)}} --}}
           <button  class="btn btn-lg btn-block btn-square btn-success rounded" type="button">
            <a target="_blank" href="/admin/applicants/levels/POS">
                <span style="color:white">Postulaciones</span>
                <span class="badge badge-primary badge-pill">{{$totalPost}}</span>    
                <span style="color:white"> <i class="nav-icon icon-flag"></i></span>
            </a>
        </button>
        <br>

           <ul class="list-group">
            @foreach ($dataStatus as $item)
            @if(($item['id']>20 && $item['id']<24) || $item['id']==40 )
                
            @php
            $id=$item['id'];
        @endphp
           
               <li class="list-group-item d-flex justify-content-between align-items-center">
                   <a class="nav-link"   
                   href="{{ route('admin/applicants/estados', ['status'=>$id]) }}">
                   {{$item['name']}}
                   <span class="badge badge-primary badge-pill">{{$item['cont']}}</span>
               </a>
                 </li>
           @endif
            @endforeach
        </ul>
        </div>
        <div class="col-lg-3">
           {{-- bloque 1 --}}
           {{-- {{dd($dataStatus)}} --}}
           <button  class="btn btn-lg btn-block btn-square btn-warning rounded" type="button">
            <a target="_blank" href="/admin/applicants/levels/ADJ">Adjudicaciones
                <span class="badge badge-primary badge-pill">{{$totalAdj}}</span>    
                <span> <i class="nav-icon icon-flag"></i></span>
            </a>
        </button>
        <br>

           <ul class="list-group">
            @foreach ($dataStatus as $item)
                @if(($item['id']>23 && $item['id']<28) || ($item['id']==39)  || ($item['id']==41)|| ($item['id']==42)|| ($item['id']==43)|| ($item['id']==44)  || ($item['id']==47) || ($item['id']==50))     
                
                @php
                $id=$item['id'];
            @endphp
               
                   <li class="list-group-item d-flex justify-content-between align-items-center">
                       <a class="nav-link"   
                       href="{{ route('admin/applicants/estados', ['status'=>$id]) }}">
                       {{$item['name']}}
                       <span class="badge badge-primary badge-pill">{{$item['cont']}}</span>
                   </a>
                     </li>
               @endif
            @endforeach
        </ul>
        </div>
        <div class="col-lg-3">
           {{-- bloque 1 --}}
           {{-- {{dd($dataStatus)}} --}}
           <button  class="btn btn-lg btn-block btn-square btn-danger rounded" type="button">
            <a target="_blank" href="/admin/applicants/levels/INH">
                <span style="color:white">Inhabilitaciones<span>
                    <span class="badge badge-primary badge-pill">{{$totalinha}}</span>    
                    <span> <i class="nav-icon icon-flag"></i></span>
                </a>
        </button>
        <br>
        <ul class="list-group">
            @foreach ($dataStatus as $item)
            @if($item['id']>28 && $item['id']<33 || $item['id']==28)       
                
            @php
            $id=$item['id'];
        @endphp
           
               <li class="list-group-item d-flex justify-content-between align-items-center">
                   <a class="nav-link"   
                   href="{{ route('admin/applicants/estados', ['status'=>$id]) }}">
                   {{$item['name']}}
                   <span class="badge badge-primary badge-pill">{{$item['cont']}}</span>
               </a>
                 </li>
           @endif
            @endforeach
        </ul>
        </div>
        
        </div>



        </div>



