@extends('brackets/admin-ui::admin.layout.default')

@section('title', trans('admin.applicant.actions.index'))

@section('body')

@if($roles->contains('ama'))
@include('admin.applicant.components.dashboard-gerencial')
@endif

@if($roles->contains('atc'))
@include('admin.applicant.components.dashboardatc')
@endif

@if($roles->contains('coop'))
@include('admin.applicant.components.dashboardcoop')
@endif

@endsection
