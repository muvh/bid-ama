@extends('brackets/admin-ui::admin.layout.default')

@section('title', trans('admin.applicant.actions.edit', ['name' => $applicant->id]))

@section('body')

    @php
        $roles = Auth::user()->getRoleNames();
       //dd($applicant)
       $statename = isset($statuse[0]) ? $statuse[0] :'';
    @endphp
<div class="container-fluid">
    <code>
        {{-- {{@$applicant->work[0]}} --}}

    </code>
    <tabs>
        <div class="alert alert-primary" role="alert">
            <div class="row">
                <div class="col-sm-5">
                    @if($applicant->financial->name)
                    <h4>IF/CAC: {{$applicant->financial->name}}
                     
                        <a class="btn btn-sm btn-info"  title="Agregar IF/CAC" data-toggle="modal" data-target="#exampleModalLg2" role="button"><i class="fa fa-edit"></i></a>
                    </h4> 

                    @else
                    
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalLg2" role="button">
                            <i class="fa fa-plus"></i>&nbsp; Agregar Cooperativa
                        </button>
                        {{-- <button class="btn btn-primary" data-toggle="modal" data-target="#exampleModalLg" >Cambiar Estado</button> </h4> --}}
                    @endif
            
                    <h4>Solicitante: {{ $applicant->names }} {{ $applicant->last_names }} </h4>

                    <h4>Estado: {{$applicant->statuses->status->name }} <button class="btn btn-primary"data-toggle="modal" data-target="#exampleModalLg" >Cambiar Estado</button> </h4>
                </div>
                <div class="col-sm-3">
                    <h4>Nro de Expediente: {{$applicant->file_number}}</h4>
                    <h4>Nro de Resolucion: {{$applicant->resolution_number}}</h4>
                   @if($applicant->construction->name)
                    <h4>ATC Asignado: {{$applicant->construction->name}} 
                        <a class="btn btn-sm btn-info"  title="Asignacion de ATC"  data-toggle="modal" data-target="#exampleModal2" role="button"><i class="fa fa-edit"></i></a>
                      <br>  Fecha Asig:  {{$applicant->atc_fec_asig ? date('d/m/Y', strtotime($applicant->atc_fec_asig)):''}}
                    </h4>
                    @else
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal2">
                        <i class="fa fa-plus"></i>&nbsp; Agregar ATCS
                    </button>
                    @endif
                </div>
                <div class="col-sm-2">
                    <h4>Supervisor:
                   @if(@$applicant->work[0]->supervisor->name )
                    
                      {{@$applicant->work[0]->supervisor->name }}
                    @else
                        <a class="btn btn-sm btn-info"  title="Agregar Supervisor"  data-toggle="modal" data-target="#exampleModal3" role="button"><i class="fa fa-edit"></i></a>
                    @endif
                        </h4>
                    <h4>Fechas de obras: {{-- {{@$applicant->work[0]->obrasfecha[0]->count()}} --}}
                       {{-- {{ $applicant->work[0]->obrasfecha[0]->type_check_id}} --}}
                       @if(@$applicant->work[0]->supervisor->name)
                                       
                                    
                                        @php ($val = 0)
                                        @foreach (@$applicant->work[0]->obrasfecha as $obra)
                                            
                                            @if(@$obra->type_check_id==1)
                                                <br>Inicio: {{ date('d/m/Y', strtotime($obra->fecha_check)) }} 
                                                @php ($val++)
                                            @endif   
                                            @if(@$obra->type_check_id==2)
                                            <br> Fin de obras: {{ date('d/m/Y', strtotime($obra->fecha_check)) }} 
                                            @php ($val++)
                                            @endif   
                                       
                                        @endforeach
                        @endif
                        @if(@$val<2 && @$applicant->work[0]->supervisor->name)
                            <a class="btn btn-sm btn-info"  title="Agregar Fechas de obras"  data-toggle="modal" data-target="#exampleModal4" role="button"><i class="fa fa-edit"></i></a>
                        @endif
                       
                  
                    </h4>
                </div>
                <div class="col-sm-2">
                    <h4>Responsable:</h4>
                    {{-- <a class="btn btn-sm btn-info"  title="Agregar Fechas de obras"  data-toggle="modal" data-target="#exampleModal5" role="button"><i class="fa fa-edit"></i></a> --}}
                </div>
            
                @if(session('message'))
                <div class="alert alert-danger" role="alert">
                    {{ session('message') }}
                  </div>
                @endif
            </div>
        </div>
            <tab name="Solicitante">
                <applicant-form
                    :action="'{{ $applicant->resource_url }}'"
                    :data = "{{ $applicant->toJson() }}"

                    :cities="{{$cities->toJson()}}"
                    :cities="{{$cities->toJson()}}"
                    :educationlevels="{{$educationlevels->toJson()}}"
                    :statusname="{{$statusName->toJson()}}"

                    :diseases="{{$diseases->toJson()}}"
                    :disabilities="{{$disabilities->toJson()}}"
                    :contactmethods="{{$contactmethods->toJson()}}"

                    :saveddiseases="{{ $saveddiseases }}"
                    :saveddisabilities="{{ $saveddisabilities }}"
                    :savedcontactmethods="{{ $savedcontactmethods }}"

                    :showrelationships = false
                    :finddataurl = "'{{ url('admin/applicants') }}'"

                    v-cloak
                    inline-template>

                    <form class="form-horizontal form-create" method="post" @submit.prevent="onSubmit" :action="action" novalidate>

                        <div class="row">
                            <div class="col">
                                <div class="card">
                                    <div class="card-header">
                                        <i class="fa fa-plus"></i> {{ trans('admin.applicant.actions.edit') }}
                                    </div>
                                    <div class="card-body">
                                        @include('admin.applicant.components.form-elements')
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-6">
                                @include('admin.applicant.components.form-elements-diseases')
                            </div>
                            <div class="col-sm-6">
                                @include('admin.applicant.components.form-elements-disabilities')
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                @include('admin.applicant.components.form-elements-contact-methods')
                            </div>
                        </div>

                        <div class="card">
                            <div class="card-footer">
                                <button id="btn-guardar" type="submit" class="btn btn-primary" :disabled="submiting">
                                    <i class="fa" :class="submiting ? 'fa-spinner' : 'fa-download'"></i>
                                   {{ trans('brackets/admin-ui::admin.btn.save') }}
                                </button>
                            </div>
                            @if($applicant->id)
                            <div class="card-footer">
                                @if($roles->contains('atc'))
                                    <a class="btn btn-primary btn-sm pull-right m-b-0" href="#confirmar" role="button"><i class="fa fa-arrow-right"></i>&nbsp; Siguiente</a>
                                    
                                @else
                                    <a class="btn btn-primary btn-sm pull-right m-b-0" href="#doc-sociales" role="button"><i class="fa fa-arrow-right"></i>&nbsp; Siguiente</a>
                                    
                                @endif
                            </div>
                            @endif
                        </div>
                        <br>
                    </form>
                 
                    
                </applicant-form>
            </tab>
            @if($roles->contains('coop') || $roles->contains('ama'))
            <tab name="Doc Sociales">
                <applicant-document-listing
                    :data="{{ $socialDocuments->toJson() }}"
                    :url="'{{ url('admin/applicants/documents/'.$applicant->id.'/'.'S') }}'"
                    :finddataurl = "'{{ url('admin/applicants') }}'"
                    inline-template>
                    
                    <div class="row">
                        <div class="col">
                            <div class="card">
                                <div class="card-header">
                                    <i class="fa fa-align-justify"></i> {{ trans('admin.applicant-document.actions.index.social') }}
                                    <a class="btn btn-primary btn-spinner btn-sm pull-right m-b-0" href="{{ url('admin/applicants/documents/create/'.$applicant->id.'/'.'S') }}" role="button"><i class="fa fa-plus"></i>&nbsp; {{ trans('admin.applicant-document.actions.create') }}</a>
                                </div>
                                <div class="card-body" v-cloak>
                                    <div class="card-block">
                                        <form @submit.prevent="">
                                            <div class="row justify-content-md-between">
                                                <div class="col col-lg-7 col-xl-5 form-group">
                                                    <div class="input-group">
                                                        <input class="form-control" placeholder="{{ trans('brackets/admin-ui::admin.placeholder.search') }}" v-model="search" @keyup.enter="filter('search', $event.target.value)" />
                                                        <span class="input-group-append">
                                                <button type="button" class="btn btn-primary" @click="filter('search', search)"><i class="fa fa-search"></i>&nbsp; {{ trans('brackets/admin-ui::admin.btn.search') }}</button>
                                            </span>
                                                    </div>
                                                </div>
                                                <div class="col-sm-auto form-group ">
                                                    <select class="form-control" v-model="pagination.state.per_page">

                                                        <option value="10">10</option>
                                                        <option value="25">25</option>
                                                        <option value="100">100</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </form>
                                       
                                        <table class="table table-hover table-listing">
                                            <thead>
                                            <tr>
                                                <th is='sortable' :column="'applicant_documents.id'">{{ trans('admin.applicant-document.columns.id') }}</th>
                                                <th is='sortable' :column="'document_types.name'">{{ trans('admin.document-type.columns.name') }}</th>
                                                <th is='sortable' :column="'received_at'">{{ trans('admin.applicant-document.columns.received_at') }}</th>
                                                <th></th>
                                            </tr>
                                            <tr v-show="(clickedBulkItemsCount > 0) || isClickedAll">
                                                <td class="bg-bulk-info d-table-cell text-center" colspan="5">
                                                    <span class="align-middle font-weight-light text-dark">{{ trans('brackets/admin-ui::admin.listing.selected_items') }} @{{ clickedBulkItemsCount }}.  <a href="#" class="text-primary" @click="onBulkItemsClickedAll('/admin/applicant-documents')" v-if="(clickedBulkItemsCount < pagination.state.total)"> <i class="fa" :class="bulkCheckingAllLoader ? 'fa-spinner' : ''"></i> {{ trans('brackets/admin-ui::admin.listing.check_all_items') }} @{{ pagination.state.total }}</a> <span class="text-primary">|</span> <a
                                                    href="#" class="text-primary" @click="onBulkItemsClickedAllUncheck()">{{ trans('brackets/admin-ui::admin.listing.uncheck_all_items') }}</a>  </span>
                                                    <span class="pull-right pr-2">
                                                <button 
                                                    class="btn btn-sm btn-danger pr-3 pl-3" 
                                                    @click="bulkDelete('/admin/applicant-documents/bulk-destroy')">
                                                    {{ trans('brackets/admin-ui::admin.btn.delete') }}
                                                </button>
                                            </span>

                                                </td>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr v-for="(item, index) in collection" :key="item.id" :class="bulkItems[item.id] ? 'bg-bulk' : ''">
                                            {{-- lala --}}
                                                <td>@{{ item.id }}</td>
                                                <td>@{{ item.name }}</td>
                                                <td>@{{ new Date(item.received_at) | dateFormat('DD-MM-YYYY')  }}</td>

                                                <td>
                                                    <div class="row no-gutters">
                                                        <template v-if="item.document_url.length>0">
                                                        <div class="col-auto">
                                                      
                                                        </div>
                                                        <div class="col-auto">
                                                            <a class="btn btn-sm btn-info" target="_blank"  title="{{ trans('brackets/admin-ui::admin.btn.document-view') }}" role="button"><i class="fa fa-eye"></i></a>
                                                        </div>
                                                        </template>
                                                        <div class="col-auto">
                                                            <a class="btn btn-sm btn-spinner btn-info" :href="item.resource_url + '/edit'" title="{{ trans('brackets/admin-ui::admin.btn.edit') }}" role="button"><i class="fa fa-edit"></i></a>
                                                        </div>
                                                        <form class="col" @submit.prevent="deleteItem(item.resource_url)">
                                                            <button type="submit" class="btn btn-sm btn-danger" title="{{ trans('brackets/admin-ui::admin.btn.delete') }}"><i class="fa fa-trash-o"></i></button>
                                                        </form>
                                                    </div>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    <!-- Modal show documents -->
                                    <div class="modal fade" id="ListDocuments" tabindex="-1" role="dialog" aria-labelledby="ListDocumentslLabel" aria-hidden="true">
                                        <div class="modal-dialog modal-lg" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel">Seleccionar cooperativa</h5>     
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                                </div>
                                                {{-- listing  --}}
                                                <h3 class="text-center">Listado de Documentos</h3>
                                                @{{item}}
                                                <table>
                                                    <head>
                                                        <tr>
                                                            
                                                            <th>
                                                                Nombre
                                                            </th>
                                                        </tr>
                                                    </head>
                                                    <tbody>
                                                          <tr v-for="list in listdoc">
                                                            <td>
                                                                @{{list.custom_properties}}
                                                            </td>
                                                        </tr> 
                                                    </tbody>

                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    {{-- fin show documents --}}

                                        <div class="row" v-if="pagination.state.total > 0">
                                            <div class="col-sm">
                                                <span class="pagination-caption">{{ trans('brackets/admin-ui::admin.pagination.overview') }}</span>
                                            </div>
                                            <div class="col-sm-auto">
                                                <pagination></pagination>
                                            </div>
                                        </div>

                                        <div class="no-items-found" v-if="!collection.length > 0">
                                            <i class="icon-magnifier"></i>
                                            <h3>{{ trans('brackets/admin-ui::admin.index.no_items') }}</h3>
                                            <p>{{ trans('brackets/admin-ui::admin.index.try_changing_items') }}</p>
                                            <a class="btn btn-primary btn-spinner"  href="{{ url('admin/applicants/documents/create/'.$applicant->id.'/'.'S') }}" role="button"><i class="fa fa-plus"></i>&nbsp; {{ trans('admin.applicant-document.actions.create') }}</a>
                                        
                                        </div>
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <a class="btn btn-primary btn-sm pull-left m-b-0" href="#solicitante" role="button"><i class="fa fa-arrow-left"></i>&nbsp; Anterior</a>
                                    <a class="btn btn-primary btn-sm pull-right m-b-0" href="#grupo-familiar" role="button"><i class="fa fa-arrow-right"></i>&nbsp; Siguiente</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </applicant-document-listing>
            </tab>
            @endif
            @if($roles->contains('coop') || $roles->contains('ama'))
            <tab name="Grupo Familiar">
                <applicant-listing
                    :data="{{ $beneficiaries->toJson() }}"
                    :url="'{{ url('admin/applicants/beneficiary/'.$applicant->id ) }}'"
                    inline-template>

                    <div class="row">
                        <div class="col">
                            <div class="card">
                                <div class="card-header">
                                    <i class="fa fa-align-justify"></i> {{ trans('admin.beneficiary.actions.index') }} 
                                    @if($roles->contains('coop') || $roles->contains('ama'))
                                        <a class="btn btn-primary btn-spinner btn-sm pull-right m-b-0" href="{{ url('admin/applicants/'.$applicant->id.'/create-beneficiary/' ) }}" role="button"><i class="fa fa-plus"></i>&nbsp; {{ trans('admin.beneficiary.actions.create') }}</a>
                                    @endif
                                </div>
                                <div class="card-body" v-cloak>
                                    <div class="card-block">
                                        <form @submit.prevent="">
                                            <div class="row justify-content-md-between">
                                                <div class="col col-lg-7 col-xl-5 form-group">
                                                    <div class="input-group">
                                                        <input class="form-control" placeholder="{{ trans('brackets/admin-ui::admin.placeholder.search') }}" v-model="search" @keyup.enter="filter('search', $event.target.value)" />
                                                        <span class="input-group-append">
                                                <button type="button" class="btn btn-primary" @click="filter('search', search)"><i class="fa fa-search"></i>&nbsp; {{ trans('brackets/admin-ui::admin.btn.search') }}</button>
                                            </span>
                                                    </div>
                                                </div>
                                                <div class="col-sm-auto form-group ">
                                                    <select class="form-control" v-model="pagination.state.per_page">

                                                        <option value="10">10</option>
                                                        <option value="25">25</option>
                                                        <option value="100">100</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </form>

                                        <table class="table table-hover table-listing">
                                            <thead>
                                            <tr>
                                                <th is='sortable' :column="'id'">{{ trans('admin.applicant.columns.id') }}</th>
                                                <th is='sortable' :column="'names'">{{ trans('admin.applicant.columns.names') }}</th>
                                                <th is='sortable' :column="'last_names'">{{ trans('admin.applicant.columns.last_names') }}</th>
                                                <th is='sortable' :column="'government_id'">{{ trans('admin.applicant.columns.government_id') }}</th>
                                                <th style="text-align: left" is='sortable' :column="'applicant_relationships.name'">{{ trans('admin.applicant.columns.applicant_relationship') }}</th>
                                                <th style="text-align: right" is='sortable' :column="'monthly_income'">{{ trans('admin.applicant.columns.monthly_income') }}</th>
                                                <th></th>
                                            </tr>
                                            <tr v-show="(clickedBulkItemsCount > 0) || isClickedAll">
                                                <td class="bg-bulk-info d-table-cell text-center" colspan="20">
                                            <span class="align-middle font-weight-light text-dark">{{ trans('brackets/admin-ui::admin.listing.selected_items') }} @{{ clickedBulkItemsCount }}.  <a href="#" class="text-primary" @click="onBulkItemsClickedAll('/admin/applicants')" v-if="(clickedBulkItemsCount < pagination.state.total)"> <i class="fa" :class="bulkCheckingAllLoader ? 'fa-spinner' : ''"></i> {{ trans('brackets/admin-ui::admin.listing.check_all_items') }} @{{ pagination.state.total }}</a> <span class="text-primary">|</span> <a
                                                    href="#" class="text-primary" @click="onBulkItemsClickedAllUncheck()">{{ trans('brackets/admin-ui::admin.listing.uncheck_all_items') }}</a>  </span>

                                                    <span class="pull-right pr-2">
                                                <button class="btn btn-sm btn-danger pr-3 pl-3" @click="bulkDelete('/admin/applicants/bulk-destroy')">{{ trans('brackets/admin-ui::admin.btn.delete') }}</button>
                                            </span>

                                                </td>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr v-for="(item, index) in collection" :key="item.id" :class="bulkItems[item.id] ? 'bg-bulk' : ''">
                                                <td>@{{ item.id }}</td>
                                                <td>@{{ item.names }}</td>
                                                <td>@{{ item.last_names }}</td>
                                                <td>@{{ item.government_id }}</td>
                                                <td style="text-align: left">@{{ item.relationship ? item.relationship.name : '' }}</td>
                                                <td style="text-align: right">@{{ item.monthly_income ? (item.monthly_income | numberFormat) :'0' }}</td>
                                                <td>
                                                    <div class="row no-gutters">
                                                        <div class="col-auto">
                                                            <a class="btn btn-sm btn-spinner btn-info" :href="'{{ url('admin/applicants') }}/{{$applicant->id}}/edit-beneficiary/'+item.id" title="{{ trans('brackets/admin-ui::admin.btn.edit') }}" role="button"><i class="fa fa-edit"></i></a>
                                                        </div>
                                                        <form class="col" @submit.prevent="deleteItem(item.resource_url)">
                                                            <button type="submit" class="btn btn-sm btn-danger" title="{{ trans('brackets/admin-ui::admin.btn.delete') }}"><i class="fa fa-trash-o"></i></button>
                                                        </form>
                                                    </div>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>

                                        <div class="row" v-if="pagination.state.total > 0">
                                            <div class="col-sm">
                                                <span class="pagination-caption">{{ trans('brackets/admin-ui::admin.pagination.overview') }}</span>
                                            </div>
                                            <div class="col-sm-auto">
                                                <pagination></pagination>
                                            </div>
                                        </div>

                                        <div class="no-items-found" v-if="!collection.length > 0">
                                            <i class="icon-magnifier"></i>
                                            <h3>{{ trans('brackets/admin-ui::admin.index.no_items') }}</h3>
                                            <p>{{ trans('brackets/admin-ui::admin.index.try_changing_items') }}</p>
                                            <a class="btn btn-primary btn-spinner" href="{{ url('admin/applicants/'.$applicant->id.'/create-beneficiary/' ) }}" role="button"><i class="fa fa-plus"></i>&nbsp; {{ trans('admin.beneficiary.actions.create') }}</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <div class="row" v-if="pagination.state.total > 0">
                                        
                                            <div class="form-group col-sm-6">
                                                <a class="btn btn-primary btn-sm pull-left m-b-0" href="#doc-sociales" role="button"><i class="fa fa-arrow-left"></i>&nbsp; Anterior</a>
                                            </div>
                                            <div class="form-group col-sm-6">
                                                <a class="btn btn-primary btn-sm pull-right m-b-0" href="#cuestionario" role="button"><i class="fa fa-arrow-right"></i>&nbsp; Siguiente</a>
                                            </div>
                                       
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </applicant-listing>
            </tab>
            @endif
                    
            @if($roles->contains('coop') || $roles->contains('ama'))
            <tab name="Doc Financieros">
                <applicant-document-listing
                    :data="{{ $finDocuments->toJson() }}"
                    :url="'{{ url('admin/applicants/documents/'.$applicant->id.'/'.'F') }}'"
                    inline-template>

                    <div class="row">
                        <div class="col">
                            <div class="card">
                                <div class="card-header">
                                    <i class="fa fa-align-justify"></i> {{ trans('admin.applicant-document.actions.index.financial') }}
                                    <a class="btn btn-primary btn-spinner btn-sm pull-right m-b-0" href="{{ url('admin/applicants/documents/create/'.$applicant->id.'/'.'F') }}" role="button"><i class="fa fa-plus"></i>&nbsp; {{ trans('admin.applicant-document.actions.create') }}</a>
                                </div>
                                <div class="card-body" v-cloak>
                                    <div class="card-block">
                                        <form @submit.prevent="">
                                            <div class="row justify-content-md-between">
                                                <div class="col col-lg-7 col-xl-5 form-group">
                                                    <div class="input-group">
                                                        <input class="form-control" placeholder="{{ trans('brackets/admin-ui::admin.placeholder.search') }}" v-model="search" @keyup.enter="filter('search', $event.target.value)" />
                                                        <span class="input-group-append">
                                                <button type="button" class="btn btn-primary" @click="filter('search', search)"><i class="fa fa-search"></i>&nbsp; {{ trans('brackets/admin-ui::admin.btn.search') }}</button>
                                            </span>
                                                    </div>
                                                </div>
                                                <div class="col-sm-auto form-group ">
                                                    <select class="form-control" v-model="pagination.state.per_page">

                                                        <option value="10">10</option>
                                                        <option value="25">25</option>
                                                        <option value="100">100</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </form>

                                        <table class="table table-hover table-listing">
                                            <thead>
                                            <tr>

                                                <th is='sortable' :column="'applicant_documents.id'">{{ trans('admin.applicant-document.columns.id') }}</th>
                                                <th is='sortable' :column="'document_types.name'">{{ trans('admin.document-type.columns.name') }}</th>
                                                <th is='sortable' :column="'received_at'">{{ trans('admin.applicant-document.columns.received_at') }}</th>

                                                <th></th>
                                            </tr>
                                            <tr v-show="(clickedBulkItemsCount > 0) || isClickedAll">
                                                <td class="bg-bulk-info d-table-cell text-center" colspan="5">
                                            <span class="align-middle font-weight-light text-dark">{{ trans('brackets/admin-ui::admin.listing.selected_items') }} @{{ clickedBulkItemsCount }}.  <a href="#" class="text-primary" @click="onBulkItemsClickedAll('/admin/applicant-documents')" v-if="(clickedBulkItemsCount < pagination.state.total)"> <i class="fa" :class="bulkCheckingAllLoader ? 'fa-spinner' : ''"></i> {{ trans('brackets/admin-ui::admin.listing.check_all_items') }} @{{ pagination.state.total }}</a> <span class="text-primary">|</span> <a
                                                    href="#" class="text-primary" @click="onBulkItemsClickedAllUncheck()">{{ trans('brackets/admin-ui::admin.listing.uncheck_all_items') }}</a>  </span>

                                                    <span class="pull-right pr-2">
                                                <button class="btn btn-sm btn-danger pr-3 pl-3" @click="bulkDelete('/admin/applicant-documents/bulk-destroy')">{{ trans('brackets/admin-ui::admin.btn.delete') }}</button>
                                            </span>

                                                </td>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr v-for="(item, index) in collection" :key="item.id" :class="bulkItems[item.id] ? 'bg-bulk' : ''">

                                                <td>@{{ item.id }}</td>
                                                <td>@{{ item.name }}</td>
                                                <td>@{{ new Date(item.received_at) | dateFormat('DD-MM-YYYY')  }}</td>

                                                <td>
                                                    <div class="row no-gutters">
                                                        <template v-if="item.document_url.length>0">
                                                            <div class="col-auto">
                                                                <a class="btn btn-sm btn-info" target="_blank" :href="item.document_url" title="{{ trans('brackets/admin-ui::admin.btn.document-view') }}" role="button"><i class="fa fa-eye"></i></a>
                                                            </div>
                                                        </template>
                                                        <div class="col-auto">
                                                            <a class="btn btn-sm btn-spinner btn-info" :href="item.resource_url + '/edit'" title="{{ trans('brackets/admin-ui::admin.btn.edit') }}" role="button"><i class="fa fa-edit"></i></a>
                                                        </div>
                                                        <form class="col" @submit.prevent="deleteItem(item.resource_url)">
                                                            <button type="submit" class="btn btn-sm btn-danger" title="{{ trans('brackets/admin-ui::admin.btn.delete') }}"><i class="fa fa-trash-o"></i></button>
                                                        </form>
                                                    </div>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>

                                        <div class="row" v-if="pagination.state.total > 0">
                                            <div class="col-sm">
                                                <span class="pagination-caption">{{ trans('brackets/admin-ui::admin.pagination.overview') }}</span>
                                            </div>
                                            <div class="col-sm-auto">
                                                <pagination></pagination>
                                            </div>
                                        </div>

                                        <div class="no-items-found" v-if="!collection.length > 0">
                                            <i class="icon-magnifier"></i>
                                            <h3>{{ trans('brackets/admin-ui::admin.index.no_items') }}</h3>
                                            <p>{{ trans('brackets/admin-ui::admin.index.try_changing_items') }}</p>
                                            <a class="btn btn-primary btn-spinner" href="{{ url('admin/applicant-documents/create') }}" role="button"><i class="fa fa-plus"></i>&nbsp; {{ trans('admin.applicant-document.actions.create') }}</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <a class="btn btn-primary btn-sm pull-left m-b-0" href="#cuestionario" role="button"><i class="fa fa-arrow-left"></i>&nbsp; Anterior</a>
                                    @if($roles->contains('coop'))
                                        <a class="btn btn-primary btn-sm pull-right m-b-0" href="#confirmar" role="button"><i class="fa fa-arrow-right"></i>&nbsp; Siguiente</a>
                                    @else
                                        <a class="btn btn-primary btn-sm pull-right m-b-0" href="#doc-tecnicos" role="button"><i class="fa fa-arrow-right"></i>&nbsp; Siguiente</a>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </applicant-document-listing>
            </tab>
            @endif
            @if($roles->contains('atc') || $roles->contains('ama'))
            <tab name="Doc Tecnicos">
                <applicant-document-listing
                    :data="{{ $techDocuments->toJson() }}"
                    :url="'{{ url('admin/applicants/documents/'.$applicant->id.'/'.'T') }}'"
                    inline-template>

                    <div class="row">
                        <div class="col">
                            <div class="card">
                                <div class="card-header">
                                    <i class="fa fa-align-justify"></i> {{ trans('admin.applicant-document.actions.edit') }}
                                    <a class="btn btn-primary btn-spinner btn-sm pull-right m-b-0" 
                                    href="{{ url('admin/applicants/documents/create/'.$applicant->id.'/'.'T') }}" 
                                    role="button">
                                    <i class="fa fa-plus"></i>
                                        &nbsp; {{ trans('admin.applicant-document.actions.create') }}</a>
                                </div>
                                <div class="card-body" >
                                    <div class="card-block">
                                        <form @submit.prevent="">
                                            <div class="row justify-content-md-between">
                                                <div class="col col-lg-7 col-xl-5 form-group">
                                                    <div class="input-group">
                                                        <input class="form-control" placeholder="{{ trans('brackets/admin-ui::admin.placeholder.search') }}" v-model="search" @keyup.enter="filter('search', $event.target.value)" />
                                                        <span class="input-group-append">
                                                <button type="button" class="btn btn-primary" @click="filter('search', search)"><i class="fa fa-search"></i>&nbsp; {{ trans('brackets/admin-ui::admin.btn.search') }}</button>
                                            </span>
                                                    </div>
                                                </div>
                                                <div class="col-sm-auto form-group ">
                                                    <select class="form-control" v-model="pagination.state.per_page">

                                                        <option value="10">10</option>
                                                        <option value="25">25</option>
                                                        <option value="100">100</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </form>

                                        <table class="table table-hover table-listing">
                                            <thead>
                                            <tr>

                                                <th is='sortable' :column="'applicant_documents.id'">{{ trans('admin.applicant-document.columns.id') }}</th>
                                                <th is='sortable' :column="'document_types.name'">{{ trans('admin.document-type.columns.name') }}</th>
                                                <th is='sortable' :column="'received_at'">{{ trans('admin.applicant-document.columns.received_at') }}</th>

                                                <th></th>
                                            </tr>
                                            <tr v-show="(clickedBulkItemsCount > 0) || isClickedAll">
                                                <td class="bg-bulk-info d-table-cell text-center" colspan="5">
                                            <span class="align-middle font-weight-light text-dark">{{ trans('brackets/admin-ui::admin.listing.selected_items') }} @{{ clickedBulkItemsCount }}.  <a href="#" class="text-primary" @click="onBulkItemsClickedAll('/admin/applicant-documents')" v-if="(clickedBulkItemsCount < pagination.state.total)"> <i class="fa" :class="bulkCheckingAllLoader ? 'fa-spinner' : ''"></i> {{ trans('brackets/admin-ui::admin.listing.check_all_items') }} @{{ pagination.state.total }}</a> <span class="text-primary">|</span> <a
                                                    href="#" class="text-primary" @click="onBulkItemsClickedAllUncheck()">{{ trans('brackets/admin-ui::admin.listing.uncheck_all_items') }}</a>  </span>

                                                    <span class="pull-right pr-2">
                                                <button class="btn btn-sm btn-danger pr-3 pl-3" @click="bulkDelete('/admin/applicant-documents/bulk-destroy')">{{ trans('brackets/admin-ui::admin.btn.delete') }}</button>
                                            </span>

                                                </td>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr v-for="(item, index) in collection" :key="item.id" :class="bulkItems[item.id] ? 'bg-bulk' : ''">

                                                <td>@{{ item.id }}</td>
                                                <td>@{{ item.name }}</td>
                                                <td>@{{ new Date(item.received_at) | dateFormat('DD-MM-YYYY')  }}</td>

                                                <td>
                                                    <div class="row no-gutters">
                                                        <template v-if="item.document_url.length>0">
                                                            <div class="col-auto">
                                                                <a class="btn btn-sm btn-info" target="_blank" :href="item.document_url" title="{{ trans('brackets/admin-ui::admin.btn.document-view') }}" role="button"><i class="fa fa-eye"></i></a>
                                                            </div>
                                                        </template>
                                                        <div class="col-auto">
                                                            <a class="btn btn-sm btn-spinner btn-info" :href="item.resource_url + '/edit'" title="{{ trans('brackets/admin-ui::admin.btn.edit') }}" role="button"><i class="fa fa-edit"></i></a>
                                                        </div>
                                                        <form class="col" @submit.prevent="deleteItem(item.resource_url)">
                                                            <button type="submit" class="btn btn-sm btn-danger" title="{{ trans('brackets/admin-ui::admin.btn.delete') }}"><i class="fa fa-trash-o"></i></button>
                                                        </form>
                                                    </div>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>

                                        <div class="row" v-if="pagination.state.total > 0">
                                            <div class="col-sm">
                                                <span class="pagination-caption">{{ trans('brackets/admin-ui::admin.pagination.overview') }}</span>
                                            </div>
                                            <div class="col-sm-auto">
                                                <pagination></pagination>
                                            </div>
                                        </div>

                                        <div class="no-items-found" v-if="!collection.length > 0">
                                            <i class="icon-magnifier"></i>
                                            <h3>{{ trans('brackets/admin-ui::admin.index.no_items') }}</h3>
                                            <p>{{ trans('brackets/admin-ui::admin.index.try_changing_items') }}</p>
                                            <a class="btn btn-primary btn-spinner" href="{{ url('admin/applicant-documents/create') }}" role="button"><i class="fa fa-plus"></i>&nbsp; {{ trans('admin.applicant-document.actions.create') }}</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <a class="btn btn-primary btn-sm pull-left m-b-0" href="#doc-financieros" role="button"><i class="fa fa-arrow-left"></i>&nbsp; Anterior</a>
                                    <a class="btn btn-primary btn-sm pull-right m-b-0" href="#calificacion-final" role="button"><i class="fa fa-arrow-right"></i>&nbsp; Siguiente</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </applicant-document-listing>
            </tab>
            @endif
            @if($roles->contains('ama'))
            <tab name="Calificacion Final">
                    <applicant-question-form

                        :action="'{{ url('admin/applicants/'.$applicant->id.'/applicants-questionnaire/2') }}'"
                        :questions="{{$questions2->toJson()}}"
                        :answers="{{ $answers2 }}"

                        v-cloak
                        inline-template>

                        <form class="form-horizontal form-create" method="post" @submit.prevent="onSubmit" :action="action" novalidate>

                            <div class="row">
                                <div class="col">
                                    <div class="card">
                                        <div class="card-header">
                                            <i class="fa fa-plus"></i> {{ trans('admin.applicant.actions.final') }}
                                        </div>
                                        <div class="card-body">
                                            @include('admin.applicant.components.form-questions')
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                            
                                <button  id="finalButton"  type="submit" class="btn btn-primary" :disabled="submiting">
                                    <i class="fa" :class="submiting ? 'fa-spinner' : 'fa-download'"></i>
                                    {{ trans('brackets/admin-ui::admin.btn.save') }}
                                </button>
                            </div>
                            <div class="card-footer">
                                    <a class="btn btn-primary btn-sm pull-left m-b-0" href="#doc-tecnicos" role="button"><i class="fa fa-arrow-left"></i>&nbsp; Anterior</a>
                                    <a class="btn btn-primary btn-sm pull-right m-b-0" href="#confirmar" role="button"><i class="fa fa-arrow-right"></i>&nbsp; Siguiente</a>
                            </div>
                           

                        </form>

                    </applicant-question-form>
            </tab>
            @endif
  
            @if($roles->contains('ama'))
            <tab name="Confirmar">
                    <confirm-ama-form
                    :action="'{{ url('admin/applicants/'.$applicant->id.'/confirm-ama') }}'"
                    :data = "{{ $applicant->toJson() }}"
                    :atcs="{{$atcs->toJson()}}"

                    :showrelationships = false
                    v-cloak
                    inline-template>

                    <form class="form-horizontal form-create" method="post" @submit.prevent="onSubmit" :action="action" novalidate>

                        <div class="row">
                            <div class="col">
                                <div class="card">
                                    <div class="card-header">
                                        <i class="fa fa-plus"></i> Confirmar
                                    </div>
                                    <div class="card-body">
                                        @include('admin.applicant.components.form-atc')
                                        
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="card">
                            <div class="card-footer">
                                <button id="btn-ama" type="submit" class="btn btn-primary" >
                                    <i class="fa" :class="submiting ? 'fa-spinner' : 'fa-download'"></i>
                                    {{ trans('brackets/admin-ui::admin.btn.save') }}
                                </button>
                            </div>
                        </div>
                     
                        <br>

                    </form>

                </confirm-ama-form>
            </tab>
                @endif
                @if($roles->contains('atc') )
                <tab name="Confirmar">
                        <confirm-atc-form
                            :action="'{{ url('admin/applicants/'.$applicant->id.'/confirm-atc') }}'"
                            v-cloak
                            inline-template>

                            <form class="form-horizontal form-create" method="post" @submit.prevent="onSubmit" :action="action" novalidate>
                                <div class="row">
                                    <div class="col">
                                        <div class="card">
                                            <div class="card-header">
                                                <i class="fa fa-plus"></i> Finalizar
                                            </div>
                                            <div class="card-body">
                                                <p>Se finaliza la evaluación técnica y se deriba a AMA</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-footer">
                                        <button id="btn-atc" type="submit" class="btn btn-primary" :disabled="submiting">
                                            <i class="fa" :class="submiting ? 'fa-spinner' : 'fa-download'"></i>
                                            {{ trans('admin.applicant.actions.finalizar') }}
                                        </button>
                                    </div>
                                </div>
                                <br>
                            </form>
                        </confirm-atc-form>
                    </tab>
                @endif
                @if($roles->contains('coop'))
                <tab name="Confirmar">
                    <confirm-coop-form
                            :action="'{{ url('admin/applicants/'.$applicant->id.'/confirm-coop') }}'"
                            
                            :confirmations="{{$confirmations}}"
                            v-cloak
                            inline-template>
                        
                            <form class="form-horizontal form-create" method="post" @submit.prevent="onSubmit" :action="action" novalidate>
                                <div class="row">
                                    <div class="col">
                                        <div class="card">
                                            <div class="card-header">
                                                <i class="fa fa-plus"></i> Derivar a AMA
                                            </div>
                                            
                                            
                                            <div class="row">
                                            
                                                <div class="form-group col-lg-12">
                                                        <label for="confirmar">Observaciones</label><br/>
                                                        <textarea v-model="form.motive" id="" cols="60" rows="10"></textarea>
                                                    </div>
                                                </div>
                                            <div class="card-body">
                                                <p>Se finaliza la evaluación financiera/social y se deriba a AMA</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-footer">
                                        <button id="btn-coop" type="submit" class="btn btn-primary" :disabled="submiting">
                                            <i class="fa" :class="submiting ? 'fa-spinner' : 'fa-download'"></i>
                                            Derivar
                                        </button>
                                    </div>
                                </div>
                                <div class="card-footer">
                                    @if($roles->contains('coop'))
                                    <a class="btn btn-primary btn-sm pull-left m-b-0" href="#doc-financieros" role="button"><i class="fa fa-arrow-left"></i>&nbsp; Anterior</a>
                                   @endif
                                </div>
                                <br>
                            </form>
                        </confirm-coop-form>
                        </tab>
                @endif
   
                    <tab name="Informaciones">
                        <div class="row">
                    <div class="col-sm-6">
                        <h1 class="text-center">Historial de Estados</h1>
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th>Fecha</th>
                                <th>Estados</th>
                                <th>Usuario</th>
                                <th>Descripción</th>
                            </tr>
                            </thead>
                            <tbody>
                              {{--  {{ dd($statuslist)}} --}}
                                @foreach ($statuslist as $list)
                            <tr>
                                <td>{{date('d/m/Y', strtotime($list->created_at)) }}</td>
                                <td>{{$list->status ?$list->status->name:'' }}</td>
                                <td>{{$list->user ? $list->user->first_name : ($list->admin_user ? $list->admin_user->first_name : 'N/A') }}</td>
                                <td>{{$list->description?$list->description:'No hay observaciones' }}</td>
                    
                            </tr>
                            @endforeach 
                            </tbody>
                        </table>
                    </div>
                        <div class="col-sm-6 border border-info rounded" style="background-color: #F0F1F7" >
                            <h1 class="text-center">Datos del Sistema Central</h1>
{{-- {{dd($vinSoli)}} --}}

 @foreach ($vinSoli as $item)

            
                        @if($item['cod']==0)
                        <h2 class="p-3 mb-2 bg-danger text-white rounded">El solicitante todavia no registra datos en la base de datos Central del MUVH</h2>
                        @else
                        
                                    <p><b> Solicitante: </b>{{$item['name']}}   </p>
                                    <p><b> Conyugue:</b> {{$item['conyugue']}} </p>
                                    <p><b> Nro de Expediente:</b> {{$item['NroExpe']}} </p>
                                    <p><b> Nro de Resolucion:</b> {{$item['nroresolucion']}} </p>
                                    <p><b> Fecha de Resolucion:</b> {{$item['fecresolucion']}} </p>
                                    <p><b> Nombre Postulante del Certificado:</b> {{$item['CerPosNombre']}} </p>
                                    <h3>Grupo Familiar</h3>
                                   
                                    <table class="table">
                                        <thead class="thead-dark">

                                            <tr>
                                                <th scope="col">CI</th>
                                                <th scope="col">Nombres y Apellidos</th>
                                                <th scope="col">Edad</th>
                                                <th scope="col">Discapacidad</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($item['grouplist'] as $flia)
                                            <tr>
                                                <td scope="row">{{$flia['ci']}}</td>
                                                <td>{{$flia['name']}}</td>
                                                <td>{{$flia['edad']}}</td>
                                                <td>{{$flia['discapacidad']}}</td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                        @endif
            @endforeach 

            
                        </div>
                    </div>
                
 
                    
                        
                    

                    </tab>

        </tabs>
    </div>

     
  <!-- Modal 1 -->
{{--   <div class="modal fade" id="exampleModal5" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true"> --}}
{{--   <div class="modal fade" id="exampleModalLg" tabindex="-1" aria-labelledby="exampleModalLgLabel" style="display: none;" aria-hidden="true"> --}}
    <div class="modal fade" id="exampleModalLg2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Seleccionar cooperativa</h5>     
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
  
        <form class="form-horizontal form-create" method="post"  action='../{{$applicant->id}}/assig-coop'>
            <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                <div class="modal-body">                 
                    <div class="form-group col-sm-6">
            
                        <label for="observations" >Observaciones</label>
                        <select class="form-control form-control-sm" name="coop">
                            @foreach($coops as $coop)
                            <option value="{{$coop->id}}">{{$coop->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
        
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Guardar</button>
                </div>
        </form>
  
      </div>
    </div>
  </div>
     
  <!-- Modal 2 -->
  <div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Seleccionar ATC</h5>     
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>

        <form class="form-horizontal form-create" method="post"  action='../{{$applicant->id}}/assig-atc-edit'>
            <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                <div class="modal-body">                 
                    <div class="form-group col-sm-6">
           

                        <label for="atc" >Seleccionar</label>
                        <select class="form-control form-control-sm" name="atc">
                            @foreach($atcs as $atc)
                            <option value="{{$atc->id}}" {{$atc->id==trim($applicant->construction->id) ? 'selected':''}} >{{$atc->name}} </option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group col-sm-6">
                        <label for="fec_atc" >Seleccionar Fecha de Asignacion de ATCS</label>
                        <datetime name="fec_atc" 
                        :config="datePickerConfig2" 
                       
                        class="flatpickr" 
                        value="{{$applicant->atc_fec_asig}}"
                        id="fec_atc" 
                        
                        placeholder="">
                    </datetime> 
                    </div>
                </div>
        
                <div class="modal-footer">
                    
                    <button id="btn-guardar" type="submit" class="btn btn-primary">Guardar</button>
                </div>
        </form>

  
      </div>
    </div>
  </div>


<!-- modal status -->

  <div class="modal fade" id="exampleModalLg" tabindex="-1" aria-labelledby="exampleModalLgLabel" style="display: none;" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLongTitle">Seleccione un estado y agregue un motivo</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                      <span aria-hidden="true">&times;</span>
                                    </button>
                                  </div>
                            <center>
                                  <form class="form-horizontal form-create" method="post" action="{{ url('admin/applicants/'.$applicant->id.'/form-status') }}">
                                    <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                                    <div class="modal-body">
                                            <div class="form-group col-sm-10">
                                                <input type="hidden" name="applicants" value="{{$applicant->id}}">
                                
                                                <div class="form-floating">
                                                
                                                    <select  name="status" id="status" class="form-control form-control-lg"  placeholder="{{ trans('admin.documents.actions.status')  }}">
                                                            <option selected value="">Seleccione estados</option>
                                                        @foreach ($statusName as $sn)
                                                            <option value="{{$sn->id}}">
                                                                @isset($sn->prefix)
                                                                {{$sn->prefix}} -

                                                                @else 
                                                                {{$sn->prefix}} 
                                                                
                                                                @endif
                                                                    {{$sn->name}} 
                                                                </option>
                                                        @endforeach
                                                    </select>
                                                    
                                            </div>
                                            
                                        </div>
                                        <div class="form-group col-sm-10">
                                            <div class="form-floating">
                                                <textarea required class="form-control form-control-success" name="obs" rows="10" placeholder="Escriba un comentario por el cambio de estaod" id="floatingTextarea"></textarea>
                                                
                                            </div>
                                                
                                        </div>
                                        @if($applicant->created_by)
                                        <div class="form-check">
                                            <input class="form-check-input" name="user_notify" type="checkbox" value="true" id="defaultCheck1">
                                            <label class="form-check-label" for="defaultCheck1">
                                              <b>Notificar al Postulante<b>
                                            </label>
                                          </div>
                                        @endif
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                                    {{-- <button type="submit" class="btn btn-primary" >Guardar</button> --}}
                                    <button id="btn-status" type="submit" class="btn btn-primary" :disabled="submiting">
                                        <i class="fa" :class="submiting ? 'fa-spinner' : 'fa-download'"></i>
                                        {{ trans('brackets/admin-ui::admin.btn.save') }}
                                    </button>
                                </div>
                                  </form>
                                  </center>
                          
                            </div>
        </div>
    </div>
<!-- modal status 3-->

  <div class="modal fade" id="exampleModal3" tabindex="-1" aria-labelledby="exampleModalLgLabel" style="display: none;" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLongTitle">Seleccione un Supervisor</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                      <span aria-hidden="true">&times;</span>
                                    </button>
                                  </div>
                            <center>
                                <work-form
                                
                                :action="'{{ url('admin/applicants/addsupervisor/'.$applicant->id) }}'"
                                :supervisors="{{$supervisors->toJson()}}"
                                :applicants="{{$applicant->id}}"
                                v-cloak
                                inline-template>
                                <form class="form-horizontal form-edit" method="post" @submit.prevent="onSubmit" :action="action" novalidate>
                                    <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                                    
                                    @include('admin.work.components.form-elements')
                                <div class="modal-footer">
                                    <button id="btn-status" type="submit" class="btn btn-primary" :disabled="submiting">
                                        <i class="fa" :class="submiting ? 'fa-spinner' : 'fa-download'"></i>
                                        {{ trans('brackets/admin-ui::admin.btn.save') }}
                                    </button>
                                </div>
                            </form>
                        </work-form>
                    </center>
                </div>
        </div>
    </div>
<!-- modal status 4-->

  <div class="modal fade" id="exampleModal4" tabindex="-1" aria-labelledby="exampleModalLgLabel" style="display: none;" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLongTitle">Opciones de obras</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                      <span aria-hidden="true">&times;</span>
                                    </button>
                                  </div>
                            <center>
                                <work-form-modal
                                :action="'{{ url('/admin/applicants/addtype/'.$applicant->id) }}'"
                                :typechecks="{{$typechecks->toJson() }}"
                                :applicant="{{$applicant}}"
                                
                                v-cloak
                                inline-template>
                                <form class="form-horizontal form-create" method="post"  @submit="onSubmit" class="add-form">
                                {{-- <form class="form-horizontal form-create" method="post"  action="/../admin/works/add-type/{{$work->id}}" > --}}
                                    {{-- <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}"> --}}
                    {{--                 @csrf  
                                    @method('PUT')  --}}
                                        <div class="modal-body">                 
                                            <div class="row">
                                                <div class="form-group col-md-6">
                                                <label for="">Tipo de seguimiento</label>
                                                                <multiselect
                                                                v-model="form.typechecks"
                                                                :options="typechecks"
                                                                :multiple="false"
                                                                track-by="id"
                                                                :taggable="true"
                                                                tag-placeholder=""
                                                                :custom-label="typeLabel"
                                                                placeholder=""
                                                                @input="executeLoader">
                                                            </multiselect>
                                                        <div v-if="errors.has('typechecks')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('government_id') }}</div>
                                                    </div>
                                              
                                                </div>
                                                <div class="row ">
                                                    <div class="form-group col-md-6" >
                                                        <label for="">Fecha</label>
                                                    <datetime v-model="form.fecha" 
                                                        :config="datePickerConfig" 
                                                        class="flatpickr" 
                                                        :class="{'form-control-danger': errors.has('fecha_tr'), 'form-control-success': fields.fecha_tr && fields.fecha_tr.valid}" 
                                                        id="fecha" 
                                                        name="fecha" 
                                                        placeholder="{{ trans('brackets/admin-ui::admin.forms.select_a_date') }}">
                                                    </datetime>
                                                    </div>
                                                </div>
                                            </div>
                                            
                                            
                                        <div class="modal-footer">
                                            <input type="hidden" v-model="form.type_check_id" id="type_check_id" name="type_check_id" >
                                            <input type="hidden"  v-model="form.applicant_id"  id="applicant_id"  name="applicant_id" >
                                            
                                            <button id="btn-guardar" type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                            <button  id="btn-guardar" type="submit" class="btn btn-primary">Guardar</button>
                                        </div>
                        
                            </form>
                            </work-form-modal>
                    </center>
                </div>
        </div>
    </div>
            
  <!-- modal status 5-->

  <div class="modal fade" id="exampleModal5" tabindex="-1" aria-labelledby="exampleModalLgLabel" style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLongTitle">Seleccionar Responsable</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                            </button>
                          </div>
                    <center>
                        <applicant-owner-form
            :action="'{{ url('admin/applicant-owners') }}'"
            :users="{{$users->toJson()}}"
                        :applicant="{{$applicant}}"
                        
                        v-cloak
                        inline-template>
                                      
            <form class="form-horizontal form-create" method="post" @submit.prevent="onSubmit" :action="action" novalidate>
                
                <div class="card-header">
                    
                                    </div>
                                    
                <div class="card-body">
                    @include('admin.applicant-owner.components.form-elements')
                </div>
                                    
                <div class="card-footer">
                    <button type="submit" class="btn btn-primary" :disabled="submiting">
                        <i class="fa" :class="submiting ? 'fa-spinner' : 'fa-download'"></i>
                        {{ trans('brackets/admin-ui::admin.btn.save') }}
                    </button>
                                </div>
                
                    </form>

        </applicant-owner-form>
            </center>
        </div>
</div>
</div>

    <notifications position="top center" group="auth"/>
  @endsection
