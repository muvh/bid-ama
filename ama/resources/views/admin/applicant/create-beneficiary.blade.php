@extends('brackets/admin-ui::admin.layout.default')

@section('title', trans('admin.applicant.actions.create'))

@section('body')

    @php($roles = Auth::user()->getRoleNames())

    <div class="container-xl">
        <applicant-form

            :action="'{{ url('admin/applicants/'.$applicant->id.'/create-beneficiary') }}'"

            :educationlevels="{{$educationlevels->toJson()}}"
            :diseases="{{$diseases->toJson()}}"
            :disabilities="{{$disabilities->toJson()}}"
            :contactmethods="{{$contactmethods->toJson()}}"
            :applicantrelationships="{{$applicantrelationships->toJson()}}"

            :showrelationships = true

            :finddataurl = "'{{ url('admin/applicants') }}'"
            v-cloak
            inline-template>

            <form class="form-horizontal form-create" method="post" @submit.prevent="onSubmit" :action="action" novalidate>
                <div class="row">
                    <div class="col">
                        <div class="card">
                            <div class="card-header">
                                <i class="fa fa-plus"></i> {{ trans('admin.beneficiary.actions.create') }} de {{ $applicant->names }} {{ $applicant->last_names }}
                            </div>
                            <div class="card-body">
                                @include('admin.applicant.components.form-elements-beneficiary')
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-6">
                        @include('admin.applicant.components.form-elements-diseases')
                    </div>
                    <div class="col-sm-6">
                        @include('admin.applicant.components.form-elements-disabilities')
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        @include('admin.applicant.components.form-elements-contact-methods')
                    </div>
                </div>

                <div class="card">
                    <div class="card-footer">
                        <button id="btn-guardar" type="submit" class="btn btn-primary" :disabled="submiting">
                            <i class="fa" :class="submiting ? 'fa-spinner' : 'fa-download'"></i>
                            {{ trans('brackets/admin-ui::admin.btn.save') }}
                        </button>
                    </div>
                </div>
                <br>

            </form>

        </applicant-form>
    </div>

@endsection

