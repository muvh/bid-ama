@extends('brackets/admin-ui::admin.layout.default')

@section('title', trans('admin.applicant.actions.create'))

@section('body')

    @php($roles = Auth::user()->getRoleNames())

    <div class="container-xl">
        <tabs>
            <tab name="Solicitantes">
                <applicant-form
                    :action="'{{ url('admin/applicants') }}'"

                    :cities="{{$cities->toJson()}}"
                    :educationlevels="{{$educationlevels->toJson()}}"
                    :diseases="{{$diseases->toJson()}}"
                    :disabilities="{{$disabilities->toJson()}}"
                    :contactmethods="{{$contactmethods->toJson()}}"

                    :showrelationships = false
                    :finddataurl = "'{{ url('admin/applicants') }}'"
                    v-cloak
                    inline-template>

                    <form class="form-horizontal form-create" method="post" @submit.prevent="onSubmit" :action="action" novalidate>
                        <div class="row">
                            <div class="col">
                                <div class="card">
                                    <div class="card-header">
                                        <i class="fa fa-plus"></i> {{ trans('admin.applicant.actions.create') }}
                                    </div>
                                    <div class="card-body">
                                        @include('admin.applicant.components.form-elements')
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-6">
                                @include('admin.applicant.components.form-elements-diseases')
                            </div>
                            <div class="col-sm-6">
                                @include('admin.applicant.components.form-elements-disabilities')
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                @include('admin.applicant.components.form-elements-contact-methods')
                            </div>
                        </div>

                        <div class="card">
                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary" :disabled="submiting">
                                    <i class="fa" :class="submiting ? 'fa-spinner' : 'fa-download'"></i>
                                    {{ trans('brackets/admin-ui::admin.btn.save') }}
                                </button>
                            </div>
                        </div>
                        <br>

                    </form>

                </applicant-form>
            </tab>
            @if($roles->contains('coop') || $roles->contains('ama'))
            <tab name="Grupo Familiar" :is-disabled="true"></tab>
            @endif

            @if($roles->contains('coop') || $roles->contains('ama'))
            <tab name="Cuestionario" :is-disabled="true"></tab>
            @endif

            @if($roles->contains('coop') || $roles->contains('ama'))
            <tab name="Doc Sociales" :is-disabled="true"></tab>
            @endif

            @if($roles->contains('coop') || $roles->contains('ama'))
            <tab name="Doc Financieros" :is-disabled="true"></tab>
            @endif

            @if($roles->contains('atc') || $roles->contains('ama'))
            <tab name="Doc Técnicos" :is-disabled="true"></tab>
            @endif

            @if($roles->contains('ama'))
                <tab name="Evaluación Final" :is-disabled="true"></tab>
            @endif

            <tab name="Confirmar" :is-disabled="true"></tab>

        </tabs>
    </div>

@endsection

