@extends('brackets/admin-ui::admin.layout.default')

@section('title', trans('admin.works-datum.actions.index'))

@section('body')

    <div class="card">

        <div class="card-header text-center">
            MAPA DE OBRAS
        </div>
        <div class="card-body">
            <mapavivienda :latitude="-25.2949068" :longitude="-57.6087548" />
        </div>

    </div>

@endsection
