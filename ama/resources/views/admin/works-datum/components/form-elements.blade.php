{{-- <div class="form-group row align-items-center" :class="{'has-danger': errors.has('applicant_id'), 'has-success': fields.applicant_id && fields.applicant_id.valid }">
    <label for="applicant_id" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.works-datum.columns.applicant_id') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.applicant_id" v-validate="'required'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('applicant_id'), 'form-control-success': fields.applicant_id && fields.applicant_id.valid}" id="applicant_id" name="applicant_id" placeholder="{{ trans('admin.works-datum.columns.applicant_id') }}">
        <div v-if="errors.has('applicant_id')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('applicant_id') }}</div>
    </div>
</div> --}}

<div class="form-group row align-items-center"
    :class="{ 'has-danger': errors.has('start_work'), 'has-success': fields.start_work && fields.start_work.valid }">
    <label for="start_work" class="col-form-label text-md-right"
        :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.works-datum.columns.start_work') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <div class="input-group input-group--custom">
            <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
            <datetime v-model="form.start_work" :config="datetimePickerConfig"
                v-validate="'required|date_format:yyyy-MM-dd HH:mm:ss'" class="flatpickr"
                :class="{ 'form-control-danger': errors.has('start_work'), 'form-control-success': fields.start_work && fields
                        .start_work.valid }"
                id="start_work" name="start_work"
                placeholder="{{ trans('brackets/admin-ui::admin.forms.select_date_and_time') }}"></datetime>
        </div>
        <div v-if="errors.has('start_work')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('start_work') }}
        </div>
    </div>
</div>

<div class="form-group row align-items-center"
    :class="{ 'has-danger': errors.has('latitude'), 'has-success': fields.latitude && fields.latitude.valid }">
    <label for="latitude" class="col-form-label text-md-right"
        :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.works-datum.columns.latitude') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.latitude" v-validate="''" @input="validate($event)" class="form-control"
            :class="{ 'form-control-danger': errors.has('latitude'), 'form-control-success': fields.latitude && fields.latitude
                    .valid }"
            id="latitude" name="latitude" placeholder="{{ trans('admin.works-datum.columns.latitude') }}">
        <div v-if="errors.has('latitude')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('latitude') }}</div>
    </div>
</div>

<div class="form-group row align-items-center"
    :class="{ 'has-danger': errors.has('longitude'), 'has-success': fields.longitude && fields.longitude.valid }">
    <label for="longitude" class="col-form-label text-md-right"
        :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.works-datum.columns.longitude') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.longitude" v-validate="''" @input="validate($event)" class="form-control"
            :class="{ 'form-control-danger': errors.has('longitude'), 'form-control-success': fields.longitude && fields
                    .longitude.valid }"
            id="longitude" name="longitude" placeholder="{{ trans('admin.works-datum.columns.longitude') }}">
        <div v-if="errors.has('longitude')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('longitude') }}</div>
    </div>
</div>
