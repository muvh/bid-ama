@extends('brackets/admin-ui::admin.layout.default')

@section('title', trans('admin.financial-summary.actions.edit', ['name' => $financialSummary->id]))

@section('body')

    <div class="container-xl">
        <div class="card">

            <financial-summary-form
                :action="'{{ $financialSummary->resource_url }}'"
                :data="{{ $financialSummary->toJson() }}"
                v-cloak
                inline-template>
            
                <form class="form-horizontal form-edit" method="post" @submit.prevent="onSubmit" :action="action" novalidate>


                    <div class="card-header">
                        <i class="fa fa-pencil"></i> {{ trans('admin.financial-summary.actions.edit', ['name' => $financialSummary->id]) }}
                    </div>

                    <div class="card-body">
                        @include('admin.financial-summary.components.form-elements')
                    </div>
                    
                    
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary" :disabled="submiting">
                            <i class="fa" :class="submiting ? 'fa-spinner' : 'fa-download'"></i>
                            {{ trans('brackets/admin-ui::admin.btn.save') }}
                        </button>
                    </div>
                    
                </form>

        </financial-summary-form>

        </div>
    
</div>

@endsection