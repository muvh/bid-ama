<div class="form-group row align-items-center" :class="{'has-danger': errors.has('applicant_id'), 'has-success': fields.applicant_id && fields.applicant_id.valid }">
    <label for="applicant_id" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.financial-summary.columns.applicant_id') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.applicant_id" v-validate="'required'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('applicant_id'), 'form-control-success': fields.applicant_id && fields.applicant_id.valid}" id="applicant_id" name="applicant_id" placeholder="{{ trans('admin.financial-summary.columns.applicant_id') }}">
        <div v-if="errors.has('applicant_id')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('applicant_id') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('desembolso'), 'has-success': fields.desembolso && fields.desembolso.valid }">
    <label for="desembolso" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.financial-summary.columns.desembolso') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.desembolso" v-validate="'required'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('desembolso'), 'form-control-success': fields.desembolso && fields.desembolso.valid}" id="desembolso" name="desembolso" placeholder="{{ trans('admin.financial-summary.columns.desembolso') }}">
        <div v-if="errors.has('desembolso')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('desembolso') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('date'), 'has-success': fields.date && fields.date.valid }">
    <label for="date" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.financial-summary.columns.date') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-sm-8'">
        <div class="input-group input-group--custom">
            <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
            <datetime v-model="form.date" :config="datePickerConfig" v-validate="'required|date_format:yyyy-MM-dd HH:mm:ss'" class="flatpickr" :class="{'form-control-danger': errors.has('date'), 'form-control-success': fields.date && fields.date.valid}" id="date" name="date" placeholder="{{ trans('brackets/admin-ui::admin.forms.select_a_date') }}"></datetime>
        </div>
        <div v-if="errors.has('date')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('date') }}</div>
    </div>
</div>


