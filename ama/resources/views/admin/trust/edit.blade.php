@extends('brackets/admin-ui::admin.layout.default')

@section('title', trans('admin.trust.actions.edit', ['name' => $trust->id]))

@section('body')

    <div class="container-xl">
        <div class="card">

            <trust-form
                :action="'{{ $trust->resource_url }}'"
                :data="{{ $trust->toJson() }}"
                :trust="{{$trust}}"
                v-cloak
                inline-template>
            
                <form class="form-horizontal form-edit" method="post" @submit.prevent="onSubmit" :action="action" novalidate>


                    <div class="card-header">
                        <i class="fa fa-pencil"></i> Solicitud de Transferencia Aporte 100%
                    </div>

                    <div class="card-body">
                        @include('admin.trust.components.form-elements-edit')
                    </div>
                    
                    @if(Request::path()=='admin/applicants/create')
                        <div class="card-footer">
                            <button type="submit" class="btn btn-primary" :disabled="submiting">
                                <i class="fa" :class="submiting ? 'fa-spinner' : 'fa-download'"></i>
                                {{ trans('brackets/admin-ui::admin.btn.save') }}
                            </button>
                        </div>
                    @else
                    @if($trust->culminado==false)
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
                            <i class="fa fa-plus"></i>&nbsp; Otorgar Subsidio
                        </button>

                    @endif
                    @endif
                </form>
               
        </trust-form>

    <table class="table table-hover table-listing">
        <thead>
                <tr>
          <th>ID</th>
          <th>ID Solicitante</th>
          <th>Nombre</th>
          <th>Monto</th>
          <th>Acción</th>
        </tr>
        </thead>
        <tbody>
            @foreach ($ApplicantsTrust as $list)
        <tr>
          <td>{{$list->id }}</td>
          <td>{{$list->applicant_id }}</td>
          <td>{{$list->applicant->names }} {{$list->applicant->last_names}}</td>
          <td>34.000.000</td>
          <td>
            @if($trust->culminado==false)
            <form class="col" action="/admin/applicants-trusts/{{$list->id}}" method="post">
                <input type="hidden" name="_method" value="delete" />
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <button type="submit" class="btn btn-sm btn-danger" title="{{ trans('brackets/admin-ui::admin.btn.delete') }}"><i class="fa fa-trash-o"></i></button>
            </form>
            @endif
          </td>
        </tr>
        @endforeach 
        </tbody>
      </table>
    @if($trust->culminado==false)
    <div class="text-right">
        <a class="btn btn btn-success" href='{{$trust->resource_url}}/showfideicomiso' title="" role="button"><i class="fa fa-arrow-circle-o-right"></i>
            Generar Doc
        </a>
    </div>
    @endif
        </div>
    
</div>

<!-- Modal 1 -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Seleccionar cooperativa</h5>     
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <trust-form-modal
        :action="'{{ url('admin/trusts/add-applicant') }}'"
        :data="{{ $trust->toJson() }}"
        :trust="{{$trust}}"
        v-cloak
        inline-template>
        <form class="form-horizontal form-create" method="post"  action="/../admin/trusts/add-applicant/{{$trust->id}}" >
            <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                <div class="modal-body">                 
                    <div class="row">
                        <div class="form-group">
                        <label for="">Nro de Cedula</label>
                                <div class="col-md-9 col-xl-10">
                                    <div class="input-group-append">
                                        <input type="text" required v-model="form.government_id" v-validate="'required'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('government_id'), 'form-control-success': fields.government_id && fields.government_id.valid}" id="government_id" name="government_id" placeholder="{{ trans('admin.applicant.columns.government_id') }}">
                                        <div class="input-group-append">
                                            <button @click="findApplicant" class="btn btn-info" type="button"><i class="fa fa-search"></i></button>
                                        </div>
                                        
                                        <div v-if="errors.has('government_id')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('government_id') }}</div>
                                    </div>
                                    <div v-if="nombre">
                                        <b>   Nombre: @{{nombre}} </b>
                                        <b>Cedula: @{{cedula}} </b>

                                    </div>
                                </div>
                                
                            </div>
                          
                            
                        </div>
                    </div>
                    <input type="hidden" v-model="trust_id" id="trust_id" name="trust_id" >
                    <input type="hidden" v-model="applicant_id" id="applicant_id" name="applicant_id" >
                <div class="modal-footer">
                    <button id="btn-guardar" type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <div v-if="listTusts===0">
                        <button  id="btn-guardar" type="submit" class="btn btn-primary">Guardar</button>
                    </div>
                </div>
               
        </form>
        </trust-form-modal>
        <notifications position="top center" group="auth"/>
      </div>
    </div>
  </div>

@endsection