<div class="form-group row align-items-center" :class="{'has-danger': errors.has('nro_tr'), 'has-success': fields.nro_tr && fields.nro_tr.valid }">
    <label for="nro_tr" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.trust.columns.nro_tr') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.nro_tr" v-validate="'required'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('nro_tr'), 'form-control-success': fields.nro_tr && fields.nro_tr.valid}" id="nro_tr" name="nro_tr" placeholder="{{ trans('admin.trust.columns.nro_tr') }}">
        <div v-if="errors.has('nro_tr')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('nro_tr') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('fecha_tr'), 'has-success': fields.fecha_tr && fields.fecha_tr.valid }">
    <label for="fecha_tr" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.trust.columns.fecha_tr') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-sm-8'">
        <div class="input-group input-group--custom">
            <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
            <datetime v-model="form.fecha_tr" :config="datePickerConfig" v-validate="'required'" class="flatpickr" :class="{'form-control-danger': errors.has('fecha_tr'), 'form-control-success': fields.fecha_tr && fields.fecha_tr.valid}" id="fecha_tr" name="fecha_tr" placeholder="{{ trans('brackets/admin-ui::admin.forms.select_a_date') }}"></datetime>
        </div>
        <div v-if="errors.has('fecha_tr')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('fecha_tr') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('financial_entity_id'), 'has-success': fields.financial_entity_id && fields.financial_entity_id.valid }">
    <label for="financial_entity_id" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">Cooperativa</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        {{-- <input type="text" v-model="form.financial_entity_id" v-validate="'required'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('financial_entity_id'), 'form-control-success': fields.financial_entity_id && fields.financial_entity_id.valid}" id="financial_entity_id" name="financial_entity_id" placeholder="{{ trans('admin.trust.columns.financial_entity_id') }}"> --}}
        <multiselect
        v-model="form.financial_entity"
        :options="{{$financial->toJson()}}"
        :multiple="false"
        track-by="id"
        label="name"
        :taggable="true"
        tag-placeholder=""
        placeholder=""
       >
    </multiselect>
        <div v-if="errors.has('financial_entity_id')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('financial_entity_id') }}</div>
    </div>
</div>


