<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
  <header>
    <img src="{{ public_path('logo-cab-transf-recursos.jpg') }}" >
    
  </header>
{{--   <footer>
    <img src="{{ public_path('logo-pie-transf-recursos.png') }}" width=" 750px" >
</footer> --}}
<main>
  
    <style type="text/css">
        .tg  {border-collapse:collapse;border-spacing:0;width:70%;margin: -10px 120px}
        .tg td{border-color:black;border-style:solid;border-width:1px;font-family:Calibri;font-size:10px;
          overflow:hidden;padding:10px 5px;word-break:normal;}
        .tg th{border-color:black;border-style:solid;border-width:1px;font-family:Calibri;font-size:10px;
          font-weight:normal;overflow:hidden;padding:10px 5px;word-break:normal;}
        .tg .tg-0pky{border-color:inherit;text-align:left;vertical-align:top}
        .tg .tg-za14{border-color:inherit;text-align:center;vertical-align:bottom}
        table.no-spacing { border-spacing:0; 
          /* Removes the cell spacing via css */ 
          border-collapse: collapse; 
          /* Optional - if you don't want to have double border where cells touch */ 
         }

        header {
                position: fixed;
                top: -30px;
                left: 90px;
                right: 0px;
                height: 50px;
                font-size: 20px !important;

    
            }
         
/*             footer {
                position: fixed; 
                bottom: 40px; 
                left: 0px; 
                right: 0px;
                height: 50px; 
                font-size: 20px !important;
            } */
            table.no-spacing td {
                border:none !important;
                padding: 0;
            }
        </style>
        <br>
        <br>
        <h5 style="text-align: center">FORMULARIO DE SOLICITUD DE TRANSFERENCIA
        <br> 100% APORTE ESTATAL</h5>
        <table class="tg">
        <thead>
          <tr>
            <th  style="text-align: center" class="tg-0pky" colspan="4">Instrucción de Transferencia de Recursos - Fideicomiso Proyecto AMA</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td class="tg-0pky">Entidad:</td>
            <td class="tg-0pky" colspan="3">Ministerio de Urbanismo, Vivienda y Hábitat</td>
          </tr>
          <tr>
            <td class="tg-0pky">Nro. de Solicitud</td>
            <td class="tg-0pky">{{$trust->nro_tr}}</td>
            <td class="tg-0pky">Fecha de Emisión</td>
            <td class="tg-0pky"> &nbsp;</td>
           
          </tr>
          <tr>
            <td class="tg-0pky"  style="text-align: center" colspan="4"> <b> Datos para la Transferencia</b></td>
          </tr>
          <tr>
            <td class="tg-0pky">Beneficiario:</td>
            <td class="tg-0pky" colspan="3">{{$trust->financial->razon_social}} </td>
          </tr>
          <tr>
            <td class="tg-0pky">RUC:</td>
            <td class="tg-0pky" colspan="3">{{$trust->financial->const_ruc}}</td>
          </tr>
          <tr>
            <td class="tg-0pky">Dirección:</td>
            <td class="tg-0pky" colspan="3">{{$trust->financial->address}}</td>
          </tr>
          <tr>
            <td class="tg-0pky">Nro. de Cta. Bancaria:</td>
            <td class="tg-0pky" colspan="3">{{$trust->financial->cta_cte}}</td>
          </tr>
          <tr>
            <td class="tg-0pky">Banco:</td>
            <td class="tg-0pky" colspan="3">{{$trust->financial->bank_name}}</td>
          </tr>
          <tr>
            <td class="tg-0pky"  style="text-align: center" colspan="3"><b>Concepto de la Transferencia</b></td>
            <td class="tg-0pky" style="text-align: center"><b>Importe Guaranies</b></td>
          </tr>
          <tr>
            <td class="tg-0pky" colspan="3">
             Subsidio otorgado por el MUVH a favor de:
              <table class="no-spacing" style="border-collapse: collapse; border-spacing:0; padding: 0;margin:0" cellspacing="0" cellpadding="0">
       
                <tbody>
                  @php
                      $contador=0;
                  @endphp
                  @foreach ($applicant as $item) 
                  @php
                      $contador=$contador+1;
                  @endphp
                  <tr>           
              <td >{{$item->applicant->names}} &nbsp; {{$item->applicant->last_names}}</td>
              <td >&nbsp;CI N° {{$item->applicant->government_id}}</td>
              <td > 
              
            
               @if($contador==$applicant->count())
               &nbsp;<u> Gs. 34.000.000 </u>
             
               @else
               &nbsp; Gs. 34.000.000
               @endif
                
              </td>
            </tr>
              @endforeach
          

          </tbody>
        </table>

             
            </td>
            <td class="tg-za14"  >
              
             Gs. {{number_format($mount,0, '.', '.')}}
              
            </td>
          </tr>
          <tr>
            <td class="tg-0pky" colspan="4">IMPORTE EN LETRAS: GUARANÍES {{$numeroTexto}}</td>
          </tr>
        </tbody>
        </table>

        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
    
    
    
        <style type="text/css">
          .tg2  {border-collapse:collapse;border-spacing:0;width:70%;margin: -10px 120px}
          .tg2 td{border-color:black;border-style:solid;border-width:1px;font-family:Arial, sans-serif;font-size:12px;
            overflow:hidden;padding:10px 5px;word-break:normal;}
          .tg2 th{border-color:black;border-style:solid;border-width:1px;font-family:Arial, sans-serif;font-size:12px;
            font-weight:normal;overflow:hidden;padding:10px 5px;word-break:normal;}
          .tg2 .tg2-nx8p{font-size:12px;text-align:left;vertical-align:top}
          .tg2 .tg2-13pz{font-size:12px;text-align:center;vertical-align:top}
          </style>
          <div style="margin: 0 auto;">
          <table class="tg2">
          <thead>
            <tr>
              <th class="tg2-nx8p"><br><br><br></th>
              <th class="tg2-nx8p"><br><br><br></th>
              <th class="tg2-nx8p"><br><br><br></th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td class="tg2-13pz">&nbsp;<br>Dirección General de la <br>UEP - BID</td>
              <td class="tg2-13pz">&nbsp;<br>Dirección General de <br>Administración y Finanzas</td>
              <td class="tg2-13pz">&nbsp;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Ministro &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
            </tr>
          </tbody>
          </table>
        </main>
</body>
</html>