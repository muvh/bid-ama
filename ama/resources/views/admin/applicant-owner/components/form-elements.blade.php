

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('admin_user_id'), 'has-success': fields.admin_user_id && fields.admin_user_id.valid }">
    <label for="admin_user_id" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.applicant-owner.columns.admin_user_id') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
            <multiselect
            v-model="form.users"
            :options="users"
            :multiple="false"
            track-by="id"
            :taggable="true"
            tag-placeholder=""
            :custom-label="customLabel"
            @input="executeLoader"
            placeholder="">
        </multiselect>
        </div>
        <input type="hidden" v-model="form.applicant_id" v-validate="'required'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('applicant_id'), 'form-control-success': fields.applicant_id && fields.applicant_id.valid}" id="applicant_id" name="applicant_id" placeholder="{{ trans('admin.applicant-owner.columns.applicant_id') }}">
        <input type="hidden"  class="form-control" type="text" v-model="form.admin_user_id"  name="admin_user_id" id="admin_user_id">
</div>


