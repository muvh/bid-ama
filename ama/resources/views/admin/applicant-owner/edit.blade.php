@extends('brackets/admin-ui::admin.layout.default')

@section('title', trans('admin.applicant-owner.actions.edit', ['name' => $applicantOwner->id]))

@section('body')

    <div class="container-xl">
        <div class="card">

            <applicant-owner-form
                :action="'{{ $applicantOwner->resource_url }}'"
                :data="{{ $applicantOwner->toJson() }}"
                v-cloak
                inline-template>
            
                <form class="form-horizontal form-edit" method="post" @submit.prevent="onSubmit" :action="action" novalidate>


                    <div class="card-header">
                        <i class="fa fa-pencil"></i> {{ trans('admin.applicant-owner.actions.edit', ['name' => $applicantOwner->id]) }}
                    </div>

                    <div class="card-body">
                        @include('admin.applicant-owner.components.form-elements')
                    </div>
                    
                    
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary" :disabled="submiting">
                            <i class="fa" :class="submiting ? 'fa-spinner' : 'fa-download'"></i>
                            {{ trans('brackets/admin-ui::admin.btn.save') }}
                        </button>
                    </div>
                    
                </form>

        </applicant-owner-form>

        </div>
    
</div>

@endsection