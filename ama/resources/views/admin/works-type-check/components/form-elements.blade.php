<div class="form-group row align-items-center" :class="{'has-danger': errors.has('work_id'), 'has-success': fields.work_id && fields.work_id.valid }">
    <label for="work_id" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.works-type-check.columns.work_id') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.work_id" v-validate="'required'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('work_id'), 'form-control-success': fields.work_id && fields.work_id.valid}" id="work_id" name="work_id" placeholder="{{ trans('admin.works-type-check.columns.work_id') }}">
        <div v-if="errors.has('work_id')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('work_id') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('type_check_id'), 'has-success': fields.type_check_id && fields.type_check_id.valid }">
    <label for="type_check_id" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.works-type-check.columns.type_check_id') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.type_check_id" v-validate="'required'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('type_check_id'), 'form-control-success': fields.type_check_id && fields.type_check_id.valid}" id="type_check_id" name="type_check_id" placeholder="{{ trans('admin.works-type-check.columns.type_check_id') }}">
        <div v-if="errors.has('type_check_id')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('type_check_id') }}</div>
    </div>
</div>


