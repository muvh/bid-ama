<div class="form-group row align-items-center" :class="{'has-danger': errors.has('name'), 'has-success': fields.name && fields.name.valid }">
    <label for="name" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.financial-entity.columns.name') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.name" v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('name'), 'form-control-success': fields.name && fields.name.valid}" id="name" name="name" placeholder="{{ trans('admin.financial-entity.columns.name') }}">
        <div v-if="errors.has('name')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('name') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('razon_social'), 'has-success': fields.razon_social && fields.razon_social.valid }">
    <label for="razon_social" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.financial-entity.columns.razon_social') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.razon_social" v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('razon_social'), 'form-control-success': fields.razon_social && fields.razon_social.valid}" id="razon_social" name="razon_social" placeholder="{{ trans('admin.financial-entity.columns.razon_social') }}">
        <div v-if="errors.has('razon_social')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('razon_social') }}</div>
    </div>
</div>
<div class="form-group row align-items-center" :class="{'has-danger': errors.has('bank_name'), 'has-success': fields.bank_name && fields.bank_name.valid }">
    <label for="bank_name" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.financial-entity.columns.bank_name') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.bank_name" v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('bank_name'), 'form-control-success': fields.bank_name && fields.bank_name.valid}" id="bank_name" name="bank_name" placeholder="{{ trans('admin.financial-entity.columns.bank_name') }}">
        <div v-if="errors.has('bank_name')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('bank_name') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('address'), 'has-success': fields.address && fields.address.valid }">
    <label for="address" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.financial-entity.columns.address') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.address" v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('address'), 'form-control-success': fields.address && fields.address.valid}" id="address" name="address" placeholder="{{ trans('admin.financial-entity.columns.address') }}">
        <div v-if="errors.has('address')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('address') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('cta_cte'), 'has-success': fields.cta_cte && fields.cta_cte.valid }">
    <label for="cta_cte" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.financial-entity.columns.cta_cte') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.cta_cte" v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('cta_cte'), 'form-control-success': fields.cta_cte && fields.cta_cte.valid}" id="cta_cte" name="cta_cte" placeholder="{{ trans('admin.financial-entity.columns.cta_cte') }}">
        <div v-if="errors.has('cta_cte')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('cta_cte') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('const_ruc'), 'has-success': fields.const_ruc && fields.const_ruc.valid }">
    <label for="const_ruc" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.financial-entity.columns.const_ruc') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.const_ruc" v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('const_ruc'), 'form-control-success': fields.const_ruc && fields.const_ruc.valid}" id="const_ruc" name="const_ruc" placeholder="{{ trans('admin.financial-entity.columns.const_ruc') }}">
        <div v-if="errors.has('const_ruc')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('const_ruc') }}</div>
    </div>
</div>


