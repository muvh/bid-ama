@extends('brackets/admin-ui::admin.layout.default')

@section('title', trans('admin.financial-entity.actions.edit', ['name' => $financialEntity->name]))

@section('body')

    <div class="container-xl">
        <div class="card">

            <financial-entity-form
                :action="'{{ $financialEntity->resource_url }}'"
                :data="{{ $financialEntity->toJson() }}"
                v-cloak
                inline-template>
            
                <form class="form-horizontal form-edit" method="post" @submit.prevent="onSubmit" :action="action" novalidate>


                    <div class="card-header">
                        <i class="fa fa-pencil"></i> {{ trans('admin.financial-entity.actions.edit', ['name' => $financialEntity->name]) }}
                    </div>

                    <div class="card-body">
                        @include('admin.financial-entity.components.form-elements')
                    </div>
                    
                    
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary" :disabled="submiting">
                            <i class="fa" :class="submiting ? 'fa-spinner' : 'fa-download'"></i>
                            {{ trans('brackets/admin-ui::admin.btn.save') }}
                        </button>
                    </div>
                    
                </form>

        </financial-entity-form>

        </div>
    
</div>

@endsection