<div class="form-group row align-items-center" :class="{'has-danger': errors.has('applicant_id'), 'has-success': fields.applicant_id && fields.applicant_id.valid }">
    <label for="applicant_id" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.applicants-trust.columns.applicant_id') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.applicant_id" v-validate="'required'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('applicant_id'), 'form-control-success': fields.applicant_id && fields.applicant_id.valid}" id="applicant_id" name="applicant_id" placeholder="{{ trans('admin.applicants-trust.columns.applicant_id') }}">
        <div v-if="errors.has('applicant_id')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('applicant_id') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('trust_id'), 'has-success': fields.trust_id && fields.trust_id.valid }">
    <label for="trust_id" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.applicants-trust.columns.trust_id') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.trust_id" v-validate="'required'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('trust_id'), 'form-control-success': fields.trust_id && fields.trust_id.valid}" id="trust_id" name="trust_id" placeholder="{{ trans('admin.applicants-trust.columns.trust_id') }}">
        <div v-if="errors.has('trust_id')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('trust_id') }}</div>
    </div>
</div>


