@extends('brackets/admin-ui::admin.layout.default')

@section('title', trans('admin.work.actions.edit', ['name' => $work->id]))

@section('body')

    <div class="container-xl">
        <div class="card">

            <work-form
                :action="'{{ $work->resource_url }}'"
                :data="{{ $work->toJson() }}"
                v-cloak
                inline-template>
            
                <form class="form-horizontal form-edit" method="post" @submit.prevent="onSubmit" :action="action" novalidate>


                    <div class="card-header">
                        <i class="fa fa-pencil"></i> {{ trans('admin.work.actions.edit', ['name' => $work->id]) }}
                    </div>

                    <div class="card-body">
                        {{-- @include('admin.work.components.form-elements-items') --}}
                    </div>
                    
                    
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary" :disabled="submiting">
                            <i class="fa" :class="submiting ? 'fa-spinner' : 'fa-download'"></i>
                            {{ trans('brackets/admin-ui::admin.btn.save') }}
                        </button>
                    </div>
                    <table class="table table-hover table-listing">
                        <thead>
                            <tr>
                            <th>ID</th>
                            <th>Checks</th>
                            <th>Fecha</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($worksTypeCheck as $items)
                        <tr>
                          <td>{{$items->typecheck->id }}</td>
                          <td>{{$items->typecheck->name}}</td>
                          <td>{{$items->fecha_check}}</td>
                         
                        
                        </tr>
                        @endforeach 
                        </tbody>
                      </table>
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
                        <i class="fa fa-plus"></i>&nbsp; Agregar Checks
                    </button>
                </form>
        </work-form>

        </div>
    
</div>
<!-- Modal 1 -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Seleccionar </h5>     
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <work-form-modal
        :action="'{{ $work->resource_url }}'"
        :data="{{ $work->toJson() }}"
        :typechecks="{{$typechecks->toJson() }}"
        v-cloak
        inline-template>
        <form class="form-horizontal form-create" @submit="onSubmit" class="add-form">
        {{-- <form class="form-horizontal form-create" method="post"  action="/../admin/works/add-type/{{$work->id}}" > --}}
            <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                <div class="modal-body">                 
                    <div class="row">
                        <div class="form-group col-md-6">
                        <label for="">Tipo de seguimiento</label>
                                        <multiselect
                                        v-model="form.typechecks"
                                        :options="typechecks"
                                        :multiple="false"
                                        track-by="id"
                                        :taggable="true"
                                        tag-placeholder=""
                                        :custom-label="typeLabel"
                                        placeholder=""
                                        @input="executeLoader">
                                    </multiselect>
                                <div v-if="errors.has('typechecks')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('government_id') }}</div>
                            </div>
                      
                        </div>
                        <div class="row ">
                            <div class="form-group col-md-6" >
                                <label for="">Fecha</label>
                            <datetime v-model="form.fecha" 
                                :config="datePickerConfig" 
                                class="flatpickr" 
                                :class="{'form-control-danger': errors.has('fecha_tr'), 'form-control-success': fields.fecha_tr && fields.fecha_tr.valid}" 
                                id="fecha" 
                                name="fecha" 
                                placeholder="{{ trans('brackets/admin-ui::admin.forms.select_a_date') }}">
                            </datetime>
                            </div>
                        </div>
                    </div>
                    <input type="hidden" value="{{$work->id}}"  id="work_id" name="work_id" >
                    <input type="hidden" v-model="form.type_check_id" id="type_check_id" name="type_check_id" >
                   
                <div class="modal-footer">
                    <button id="btn-guardar" type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button  id="btn-guardar" type="submit" class="btn btn-primary">Guardar</button>
                </div>

    </form>
    </work-form-modal>
        <notifications position="top center" group="auth"/>
      </div>
    </div>
  </div>
@endsection