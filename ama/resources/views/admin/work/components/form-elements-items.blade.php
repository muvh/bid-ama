<div class="form-group row align-items-center" :class="{'has-danger': errors.has('fecha_ws'), 'has-success': fields.fecha_ws && fields.fecha_ws.valid }">
    <label for="fecha_ws" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.work.columns.fecha_ws') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-sm-8'">
        <div class="input-group input-group--custom">
            <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
            {{-- <datetime v-model="form.fecha_ws" :config="datePickerConfig" v-validate="'required|date_format:yyyy-MM-dd HH:mm:ss'" class="flatpickr" :class="{'form-control-danger': errors.has('fecha_ws'), 'form-control-success': fields.fecha_ws && fields.fecha_ws.valid}" id="fecha_ws" name="fecha_ws" placeholder="{{ trans('brackets/admin-ui::admin.forms.select_a_date') }}"></datetime> --}}
            <input readonly type="text" value="{{date('d/m/Y', strtotime($work->fecha_ws))}}" class="form-control">
        </div>
        <div v-if="errors.has('fecha_ws')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('fecha_ws') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('applicant_id'), 'has-success': fields.applicant_id && fields.applicant_id.valid }">
    <label for="applicant_id" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.work.columns.applicant_id') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
         {{-- <input type="hidden" v-model="form.applicant_id" v-validate="'required'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('applicant_id'), 'form-control-success': fields.applicant_id && fields.applicant_id.valid}" id="applicant_id" name="applicant_id" placeholder="{{ trans('admin.work.columns.applicant_id') }}"> --}}
         {{-- <input type="text" v-model="form.applicants" v-validate="'required'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('applicant_id'), 'form-control-success': fields.applicant_id && fields.applicant_id.valid}" id="applicant_id" name="applicant_id" placeholder="{{ trans('admin.work.columns.applicant_id') }}"> --}}
     {{-- <input readonly type="text" class="form-control" value="{{$work->applicant->name}}{{$work->applicant->last_name}}"> --}}

        <div v-if="errors.has('applicant')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('applicant_id') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('supervisor_id'), 'has-success': fields.supervisor_id && fields.supervisor_id.valid }">
    <label for="supervisor_id" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.work.columns.supervisor_id') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
         {{-- <input type="hidden" v-model="form.supervisor_id" v-validate="'required'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('supervisor_id'), 'form-control-success': fields.supervisor_id && fields.supervisor_id.valid}" id="supervisor_id" name="supervisor_id" placeholder="{{ trans('admin.work.columns.supervisor_id') }}">
         <input type="text" v-model="form.supervisors" v-validate="'required'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('supervisor_id'), 'form-control-success': fields.supervisor_id && fields.supervisor_id.valid}" id="supervisor_id" name="supervisor_id" placeholder="{{ trans('admin.work.columns.supervisor_id') }}"> --}}
         <input readonly type="text" class="form-control" value="{{$work->supervisor->name}}">

        <div v-if="errors.has('supervisor_id')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('supervisor_id') }}</div>
    </div>
</div>


