<div class="form-group row align-items-center" :class="{'has-danger': errors.has('supervisor_id'), 'has-success': fields.supervisor_id && fields.supervisor_id.valid }">
    <label for="supervisor_id" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.work.columns.supervisor_id') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
         <input type="hidden" v-model="form.supervisor_id" v-validate="'required'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('supervisor_id'), 'form-control-success': fields.supervisor_id && fields.supervisor_id.valid}" id="supervisor_id" name="supervisor_id" placeholder="{{ trans('admin.work.columns.supervisor_id') }}">
         
         
        <multiselect
        v-model="form.supervisor"
        :options="supervisors"
        :multiple="false"
        track-by="id"
        :taggable="true"
        tag-placeholder=""
        :custom-label="supervisorLabel"
        @input="supervisorsLoader"
        placeholder="">
    </multiselect>
        
        <div v-if="errors.has('supervisor_id')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('supervisor_id') }}</div>
    </div>
</div>

<input type="hidden" v-model="form.fecha_ws" class="form-control">
<input type="hidden" v-model="form.applicant_id" v-validate="'required'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('applicant_id'), 'form-control-success': fields.applicant_id && fields.applicant_id.valid}" id="applicant_id" name="applicant_id" placeholder="{{ trans('admin.work.columns.applicant_id') }}">