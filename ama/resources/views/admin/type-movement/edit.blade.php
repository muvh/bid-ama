@extends('brackets/admin-ui::admin.layout.default')

@section('title', trans('admin.type-movement.actions.edit', ['name' => $typeMovement->name]))

@section('body')

    <div class="container-xl">
        <div class="card">

            <type-movement-form
                :action="'{{ $typeMovement->resource_url }}'"
                :data="{{ $typeMovement->toJson() }}"
                v-cloak
                inline-template>
            
                <form class="form-horizontal form-edit" method="post" @submit.prevent="onSubmit" :action="action" novalidate>


                    <div class="card-header">
                        <i class="fa fa-pencil"></i> {{ trans('admin.type-movement.actions.edit', ['name' => $typeMovement->name]) }}
                    </div>

                    <div class="card-body">
                        @include('admin.type-movement.components.form-elements')
                    </div>
                    
                    
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary" :disabled="submiting">
                            <i class="fa" :class="submiting ? 'fa-spinner' : 'fa-download'"></i>
                            {{ trans('brackets/admin-ui::admin.btn.save') }}
                        </button>
                    </div>
                    
                </form>

        </type-movement-form>

        </div>
    
</div>

@endsection