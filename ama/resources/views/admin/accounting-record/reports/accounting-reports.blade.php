<table>
    <thead>
        <tr>
            <th>Descripcion</th>
            <th>Tipo de Movimiento</th>
            <th>Cantidad</th>
            <th>Balance</th>
            <th>Resolucion</th>
            <th>Fecha Registro</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($invoices as $invoice)
            <tr>
                <td>{{ $invoice->summary }}</td>
                <td>{{ $invoice->typeMovement['name'] }}</td>
                <td>{{ $invoice->beneficiaries_count > 0 ? $invoice->beneficiaries_count : 'N/A' }}</td>
                <td>{{ $invoice->balance }}</td>
                <td>{{ $invoice->resolution }}</td>
                <td>{{ $invoice->registration_date }}</td>
            </tr>
        @endforeach
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>Ingreso: {{ $ingresos }}</td>
            <td>Desembolso: {{ $egresos }}</td>
            <td>Saldo: {{ $balance }}</td>
            <td>Subsidios Disponibles: {{ $subsidios }}</td>
            <td></td>
            <td></td>
        </tr>
    </tbody>
</table>
