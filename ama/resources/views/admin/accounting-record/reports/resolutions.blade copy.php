<h2>Resoluciones</h2>

<table>
    <thead>
        <tr>
            <th>Resoluciones Aprobadas</th>
            <th>Subsidios Habitacionales</th>
            <th>Monto del subsidio</th>
            <th>Totales</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($invoices as $invoice)
            <tr>
                <td>{{ $invoice->summary }}</td>
                <td>{{ $invoice->typeMovement['name'] }}</td>
                <td>{{ $invoice->beneficiaries_count > 0 ? $invoice->beneficiaries_count : 'N/A' }}</td>
                <td>{{ $invoice->balance }}</td>
            </tr>
        @endforeach
        <tr>
            <td><strong>TOTALES</strong></td>
            <td></td>
            <td></td>
            <td></td>

        </tr>

    </tbody>
</table>
