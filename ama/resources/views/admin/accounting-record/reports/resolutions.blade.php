<h2>Tranferencias</h2>

<table>
    <thead>
        <tr>
            <th>Nro Sol</th>
            <th>Fecha RT</th>
            <th>Fecha 80</th>
            <th>Fecha 20</th>
            <th>Estado</th>
            <th>Nro Nota BID</th>
            <th>Tipo de Orden</th>
            <th>ATC</th>
            <th>Razon Social ATC</th>
            <th>Nombre Benf.</th>
            <th>Apellido Benf.</th>
            <th>CI</th>
            <th>Cooperativa</th>
            <th>Razon Social</th>
            <th>Observacion</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($invoices as $invoice)
            <tr>
                <td>{{ $invoice->nro_solictud_rt }}</td>
                <td>{{ $invoice->fecha_rt }}</td>
                <td>{{ $invoice->applicantresource->applicant->enviocac ? $invoice->applicantresource->applicant->enviocac->created_at : '' }}
                </td>
                <td>{{ $invoice->applicantresource->applicant->envio20cac ? $invoice->applicantresource->applicant->envio20cac->created_at : '' }}
                </td>
                <td>{{ $invoice->applicantresource->applicant->statuses->status->name }}</td>
                <td>{{ $invoice->nro_nota_bid }}</td>
                <td>{{ $invoice->typeorder->title }}</td>
                <td>{{ $invoice->construction->name }}</td>
                <td>{{ $invoice->construction->razon_social }}</td>
                <td>{{ $invoice->applicantresource->applicant->names }} </td>
                <td>{{ $invoice->applicantresource->applicant->last_names }}</td>
                <td>{{ $invoice->applicantresource->applicant->government_id }}</td>
                <td>{{ $invoice->applicantresource->applicant->financial ? $invoice->applicantresource->applicant->financial->name : 'N/A' }}
                </td>
                <td>{{ $invoice->applicantresource->applicant->financial ? $invoice->applicantresource->applicant->financial->razon_social : 'N/A' }}
                </td>
                <td>{{ $invoice->obs }}</td>
            </tr>
        @endforeach
        {{-- <tr>
            <td><strong>TOTALES</strong></td>
            <td></td>
            <td></td>
            <td></td>

        </tr> --}}

    </tbody>
</table>
