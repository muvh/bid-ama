<div class="form-group row align-items-center"
    :class="{ 'has-danger': errors.has('summary'), 'has-success': fields.summary && fields.summary.valid }">
    <label for="summary" class="col-form-label text-md-right"
        :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.accounting-record.columns.summary') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.summary" v-validate="'required'" @input="validate($event)" class="form-control"
            :class="{
                'form-control-danger': errors.has('summary'),
                'form-control-success': fields.summary && fields.summary
                    .valid
            }"
            id="summary" name="summary" placeholder="{{ trans('admin.accounting-record.columns.summary') }}">
        <div v-if="errors.has('summary')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('summary') }}</div>
    </div>
</div>

<div class="form-group row align-items-center"
    :class="{
        'has-danger': errors.has('type_movements_id'),
        'has-success': fields.type_movements_id && fields.type_movements_id
            .valid
    }">
    <label for="type_movements_id" class="col-form-label text-md-right"
        :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.accounting-record.columns.type_movements_id') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <multiselect v-model="form.type_movements" :options="type_movements" :multiple="false" track-by="id"
            label="name" :taggable="true" tag-placeholder=""
            placeholder="{{ trans('admin.workflowstate.actions.search') }}">
        </multiselect>
        {{-- <input type="text" v-model="form.type_movements_id" v-validate="'required'" @input="validate($event)"
            class="form-control"
            :class="{
                'form-control-danger': errors.has('type_movements_id'),
                'form-control-success': fields
                    .type_movements_id && fields.type_movements_id.valid
            }"
            id="type_movements_id" name="type_movements_id"
            placeholder="{{ trans('admin.accounting-record.columns.type_movements_id') }}"> --}}
        <div v-if="errors.has('type_movements_id')" class="form-control-feedback form-text" v-cloak>
            @{{ errors.first('type_movements_id') }}</div>
    </div>
</div>

{{-- <div class="form-group row align-items-center"
    :class="{ 'has-danger': errors.has('amount'), 'has-success': fields.amount && fields.amount.valid }">
    <label for="amount" class="col-form-label text-md-right"
        :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.accounting-record.columns.amount') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.amount" @input="validate($event)" class="form-control"
            :class="{
                'form-control-danger': errors.has('amount'),
                'form-control-success': fields.amount && fields.amount
                    .valid
            }"
            id="amount" name="amount" placeholder="{{ trans('admin.accounting-record.columns.amount') }}">
        <div v-if="errors.has('amount')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('amount') }}</div>
    </div>
</div> --}}

<div class="form-group row align-items-center"
    :class="{ 'has-danger': errors.has('balance'), 'has-success': fields.balance && fields.balance.valid }">
    <label for="balance" class="col-form-label text-md-right"
        :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.accounting-record.columns.balance') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.balance" @input="validate($event)" class="form-control"
            :class="{
                'form-control-danger': errors.has('balance'),
                'form-control-success': fields.balance && fields.balance
                    .valid
            }"
            id="balance" name="balance" placeholder="{{ trans('admin.accounting-record.columns.balance') }}">
        <div v-if="errors.has('balance')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('balance') }}</div>
    </div>
</div>

{{-- <div class="form-check row"
    :class="{ 'has-danger': errors.has('confirmed'), 'has-success': fields.confirmed && fields.confirmed.valid }">
    <div class="ml-md-auto" :class="isFormLocalized ? 'col-md-8' : 'col-md-10'">
        <input class="form-check-input" id="confirmed" type="checkbox" v-model="form.confirmed" v-validate="''"
            data-vv-name="confirmed" name="confirmed_fake_element">
        <label class="form-check-label" for="confirmed">
            {{ trans('admin.accounting-record.columns.confirmed') }}
        </label>
        <input type="hidden" name="confirmed" :value="form.confirmed">
        <div v-if="errors.has('confirmed')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('confirmed') }}</div>
    </div>
</div> --}}

<div class="form-group row align-items-center"
    :class="{ 'has-danger': errors.has('resolution'), 'has-success': fields.resolution && fields.resolution.valid }">
    <label for="resolution" class="col-form-label text-md-right"
        :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.accounting-record.columns.resolution') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.resolution" @input="validate($event)" class="form-control"
            :class="{
                'form-control-danger': errors.has('resolution'),
                'form-control-success': fields.resolution && fields
                    .resolution.valid
            }"
            id="resolution" name="resolution" placeholder="{{ trans('admin.accounting-record.columns.resolution') }}">
        <div v-if="errors.has('resolution')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('resolution') }}
        </div>
    </div>
</div>

<div class="form-group row align-items-center"
    :class="{
        'has-danger': errors.has('registration_date'),
        'has-success': fields.registration_date && fields.registration_date
            .valid
    }">
    <label for="registration_date" class="col-form-label text-md-right"
        :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.accounting-record.columns.registration_date') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-sm-8'">
        <div class="input-group input-group--custom">
            <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
            <datetime v-model="form.registration_date" :config="datePickerConfig"
                v-validate="'required|date_format:yyyy-MM-dd HH:mm:ss'" class="flatpickr"
                :class="{
                    'form-control-danger': errors.has('registration_date'),
                    'form-control-success': fields
                        .registration_date && fields.registration_date.valid
                }"
                id="registration_date" name="registration_date"
                placeholder="{{ trans('brackets/admin-ui::admin.forms.select_a_date') }}"></datetime>
        </div>
        <div v-if="errors.has('registration_date')" class="form-control-feedback form-text" v-cloak>
            @{{ errors.first('registration_date') }}</div>
    </div>
</div>
