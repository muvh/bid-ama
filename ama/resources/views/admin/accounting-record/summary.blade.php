
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Resumen Contable</title>
</head>
<body>
  <header>
    <img src="{{ public_path('logo-cab-transf-recursos.jpg') }}" >
  </header>
  <footer>
    <img src="{{ public_path('logo-pie-transf-recursos.png') }}" width=" 750px" >
</footer>
<main>
    <style type="text/css">

        .tg  {border-collapse:collapse;border-spacing:0;width:75%;margin: -10px 120px}
        .tg td{border-color:black;border-style:solid;border-width:1px;font-family:Calibri;font-size:10px;
          overflow:hidden;padding:10px 5px;word-break:normal;}
        .tg th{border-color:black;border-style:solid;border-width:1px;font-family:Calibri;font-size:10x;
          font-weight:normal;overflow:hidden;padding:10px 5px;word-break:normal;}
        .tg .tg-0pky{border-color:inherit;text-align:left;vertical-align:top}
        .tg .tg-za14{border-color:inherit;text-align:center;vertical-align:bottom}
        body {
                margin-top: 2cm;
                margin-left: 0cm;
                margin-right: 0cm;
                margin-bottom: 2cm;
            }

        header {
                position: fixed;
                top: -30px;
                left: 90px;
                right: 0px;
                height: 50px;
                font-size: 20px !important;

    
            }
         
            footer {
                position: fixed; 
                bottom: 20px; 
                left: 0px; 
                right: 0px;
                height: 50px; 
                font-size: 20px !important;
            }
            .noBorder {
                border:none !important;
            }
        </style>
      <br>
      
     


        <h5 style="text-align: center; margin: 20px 10px">&nbsp;</h5>

        <table class="tg">
          <thead>
            <tr>
              <th class="tg-bzci" colspan="4"><span style="font-weight:bold">Resumen Contable</span></th>
            </tr>
          </thead>
          <tbody>
            <tr>                                                              
              <td class="tg-0lax"><span style="font-weight:bold">Ingresos: </span>{{number_format($ingresos,0, '.', '.')}}</td>
              <td class="tg-0lax"><span style="font-weight:bold">Desembolsos:</span> {{number_format($egresos,0, '.', '.')}}</td>
              <td class="tg-0lax"><span style="font-weight:bold">Saldo: </span>{{number_format($balance,0, '.', '.')}}</td>
              <td class="tg-0lax"><span style="font-weight:bold">Subsidios Disponibles:</span>{{number_format($subsidios,0, '.', '.')}}</td>
            </tr>
          </tbody>
          </table>
          
          <br>
          <br>

  
            <table class="tg">
            <thead>
              <tr>
                <td class="tg-0lax"><span style="font-weight:bold">Resumen</span></td>
                <td class="tg-0lax"><span style="font-weight:bold">Tipo de movimientos</span></td>
                <td class="tg-0lax"><span style="font-weight:bold">Cantidad</span></td>
                <td class="tg-0lax"><span style="font-weight:bold">Balance</span></td>
                <td class="tg-0lax"><span style="font-weight:bold">Confirmado</span></td>
                <td class="tg-0lax"><span style="font-weight:bold">Resolución</span></td>
                <td class="tg-0lax"><span style="font-weight:bold">Fecha de Registro</span></td>
              </tr>
            </thead>
            <tbody>
             @foreach ($record as $item)
                 
             
                <tr>
                  <td class="tg-0lax">{{$item->summary}}</td>
                  <td class="tg-0lax">
                    @if($item->type_movements_id==1)
                    {{'Transferencia de recursos'}}
                    @endif
                    @if($item->type_movements_id==2)
                    {{'Reserva'}}
                    @endif
                    @if($item->type_movements_id==3)
                    {{'Devolución por Recisión'}}
                    @endif
                    @if($item->type_movements_id==4)
                    {{'Devolución Temporal'}}
                    @endif
                    @if($item->type_movements_id==5)
                    {{'Reserva Masiva'}}
                    @endif
                  </td>
                 
                  <td class="tg-0lax">
                    {{$item->beneficiaries_count}}
                  </td>
                  <td class="tg-0lax"> {{number_format($item->balance,0, '.', '.')}}</td>
                  <td class="tg-0lax">
                    @if($item->confirmed==false)
                      {{'NO'}}
                    @endif
                    @if($item->confirmed==true)
                      {{'SI'}}
                    @endif
                  </td>
                  <td class="tg-0lax">
                    {{$item->resolution}}
                  </td>
                  <td class="tg-0lax">
                    {{ date('d/m/Y', strtotime($item->registration_date) )}}
                  </td>
                </tr>
              @endforeach
            </tbody>
            </table>


      
        <br>
        <br>

    
    
   
        
        </main>
</body>
</html>