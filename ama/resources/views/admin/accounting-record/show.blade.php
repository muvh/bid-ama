@extends('brackets/admin-ui::admin.layout.default')

@section('title', trans('admin.accounting-record.actions.index'))

@section('body')

    <div class="card">
        <div class="card-header text-center">
            Resumen Contable

            {{-- <a class="btn btn-primary btn-spinner btn-sm pull-right m-b-0" href="{{ url()->previous() }}" role="button"><i
                    class="fa fa-undo"></i>&nbsp; {{ trans('admin.guest.actions.back') }}</a> --}}
        </div>
        <div class="card-body">
            <div class="row">
                <div class="form-group col-sm-3">
                    <p class="card-text"><strong>Ingresos: </strong> {{ number_format($ingresos, 0, '.', '.') }} </p>
                </div>
                <div class="form-group col-sm-3">
                    <p class="card-text"><strong>Desembolsos: </strong>{{ number_format($egresos, 0, '.', '.') }}
                    </p>
                </div>
                <div class="form-group col-sm-3">
                    <p class="card-text"><strong>Saldo: </strong>{{ number_format($balance, 0, '.', '.') }} </p>
                </div>
                <div class="form-group col-sm-3">
                    <p class="card-text"><strong>Subsidios Disponibles:
                        </strong>{{ $subsidios }} </p>
                </div>
            </div>
        </div>
    </div>


    <applicant-record-listing :data="{{ $data->toJson() }}"
        :confirmed="{{ $accountingRecord['confirmed'] ? 'true' : 'false' }}"
        :url="'{{ url('admin/accounting-records/' . $accountingRecord['id'] . '/show') }}'" inline-template>


        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="card-header">
                        <i class="fa fa-align-justify"></i> Detalle Reserva: {{ $accountingRecord->summary }}
                        <a class="btn btn-primary btn-spinner btn-sm pull-right m-b-0"
                            href="{{ url('admin/accounting-records/' . $accountingRecord['id'] . '/applicant-record/create') }}"
                            role="button"><i class="fa fa-plus"></i>&nbsp;
                            {{ trans('admin.applicant-record.actions.create') }}</a>
                    </div>
                    <div class="card-header" v-if="confirm==false">
                        <form class="col" @submit.prevent="confirmItem('confirm')">
                            <button
                                class="btn btn-lg btn-block btn-square btn-success rounded  pull-right m-b-0">Confirmar</button>
                        </form>
                    </div>
                    <div class="card-body" v-cloak>
                        <div class="card-block">
                            <form @submit.prevent="">
                                <div class="row justify-content-md-between">
                                    <div class="col col-lg-7 col-xl-5 form-group">
                                        <div class="input-group">
                                            <input class="form-control"
                                                placeholder="{{ trans('brackets/admin-ui::admin.placeholder.search') }}"
                                                v-model="search" @keyup.enter="filter('search', $event.target.value)" />
                                            <span class="input-group-append">
                                                <button type="button" class="btn btn-primary"
                                                    @click="filter('search', search)"><i class="fa fa-search"></i>&nbsp;
                                                    {{ trans('brackets/admin-ui::admin.btn.search') }}</button>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="col-sm-auto form-group ">
                                        <select class="form-control" v-model="pagination.state.per_page">

                                            <option value="10">10</option>
                                            <option value="25">25</option>
                                            <option value="100">100</option>
                                        </select>
                                    </div>
                                </div>
                            </form>

                            <table class="table table-hover table-listing">
                                <thead>
                                    <tr>
                                        {{-- <th class="bulk-checkbox">
                                            <input class="form-check-input" id="enabled" type="checkbox"
                                                v-model="isClickedAll" v-validate="''" data-vv-name="enabled"
                                                name="enabled_fake_element" @click="onBulkItemsClickedAllWithPagination()">
                                            <label class="form-check-label" for="enabled">
                                                #
                                            </label>
                                        </th>

                                        <th is='sortable' :column="'id'">
                                            {{ trans('admin.applicant-record.columns.id') }}</th> --}}
                                        {{-- <th is='sortable' :column="'accounting_record_id'">
                                            {{ trans('admin.applicant-record.columns.accounting_record_id') }}</th> --}}
                                        <th is='sortable' :column="'applicant_id'">
                                            {{ trans('admin.applicant-record.columns.applicant_id') }}</th>
                                        <th is='sortable' :column="'amount'">
                                            Documento</th>
                                        <th is='sortable' :column="'amount'">
                                            Cooperativa</th>
                                        <th is='sortable' :column="'amount'">
                                            {{ trans('admin.applicant-record.columns.amount') }}</th>

                                        <th></th>
                                    </tr>
                                    <tr v-show="(clickedBulkItemsCount > 0) || isClickedAll">
                                        <td class="bg-bulk-info d-table-cell text-center" colspan="6">
                                            <span
                                                class="align-middle font-weight-light text-dark">{{ trans('brackets/admin-ui::admin.listing.selected_items') }}
                                                @{{ clickedBulkItemsCount }}. <a href="#" class="text-primary"
                                                    @click="onBulkItemsClickedAll('/admin/applicant-records')"
                                                    v-if="(clickedBulkItemsCount < pagination.state.total)"> <i
                                                        class="fa"
                                                        :class="bulkCheckingAllLoader ? 'fa-spinner' : ''"></i>
                                                    {{ trans('brackets/admin-ui::admin.listing.check_all_items') }}
                                                    @{{ pagination.state.total }}</a> <span class="text-primary">|</span> <a
                                                    href="#" class="text-primary"
                                                    @click="onBulkItemsClickedAllUncheck()">{{ trans('brackets/admin-ui::admin.listing.uncheck_all_items') }}</a>
                                            </span>

                                            <span class="pull-right pr-2">
                                                <button class="btn btn-sm btn-danger pr-3 pl-3"
                                                    @click="bulkDelete('/admin/applicant-records/bulk-destroy')">{{ trans('brackets/admin-ui::admin.btn.delete') }}</button>
                                            </span>

                                        </td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr v-for="(item, index) in collection" :key="item.id"
                                        :class="bulkItems[item.id] ? 'bg-bulk' : ''">
                                        {{-- <td class="bulk-checkbox">
                                            <input class="form-check-input" :id="'enabled' + item.id" type="checkbox"
                                                v-model="bulkItems[item.id]" v-validate="''"
                                                :data-vv-name="'enabled' + item.id"
                                                :name="'enabled' + item.id + '_fake_element'"
                                                @click="onBulkItemClicked(item.id)" :disabled="bulkCheckingAllLoader">
                                            <label class="form-check-label" :for="'enabled' + item.id">
                                            </label>
                                        </td>

                                        <td>@{{ item.id }}</td> --}}
                                        {{-- <td>@{{ item.accounting_record_id }}</td> --}}
                                        <td>@{{ item.applicant.names }} @{{ item.applicant.last_names }}</td>
                                        <td>@{{ formatPrice(item.applicant.government_id) }} </td>
                                        <td>@{{ item.applicant.financial.razon_social }} </td>

                                        <td>@{{ formatPrice(item.amount) }}</td>

                                        <td>
                                            <div class="row no-gutters">
                                                <div class="col-auto">
                                                    <a class="btn btn-sm btn-spinner btn-info"
                                                        :href="item.resource_url + '/edit'"
                                                        title="{{ trans('brackets/admin-ui::admin.btn.edit') }}"
                                                        role="button"><i class="fa fa-edit"></i></a>
                                                </div>
                                                <form class="col" @submit.prevent="deleteItem(item.resource_url)">
                                                    <button type="submit" class="btn btn-sm btn-danger"
                                                        title="{{ trans('brackets/admin-ui::admin.btn.delete') }}"><i
                                                            class="fa fa-trash-o"></i></button>
                                                </form>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        {{-- <td></td>
                                        <td></td> --}}
                                        <td></td>
                                        <td></td>

                                        <td></td>
                                        <td v-if="borrado == false">
                                            <strong>{{ $accountingRecord->beneficiaries->sum('amount') }}</strong>
                                        </td>
                                        <td v-else><strong> @{{ totalBorrado }}</strong> </td>

                                        <td></td>
                                    </tr>
                                </tbody>
                            </table>

                            <div class="row" v-if="pagination.state.total > 0">
                                <div class="col-sm">
                                    <span
                                        class="pagination-caption">{{ trans('brackets/admin-ui::admin.pagination.overview') }}</span>
                                </div>
                                <div class="col-sm-auto">
                                    <pagination></pagination>
                                </div>
                            </div>

                            <div class="no-items-found" v-if="!collection.length > 0">
                                <i class="icon-magnifier"></i>
                                <h3>{{ trans('brackets/admin-ui::admin.index.no_items') }}</h3>
                                <p>{{ trans('brackets/admin-ui::admin.index.try_changing_items') }}</p>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </applicant-record-listing>

@endsection
