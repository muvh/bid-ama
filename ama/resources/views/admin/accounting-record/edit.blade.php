@extends('brackets/admin-ui::admin.layout.default')

@section('title', trans('admin.accounting-record.actions.edit', ['name' => $accountingRecord->id]))

@section('body')

    <div class="container-xl">
        <div class="card">

            <accounting-record-form :action="'{{ $accountingRecord->resource_url }}'"
                :data="{{ $accountingRecord->toJson() }}" :type_movements="{{ $type_movements }}" v-cloak inline-template>

                <form class="form-horizontal form-edit" method="post" @submit.prevent="onSubmit" :action="action"
                    novalidate>


                    <div class="card-header">
                        <i class="fa fa-pencil"></i>
                        {{ trans('admin.accounting-record.actions.edit', ['name' => $accountingRecord->id]) }}
                    </div>

                    <div class="card-body">
                        @include('admin.accounting-record.components.form-elementsedit')
                    </div>


                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary" :disabled="submiting">
                            <i class="fa" :class="submiting ? 'fa-spinner' : 'fa-download'"></i>
                            {{ trans('brackets/admin-ui::admin.btn.save') }}
                        </button>
                    </div>

                </form>

            </accounting-record-form>

        </div>

    </div>

@endsection
