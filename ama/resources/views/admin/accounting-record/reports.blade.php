@extends('brackets/admin-ui::admin.layout.default')

@section('title', trans('admin.accounting-record.actions.create'))

@section('body')

    <div class="card">
        <div class="card-header text-center">
            Resumen Contable

            {{-- <a class="btn btn-primary btn-spinner btn-sm pull-right m-b-0" href="{{ url()->previous() }}" role="button"><i
                    class="fa fa-undo"></i>&nbsp; {{ trans('admin.guest.actions.back') }}</a> --}}
        </div>
        <div class="card-body">
            <div class="row">
                <div class="form-group col-sm-3">
                    <p class="card-text"><strong>Ingresos: </strong> {{ number_format($ingresos, 0, '.', '.') }} </p>
                </div>
                <div class="form-group col-sm-3">
                    <p class="card-text"><strong>Desembolsos: </strong>{{ number_format($egresos, 0, '.', '.') }}
                    </p>
                </div>
                <div class="form-group col-sm-3">
                    <p class="card-text"><strong>Saldo: </strong>{{ number_format($balance, 0, '.', '.') }} </p>
                </div>
                <div class="form-group col-sm-3">
                    <p class="card-text"><strong>Subsidios Disponibles:
                        </strong>{{ $subsidios }} </p>
                </div>
            </div>
        </div>
    </div>

    <div class="card">
        <div class="card-header text-center">
            Reportes

            {{-- <a class="btn btn-primary btn-spinner btn-sm pull-right m-b-0" href="{{ url()->previous() }}" role="button"><i
                    class="fa fa-undo"></i>&nbsp; {{ trans('admin.guest.actions.back') }}</a> --}}
        </div>
        <div class="card-body">
            <div class="row">
                <div class="form-group col-sm-3">
                    <p class="card-text">
                        <a class="btn btn-success" href="{{ 'reports/accounting' }}" role="button"><i
                                class="fa fa-file-excel-o"></i>&nbsp; Resumen Contable</a>
                    </p>
                </div>
                <div class="form-group col-sm-3">
                    <p class="card-text">
                        <a class="btn btn-success" href="{{ 'reports/resolutions' }}" role="button"><i
                                class="fa fa-file-excel-o"></i>&nbsp; Resoluciones Aprobadas</a>
                    </p>
                </div>
                <div class="form-group col-sm-3">
                    <p class="card-text">
                        <a class="btn btn-success" href="{{ 'reports/accounting' }}" role="button"><i
                                class="fa fa-file-excel-o"></i>&nbsp; {{ trans('admin.guest.actions.back') }}</a>
                    </p>
                </div>
                <div class="form-group col-sm-3">
                    <p class="card-text">
                        <a class="btn btn-success" href="{{ 'reports/accounting' }}" role="button"><i
                                class="fa fa-file-excel-o"></i>&nbsp; {{ trans('admin.guest.actions.back') }}</a>
                    </p>
                </div>
            </div>
        </div>
    </div>


@endsection
