<div class="form-group row align-items-center" :class="{'has-danger': errors.has('income'), 'has-success': fields.income && fields.income.valid }">
    <label for="income" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.accounting-summary.columns.income') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.income" v-validate="'required'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('income'), 'form-control-success': fields.income && fields.income.valid}" id="income" name="income" placeholder="{{ trans('admin.accounting-summary.columns.income') }}">
        <div v-if="errors.has('income')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('income') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('expenses'), 'has-success': fields.expenses && fields.expenses.valid }">
    <label for="expenses" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.accounting-summary.columns.expenses') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.expenses" v-validate="'required'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('expenses'), 'form-control-success': fields.expenses && fields.expenses.valid}" id="expenses" name="expenses" placeholder="{{ trans('admin.accounting-summary.columns.expenses') }}">
        <div v-if="errors.has('expenses')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('expenses') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('balance'), 'has-success': fields.balance && fields.balance.valid }">
    <label for="balance" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.accounting-summary.columns.balance') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.balance" v-validate="'required'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('balance'), 'form-control-success': fields.balance && fields.balance.valid}" id="balance" name="balance" placeholder="{{ trans('admin.accounting-summary.columns.balance') }}">
        <div v-if="errors.has('balance')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('balance') }}</div>
    </div>
</div>


