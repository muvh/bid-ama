<input type="hidden" v-model="form.applicant_id" v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('applicant_id'), 'form-control-success': fields.applicant_id && fields.applicant_id.valid}" id="applicant_id" name="applicant_id" placeholder="{{ trans('admin.applicant-document.columns.applicant_id') }}">
<div class="form-group row align-items-center" :class="{'has-danger': errors.has('document_id'), 'has-success': fields.document_id && fields.document_id.valid }">
    <label for="document_id" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.applicant-document.columns.document_id') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
            <multiselect
                v-model="form.document"
                :options="documents"
                :multiple="false"
                track-by="id"
                label="name"
                :taggable="true"
                tag-placeholder=""
                placeholder="{{ trans('admin.documents.actions.search')  }}">
            </multiselect>

        <div v-if="errors.has('document_id')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('document_id') }}</div>
    </div>
</div>
<div class="form-group row align-items-center" :class="{'has-danger': errors.has('received_at'), 'has-success': fields.received_at && fields.received_at.valid }">
    <label for="received_at" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.applicant-document.columns.received_at') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
            <datetime v-model="form.received_at" :config="datePickerConfig" v-validate="'date_format:yyyy-MM-dd'" class="flatpickr" :class="{'form-control-danger': errors.has('received_at'), 'form-control-success': fields.received_at && fields.received_at.valid}" id="received_at" name="received_at" placeholder="{{ trans('brackets/admin-ui::admin.forms.select_date_and_time') }}"></datetime>
        <div v-if="errors.has('received_at')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('received_at') }}</div>
    </div>
</div>
<div class="row">
    <div class="form-group col-md-1">
    </div>
    <div class="form-group col-md-9">
        @include('brackets/admin-ui::admin.includes.media-uploader', [
        'mediaCollection' => app(App\Models\ApplicantDocument::class)->getMediaCollection('supporting-documents'),
        'media' => isset($applicantDocument)?$applicantDocument->getThumbs200ForCollection('supporting-documents'):null,
        'label' => 'Documento'
        ])
    </div>
</div>


