@extends('brackets/admin-ui::admin.layout.default')

@section('title', trans('admin.document-type.actions.edit', ['name' => $documentType->name]))

@section('body')

    <div class="container-xl">
        <div class="card">

            <document-type-form
                :action="'{{ $documentType->resource_url }}'"
                :data="{{ $documentType->toJson() }}"
                v-cloak
                inline-template>
            
                <form class="form-horizontal form-edit" method="post" @submit.prevent="onSubmit" :action="action" novalidate>


                    <div class="card-header">
                        <i class="fa fa-pencil"></i> {{ trans('admin.document-type.actions.edit', ['name' => $documentType->name]) }}
                    </div>

                    <div class="card-body">
                        @include('admin.document-type.components.form-elements')
                    </div>
                    
                    
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary" :disabled="submiting">
                            <i class="fa" :class="submiting ? 'fa-spinner' : 'fa-download'"></i>
                            {{ trans('brackets/admin-ui::admin.btn.save') }}
                        </button>
                    </div>
                    
                </form>

        </document-type-form>

        </div>
    
</div>

@endsection