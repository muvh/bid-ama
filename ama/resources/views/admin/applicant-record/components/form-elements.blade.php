{{-- <div class="form-group row align-items-center" :class="{'has-danger': errors.has('accounting_record_id'), 'has-success': fields.accounting_record_id && fields.accounting_record_id.valid }">
    <label for="accounting_record_id" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.applicant-record.columns.accounting_record_id') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.accounting_record_id" v-validate="'required'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('accounting_record_id'), 'form-control-success': fields.accounting_record_id && fields.accounting_record_id.valid}" id="accounting_record_id" name="accounting_record_id" placeholder="{{ trans('admin.applicant-record.columns.accounting_record_id') }}">
        <div v-if="errors.has('accounting_record_id')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('accounting_record_id') }}</div>
    </div>
</div> --}}

<div class="row">
    <div class="form-group col-sm-3">
        <label for="government_id">{{ trans('admin.applicant.columns.government_id') }}</label>
        <div class="input-group mb-3">
            <input type="text" v-model="form.government_id" v-validate="''" @input="validate($event)" class="form-control"
                :class="{
                    'form-control-danger': errors.has('government_id'),
                    'form-control-success': fields.government_id &&
                        fields.government_id.valid
                }"
                id="government_id" name="government_id"
                placeholder="{{ trans('admin.applicant.columns.government_id') }}">
            <div v-if="errors.has('government_id')" class="form-control-feedback form-text" v-cloak>
                @{{ errors.first('government_id') }}</div>
            <div class="input-group-append">
                <button @click="findData" class="btn btn-primary" type="button"><i class="fa fa-random"
                        aria-hidden="true"></i></button>
                <div id="verificando" style="display: none;"><img width="40px" src="/images/loading.gif"></div>
            </div>
        </div>
    </div>

    <div class="form-group col-sm-4">
        <label for="names">{{ trans('admin.applicant.columns.names') }}</label>
        <input readonly type="text" onkeyup="javascript:this.value=this.value.toUpperCase();" v-model="form.names"
            v-validate="''" @input="validate($event)" class="form-control"
            :class="{ 'form-control-danger': errors.has('names'), 'form-control-success': fields.names && fields.names.valid }"
            id="names" name="names" placeholder="{{ trans('admin.applicant.columns.names') }}">
        <div v-if="errors.has('names')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('names') }}</div>
    </div>
    <div class="form-group col-sm-5">
        <label for="last_names">{{ trans('admin.applicant.columns.last_names') }}</label>
        <input readonly type="text" onkeyup="javascript:this.value=this.value.toUpperCase();"
            v-model="form.last_names" v-validate="''" @input="validate($event)" class="form-control"
            :class="{
                'form-control-danger': errors.has('last_names'),
                'form-control-success': fields.last_names && fields
                    .last_names.valid
            }"
            id="last_names" name="last_names" placeholder="{{ trans('admin.applicant.columns.last_names') }}">
        <div v-if="errors.has('last_names')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('last_names') }}
        </div>
    </div>
</div>

<div class="row">
    <div class="form-group col-sm-3">
        <label for="applicant_id">{{ trans('admin.applicant-record.columns.applicant_id') }}</label>

        <input readonly type="text" v-model="form.applicant_id" v-validate="'required'" @input="validate($event)"
            class="form-control"
            :class="{
                'form-control-danger': errors.has('applicant_id'),
                'form-control-success': fields.applicant_id && fields
                    .applicant_id.valid
            }"
            id="applicant_id" name="applicant_id"
            placeholder="{{ trans('admin.applicant-record.columns.applicant_id') }}">
        <div v-if="errors.has('applicant_id')" class="form-control-feedback form-text" v-cloak>
            @{{ errors.first('applicant_id') }}
        </div>

    </div>

    <div class="form-group col-sm-4">
        <label for="amount">{{ trans('admin.applicant-record.columns.amount') }}</label>

        <input type="text" v-model="form.amount" v-validate="'required'" @input="validate($event)"
            class="form-control"
            :class="{
                'form-control-danger': errors.has('amount'),
                'form-control-success': fields.amount && fields.amount
                    .valid
            }"
            id="amount" name="amount" placeholder="{{ trans('admin.applicant-record.columns.amount') }}">
        <div v-if="errors.has('amount')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('amount') }}
        </div>

    </div>
</div>
