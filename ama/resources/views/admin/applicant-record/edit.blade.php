@extends('brackets/admin-ui::admin.layout.default')

@section('title', trans('admin.applicant-record.actions.edit', ['name' => $applicantRecord->id]))

@section('body')

    <div class="container-xl">
        <div class="card">

            <applicant-record-form
                :action="'{{ $applicantRecord->resource_url }}'"
                :data="{{ $applicantRecord->toJson() }}"
                v-cloak
                inline-template>
            
                <form class="form-horizontal form-edit" method="post" @submit.prevent="onSubmit" :action="action" novalidate>


                    <div class="card-header">
                        <i class="fa fa-pencil"></i> {{ trans('admin.applicant-record.actions.edit', ['name' => $applicantRecord->id]) }}
                    </div>

                    <div class="card-body">
                        @include('admin.applicant-record.components.form-elements')
                    </div>
                    
                    
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary" :disabled="submiting">
                            <i class="fa" :class="submiting ? 'fa-spinner' : 'fa-download'"></i>
                            {{ trans('brackets/admin-ui::admin.btn.save') }}
                        </button>
                    </div>
                    
                </form>

        </applicant-record-form>

        </div>
    
</div>

@endsection