@php
    $roles = Auth::user()->getRoleNames();
@endphp


{{-- {{dd($menuIns)}} --}}
<div class="sidebar">
    <nav class="sidebar-nav">
        <ul class="nav">
            <li class="nav-title">{{ trans('brackets/admin-ui::admin.sidebar.content') }}</li>


            @if (url('/') == 'https://ama.muvh.gov.py')
                <li class="nav-item"> <a href="{{ 'https://ama.muvh.gov.py/admin' }}" class="nav-link"><i
                            class="nav-icon fa fa-home"></i> INICIO</a></li>
            @else
                <li class="nav-item"> <a href="{{ env('APP_URL') }}/admin/" class="nav-link"> <i
                            class="nav-icon fa fa-home"></i> INICIO</a></li>
            @endif
            <li class="nav-item"> <a class="nav-link" href="/admin/applicants/graphic"><i
                        class="nav-icon fa fa-sitemap"></i> Estados Detallados</a></li>
            @if ($roles->contains('coop') || $roles->contains('atc'))
                <li class="nav-item"><a class="nav-link" href="{{ url('admin/applicants') }}"><i
                            class="nav-icon fa fa-plus-circle"></i> {{ trans('admin.applicant.title') }}</a></li>
                <li class="nav-item"><a class="nav-link" href="{{ url('admin/applicants/processed') }}"><i
                            class="nav-icon fa fa-plus-circle"></i> {{ trans('admin.applicant-processed.title') }}</a>
                </li>
            @endif
            @if ($roles->contains('ama') || $roles->contains('callcenter'))
                <li class="nav-item nav-dropdown">
                    <a class="nav-link nav-dropdown-toggle" href="#">
                        <i class="nav-icon fa fa-cogs"></i> Inscripciones
                    </a>
                    <ul class="nav-dropdown-items">
                        @foreach ($menuIns as $item)
                            @if ($item['prefix'] == 'INS')
                                @php
                                    $id = $item['id'];
                                @endphp
                                <li class="nav-item"><a class="nav-link" {{-- href="admin/applicants/estados/{{$id}}"> --}}
                                        href="{{ route('admin/applicants/estados', ['status' => $id]) }}">
                                        <i class="nav-icon fa fa-cogs"></i> {{ $item['name'] }}</a></li>
                            @endif
                        @endforeach

                    </ul>
                </li>
                <li class="nav-item nav-dropdown">
                    <a class="nav-link nav-dropdown-toggle" href="#">
                        <i class="nav-icon fa fa-file"></i> Postulaciones
                    </a>
                    <ul class="nav-dropdown-items">
                        @foreach ($menuIns as $item)
                            @if ($item['prefix'] == 'POS')
                                @php
                                    $id = $item['id'];
                                @endphp
                                <li class="nav-item"><a class="nav-link" {{-- href="admin/applicants/estados/{{$id}}"> --}}
                                        href="{{ route('admin/applicants/estados', ['status' => $id]) }}">
                                        <i class="nav-icon fa fa-file"></i> {{ $item['name'] }}</a></li>
                            @endif
                        @endforeach

                    </ul>
                </li>
                <li class="nav-item nav-dropdown">
                    <a class="nav-link nav-dropdown-toggle" href="#">
                        <i class="nav-icon fa fa-plus-circle"></i> Adjudicaciones
                    </a>
                    <ul class="nav-dropdown-items">
                        @foreach ($menuIns as $item)
                            @if ($item['prefix'] == 'ADJ')
                                @php
                                    $id = $item['id'];
                                @endphp
                                <li class="nav-item"><a class="nav-link" {{-- href="admin/applicants/estados/{{$id}}"> --}}
                                        href="{{ route('admin/applicants/estados', ['status' => $id]) }}">
                                        <i class="nav-icon fa fa-plus-circle"></i> {{ $item['name'] }}</a></li>
                            @endif
                        @endforeach

                    </ul>
                </li>
                <li class="nav-item nav-dropdown">
                    <a class="nav-link nav-dropdown-toggle" href="#">
                        <i class="nav-icon fa fa-trash-o"></i>Inhabilitaciones
                    </a>
                    <ul class="nav-dropdown-items">
                        @foreach ($menuIns as $item)
                            @if ($item['prefix'] == 'INH')
                                @php
                                    $id = $item['id'];
                                @endphp
                                <li class="nav-item"><a class="nav-link" {{-- href="admin/applicants/estados/{{$id}}"> --}}
                                        href="{{ route('admin/applicants/estados', ['status' => $id]) }}">
                                        <i class="nav-icon fa fa-trash-o"></i> {{ $item['name'] }}</a></li>
                            @endif
                        @endforeach

                    </ul>
                </li>




            @endif
            {{-- <li class="nav-item"><a class="nav-link" href="{{ url('admin/financial-summaries') }}"><i class="nav-icon icon-puzzle"></i> {{ trans('admin.financial-summary.title') }}</a></li> --}}
            @if ($roles->contains('ama'))
                {{-- COORD TECNIC --}}
                <li class="nav-item nav-dropdown">
                    <a class="nav-link nav-dropdown-toggle" href="#">
                        <i class="nav-icon fa fa-money"></i> Coord. Técnica
                    </a>

                    <ul class="nav-dropdown-items">
                        {{-- <li class="nav-item"><a class="nav-link" href="{{ url('admin/applicants/calif-proyectos/23') }}"><i class="nav-icon icon-diamond"></i> Calif. de Proyecto</a></li> --}}
                        <li class="nav-item"><a class="nav-link" href="{{ url('admin/certificates') }}"><i
                                    class="nav-icon icon-star"></i> {{ trans('admin.certificate.title') }}</a></li>
                        <li class="nav-item"><a class="nav-link" href="{{ url('admin/certificates/certificate') }}"><i
                                    class="nav-icon icon-star"></i> {{ trans('admin.certificate.titlefinalobra') }}</a>
                        </li>
                        <li class="nav-item"><a class="nav-link" href="{{ url('admin/type-checks') }}"><i
                                    class="nav-icon icon-star"></i> {{ trans('admin.type-check.title') }}</a></li>
                        <li class="nav-item"><a class="nav-link" href="{{ url('admin/supervisors') }}"><i
                                    class="nav-icon icon-graduation"></i> {{ trans('admin.supervisor.title') }}</a>
                        </li>

                    </ul>
                </li>

                {{-- ********FINANCIERO --}}
                <li class="nav-item nav-dropdown">
                    <a class="nav-link nav-dropdown-toggle" href="#">
                        <i class="nav-icon fa fa-money"></i> Financieros
                    </a>
                    <ul class="nav-dropdown-items">
                        <li class="nav-item"><a class="nav-link" href="{{ url('admin/trusts') }}"><i
                                    class="nav-icon icon-diamond"></i> {{ trans('admin.trust.title') }}</a></li>
                        <li class="nav-item"><a class="nav-link" href="{{ url('admin/type-orders') }}"><i
                                    class="nav-icon icon-star"></i> {{ trans('admin.type-order.title') }}</a></li>
                        {{-- <li class="nav-item"><a class="nav-link" href="{{ url('admin/transfer-orders') }}"><i class="nav-icon icon-book-open"></i> {{ trans('admin.transfer-order.title') }}</a></li>
                <li class="nav-item"><a class="nav-link" href="{{ url('admin/applicants/generacion-op-20/42') }}"><i class="nav-icon icon-diamond"></i> Generar. OP 20%</a></li> --}}
                        <li class="nav-item"><a class="nav-link" href="{{ url('admin/resource-transfers') }}"><i
                                    class="nav-icon icon-graduation"></i>
                                {{ trans('admin.resource-transfer.title') }}</a></li>

                        <li class="nav-item"><a class="nav-link" href="{{ url('admin/insurance-entities') }}"><i
                                    class="nav-icon icon-graduation"></i>
                                {{ trans('admin.insurance-entity.title') }}</a></li>
                        <li class="nav-item"><a class="nav-link" href="{{ url('admin/insurance-policies') }}"><i
                                    class="nav-icon icon-drop"></i> {{ trans('admin.insurance-policy.title') }}</a>
                        </li>
                    </ul>
                </li>






                <li class="nav-item"><a class="nav-link" href="{{ url('admin/feedback') }}"><i
                            class="nav-icon icon-diamond"></i> {{ trans('admin.feedback.title') }}</a></li>
                <li class="nav-item"><a class="nav-link" href="{{ url('admin/visits') }}"><i
                            class="nav-icon icon-compass"></i> {{ trans('admin.visit.title') }}</a></li>
                <li class="nav-item"><a class="nav-link" href="{{ url('admin/works-data') }}"><i class="nav-icon icon-compass"></i> {{ trans('admin.works-datum.title') }}</a></li>
           <li class="nav-item"><a class="nav-link" href="{{ url('admin/accounting-records') }}"><i class="nav-icon icon-book-open"></i> {{ trans('admin.accounting-record.title') }}</a></li>
          {{-- <li class="nav-item"><a class="nav-link" href="{{ url('admin/applicant-records') }}"><i class="nav-icon icon-book-open"></i> {{ trans('admin.applicant-record.title') }}</a></li>
            <li class="nav-item"><a class="nav-link" href="{{ url('admin/accounting-summaries') }}"><i class="nav-icon icon-ghost"></i> {{ trans('admin.accounting-summary.title') }}</a></li>
           <li class="nav-item"><a class="nav-link" href="{{ url('admin/type-movements') }}"><i class="nav-icon icon-plane"></i> {{ trans('admin.type-movement.title') }}</a></li> --}}
           {{-- Do not delete me :) I'm used for auto-generation menu items --}}
            @endif
            {{-- Report --}}
            @if ($roles->contains('coop'))
                <li class="nav-title">{{ trans('admin.sidebar.reports') }}</li>
                <li class="nav-item nav-dropdown">
                    <a class="nav-link nav-dropdown-toggle" href="#">
                        <i class="nav-icon fa fa-file-text"></i> {{ trans('admin.sidebar.general') }}
                    </a>
                    <ul class="nav-dropdown-items">
                        <li class="nav-item"><a class="nav-link"
                                href="{{ url('admin/reports/applicants-coop-status?type_report=coop') }}">{{ trans('admin.reports.actions.applicants-coop-status') }}</a>
                        </li>


                    </ul>
                </li>
            @endif
            @if ($roles->contains('atc'))
                <li class="nav-title">{{ trans('admin.sidebar.reports') }}</li>
                <li class="nav-item nav-dropdown">
                    <a class="nav-link nav-dropdown-toggle" href="#">
                        <i class="nav-icon fa fa-file-text"></i> {{ trans('admin.sidebar.general') }}
                    </a>
                    <ul class="nav-dropdown-items">
                        <li class="nav-item"><a class="nav-link"
                                href="{{ url('admin/reports/applicants-coop-status?type_report=atc') }}">{{ trans('admin.reports.actions.applicants-coop-status') }}</a>
                        </li>


                    </ul>
                </li>
            @endif
            {{-- end Report  --}}
            @if ($roles->contains('ama') || $roles->contains('callcenter'))
                <li class="nav-title">{{ trans('admin.sidebar.reports') }}</li>
                <li class="nav-item nav-dropdown">
                    <a class="nav-link nav-dropdown-toggle" href="#">
                        <i class="nav-icon fa fa-file-text"></i> {{ trans('admin.sidebar.general') }}
                    </a>
                    <ul class="nav-dropdown-items">

                        <li class="nav-item"><a class="nav-link"
                                href="{{ url('admin/reports/applicants-by-resolution') }}">{{ trans('admin.reports.actions.applicants-by-resolution') }}</a>
                        </li>
                        <li class="nav-item"><a class="nav-link"
                                href="{{ url('admin/reports/applicants-by-resolution-social') }}">{{ trans('admin.reports.actions.applicants-by-resolution-social') }}</a>
                        </li>
                        <li class="nav-item"><a class="nav-link"
                                href="{{ url('admin/reports/searchci') }}">Buscar</a></li>

                    </ul>
                </li>
                <li class="nav-item nav-dropdown">
                    <a class="nav-link nav-dropdown-toggle" href="#">
                        <i class="nav-icon fa fa-file-text"></i> Filtros
                    </a>
                    <ul class="nav-dropdown-items">

                        <li class="nav-item"><a class="nav-link" href="{{ url('admin/reports/report') }}">Filtrados
                                por Selección</a></li>
                       {{--  <li class="nav-item"><a class="nav-link" href="{{ url('admin/reports/reportEtapas') }}">Filtrados
                                por Etapas</a></li> --}}
                        {{--                   <li class="nav-item"><a class="nav-link" href="{{ url('admin/reports/report') }}">EXPORT EXCEl</a></li> --}}
                        <li class="nav-item"><a class="nav-link"
                                href="{{ url('admin/reports/search-names') }}">Filtros por Nombres</a></li>


                    </ul>
                </li>

                <li class="nav-item nav-dropdown">
                    <a class="nav-link nav-dropdown-toggle" href="#">
                        <i class="nav-icon fa fa-plus-circle"></i> Graficos
                    </a>
                    <ul class="nav-dropdown-items">

                        <li class="nav-item"><a class="nav-link"
                                href="{{ url('admin/reports/applicants-by-coop') }}">{{ trans('admin.reports.actions.applicants-by-coop') }}</a>
                        </li>
                        <li class="nav-item"><a class="nav-link"
                                href="{{ url('admin/reports/listado-ejecuciones') }}">Listado de ejecuciones</a></li>
                        <li class="nav-item"><a class="nav-link"
                                href="{{ url('admin/reports/asignaciones_atc') }}">Asignaciones de ATC</a></li>

                    </ul>
                </li>
            @endif
            @if ($roles->contains('ama'))
                <li class="nav-title">{{ trans('brackets/admin-ui::admin.sidebar.settings') }}</li>
                <li class="nav-item nav-dropdown">
                    <a class="nav-link nav-dropdown-toggle" href="#">
                        <i class="nav-icon fa fa-cogs"></i> {{ trans('admin.sidebar.settings.general') }}
                    </a>
                    <ul class="nav-dropdown-items">

                        <li class="nav-item"><a class="nav-link" href="{{ url('admin/construction-entities') }}"><i
                                    class="nav-icon fa fa-truck"></i>
                                {{ trans('admin.construction-entity.title') }}</a></li>
                        <li class="nav-item"><a class="nav-link" href="{{ url('admin/financial-entities') }}"><i
                                    class="nav-icon fa fa-money"></i> {{ trans('admin.financial-entity.title') }}</a>
                        </li>
                        <li class="nav-item"><a class="nav-link" href="{{ url('admin/states') }}"><i
                                    class="nav-icon fa fa-globe"></i> {{ trans('admin.state.title') }}</a></li>
                        <li class="nav-item"><a class="nav-link" href="{{ url('admin/cities') }}"><i
                                    class="nav-icon fa fa-building"></i> {{ trans('admin.city.title') }}</a></li>
                        <li class="nav-item"><a class="nav-link" href="{{ url('admin/contact-methods') }}"><i
                                    class="nav-icon fa fa-compress"></i> {{ trans('admin.contact-method.title') }}</a>
                        </li>
                        <li class="nav-item"><a class="nav-link" href="{{ url('admin/disabilities') }}"><i
                                    class="nav-icon fa fa-wheelchair"></i> {{ trans('admin.disability.title') }}</a>
                        </li>
                        <li class="nav-item"><a class="nav-link" href="{{ url('admin/diseases') }}"><i
                                    class="nav-icon fa fa-medkit"></i> {{ trans('admin.disease.title') }}</a></li>
                        <li class="nav-item"><a class="nav-link" href="{{ url('admin/document-types') }}"><i
                                    class="nav-icon icon-flag"></i> {{ trans('admin.document-type.title') }}</a></li>
                        <li class="nav-item"><a class="nav-link" href="{{ url('admin/education-levels') }}"><i
                                    class="nav-icon fa fa-book"></i> {{ trans('admin.education-level.title') }}</a>
                        </li>
                        <li class="nav-item"><a class="nav-link"
                                href="{{ url('admin/applicant-relationships') }}"><i
                                    class="nav-icon fa fa-retweet"></i>
                                {{ trans('admin.applicant-relationship.title') }}</a></li>
                        <li class="nav-item"><a class="nav-link" href="{{ url('admin/statuses') }}"><i
                                    class="nav-icon fa fa-sitemap"></i> {{ trans('admin.status.title') }}</a></li>
                        <li class="nav-item"><a class="nav-link" href="{{ url('admin/questionnaire-types') }}"><i
                                    class="nav-icon fa fa-file"></i> {{ trans('admin.questionnaire-type.title') }}</a>
                        </li>
                        <li class="nav-item"><a class="nav-link"
                                href="{{ url('admin/questionnaire-templates') }}"><i
                                    class="nav-icon fa fa-file-text"></i>
                                {{ trans('admin.questionnaire-template.title') }}</a></li>
                        <li class="nav-item"><a class="nav-link" href="{{ url('admin/familiar-settings') }}"><i
                                    class="nav-icon icon-puzzle"></i> {{ trans('admin.familiar-setting.title') }}</a>
                        </li>
                        <li class="nav-item"><a class="nav-link" href="{{ url('admin/translations') }}"><i
                                    class="nav-icon icon-location-pin"></i> {{ __('Translations') }}</a></li>

                    </ul>
                </li>
            @endif


            @if ($roles->contains('ama'))
                <li class="nav-title">{{ trans('brackets/admin-ui::admin.sidebar.access') }}</li>
                <li class="nav-item"><a class="nav-link" href="{{ url('admin/admin-users') }}"><i
                            class="nav-icon icon-user"></i> {{ __('Manage access') }}</a></li>
            @endif

            {{-- Do not delete me :) I'm also used for auto-generation menu items --}}
            {{-- <li class="nav-item"><a class="nav-link" href="{{ url('admin/configuration') }}"><i class="nav-icon icon-settings"></i> {{ __('Configuration') }}</a></li> --}}
        </ul>
    </nav>
    <button class="sidebar-minimizer brand-minimizer" type="button"></button>
</div>
