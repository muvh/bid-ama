@extends('brackets/admin-ui::admin.layout.usersys')

@section('title', trans('admin.guest.projects'))

@section('body')

    <div class="card">
        <div class="card-header text-center">
            DATOS DEL SOLICITANTE

            <a class="btn btn-primary btn-spinner btn-sm pull-right m-b-0" href="{{ url()->previous() }}" role="button"><i
                    class="fa fa-undo"></i>&nbsp; {{ trans('admin.guest.actions.back') }}</a>
        </div>

        <div class="card-body">

            <div class="row">
                <div class="form-group col-sm-4">
                    <p class="card-text"><strong>Nombres:</strong> {{ $applicant->names }}</p>
                </div>
                <div class="form-group col-sm-4">
                    <p class="card-text"><strong>Apellidos:</strong> {{ $applicant->last_names }}</p>
                </div>
                <div class="form-group col-sm-4">
                    <p class="card-text"><strong>Documento:</strong> {{ $applicant->government_id }}</p>
                </div>
            </div>

            <div class="row">
                <div class="form-group col-sm-4">
                    <p class="card-text"><strong>Avance:</strong> {{ $avance }} %</p>
                </div>
                <div class="form-group col-sm-4">
                    <p class="card-text"><strong>Estado:</strong>
                        {{ $avance = 100 ? 'CULMINADO' : ($avance < 50 ? 'EJECUCION' : 'INICIAL') }}</p>
                </div>
                <div class="form-group col-sm-4">
                    <p class="card-text"></p>
                </div>
            </div>
            <div class="row">
                <div class="form-group col-sm-4">
                    <p class="card-text"><strong>Fecha Inicio de Obra:</strong>
                        {{ $applicant->workdata ? $applicant->workdata->start_work : '' }}</p>
                </div>
                <div class="form-group col-sm-4">
                    <p class="card-text"><strong>Latitud:</strong>
                        {{ $applicant->workdata ? $applicant->workdata->latitude : '' }}</p>
                </div>
                <div class="form-group col-sm-4">
                    <p class="card-text"><strong>Longitud:</strong>
                        {{ $applicant->workdata ? $applicant->workdata->longitude : '' }}</p>
                </div>
            </div>
            <div class="row">
                <div class="form-group col-sm-4">
                    @if ($applicant->workdata)
                        <a class="btn  btn-success" href="/admin/works-data/{{ $applicant->workdata->id }}/edit"
                            title="Actualizar Datos Obra" role="button"> Actualizar Datos Obra
                        </a>
                    @else
                        <a class="btn  btn-success" href="/admin/applicants/{{ $applicant->id }}/workcreate"
                            title="Actualizar Datos Obra" role="button"> Actualizar Datos Obra
                        </a>
                    @endif

                </div>
            </div>
        </div>
    </div>

    <div class="card">
        <div class="card-header text-center">
            LINEA TEMPORAL
        </div>

        <div class="card-body">
            <grafico :label="{{ json_encode($visitas) }}" :values="{{ json_encode($avances) }}" />
        </div>
    </div>

    <div class="card">
        <div class="card-header text-center">
            UBICACION DEL PROYECTO
            <a class="btn btn-danger btn-sm pull-right m-b-0"
                href="http://maps.google.com/maps/search/?api=1&query={{ $applicant->workdata ? $applicant->workdata->latitude : '-25.2949068' }}%2C{{ $applicant->workdata ? $applicant->workdata->longitude : '-57.6087548' }}&zoom=15&basemap=satellite&layer=transit"
                target="_blank"><i class="fa fa-map-marker"></i> Google Maps</a>
        </div>

        <div class="card-body">
            <googlemap :latitude="{{ $applicant->workdata ? $applicant->workdata->latitude : '-25.2949068' }}"
                :longitude="{{ $applicant->workdata ? $applicant->workdata->longitude : '-57.6087548' }}" />
        </div>
    </div>

    <div class="card">
        <div class="card-header text-center">
            VISITAS
        </div>

        <div class="card-body">
            <visit-listing :data="{{ $data->toJson() }}" :url="'{{ url('admin/visits') }}'" inline-template>

                <div class="row">
                    <div class="col">
                        <div class="card">
                            <div class="card-header">
                                <i class="fa fa-align-justify"></i> {{ trans('admin.visit.actions.index') }}
                                <a class="btn btn-primary btn-spinner btn-sm pull-right m-b-0"
                                    href="{{ url('admin/applicants/' . $applicant->id . '/visits/create') }}"
                                    role="button"><i class="fa fa-plus"></i>&nbsp;
                                    {{ trans('admin.visit.actions.create') }}</a>
                            </div>
                            <div class="card-body" v-cloak>
                                <div class="card-block">
                                    <form @submit.prevent="">
                                        <div class="row justify-content-md-between">
                                            <div class="col col-lg-7 col-xl-5 form-group">
                                                <div class="input-group">
                                                    <input class="form-control"
                                                        placeholder="{{ trans('brackets/admin-ui::admin.placeholder.search') }}"
                                                        v-model="search"
                                                        @keyup.enter="filter('search', $event.target.value)" />
                                                    <span class="input-group-append">
                                                        <button type="button" class="btn btn-primary"
                                                            @click="filter('search', search)"><i
                                                                class="fa fa-search"></i>&nbsp;
                                                            {{ trans('brackets/admin-ui::admin.btn.search') }}</button>
                                                    </span>
                                                </div>
                                            </div>
                                            <div class="col-sm-auto form-group ">
                                                <select class="form-control" v-model="pagination.state.per_page">

                                                    <option value="10">10</option>
                                                    <option value="25">25</option>
                                                    <option value="100">100</option>
                                                </select>
                                            </div>
                                        </div>
                                    </form>

                                    <table class="table table-hover table-listing">
                                        <thead>
                                            <tr>
                                                <th class="bulk-checkbox">
                                                    <input class="form-check-input" id="enabled" type="checkbox"
                                                        v-model="isClickedAll" v-validate="''" data-vv-name="enabled"
                                                        name="enabled_fake_element"
                                                        @click="onBulkItemsClickedAllWithPagination()">
                                                    <label class="form-check-label" for="enabled">
                                                        #
                                                    </label>
                                                </th>

                                                <th is='sortable' :column="'id'">
                                                    {{ trans('admin.visit.columns.id') }}</th>
                                                <th is='sortable' :column="'applicant_id'">
                                                    {{ trans('admin.visit.columns.applicant_id') }}</th>
                                                <th is='sortable' :column="'visit_number'">
                                                    {{ trans('admin.visit.columns.visit_number') }}</th>
                                                <th is='sortable' :column="'advance'">
                                                    {{ trans('admin.visit.columns.advance') }}</th>
                                                <th is='sortable' :column="'latitude'">
                                                    {{ trans('admin.visit.columns.latitude') }}</th>
                                                <th is='sortable' :column="'longitude'">
                                                    {{ trans('admin.visit.columns.longitude') }}</th>
                                                <th is='sortable' :column="'visit_date'">
                                                    {{ trans('admin.visit.columns.visit_date') }}</th>

                                                <th></th>
                                            </tr>
                                            <tr v-show="(clickedBulkItemsCount > 0) || isClickedAll">
                                                <td class="bg-bulk-info d-table-cell text-center" colspan="9">
                                                    <span
                                                        class="align-middle font-weight-light text-dark">{{ trans('brackets/admin-ui::admin.listing.selected_items') }}
                                                        @{{ clickedBulkItemsCount }}. <a href="#" class="text-primary"
                                                            @click="onBulkItemsClickedAll('/admin/visits')"
                                                            v-if="(clickedBulkItemsCount < pagination.state.total)"> <i
                                                                class="fa"
                                                                :class="bulkCheckingAllLoader ? 'fa-spinner' : ''"></i>
                                                            {{ trans('brackets/admin-ui::admin.listing.check_all_items') }}
                                                            @{{ pagination.state.total }}</a> <span class="text-primary">|</span>
                                                        <a href="#" class="text-primary"
                                                            @click="onBulkItemsClickedAllUncheck()">{{ trans('brackets/admin-ui::admin.listing.uncheck_all_items') }}</a>
                                                    </span>

                                                    <span class="pull-right pr-2">
                                                        <button class="btn btn-sm btn-danger pr-3 pl-3"
                                                            @click="bulkDelete('/admin/visits/bulk-destroy')">{{ trans('brackets/admin-ui::admin.btn.delete') }}</button>
                                                    </span>

                                                </td>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr v-for="(item, index) in collection" :key="item.id"
                                                :class="bulkItems[item.id] ? 'bg-bulk' : ''">
                                                <td class="bulk-checkbox">
                                                    <input class="form-check-input" :id="'enabled' + item.id"
                                                        type="checkbox" v-model="bulkItems[item.id]" v-validate="''"
                                                        :data-vv-name="'enabled' + item.id"
                                                        :name="'enabled' + item.id + '_fake_element'"
                                                        @click="onBulkItemClicked(item.id)"
                                                        :disabled="bulkCheckingAllLoader">
                                                    <label class="form-check-label" :for="'enabled' + item.id">
                                                    </label>
                                                </td>

                                                <td>@{{ item.id }}</td>
                                                <td>@{{ item.applicant_id }}</td>
                                                <td>@{{ item.visit_number }}</td>
                                                <td>@{{ item.advance }}</td>
                                                <td>@{{ item.latitude }}</td>
                                                <td>@{{ item.longitude }}</td>
                                                <td>@{{ item.visit_date | datetime }}</td>

                                                <td>
                                                    <div class="row no-gutters">
                                                        <div class="col-auto">
                                                            <a class="btn btn-sm btn-spinner btn-info"
                                                                :href="item.resource_url + '/edit'"
                                                                title="{{ trans('brackets/admin-ui::admin.btn.edit') }}"
                                                                role="button"><i class="fa fa-edit"></i></a>
                                                        </div>
                                                        <form class="col"
                                                            @submit.prevent="deleteItem(item.resource_url)">
                                                            <button type="submit" class="btn btn-sm btn-danger"
                                                                title="{{ trans('brackets/admin-ui::admin.btn.delete') }}"><i
                                                                    class="fa fa-trash-o"></i></button>
                                                        </form>
                                                    </div>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>

                                    <div class="row" v-if="pagination.state.total > 0">
                                        <div class="col-sm">
                                            <span
                                                class="pagination-caption">{{ trans('brackets/admin-ui::admin.pagination.overview') }}</span>
                                        </div>
                                        <div class="col-sm-auto">
                                            <pagination></pagination>
                                        </div>
                                    </div>

                                    <div class="no-items-found" v-if="!collection.length > 0">
                                        <i class="icon-magnifier"></i>
                                        <h3>{{ trans('brackets/admin-ui::admin.index.no_items') }}</h3>
                                        <p>{{ trans('brackets/admin-ui::admin.index.try_changing_items') }}</p>
                                        <a class="btn btn-primary btn-spinner" href="{{ url('admin/visits/create') }}"
                                            role="button"><i class="fa fa-plus"></i>&nbsp;
                                            {{ trans('admin.visit.actions.create') }}</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </visit-listing>
        </div>
    </div>



    {{-- <div class="row">
        <div class="col">
            <div class="card">
                <div class="card-header">
                    <i class="fa fa-align-justify"></i> {{ trans('admin.guest.actions.syncmuvh') }}
                </div>
                <div class="card-body" v-cloak>
                    <!--<div class="card-block">-->
                    <table class="table table-sm table-hover table-borderless">
                        <thead>
                            <tr>

                                <th>{{ trans('admin.visit.columns.id') }}</th>
                                <th>{{ trans('admin.visit.columns.visit_number') }}</th>
                                <th>{{ trans('admin.visit.columns.advance') }}</th>
                                <th>{{ trans('admin.visit.columns.visit_date') }}</th>
                                <th>{{ trans('admin.visit.columns.latitude') }}</th>
                                <th>{{ trans('admin.visit.columns.longitude') }}</th>
                                <th>{{ trans('admin.visit.columns.visitid') }}</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($visitas as $key => $item)
                                <tr>
                                    <td>{{ $item->id }}</td>
                                    <td>{{ $item->visit_number }}</td>
                                    <td>{{ $item->advance }}</td>
                                    <td>{{ $item->visit_date }}</td>
                                    <td>{{ $item->latitude }}</td>
                                    <td>{{ $item->longitude }}</td>
                                    <td>{{ $item->visit_id }}</td>
                                    <td>
                                        <div class="col-auto">
                                            <a class="btn btn-sm btn-spinner btn-info" :href="item.resource_url + '/edit'"
                                                title="{{ trans('brackets/admin-ui::admin.btn.edit') }}" role="button"><i
                                                    class="fa fa-edit"></i></a>
                                        </div>
                                        <div class="row no-gutters">
                                            <div class="col-auto">
                                                <a class="btn btn-sm btn-spinner btn-info"
                                                    href="{{ url('admin/visits/' . $item->id . '/show') }}"
                                                    title="{{ trans('brackets/admin-ui::admin.btn.show') }}"
                                                    role="button"><i class="fa fa-camera"></i></a>
                                            </div>
                                        </div>
                                    </td>
                                    {{-- <td>
                                        <div class="row no-gutters">
                                            <div class="col-auto">
                                                <a class="btn btn-sm btn-spinner btn-info" href="{{ url('admin/visits/' . $project->SEOBId . '/' . $item->visit_id . '/syncimage') }}"
                                                    role="button"><i class="fa fa-camera"></i></a>
                                            </div>
                                        </div>
                                    </td>
    </tr>
    @endforeach
    </tbody>
    </table>
    </div>
    </div>
    </div>
    </div> --}}




@endsection
