{{-- <div class="form-group row align-items-center"
    :class="{ 'has-danger': errors.has('applicant_id'), 'has-success': fields.applicant_id && fields.applicant_id.valid }">
    <label for="applicant_id" class="col-form-label text-md-right"
        :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.visit.columns.applicant_id') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.applicant_id" v-validate="'required'" @input="validate($event)"
            class="form-control"
            :class="{
                'form-control-danger': errors.has('applicant_id'),
                'form-control-success': fields.applicant_id && fields
                    .applicant_id.valid
            }"
            id="applicant_id" name="applicant_id" placeholder="{{ trans('admin.visit.columns.applicant_id') }}">
        <div v-if="errors.has('applicant_id')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('applicant_id') }}
        </div>
    </div>
</div> --}}

<div class="form-group row align-items-center"
    :class="{ 'has-danger': errors.has('visit_number'), 'has-success': fields.visit_number && fields.visit_number.valid }">
    <label for="visit_number" class="col-form-label text-md-right"
        :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.visit.columns.visit_number') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.visit_number" v-validate="'required'" @input="validate($event)"
            class="form-control"
            :class="{
                'form-control-danger': errors.has('visit_number'),
                'form-control-success': fields.visit_number && fields
                    .visit_number.valid
            }"
            id="visit_number" name="visit_number" placeholder="{{ trans('admin.visit.columns.visit_number') }}">
        <div v-if="errors.has('visit_number')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('visit_number') }}
        </div>
    </div>
</div>

<div class="form-group row align-items-center"
    :class="{ 'has-danger': errors.has('advance'), 'has-success': fields.advance && fields.advance.valid }">
    <label for="advance" class="col-form-label text-md-right"
        :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.visit.columns.advance') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <select readonly class="form-control" v-model="form.advance" name="marital_status" id="">
            <option value="5">5</option>
            <option value="10">10</option>
            <option value="10">15</option>
            <option value="20">20</option>
            <option value="25">25</option>
            <option value="30">30</option>
            <option value="35">35</option>
            <option value="40">40</option>
            <option value="45">45</option>
            <option value="50">50</option>
            <option value="55">55</option>
            <option value="60">60</option>
            <option value="65">65</option>
            <option value="70">70</option>
            <option value="75">75</option>
            <option value="80">80</option>
            <option value="85">85</option>
            <option value="90">90</option>
            <option value="95">95</option>
            <option value="100">100</option>
        </select>
        {{-- <input type="text" v-model="form.advance" v-validate="'required'" @input="validate($event)"
        class="form-control" :class="{'form-control-danger': errors.has('advance'), 'form-control-success': fields.advance && fields.advance.valid}"
        id="advance" name="advance" placeholder="{{ trans('admin.visit.columns.advance') }}"> --}}
        <div v-if="errors.has('advance')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('advance') }}</div>
    </div>
</div>

<div class="form-group row align-items-center"
    :class="{ 'has-danger': errors.has('latitude'), 'has-success': fields.latitude && fields.latitude.valid }">
    <label for="latitude" class="col-form-label text-md-right"
        :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.visit.columns.latitude') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.latitude" v-validate="''" @input="validate($event)" class="form-control"
            :class="{
                'form-control-danger': errors.has('latitude'),
                'form-control-success': fields.latitude && fields.latitude
                    .valid
            }"
            id="latitude" name="latitude" placeholder="{{ trans('admin.visit.columns.latitude') }}">
        <div v-if="errors.has('latitude')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('latitude') }}</div>
    </div>
</div>

<div class="form-group row align-items-center"
    :class="{ 'has-danger': errors.has('longitude'), 'has-success': fields.longitude && fields.longitude.valid }">
    <label for="longitude" class="col-form-label text-md-right"
        :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.visit.columns.longitude') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.longitude" v-validate="''" @input="validate($event)" class="form-control"
            :class="{
                'form-control-danger': errors.has('longitude'),
                'form-control-success': fields.longitude && fields
                    .longitude.valid
            }"
            id="longitude" name="longitude" placeholder="{{ trans('admin.visit.columns.longitude') }}">
        <div v-if="errors.has('longitude')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('longitude') }}</div>
    </div>
</div>

<div class="form-group row align-items-center"
    :class="{ 'has-danger': errors.has('visit_date'), 'has-success': fields.visit_date && fields.visit_date.valid }">
    <label for="visit_date" class="col-form-label text-md-right"
        :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.visit.columns.visit_date') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <div class="input-group input-group--custom">
            <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
            <datetime v-model="form.visit_date" :config="datetimePickerConfig"
                v-validate="'required|date_format:yyyy-MM-dd HH:mm:ss'" class="flatpickr"
                :class="{
                    'form-control-danger': errors.has('visit_date'),
                    'form-control-success': fields.visit_date && fields
                        .visit_date.valid
                }"
                id="visit_date" name="visit_date"
                placeholder="{{ trans('brackets/admin-ui::admin.forms.select_date_and_time') }}"></datetime>
        </div>
        <div v-if="errors.has('visit_date')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('visit_date') }}
        </div>
    </div>
</div>


<div class="form-group row align-items-center"
    :class="{ 'has-danger': errors.has('footnote'), 'has-success': fields.footnote && fields.footnote.valid }">
    <label for="end" class="col-form-label text-md-right"
        :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.call.columns.footnote') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <!--<div class="input-group input-group--custom">
            <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
            <datetime v-model="form.end" :config="datetimePickerConfig" v-validate="'required'" class="flatpickr" :class="{ 'form-control-danger': errors.has('end'), 'form-control-success': fields.end && fields.end.valid }" id="end" name="end" placeholder="{{ trans('brackets/admin-ui::admin.forms.select_date_and_time') }}"></datetime>
        </div> -->
        <wysiwyg v-model="form.footnote" v-validate="'required'" id="text" name="text"
            :config="mediaWysiwygConfig" />
        <div v-if="errors.has('footnote')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('footnote') }}</div>
    </div>

</div>
