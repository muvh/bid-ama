<div class="form-group row align-items-center" :class="{'has-danger': errors.has('nro_solictud_rt'), 'has-success': fields.nro_solictud_rt && fields.nro_solictud_rt.valid }">
    <label for="nro_solictud_rt" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.resource-transfer.columns.nro_solictud_rt') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.nro_solictud_rt" v-validate="'required'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('nro_solictud_rt'), 'form-control-success': fields.nro_solictud_rt && fields.nro_solictud_rt.valid}" id="nro_solictud_rt" name="nro_solictud_rt" placeholder="{{ trans('admin.resource-transfer.columns.nro_solictud_rt') }}">
        <div v-if="errors.has('nro_solictud_rt')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('nro_solictud_rt') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('fecha_rt'), 'has-success': fields.fecha_rt && fields.fecha_rt.valid }">
    <label for="fecha_rt" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.resource-transfer.columns.fecha_rt') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-sm-8'">
        <div class="input-group input-group--custom">
            <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
            <datetime v-model="form.fecha_rt" :config="datePickerConfig"  class="flatpickr" :class="{'form-control-danger': errors.has('fecha_rt'), 'form-control-success': fields.fecha_rt && fields.fecha_rt.valid}" id="fecha_rt" name="fecha_rt" placeholder="{{ trans('brackets/admin-ui::admin.forms.select_a_date') }}"></datetime>
        </div>
        <div v-if="errors.has('fecha_rt')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('fecha_rt') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('type_order_id'), 'has-success': fields.type_order_id && fields.type_order_id.valid }">
    <label for="type_order_id" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.resource-transfer.columns.type_order_id') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
            
            <multiselect
            v-model="form.type_order"
            :options="{{$typeorders->toJson()}}"
            :multiple="false"
            track-by="id"
            label="title"
            :taggable="true"
            tag-placeholder=""
            placeholder=""
           >
        <div v-if="errors.has('type_order_id')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('type_order_id') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('construction_entity_id'), 'has-success': fields.construction_entity_id && fields.construction_entity_id.valid }">
    <label for="construction_entity_id" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.resource-transfer.columns.construction_entity_id') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
            <multiselect
        v-model="form.construction_entity"
        :options="{{$constructions->toJson()}}"
        :multiple="false"
        track-by="id"
        label="name"
        :taggable="true"
        tag-placeholder=""
        placeholder=""
       >
    </multiselect>
        <div v-if="errors.has('construction_entity_id')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('construction_entity_id') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('nro_nota_bid'), 'has-success': fields.nro_nota_bid && fields.nro_nota_bid.valid }">
    <label for="nro_nota_bid" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.resource-transfer.columns.nro_nota_bid') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.nro_nota_bid" v-validate="'required'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('nro_nota_bid'), 'form-control-success': fields.nro_nota_bid && fields.nro_nota_bid.valid}" id="nro_nota_bid" name="nro_nota_bid" placeholder="{{ trans('admin.resource-transfer.columns.nro_nota_bid') }}">
        <div v-if="errors.has('nro_nota_bid')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('nro_nota_bid') }}</div>
    </div>
</div>

