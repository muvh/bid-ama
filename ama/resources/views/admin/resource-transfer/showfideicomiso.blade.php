
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
  <header>
    <img src="{{ public_path('logo-cab-transf-recursos.jpg') }}" >
  </header>
  <footer>
    <img src="{{ public_path('logo-pie-transf-recursos.png') }}" width=" 750px" >
</footer>
<main>
    <style type="text/css">

        .tg  {border-collapse:collapse;border-spacing:0;width:75%;margin: -10px 120px}
        .tg td{border-color:black;border-style:solid;border-width:1px;font-family:Calibri;font-size:10px;
          overflow:hidden;padding:10px 5px;word-break:normal;}
        .tg th{border-color:black;border-style:solid;border-width:1px;font-family:Calibri;font-size:10x;
          font-weight:normal;overflow:hidden;padding:10px 5px;word-break:normal;}
        .tg .tg-0pky{border-color:inherit;text-align:left;vertical-align:top}
        .tg .tg-za14{border-color:inherit;text-align:center;vertical-align:bottom}
        header {
                position: fixed;
                top: -30px;
                left: 90px;
                right: 0px;
                height: 50px;
                font-size: 20px !important;

    
            }
         
            footer {
                position: fixed; 
                bottom: 40px; 
                left: 0px; 
                right: 0px;
                height: 50px; 
                font-size: 20px !important;
            }
            .noBorder {
                border:none !important;
            }
        </style>
      
<br>
<br>
        <h5 style="text-align: center; margin: 20px 10px">FORMULARIO DE ORDEN DE TRANSFERENCIA DE RECURSOS</h5>

        <table class="tg">
        <thead>
          <tr>
            <td  style="text-align: center" class="tg-0pky" colspan="4"> {{$resourceTransfer->typeorder->title}}</td>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td class="tg-0pky">Entidad:</td>
            <td class="tg-0pky" colspan="3">Ministerio de Urbanismo, Vivienda y Hábitat (MUVH)</td>
          </tr>
          <tr>
            <td class="tg-0pky">Nro. de Resolucion</td>
            <td class="tg-0pky">{{$applicant[0]->applicant->resolution_number}}</td>
            <td class="tg-0pky">Fecha de Emisión</td>
            <td class="tg-0pky">{{empty($applicant[0]->applicant->resolution_date)?'':date('d/m/Y', strtotime($applicant[0]->applicant->resolution_date))}}</td>
           
          </tr>
          <tr>
            <td class="tg-0pky">Nro. de Solicitud</td>
            <td class="tg-0pky">{{$resourceTransfer->nro_solictud_rt}}</td>
            <td class="tg-0pky">Fecha de Emisión</td>
            <td class="tg-0pky">{{empty($resourceTransfer->fecha_rt)?'':date('d/m/Y', strtotime($resourceTransfer->fecha_rt))}}</td>
           
          </tr>
          <tr>
            <td class="tg-0pky"  style="text-align: center" colspan="4"> <b> Datos para la Transferencia</b></td>
          </tr>
          <tr>
            <td class="tg-0pky">ATC:</td>
            <td class="tg-0pky" colspan="3">{{$resourceTransfer->construction->razon_social}} </td>
          </tr>
          <tr>
            <td class="tg-0pky">RUC:</td>
            <td class="tg-0pky" colspan="3">{{$resourceTransfer->construction->const_ruc}}</td>
          </tr>

          <tr>
            <td class="tg-0pky">Nro. de Cta. Bancaria:</td>
            <td class="tg-0pky" colspan="3">{{$resourceTransfer->construction->cta_cte}}</td>
          </tr>
          <tr>
            <td class="tg-0pky">Banco:</td>
            <td class="tg-0pky" colspan="3">{{$resourceTransfer->construction->bank_name}}</td>
          </tr>
          <tr>
            <td class="tg-0pky"  style="text-align: center" colspan="3"><b>Concepto de la Transferencia</b></td>
            <td class="tg-0pky" style="text-align: center"><b>Importe Guaranies</b></td>
          </tr>
          <tr>
            <td class="tg-0pky" colspan="3">
              @if($resourceTransfer->type_order_id==2)
              Pago Final 20% del Costo del Proyecto
              @else
              1er Desembolso 80% del Costo Total del Proyecto
              @endif
              <table style="border: none" cellspacing="0" cellpadding="0">
       
                <tbody>
                  @php
                  $contador=0;
              @endphp
              @foreach ($applicant as $item) 
              @php
                  $contador=$contador+1;
              @endphp
                  <tr>           
              <td class="noBorder">{{$item->applicant->names}} &nbsp; {{$item->applicant->last_names}}</td>
              <td class="noBorder">CI N° {{$item->applicant->government_id}}</td>
              <td class="noBorder"> @if($resourceTransfer->type_order_id==2)
                
                          @if($contador==$applicant->count())
                          <u> Gs. 7.000.000 </u>
                        
                          @else
                          Gs. 7.000.000
                          @endif

                @else
                

                            @if($contador==$applicant->count())
                            <u> Gs. 28.000.000 </u>
                          
                            @else
                            Gs. 28.000.000
                            @endif
                @endif
              </td>
            </tr>
              @endforeach
          </tbody>
        </table>
            </td>
            <td  class="tg-za14" > Gs. {{number_format($mount,0, '.', '.')}}</td>
          </tr>
          <tr>
            <td class="tg-0pky" colspan="4">IMPORTE EN LETRAS: GUARANÍES {{$numeroTexto}}</td>
          </tr>
        </tbody>
        </table>
     
      
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
    
    
   
        <style type="text/css">
          .tg2  {border-collapse:collapse;border-spacing:0;width:75%;margin: -10px 120px}
          .tg2 td{border-color:black;border-style:solid;border-width:1px;font-family:Calibri;font-size:12px;
            overflow:hidden;padding:10px 5px;word-break:normal;}
          .tg2 th{border-color:black;border-style:solid;border-width:1px;font-family:Calibri;font-size:12px;
            font-weight:normal;overflow:hidden;padding:10px 5px;word-break:normal;}
          .tg2 .tg2-nx8p{font-size:12px;text-align:left;vertical-align:top}
          .tg2 .tg2-13pz{font-size:12px;text-align:center;vertical-align:top}
          </style>
          <div style="margin: 0 auto;">
          <table class="tg2">
          <thead>
            <tr>
              <th class="tg2-nx8p"><br><br><br></th>
              <th class="tg2-nx8p"><br><br><br></th>

              
            </tr>
          </thead>
          <tbody>
            <tr>
              <td class="tg2-13pz">&nbsp;<br>Dirección General de la <br>UEP - BID</td>
              <td class="tg2-13pz">&nbsp;<br>Dirección General de <br>Administración y Finanzas</td>
              
            </tr>
          </tbody>
          </table>
        </main>
</body>
</html>