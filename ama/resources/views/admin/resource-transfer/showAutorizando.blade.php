
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
  <header>
    <img src="{{ public_path('logo-cab-transf-recursos.jpg') }}" >
  </header>
  <footer>
    <img src="{{ public_path('logo-pie-transf-recursos.png') }}" width=" 750px" >
</footer>
<main>
    <style type="text/css">

        main div p {font-size: 14px}
        header {
                position: fixed;
                top: -30px;
                left: 90px;
                right: 0px;
                height: 50px;
                font-size: 20px !important;
            }
         
            footer {
                position: fixed; 
                bottom: 40px; 
                left: 0px; 
                right: 0px;
                height: 50px; 
                font-size: 20px !important;
            }
            .noBorder {
                border:none !important;
            }
        </style>
      
<br>
<br>
<div style="font-family: Calibri; text-align: justify; margin: 20px 50px">
@php
    $meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
    
    $fecha1 = date_create();
    $fecha2 = date_create(Carbon::parse($resourceTransfer->fecha_rt)->format('Y-m-d') );
    $dias = date_diff($fecha2, $fecha1)->format('%R%a');

@endphp

        <h4 style="text-align: center; margin: 20px 10px">DIRECCIÓN GENERAL UNIDAD EJECUTORA DEL PROGRAMA BID</h4>

      {{-- {{date('d', $resourceTransfer->fecha_rt)}} --}}
       {{--  <p>{{ \Carbon\Carbon::parse($resourceTransfer->fecha_rt)->format('j F, Y') }}</p> --}}
        <p style="text-align: right"> Asunción {{date('d', strtotime($resourceTransfer->fecha_rt))}} de {{$meses[date('m', strtotime($resourceTransfer->fecha_rt))-1]}} del {{date('Y', strtotime($resourceTransfer->fecha_rt))}} </p>
        {{-- <p style="text-align: right">Asuncion {{ \Carbon\Carbon::parse($resourceTransfer->created_at)->format('d')}} de {{$meses[date('n')-1]}} del {{date('Y')}}</p> --}}
        <p>
          
          @if($dias > 1)
          NOTA AMA BID 
          @else
          NCFAM BID
          @endif
          
          {{$resourceTransfer->nro_nota_bid}}</p>
        <br>
        <br>
        <P><b>Señores</b></P>
        <P><b>{{$applicant[0]->applicant->financial->razon_social}}</b></P>
        <P><b><u>PRESENTE</u></b></P>
        <br>
        <p>De nuestra mayor consideracion:</p>
        <br>
        <p style="text-indent: 3em">Comunicamos que la empresa <b>{{$resourceTransfer->construction->razon_social}}</b>, habilitada como Servicio de Asistencia Técnica (ATC)
          ha cumplido con los requisitos administrativos por tanto autorizamos el desembolso del 
          @if($resourceTransfer->type_order_id==2)
           20% del Costo del Proyecto
          @else
           80% del Costo Total del Proyecto
          @endif
    
          por beneficiario.
        </p>
        <br>
        <p><b> <u>Datos del ATC:</u> </b></p>
        <br>
        <p>
        Banco: {{$resourceTransfer->construction->bank_name}} <br>
        Cuenta N°: {{$resourceTransfer->construction->cta_cte}}  <br>
        RUC: {{$resourceTransfer->construction->const_ruc}} <br>
        Representante Legal:  {{$resourceTransfer->construction->repre}} <br>
        Se Adjunta Formulario de Orden de Transferencia N°  {{$resourceTransfer->nro_solictud_rt}} </p>
      </div>
      </main>
      
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>    
        <style type="text/css">
          .tg2  {border-collapse:none;border-spacing:0;width:80%;margin: 0px 80px}
          .tg2 td{border-color:black;border-style:none;border-width:1px;font-family:Calibri;font-size:16px;
            overflow:hidden;padding:10px 5px;word-break:normal;}
          .tg2 th{border-color:black;border-style:none;border-width:1px;font-family:Calibri;font-size:16px;
            font-weight:normal;overflow:hidden;padding:10px 5px;word-break:normal;}
          .tg2 .tg2-nx8p{font-size:16px;text-align:left;vertical-align:top}
          .tg2 .tg2-13pz{font-size:16px;text-align:center;vertical-align:top}
          </style>
          <div style="margin: 0 auto;">
          <table class="tg2">
          <thead>
            <tr>
              <th class="tg2-nx8p"><br><br><br></th>
              <th class="tg2-nx8p"><br><br><br></th>

              
            </tr>
          </thead>
          <tbody>
            <tr>
              <td class="tg2-13pz">&nbsp;<br>Dirección General de la <br>UEP - BID</td>
              <td class="tg2-13pz">&nbsp;<br>Dirección General de <br>Administración y Finanzas</td>
              
            </tr>
          </tbody>
          </table>
      
</body>
</html>