@extends('brackets/admin-ui::admin.layout.default')

@section('title', trans('admin.resource-transfer.actions.edit', ['name' => $resourceTransfer->id]))

@section('body')

    <div class="container-xl">
        <div class="card">

            <resource-transfer-form
                :action="'{{ $resourceTransfer->resource_url }}'"
                :data="{{ $resourceTransfer->toJson() }}"
                :resourcetransfer="{{$resourceTransfer}}"
                v-cloak
                inline-template>
            
                <form class="form-horizontal form-edit" method="post" @submit.prevent="onSubmit" :action="action" novalidate>


                    <div class="card-header">
                        <i class="fa fa-pencil"></i> {{ trans('admin.resource-transfer.actions.edit', ['name' => $resourceTransfer->id]) }}
                    </div>

                    <div class="card-body">
                        @include('admin.resource-transfer.components.form-elements-applicant')
                        <button type="button" class=" float-righ btn btn-primary" data-toggle="modal" data-target="#exampleModal2">
                            <i class="fa fa-pencil"></i>&nbsp; Editar
                        </button>
                    </div>
                    
                    
                    <div class="card-footer">
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
                            <i class="fa fa-plus"></i>&nbsp; Otorgar Subsidio
                        </button>
                        
                           
                        
                    </div>
                    
                </form>

        </resource-transfer-form>
       {{$resourceTransfer->culminada}}
        <table class="table table-hover table-listing">
            <thead>
                    <tr>
              <th>ID</th>
              <th>Nombre</th>
              <th>Monto</th>
              <th>Acciones</th>
            </tr>
            </thead>
            <tbody>
                @foreach ($apprestra as $list)
            <tr>
              <td>{{$list->applicant_id }}</td>
              <td>{{$list->applicant->names }} {{$list->applicant->last_names}}</td>
              @if($resourceTransfer->type_order_id===1)
              <td>28.000.000</td>
              @else
              <td>7.000.000</td>
              @endif
              <td>  
               
                
                <form class="col" action="/admin/applicant-resource-transfers/{{$list->id}}" method="post">
                    <input type="hidden" name="_method" value="delete" />
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <button type="submit" class="btn btn-sm btn-danger" title="{{ trans('brackets/admin-ui::admin.btn.delete') }}"><i class="fa fa-trash-o"></i></button>
                </form>
                
            </td>
            </tr>
            @endforeach 
            </tbody>
          </table>
          @if($resourceTransfer->culminado==false)
          <div class="text-right">
              <a class="btn btn btn-success" href='{{$resourceTransfer->resource_url}}/showfideicomiso' title="" role="button"><i class="fa fa-arrow-circle-o-right"></i>
                  Generar Doc
              </a>
          </div>
          @endif
        </div>
    
</div>


<!-- Modal 1 -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">

        </div>
        <resource-transfer-modal
        :action="'{{ url('admin/resource-transfers/add-applicant') }}'"
        :data="{{ $resourceTransfer->toJson() }}"
        :resourcetransfer="{{$resourceTransfer}}"
        v-cloak
        inline-template>
         <form class="form-horizontal form-create" method="post"  action="/../admin/resource-transfers/add-applicant/{{$resourceTransfer->id}}" >
            <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                <div class="modal-body">                 
                    <div class="row">
                        <div class="form-group">
                        <label for="">Nro de Cedula</label>
                                <div class="col-md-9 col-xl-10">
                                    <div class="input-group-append">
                                        <input type="text" required v-model="form.government_id" v-validate="'required'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('government_id'), 'form-control-success': fields.government_id && fields.government_id.valid}" id="government_id" name="government_id" placeholder="{{ trans('admin.applicant.columns.government_id') }}">
                                        <div class="input-group-append">
                                            <button @click="findApplicant" class="btn btn-info" type="button"><i class="fa fa-search"></i></button>
                                        </div>
                                        <div v-if="errors.has('government_id')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('government_id') }}</div>
                                    </div>
                                </div>
                                
                            </div>
                            <div v-if="listApplicant">
                                <h3>Nombres: @{{listApplicant.names}} @{{listApplicant.last_names}}</h3>
                                <h3>Ci.Nro: @{{listApplicant.government_id}} </h3>
                            </div>
                            
                        </div>
                    </div>
                    <input type="hidden" v-model="resource_transfer_id" id="resource_transfer_id" name="resource_transfer_id" >
                    <input type="hidden" v-model="applicant_id" id="applicant_id" name="applicant_id" >
                <div class="modal-footer">
                    <button id="btn-guardar" type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    <button v-if="listApplicant.id" id="btn-guardar" type="submit" class="btn btn-primary">Guardar</button>
                </div>
        </form> 
       
            
      

        </resource-transfer-modal>
        <notifications position="top center" group="auth"/>
      </div>
    </div>
  </div>
  {{-- Fin modal 1  --}}
  <!-- Modal 2 -->
<div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
        </div>
        <resource-transfer-modal
        :action="'{{ url('admin/resource-transfers/add-applicant') }}'"
        :data="{{ $resourceTransfer->toJson() }}"
        :resourcetransfer="{{$resourceTransfer}}"
        v-cloak
        inline-template>
        <form class="form-horizontal form-edit" method="post" action="/../admin/resource-transfers/editresource/{{$resourceTransfer->id}}">
            {{-- <form class="form-horizontal form-create" method="post" action="{{ url('admin/applicants/'.$applicant->id.'/form-status') }}"> --}}
                <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">

            <div class="card-header">
                <i class="fa fa-pencil"></i> {{ trans('admin.resource-transfer.actions.edit', ['name' => $resourceTransfer->id]) }}
            </div>

            <div class="card-body">
                @include('admin.resource-transfer.components.form-elements')
                <button type="submit" class=" float-righ btn btn-primary" data-toggle="modal" data-target="#exampleModal">
                    <i class="fa fa-pencil"></i>&nbsp; Guardar
                </button>
            </div>
            
            
            <div class="modal-footer">
                <button id="btn-guardar" type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                
                   
                
            </div>
        </form>
    </resource-transfer-modal>
      </div>
    </div>
</div>

<!-- modal status 3-->
<div class="modal fade" id="exampleModal3" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel3" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
        </div>
        <resource-transfer-form
        :action="'{{ url('admin/resource-transfers/'.$resourceTransfer->id.'/addobs') }}'"
        :data="{{ $resourceTransfer->toJson() }}"
        :resourcetransfer="{{$resourceTransfer}}"
        v-cloak
        inline-template>
        {{-- <form class="form-horizontal form-edit" method="post" @submit.prevent="onSubmit" :action="action" novalidate> --}}
          <form class="form-horizontal form-edit" method="post" action="/admin/resource-transfers/addobs/{{$resourceTransfer->id}}"> 
            {{-- <form class="form-horizontal form-create" method="post" action="{{ url('admin/applicants/'.$applicant->id.'/form-status') }}"> --}}
                <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">

            <div class="card-header">
                <i class="fa fa-pencil"></i> Cargar Motivo de anulación
            </div>

            <div class="card-body">
                <div class="form-group row align-items-center" :class="{'has-danger': errors.has('obs'), 'has-success': fields.obs && fields.obs.valid }">
                    <label for="obs" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">Observaciones</label>
                        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
                            <textarea  style="background-color: #e4e7ea;" name="obs" rows="5" cols="50" v-model="form.obs"></textarea>
                        <div v-if="errors.has('obs')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('obs') }}</div>
                    </div>
                </div>
                <button type="submit" class=" float-righ btn btn-primary" data-toggle="modal" data-target="#exampleModalLabel3">
                    <i class="fa fa-pencil"></i>&nbsp; Guardar
                </button>
            </div>
            
            
            <div class="modal-footer">
                <button id="btn-guardar" type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                
                   
                
            </div>
        </form>
    </resource-transfer-form>
      </div>
    </div>
</div>

<!-- modal status 3-->





@endsection