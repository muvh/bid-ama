@extends('brackets/admin-ui::admin.layout.default')

@section('title', trans('admin.resource-transfer.actions.create'))

@section('body')

    <div class="container-xl">

                <div class="card">
        
        <resource-transfer-form
            :action="'{{ url('admin/resource-transfers') }}'"
            v-cloak
            inline-template>

            <form class="form-horizontal form-create" method="post" @submit.prevent="onSubmit" :action="action" novalidate>
                
                <div class="card-header">
                    <i class="fa fa-plus"></i> {{ trans('admin.resource-transfer.actions.create') }}
                </div>

                <div class="card-body">
                    @include('admin.resource-transfer.components.form-elements-create')
                </div>
                                
                <div class="card-footer">
                    <button id="btn-guardar" type="submit" class="btn btn-primary" :disabled="submiting">
                        <i class="fa" :class="submiting ? 'fa-spinner' : 'fa-download'"></i>
                        {{ trans('brackets/admin-ui::admin.btn.save') }}
                    </button>
                </div>
                
            </form>

        </resource-transfer-form>

        </div>

        </div>

    
@endsection