@extends('brackets/admin-ui::admin.layout.default')

@section('title', trans('admin.applicant-resource-transfer.actions.edit', ['name' => $applicantResourceTransfer->id]))

@section('body')

    <div class="container-xl">
        <div class="card">

            <applicant-resource-transfer-form
                :action="'{{ $applicantResourceTransfer->resource_url }}'"
                :data="{{ $applicantResourceTransfer->toJson() }}"
                v-cloak
                inline-template>
            
                <form class="form-horizontal form-edit" method="post" @submit.prevent="onSubmit" :action="action" novalidate>


                    <div class="card-header">
                        <i class="fa fa-pencil"></i> {{ trans('admin.applicant-resource-transfer.actions.edit', ['name' => $applicantResourceTransfer->id]) }}
                    </div>

                    <div class="card-body">
                        @include('admin.applicant-resource-transfer.components.form-elements')
                    </div>
                    
                    
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary" :disabled="submiting">
                            <i class="fa" :class="submiting ? 'fa-spinner' : 'fa-download'"></i>
                            {{ trans('brackets/admin-ui::admin.btn.save') }}
                        </button>
                    </div>
                    
                </form>

        </applicant-resource-transfer-form>

        </div>
    
</div>

@endsection