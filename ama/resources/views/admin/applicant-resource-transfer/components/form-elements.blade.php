<div class="form-group row align-items-center" :class="{'has-danger': errors.has('applicant_id'), 'has-success': fields.applicant_id && fields.applicant_id.valid }">
    <label for="applicant_id" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.applicant-resource-transfer.columns.applicant_id') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.applicant_id" v-validate="'required'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('applicant_id'), 'form-control-success': fields.applicant_id && fields.applicant_id.valid}" id="applicant_id" name="applicant_id" placeholder="{{ trans('admin.applicant-resource-transfer.columns.applicant_id') }}">
        <div v-if="errors.has('applicant_id')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('applicant_id') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('resource_transfer_id'), 'has-success': fields.resource_transfer_id && fields.resource_transfer_id.valid }">
    <label for="resource_transfer_id" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.applicant-resource-transfer.columns.resource_transfer_id') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.resource_transfer_id" v-validate="'required'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('resource_transfer_id'), 'form-control-success': fields.resource_transfer_id && fields.resource_transfer_id.valid}" id="resource_transfer_id" name="resource_transfer_id" placeholder="{{ trans('admin.applicant-resource-transfer.columns.resource_transfer_id') }}">
        <div v-if="errors.has('resource_transfer_id')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('resource_transfer_id') }}</div>
    </div>
</div>


