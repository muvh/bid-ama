<h3>Datos para Orden de Transferencia</h3>
<div class="row">
   <div class="col-sm-6">
<div class="form-group" :class="{'has-danger': errors.has('government_id'), 'has-success': fields.government_id && fields.government_id.valid }">
    <label for="government_id" class="col-form-label " :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.applicant.columns.government_id') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-4'">
            <div class="input-group-append">
                <input type="text" required v-model="form.government_id" v-validate="'required'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('government_id'), 'form-control-success': fields.government_id && fields.government_id.valid}" id="government_id" name="government_id" placeholder="{{ trans('admin.applicant.columns.government_id') }}">
                <div class="input-group-append">
                    <button @click="findApplicant" class="btn btn-info" type="button"><i class="fa fa-search"></i></button>
                </div>
                <div v-if="errors.has('government_id')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('government_id') }}</div>
            </div>
        </div>
</div>

<div class="form-group">
<div :class="isFormLocalized ? 'col-md-4' : 'col-sm-8'">
        <label for="name" class="col-form-label" > Nombre Completo</label>
        <input type="text" readonly v-model="form.name" v-validate="'required'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('government_id'), 'form-control-success': fields.name && fields.name.valid}" id="name" name="name" placeholder="{{ trans('admin.applicant.columns.names') }}">

</div>
</div>
<div v-if="polizalists.length == 2"> 
     <div class="form-group" :class="{'has-danger': errors.has('type_order_id'), 'has-success': fields.type_order_id && fields.type_order_id.valid }">
            <label for="type_order_id" class="col-form-label " :class="isFormLocalized ? 'col-md-4' : 'col-md-3'">{{ trans('admin.transfer-order.columns.type_order_id') }}</label>
                <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
                    <multiselect
                        v-model="form.type_order_id"
                        :options="{{$typeOrders->toJson()}}"
                        :multiple="false"
                        track-by="id"
                        label="title"
                        :taggable="true"
                        tag-placeholder=""
                        placeholder="{{ trans('admin.applicant.actions.search')  }}"
                        @input="executeLoader">
                    </multiselect>
                <div v-if="errors.has('type_order_id')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('type_order_id') }}</div>
            </div>
        </div>

        <div class="form-group" :class="{'has-danger': errors.has('nro_solicitud'), 'has-success': fields.nro_solicitud && fields.nro_solicitud.valid }">
            <label for="nro_solicitud" class="col-form-label " :class="isFormLocalized ? 'col-md-4' : 'col-md-3'">{{ trans('admin.transfer-order.columns.nro_solicitud') }}</label>
            <div :class="isFormLocalized ? 'col-md-4' : 'col-sm-8'">
                   
                    <input type="text" v-model="form.nro_solicitud" v-validate="'required'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('nro_solicitud'), 'form-control-success': fields.nro_solicitud && fields.nro_solicitud.valid}" id="nro_solicitud" name="nro_solicitud" placeholder="{{ trans('admin.transfer-order.columns.nro_solicitud') }}">
                
                <div v-if="errors.has('nro_solicitud')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('nro_solicitud') }}</div>
            </div>
        </div>
    



    <div class="form-group" :class="{'has-danger': errors.has('fecha_ot'), 'has-success': fields.fecha_ot && fields.fecha_ot.valid }">
        <label for="fecha_ot" class="col-form-label " :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.transfer-order.columns.fecha_ot') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-sm-8'">
            <div class="input-group input-group--custom">
                <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                <datetime required v-model="form.fecha_ot" :config="datePickerConfig" v-validate="'required'" class="flatpickr" :class="{'form-control-danger': errors.has('fecha_ot'), 'form-control-success': fields.fecha_ot && fields.fecha_ot.valid}" id="fecha_ot" name="fecha_ot" placeholder="{{ trans('brackets/admin-ui::admin.forms.select_a_date') }}"></datetime>
            </div>
            <div v-if="errors.has('fecha_ot')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('fecha_ot') }}</div>
        </div>
    </div>


    <div class="form-group" :class="{'has-danger': errors.has('importe'), 'has-success': fields.importe && fields.importe.valid }">
        <label for="importe" class="col-form-label " :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.transfer-order.columns.importe') }}</label>
            <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
            <input type="text" v-model="form.importe" v-validate="'required'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('importe'), 'form-control-success': fields.importe && fields.importe.valid}" id="importe" name="importe" placeholder="{{ trans('admin.transfer-order.columns.importe') }}">
            <div v-if="errors.has('importe')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('importe') }}</div>
        </div>
    </div>


    <div class="form-group" :class="{'has-danger': errors.has('construction_entity_id'), 'has-success': fields.construction_entity_id && fields.construction_entity_id.valid }">
        <label for="construction_entity_id" class="col-form-label " :class="isFormLocalized ? 'col-md-4' : 'col-md-3'">{{ trans('admin.transfer-order.columns.construction_entity_id') }}</label>
            <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
            <input type="hidden" v-model="form.construction_entity_id" v-validate="'required'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('construction_entity_id'), 'form-control-success': fields.construction_entity_id && fields.construction_entity_id.valid}" id="construction_entity_id" name="construction_entity_id" placeholder="{{ trans('admin.transfer-order.columns.construction_entity_id') }}">
            <input type="type" readonly v-model="form.construction_entity_name" v-validate="'required'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('construction_entity_name'), 'form-control-success': fields.construction_entity_name && fields.construction_entity_name.valid}" id="construction_entity_name" name="construction_entity_name" >
            <div v-if="errors.has('construction_entity_id')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('construction_entity_id') }}</div>
        </div>
    </div>


    <div class="form-group" :class="{'has-danger': errors.has('applicant_id'), 'has-success': fields.applicant_id && fields.applicant_id.valid }">
        <label for="applicant_id" class="col-form-label " :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.transfer-order.columns.applicant_id') }}</label>
            <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
            <input type="text" readonly v-model="form.applicant_id" v-validate="'required'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('applicant_id'), 'form-control-success': fields.applicant_id && fields.applicant_id.valid}" id="applicant_id" name="applicant_id" placeholder="{{ trans('admin.transfer-order.columns.applicant_id') }}">
            <div v-if="errors.has('applicant_id')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('applicant_id') }}</div>
        </div>
    </div>


    <div class="form-group" :class="{'has-danger': errors.has('financial_entity_id'), 'has-success': fields.financial_entity_id && fields.financial_entity_id.valid }">
        <label for="financial_entity_id" class="col-form-label " :class="isFormLocalized ? 'col-md-4' : 'col-md-3'">{{ trans('admin.transfer-order.columns.financial_entity_id') }}</label>
            <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
            <input type="hidden" v-model="form.financial_entity_id" v-validate="'required'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('financial_entity_id'), 'form-control-success': fields.financial_entity_id && fields.financial_entity_id.valid}" id="financial_entity_id" name="financial_entity_id" placeholder="{{ trans('admin.transfer-order.columns.financial_entity_id') }}">
            <input type="text" readonly v-model="form.financial_entity_name" v-validate="'required'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('financial_entity_name'), 'form-control-success': fields.financial_entity_name && fields.financial_entity_name.valid}" id="financial_entity_name" name="financial_entity_name" >
            <div v-if="errors.has('financial_entity_id')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('financial_entity_id') }}</div>
        </div>
    </div>
    </div>
    </div>
        <div class="col-sm-6">
           
        
            <h3>Datos de poliza</h3>
            
            <div v-if="polizalists.length < 2"> 
                <h4>Se debe crear dos polizas antes de generar la orden</h4>

                <a v-if="newpoliza" class="btn btn-primary btn-spinner btn-sm pull-left m-b-0" :href="'/admin/insurance-policies/'+form.applicant_id+'/createOrdenTra'" role="button"><i class="fa fa-plus"></i>&nbsp; {{ trans('admin.insurance-policy.actions.create') }}</a>
            </div>
            <div v-if="polizalists.length >0"> 
            <table class="table table-hover table-listing">
                <thead>
                <tr>
                    <th>Nro de Poliza</th>
                    <th>Concepto</th>
                    <th>Importe</th>
                    <th>Fecha vigencia</th>
                    <th>Fecha vencimiento</th>
                    <th>Fecha Desembolso</th>
                </tr>
            </thead>
                <tbody>
                    <tr v-for="(polizalist, key) in polizalists">
                        <td>@{{ polizalist.nro_poliza }}</td>
                        <td>@{{ polizalist.concepto }}</td>
                        <td>@{{ polizalist.importe  }}</td>
                        <td>@{{ polizalist.fecha_vigencia | formatDate }}</td>
                        <td>@{{ polizalist.fecha_vencimiento | formatDate }}</td>
                        <td>@{{ polizalist.fecha_desembolso | formatDate }}</td>
                       
                    </tr>
                </tbody>
            </table>

        </div>
     


    </div>



