@extends('brackets/admin-ui::admin.layout.default')

@section('title', trans('admin.transfer-order.actions.create'))

@section('body')

    <div class="container-fluid">

                <div class="card">
        
        <transfer-order-form
            :action="'{{ url('admin/transfer-orders') }}'"
         
            :governmentid="0"
      
            
            v-cloak
            inline-template>
            <form class="form-horizontal form-create" method="post" @submit.prevent="onSubmit" :action="action" novalidate>
               
                <div class="card-header">
                    <i class="fa fa-plus"></i> {{ trans('admin.transfer-order.actions.create') }}
                </div>

                <div class="card-body">
                    @include('admin.transfer-order.components.form-elements')
                </div>
                <div v-if="!newpoliza">               
                <div class="card-footer">
                    <button id="btn-guardar" type="submit" class="btn btn-primary" :disabled="submiting">
                        <i class="fa" :class="submiting ? 'fa-spinner' : 'fa-download'"></i>
                        {{ trans('brackets/admin-ui::admin.btn.save') }}
                    </button>
                </div>
                </div>
                
            </form>

        </transfer-order-form>
        <notifications position="top center" group="auth"/>
        </div>

        </div>

    
@endsection