
{{-- {{$transferOrder}} --}}
<style type="text/css">
    .tg  {border-collapse:collapse;border-spacing:0;  width:100%;}
    .tg td{border-color:black;border-style:solid;border-width:1px;font-family:Arial, sans-serif;font-size:10px;
      overflow:hidden;padding:10px 5px;word-break:normal;}
    .tg th{border-color:black;border-style:solid;border-width:1px;font-family:Arial, sans-serif;font-size:10px;
      font-weight:normal;overflow:hidden;padding:10px 5px;word-break:normal;}
    .tg .tg-x1hj{border-color:inherit;font-size:16px;text-align:left;vertical-align:top;text-align:left}
    .tg .tg-9d8n{border-color:inherit;font-size:16px;text-align:center;vertical-align:top}
    .tg .tg-spag{border-color:inherit;font-size:16px;font-weight:bold;text-align:left;vertical-align:top}
    </style>
    <h3 style="text-align: center">FORMULARIO DE ORDEN DE TRANSFERENCIA DE RECURSOS</h3>
    <table class="tg">
    <thead>
      <tr>
        <th class="tg-x1hj" colspan="4">{{$transferOrder->typeorder->title}} </th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td class="tg-x1hj">Entidad:</td>
        <td class="tg-x1hj" colspan="3">Ministerio de Urbanismo, Vivienda y Habitat (MUVH)</td>
      </tr>
      <tr>
        <td class="tg-x1hj">N° de Resolucion</td>
        <td class="tg-x1hj">{{$transferOrder->applicant->resolution_number}} </td>
        <td class="tg-x1hj">Fecha de Emision</td>
        <td class="tg-x1hj">{{date('d/m/Y', strtotime($transferOrder->applicant->resolution_date))}}</td>
      </tr>
      <tr>
        <td class="tg-x1hj">N° de Solicitud</td>
        <td class="tg-x1hj">{{$transferOrder->nro_solicitud}}</td>
        <td class="tg-x1hj">Fecha de emision</td>
        <td class="tg-x1hj">{{date('d/m/Y', strtotime($transferOrder->fecha_ot))}}</td>
      </tr>
      <tr>
        <td class="tg-x1hj">Cooperativa:</td>
        <td class="tg-x1hj" colspan="3">{{$transferOrder->financial->name}}</td>
      </tr>
      <tr>
        <td class="tg-9d8n" colspan="4"><span style="font-weight:bold">Datos para la Transferencia</span></td>
      </tr>
      <tr>
        <td class="tg-x1hj">ATC:</td>
        <td class="tg-x1hj" colspan="3">{{$transferOrder->construction->razon_social}}</td>
      </tr>
      <tr>
        <td class="tg-x1hj">RUC:</td>
        <td class="tg-x1hj" colspan="3">{{$transferOrder->construction->const_ruc}}</td>
      </tr>
      <tr>
        <td class="tg-x1hj">Banco</td>
        <td class="tg-x1hj" colspan="3">{{$transferOrder->construction->bank_name}}</td>
      </tr>
      <tr>
        <td class="tg-x1hj">Cta. Cte.:</td>
        <td class="tg-x1hj" colspan="3">{{$transferOrder->construction->cta_cte}}</td>
      </tr>
      <tr>
        <td class="tg-spag" colspan="3">Concepto de la transferencia</td>
        <td class="tg-9d8n"><span style="font-weight:bold">Importe en </span><br><span style="font-weight:bold">Guaranies</span></td>
      </tr>
      <tr>
        <td class="tg-x1hj" colspan="3">
          <p>
          {{$textConcepto}}
          </p>
          <p>

            {{$transferOrder->applicant->names}} &nbsp; &nbsp; &nbsp; <span style="font-weight:bold">CI. N° </span>{{$transferOrder->applicant->government_id}}
            &nbsp; &nbsp; &nbsp;
            <span style="font-weight:bold">Monto: </span>{{number_format($transferOrder->importe,0, '.', '.')}}
          </p>
        </td>
        <td class="tg-x1hj">{{number_format($transferOrder->importe,0, '.', '.')}}</td>
       
      </tr>
      <tr>
        <td class="tg-x1hj"><span style="font-weight:bold">Importe en letras:</span></td>
        <td class="tg-x1hj" colspan="3">
          @if($transferOrder->type_order_id==1)
            Guaranies Veintiocho Millones
          @else
            Guaranies {{$mount}}
          @endif
        </td>
      </tr>
    </tbody>
    </table>

    <br>
    <br>



    <style type="text/css">
      .tg2  {border-collapse:collapse;border-spacing:0;width:100%;}
      .tg2 td{border-color:black;border-style:solid;border-width:1px;font-family:Arial, sans-serif;font-size:14px;
        overflow:hidden;padding:10px 5px;word-break:normal;}
      .tg2 th{border-color:black;border-style:solid;border-width:1px;font-family:Arial, sans-serif;font-size:14px;
        font-weight:normal;overflow:hidden;padding:10px 5px;word-break:normal;}
      .tg2 .tg2-nx8p{font-size:18px;text-align:left;vertical-align:top}
      .tg2 .tg2-13pz{font-size:18px;text-align:center;vertical-align:top}
      </style>
      <div style="margin: 0 auto;">
      <table class="tg2">
      <thead>
        <tr>
          <th class="tg2-nx8p"><br><br><br></th>
          <th class="tg2-nx8p"><br><br><br></th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td class="tg2-13pz">Lic. María Gloria Páez<br>Dirección General de la <br>UEP - BID</td>
          <td class="tg2-13pz">Lic. Arnaldo Andrés Lezcano<br>Dirección General de <br>Administración y Finanzas</td>
        </tr>
      </tbody>
      </table>
    </div>
