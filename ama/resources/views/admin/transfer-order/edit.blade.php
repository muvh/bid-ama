@extends('brackets/admin-ui::admin.layout.default')

@section('title', trans('admin.transfer-order.actions.edit', ['name' => $transferOrder->id]))

@section('body')

    <div class="container-xl">
        <div class="card">

            <transfer-order-form
                :action="'{{ $transferOrder->resource_url }}'"
                :data="{{ $transferOrder->toJson() }}"
                v-cloak
                inline-template>
            
                <form class="form-horizontal form-edit" method="post" @submit.prevent="onSubmit" :action="action" novalidate>


                    <div class="card-header">
                        <i class="fa fa-pencil"></i> {{ trans('admin.transfer-order.actions.edit', ['name' => $transferOrder->id]) }}
                    </div>

                    <div class="card-body">
                        @include('admin.transfer-order.components.form-elements')
                    </div>
                    
                    
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary" :disabled="submiting">
                            <i class="fa" :class="submiting ? 'fa-spinner' : 'fa-download'"></i>
                            {{ trans('brackets/admin-ui::admin.btn.save') }}
                        </button>
                    </div>
                    
                </form>

        </transfer-order-form>
        <notifications position="top center" group="auth"/>
        </div>
    
</div>

@endsection