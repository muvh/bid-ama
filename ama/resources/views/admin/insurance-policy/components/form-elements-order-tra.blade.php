<div class="form-group row align-items-center" :class="{'has-danger': errors.has('nro_poliza'), 'has-success': fields.nro_poliza && fields.nro_poliza.valid }">
    <label for="nro_poliza" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.insurance-policy.columns.nro_poliza') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.nro_poliza" v-validate="'required'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('nro_poliza'), 'form-control-success': fields.nro_poliza && fields.nro_poliza.valid}" id="nro_poliza" name="nro_poliza" placeholder="{{ trans('admin.insurance-policy.columns.nro_poliza') }}">
        <div v-if="errors.has('nro_poliza')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('nro_poliza') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('concepto'), 'has-success': fields.concepto && fields.concepto.valid }">
    <label for="concepto" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.insurance-policy.columns.concepto') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
      {{--       <multiselect 
            v-model="user" 
            name="userid"
            :options="types.map(type => type.id)" 
            :custom-label="opt => types.find(x => x.id == opt).name">
          </multiselect> --}}

         
        <multiselect
        v-model="form.conceptoid"
        :options="{{$conceptOption}}"
        
        :multiple="false"
        track-by="id"
        label="text"
     
        tag-placeholder=""
        placeholder="Seleccionar"
        @input="executeLoader"
        >
    </multiselect>
    <input type="hidden" v-model="form.concepto" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('applicant_id'), 'form-control-success': fields.concepto && fields.concepto.valid}" >
        
        <div v-if="errors.has('concepto')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('concepto') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('fecha_vigencia'), 'has-success': fields.fecha_vigencia && fields.fecha_vigencia.valid }">
    <label for="fecha_vigencia" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.insurance-policy.columns.fecha_vigencia') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-sm-8'">
        <div class="input-group input-group--custom">
            <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
            <datetime v-model="form.fecha_vigencia" :config="datePickerConfig" v-validate="'required'" class="flatpickr" :class="{'form-control-danger': errors.has('fecha_vigencia'), 'form-control-success': fields.fecha_vigencia && fields.fecha_vigencia.valid}" id="fecha_vigencia" name="fecha_vigencia" placeholder="{{ trans('brackets/admin-ui::admin.forms.select_a_date') }}"></datetime>
        </div>
        <div v-if="errors.has('fecha_vigencia')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('fecha_vigencia') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('fecha_vencimiento'), 'has-success': fields.fecha_vencimiento && fields.fecha_vencimiento.valid }">
    <label for="fecha_vencimiento" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.insurance-policy.columns.fecha_vencimiento') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-sm-8'">
        <div class="input-group input-group--custom">
            <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
            <datetime v-model="form.fecha_vencimiento" :config="datePickerConfig" v-validate="'required'" class="flatpickr" :class="{'form-control-danger': errors.has('fecha_vencimiento'), 'form-control-success': fields.fecha_vencimiento && fields.fecha_vencimiento.valid}" id="fecha_vencimiento" name="fecha_vencimiento" placeholder="{{ trans('brackets/admin-ui::admin.forms.select_a_date') }}"></datetime>
        </div>
        <div v-if="errors.has('fecha_vencimiento')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('fecha_vencimiento') }}</div>
    </div>
</div>


<div class="form-group row align-items-center" :class="{'has-danger': errors.has('importe'), 'has-success': fields.importe && fields.importe.valid }">
    <label for="importe" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.insurance-policy.columns.importe') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.importe" v-validate="'required'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('importe'), 'form-control-success': fields.importe && fields.importe.valid}" id="importe" name="importe" placeholder="{{ trans('admin.insurance-policy.columns.importe') }}">
        <div v-if="errors.has('importe')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('importe') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('applicant_id'), 'has-success': fields.applicant_id && fields.applicant_id.valid }">
    <label for="applicant_id" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">Proyecto:</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
            <multiselect
            v-model="form.applicant"
            :options="{{$applicantSelect}}"
            
            :multiple="false"
            track-by="id"
            label="names"
         
            tag-placeholder=""
            placeholder="Seleccionar"
            @input="searchApplicant"
            >
        </multiselect>
        <input type="hidden" v-model="form.applicant_id" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('applicant_id'), 'form-control-success': fields.applicant_id && fields.applicant_id.valid}" id="applicant_id" name="applicant_id" placeholder="{{ trans('admin.insurance-policy.columns.applicant_id') }}">
       
        <div v-if="errors.has('applicant_id')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('applicant_id') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('insurance_entity_id'), 'has-success': fields.insurance_entity_id && fields.insurance_entity_id.valid }">
    <label for="insurance_entity_id" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.insurance-policy.columns.insurance_entity_id') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">

<multiselect
v-model="form.insuranceentity"
:options="{{$seguros}}"

:multiple="false"
track-by="id"
label="title"

tag-placeholder=""
placeholder="Aseguradoras"
@input="searchSeguro"
>
</multiselect>
<input type="hidden" v-model="form.insurance_entity_id" @input="validate($event)" class="form-control" >
        
        <div v-if="errors.has('insurance_entity_id')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('insurance_entity_id') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('construction_entity_id'), 'has-success': fields.construction_entity_id && fields.construction_entity_id.valid }">
    <label for="construction_entity_id" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.insurance-policy.columns.construction_entity_id') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="hidden" v-model="form.construction_entity_id" v-validate="'required'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('construction_entity_id'), 'form-control-success': fields.construction_entity_id && fields.construction_entity_id.valid}" id="construction_entity_id" name="construction_entity_id" placeholder="{{ trans('admin.insurance-policy.columns.construction_entity_id') }}">
        <input type="text" readonly class="form-control" v-model="form.construction_entity_name" >
        <div v-if="errors.has('construction_entity_id')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('construction_entity_id') }}</div>
    </div>
</div>


