<div class="form-group row align-items-center" :class="{'has-danger': errors.has('nro_poliza'), 'has-success': fields.nro_poliza && fields.nro_poliza.valid }">
    <label for="nro_poliza" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.insurance-policy.columns.nro_poliza') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.nro_poliza" v-validate="'required'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('nro_poliza'), 'form-control-success': fields.nro_poliza && fields.nro_poliza.valid}" id="nro_poliza" name="nro_poliza" placeholder="{{ trans('admin.insurance-policy.columns.nro_poliza') }}">
        <div v-if="errors.has('nro_poliza')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('nro_poliza') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('fecha_desembolso'), 'has-success': fields.fecha_desembolso && fields.fecha_desembolso.valid }">
    <label for="fecha_desembolso" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.insurance-policy.columns.fecha_desembolso') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-sm-8'">
        <div class="input-group input-group--custom">
            <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
            <datetime v-model="form.fecha_desembolso" :config="datePickerConfig"  class="flatpickr" :class="{'form-control-danger': errors.has('fecha_desembolso'), 'form-control-success': fields.fecha_desembolso && fields.fecha_desembolso.valid}" id="fecha_desembolso" name="fecha_desembolso" placeholder="{{ trans('brackets/admin-ui::admin.forms.select_a_date') }}"></datetime>
        </div>
        <div v-if="errors.has('fecha_desembolso')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('fecha_desembolso') }}</div>
    </div>
</div>
<div class="form-group row align-items-center" :class="{'has-danger': errors.has('fecha_envio_cac'), 'has-success': fields.fecha_envio_cac && fields.fecha_envio_cac.valid }">
    <label for="fecha_envio_cac" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.insurance-policy.columns.fecha_envio_cac') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-sm-8'">
        <div class="input-group input-group--custom">
            <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
            <datetime v-model="form.fecha_envio_cac" :config="datePickerConfig" class="flatpickr" :class="{'form-control-danger': errors.has('fecha_envio_cac'), 'form-control-success': fields.fecha_envio_cac && fields.fecha_envio_cac.valid}" id="fecha_envio_cac" name="fecha_envio_cac" placeholder="{{ trans('brackets/admin-ui::admin.forms.select_a_date') }}"></datetime>
        </div>
        <div v-if="errors.has('fecha_envio_cac')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('fecha_envio_cac') }}</div>
    </div>
</div>

{{-- <div class="form-group row align-items-center" :class="{'has-danger': errors.has('importe'), 'has-success': fields.importe && fields.importe.valid }">
    <label for="importe" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.insurance-policy.columns.importe') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.importe" v-validate="'required'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('importe'), 'form-control-success': fields.importe && fields.importe.valid}" id="importe" name="importe" placeholder="{{ trans('admin.insurance-policy.columns.importe') }}">
        <div v-if="errors.has('importe')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('importe') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('applicant_id'), 'has-success': fields.applicant_id && fields.applicant_id.valid }">
    <label for="applicant_id" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.insurance-policy.columns.applicant_id') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.applicant_id" v-validate="'required'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('applicant_id'), 'form-control-success': fields.applicant_id && fields.applicant_id.valid}" id="applicant_id" name="applicant_id" placeholder="{{ trans('admin.insurance-policy.columns.applicant_id') }}">
        <div v-if="errors.has('applicant_id')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('applicant_id') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('insurance_entity_id'), 'has-success': fields.insurance_entity_id && fields.insurance_entity_id.valid }">
    <label for="insurance_entity_id" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.insurance-policy.columns.insurance_entity_id') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.insurance_entity_id" v-validate="'required'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('insurance_entity_id'), 'form-control-success': fields.insurance_entity_id && fields.insurance_entity_id.valid}" id="insurance_entity_id" name="insurance_entity_id" placeholder="{{ trans('admin.insurance-policy.columns.insurance_entity_id') }}">
        <div v-if="errors.has('insurance_entity_id')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('insurance_entity_id') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('construction_entity_id'), 'has-success': fields.construction_entity_id && fields.construction_entity_id.valid }">
    <label for="construction_entity_id" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.insurance-policy.columns.construction_entity_id') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.construction_entity_id" v-validate="'required'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('construction_entity_id'), 'form-control-success': fields.construction_entity_id && fields.construction_entity_id.valid}" id="construction_entity_id" name="construction_entity_id" placeholder="{{ trans('admin.insurance-policy.columns.construction_entity_id') }}">
        <div v-if="errors.has('construction_entity_id')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('construction_entity_id') }}</div>
    </div>
</div>


 --}}
 <div class="form-group row align-items-center" :class="{'has-danger': errors.has('fecha_vigencia'), 'has-success': fields.fecha_vigencia && fields.fecha_vigencia.valid }">
    <label for="fecha_vigencia" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.insurance-policy.columns.fecha_vigencia') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-sm-8'">
        <div class="input-group input-group--custom">
            <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
            <datetime v-model="form.fecha_vigencia" :config="datePickerConfig" v-validate="'required'" class="flatpickr" :class="{'form-control-danger': errors.has('fecha_vigencia'), 'form-control-success': fields.fecha_vigencia && fields.fecha_vigencia.valid}" id="fecha_vigencia" name="fecha_vigencia" placeholder="{{ trans('brackets/admin-ui::admin.forms.select_a_date') }}"></datetime>
        </div>
        <div v-if="errors.has('fecha_vigencia')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('fecha_vigencia') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('fecha_vencimiento'), 'has-success': fields.fecha_vencimiento && fields.fecha_vencimiento.valid }">
    <label for="fecha_vencimiento" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.insurance-policy.columns.fecha_vencimiento') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-sm-8'">
        <div class="input-group input-group--custom">
            <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
            <datetime v-model="form.fecha_vencimiento" :config="datePickerConfig" v-validate="'required'" class="flatpickr" :class="{'form-control-danger': errors.has('fecha_vencimiento'), 'form-control-success': fields.fecha_vencimiento && fields.fecha_vencimiento.valid}" id="fecha_vencimiento" name="fecha_vencimiento" placeholder="{{ trans('brackets/admin-ui::admin.forms.select_a_date') }}"></datetime>
        </div>
        <div v-if="errors.has('fecha_vencimiento')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('fecha_vencimiento') }}</div>
    </div>
</div>