@extends('brackets/admin-ui::admin.layout.default')

@section('title', trans('admin.insurance-policy.actions.edit', ['name' => $insurancePolicy->id]))

@section('body')

    <div class="container-xl">
        <div class="card">

            <insurance-policy-form
                :action="'{{ $insurancePolicy->resource_url }}'"
                :data="{{ $insurancePolicy->toJson() }}"
                v-cloak
                inline-template>
            
                <form class="form-horizontal form-edit" method="post" @submit.prevent="onSubmit" :action="action" novalidate>


                    <div class="card-header">
                        <i class="fa fa-pencil"></i> {{ trans('admin.insurance-policy.actions.edit', ['name' => $insurancePolicy->id]) }}
                    </div>

                    <div class="card-body">
                        @include('admin.insurance-policy.components.form-elements')
                    </div>
                    
                    
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary" :disabled="submiting">
                            <i class="fa" :class="submiting ? 'fa-spinner' : 'fa-download'"></i>
                            {{ trans('brackets/admin-ui::admin.btn.save') }}
                        </button>
                    </div>
                    
                </form>

        </insurance-policy-form>

        </div>
    
</div>

@endsection