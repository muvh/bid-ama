@extends('brackets/admin-ui::admin.layout.default')

@section('title', trans('admin.insurance-policy.actions.index'))

@section('body')

    <insurance-policy-listing
        :data="{{ $data->toJson() }}"
        :url="'{{ url('admin/insurance-policies') }}'"
        inline-template>

        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="card-header">
                        <i class="fa fa-align-justify"></i> {{ trans('admin.insurance-policy.actions.index') }}
                        <a class="btn btn-primary btn-spinner btn-sm pull-right m-b-0" href="{{ url('admin/insurance-policies/create') }}" role="button"><i class="fa fa-plus"></i>&nbsp; {{ trans('admin.insurance-policy.actions.create') }}</a>
                        <a style="margin: 0px 20px 0px 20px" class="btn btn-primary btn-sm pull-right m-b-0" href="{{ url('admin/insurance-policies/export/') }}" role="button"><i class="fa fa-download"></i>&nbsp; {{ trans('admin.applicant.actions.export') }}</a>
                    </div>
                    <div class="card-body" v-cloak>
                        <div class="card-block">
                            <form @submit.prevent="">
                                <div class="row justify-content-md-between">
                                    <div class="col col-lg-7 col-xl-5 form-group">
                                        <div class="input-group">
                                            <input class="form-control" placeholder="{{ trans('brackets/admin-ui::admin.placeholder.search') }}" v-model="search" @keyup.enter="filter('search', $event.target.value)" />
                                            <span class="input-group-append">
                                                <button type="button" class="btn btn-primary" @click="filter('search', search)"><i class="fa fa-search"></i>&nbsp; {{ trans('brackets/admin-ui::admin.btn.search') }}</button>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="col-sm-auto form-group ">
                                        <select class="form-control" v-model="pagination.state.per_page">
                                            
                                            <option value="10">10</option>
                                            <option value="25">25</option>
                                            <option value="100">100</option>
                                        </select>
                                    </div>
                                </div>
                            </form>

                            <table class="table table-hover table-listing">
                                <thead>
                                    <tr>
                                        <th class="bulk-checkbox">
                                            <input class="form-check-input" id="enabled" type="checkbox" v-model="isClickedAll" v-validate="''" data-vv-name="enabled"  name="enabled_fake_element" @click="onBulkItemsClickedAllWithPagination()">
                                            <label class="form-check-label" for="enabled">
                                                #
                                            </label>
                                        </th>

                                        <th is='sortable' :column="'id'">{{ trans('admin.insurance-policy.columns.id') }}</th>
                                        <th is='sortable' :column="'nro_poliza'">{{ trans('admin.insurance-policy.columns.nro_poliza') }}</th>
                                        <th is='sortable' :column="'concepto'">{{ trans('admin.insurance-policy.columns.concepto') }}</th>
                                        <th is='sortable' :column="'fecha_vigencia'">{{ trans('admin.insurance-policy.columns.fecha_vigencia') }}</th>
                                        <th is='sortable' :column="'fecha_vencimiento'">{{ trans('admin.insurance-policy.columns.fecha_vencimiento') }}</th>
                                        <th is='sortable' :column="'fecha_desembolso'">{{ trans('admin.insurance-policy.columns.fecha_desembolso') }}</th>
                                        <th is='sortable' :column="'fecha_envio_cac'">{{ trans('admin.insurance-policy.columns.fecha_envio_cac') }}</th>
                                        <th is='sortable' :column="'importe'">{{ trans('admin.insurance-policy.columns.importe') }}</th>
                                        <th is='sortable' :column="'government_id'">{{ trans('admin.insurance-policy.columns.applicant.government_id') }}</th>
                                        <th is='sortable' :column="'applicant_id'">{{ trans('admin.insurance-policy.columns.applicant_id') }}</th>
                                        <th is='sortable' :column="'insurance_entity_id'">{{ trans('admin.insurance-policy.columns.insurance_entity_id') }}</th>
                                        <th is='sortable' :column="'construction_entity_id'">{{ trans('admin.insurance-policy.columns.construction_entity_id') }}</th>
                                        <th is='sortable' :column="'dias'">{{ trans('admin.insurance-policy.columns.dias') }}</th>
                                        <th is='sortable' :column="'culminado'">{{ trans('admin.insurance-policy.columns.culminado') }}</th>

                                        <th></th>
                                    </tr>
                                    <tr v-show="(clickedBulkItemsCount > 0) || isClickedAll">
                                        <td class="bg-bulk-info d-table-cell text-center" colspan="12">
                                            <span class="align-middle font-weight-light text-dark">{{ trans('brackets/admin-ui::admin.listing.selected_items') }} @{{ clickedBulkItemsCount }}.  <a href="#" class="text-primary" @click="onBulkItemsClickedAll('/admin/insurance-policies')" v-if="(clickedBulkItemsCount < pagination.state.total)"> <i class="fa" :class="bulkCheckingAllLoader ? 'fa-spinner' : ''"></i> {{ trans('brackets/admin-ui::admin.listing.check_all_items') }} @{{ pagination.state.total }}</a> <span class="text-primary">|</span> <a
                                                        href="#" class="text-primary" @click="onBulkItemsClickedAllUncheck()">{{ trans('brackets/admin-ui::admin.listing.uncheck_all_items') }}</a>  </span>

                                            <span class="pull-right pr-2">
                                                <button class="btn btn-sm btn-danger pr-3 pl-3" @click="bulkDelete('/admin/insurance-policies/bulk-destroy')">{{ trans('brackets/admin-ui::admin.btn.delete') }}</button>
                                            </span>

                                        </td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr v-for="(item, index) in collection" :key="item.id" :class="bulkItems[item.id] ? 'bg-bulk' : ''">
                                        <td class="bulk-checkbox">
                                            <input class="form-check-input" :id="'enabled' + item.id" type="checkbox" v-model="bulkItems[item.id]" v-validate="''" :data-vv-name="'enabled' + item.id"  :name="'enabled' + item.id + '_fake_element'" @click="onBulkItemClicked(item.id)" :disabled="bulkCheckingAllLoader">
                                            <label class="form-check-label" :for="'enabled' + item.id">
                                            </label>
                                        </td>

                                    <td>@{{ item.id }}</td>
                                        <td>@{{ item.nro_poliza }}</td>
                                        <td>@{{ item.concepto }}</td>
                                        <td>@{{ item.fecha_vigencia | formatDate }}</td>
                                        <td>@{{ item.fecha_vencimiento | formatDate }}</td>
                                        <td>@{{ item.fecha_desembolso | formatDate }}</td>
                                        <td>@{{ item.fecha_envio_cac | formatDate }}</td>
                                        <td>@{{ item.importe }}</td>
                                        <td>@{{ item.applicant.government_id }} </td>
                                        <td>@{{ item.applicant.names }} @{{ item.applicant.last_names }}  </td>
                                        <td>@{{ item.insurance_entity.title }}</td>
                                        <td>@{{ item.construction.name }}</td>
                                        <td>@{{ item.dias }}</td>
                                        
                                        <td>
                                            <label class="switch switch-3d switch-success">
                                                <input type="checkbox" class="switch-input" v-model="collection[index].culminado" @change="toggleSwitch(item.resource_url, 'culminado', collection[index])">
                                                <span class="switch-slider"></span>
                                            </label>
                                        </td>
                                        
                                        <td>
                                            <div class="row no-gutters">
                                                <div class="col-auto">
                                                    <a class="btn btn-sm btn-spinner btn-info" :href="item.resource_url + '/edit'" title="{{ trans('brackets/admin-ui::admin.btn.edit') }}" role="button"><i class="fa fa-edit"></i></a>
                                                </div> 
{{--                                                 <form class="col" @submit.prevent="deleteItem(item.resource_url)">
                                                    <button type="submit" class="btn btn-sm btn-danger" title="{{ trans('brackets/admin-ui::admin.btn.delete') }}"><i class="fa fa-trash-o"></i></button>
                                                </form> --}}
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>

                            <div class="row" v-if="pagination.state.total > 0">
                                <div class="col-sm">
                                    <span class="pagination-caption">{{ trans('brackets/admin-ui::admin.pagination.overview') }}</span>
                                </div>
                                <div class="col-sm-auto">
                                    <pagination></pagination>
                                </div>
                            </div>

                            <div class="no-items-found" v-if="!collection.length > 0">
                                <i class="icon-magnifier"></i>
                                <h3>{{ trans('brackets/admin-ui::admin.index.no_items') }}</h3>
                                <p>{{ trans('brackets/admin-ui::admin.index.try_changing_items') }}</p>
                                <a class="btn btn-primary btn-spinner" href="{{ url('admin/insurance-policies/create') }}" role="button"><i class="fa fa-plus"></i>&nbsp; {{ trans('admin.insurance-policy.actions.create') }}</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </insurance-policy-listing>

@endsection