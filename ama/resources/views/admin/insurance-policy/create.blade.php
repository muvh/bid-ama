@extends('brackets/admin-ui::admin.layout.default')

@section('title', trans('admin.insurance-policy.actions.create'))

@section('body')

    <div class="container-xl">

                <div class="card">
        
        <insurance-policy-form
            :action="'{{ url('admin/insurance-policies') }}'"
            v-cloak
            inline-template>

            <form class="form-horizontal form-create" method="post" @submit.prevent="onSubmit" :action="action" novalidate>
                
                <div class="card-header">
                    <i class="fa fa-plus"></i> {{ trans('admin.insurance-policy.actions.create') }}
                </div>

                <div class="card-body">
                    @include('admin.insurance-policy.components.form-elements-order-tra')
                </div>
                                
                <div class="card-footer">
                    <button id="btn-guardar" type="submit" class="btn btn-primary" :disabled="submiting">
                        <i class="fa" :class="submiting ? 'fa-spinner' : 'fa-download'"></i>
                        {{ trans('brackets/admin-ui::admin.btn.save') }}
                    </button>
                </div>
                
            </form>

        </insurance-policy-form>

        </div>

        </div>

    
@endsection