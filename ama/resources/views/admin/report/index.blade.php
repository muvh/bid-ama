@extends('brackets/admin-ui::admin.layout.default')

@section('title', trans('admin.applicant.actions.index'))

@section('body')

    <applicant-listing
        :data="{{ $data->toJson() }}"
        :url="'{{ url('admin/reports/applicants-by-resolution') }}'"
        :export-url = "'{{ url('admin/reports/applicants-by-resolution/export/') }}'"
        inline-template>

        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="card-header">
                        <i class="fa fa-align-justify"></i>{{ trans('admin.reports.actions.applicants-by-resolution') }}
                        <template v-if="collection.length>0">
                            <export-button v-bind:resolution="collection[0].resolution_number" url="{{ url('admin/reports/applicants-by-resolution/export') }}/" text-button="{{ trans('admin.applicant.actions.export') }}"></export-button>
                        </template>
                    </div>
                    <div class="card-body" v-cloak>
                        <div class="card-block">
                            <form @submit.prevent="">
                                <div class="row justify-content-md-between">
                                    <div class="col col-lg-7 col-xl-5 form-group">
                                        <div class="input-group">
                                            <input class="form-control" placeholder="{{ trans('brackets/admin-ui::admin.placeholder.search-by-resolution') }}" v-model="search" @keyup.enter="filter('search', $event.target.value)" />
                                            <span class="input-group-append">
                                                <button type="button" class="btn btn-primary" @click="filter('search', search)"><i class="fa fa-search"></i>&nbsp; {{ trans('brackets/admin-ui::admin.btn.search') }}</button>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="col-sm-auto form-group ">
                                        <select class="form-control" v-model="pagination.state.per_page">
                                            <option value="10">10</option>
                                            <option value="25">25</option>
                                            <option value="100">100</option>
                                        </select>
                                    </div>
                                </div>
                            </form>

                            <table class="table table-hover table-listing">
                                <thead>
                                    <tr>
                                        <th is='sortable' :column="'id'">Nro</th>
                                        <th is='sortable' :column="'file_number'">Expediente</th>
                                        <th is='sortable' :column="'created_at'">Ingreso</th>
                                        <th is='sortable' :column="'names'">Beneficiario</th>
                                        <th is='sortable' :column="'government_id'">CI Nro</th>
                                        <th>Conyuge</th>
                                        <th>CI Nro</th>
                                        <th is='sortable' :column="'marital_status'">Estado Civil</th>
                                        <th is='sortable' :column="'total_monthly_income'">Ingreso</th>
                                        <th is='sortable' :column="'centityname'">ATC</th>
                                        <th is='sortable' :column="'fentityname'">Coop.</th>
                                        <th is='sortable' :column="'city_name'">Ciudad</th>
                                        <th is='sortable' :column="'city_name'">Estado</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr v-for="(item, index) in collection" :key="item.id" :class="bulkItems[item.id] ? 'bg-bulk' : ''">
                                        <td>@{{ item.id }}</td>
                                        <td>@{{ item.file_number }}</td>
                                        <td>@{{ item.created_at }}</td>
                                        <td>@{{ item.names }} @{{ item.last_names }}</td>
                                        <td>@{{ item.government_id }}</td>
                                        <td>@{{ item.conyuge ? item.conyuge.names : '' | limitCharecters(20) }}</td>
                                        <td>@{{ item.conyuge ? item.conyuge.government_id : '' | limitCharecters(20) }}</td>
                                        <td>@{{ item.marital_status }}</td>
                                        <td style="text-align: right">@{{ (item.monthly_income ? parseInt(item.monthly_income)  : 0 ) + (item.conyuge ? parseInt(item.conyuge.monthly_income ? item.conyuge.monthly_income : 0) : 0) | numberFormat }}</td>
                                        <td>@{{ item.construction.name }}</td>
                                        <td>@{{ item.financial.name }}</td>
                                        <td>@{{ item.city.name }}</td>
                                      {{--   <td>@{{ item.city ? item.city.name : '' }}</td> --}}
                                       
                                        <td style="text-align: center">@{{ item.statuses.status.name }}</td>
                                        <td>
                                            <div class="row no-gutters">
                                                <div class="col-auto">
                                                    <a class="btn btn-sm btn-spinner btn-info" :href="'../applicants/'+item.id + '/edit'" title="{{ trans('brackets/admin-ui::admin.btn.edit') }}" role="button"><i class="fa fa-pencil"></i></a>
                                                </div>
 
                                            {{--     <div class="col-auto">
                                                    <a class="btn btn-sm btn-spinner btn-info" :href="item.resource_url + '/print'" title="{{ trans('brackets/admin-ui::admin.btn.print') }}" role="button"><i class="fa fa-print"></i></a>
                                                </div> --}}
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>

                            <div class="row" v-if="pagination.state.total > 0">
                                <div class="col-sm">
                                    <span class="pagination-caption">{{ trans('brackets/admin-ui::admin.pagination.overview') }}</span>
                                </div>
                                <div class="col-sm-auto">
                                    <pagination></pagination>
                                </div>
                            </div>

                            <div class="no-items-found" v-if="!collection.length > 0">
                                <i class="icon-magnifier"></i>
                                <h3>{{ trans('brackets/admin-ui::admin.index.no_items') }}</h3>
                                <p>{{ trans('brackets/admin-ui::admin.index.try_changing_items') }}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </applicant-listing>

@endsection
