@extends('brackets/admin-ui::admin.layout.default')

@section('title', trans('admin.applicant.actions.social'))

@section('body')
<div class="container-fluid">

    <atc-status
        :data="{{ $data->toJson() }}"
        :url="'{{ url('admin/reports/atc-by-status') }}'"
    
        inline-template>
      
        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="card-header">

                        <i class="fa fa-align-justify"></i>{{ trans('admin.reports.actions.applicants-by-resolution-social') }}
                     {{--    <template v-if="collection.length>0">
                            <export-button-social  url="{{ url('admin/reports/applicants-by-resolution-social/export/'.$dateto.'/'.$datefrom.'/'.$status.'/') }}" text-button="{{ trans('admin.applicant.actions.export') }}"></export-button-social>
                        </template> --}}

                    </div>
                    <div class="card-body" v-cloak>
                        <div class="card-block">
                            <div class="row">
                                <div class="col-sm-2">
                                     {{-- @{{data}}  --}}
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio"  v-model="form.radio" @click="showtemplate(1)" value="1"   name="flexRadioDefault1" id="flexRadioDefault1">
                                        <label class="form-check-label" for="flexRadioDefault1">
                                          Por ATCs
                                        </label>
                                      </div>
                                      <div class="form-check">
                                        <input class="form-check-input" type="radio" v-model="form.radio" value="2"   @click="showtemplate(2)" name="flexRadioDefault2" id="flexRadioDefault2">
                                        <label class="form-check-label" for="flexRadioDefault2">
                                          Por Cooperativas
                                        </label>
                                      </div>
                                      <div class="form-check">
                                        <input class="form-check-input" type="radio" v-model="form.radio" value="3"  @click="showtemplate(3)" name="flexRadioDefault3" id="flexRadioDefault3">
                                        <label class="form-check-label" for="flexRadioDefault2">
                                          Por Municipios
                                        </label>
                                      </div>
                                      <div class="form-check">
                                        <input class="form-check-input" type="radio" v-model="form.radio" value="4"  @click="showtemplate(4)" name="flexRadioDefault4" id="flexRadioDefault4">
                                        <label class="form-check-label" for="flexRadioDefault4">
                                          Por Config. Familiar
                                        </label>
                                      </div>
                                    
                                </div>
                              
                                <div class="col-10">
                                    {{-- <div v-if="showFirst === 'true'">First view</div> --}}
                                    {{-- @{{form}} --}}
                                    
                                   <transition name="fade">
                                    <template v-if="form.atc">
                                    <form class="form-horizontal form-edit" @submit.prevent="onSearchforParams">
                                        <div class="row justify-content-md-between">
                                            <div class="col col-lg-7 col-xl-3 form-group">
                                                
                                                <div class="form-group">
                                                    <multiselect 
                                                    v-model="form.entity" 
                                                    :options="{{ $ConsEntity->toJson() }}" 
                                                    placeholder="Seleccionar ATCs" 
                                                    label="name" 
                                                    track-by="id" 
                                                    :multiple="false"
                                                    :taggable="true"
                                                    >
                                                </multiselect>
                                                
                                            </div>
                                            <div class="form-group">
                                                
                                                <multiselect 
                                                v-model="form.status" 
                                                :options="{{ $statusName->toJson() }}" 
                                                placeholder="Seleccionar Estados" 
                                                label="name" 
                                                track-by="id" 
                                                :taggable="true"
                                                :multiple="false">
                                            </multiselect>
                                            
                                        </div>
                                        <div class="form-group">
                                           
                                            {{--  <input class="form-control" placeholder="{{ trans('brackets/admin-ui::admin.placeholder.search-by-resolution') }}" v-model="search" @keyup.enter="filter('search', $event.target.value)" /> --}}
                                            <span class="input-group-append">
                                                <button id="btn-reportsocial" type="submit" class="btn btn-primary" >
                                                    <i class="fa fa-search"></i>&nbsp; {{ trans('brackets/admin-ui::admin.btn.search') }}</button>
                                                </span>
                                            </div>
                                            
                                        </div>
                                        <div class="col-sm-auto form-group ">
                                            <select class="form-control" v-model="pagination.state.per_page">
                                                <option value="10">10</option>
                                                <option value="25">25</option>
                                                <option value="100">100</option>
                                            </select>
                                        </div>
                                    </div>
                                </form>
                            </template>
                                   </transition>
                                   
                                   <transition name="fade">
                                    <template v-if="form.coop">
                                    <form class="form-horizontal form-edit" @submit.prevent="onSearchforParams">
                                        <div class="row justify-content-md-between">
                                            <div class="col col-lg-7 col-xl-3 form-group">
                                                
                                                <div class="form-group">
                                                    <multiselect 
                                                    v-model="form.coop" 
                                                    :options="{{ $coops->toJson() }}" 
                                                    placeholder="Seleccionar Cooperativas" 
                                                    label="name" 
                                                    track-by="id" 
                                                    :multiple="false"
                                                    :taggable="true"
                                                    >
                                                </multiselect>
                                                
                                            </div>
                                            <div class="form-group">
                                                
                                                <multiselect 
                                                v-model="form.status" 
                                                :options="{{ $statusName->toJson() }}" 
                                                placeholder="Seleccionar Estados" 
                                                label="name" 
                                                track-by="id" 
                                                :taggable="true"
                                                :multiple="false">
                                            </multiselect>
                                            
                                        </div>
                                        <div class="form-group">
                                           
                                            {{--  <input class="form-control" placeholder="{{ trans('brackets/admin-ui::admin.placeholder.search-by-resolution') }}" v-model="search" @keyup.enter="filter('search', $event.target.value)" /> --}}
                                            <span class="input-group-append">
                                                <button id="btn-reportsocial" type="submit" class="btn btn-primary" >
                                                    <i class="fa fa-search"></i>&nbsp; {{ trans('brackets/admin-ui::admin.btn.search') }}</button>
                                                </span>
                                            </div>
                                            
                                        </div>
                                        <div class="col-sm-auto form-group ">
                                            <select class="form-control" v-model="pagination.state.per_page">
                                                <option value="10">10</option>
                                                <option value="25">25</option>
                                                <option value="100">100</option>
                                            </select>
                                        </div>
                                    </div>
                                </form>
                            </template>
                        </transition> 
                                   <transition name="fade">
                                    <template v-if="form.municipio">
                                    <form class="form-horizontal form-edit" @submit.prevent="onSearchforParams">
                                        <div class="row justify-content-md-between">
                                            <div class="col col-lg-7 col-xl-3 form-group">
                                                
                                                <div class="form-group">
                                                    <multiselect 
                                                    v-model="form.municio" 
                                                    :options="{{ $municipios->toJson() }}" 
                                                    placeholder="Seleccionar Ciudades" 
                                                    label="name" 
                                                    track-by="id" 
                                                    :multiple="false"
                                                    :taggable="true"
                                                    >
                                                </multiselect>
                                                
                                            </div>
                                            <div class="form-group">
                                                
                                                <multiselect 
                                                v-model="form.status" 
                                                :options="{{ $statusName->toJson() }}" 
                                                placeholder="Seleccionar Estatdos" 
                                                label="name" 
                                                track-by="id" 
                                                :taggable="true"
                                                :multiple="false">
                                            </multiselect>
                                            
                                        </div>
                                        <div class="form-group">
                                           
                                            {{--  <input class="form-control" placeholder="{{ trans('brackets/admin-ui::admin.placeholder.search-by-resolution') }}" v-model="search" @keyup.enter="filter('search', $event.target.value)" /> --}}
                                            <span class="input-group-append">
                                                <button id="btn-reportsocial" type="submit" class="btn btn-primary" >
                                                    <i class="fa fa-search"></i>&nbsp; {{ trans('brackets/admin-ui::admin.btn.search') }}</button>
                                                </span>
                                            </div>
                                            
                                        </div>
                                        <div class="col-sm-auto form-group ">
                                            <select class="form-control" v-model="pagination.state.per_page">
                                                <option value="10">10</option>
                                                <option value="25">25</option>
                                                <option value="100">100</option>
                                            </select>
                                        </div>
                                    </div>
                                </form>
                            </template>
                        </transition> 
                        <transition name="fade">
                            <template v-if="form.configfamily">
                                <form class="form-horizontal form-edit" @submit.prevent="onSearchforParams">
                                    <div class="row justify-content-md-between">
                                        <div class="col col-lg-7 col-xl-3 form-group">
                                            <div class="form-group">
                                                        <multiselect 
                                                        v-model="form.config" 
                                                        :options="{{ $configfamily->toJson() }}" 
                                                        placeholder="Seleccionar Config. Familiar" 
                                                        label="name" 
                                                        track-by="id" 
                                                        :taggable="true"
                                                        :multiple="false">
                                                    </multiselect>
                                            </div>
                                            <div class="form-group">
                                           
                                                {{--  <input class="form-control" placeholder="{{ trans('brackets/admin-ui::admin.placeholder.search-by-resolution') }}" v-model="search" @keyup.enter="filter('search', $event.target.value)" /> --}}
                                                <span class="input-group-append">
                                                    <button id="btn-reportsocial" type="submit" class="btn btn-primary" >
                                                        <i class="fa fa-search"></i>&nbsp; {{ trans('brackets/admin-ui::admin.btn.search') }}</button>
                                                    </span>
                                                </div>
                                        </div>
                                    </div>
                                </form>
                            </template>
                        </transition>

                                </div>
                                
                        </div>
                        </div>
                    {{--     @{{data}} --}}
                         {{-- @{{socialLists}} --}}
                        
                        <table v-if="socialLists.total" class="table table-hover table-listing">
                            <thead>
                                <tr>
                                    <th class="bulk-checkbox">
                                        <input class="form-check-input" id="enabled" type="checkbox" v-model="isClickedAll" v-validate="''" data-vv-name="enabled"  name="enabled_fake_element" @click="onBulkItemsClickedAllWithPagination()">
                                        <label class="form-check-label" for="enabled">
                                            #
                                        </label>
                                    </th>
                                    <th is='sortable' :column="'id'">{{ trans('admin.applicant.columns.id') }}</th>
                                    <th is='sortable' :column="'names'">{{ trans('admin.applicant.columns.names') }}</th>
                                    <th is='sortable' :column="'last_names'">{{ trans('admin.applicant.columns.last_names') }}</th>
                                    <th is='sortable' :column="'government_id'" style="text-align: right">{{ trans('admin.applicant.columns.government_id') }}</th>
                                    <th is='sortable' :column="'city_name'">{{ trans('admin.applicant.columns.city_id') }}</th>
                                    <th is='sortable' :column="'total_beneficiaries'">{{ trans('admin.applicant.columns.beneficiaries_short') }}</th>
                                    <th is='sortable' :column="'total_monthly_income'" style="text-align: right">{{ trans('admin.applicant.columns.sum') }}</th>
                                  
                                    <th is='sortable' :column="'fentityname'">{{ trans('admin.applicant.columns.financial_entity') }}</th>
                                    <th is='sortable' :column="'centityname'">{{ trans('admin.applicant.columns.construction_entity') }}</th>
                                 
                                    <th is='sortable' :column="'applicant_status'" style="text-align: center">{{ trans('admin.applicant.columns.applicant_status') }}</th>
                                    <th is='sortable' :column="'created_at'" style="text-align: center">{{ trans('admin.applicant.columns.create_at') }}</th>
                                    <th is='sortable' :column="'created_at'" style="text-align: center">{{ trans('admin.applicant.columns.atc_fec_asig') }}</th>
                                    <th is='sortable' :column="'created_at'" style="text-align: center">{{ trans('admin.applicant.columns.atc_fec_asig_dif') }}</th>
                                  

                                    <th></th>
                                </tr>
                                <tr v-show="(clickedBulkItemsCount > 0) || isClickedAll">
                                    <td class="bg-bulk-info d-table-cell text-center" colspan="20">
                                        <span class="align-middle font-weight-light text-dark">{{ trans('brackets/admin-ui::admin.listing.selected_items') }} @{{ clickedBulkItemsCount }}.  <a href="#" class="text-primary" @click="onBulkItemsClickedAll('/admin/applicants')" v-if="(clickedBulkItemsCount < pagination.state.total)"> <i class="fa" :class="bulkCheckingAllLoader ? 'fa-spinner' : ''"></i> {{ trans('brackets/admin-ui::admin.listing.check_all_items') }} @{{ pagination.state.total }}</a> <span class="text-primary">|</span> <a
                                                    href="#" class="text-primary" @click="onBulkItemsClickedAllUncheck()">{{ trans('brackets/admin-ui::admin.listing.uncheck_all_items') }}</a>  </span>

                                        <span class="pull-right pr-2">
                                            <button class="btn btn-sm btn-danger pr-3 pl-3" @click="bulkDelete('/admin/applicants/bulk-destroy')">{{ trans('brackets/admin-ui::admin.btn.delete') }}</button>
                                        </span>

                                    </td>
                                </tr>
                            </thead>
                            <tbody>
                                <tr v-for="item in socialLists.data"  >

                                    <td class="bulk-checkbox">
                                        <input class="form-check-input" :id="'enabled' + item.id" type="checkbox" v-model="bulkItems[item.id]" v-validate="''" :data-vv-name="'enabled' + item.id"  :name="'enabled' + item.id + '_fake_element'" @click="onBulkItemClicked(item.id)" :disabled="bulkCheckingAllLoader">
                                        <label class="form-check-label" :for="'enabled' + item.id">
                                        </label>
                                    </td>
                                    <td>@{{ item.id }}</td>
                                    <td>@{{ item.names }}</td>
                                    <td>@{{ item.last_names }}</td>
                                    <td style="text-align: right">@{{ item.government_id | numberFormat }}</td>
                                    <td>@{{ item.city ? item.city.name : '' }}</td>
                                    <td style="text-align: center">@{{ item.beneficiaries.length + 1 }}</td>
                                    <td style="text-align: right">@{{ (item.monthly_income ? parseInt(item.monthly_income)  : 0 ) + (item.conyuge ? parseInt(item.conyuge.monthly_income ? item.conyuge.monthly_income : 0) : 0) | numberFormat }}</td>
                                   
                                    <td v-tooltip="item.financial.name">@{{ item.financial ? item.financial.name : '' | limitCharecters(10) }}</td>
                                    <td v-tooltip="item.construction.name">@{{ item.construction.name ? item.construction.name : '' | limitCharecters(10) }}</td>
                                   
                                    <td style="text-align: center">@{{ item.statuses.status.name }}</td>
                                    <td style="text-align: center">@{{ item.created_at| formatDate }}</td>
                                    <td style="text-align: center">@{{ item.atc_fec_asig  }}</td>
                                    <td v-if="item.atc_fec_asig" style="text-align: center">
                                     <span style="font-weight: bold">   @{{compareWithToday(item.atc_fec_asig )}} </span>
                                    </td>
                                    <td v-else style="text-align: center">
                                       0
                                    </td>
                                    
                                    <td>
                                        <div class="row no-gutters">
                                            <div class="col-auto">
                                                <a class="btn btn-sm btn-spinner btn-info" :href="item.resource_url + '/edit'" title="{{ trans('brackets/admin-ui::admin.btn.edit') }}" role="button"><i class="fa fa-edit"></i></a>
                                            </div>
                                         {{--   <form class="col" @submit.prevent="deleteItem(item.resource_url)">
                                                <button type="submit" class="btn btn-sm btn-danger" title="{{ trans('brackets/admin-ui::admin.btn.delete') }}"><i class="fa fa-trash-o"></i></button>
                                            </form>
                                             <div class="col-auto">
                                                <a class="btn btn-sm btn-spinner btn-info" :href="item.resource_url + '/print'" title="{{ trans('brackets/admin-ui::admin.btn.print') }}" role="button"><i class="fa fa-print"></i></a>
                                            </div> --}}
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>

                        <div class="alert alert-danger" role="alert" v-else-if="socialLists.total<1" style=" text-align: center">No se han encontrado registros</div>

                        {{-- @{{form}} --}}
                        {{--  @{{socialLists}}  --}}
                        <div v-if="socialLists.total" class="row">
                            <div class="col-sm">
                                <span class="pagination-caption">Registros desde @{{socialLists.from}}  a @{{socialLists.to}}  de un total de @{{socialLists.total}}  registros.</span>
                            </div>
                        </div>
                   {{--      <div class="row" v-if="pagination.state.total > 0">
                            <div class="col-sm">
                                <span class="pagination-caption">{{ trans('brackets/admin-ui::admin.pagination.overview') }}</span>
                            </div>
                            <div class="col-sm-auto">
                                <pagination></pagination>
                            </div>
                        </div> --}}
                        <hr>
                        
                        <o-pagination
                        v-if="socialLists.total > 0"
                        @change="ListsPage(socialLists.path)"
                        :total="socialLists.total"
                        :current.sync="socialLists.actualPage"
                       
                        :per-page="socialLists.per_page">
                            </o-pagination>





      
                        </div>
                    </div>
                </div>
            </div>
        
    </atc-status>
     
</div>

@endsection
