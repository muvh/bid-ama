@extends('brackets/admin-ui::admin.layout.default')

@section('title', trans('admin.applicant.actions.index'))

@section('body')
<tabs>

    <tab name="Estados Actuales">
        
        <button onclick="location.reload()" class="btn-primary" >
             Actualizar <i class="fa fa-repeat"></i>
        </button>
        {!! $statusChart->container() !!}
        {!! $statusChart->script() !!}
    </tab>

    <tab name="Detalles por cooperativas">
    <applicant-by-coop
        :data="{{ $data->toJson() }}"
        :url="'{{ url('admin/reports/applicants-by-coop') }}'"
        :export-url = "'{{ url('admin/reports/applicants-by-coop/export/') }}'"
        inline-template>

        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="card-header">
                        <i class="fa fa-align-justify"></i>{{ trans('admin.reports.actions.applicants-by-resolution') }}
                        <template v-if="collection.length>0">
                            <export-button v-bind:resolution="collection[0].resolution_number" url="{{ url('admin/reports/applicants-by-resolution/export') }}/" text-button="{{ trans('admin.applicant.actions.export') }}"></export-button>
                        </template>
                    </div>
                    <div class="card-body" v-cloak>
                        <div class="card-block">
                            <form class="form-horizontal form-edit" v-on:sumit.prevent="">
                                <div class="row justify-content-md-between">
                                    <div class="col col-lg-7 col-xl-3 form-group">
                                        <div class="form-group">
                                           
                                            <label for="cooperativas" >Seleccionar Cooperativas</label>
                                            <select class="form-control form-control-sm" name="coop">
                                                @foreach($coops as $coop)
                                                <option value="{{$coop->id}}">{{$coop->name}}</option>
                                                @endforeach
                                            </select>
                                             
                                          
                                        </div>
                               
                          
                                        <div class="form-group">
                                           {{--  <input class="form-control" placeholder="{{ trans('brackets/admin-ui::admin.placeholder.search-by-resolution') }}" v-model="search" @keyup.enter="filter('search', $event.target.value)" /> --}}
                                            <span class="input-group-append">
                                                <button id="btn-reportsocial" type="submit" class="btn btn-primary" >
                                                    <i class="fa fa-search"></i>&nbsp; {{ trans('brackets/admin-ui::admin.btn.search') }}</button>
                                            </span>
                                        </div>
                              
                                    </div>
                                    <div class="col-sm-auto form-group ">
                                        <select class="form-control" v-model="pagination.state.per_page">
                                            <option value="10">10</option>
                                            <option value="25">25</option>
                                            <option value="100">100</option>
                                        </select>
                                    </div>
                                </div>
                            </form>

                            <table class="table table-hover table-listing">
                                <thead>
                                    <tr>
                                        <th is='sortable' :column="'id'">Nro</th>
                                        <th is='sortable' :column="'file_number'">Expediente</th>
                                        <th is='sortable' :column="'admission_data'">Ingreso</th>
                                        <th is='sortable' :column="'applicant_full_name'">Beneficiario</th>
                                        <th is='sortable' :column="'government_id'">CI Nro</th>
                                        <th>Conyuge</th>
                                        <th>CI Nro</th>
                                        <th is='sortable' :column="'marital_status'">Estado Civil</th>
                                        <th is='sortable' :column="'total_monthly_income'">Ingreso</th>
                                        <th is='sortable' :column="'centityname'">ATC</th>
                                        <th is='sortable' :column="'fentityname'">Coop.</th>
                                        <th is='sortable' :column="'city_name'">Ciudad</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr v-for="(item, index) in collection" :key="item.id" :class="bulkItems[item.id] ? 'bg-bulk' : ''">
                                        <td>@{{ item.id }}</td>
                                        <td>@{{ item.file_number }}</td>
                                        <td>@{{ item.admission_data }}</td>
                                        <td>@{{ item.applicant_full_name }}</td>
                                        <td>@{{ item.government_id }}</td>
                                        <td>@{{ item.spouse_data?(item.spouse_data.split('-'))[0]:'' }}</td>
                                        <td>@{{ item.spouse_data?(item.spouse_data.split('-'))[1]:'' }}</td>
                                        <td>@{{ item.marital_status }}</td>
                                        <td>@{{ item.total_monthly_income }}</td>
                                        <td>@{{ item.centityname }}</td>
                                        <td>@{{ item.fentityname }}</td>
                                        <td>@{{ item.city_name }}</td>
                                    </tr>
                                </tbody>
                            </table>
                       {{--       {{ $data->links() }}--}}
                            
                           <div class="row" v-if="pagination.state.total > 0">
                                <div class="col-sm">
                                    <span class="pagination-caption">{{ trans('brackets/admin-ui::admin.pagination.overview') }}</span>
                                </div>
                                <div class="col-sm-auto">
                                    <pagination></pagination>
                                </div>
                            </div> 

                            <div class="no-items-found" v-if="!collection.length > 0">
                                <i class="icon-magnifier"></i>
                                <h3>{{ trans('brackets/admin-ui::admin.index.no_items') }}</h3>
                                <p>{{ trans('brackets/admin-ui::admin.index.try_changing_items') }}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </applicant-by-coop>
    </tab>
</tabs>

@endsection
