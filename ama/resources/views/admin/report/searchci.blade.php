@extends('brackets/admin-ui::admin.layout.default')

@section('title', trans('admin.applicant.actions.social'))

@section('body')
@php($roles = Auth::user()->getRoleNames())
<div class="container-fluid">
    <tabs>
   

        <tab name="Buscar Por CI">
            <listing-search-ci
            :url="'{{ url('admin/reports/searchci') }}'"
            :items="{{$items->toJson()}}"
            v-cloak
            inline-template>
            <div class="row">
                <div class="col">
                    <div class="card">
                        <div class="card-header">
                            <i class="fa fa-align-justify"></i>{{ trans('admin.reports.actions.searchci') }}
                        </div>
                        <div class="card-body" v-cloak>
                            <div class="card-block">
                                
                                <form class="form-horizontal form-edit"  @submit.prevent="onSearchCI">
                                   {{--  <form class="form-horizontal form-edit"  @submit.prevent="onSubmit" > --}}
                                    <div class="row justify-content-md-between">
                                        <div class="col col-lg-7 col-xl-3 form-group">
                                          
                                            <div class="form-group">
                                                                  
                                                <input type="text" required v-model="form.txtci" placeholder="Buscar por Cédula" class="form-control"  name="txtci" > 
                                                
                                            </div>
                                        
                                            <div class="form-group">
                                               {{--  <input class="form-control" placeholder="{{ trans('brackets/admin-ui::admin.placeholder.search-by-resolution') }}" v-model="search" @keyup.enter="filter('search', $event.target.value)" /> --}}
                                                <span class="input-group-append">
                                                    <button id="btn-buscar" type="submit" class="btn btn-primary" >
                                                        <i class="fa fa-search"></i>&nbsp; {{ trans('brackets/admin-ui::admin.btn.search') }}</button>
                                                </span>
                                            </div>
                                  
                                        </div>
                                        <div class="col-sm-auto form-group ">
                                   
                                        </div>
                                    </div>
                                </form>
                            </div>
                           
                                <table class="table table-hover table-listing">
                                    <thead>
                                        <tr>
                                            <th is='sortable' :column="'id'">Nro</th>
                                            <th is='sortable' :column="'file_number'">Expediente</th>
                                            <th is='sortable' :column="'created_at'">Ingreso</th>
                                            <th is='sortable' :column="'names'">Beneficiario</th>
                                            <th is='sortable' :column="'government_id'">CI Nro</th>
                                            <th>Conyuge</th>
                                            <th>CI Nro</th>
                                            <th is='sortable' :column="'marital_status'">Estado Civil</th>
                                            <th is='sortable' :column="'address'">Dirección</th>
                                            <th is='sortable' :column="'neighborhood'">Barrio</th>
                                            <th is='sortable' :column="'status.name'">Status</th>
                                            <th is='sortable' :column="'monthly_income'">Ingreso</th>
                                            <th is='sortable' :column="'constuction.name'">ATC</th>
                                            <th is='sortable' :column="'financial.name'">Coop</th>
                                            <th is='sortable' :column="'city.name'">Telefono</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {{-- <tr v-show="items"> --}}
                                         <tr v-for="item in items" :key="item.id" :class="bulkItems[item.id] ? 'bg-bulk' : ''"> 
                                            <td>@{{ item.id }}</td>
                                            <td>@{{ items.file_number }}</td>
                                             <td style="text-align: center">@{{ item.registration_date ? item.registration_date : item.created_at }}</td>
                                            <td>@{{ item.names}}  @{{item.last_names}}</td>
                                            <td>@{{ item.government_id }}</td>
                                            <td>@{{ item.conyuge ? item.conyuge.names : '' | limitCharecters(20) }}</td>
                                            <td>@{{ item.conyuge ? item.conyuge.government_id : '' | limitCharecters(20) }}</td>
                                            <td>@{{ item.marital_status }}</td>
                                       {{--      <td>@{{ item.address ? item.address : ''  }}</td> --}}
                                            <td>@{{ item.address }}</td>
                                            <td>@{{ item.neighborhood?item.neighborhood:'' }}</td>
                                            <td style="text-align: center">@{{ item.statuses.status.name }}</td>
                                            <td style="text-align: right">@{{ (item.monthly_income ? parseInt(item.monthly_income)  : 0 ) + (item.conyuge ? parseInt(item.conyuge.monthly_income ? item.conyuge.monthly_income : 0) : 0) | numberFormat }}</td>
                                            <td>@{{ item.construction.name }}</td>
                                            <td>@{{ item.financial.name }}</td>
                                            
                                            <td>@{{ item.applicant_get_cellphone?item.applicant_get_cellphone.description:'' }}</td>
                                        {{--    <td>@{{ item }}</td> --}}
                                    <td>
                                            <div class="row no-gutters">
                                              
                                                @if($roles->contains('callcenter'))
{{--                                                 <div class="col-auto">
                                                    <a class="btn btn-sm btn-spinner btn-warning" :href="item.resource_url + '/show'" title="{{ trans('brackets/admin-ui::admin.btn.edit') }}" role="button"><i class="fa fa-edit"></i></a>
                                                </div> --}}
                                                @else
                                                  <div class="col-auto">
                                                    <a class="btn btn-sm btn-spinner btn-info" :href="'../applicants/'+item.id + '/edit'" title="{{ trans('brackets/admin-ui::admin.btn.edit') }}" role="button"><i class="fa fa-pencil"></i></a>
                                                </div>
                                                @endif
                                         
                                            </div>
                                        </td> 
                                        </tr>
                                    </tbody>
                                </table>
    
                             
    
                        
                            </div>{{-- endcard-body --}}
                        </div> {{-- endCARD --}}
                    </div> {{-- endCOL --}}
                </div>    {{-- endrow --}}
            </listing-search-ci>
        </tab>
        
    </tabs>    
</div>

@endsection
