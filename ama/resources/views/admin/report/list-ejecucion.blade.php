@extends('brackets/admin-ui::admin.layout.default')

@section('title', trans('admin.applicant.actions.index'))

@section('body')

    <report-ejecucion :url="'{{ url('admin/reports/listado-ejecuciones') }}'" v-cloak inline-template>
        <div>
            <div class="row">
                <div class="col-md-4">

                    <button class="btn btn-primary" v-on:click="onRefresh(2019)">2019</button>
                    <button class="btn btn-primary" v-on:click="onRefresh(2020)">2020</button>
                    <button class="btn btn-primary" v-on:click="onRefresh(2021)">2021</button>
                    <button class="btn btn-primary" v-on:click="onRefresh(2022)">2022</button>
                    <button class="btn btn-primary" v-on:click="onRefresh(2023)">2023</button>
                    <button class="btn btn-primary" v-on:click="onRefresh(2024)">2024</button>

                    <table class="table table-hover table-listing">
                        <thead>
                            <tr>
                                <th>Meses del @{{ yy }}</th>
                                <th>Metas</th>
                                <th>Total por MES</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr v-for="list in lists">
                                <td>@{{ list.linetime }}</td>
                                <td>@{{ list.obj }}</td>
                                <td>@{{ list.total }}</td>
                            </tr>
                            <tr>
                                <td><b>Total por año</b></td>
                                <td><b>@{{ totalProyeccion }}</b></td>
                                <td><b>@{{ totalAdj }}</b></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="col-md-4">


                    {{-- {!! $chart->container() !!} --}}
                    <line-chart-adj :chart-data="datacollection"></line-chart-adj>
                    {{-- <line-chart :data="onRefresh" :options="{responsive: true, maintainAspectRatio: false}"></line-chart> --}}
                    {{--  <line-chart-adj :cahart-data="onRefresh" :options="{responsive: true, maintainAspectRatio: false}"></line-chart-adj> --}}
                </div>
            </div>




        </div>


    </report-ejecucion>
@endsection
