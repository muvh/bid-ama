@extends('brackets/admin-ui::admin.layout.default')

@section('title', trans('admin.applicant.actions.social'))

@section('body')
<div class="container-fluid">
    <tabs>
   

        <tab name="Buscar Por CI">
            <listing-search-checks
            :url="'{{ url('admin/reports/verificacion-institucional') }}'"
            :items="{{$items}}"
            v-cloak
            inline-template>
            <div class="row">
                <div class="col">
                    <div class="card">
                        <div class="card-header">
                            <i class="fa fa-align-justify"></i>{{ trans('admin.reports.actions.searchci') }}
                        </div>
                        <div class="card-body" v-cloak>
                            <div class="card-block">
                                
                                <form class="form-horizontal form-edit"  @submit.prevent="onSearchCI">
                                   {{--  <form class="form-horizontal form-edit"  @submit.prevent="onSubmit" > --}}
                                    <div class="row justify-content-md-between">
                                        <div class="col col-lg-7 col-xl-3 form-group">
                                          
                                            <div class="form-group">
                                                                  
                                                <input type="text" required v-model="form.txtci" placeholder="Buscar por Cédula" class="form-control"  name="txtci" > 
                                                
                                            </div>
                                        
                                            <div class="form-group">
                                               {{--  <input class="form-control" placeholder="{{ trans('brackets/admin-ui::admin.placeholder.search-by-resolution') }}" v-model="search" @keyup.enter="filter('search', $event.target.value)" /> --}}
                                                <span class="input-group-append">
                                                    <button id="btn-buscar" type="submit" class="btn btn-primary" >
                                                        <i class="fa fa-search"></i>&nbsp; {{ trans('brackets/admin-ui::admin.btn.search') }}</button>
                                                </span>
                                            </div>
                                  
                                        </div>
                                        <div class="col-sm-auto form-group ">
                                   
                                        </div>
                                    </div>
                                </form>
                            </div>
                           
                                
    
                             
    
                        
                            </div>{{-- endcard-body --}}
                        </div> {{-- endCARD --}}
                    </div> {{-- endCOL --}}
                </div>    {{-- endrow --}}
            </listing-search-checks>
        </tab>
        
    </tabs>    
</div>

@endsection
