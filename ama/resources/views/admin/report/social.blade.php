@extends('brackets/admin-ui::admin.layout.default')

@section('title', trans('admin.applicant.actions.social'))

@section('body')
<div class="container-fluid">

    <applicant-listing
        :data="{{ $data->toJson() }}"
        :url="'{{ url('admin/reports/applicants-by-resolution-social/') }}'"
        :export-url = "'{{ url('admin/reports/applicants-by-resolution-social/export/') }}'"
        :status={{$status}}
        v-cloak
        inline-template>

        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="card-header">

                        <i class="fa fa-align-justify"></i>{{ trans('admin.reports.actions.applicants-by-resolution-social') }}
                        <template v-if="collection.length>0">
                            <export-button-social  url="{{ url('admin/reports/applicants-by-resolution-social/export/'.$dateto.'/'.$datefrom.'/'.$status.'/') }}" text-button="{{ trans('admin.applicant.actions.export') }}"></export-button-social>
                        </template>

                    </div>
                    <div class="card-body" v-cloak>
                        <div class="card-block">
                            
                            <form class="form-horizontal form-edit" @submit.prevent="onSearchforParams">
                                <div class="row justify-content-md-between">
                                    <div class="col col-lg-7 col-xl-3 form-group">
                                        <div class="form-group">
                                           
                                                        <datetime  v-model="form.date_to" :config="datePickerConfig"  class="flatpickr" :class="{'form-control-danger': errors.has('date_to'), 'form-control-success': fields.date_from && fields.date_to.valid}" id="date_to" name="date_to" placeholder="Fecha desde"></datetime>
                                                    <div v-if="errors.has('date_to')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('date_from') }}</div>
                                        </div>
                                        <div class="form-group">
                                                        <datetime  v-model="form.date_from" :config="datePickerConfig" class="flatpickr" :class="{'form-control-danger': errors.has('date_from'), 'form-control-success': fields.date_from && fields.date_from.valid}" id="date_from" name="date_from" placeholder="Fecha hasta"></datetime>
                                                    <div v-if="errors.has('date_from')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('date_from') }}</div>
                                                </div>
                                                <div class="form-group">
                                                
                                                    <select  name="status" id="status" v-model="form.status" class="form-control form-control-success"  placeholder="{{ trans('admin.documents.actions.status')  }}">
                                                            <option selected value="">Seleccione estados</option>
                                                        @foreach ($statusName as $sn)
                                                            <option value="{{$sn->id}}">
                                                                @isset($sn->prefix)
                                                                {{$sn->prefix}} -

                                                                @else 
                                                                {{$sn->prefix}} 
                                                                
                                                                @endif
                                                                    {{$sn->name}} 
                                                                </option>
                                                        @endforeach
                                                    </select>
                                                    
                                            </div>
                                        <div class="form-group">
                                           {{--  <input class="form-control" placeholder="{{ trans('brackets/admin-ui::admin.placeholder.search-by-resolution') }}" v-model="search" @keyup.enter="filter('search', $event.target.value)" /> --}}
                                            <span class="input-group-append">
                                                <button id="btn-reportsocial" type="submit" class="btn btn-primary" >
                                                    <i class="fa fa-search"></i>&nbsp; {{ trans('brackets/admin-ui::admin.btn.search') }}</button>
                                            </span>
                                        </div>
                              
                                    </div>
                                    <div class="col-sm-auto form-group ">
                                        <select class="form-control" v-model="pagination.state.per_page">
                                            <option value="10">10</option>
                                            <option value="25">25</option>
                                            <option value="100">100</option>
                                        </select>
                                    </div>
                                </div>
                            </form>
                        </div>
                       
                            <table class="table table-hover table-listing">
                                <thead>
                                    <tr>
                                        <th is='sortable' :column="'id'">Nro</th>
                                        <th is='sortable' :column="'file_number'">Expediente</th>
                                        <th is='sortable' :column="'created_at'">Ingreso</th>
                                        <th is='sortable' :column="'names'">Beneficiario</th>
                                        <th is='sortable' :column="'government_id'">CI Nro</th>
                                        <th>Conyuge</th>
                                        <th>CI Nro</th>
                                        <th is='sortable' :column="'marital_status'">Estado Civil</th>
                                        <th is='sortable' :column="'address'">Dirección</th>
                                        <th is='sortable' :column="'neighborhood'">Barrio</th>
                                        <th is='sortable' :column="'status.name'">Status</th>
                                        <th is='sortable' :column="'monthly_income'">Ingreso</th>
                                        <th is='sortable' :column="'constuction.name'">ATC</th>
                                        <th is='sortable' :column="'financial.name'">Coop .</th>
                                        <th is='sortable' :column="'city.name'">Telefono</th>

                                    </tr>
                                </thead>
                                <tbody>
                                    <tr v-for="item in socialLists" :key="item.id" :class="bulkItems[item.id] ? 'bg-bulk' : ''">
                                        <td>@{{ item.id }}</td>
                                        <td>@{{ item.file_number }}</td>
                                        <td style="text-align: center">@{{ item.created_at| formatDate }}</td>
                                        <td>@{{ item.names}}  @{{item.last_names}}</td>
                                        <td>@{{ item.government_id }}</td>
                                        <td>@{{ item.conyuge ? item.conyuge.names : '' | limitCharecters(20) }}</td>
                                        <td>@{{ item.conyuge ? item.conyuge.government_id : '' | limitCharecters(20) }}</td>
                                        <td>@{{ item.marital_status }}</td>
                                        <td>@{{ item.address ? item.address : ''  }}</td>
                                        <td>@{{ item.neighborhood?item.neighborhood:'' }}</td>
                                        <td style="text-align: center">@{{ item.statuses.status.name }}</td>
                                        <td style="text-align: right">@{{ (item.monthly_income ? parseInt(item.monthly_income)  : 0 ) + (item.conyuge ? parseInt(item.conyuge.monthly_income ? item.conyuge.monthly_income : 0) : 0) | numberFormat }}</td>
                                        <td>@{{ item.financial.name }}</td>
                                        <td>@{{ item.construction.name }}</td>
                                        
                                        <td>@{{ item.applicant_get_cellphone?item.applicant_get_cellphone.description:'' }}</td>
                                     {{--    <td>@{{ item }}</td> --}}
                                     <td>
                                        <div class="row no-gutters">
                                            <div class="col-auto">
                                                <a class="btn btn-sm btn-spinner btn-info" :href="'../applicants/'+item.id + '/edit'" title="{{ trans('brackets/admin-ui::admin.btn.edit') }}" role="button"><i class="fa fa-pencil"></i></a>
                                            </div>

                                        {{--     <div class="col-auto">
                                                <a class="btn btn-sm btn-spinner btn-info" :href="item.resource_url + '/print'" title="{{ trans('brackets/admin-ui::admin.btn.print') }}" role="button"><i class="fa fa-print"></i></a>
                                            </div> --}}
                                        </div>
                                    </td>
                                    </tr>
                                </tbody>
                            </table>

{{-- <pagination :data="socialLists">
    <template #prev-nav>
        <span>&lt; Previous</span>
    </template>
    <template #next-nav>
        <span>Next &gt;</span>
    </template>
</pagination> --}}

<v-paginator :ref="vpaginator" :data="socialLists" :resource_url="resource_url" @update="updateResource"></v-paginator


                            <div class="row" v-if="pagination.state.total > 0">
                                <div class="col-sm">
                                    <span class="pagination-caption">{{ trans('brackets/admin-ui::admin.pagination.overview') }}</span>
                                </div>
                                <div class="col-sm-auto">
                                    <pagination></pagination>
                                </div>
                            </div>

                   {{--          <div class="no-items-found" v-if="!collection.length > 0">
                                <i class="icon-magnifier"></i>
                                <h3>{{ trans('brackets/admin-ui::admin.index.no_items') }}</h3>
                                <p>{{ trans('brackets/admin-ui::admin.index.try_changing_items') }}</p>
                            </div> --}}
                        </div>
                    </div>
                </div>
            </div>
        
    </applicant-listing>
     
</div>

@endsection
