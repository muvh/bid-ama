@extends('brackets/admin-ui::admin.layout.default')

@section('title', trans('admin.applicant.actions.social'))

@section('body')

    <applicant-listing-coop
        :data="{{ $data->toJson() }}"
        :url="'{{ url('admin/reports/applicants-coop-status') }}'"
        :export-url = "'{{ url('admin/reports/applicants-by-resolution-social/export/') }}'"
        :date_to="'{{$date_to}}'"
        :date_from="'{{$date_from}}'"
        inline-template>

        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="card-header">

                        <i class="fa fa-align-justify"></i>{{ trans('admin.reports.actions.applicants-coop-status') }}
                        <template v-if="collection.length>0">
                            <export-button-social  url="{{ url('admin/reports/applicants-by-resolution-social/export/'.$date_to.'/'.$date_from.'/') }}" text-button="{{ trans('admin.applicant.actions.export') }}"></export-button-social>
                        </template>

                    </div>
                    <div class="card-body" v-cloak>
                        <div class="card-block">
                            
                            <form class="form-horizontal form-edit" v-on:sumit.prevent="">
                                <div class="row justify-content-md-between">
                                    <div class="col col-lg-7 col-xl-3 form-group">
                                        <div class="form-group">
                                           
                                                    <datetime  
                                                        v-model="form.date_to" 
                                                        :config="datePickerConfig" 
                                                        v-validate="'date_format:dd-MM-yyyy'" 
                                                        
                                                        class="flatpickr" 
                                                        :class="{'form-control-danger': errors.has('date_to'), 'form-control-success': fields.date_from && fields.date_to.valid}" 
                                                        id="date_to" 
                                                        name="date_to" 
                                                        placeholder="Fecha desde">
                                                    </datetime>
                                                    <div v-if="errors.has('date_to')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('date_to') }}</div>
                                             
                                          
                                        </div>
                                        <div class="form-group">
                                              
                                                        <datetime  
                                                        v-model="form.date_from" 
                                                        :config="datePickerConfig" 
                                                        v-validate="'date_format:dd-MM-yyyy'" 
                                                        class="flatpickr" 
                                                        :class="{'form-control-danger': errors.has('date_from'), 'form-control-success': fields.date_from && fields.date_from.valid}" 
                                                        id="date_from" 
                                                        name="date_from" 
                                                        placeholder="Fecha hasta">
                                                    </datetime>
                                                    <div v-if="errors.has('date_from')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('date_from') }}</div>
                                        
                                        </div>
                                        <div class="form-group">
                                            
                                          
                                            <input class="form-check-input" id="checkbox" type="checkbox" v-model="form.dispatched" v-validate="'required'" data-vv-name="dispatched"  name="dispatched">
                                            <label class="form-check-label" for="checkbox">Enviados</label>
                                            
                                            <input type="hidden" name="dispatched" :value="form.dispatched">
                                            <input type="hidden" name="type_report" value="{{request()->get('type_report')}}">
                                              
  {{--                                              <datetime  v-model="form.date_from" :config="datePickerConfig" v-validate="'date_format:yyyy-MM-dd'" class="flatpickr" :class="{'form-control-danger': errors.has('date_from'), 'form-control-success': fields.date_from && fields.date_from.valid}" id="date_from" name="date_from" placeholder="Fecha hasta"></datetime>
                                                    <div v-if="errors.has('date_from')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('date_from') }}</div> --}}
                                        
                                        </div>
                                        <div class="form-group">
                                           {{--  <input class="form-control" placeholder="{{ trans('brackets/admin-ui::admin.placeholder.search-by-resolution') }}" v-model="search" @keyup.enter="filter('search', $event.target.value)" /> --}}
                                            <span class="input-group-append">
                                                <button id="btn-reportsocial" type="submit" class="btn btn-primary" >
                                                    <i class="fa fa-search"></i>&nbsp; {{ trans('brackets/admin-ui::admin.btn.search') }}</button>
                                            </span>
                                        </div>
                              
                                    </div>
                                    <div class="col-sm-auto form-group ">
                                        <select class="form-control" v-model="pagination.state.per_page">
                                            <option value="10">10</option>
                                            <option value="25">25</option>
                                            <option value="100">100</option>
                                        </select>
                                    </div>
                                </div>
                            </form>
                        </div>
                        
                            <table class="table table-hover table-listing">
                                <thead>
                                    <tr>
                                        <th is='sortable' :column="'id'">Nro</th>
                                        <th is='sortable' :column="'applicantsid'">Codigo Solicitante </th>
                                        <th is='sortable' :column="'file_number'">Expediente</th>
                                        <th is='sortable' :column="'admission_data'">Ingreso</th>
                                        <th is='sortable' :column="'applicant_full_name'">Beneficiario</th>
                                        <th is='sortable' :column="'government_id'">CI Nro</th>
                                        <th>Conyuge</th>
                                        <th>CI Nro</th>
                                        <th is='sortable' :column="'marital_status'">Estado Civil</th>
                                        <th is='sortable' :column="'address'">Dirección</th>
                                        <th is='sortable' :column="'neighborhood'">Barrio</th>
                                        <th is='sortable' :column="'description'">Observacion</th>
                                        <th is='sortable' :column="'total_monthly_income'">Ingreso</th>
                                        <th is='sortable' :column="'centityname'">ATC</th>
                                        <th is='sortable' :column="'fentityname'">Coop.</th>
                                        <th is='sortable' :column="'city_name'">Ciudad</th>
                                        <th is='sortable' :column="'dispatched'">Enviados</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr v-for="(item, index) in collection" :key="item.id" :class="bulkItems[item.id] ? 'bg-bulk' : ''">
                                        <td>@{{ item.id }}</td>
                                        <td>@{{ item.applicantsid }}</td>
                                        <td>@{{ item.file_number }}</td>
                                        <td>@{{ new Date(item.admission_data) | dateFormat('DD-MM-YYYY')  }}</td>
                                        <td>@{{ item.applicant_full_name }}</td>
                                        <td>@{{ item.government_id }}</td>
                                        <td>@{{ item.spouse_data?(item.spouse_data.split('-'))[0]:'' }}</td>
                                        <td>@{{ item.spouse_data?(item.spouse_data.split('-'))[1]:'' }}</td>
                                        <td>@{{ item.marital_status }}</td>
                                        <td>@{{ item.address }}</td>
                                        <td>@{{ item.neighborhood }}</td>
                                        <td>@{{ item.description }}</td>
                                        <td>@{{ item.total_monthly_income }}</td>
                                        <td>@{{ item.centityname }}</td>
                                        <td>@{{ item.fentityname }}</td>
                                        <td>@{{ item.city_name }}</td>
                                        @if(request()->get('type_report')=='atc' )
                                        <td>@{{ item.dispatched2}}</td>
                                        @endif
                                        @if(request()->get('type_report')=='coop' )
                                        <td>@{{ item.dispatched}}</td>
                                        @endif
                                    </tr>
                                </tbody>
                            </table>

                            <div class="row" v-if="pagination.state.total > 0">
                                <div class="col-sm">
                                    <span class="pagination-caption">{{ trans('brackets/admin-ui::admin.pagination.overview') }}</span>
                                </div>
                                <div class="col-sm-auto">
                                    <pagination></pagination>
                                </div>
                            </div>

                            <div class="no-items-found" v-if="!collection.length > 0">
                                <i class="icon-magnifier"></i>
                                <h3>{{ trans('brackets/admin-ui::admin.index.no_items') }}</h3>
                                <p>{{ trans('brackets/admin-ui::admin.index.try_changing_items') }}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </applicant-listing>

@endsection
