@extends('brackets/admin-ui::admin.layout.default')

@section('title', trans('admin.applicant.actions.social'))

@section('body')

<form class="form-horizontal form-create" action="{{ url('admin/reports/filtrosEtapas') }}" method="post">
    {{ csrf_field() }}
    <div class="row">
        <div class="col">
            <div class="card">
                <div class="card-header">
                    <i class="fa fa-plus"></i> Listado de Área Social
                </div>
                <div class="card-body">
                    <p style="color:red"> * Este reporte descarga un archivo Excel con los parametros indicados</p>
                    
                    <div class="row">
                        <div class="form-group col-sm-4">
                            <label for="address" >ATC'S</label>
                            <select name="atc" class="form-control">
                                <option value="">Seleccione Atc</option>
                                @foreach ($atc as $at)
                                    <option value="{{ $at['id'] }}" {{ old('atc',isset($atcic)?$atcic:'') == $at['id'] ? "selected":""}}>{{ $at['name'] }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-sm-4">
                            <label for="address" >Cooperativas</label>
                            <select name="coop" class="form-control">
                                <option value="">Seleccione Cooperativa</option>
                                @foreach ($coops as $coop)
                                    <option value="{{ $coop['id'] }}" {{ old('atc',isset($coopic)?$coopic:'') == $coop['id'] ? "selected":""}}>{{ $coop['name'] }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-sm-4">
                            <label for="address" >Municipios</label>
                            <select name="muni" class="form-control">
                                <option value="">Seleccione Municipio</option>
                                @foreach ($municipios as $muni)
                                    <option value="{{ $muni['id'] }}" {{ old('atc',isset($muniic)?$muniic:'') == $muni['id'] ? "selected":""}}>{{ $muni['name'] }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-sm-4">
                            <label for="address" >Config Familiar</label>
                            <select name="config" class="form-control">
                                <option value="">seleccione config familiar</option>
                                @foreach ($configfamily as $cf)
                                    <option value="{{ $cf['id'] }}" {{ old('atc',isset($configic)?$configic:'') == $cf['id'] ? "selected":""}}>{{ $cf['name'] }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-sm-4">
                            <label for="address" >Etapas</label>

                            <div class="form-check">
                                <input name="nameetapas[]" class="form-check-input" type="checkbox" value="INS" id="defaultCheck1">
                                <label class="form-check-label" for="defaultCheck1">
                                 INSCRIPCIONES
                                </label>
                              </div>
                              <div class="form-check">
                                <input name="nameetapas[]" class="form-check-input" type="checkbox" value="POS" id="defaultCheck2">
                                <label class="form-check-label" for="defaultCheck2">
                                  POSTULACIONES
                                </label>
                              </div>
                              <div class="form-check">
                                <input name="nameetapas[]" class="form-check-input" type="checkbox" value="ADJ" id="defaultCheck3">
                                <label class="form-check-label" for="defaultCheck3">
                                  ADJUDICACIONES
                                </label>
                              </div>
                              <div class="form-check">
                                <input name="nameetapas[]" class="form-check-input" type="checkbox" value="INH" id="defaultCheck4">
                                <label class="form-check-label" for="defaultCheck4">
                                  INHABILITADOs
                                </label>
                              </div>
                            
                        </div>
                        {{--<div class="form-group col-sm-4">
                            <label for="address" >Cooperativas</label>
                            <input type="text"   class="form-control" id="address" name="address" placeholder="{{ trans('admin.applicant.columns.address') }}">
                        </div>--}}
                    </div>
                </div>
                <div class="card-footer">
                    <button type="submit" class="btn btn-primary pull-right" >
                        <i class="fa" ></i>
                        Buscar
                    </button>
                </div>
            </div>
        </div>
    </div>

</form>

@endsection
