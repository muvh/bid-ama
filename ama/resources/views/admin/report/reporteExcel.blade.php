<h2 style="align-content: center">LISTADO AREA SOCIAL</h2>
<h4>ATC: {{ $atc }}</h4>
<h4>COOPERATIVA: {{ $coop }}</h4>
<h4>MUNICIPIO: {{ $muni }}</h4>
<h4>CONF. FAMILIAR: {{ $config }}</h4>
<h4>ESTADO: {{ $estado }}</h4>
<table>
    <thead>
        <tr>
            {{-- <th style="background-color: #001C54; color: white;">NRO</th>
        <th style="background-color: #001C54; color: white;">CODIGO</th> --}}
            <th style="background-color: #001C54; color: white;">#</th>
            <th style="background-color: #001C54; color: white;">NOMBRE</th>
            <th style="background-color: #001C54; color: white;">DOCUMENTO</th>
            <th style="background-color: #001C54; color: white;">DIRECCION</th>
            <th style="background-color: #001C54; color: white;">TELEFONO</th>
            <th style="background-color: #001C54; color: white;">CIUDAD</th>
            <th style="background-color: #001C54; color: white;">BENEF</th>
            <th style="background-color: #001C54; color: white;">CONFIG FAMILIAR</th>
            <th style="background-color: #001C54; color: white;">INGRESO FAMILIAR</th>
            <th style="background-color: #001C54; color: white;">COOPERATIVA</th>
            <th style="background-color: #001C54; color: white;">ATC</th>
            <th style="background-color: #001C54; color: white;">ETAPA</th>
            <th style="background-color: #001C54; color: white;">ESTADO</th>
            <th style="background-color: #001C54; color: white;">FECHA INSCRIPCION</th>
            <th style="background-color: #001C54; color: white;">ATC FECHA ASIG</th>
            <th style="background-color: #001C54; color: white;">DIF DIAS ASIG</th>
            <th style="background-color: #001C54; color: white;">80% ENVIADO CAC</th>
            <th style="background-color: #001C54; color: white;">20% ENVIADO CAC</th>
            <th style="background-color: #001C54; color: white;">Discapacidad</th>
            <th style="background-color: #001C54; color: white;">Enfermedad</th>
            <th style="background-color: #001C54; color: white;">Observaciones</th>
        </tr>
    </thead>
    <tbody>
        @php
            $aux = 1;
        @endphp
        @foreach ($invoices as $index => $invoice)
            @php
                $date = $invoice->atc_fec_asig;
                $datework = Carbon\Carbon::createFromDate($date);
                $now = Carbon\Carbon::now();
                $testdate = $datework->diffInDays($now);

            @endphp
            <tr>
                <td style="width: 30pt; border: 1px solid black ;">{{ $index + 1 }}</td>
                <td style="width: 200pt; border: 1px solid black ;">{{ $invoice->names . ' ' . $invoice->last_names }}
                </td>
                <th style="width: 70pt; border: 1px solid black ;">{{ $invoice->government_id }}</th>
                <th style="width: 70pt; border: 1px solid black ;">{{ $invoice->address }}</th>
                <th style="width: 70pt; border: 1px solid black ;">
                    {{ $invoice->applicantGetCellphone ? $invoice->applicantGetCellphone->description : '' }}</th>
                <th style="width: 150pt; border: 1px solid black ;">{{ $invoice->city->name }}</th>
                <td style="width: 50pt; border: 1px solid black ;">{{ $invoice->beneficiaries->count() }}</td>
                <th style="width: 150pt; border: 1px solid black ;">
                    {{ $invoice->familiarSetting ? $invoice->familiarSetting->name : '' }}</th>
                <th style="width: 100pt; border: 1px solid black ;">{{ $invoice->monthly_income }}</th>
                <th style="width: 150pt; border: 1px solid black ;">{{ $invoice->financial->name }}</th>
                <th style="width: 150pt; border: 1px solid black ;">{{ $invoice->atc ? $invoice->atc->name : 'N/A' }}
                </th>
                <th style="width: 100pt; border: 1px solid black ;">
                    @if ($invoice->statuses->status->prefix == 'INS')
                        {{ 'INSCRIPCION' }}
                    @endif
                    @if ($invoice->statuses->status->prefix == 'POS')
                        {{ 'POSTULACION' }}
                    @endif
                    @if ($invoice->statuses->status->prefix == 'ADJ')
                        {{ 'ADJUDICACION' }}
                    @endif
                    @if ($invoice->statuses->status->prefix == 'INH')
                        {{ 'INHABILITADO' }}
                    @endif

                </th>
                <th style="width: 100pt; border: 1px solid black ;">{{ $invoice->statuses->status->name }}</th>
                <th style="width: 100pt; border: 1px solid black ;">
                    {{ $invoice->registration_date ? date('d/m/Y', strtotime($invoice->registration_date)) : date('d/m/Y', strtotime($invoice->created_at)) }}
                </th>
                <th style="width: 100pt; border: 1px solid black ;">
                    {{ $invoice->atc_fec_asig ? date('d/m/Y', strtotime($invoice->atc_fec_asig)) : '' }}</th>
                <th style="width: 100pt; border: 1px solid black ;">{{ $testdate ? $testdate : '' }}</th>
                <th style="width: 100pt; border: 1px solid black ;">
                    {{ $invoice->enviocac ? Carbon\Carbon::parse($invoice->enviocac->created_at)->format('d/m/Y') : '' }}
                </th>
                <th style="width: 100pt; border: 1px solid black ;">
                    {{ $invoice->envio20cac ? Carbon\Carbon::parse($invoice->envio20cac->created_at)->format('d/m/Y') : '' }}
                </th>
                <th style="width: 150pt; border: 1px solid black ;">
                    {{ $invoice->reportDisabilities ? $invoice->reportDisabilities->disability->name : '' }}</th>
                <th style="width: 150pt; border: 1px solid black ;">
                    {{ $invoice->reportDiseases ? $invoice->reportDiseases->disease->name : '' }}</th>
                <th style="width: 800pt; border: 1px solid black ;">

                    <ul>
                        @foreach ($invoice->applicantStatuses as $item)
                            <li>Fecha: {{ $item['created_at'] }} Descripcion: {{ $item['description'] }}</li>
                        @endforeach
                    </ul>

                </th>
                {{-- <td style="width: 25pt; align-content: center; border: 1px solid black ;">{{ $aux }}</td>
            <td style="width: 40pt; border: 1px solid black ;">{{ $invoice->call->Position->acronym }}</td>
            <td style="width: 200pt; border: 1px solid black ;">{{ $invoice->resume->names.' '.$invoice->resume->last_names }}</td>
            <th style="width: 100pt; border: 1px solid black ;">{{ $invoice->resume->birthdate }}</th>
            <th style="width: 70pt; border: 1px solid black ;">{{ $invoice->resume->government_id }}</th>
            <th style="width: 200pt; border: 1px solid black ;">{{ $invoice->resume->email }}</th>
            <th style="width: 80pt; border: 1px solid black ;">{{ $invoice->statuses->description }}</th>
            <th style="border: 1px solid black ; background-color: {{ $invoice->statuses->status->name == "Admitido" ? "#024e16" : "#BA3A3B" }} ; width: 60pt; color: white;">{{ $invoice->statuses->status->name == "Admitido" ? "Cumple" : 'No cumple' }}</th> --}}
            </tr>
            @php
                $aux++;
            @endphp
        @endforeach
    </tbody>
</table>
