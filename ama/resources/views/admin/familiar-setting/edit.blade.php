@extends('brackets/admin-ui::admin.layout.default')

@section('title', trans('admin.familiar-setting.actions.edit', ['name' => $familiarSetting->name]))

@section('body')

    <div class="container-xl">
        <div class="card">

            <familiar-setting-form
                :action="'{{ $familiarSetting->resource_url }}'"
                :data="{{ $familiarSetting->toJson() }}"
                v-cloak
                inline-template>
            
                <form class="form-horizontal form-edit" method="post" @submit.prevent="onSubmit" :action="action" novalidate>


                    <div class="card-header">
                        <i class="fa fa-pencil"></i> {{ trans('admin.familiar-setting.actions.edit', ['name' => $familiarSetting->name]) }}
                    </div>

                    <div class="card-body">
                        @include('admin.familiar-setting.components.form-elements')
                    </div>
                    
                    
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary" :disabled="submiting">
                            <i class="fa" :class="submiting ? 'fa-spinner' : 'fa-download'"></i>
                            {{ trans('brackets/admin-ui::admin.btn.save') }}
                        </button>
                    </div>
                    
                </form>

        </familiar-setting-form>

        </div>
    
</div>

@endsection