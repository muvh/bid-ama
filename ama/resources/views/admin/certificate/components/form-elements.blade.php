<div class="form-group row align-items-center" :class="{'has-danger': errors.has('fecha_tr'), 'has-success': fields.fecha_tr && fields.fecha_tr.valid }">
    <label for="fecha_tr" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.certificate.columns.fecha_tr') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-sm-8'">
        <div class="input-group input-group--custom">
            <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
            <datetime v-model="form.fecha_tr" :config="datePickerConfig" v-validate="'required|date_format:yyyy-MM-dd HH:mm:ss'" class="flatpickr" :class="{'form-control-danger': errors.has('fecha_tr'), 'form-control-success': fields.fecha_tr && fields.fecha_tr.valid}" id="fecha_tr" name="fecha_tr" placeholder="{{ trans('brackets/admin-ui::admin.forms.select_a_date') }}"></datetime>
        </div>
        <div v-if="errors.has('fecha_tr')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('fecha_tr') }}</div>
    </div>
</div>
<div class="form-group row align-items-center" :class="{'has-danger': errors.has('applicant'), 'has-success': fields.applicant && fields.applicant.valid }">
    <label for="sat" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.user.columns.applicant') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <multiselect
            v-model="form.applicant"
            :options="applicants"
            :multiple="false"
            track-by="id"
            :taggable="true"
            tag-placeholder=""
            :custom-label="customLabel"
            placeholder="">
        </multiselect>
        {{--<input type="text" v-model="form.sat_ruc" v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('sat_ruc'), 'form-control-success': fields.sat_ruc && fields.sat_ruc.valid}" id="sat_ruc" name="sat_ruc" placeholder="{{ trans('admin.user.columns.sat_ruc') }}">--}}
        <div v-if="errors.has('applicant')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('applicant') }}</div>
    </div>
</div>


