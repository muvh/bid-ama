@php
$naRed='<span id="nda">N/A</span>';
@endphp
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
  <header>
    <img src="{{ public_path('logo-cab-transf-recursos.jpg') }}" >
  </header>
  <footer>
    <img src="{{ public_path('logo-pie-transf-recursos.png') }}" width=" 750px" >
</footer>
<main>
    <style type="text/css">

        .tg  {border-collapse:collapse;border-spacing:0;width:80%;margin: -10px 80px}
        .tg td{border-color:black;border-style:solid;border-width:1px;font-family:Arial, sans-serif;font-size:12px;
          overflow:hidden;padding:10px 5px;word-break:normal;}
        .tg th{border-color:black;border-style:solid;border-width:1px;font-family:Arial, sans-serif;font-size:12px;
          font-weight:normal;overflow:hidden;padding:10px 5px;word-break:normal;}
        .tg .tg-0pky{border-color:inherit;text-align:left;vertical-align:top}
        header {
                position: fixed;
                top: -30px;
                left: 90px;
                right: 0px;
                height: 50px;
                font-size: 20px !important;
            }

            footer {
                position: fixed;
                bottom: 40px;
                left: 0px;
                right: 0px;
                height: 50px;
                font-size: 20px !important;
            }
            .noBorder {
                border:none !important;
            }
            #nda{color: red;}
        </style>

<br>
<br>
        <h2 style="text-align: center; padding-top: 20px">CERTIFICADO FINAL DE OBRA</h2>
        <h4 style="text-align: center; ">LEY Nº 5.665/16</h4>
        <h4 style="text-align: center; ">PROGRAMA DE MEJORAMIENTO DE VIVIENDA Y DEL HABITAT</h4>
        <div style="text-align: center; ">
            <img  src="{{ public_path('logo_ama.png') }}" >
        </div>

        <h4 style="text-align: center;">COMPONENTE I</h4>

        <p style="padding-left: 40px; padding-right: 40px; text-align: justify;
        text-justify: inter-word;">
        El Ministerio de Urbanismo, Vivienda y Hábitat (MUVH), a través de la UEP- BID certifica que, la ATC  <strong>ATC {!! $applicant->atc ? $applicant->atc->name : $naRed !!}</strong>  con
        <strong>RUC {!! isset($applicant->atc->const_ruc) ? $applicant->atc->const_ruc : $naRed !!}</strong> , representado por
        <strong>{!! $applicant->atc ? $applicant->atc->repre : $naRed !!}</strong> con <strong>C.I. N° {!! isset($applicant->atc->nro_ci) ? number_format(str_replace('.','',$applicant->atc->nro_ci),0,'.','.' ) : $naRed !!}</strong>, 
        ha cumplido con todos los parámetros requeridos para su participación en el “Proyecto de Mejoramiento y/o Ampliación de Vivienda en el Área Metropolitana de Asunción”; y conforme al ACTA DE RECEPCIÓN DE OBRAS de fecha 
        @if(@$applicant->work[0])
          @foreach ($applicant->work[0]->obrasfecha as $obra)
                  @if(@$obra->type_check_id==2)
                    <strong> {{ date('d/m/Y', strtotime($obra->fecha_check)) }}</strong>,
              @endif 
          @endforeach
        @else 
        <strong>{!! $naRed !!}</strong>,
        @endif
        finalizado con éxito la vivienda perteneciente a <strong> {{ $applicant['names'] }} {{ $applicant['last_names'] }} </strong>
        con <strong>C.I. N°  
          @php
              
              $mystring = $applicant['government_id'];
              $findme   = 'B';
              $pos = strpos($mystring, $findme);
              
              if ($pos !== false) {
                echo $applicant['government_id'];
              } else {
                echo number_format($applicant['government_id'] ? $applicant['government_id'] : '123',0,'.','.' );  
              }
              @endphp
              
              
              
              
            </strong>,
        con SVS aprobado por Resolución N° <strong>{!! $applicant['resolution_number']? $applicant['resolution_number']: $naRed !!} </strong> con fecha <strong>{!! $applicant['resolution_date']?date('d/m/Y', strtotime($applicant['resolution_date'])): $naRed !!} </strong>, cuya propiedad está ubicada en la Ciudad de <strong>{{ $applicant->city->name }}</strong>
        </p>

        @php
            $date = \Carbon\Carbon::parse($fecha);
        @endphp

        <p style="padding-left: 40px; padding-right: 40px; padding-top: 20px; text-align: right;">Asuncion, {{ date('d',strtotime($fecha)) }} de {{ $date->formatLocalized('%B') }} de {{ date('Y',strtotime($fecha)) }}</p>

        <table style="padding-left: 40px; padding-right: 40px;   width: 100%; padding-top: 150px">
            <tr >
                <td style="border-bottom: 1px solid black;"></td>
                <td></td>
                <td style="border-bottom: 1px solid black"></td>
            </tr>
            <tr>
                <td style="text-align: center"><strong>Coordinación Técnica – AMA-BID</strong></td>
                <td style="width: 10%"></td>
                <td style="text-align: center "><strong>Dirección – AMA-BID</strong></td>
            </tr>
        </table>

        <table style="padding-left: 60px; padding-right: 60px;   width: 100%; padding-top: 100px">
            <tr >
                <td style="width: 25%;"></td>
                <td style="border-bottom: 1px solid black; width: 50%;"></td>
                <td ></td>
            </tr>
            <tr><td ></td>
                <td style="text-align: center"><strong>Dirección General – UEP/BID</strong></td>
                <td ></td>
            </tr>
        </table>

        {{--<style type="text/css">
          .tg2  {border-collapse:collapse;border-spacing:0;width:80%;margin: 0px 80px}
          .tg2 td{border-color:black;border-style:solid;border-width:1px;font-family:Arial, sans-serif;font-size:12px;
            overflow:hidden;padding:10px 5px;word-break:normal;}
          .tg2 th{border-color:black;border-style:solid;border-width:1px;font-family:Arial, sans-serif;font-size:12px;
            font-weight:normal;overflow:hidden;padding:10px 5px;word-break:normal;}
          .tg2 .tg2-nx8p{font-size:12px;text-align:left;vertical-align:top}
          .tg2 .tg2-13pz{font-size:12px;text-align:center;vertical-align:top}
          </style>
          <table class="tg2" style="padding-top: 150px">
          <thead>
            <tr>
              <th class="tg2-nx8p"><br><br><br></th>
              <th class="tg2-nx8p"><br><br><br></th>


            </tr>
          </thead>
          <tbody>
            <tr>
              <td class="tg2-13pz">Lic. María Gloria Páez<br>Dirección General de la <br>UEP - BID</td>
              <td class="tg2-13pz">Lic. Arnaldo Andrés Lezcano<br>Dirección General de <br>Administración y Finanzas</td>

            </tr>
          </tbody>
          </table>--}}
        </main>
</body>
</html>
