@php
    
$naRed='<span id="nda"> N/A </span>';
@endphp
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
  <header>
    <img src="{{ public_path('logo-cab-transf-recursos.jpg') }}" >
  </header>
  <footer>
    <img src="{{ public_path('logo-pie-transf-recursos.png') }}" width=" 750px" >
</footer>
<main>
    <style type="text/css">

        .tg  {border-collapse:collapse;border-spacing:0;width:80%;margin: -10px 80px}
        .tg td{border-color:black;border-style:solid;border-width:1px;font-family:Arial, sans-serif;font-size:12px;
          overflow:hidden;padding:10px 5px;word-break:normal;}
        .tg th{border-color:black;border-style:solid;border-width:1px;font-family:Arial, sans-serif;font-size:12px;
          font-weight:normal;overflow:hidden;padding:10px 5px;word-break:normal;}
        .tg .tg-0pky{border-color:inherit;text-align:left;vertical-align:top}
        header {
                position: fixed;
                top: -30px;
                left: 90px;
                right: 0px;
                height: 50px;
                font-size: 20px !important;
            }

            footer {
                position: fixed;
                bottom: 40px;
                left: 0px;
                right: 0px;
                height: 50px;
                font-size: 20px !important;
            }
            .noBorder {
                border:none !important;
            }
            #nda{color: red;}
        </style>

<br>
<br>

        <h2 style="text-align: center; padding-top: 20px">CALIFICACION DE PROYECTO</h2>
        <h4 style="text-align: center; ">LEY Nº 5.665/16</h4>
        <h4 style="text-align: center; ">PROGRAMA DE MEJORAMIENTO DE VIVIENDA Y DEL HABITAT</h4>
        <h4 style="text-align: center;">COMPONENTE I</h4>
        <h4 style="text-align: center;">Proyecto de Mejoramiento y Ampliación de Vivienda en el Área metropolitana de Asunción</h4>
        <p style="padding-left: 40px; padding-right: 40px; text-align: justify;
        text-justify: inter-word;">
        La Unidad Ejecutora del Programa-BID (UEP- BID), certifica que el “Proyecto de Mejoramiento y/o Ampliación de Vivienda en el Área Metropolitana de Asunción”;
        presentado y a ser ejecutado por el <strong>ATC {!! $applicant->atc ? $applicant->atc->name : $naRed !!}</strong>  con
        <strong>RUC {!! $applicant->atc ? $applicant->atc->const_ruc :  $naRed !!}</strong> , representado por
        <strong>{!! $applicant->atc ? $applicant->atc->repre : $naRed !!}</strong> 
        con <strong>C.I. N° {!! $applicant->atc ? number_format(str_replace('.','',$applicant->atc->nro_ci),0,'.','.' ) : $naRed !!}</strong>
        según expediente <strong>N° {!! $applicant->file_number ? $applicant->file_number: $naRed !!}</strong>  de fecha <strong>{!! $applicant->file_date ? date('d/m/Y', strtotime($applicant->file_date)): $naRed !!}</strong> , perteneciente al beneficiario/a <strong> {{ $applicant['names'] }} {{ $applicant['last_names'] }} </strong>
        con <strong>C.I. N°  {{ number_format($applicant['government_id'] ? $applicant['government_id'] : '123',0,'.','.' )  }}</strong>,
        postulante al Aporte Estatal para el Mejoramiento y /o Ampliación de su vivienda, cuya propiedad está ubicada en la Ciudad de <strong>{{ $applicant->city->name }}</strong>,
        cumple con los requisitos establecidos en el Reglamento Operativo del Programa aprobado por Resolución N° 2157 de fecha 29/10/2023.
        </p>

        <table style="padding-left: 40px; padding-right: 40px;  width: 100%">
            <tr>
                <td><strong>Valor del Mejoramiento y/o ampliación</strong></td>
                <td style="text-align: right"><strong>Gs.35.000.000.- </strong></td>
            </tr>
            <tr>
                <td><strong>Valor del Aporte Estatal</strong></td>
                <td style="text-align: right"><strong>Gs.34.000.000.-</strong></td>
            </tr>
        </table>
        @php
            $date = \Carbon\Carbon::parse($fecha);
        @endphp

        <p style="padding-left: 40px; padding-right: 40px; padding-top: 20px; text-align: right;">Asuncion, {{ date('d',strtotime($fecha)) }} de {{ $date->formatLocalized('%B') }} de {{ date('Y',strtotime($fecha)) }}</p>

        <table style="padding-left: 40px; padding-right: 40px;   width: 100%; padding-top: 150px">
            <tr >
                <td style="border-bottom: 1px solid black;"></td>
                <td></td>
                <td style="border-bottom: 1px solid black"></td>
            </tr>
            <tr>
                <td style="text-align: center"><strong>Coordinación Técnica – AMA-BID</strong></td>
                <td style="width: 10%"></td>
                <td style="text-align: center "><strong>Dirección – AMA-BID</strong></td>
            </tr>
        </table>

        {{--<style type="text/css">
          .tg2  {border-collapse:collapse;border-spacing:0;width:80%;margin: 0px 80px}
          .tg2 td{border-color:black;border-style:solid;border-width:1px;font-family:Arial, sans-serif;font-size:12px;
            overflow:hidden;padding:10px 5px;word-break:normal;}
          .tg2 th{border-color:black;border-style:solid;border-width:1px;font-family:Arial, sans-serif;font-size:12px;
            font-weight:normal;overflow:hidden;padding:10px 5px;word-break:normal;}
          .tg2 .tg2-nx8p{font-size:12px;text-align:left;vertical-align:top}
          .tg2 .tg2-13pz{font-size:12px;text-align:center;vertical-align:top}
          </style>
          <table class="tg2" style="padding-top: 150px">
          <thead>
            <tr>
              <th class="tg2-nx8p"><br><br><br></th>
              <th class="tg2-nx8p"><br><br><br></th>


            </tr>
          </thead>
          <tbody>
            <tr>
              <td class="tg2-13pz">Lic. María Gloria Páez<br>Dirección General de la <br>UEP - BID</td>
              <td class="tg2-13pz">Lic. Arnaldo Andrés Lezcano<br>Dirección General de <br>Administración y Finanzas</td>

            </tr>
          </tbody>
          </table>--}}
        </main>
</body>
</html>
