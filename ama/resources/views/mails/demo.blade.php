<p>Hola <em>{{ $demo->receiver }}</em>,</p>
<p>Usted ha realizado una PRE-INSCRIPCION (Etapa de análisis documental) del Proyecto AMA.</p>
<p><strong><u>Datos del Solicitante:</u></strong></p>
<div>
<p><strong>Postulante:</strong>&nbsp;{{ $demo->demo_one }}</p>
<p><strong>Cónyuge:</strong>&nbsp;{{ $demo->demo_two }}</p>
</div>
<p><strong><u>Para continuar con el proceso de Evaluación Requerimos la presentación física de los siguientes documentos.</u></strong></p>

<p>1. FOTOCOPIA SIMPLE DE CI DEL SOLICITANTE, CONYUGE O PAREJA DE HECHO Y/O FAMILIARES QUE VIVEN EN LA CASA.​</p>
<p>2. *SI ESTA CASADO/A FOTOCOPIA AUTENTICADA DE CERTIFICADO DE MATRIMONIO O LIBRETA DE FAMILIA (si en la cédula no figura CA),* SI ESTA VIVIENDO EN UNION DE HECHO, PRESENTAR CERTIFICADO DE VIDA Y RESIDENCIA (y declaración Jurada de Unión de Hechos a ser facilitado por AMA). * SI ESTA DIVORCIADO, constancia de disolución conyugal o de haber iniciado trámite de divorcio.</p>
<p>3. FOTOCOPIA AUTENTICADA DEL INMUEBLE; *SI ES TITULO con Sello de Registro Público) * SI ES CONTRATO PRIVADO (con copia de Boleta de Ande) SI ES BOLETA DE PAGO DE CUOTAS APARTIR DEL 70% O MAS (con constancia de cancelación de pago emitido por la inmobiliaria y copia de boleta de Ande).</p>
<p>4. CONSTANCIA DE INGRESO: *SI TIENE TRABAJO FORMAL (certificado de trabajo o liquidación de salario). *SI TIENE IVA ACTIVO presentar 3 últimas declaraciones juradas del IVA).* SI TIENE INGRESO INFORMAL Y/O TRABAJO POR CUENTA PROPIA presentar declaración jurada de ingreso.</p>
<p><u> <strong>Para más información póngase en contacto: (021) 4133135 - (021) 4133137 de 08:00 a 16:00 horas </strong> </u></p>
<p>
    <br />Muchas Gracias, 
    <br />
    <em>{{ $demo->sender }}</em>
</p>