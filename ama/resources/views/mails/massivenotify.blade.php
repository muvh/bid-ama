<p>Hola <i>{{ $demo->receiver }}</i>,</p>
<p>Usted había realizado una Pre-Inscripción en la plataforma AMA del Proyecto AMA-BID del Ministerio de Vivienda y Hábitat (MUVH), para completar oficialmente la inscripción es necesario la presentación de documentos respaldatorios para proseguir con el proceso.</p>
<p><span style="text-decoration: underline;"><b>Considerar los siguientes documentos a presentar:</b></span></p>
<ol>
<li>FOTOCOPIA SIMPLE DE CI DEL SOLICITANTE, CONYUGE O PAREJA DE HECHO Y/O FAMILIARES QUE VIVEN EN LA CASA.</li>
<li>*SI ESTA CASADO/A FOTOCOPIA AUTENTICADA DE CERTIFICADO DE MATRIMONIO O LIBRETA DE FAMILIA (si en la cédula no figura CA), <br />* SI ESTÁ VIVIENDO EN UNION DE HECHO, PRESENTAR CERTIFICADO DE VIDA Y RESIDENCIA (y declaración Jurada de Unión de Hechos a ser facilitado por el MUVH). <br />* SI ESTA DIVORCIADO/A, constancia de disolución conyugal o de haber iniciado trámite de divorcio.<br />* SI ESTA SEPARADO/A, Constancia del Juzgado.</li>
<li>FOTOCOPIA AUTENTICADA DEL INMUEBLE; <br />* SI ES TITULO con Sello de Registro Público) <br />* SI ES CONTRATO PRIVADO acompañado del título matriz. En caso de estar pagando a cuotas por el inmueble presentar BOLETA DE PAGO DE CUOTAS APARTIR DEL 50% O MAS o constancia de cancelación de pago emitido por la inmobiliaria y copia de boleta de ANDE).</li>
<li>CONSTANCIA DE INGRESO: <br />* SI TIENE TRABAJO FORMAL (certificado de trabajo o liquidación de salario). <br />* SI TIENE IVA ACTIVO presentar 3 últimas declaraciones juradas del IVA). <br />* SI TIENE INGRESO INFORMAL Y/O TRABAJO POR CUENTA PROPIA presentar declaración jurada de ingreso a ser facilitado por el MUVH</li>
<li><b>Vivir en una de estas ciudades: 
    <br /> Asunción, <br />
    Lambaré,<br /> 
    Villa Elisa, <br />
    Fdo. De la Mora,<br />
    Capiatá, <br />
    Luque, <br />
    San Lorenzo, <br />
    Ñemby, <br />
    San Antonio, <br />
    Mariano R. Alonzo y <br />
    Limpio.</b>
    </li>
 </ol>
<p><strong>El proyecto está vigente hasta octubre 2022 es importante presentar con antelación los documentos <br /></strong></p>
<p><strong>Para realizar la inscripción oficial, deberá acercarse con los documentos que corresponda a la oficina del proyecto (Humaitá c/Nuestra Señora de la Asunción edificio planeta 1, piso 13.<br /></strong></p>
<p><strong>Para más información póngase en contacto: (021) 4133135 &ndash; (021) 4133137 de 08:00 a 16:00 horas</strong></p>

<p>Muchas Gracias, 
    <br />
    <i>{{ $demo->sender }}</i></p>
