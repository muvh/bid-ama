Hola  <i>{{ $demo->receiver }}</i>,
<p>Hemos actualizado su estado de postulación, con los siguientes detalles: </p>
<p><u>Datos de la Postulación:</u></p>
<div>
<p><b>Postulante:</b>&nbsp;{{ $demo->name }}</p>
<p><b>Estado actual:</b>&nbsp;{{ $demo->status }}</p>
<p><b>Observaciones:</b>&nbsp;{{ $demo->obs }}</p>
</div>
<br/>
Muchas Gracias,
<br/>
<i>{{ $demo->sender }}</i>
