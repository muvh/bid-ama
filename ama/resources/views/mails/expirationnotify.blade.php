<!doctype html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0">
    <title>Llamado de emergencia</title>
    <style type="text/css">
      .tg  {border-collapse:collapse;border-color:#93a1a1;border-spacing:0;}
.tg td{background-color:#fdf6e3;border-color:#93a1a1;border-style:solid;border-width:0px;color:#002b36;
  font-family:Arial, sans-serif;font-size:14px;overflow:hidden;padding:7px 7px;word-break:normal;}
.tg th{background-color:#657b83!important;border-color:#93a1a1;border-style:solid;border-width:0px;color:#fdf6e3;
  font-family:Arial, sans-serif;font-size:14px;font-weight:normal;overflow:hidden;padding:7px 7px;word-break:normal;}
.tg .tg-0lax{text-align:left;vertical-align:top}
    </style>
</head>
<body>

Buenas,
<p>Este es el reporte de vencimiento de polizas: </p>
<p><u>Polizas proximas a vencer:</u></p>
<div>


<style type="text/css">
.tg  {border-collapse:collapse;border-color:#93a1a1;border-spacing:0;width:100%;}
.tg td{background-color:#fdf6e3;border-color:#93a1a1;border-style:solid;border-width:0px;color:#002b36;
  font-family:Arial, sans-serif;font-size:14px;overflow:hidden;padding:7px 7px;word-break:normal;}
.tg th{background-color:#657b83;border-color:#93a1a1;border-style:solid;border-width:0px;color:#fdf6e3;
  font-family:Arial, sans-serif;font-size:14px;font-weight:normal;overflow:hidden;padding:7px 7px;word-break:normal;}
.tg .tg-0lax{text-align:left;vertical-align:top}
</style>
<table class="tg">
<thead>

  <tr>
	        <th class="tg-0lax">Poliza</th>
            <th class="tg-0lax">CI.</th>
            <th class="tg-0lax">Titular</th>
            <th class="tg-0lax">ATC</th>
            <th class="tg-0lax">Cooperativa</th>
            <th class="tg-0lax">Concepto</th>
            <th class="tg-0lax">Vencimiento</th>
            <th class="tg-0lax">Dias</th>

  </tr>
</thead>
<tbody>
@foreach ($data->polizas as $item)
  <tr>
    <td class="tg-0lax">{{$item['poliza']}}</td>
    <td class="tg-0lax">{{$item['ci']}}</td>
    <td class="tg-0lax">{{$item['titular']}}</td>
    <td class="tg-0lax">{{ $item['atc'] }}</td>
    <td class="tg-0lax">{{ $item['coop'] }}</td>
    <td class="tg-0lax">{{$item['concepto']}}</td>
    <td class="tg-0lax">{{  date('d/m/Y',strtotime($item['vencimiento']))}}</td>
    <td class="tg-0lax">{{$item['dias']}}</td>
  </tr>
@endforeach
</tbody>
</table>

</div>
<br/>
Muchas Gracias,
<br/>
<i>Sistema AMA</i>
</body>
</html>
