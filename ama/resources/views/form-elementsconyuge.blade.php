<div class="row">
    <div class="form-group col-sm-4" :class="{'has-danger': errors.has('government_id')}">
        <label for="government_id" >{{ trans('admin.applicant.columns.government_id') }}</label>
        <div class="input-group mb-3">
        <input @change="findData" type="text" v-model="form.government_id" v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('government_id'), 'form-control-success': fields.government_id && fields.government_id.valid}" id="government_id" name="government_id" placeholder="{{ trans('admin.applicant.columns.government_id') }}">
        <div class="input-group-append">
            <button @click="findData" class="btn btn-primary" type="button"><i class="fa fa-random" aria-hidden="true"></i></button>
        </div>
        </div>
        <div v-if="errors.has('government_id')" class="form-control-feedback form-text " v-cloak>@{{ errors.first('government_id') }}</div>
    </div>

    <div class="form-group col-sm-4" :class="{'has-danger': errors.has('names')}">
        <label for="names">{{ trans('admin.applicant.columns.names') }}</label>
        <input readonly type="text" onkeyup="javascript:this.value=this.value.toUpperCase();" v-model="form.names" v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('names'), 'form-control-success': fields.names && fields.names.valid}" id="names" name="names" placeholder="{{ trans('admin.applicant.columns.names') }}">
        <div v-if="errors.has('names')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('names') }}</div>
    </div>
    <div class="form-group col-sm-4" :class="{'has-danger': errors.has('last_names')}">
        <label for="last_names" >{{ trans('admin.applicant.columns.last_names') }}</label>
        <input readonly type="text" onkeyup="javascript:this.value=this.value.toUpperCase();" v-model="form.last_names" v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('last_names'), 'form-control-success': fields.last_names && fields.last_names.valid}" id="last_names" name="last_names" placeholder="{{ trans('admin.applicant.columns.last_names') }}">
        <div v-if="errors.has('last_names')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('last_names') }}</div>
    </div>
</div>

<!--<div class="row">
    <div class="form-group col-sm-4" :class="{'has-danger': errors.has('address')}">
        <label for="address" >{{ trans('admin.applicant.columns.address') }}</label>
        <input type="text" onkeyup="javascript:this.value=this.value.toUpperCase();" v-model="form.address" v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('address'), 'form-control-success': fields.address && fields.address.valid}" id="address" name="address" placeholder="{{ trans('admin.applicant.columns.address') }}">
        <div v-if="errors.has('address')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('address') }}</div>
    </div>
    <div class="form-group col-sm-4" :class="{'has-danger': errors.has('neighborhood')}">
        <label for="neighborhood" >{{ trans('admin.applicant.columns.neighborhood') }}</label>
        <input type="text" onkeyup="javascript:this.value=this.value.toUpperCase();" v-model="form.neighborhood" v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('neighborhood'), 'form-control-success': fields.neighborhood && fields.neighborhood.valid}" id="neighborhood" name="neighborhood" placeholder="{{ trans('admin.applicant.columns.neighborhood') }}">
        <div v-if="errors.has('neighborhood')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('neighborhood') }}</div>
    </div>
    <div class="form-group col-sm-4" :class="{'has-danger': errors.has('city')}">
        <label for="city">{{ trans('admin.applicant.columns.city_id') }}</label>

        <multiselect
            v-model="form.city"
            :options="cities"
            :multiple="false"
            track-by="id"
            label="name"
            :taggable="true"
            tag-placeholder=""
            placeholder="{{ trans('admin.applicant.actions.search')  }}">
        </multiselect>

        <div v-if="errors.has('city')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('city') }}</div>
    </div>
</div>-->

<div class="row">
    <div class="form-group col-sm-4" :class="{'has-danger': errors.has('nationality')}">
        <label for="nationality" >{{ trans('admin.applicant.columns.nationality') }}</label>
        <input readonly type="text" v-model="form.nationality" v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('nationality'), 'form-control-success': fields.nationality && fields.nationality.valid}" id="nationality" name="nationality" placeholder="{{ trans('admin.applicant.columns.nationality') }}">
        <div v-if="errors.has('nationality')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('nationality') }}</div>
    </div>
    <!--<div class="form-group col-sm-3" :class="{'has-danger': errors.has('marital_status')}">
        <label for="marital_status" >{{ trans('admin.applicant.columns.marital_status') }}</label>
        <input readonly type="text"  v-model="form.marital_status" v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('marital_status'), 'form-control-success': fields.marital_status && fields.marital_status.valid}" id="marital_status" name="marital_status" placeholder="{{ trans('admin.applicant.columns.marital_status') }}">
        <div v-if="errors.has('marital_status')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('marital_status') }}</div>
    </div>-->
    <!--<div class="form-group col-sm-4" :class="{'has-danger': errors.has('gender')}">
        <label for="gender">{{ trans('admin.applicant.columns.gender') }}</label>
        <input readonly type="text" v-model="form.gender" v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('gender'), 'form-control-success': fields.gender && fields.gender.valid}" id="gender" name="gender" placeholder="{{ trans('admin.applicant.columns.gender') }}">
        <div v-if="errors.has('gender')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('gender') }}</div>
    </div> -->
    <div class="form-group col-sm-4" :class="{'has-danger': errors.has('birthdate')}">
        <label for="birthdate">{{ trans('admin.applicant.columns.birthdate') }}</label>
        <div class="input-group input-group--custom">
            <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
            <datetime

            v-model="form.birthdate"
            :config="datePickerConfig"
            v-validate="'date_format:yyyy-MM-dd'"
            class="flatpickr"

            :class="{'form-control-danger': errors.has('birthdate'), 'form-control-success': fields.birthdate && fields.birthdate.valid}"
            id="birthdate" name="birthdate"
            placeholder="{{ trans('brackets/admin-ui::admin.forms.select_date_and_time') }}">
        </datetime>
        </div>
        <div v-if="errors.has('birthdate')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('birthdate') }}</div>
    </div>
        <div class="form-group col-sm-4" :class="{'has-danger': errors.has('monthly_income')}">
        <label for="monthly_income">{{ trans('admin.applicant.columns.monthly_income') }}</label>
        <input type="text" v-model="form.monthly_income" v-validate="'decimal'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('monthly_income'), 'form-control-success': fields.monthly_income && fields.monthly_income.valid}" id="monthly_income" name="monthly_income" placeholder="{{ trans('admin.applicant.columns.monthly_income') }}">
        <div v-if="errors.has('monthly_income')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('monthly_income') }}</div>
    </div>
</div>

<div class="row">
    <div class="form-group col-sm-4" :class="{'has-danger': errors.has('educationlevel')}">
        <label for="educationlevel">{{ trans('admin.applicant.columns.education_level') }}</label>

        <multiselect
            v-model="form.educationlevel"
            :options="educationlevels"
            :multiple="false"
            track-by="id"
            label="name"
            :taggable="true"
            tag-placeholder=""
            placeholder="{{ trans('admin.applicant.actions.search')  }}">
        </multiselect>

        <div v-if="errors.has('educationlevel')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('educationlevel') }}</div>
    </div>
    <div class="form-group col-sm-4" :class="{'has-danger': errors.has('occupation')}">
        <label for="occupation">{{ trans('admin.applicant.columns.occupation') }}</label>
        <input type="text" v-model="form.occupation" v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('occupation'), 'form-control-success': fields.occupation && fields.occupation.valid}" id="occupation" name="occupation" placeholder="{{ trans('admin.applicant.columns.occupation') }}">
        <div v-if="errors.has('occupation')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('occupation') }}</div>
    </div>

</div>
