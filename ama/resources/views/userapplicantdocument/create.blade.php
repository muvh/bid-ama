@extends('brackets/admin-ui::admin.layout.usersys')

@section('title', trans('admin.applicant-document.actions.create'))

@section('body')

    <div class="container-xl">
        <div class="card">


            <form action="{{ url('documents/create/'.$applicantID.'/'.$type) }}" class="form-horizontal form-create"  method="post" enctype="multipart/form-data">
            <div class="card-header">
                <i class="fa fa-plus"></i> {{ trans('admin.applicant-document.actions.create') }}
            </div>
            @csrf
            <div class="card-body">
                <div class="alert alert-success">
                <ul>
                    <li>Solo es necesario subir fotos de las cédulas y en el caso del documento de la tenencia subir foto de la portada del mismo.</li>
                </ul>
            </div>
                @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
                <div class="form-group col-sm-12" :class="{'has-danger': errors.has('address')}">
                    <label for="document" >{{ trans('admin.applicant-document.columns.document_id') }}</label>
                    <div>
                        <select name="document" id="document" class="form-control" placeholder="{{ trans('admin.documents.actions.search')  }}">
                                <option value="">Seleccione Documento</option>
                            @foreach ($documents as $item)
                                <option value="{{$item->id}}">{{$item->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group col-sm-12" :class="{'has-danger': errors.has('address')}">
                    <label for="document" >{{ trans('admin.applicant-document.columns.file') }}</label>
                    <div>
                        <input type="file" name="file" class="form-control-file" id="chooseFile">
                    </div>
                </div>

            </div>
            <div class="card-footer">
                <button type="submit" class="btn btn-primary" >
                    <i class="fa fa-download"></i>
                    {{ trans('brackets/admin-ui::admin.btn.save') }}
                </button>
            </div>
            </form>
        </div>
        </div>
@endsection
