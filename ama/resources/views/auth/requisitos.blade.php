@extends('brackets/admin-ui::admin.layout.master')

@section('title', trans('brackets/admin-auth::admin.login.title'))

@section('content')
	<div class="container" id="app">
	    <div class="row align-items-center justify-content-center auth">
            <center>
                <img src="/images/ama-2022.jpg" class="img-fluid rounded mx-auto d-block" alt="">
               <br>
                <button  class="btn btn-lg btn-block btn-square" style="background-color: #D7040C; color: aliceblue;font-size: 150%" type="button">
                    <a style="color: aliceblue;" href="media/ANEXO-A-1-FICHA-DE-INSCRIPCION.docx">
                        <i class="nav-icon fa fa-download"></i>
                    <b>  Descargue la Solicitud de Inscripción al Proyecto AMA </b>
                    </a>
                </button>
               </center>
	        </div>
	    </div>

	</div>

@endsection


@section('bottom-scripts')
<script type="text/javascript">
    // fix chrome password autofill
    // https://github.com/vuejs/vue/issues/1331
    //document.getElementById('exampleModalLg').modal('show'); // abrir
    //document.getElementById('password').dispatchEvent(new Event('input'));
 

</script>
@endsection
