@if ( $applicant->statuses->status->id == 1 )
    @if ( count($techDocuments) > 1 )
        <a href="/transition" class="btn btn-block bg-warning" type="button"> <strong><i class="fa fa-paper-plane"></i> Enviar Postulación</strong></a>
    @else
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
        <strong>Importante!</strong> Tienes documentos pendientes de carga.
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    @endif
@else
<div class="alert alert-info alert-dismissible fade show" role="alert">
    La Postulación se encuentra en el estado: <strong> {{ $applicant->statuses->status->name }} </strong>
</div>
@endif

<br>

<div class="card">
    <h5 class="card-header">TITULAR: {{$applicant->names}} {{$applicant->last_names}} - DOCUMENTO: {{number_format($applicant->government_id,0,'','.')}}</h5>
    <div class="card-body">
      <div class="row">
            <div class="form-group col-sm-4">
                <p class="card-text">Ciudad: {{$applicant->city->name}}</p>
            </div>

            <div class="form-group col-sm-4">
                <p class="card-text">Barrio: {{$applicant->neighborhood}}</p>
            </div>
            <div class="form-group col-sm-4">
                <p class="card-text">Direccion: {{$applicant->address}}</p>
            </div>
    </div>
    <div class="row">
            <div class="form-group col-sm-4">
                <p class="card-text">Nacionalidad: {{$applicant->nationality}}</p>
            </div>

            <div class="form-group col-sm-4">
                <p class="card-text">Nacimiento: {{$applicant->birthdate}}</p>
            </div>
            <div class="form-group col-sm-4">
                <p class="card-text">Ingreso Mensual: {{ number_format($applicant->monthly_income,0,'','.') }}</p>
            </div>
    </div>
    @if ( $applicant->statuses->status->id == 1 )
    <a href="{{$applicant->id}}/edit" class="btn btn-primary">Editar</a>
    @endif
    </div>
  </div>

@if ($conyuge)
<div class="card">
    <h5 class="card-header">CONYUGE: {{$conyuge->names}} {{$conyuge->last_names}} - DOCUMENTO: {{number_format($conyuge->government_id,0,'','.')}}</h5>
    <div class="card-body">
    <div class="row">
            <div class="form-group col-sm-4">
                <p class="card-text">Nacionalidad: {{$conyuge->nationality}}</p>
            </div>

            <div class="form-group col-sm-4">
                <p class="card-text">Nacimiento: {{$conyuge->birthdate}}</p>
            </div>
            <div class="form-group col-sm-4">
                <p class="card-text">Ingreso Mensual: {{number_format($conyuge->monthly_income,0,'','.') }}</p>
            </div>
    </div>
    @if ( $applicant->statuses->status->id == 1 )
    <a href="{{$conyuge->id}}/editconyuge" class="btn btn-primary">Editar</a>
    @endif
    </div>
  </div>
@else
    @if ( $applicant->statuses->status->id == 1 )
        <a href="/createconyuge"><button  class="btn btn-block bg-primary" type="button"><i class="fa fa-plus"></i> Ingrese Conyuge</button></a>
    <br>
    @endif
@endif




  <applicant-document-listing
  :data="{{ $techDocuments->toJson() }}"
  :url="'{{ url('getDocuments/') }}'"
  inline-template>

  <div class="row">
      <div class="col">
          <div class="card">
              <div class="card-header">
                  <i class="fa fa-align-justify"></i> {{ trans('admin.applicantuser.documents') }}
                  @if ( $applicant->statuses->status->id == 1 )
                    <a class="btn btn-primary btn-spinner btn-sm pull-right m-b-0"
                    href="{{ url('documents/create/'.$applicant->id.'/S') }}"
                    role="button">
                    <i class="fa fa-plus"></i>
                        &nbsp; {{ trans('admin.applicant-document.actions.create') }}</a>
                  @endif

              </div>
              <div class="card-body" >
                  <div class="card-block">
                      <form @submit.prevent="">
                          <div class="row justify-content-md-between">
                              <div class="col col-lg-7 col-xl-5 form-group">
                                  <div class="input-group">
                                      <input class="form-control" placeholder="{{ trans('brackets/admin-ui::admin.placeholder.search') }}" v-model="search" @keyup.enter="filter('search', $event.target.value)" />
                                      <span class="input-group-append">
                              <button type="button" class="btn btn-primary" @click="filter('search', search)"><i class="fa fa-search"></i>&nbsp; {{ trans('brackets/admin-ui::admin.btn.search') }}</button>
                          </span>
                                  </div>
                              </div>
                              <div class="col-sm-auto form-group ">
                                  <select class="form-control" v-model="pagination.state.per_page">

                                      <option value="10">10</option>
                                      <option value="25">25</option>
                                      <option value="100">100</option>
                                  </select>
                              </div>
                          </div>
                      </form>

                      <table class="table table-hover table-listing">
                          <thead>
                          <tr>

                              <th is='sortable' :column="'applicant_documents.id'">{{ trans('admin.applicant-document.columns.id') }}</th>
                              <th is='sortable' :column="'document_types.name'">{{ trans('admin.document-type.columns.name') }}</th>
                              <th is='sortable' :column="'received_at'">{{ trans('admin.applicant-documentuser.columns.received_at') }}</th>

                              <th></th>
                          </tr>
                          <tr v-show="(clickedBulkItemsCount > 0) || isClickedAll">
                              <td class="bg-bulk-info d-table-cell text-center" colspan="5">
                          <span class="align-middle font-weight-light text-dark">{{ trans('brackets/admin-ui::admin.listing.selected_items') }} @{{ clickedBulkItemsCount }}.  <a href="#" class="text-primary" @click="onBulkItemsClickedAll('/admin/applicant-documents')" v-if="(clickedBulkItemsCount < pagination.state.total)"> <i class="fa" :class="bulkCheckingAllLoader ? 'fa-spinner' : ''"></i> {{ trans('brackets/admin-ui::admin.listing.check_all_items') }} @{{ pagination.state.total }}</a> <span class="text-primary">|</span> <a
                                  href="#" class="text-primary" @click="onBulkItemsClickedAllUncheck()">{{ trans('brackets/admin-ui::admin.listing.uncheck_all_items') }}</a>  </span>

                                  <span class="pull-right pr-2">
                              <button class="btn btn-sm btn-danger pr-3 pl-3" @click="bulkDelete('/admin/applicant-documents/bulk-destroy')">{{ trans('brackets/admin-ui::admin.btn.delete') }}</button>
                          </span>

                              </td>
                          </tr>
                          </thead>
                          <tbody>
                          <tr v-for="(item, index) in collection" :key="item.id" :class="bulkItems[item.id] ? 'bg-bulk' : ''">

                              <td>@{{ item.id }}</td>
                              <td>@{{ item.name }}</td>
                              <td>@{{ new Date(item.received_at) | dateFormat('DD-MM-YYYY')  }}</td>

                              <td>

                                  <div class="row no-gutters">
                                      <template v-if="item.document_url.length>0">
                                          <div class="col-auto">
                                              <a class="btn btn-sm btn-info" target="_blank" :href="item.document_url" title="{{ trans('brackets/admin-ui::admin.btn.document-view') }}" role="button"><i class="fa fa-eye"></i></a>
                                          </div>
                                      </template>
                                      @if ( $applicant->statuses->status->id == 1 )
                                      <form class="col" @submit.prevent="deleteDocument(item.id)">
                                          <button type="submit" class="btn btn-sm btn-danger" title="{{ trans('brackets/admin-ui::admin.btn.delete') }}"><i class="fa fa-trash-o"></i></button>
                                      </form>
                                      @endif
                                  </div>

                              </td>
                          </tr>
                          </tbody>
                      </table>

                      <div class="row" v-if="pagination.state.total > 0">
                          <div class="col-sm">
                              <span class="pagination-caption">{{ trans('brackets/admin-ui::admin.pagination.overview') }}</span>
                          </div>
                          <div class="col-sm-auto">
                              <pagination></pagination>
                          </div>
                      </div>
                      @if ( $applicant->statuses->status->id == 1 )
                      <div class="no-items-found" v-if="!collection.length > 0">
                          <i class="icon-magnifier"></i>
                          <h3>{{ trans('brackets/admin-ui::admin.index.no_items') }}</h3>
                          <p>{{ trans('brackets/admin-ui::admin.index.try_changing_items') }}</p>
                          <a class="btn btn-primary btn-spinner" href="{{ url('documents/create/'.$applicant->id.'/S') }}" role="button"><i class="fa fa-plus"></i>&nbsp; {{ trans('admin.applicant-document.actions.create') }}</a>
                      </div>
                      @endif
                  </div>
              </div>
              <!--<div class="card-footer">
                  <a class="btn btn-primary btn-sm pull-left m-b-0" href="#doc-financieros" role="button"><i class="fa fa-arrow-left"></i>&nbsp; Anterior</a>
                  <a class="btn btn-primary btn-sm pull-right m-b-0" href="#calificacion-final" role="button"><i class="fa fa-arrow-right"></i>&nbsp; Siguiente</a>
              </div>-->
          </div>
      </div>
  </div>
</applicant-document-listing>






