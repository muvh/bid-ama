@extends('brackets/admin-ui::admin.layout.default-tracking')

@section('title', trans('admin.applicant.actions.trakings'))

@section('body')

    <tracking-listing
        :data="{{ $data->toJson() }}"
        :url="'{{ url('application-tracking') }}'"
        inline-template>

        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="card-header">
                        <i class="fa fa-align-justify"></i>{{ trans('admin.applicant.actions.trakings') }}
                    </div>

                    <div class="card-header">
                        <form @submit.prevent="">
                            <div class="row justify-content-md-between">
                                <div class="col col-lg-3 form-group">
                                    <div class="input-group">
                                        <input class="form-control" placeholder="{{ trans('brackets/admin-ui::admin.placeholder.search-by-goverment_id') }}" v-model="search" @keyup.enter="filter('search', $event.target.value)" />
                                        <span class="input-group-append">
                                                <button type="button" class="btn btn-primary" @click="filter('search', search)"><i class="fa fa-search"></i>&nbsp; {{ trans('brackets/admin-ui::admin.btn.search') }}</button>
                                            </span>
                                    </div>
                                </div>
                            </div>
                        </form> 
<button type="button" class="btn btn-primary" data-toggle="modal" data-target=".bd-example-modal-lg">Ver Requisitos</button>
<button type="button" class="btn btn-primary" data-toggle="modal" data-target=".bd2-example-modal-lg">Ver Estados</button>
                        <template v-if="extra">
                        <tracking-form
                            :data = "extra"
                            :action="'{{ url('admin/applicants') }}'"
                            v-cloak
                            inline-template>

                            <form class="form-horizontal form-create" method="post" @submit.prevent="onSubmit" :action="action" novalidate>
                                <div class="row">
                                    <div class="col">
                                        <div class="card">
                                            <div class="card-body">
                                                <div class="row">
                                                    <div class="form-group col-sm-2">
                                                        <label for="government_id" >{{ trans('admin.applicant.columns.government_id') }}</label>
                                                        <input disabled type="text" v-model="form.government_id" v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('government_id'), 'form-control-success': fields.government_id && fields.government_id.valid}" id="government_id" name="government_id" placeholder="{{ trans('admin.applicant.columns.government_id') }}">
                                                    </div>
                                                    <div class="form-group col-sm-5">
                                                        <label for="names">{{ trans('admin.applicant.columns.names') }}</label>
                                                        <input disabled  type="text" v-model="form.names" v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('names'), 'form-control-success': fields.names && fields.names.valid}" id="names" name="names" placeholder="{{ trans('admin.applicant.columns.names') }}">
                                                    </div>
                                                    <div class="form-group col-sm-5">
                                                        <label for="last_names" >{{ trans('admin.applicant.columns.last_names') }}</label>
                                                        <input disabled  type="text" v-model="form.last_names" v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('last_names'), 'form-control-success': fields.last_names && fields.last_names.valid}" id="last_names" name="last_names" placeholder="{{ trans('admin.applicant.columns.last_names') }}">
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="form-group col-sm-3">
                                                        <label for="nationality" >{{ trans('admin.applicant.columns.nationality') }}</label>
                                                        <input disabled  type="text" v-model="form.nationality" v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('nationality'), 'form-control-success': fields.nationality && fields.nationality.valid}" id="nationality" name="nationality" placeholder="{{ trans('admin.applicant.columns.nationality') }}">
                                                    </div>
                                                    <div class="form-group col-sm-3">
                                                        <label for="marital_status" >{{ trans('admin.applicant.columns.marital_status') }}</label>
                                                        <input disabled type="text" v-model="form.marital_status" v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('marital_status'), 'form-control-success': fields.marital_status && fields.marital_status.valid}" id="marital_status" name="marital_status" placeholder="{{ trans('admin.applicant.columns.marital_status') }}">
                                                    </div>
                                                    <div class="form-group col-sm-3">
                                                        <label for="gender">{{ trans('admin.applicant.columns.gender') }}</label>
                                                        <input disabled  type="text" v-model="form.gender" v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('gender'), 'form-control-success': fields.gender && fields.gender.valid}" id="gender" name="gender" placeholder="{{ trans('admin.applicant.columns.gender') }}">
                                                    </div>
                                                    <div class="form-group col-sm-3">
                                                        <label for="birthdate">{{ trans('admin.applicant.columns.birthdate') }}</label>
                                                        <input disabled  type="text" v-model="form.birthdate" v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('birthdate'), 'form-control-success': fields.birthdate && fields.birthdate.valid}" id="birthdate" name="birthdate" placeholder="{{ trans('admin.applicant.columns.birthdate') }}">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </tracking-form>
                        </template>
                    </div>


                    <div class="card-body" v-cloak>
                        <div class="card-block">
                            <table class="table table-hover">
                                <thead>
                                <tr>
                                    <th is='sortable' :column="'created_at'">Fecha</th>
                                    <th>Estados</th>
                                    <th>Usuario</th>
                                    <th>Descripción</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr v-for="(item, index) in collection" :key="item.id">
                                    <td>@{{ item.created_at | date('DD-MM-YYYY') }}</td>
                                    <td>@{{ item.status ? item.status.name : '' }}</td>
                                    <td>@{{ item.user ? item.user.full_name : '' }}</td>
                                    <td>@{{ item.description ? item.description : 'No hay observaciones' }}</td>
                                </tr>
                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </tracking-listing>
    <div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
          <div class="modal-content">
            <img src="https://ama.muvh.gov.py/images/ama-2022.jpg" alt="">
          </div>
        </div>
      </div>
    <div class="modal fade bd2-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
          <div class="modal-content">
            <img src="https://ama.muvh.gov.py/images/estados.png " alt="">
          </div>
        </div>
      </div>



@endsection
