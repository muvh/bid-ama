@extends('brackets/admin-ui::admin.layout.usersys')

@section('title', trans('admin.applicant.actions.trakings'))

@section('body')

<div class="card text-white bg-primary text-center">
    <div class="card-body">
    <blockquote class="card-bodyquote">
    <h2>Bienvenido {{ Auth::user()->name }}</h2>
    <p>Resumen de Postulación</p>
    <!--<footer>
        <div class="row align-items-center">
            <div class="col-6 col-sm-4 col-md-2 col-xl mb-3 mb-xl-0">
                <a href="/applicants"><button  class="btn btn-lg btn-block btn-square btn-secondary" type="button">Postulantes</button></a>
            </div>
            <div class="col-6 col-sm-4 col-md-2 col-xl mb-3 mb-xl-0">
            <a href="/reports/applicants-by-resolution-social"><button class="btn btn-lg btn-block btn-square btn-lg btn-success" type="button">Reportes</button></a>
            </div>
            <div class="col-6 col-sm-4 col-md-2 col-xl mb-3 mb-xl-0">
            <a href="/applicants/processed"><button class="btn btn-lg btn-block btn-square btn-warning" type="button">{{ trans('admin.applicant.history') }}</button></a>
            </div>
        </div>
    </footer>-->
    </blockquote>
    </div>
</div>

@if ($applicant)
@include('tabs')
@else
    <a href="/create" class="btn btn-block bg-primary"><i class="fa fa-plus"></i> Ingrese Postulante</a>
@endif



@endsection
