@extends('brackets/admin-ui::admin.layout.usersys')

@section('title', trans('admin.applicant.actions.trakings'))

@section('body')


<div class="alert alert-danger alert-dismissible fade show" role="alert">
    <strong>Importante!</strong> Una vez enviado ya no podrás modificar los datos de postulación.
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button>
  </div>
<div class="card">
    <h5 class="card-header text-center"><strong>RESUMEN POSTULACIÓN</strong></h5>
    <div class="card-body">
        <div class="row">
            <div class="form-group col-sm-4">
                <h4><strong>Titular</strong></h4>
            </div>

        </div>
        <div class="row">
            <div class="form-group col-sm-4">
                <p class="card-text">Nombre: {{$applicant->names}}</p>
            </div>

            <div class="form-group col-sm-4">
                <p class="card-text">Apellido: {{$applicant->last_names}}</p>
            </div>
            <div class="form-group col-sm-4">
                <p class="card-text">Documento: {{number_format($applicant->government_id,0,'','.')}}</p>
            </div>
    </div>
      <div class="row">
            <div class="form-group col-sm-4">
                <p class="card-text">Ciudad: {{$applicant->city->name}}</p>
            </div>

            <div class="form-group col-sm-4">
                <p class="card-text">Barrio: {{$applicant->neighborhood}}</p>
            </div>
            <div class="form-group col-sm-4">
                <p class="card-text">Direccion: {{$applicant->address}}</p>
            </div>
    </div>
    <div class="row">
            <div class="form-group col-sm-4">
                <p class="card-text">Nacionalidad: {{$applicant->nationality}}</p>
            </div>

            <div class="form-group col-sm-4">
                <p class="card-text">Nacimiento: {{$applicant->birthdate}}</p>
            </div>
            <div class="form-group col-sm-4">
                <p class="card-text">Ingreso Mensual: {{ number_format($applicant->monthly_income,0,'','.') }}</p>
            </div>
    </div>
    <div class="row">
            <div class="form-group col-sm-4">
                <p class="card-text"><strong>Metodos de Contacto</strong></p>
            </div>
    </div>
    @foreach ($contact as $item)
    <p>{{$item->contactMethod->name}}: {{$item->description}}</p>
    @endforeach

    @if ($conyuge)
    <div class="row">
        <div class="form-group col-sm-4">
            <h4><strong>Conyuge</strong></h4>
        </div>
    </div>
    <div class="row">
        <div class="form-group col-sm-4">
            <p class="card-text">Nombre: {{$conyuge->names}}</p>
        </div>

        <div class="form-group col-sm-4">
            <p class="card-text">Apellido: {{$conyuge->last_names}}</p>
        </div>
        <div class="form-group col-sm-4">
            <p class="card-text">Documento: {{number_format($conyuge->government_id,0,'','.')}}</p>
        </div>
</div>
    <div class="row">
            <div class="form-group col-sm-4">
                <p class="card-text">Nacionalidad: {{$conyuge->nationality}}</p>
            </div>

            <div class="form-group col-sm-4">
                <p class="card-text">Nacimiento: {{$conyuge->birthdate}}</p>
            </div>
            <div class="form-group col-sm-4">
                <p class="card-text">Ingreso Mensual: {{number_format($conyuge->monthly_income,0,'','.')}}</p>
            </div>
    </div>
    @endif
    <div class="row">
        <div class="form-group col-sm-4">
            <h4><strong>Documentos Adjuntos</strong></h4>
        </div>
    </div>
    <table class="table table-hover table-listing">
        <thead>
        <tr>
            <th >{{ trans('admin.applicant-document.columns.id') }}</th>
            <th >{{ trans('admin.document-type.columns.name') }}</th>
            <th></th>
        </tr>
        </thead>
        <tbody>
            @foreach ($used as $item)
            <tr>
                <td>{{$item->id}}</td>
                <td>{{$item->name}}</td>
                <td></td>
            </tr>
            @endforeach


        </tbody>
    </table>
    <a href="/transitionsave" class="btn btn-block text-white bg-info"><i class="fa fa-paper-plane"></i> Enviar</a>
  </div>

@endsection
