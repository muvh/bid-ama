<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Tooltip Message
    |--------------------------------------------------------------------------
    |
    | Text that appears in the tooltip when the cursor hover the bubble, before
    | the popup opens.
    |
    */

    'tooltip' => 'Sugerencias',

    /*
    |--------------------------------------------------------------------------
    | Popup Title
    |--------------------------------------------------------------------------
    |
    | This is the text that will appear below the logo in the feedback popup
    |
    */

    'title' => 'Ayúdanos a mejorar',

    /*
    |--------------------------------------------------------------------------
    | Success Message
    |--------------------------------------------------------------------------
    |
    | This message will be displayed if the feedback message is correctly sent.
    |
    */

    'success' => 'Gracias por su Ayuda!',

    /*
    |--------------------------------------------------------------------------
    | Placeholder
    |--------------------------------------------------------------------------
    |
    | This text will appear as the placeholder of the textarea in which the
    | the user will type his feedback.
    |
    */

    'placeholder' => 'Ingrese el reclamo con el CI del postulante aqui..',

    /*
    |--------------------------------------------------------------------------
    | Button Label
    |--------------------------------------------------------------------------
    |
    | Text of the confirmation button to send the feedback.
    |
    */

    'button' => 'Enviar',

    /*
    |--------------------------------------------------------------------------
    | Feedback Texts
    |--------------------------------------------------------------------------
    |
    | Must match the feedbacks array from the config file
    |
    */
    'feedbacks' => [
        'like' => [
            'title' => 'Me Gusta Algo',
            'label' => 'Qué te gustó ?',
        ],
        'dislike' => [
            'title' => 'No me gusta algo',
            'label' => '¿Qué te disgustó?',
        ],
        'suggestion' => [
            'title' => 'Tengo un reclamo',
            'label' => 'Cuál es el reclamo ?',
        ],
         'bug' => [
             'title' => 'Reclamo Urgente',
             'label' => 'Ingrese su reclamo urgente',
         ],
    ],
];
