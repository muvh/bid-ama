<?php

return [
    'admin-user' => [
        'title' => 'Users',

        'actions' => [
            'index' => 'Users',
            'create' => 'New User',
            'edit' => 'Edit :name',
            'edit_profile' => 'Edit Profile',
            'edit_password' => 'Edit Password',
        ],

        'columns' => [
            'id' => 'ID',
            'first_name' => 'First name',
            'last_name' => 'Last name',
            'email' => 'Email',
            'password' => 'Password',
            'password_repeat' => 'Password Confirmation',
            'activated' => 'Activated',
            'forbidden' => 'Forbidden',
            'language' => 'Language',

            //Belongs to many relations
            'roles' => 'Roles',

        ],
    ],

    'document-type' => [
        'title' => 'Document Types',

        'actions' => [
            'index' => 'Document Types',
            'create' => 'New Document Type',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => 'ID',
            'name' => 'Name',
            'enabled' => 'Enabled',
            'type' => 'Type',

        ],
    ],

    'applicant-document' => [
        'title' => 'Applicant Documents',

        'actions' => [
            'index' => 'Applicant Documents',
            'create' => 'New Applicant Document',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => 'ID',
            'applicant_id' => 'Applicant',
            'document_id' => 'Document',

        ],
    ],

    'construction-entity' => [
        'title' => 'Construction Entities',

        'actions' => [
            'index' => 'Construction Entities',
            'create' => 'New Construction Entity',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => 'ID',
            'name' => 'Name',

        ],
    ],

    'financial-entity' => [
        'title' => 'Financial Entities',

        'actions' => [
            'index' => 'Financial Entities',
            'create' => 'New Financial Entity',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => 'ID',
            'name' => 'Name',

        ],
    ],

    'dialogs' => [
        'duplicateDialog' => [
            'title' => 'Atención!',
            'text' => 'Do you really want to duplicate this item?',
            'yes' => 'Yes, duplicate.',
            'no' => 'No, cancel.',
            'success_title' => 'Success!',
            'success' => 'Item successfully duplicated.',
            'error_title' => 'Error!',
            'error' => 'An error has occured.',
        ],
        'deleteDialog' => [
            'title' => 'Atención!',
            'text' => 'Realmente quieres eliminar este elemento?',
            'yes' => 'Si, borrar.',
            'no' => 'No, cancelar.',
            'success_title' => 'Éxito!',
            'success' => 'Elemento eliminado con éxito.',
            'error_title' => 'Error!',
            'error' => 'Se ha producido un error.',
        ],
        'publishNowDialog' => [
            'title' => 'Warning!',
            'text' => 'Do you really want to publish this item now?',
            'yes' => 'Yes, publish now.',
            'no' => 'No, cancel.',
            'success_title' => 'Success!',
            'success' => 'Item successfully published.',
            'error_title' => 'Error!',
            'error' => 'An error has occured.',
        ],
        'unpublishNowDialog' => [
            'title' => 'Warning!',
            'text' => 'Do you really want to unpublish this item now?',
            'yes' => 'Yes, unpublish now.',
            'no' => 'No, cancel.',
            'success_title' => 'Success!',
            'success' => 'Item successfully published.',
            'error_title' => 'Error!',
            'error' => 'An error has occured.',
        ],
        'publishLaterDialog' => [
            'text' => 'Please choose the date when the item should be published:',
            'yes' => 'Save',
            'no' => 'Cancel',
            'success_title' => 'Success!',
            'success' => 'Item was successfully saved.',
            'error_title' => 'Error!',
            'error' => 'An error has occured.',
        ],
    ],

    'status' => [
        'title' => 'Statuses',

        'actions' => [
            'index' => 'Statuses',
            'create' => 'New Status',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => 'ID',
            'name' => 'Name',
            'applicant' => 'Applicant',
            
        ],
    ],

    'status' => [
        'title' => 'Statuses',

        'actions' => [
            'index' => 'Statuses',
            'create' => 'New Status',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => 'ID',
            'name' => 'Name',
            'applicant' => 'Applicant',
            'prefix' => 'Prefix',
            
        ],
    ],

    'financial-summary' => [
        'title' => 'Financial Summaries',

        'actions' => [
            'index' => 'Financial Summaries',
            'create' => 'New Financial Summary',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => 'ID',
            'applicant_id' => 'Applicant',
            'desembolso' => 'Desembolso',
            'date' => 'Date',
            
        ],
    ],

    'familiar-setting' => [
        'title' => 'Familiar Settings',

        'actions' => [
            'index' => 'Familiar Settings',
            'create' => 'New Familiar Setting',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => 'ID',
            'name' => 'Name',
            
        ],
    ],

    'type-order' => [
        'title' => 'Type Orders',

        'actions' => [
            'index' => 'Type Orders',
            'create' => 'New Type Order',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => 'ID',
            'title' => 'Title',
            
        ],
    ],

    'transfer-order' => [
        'title' => 'Transfer Orders',

        'actions' => [
            'index' => 'Transfer Orders',
            'create' => 'New Transfer Order',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => 'ID',
            'nro_solicitud' => 'Nro solicitud',
            'fecha_ot' => 'Fecha ot',
            'importe' => 'Importe',
            'type_order_id' => 'Type order',
            'construction_entity_id' => 'Construction entity',
            'applicant_id' => 'Applicant',
            'financial_entity_id' => 'Financial entity',
            
        ],
    ],

    'insurance-entity' => [
        'title' => 'Insurance Entities',

        'actions' => [
            'index' => 'Insurance Entities',
            'create' => 'New Insurance Entity',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => 'ID',
            'title' => 'Title',
            
        ],
    ],

    'insurance-policy' => [
        'title' => 'Insurance Policies',

        'actions' => [
            'index' => 'Insurance Policies',
            'create' => 'New Insurance Policy',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => 'ID',
            'nro_poliza' => 'Nro poliza',
            'concepto' => 'Concepto',
            'fecha_vigencia' => 'Fecha vigencia',
            'fecha_vencimiento' => 'Fecha vencimiento',
            'fecha_desembolso' => 'Fecha desembolso',
            'importe' => 'Importe',
            'applicant_id' => 'Applicant',
            'insurance_entity_id' => 'Insurance entity',
            'construction_entity_id' => 'Construction entity',
            
        ],
    ],

    'construction-entity' => [
        'title' => 'Construction Entities',

        'actions' => [
            'index' => 'Construction Entities',
            'create' => 'New Construction Entity',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => 'ID',
            'name' => 'Name',
            'bank_name' => 'Bank name',
            'cta_cte' => 'Cta cte',
            'const_ruc' => 'Const ruc',
            
        ],
    ],

    'financial-entity' => [
        'title' => 'Financial Entities',

        'actions' => [
            'index' => 'Financial Entities',
            'create' => 'New Financial Entity',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => 'ID',
            'name' => 'Name',
            'bank_name' => 'Bank name',
            'address' => 'Address',
            'cta_cte' => 'Cta cte',
            'const_ruc' => 'Const ruc',
            
        ],
    ],

    'trust' => [
        'title' => 'Trusts',

        'actions' => [
            'index' => 'Trusts',
            'create' => 'New Trust',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => 'ID',
            'nro_tr' => 'Nro tr',
            'fecha_tr' => 'Fecha tr',
            'construction_entity_id' => 'Construction entity',
            
        ],
    ],

    'applicant-trust' => [
        'title' => 'Applicant Trusts',

        'actions' => [
            'index' => 'Applicant Trusts',
            'create' => 'New Applicant Trust',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => 'ID',
            
        ],
    ],

    'applicants-trust' => [
        'title' => 'Applicants Trusts',

        'actions' => [
            'index' => 'Applicants Trusts',
            'create' => 'New Applicants Trust',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => 'ID',
            'applicant_id' => 'Applicant',
            'trust_id' => 'Trust',
            
        ],
    ],

    'resource-transfer' => [
        'title' => 'Resource Transfers',

        'actions' => [
            'index' => 'Resource Transfers',
            'create' => 'New Resource Transfer',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => 'ID',
            'nro_solictud_rt' => 'Nro solictud rt',
            'fecha_rt' => 'Fecha rt',
            'type_order_id' => 'Type order',
            'construction_entity_id' => 'Construction entity',
            
        ],
    ],

    'applicant-resource-transfer' => [
        'title' => 'Applicant Resource Transfers',

        'actions' => [
            'index' => 'Applicant Resource Transfers',
            'create' => 'New Applicant Resource Transfer',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => 'ID',
            'applicant_id' => 'Applicant',
            'resource_transfer_id' => 'Resource transfer',
            
        ],
    ],

    'construction-entity' => [
        'title' => 'Construction Entities',

        'actions' => [
            'index' => 'Construction Entities',
            'create' => 'New Construction Entity',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => 'ID',
            'name' => 'Name',
            'bank_name' => 'Bank name',
            'cta_cte' => 'Cta cte',
            'const_ruc' => 'Const ruc',
            'razon_social' => 'Razon social',
            'repre' => 'Repre',
            'nro_ci' => 'Nro ci',
            
        ],
    ],

    'certificate' => [
        'title' => 'Certificates',

        'actions' => [
            'index' => 'Certificates',
            'create' => 'New Certificate',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => 'ID',
            'fecha_tr' => 'Fecha tr',
            
        ],
    ],

    'supervisor' => [
        'title' => 'Supervisors',

        'actions' => [
            'index' => 'Supervisors',
            'create' => 'New Supervisor',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => 'ID',
            'name' => 'Name',
            
        ],
    ],

    'type-check' => [
        'title' => 'Type Checks',

        'actions' => [
            'index' => 'Type Checks',
            'create' => 'New Type Check',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => 'ID',
            'name' => 'Name',
            
        ],
    ],

    'work' => [
        'title' => 'Works',

        'actions' => [
            'index' => 'Works',
            'create' => 'New Work',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => 'ID',
            'fecha_ws' => 'Fecha ws',
            'applicant_id' => 'Applicant',
            'supervisor_id' => 'Supervisor',
            
        ],
    ],

    'works-type-check' => [
        'title' => 'Works Type Checks',

        'actions' => [
            'index' => 'Works Type Checks',
            'create' => 'New Works Type Check',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => 'ID',
            'work_id' => 'Work',
            'type_check_id' => 'Type check',
            
        ],
    ],

    'feedback' => [
        'title' => 'Feedbacks',

        'actions' => [
            'index' => 'Feedbacks',
            'create' => 'New Feedback',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => 'ID',
            'type' => 'Type',
            'message' => 'Message',
            'user_info' => 'User info',
            'reviewed' => 'Reviewed',
            
        ],
    ],

    'visit' => [
        'title' => 'Visits',

        'actions' => [
            'index' => 'Visits',
            'create' => 'New Visit',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => 'ID',
            'applicant_id' => 'Applicant',
            'visit_number' => 'Visit number',
            'advance' => 'Advance',
            'latitude' => 'Latitude',
            'longitude' => 'Longitude',
            'visit_date' => 'Visit date',
            
        ],
    ],

    'works-datum' => [
        'title' => 'Works Data',

        'actions' => [
            'index' => 'Works Data',
            'create' => 'New Works Datum',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => 'ID',
            'applicant_id' => 'Applicant',
            'start_work' => 'Start work',
            'latitude' => 'Latitude',
            'longitude' => 'Longitude',
            
        ],
    ],

    'accounting-record' => [
        'title' => 'Accounting Records',

        'actions' => [
            'index' => 'Accounting Records',
            'create' => 'New Accounting Record',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => 'ID',
            'summary' => 'Summary',
            'type_movements_id' => 'Type movements',
            'amount' => 'Amount',
            'balance' => 'Balance',
            'confirmed' => 'Confirmed',
            'resolution' => 'Resolution',
            'registration_date' => 'Registration date',
            '0' => '0',
            
        ],
    ],

    'applicant-record' => [
        'title' => 'Applicant Records',

        'actions' => [
            'index' => 'Applicant Records',
            'create' => 'New Applicant Record',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => 'ID',
            'accounting_record_id' => 'Accounting record',
            'applicant_id' => 'Applicant',
            'amount' => 'Amount',
            
        ],
    ],

    'accounting-summary' => [
        'title' => 'Accounting Summary',

        'actions' => [
            'index' => 'Accounting Summary',
            'create' => 'New Accounting Summary',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => 'ID',
            'income' => 'Income',
            'expenses' => 'Expenses',
            'balance' => 'Balance',
            
        ],
    ],

    'type-movement' => [
        'title' => 'Type Movements',

        'actions' => [
            'index' => 'Type Movements',
            'create' => 'New Type Movement',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => 'ID',
            'name' => 'Name',
            
        ],
    ],

    // Do not delete me :) I'm used for auto-generation
];
