<?php

return [
    'admin-user' => [
        'title' => 'Users',

        'actions' => [
            'index' => 'Users',
            'create' => 'New User',
            'edit' => 'Edit :name',
            'edit_profile' => 'Edit Profile',
            'edit_password' => 'Edit Password',
        ],

        'columns' => [
            'id' => 'ID',
            'first_name' => 'First name',
            'last_name' => 'Last name',
            'email' => 'Email',
            'password' => 'Password',
            'password_repeat' => 'Password Confirmation',
            'activated' => 'Activated',
            'forbidden' => 'Forbidden',
            'language' => 'Language',

            //Belongs to many relations
            'roles' => 'Roles',

        ],
    ],

    'document-type' => [
        'title' => 'Document Types',

        'actions' => [
            'index' => 'Document Types',
            'create' => 'New Document Type',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => 'ID',
            'name' => 'Name',
            'enabled' => 'Enabled',
            'type' => 'Type',

        ],
    ],

    'applicant-document' => [
        'title' => 'Applicant Documents',

        'actions' => [
            'index' => 'Applicant Documents',
            'create' => 'New Applicant Document',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => 'ID',
            'applicant_id' => 'Applicant',
            'document_id' => 'Document',

        ],
    ],

    'construction-entity' => [
        'title' => 'Construction Entities',

        'actions' => [
            'index' => 'Construction Entities',
            'create' => 'New Construction Entity',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => 'ID',
            'name' => 'Name',

        ],
    ],

    'financial-entity' => [
        'title' => 'Financial Entities',

        'actions' => [
            'index' => 'Financial Entities',
            'create' => 'New Financial Entity',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => 'ID',
            'name' => 'Name',

        ],
    ],

    'dialogs' => [
        'duplicateDialog' => [
            'title' => 'Atención!',
            'text' => 'Do you really want to duplicate this item?',
            'yes' => 'Yes, duplicate.',
            'no' => 'No, cancel.',
            'success_title' => 'Success!',
            'success' => 'Item successfully duplicated.',
            'error_title' => 'Error!',
            'error' => 'An error has occured.',
        ],
        'deleteDialog' => [
            'title' => 'Atención!',
            'text' => 'Realmente quieres eliminar este elemento?',
            'yes' => 'Si, borrar.',
            'no' => 'No, cancelar.',
            'success_title' => 'Éxito!',
            'success' => 'Elemento eliminado con éxito.',
            'error_title' => 'Error!',
            'error' => 'Se ha producido un error.',
        ],
        'publishNowDialog' => [
            'title' => 'Warning!',
            'text' => 'Do you really want to publish this item now?',
            'yes' => 'Yes, publish now.',
            'no' => 'No, cancel.',
            'success_title' => 'Success!',
            'success' => 'Item successfully published.',
            'error_title' => 'Error!',
            'error' => 'An error has occured.',
        ],
        'unpublishNowDialog' => [
            'title' => 'Warning!',
            'text' => 'Do you really want to unpublish this item now?',
            'yes' => 'Yes, unpublish now.',
            'no' => 'No, cancel.',
            'success_title' => 'Success!',
            'success' => 'Item successfully published.',
            'error_title' => 'Error!',
            'error' => 'An error has occured.',
        ],
        'publishLaterDialog' => [
            'text' => 'Please choose the date when the item should be published:',
            'yes' => 'Save',
            'no' => 'Cancel',
            'success_title' => 'Success!',
            'success' => 'Item was successfully saved.',
            'error_title' => 'Error!',
            'error' => 'An error has occured.',
        ],
    ],

    // Do not delete me :) I'm used for auto-generation
];
