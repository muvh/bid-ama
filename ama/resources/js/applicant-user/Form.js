import AppForm from '../admin/app-components/Form/AppForm';

Vue.component('applicantuser-form', {
    mixins: [AppForm],
    props: ['cities', 'educationlevels', 'diseases', 'disabilities', 'applicantrelationships',
            'saveddiseases', 'saveddisabilities', 'phonenumber', 'finddataurl','showrelationships'
    ],
    data: function() {
        return {
            form: {
                names:  '',
                last_names:  '',
                birthdate:  '',
                gender:  '',
                nationality: '',
                government_id:  '',
                marital_status:  '',
                pregnant:  false,
                pregnancy_due_date:  null,
                parent_applicant:  null,
                cadaster:  '',
                property_id:  '',
                occupation:  '',
                ruc:  '',
                monthly_income:  '',
                address: '',
                neighborhood: '',
                city:  '' ,
                educationlevel:  '',
                disease: '',
                disability: '',
                contactmethod: '',
                applicantrelationship: '',
                phone: '',


            },
            datePickerConfig: {
                dateFormat: 'Y-m-d',
                altFormat: 'd-m-Y',
                noCalendar: true
            },

        }
    },

    methods: {
        addNewContact: function () {
            this.form.contactcomponents.push(Vue.util.extend({}, this.form.contactcomponent))
        },
        deleteContact: function (index) {
            Vue.delete(this.form.contactcomponents, index);
        },
        findData: function () {

            let loader = this.$loading.show({
                canCancel: false,
            });

            axios.get(this.form.government_id+'/hadbenefit').then(
                (response) => {
                    if(response.data.error===false){
                        if(parseInt(response.data.message.cod)===1){
                            loader.hide();
                            this.showModal();
                        }
                    }else{
                        this.$notify({ type: 'error', title: 'Error buscando beneficios', text: response.data.message});
                        loader.hide();
                    }
                });

            axios.get(this.form.government_id+'/identificaciones').then(
                (response) => {
                    loader.hide();
                    if(response.data.error===false){
                        var data = response.data.message;
                        this.form.names = data.nombres;
                        this.form.last_names = data.apellido;
                        //this.form.gender = data.sexo;
                        this.form.marital_status = data.estadoCivil.trim()  ;
                        this.form.nationality = data.nacionalidadBean;
                        var date = new Date(data.fechNacim);
                        this.form.birthdate = date.toISOString().split('T')[0];
                    }else{
                        this.$notify({ type: 'error', title: 'Error buscando datos', text: response.data.message });
                        loader.hide();
                    }
                })
        },
    }

});
