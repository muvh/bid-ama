import AppListing from '../app-components/Listing/AppListing';

Vue.component('document-type-listing', {
    mixins: [AppListing]
});