import AppForm from '../app-components/Form/AppForm';

Vue.component('work-form', {
    mixins: [AppForm],
    props:['supervisors', 'applicants'],
    data: function() {
        return {
            form: {
                //fecha_ws:  '' ,
                fecha_ws:new Date().toISOString().slice(0,10),
                applicant_id:  '' ,
                supervisor_id:  '' ,
                
            },
             datePickerConfig: {
              dateFormat: 'Y-m-d',    
              //altInput: false, 
              //noCalendar: true,
              altFormat: 'd-m-Y'
          },
        }
    },   
    created () {
      //console.log(this.governmentid);
      if(this.applicants>0){
          this.form.applicant_id=this.applicants
          this.findApplicant()
      }
  },  methods: {
        applicantsLabel({ names, last_names, government_id }) {
          return `${government_id} - ${names} ${last_names==null?'':last_names}`;
        },
        supervisorLabel({ id, name}) {
          return `${id} - ${name} `;
        },
        applicantsLoader(selectID){
          //console.log(selectID.id)
          this.form.applicant_id=selectID.id
        },
        supervisorsLoader(selectID){
          //console.log(selectID.id)
          this.form.supervisor_id=selectID.id
        }
      },

});
Vue.component('work-form-modal', {
    mixins: [AppForm],
    props:['typechecks','applicant'],
    data: function() {
        return {
            form: {
                fecha:  '' ,
                typechecks:  '' ,
                work_id:  '' ,
                applicant_id:  '' ,
                type_check_id:  '' ,
                datePickerConfig: {
                 
            },
             dateFormat: 'Y-m-d',    
            //altInput: false, 
            //noCalendar: true,
            altFormat: 'd-m-Y'
        } 
        }
    },
    created () {
      console.log(this.applicant)
      this.form.applicant_id=this.applicant.id;
      this.form.work_id=this.applicant.work[0].id;
      console.log(this.form.work_id)
        },
      methods: {
        applicantsLabel({ names, last_names, government_id }) {
          return `${government_id} - ${names} ${last_names==null?'':last_names}`;
        },
        typeLabel({ id, name}) {
          return `${id} - ${name} `;
        },
    
        supervisorsLoader(selectID){
          //console.log(selectID.id)
          this.form.supervisor_id=selectID.id
        },
        executeLoader(selectID){
          this.form.type_check_id=selectID.id

        }, onSuccess(data) {
          if(data.notify) {
              this.$notify({ type: data.notify.type, title: data.notify.title, text: data.notify.message});
             
              $('#btn-guardar').css({ 'pointer-events': 'all' });
              $('#btn-guardar').find('i').removeClass().addClass('fa fa-download');
              $('#btn-guardar').removeAttr("disabled");


          } else if (data.redirect) {
              window.location.replace(data.redirect);
          }
      }
      },

});

