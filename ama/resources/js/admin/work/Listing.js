import AppListing from '../app-components/Listing/AppListing';

Vue.component('work-listing', {
    mixins: [AppListing]
});