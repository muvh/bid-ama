import AppListing from '../app-components/Listing/AppListing';

Vue.component('insurance-entity-listing', {
    mixins: [AppListing]
});