import AppForm from '../app-components/Form/AppForm';

Vue.component('visit-form', {
    mixins: [AppForm],
    props: ['applicant'],
    data: function() {
        return {
            form: {
                applicant_id:  this.applicant ,
                visit_number:  '' ,
                advance:  '' ,
                latitude:  '' ,
                longitude:  '' ,
                visit_date: '',
                footnote: '',

            },
            mediaCollections: ['visit_gallery']
        }
    }

});
