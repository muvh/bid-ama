import AppForm from '../app-components/Form/AppForm';

Vue.component('transfer-order-form', {
    mixins: [AppForm],
    props: ['governmentid'],
    data: function() {
        return {
            form: {
                nro_solicitud:  '' ,
                fecha_ot:  '' ,
                importe:  '' ,
                type_order_id:  '' ,
                construction_entity_id:  '' ,
                construction_entity_name:  '' ,
                applicant_id:  '' ,
                financial_entity_id:  '' ,
                financial_entity_name:  '' ,
                government_id:  '' ,
                name:  '' ,
                last_name:  '' ,
                nro_poliza:'',
                aseguradoras:'',
                concepto:''
                
            }, 
            datePickerConfig: {
                dateFormat: 'Y-m-d',    
                //altInput: false, 
                //noCalendar: true,
                altFormat: 'd-m-Y',
                locale: 'es.js',
            },
            polizalists:[],
            newpoliza:false
        }
    },
    created () {
        //console.log(this.governmentid);
        if(this.governmentid>0){
            this.form.government_id=this.governmentid
            this.findApplicant()
        }
    },
    methods: {
        executeLoader(selectID){
            //console.log(selectID.id);
            if(selectID.id==1){
                this.form.importe='28.000.000';
            }else{
                this.form.importe='7.000.000';
            }
        },

        findApplicant(){
            if(this.form.government_id==''){
                //console.log('findApplicant ok'+this.form.government_id)
                  this.$notify({ group: 'auth', 
                  type:'error',
                  text: 'Debe agregar un numero de Cedula que ya se encuentre en la base de datos de AMA' })
                  
            }else{

                axios.get('/admin/transfer-orders/'+this.form.government_id+'/find-applicant').then(
                    (response) => {
                        console.log('findApplicant '+response.data)
                        if(response.data.cod==0){
                            this.$notify({ group: 'auth', 
                            type:'warn',
                            text: 'El numero de cedula ingresado no se ha podido encontrar!!' })
                        }else{
                            //console.log('findApplicant '+this.form.government_id)
                            console.log(response.data)
                            var lastname;
                            if(response.data.data.last_names===null){
                                lastname='';
                            }else{
                                lastname=response.data.data.last_names;
                            } 
                            this.form.name=response.data.data.names+" "+lastname
                            console.log(response.data.data.names+" "+lastname);
                            //this.form.last_names=response.data.data.last_names
                            this.form.financial_entity_id=response.data.data.financial_entity
                            this.form.construction_entity_id=response.data.data.construction_entity
                            this.form.construction_entity_name=response.data.data.construction.name
                            this.form.financial_entity_name=response.data.data.financial.name
                            this.form.applicant_id=response.data.data.id
                            this.polizalists=response.data.poliza
                            
                            console.log('LENGTH'+response.data.poliza.length);
                            if(response.data.poliza.length==0 || response.data.poliza.length==1){
                                //console.log(response.data);
                                this.newpoliza=true;
                                
                            }
                     
                        }
                    });
        }
        }
        ,
                        onSuccess(data) {
                            if(data.notify) {
                                this.$notify({ type: data.notify.type, title: data.notify.title, text: data.notify.message});
                               
                                $('#btn-guardar').css({ 'pointer-events': 'all' });
                                $('#btn-guardar').find('i').removeClass().addClass('fa fa-download');
                                $('#btn-guardar').removeAttr("disabled");
                
                
                            } else if (data.redirect) {
                                window.location.replace(data.redirect);
                            }
                        },
    },
});
import moment from 'moment';
Vue.filter('formatDate', function(value) {
  if (value) {
      return moment(String(value)).format('DD/MM/YYYY')
  }
});
