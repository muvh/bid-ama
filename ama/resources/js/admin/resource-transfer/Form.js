import AppForm from '../app-components/Form/AppForm';

Vue.component('resource-transfer-form', {
    mixins: [AppForm],
    props: ['resourcetransfer'],
    data: function() {
        return {
            form: {
                nro_solictud_rt:  '' ,
                fecha_rt:  '' ,
                fecha_rt_date:  '' ,
                type_order_id:  '' ,
                type_order:  '' ,
                type_order_name:  '' ,
                construction_entity_id:  '' ,
                construction_entity:  '' ,
                construction_entity_name:  '' ,
                
            },
            datePickerConfig: {
            dateFormat: 'Y-m-d',    
            //altInput: false, 
            //noCalendar: true,
            altFormat: 'd-m-Y'
        },
        }
    }, 
    created() {
        //console.log(this.resourcetransfer)
       this.form.type_order_name= this.resourcetransfer.typeorder.title
       this.form.construction_entity_name= this.resourcetransfer.construction.name
       var dt=moment(String(this.resourcetransfer.fecha_rt)).format('DD-MM-YYYY');
       this.form.fecha_rt_date=dt;
       this.findlistado()
    },

    methods: {
        onSuccess(data) {
            if(data.notify) {
                this.$notify({ type: data.notify.type, title: data.notify.title, text: data.notify.message});
               
                $('#btn-guardar').css({ 'pointer-events': 'all' });
                $('#btn-guardar').find('i').removeClass().addClass('fa fa-download');
                $('#btn-guardar').removeAttr("disabled");


            } else if (data.redirect) {
                window.location.replace(data.redirect);
            }
        },
        getWidth(index){
            //console.log(index.applicant_relationship)
            if(index === null) {
                return "";
              }else{
                return "background-color:yellow "
              }
        },
        findlistado(){
            //console.log('findlistado ->'+this.resourcetransfer.id)
            axios.get('/admin/resource-transfers/find-listado/'+this.resourcetransfer.id).then(
                (response) => { 
                    console.log(response.data)
                    this.listado = response.data;

                });
        },
    },

});


Vue.component('resource-transfer-modal', {
    mixins: [AppForm],
    props: ['resourcetransfer'],
    data: function() {
        return {
            form: {
  
                government_id:'',
                applicant_id:'',
                resource_transfer_id:'',
                
            },
            applicant_id:'',
            resource_transfer_id:'',
            datePickerConfig: {
            dateFormat: 'Y-m-d',    
            //altInput: false, 
            //noCalendar: true,
            altFormat: 'd-m-Y'
            },
                listApplicant:[]
        }

        
    },
    created(){
     
    },
methods:{
    findApplicant(){
        if(this.form.government_id==''){
            //console.log('findApplicant ok'+this.form.government_id)
              this.$notify({ group: 'auth', 
              type:'error',
              text: 'Debe agregar un numero de Cedula que ya se encuentre en la base de datos de AMA' })
              
        }else{

            axios.get('/admin/resource-transfers/'+this.form.government_id+'/find-applicant').then(
                (response) => {
                    if(response.data.cod==0){
                        this.$notify({ group: 'auth', 
                        type:'warn',
                        text: 'El numero de cedula ingresado no se ha podido encontrar!!' })
                    }else{
                        //console.log('findApplicant '+this.form.government_id)
                        var lastname;
                        if(response.data.data.last_names===null){
                            lastname='';
                        }else{
                            lastname=response.data.data.last_names;
                        }
                        //this.listApplicant=response.data.data.names+" "+lastname
                        //console.log(this.resourcetransfer)
                        this.listApplicant=response.data.data
                        this.applicant_id=response.data.data.id
                        this.resource_transfer_id=this.resourcetransfer.id
                        //this.form.last_names=response.data.data.last_names
              
                    }
                });
    }
    },
    onSuccess(data) {
        if(data.notify) {
            this.$notify({ type: data.notify.type, title: data.notify.title, text: data.notify.message});
           
            $('#btn-guardar').css({ 'pointer-events': 'all' });
            $('#btn-guardar').find('i').removeClass().addClass('fa fa-download');
            $('#btn-guardar').removeAttr("disabled");


        } else if (data.redirect) {
            window.location.replace(data.redirect);
        }
    },
}
});

import moment from 'moment';
Vue.filter('formatDate', function(value) {
  if (value) {
      return moment(String(value)).format('DD/MM/YYYY hh:mm')
  }
});