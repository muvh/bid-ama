import AppForm from '../app-components/Form/AppForm';

Vue.component('certificate-form', {
    mixins: [AppForm],
    props:['applicants'],
    data: function() {
        return {
            form: {
                fecha_tr:  '' ,
                applicants: ''

            }
        }
    },
    methods: {
        customLabel({ names, last_names, government_id }) {
          return `${government_id} - ${names} ${last_names?last_names:''}`;
        },
        onSuccess(data) {
          if(data.notify) {
              this.$notify({ type: data.notify.type, title: data.notify.title, text: data.notify.message});
             
              $('#btn-guardar').css({ 'pointer-events': 'all' });
              $('#btn-guardar').find('i').removeClass().addClass('fa fa-download');
              $('#btn-guardar').removeAttr("disabled");
  
  
          } else if (data.redirect) {
              window.location.replace(data.redirect);
          }
      },
      },

});
import moment from 'moment';
Vue.filter('formatDate', function(value) {
  if (value) {
      return moment(String(value)).format('DD/MM/YYYY')
  }
});