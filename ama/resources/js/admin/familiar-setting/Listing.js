import AppListing from '../app-components/Listing/AppListing';

Vue.component('familiar-setting-listing', {
    mixins: [AppListing]
});