import AppForm from '../app-components/Form/AppForm';

Vue.component('works-datum-form', {
    mixins: [AppForm],
    props: ['applicant'],
    data: function() {
        return {
            form: {
                applicant_id:  this.applicant ,
                start_work:  '' ,
                latitude:  '' ,
                longitude:  '' ,

            }
        }
    }

});
