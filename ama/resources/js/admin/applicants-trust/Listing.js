import AppListing from '../app-components/Listing/AppListing';

Vue.component('applicants-trust-listing', {
    mixins: [AppListing]
});