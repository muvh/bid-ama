import AppForm from '../app-components/Form/AppForm';

Vue.component('accounting-summary-form', {
    mixins: [AppForm],
    data: function() {
        return {
            form: {
                income:  '' ,
                expenses:  '' ,
                balance:  '' ,
                
            }
        }
    }

});