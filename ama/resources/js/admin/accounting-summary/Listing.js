import AppListing from '../app-components/Listing/AppListing';

Vue.component('accounting-summary-listing', {
    mixins: [AppListing]
});