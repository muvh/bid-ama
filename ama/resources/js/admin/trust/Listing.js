import AppListing from '../app-components/Listing/AppListing';

Vue.component('trust-listing', {
    mixins: [AppListing]
});