import AppForm from '../app-components/Form/AppForm';

Vue.component('trust-form', {
    mixins: [AppForm],
    props: ['trust'],
    data: function() {
        return {
            form: {
                nro_tr:  '' ,
                fecha_tr:  '' ,
                financial_entity_id:  '' ,
                financial_entity:  '' ,
                btnboton:false,
                name:'',
                date:'',
            },
            datePickerConfig: {
            dateFormat: 'Y-m-d',    
            //altInput: false, 
            //noCalendar: true,
            altFormat: 'd-m-Y'
        },

        listado:[],
       
        }
    },
     created () {
    var dt=moment(String(this.trust.fecha_tr)).format('DD-MM-YYYY');
     this.form.name=this.trust.financial.name;
     this.form.date=dt;

    this.findlistado();
           
    },
    methods:{
        findlistado(){
            axios.get('/admin/trusts/find-listado/'+this.trust.id).then(
                (response) => { 
                    console.log(response.data)
                    this.listado = response.data;

                });
        },
        onSuccess(data) {
            if(data.notify) {
                this.$notify({ type: data.notify.type, title: data.notify.title, text: data.notify.message});
               
                $('#btn-guardar').css({ 'pointer-events': 'all' });
                $('#btn-guardar').find('i').removeClass().addClass('fa fa-download');
                $('#btn-guardar').removeAttr("disabled");


            } else if (data.redirect) {
                window.location.replace(data.redirect);
            }
        },
    }
    
});

import moment from 'moment'

Vue.filter('formatDate', function(value) {
  if (value) {
    return moment(String(value)).format('MM/DD/YYYY hh:mm')
  }
});



Vue.component('trust-form-modal', {
    mixins: [AppForm],
    props: ['trust'],
    data: function() {
        return {
            form: {
  
                government_id:'',
                applicant_id:'',
                trust_id:'',

                
            },
            nombre:' ',
            cedula:' ',
            applicant_id:'',
            trust_id:'',
            datePickerConfig: {
            dateFormat: 'Y-m-d',    
            //altInput: false, 
            //noCalendar: true,
            altFormat: 'd-m-Y'
            },
                listApplicant:[],
                listTusts:[]
        }

        
    },
 
methods:{
    findApplicant(){
        if(this.form.government_id==''){
            //console.log('findApplicant ok'+this.form.government_id)
              this.$notify({ group: 'auth', 
              type:'error',
              text: 'Debe agregar un numero de Cedula que ya se encuentre en la base de datos de AMA' })
              
        }else{

            axios.get('/admin/trusts/'+this.form.government_id+'/find-applicant').then(
                (response) => {
                    if(response.data.cod==0){
                        this.$notify({ group: 'auth', 
                        type:'warn',
                        text: 'El numero de cedula ingresado no se ha podido encontrar!!' })
                    }else if(response.data.cod==2){
                        this.$notify({ group: 'auth', 
                        type:'warn',
                        text: 'El numero de cedula ya se encuentra cargado' })
                        this.listApplicant=response.data.data
                        this.listTusts=response.data.trust
                    
                    }else{
                        console.log('findApplicant '+response.data.data.last_names)
                        var lastname;
                        if(response.data.data.last_names===null){
                            lastname='';
                        }else{
                            lastname=response.data.data.last_names;
                        }
                        this.nombre=response.data.data.names+" "+lastname
                        this.cedula=response.data.data.government_id
                        console.log(response.data)
                        this.listApplicant=response.data.data
                        this.listTusts=response.data.trust
                        this.applicant_id=response.data.data.id
                        this.trust_id=this.trust.id
                        //this.form.last_names=response.data.data.last_names
              
                    }
                });
    }
    },
    deleteItem(url){
        console.log(url)
     /*    axios.get('/admin/trusts/'+this.form.government_id+'/find-applicant').then(
            (response) => {
                if(response.data.cod==0){ */
    },
    onSuccess(data) {
        if(data.notify) {
            this.$notify({ type: data.notify.type, title: data.notify.title, text: data.notify.message});
           
            $('#btn-guardar').css({ 'pointer-events': 'all' });
            $('#btn-guardar').find('i').removeClass().addClass('fa fa-download');
            $('#btn-guardar').removeAttr("disabled");


        } else if (data.redirect) {
            window.location.replace(data.redirect);
        }
    },
}
});