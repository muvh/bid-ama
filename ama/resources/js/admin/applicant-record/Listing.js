import AppListing from '../app-components/Listing/AppListing';

Vue.component('applicant-record-listing', {
    mixins: [AppListing],
    props: ['confirmed'],
    data: function() {
        return {
            confirm: this.confirmed,
            borrado: false,
            totalBorrado: ''
        }
    },
    methods: {
        formatPrice : function (value) {
        let val = (value/1).toFixed(0).replace('.', ',')
        return val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")
        },
        deleteItem: function deleteItem(url) {
            var _this7 = this;

            this.$modal.show('dialog', {
                title: 'Atención!',
                text: 'Realmente quieres eliminar este elemento?',
                buttons: [{ title: 'No, cancelar.' }, {
                    title: '<span class="btn-dialog btn-danger">Si, borrar.<span>',
                    handler: function handler() {
                        _this7.$modal.hide('dialog');
                        axios.delete(url).then(function (response) {
                            _this7.loadData();
                            console.log(response.data);
                            _this7.borrado = true
                            _this7.totalBorrado = response.data
                            _this7.$notify({ type: 'success', title: 'Éxito!', text: response.data.message ? response.data.message : 'Elemento eliminado con éxito.' });
                        }, function (error) {
                            _this7.$notify({ type: 'error', title: 'Error!', text: error.response.data.message ? error.response.data.message : 'Un error ha ocurrido.' });
                        });
                    }
                }]
            });
        },
        confirmItem: function (url) {
            var _this7 = this;

            this.$modal.show('dialog', {
                title: 'Atención!',
                text: 'Realmente quieres confirmar esta Reserva?',
                buttons: [{ title: 'No, cancelar.' }, {
                    title: '<span class="btn-dialog btn-success">Si, Confirmar.<span>',
                    handler: function handler() {
                        _this7.$modal.hide('dialog');
                        axios.post(url).then(function (response) {
                            console.log(response);
                            _this7.confirm = true
                            //_this7.loadData();
                            _this7.$notify({ type: 'success', title: 'Éxito!', text: response.data.message ? response.data.message : 'Elemento eliminado con éxito.' });
                        }, function (error) {
                            console.log(error);
                            //_this7.$notify({ type: 'error', title: 'Error!', text: error.response.data.message ? error.response.data.message : 'Un error ha ocurrido.' });
                        });
                    }
                }]
            });
        },
    },
});
