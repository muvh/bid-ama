import AppForm from '../app-components/Form/AppForm';

Vue.component('applicant-record-form', {
    mixins: [AppForm],
    props: ['accounting_record','finddataurl'],
    data: function() {
        return {
            form: {
                accounting_record_id: this.accounting_record,
                names:  '',
                last_names:  '',
                applicant_id: '',
                government_id:  '',
                amount:  '34000000' ,

            }
        }
    },
    methods: {
        findData: function () {


            $('#verificando').show()
            //busca si existe en la BD de postgres
            axios.get(this.finddataurl+'/'+this.form.government_id+'/findapplicantrecord').then(
                (response) => {
                    console.log(response.data);
                    $('#verificando').hide();
                    if(response.data.error===false){
                        var data = response.data;
                        this.form.names = data.nombre;
                        this.form.last_names = data.apellido;
                        this.form.applicant_id = data.id

                    }else{
                        this.$notify({ type: 'error', title: 'Error buscando datos', text: response.data.message});
                    }

                });





            },
    },

});
