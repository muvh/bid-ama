import './bootstrap';

import 'vue-multiselect/dist/vue-multiselect.min.css';
import flatPickr from 'vue-flatpickr-component';
import VueQuillEditor from 'vue-quill-editor';
import Notifications from 'vue-notification';
import Multiselect from 'vue-multiselect';
import VeeValidate from 'vee-validate';
//import es from 'vue-flatpickr-component/dist/locale/es';
//import es from 'vee-validate/dist/locale/es';
import 'flatpickr/dist/flatpickr.css';
import VueCookie from 'vue-cookie';
import { Admin } from 'craftable';
import VModal from 'vue-js-modal'
import Vue from 'vue';
import * as VueGoogleMaps from 'vue2-google-maps'
import {Tabs, Tab} from 'vue-tabs-component';
import VueFilterDateFormat from 'vue-filter-date-format';
import VuePaginator from 'vuejs-paginator'

import './app-components/bootstrap';
import './index';

import 'craftable/dist/ui';

import 'vue-tabs-component/docs/resources/tabs-component.css';

import Loading from 'vue-loading-overlay';
import 'vue-loading-overlay/dist/vue-loading.css';


import Oruga from '@oruga-ui/oruga';
import '@oruga-ui/oruga/dist/oruga.css';
import '@oruga-ui/oruga/dist/oruga-full.css';
Vue.use(Oruga);

Vue.component('multiselect', Multiselect);

Vue.use(VeeValidate, {strict: true});

Vue.component('datetime', flatPickr);
Vue.component('grafico', require('../components/grafico.vue').default);
Vue.component('slide', require('../components/slide.vue').default);
Vue.component('googlemap', require('../components/AddGoogleMap.vue').default);
Vue.component('mapavivienda', require('../components/mapavivienda.vue').default);
Vue.component('mapa-component', require('../components/mapa.vue').default);
Vue.use(VModal, { dialog: true, dynamic: true, injectModalsContainer: true });
Vue.use(VueQuillEditor);
Vue.use(Notifications);
Vue.use(VueCookie);
Vue.use(VueFilterDateFormat);

Vue.use(Loading, {
    isFullPage : true,
    backgroundColor: '#000',
    opacity: 0.7,
    zIndex: 9999999,
    loader: 'dots',
    color : '#fff',
    width : 70,
    height : 70,
})

Vue.use(VueGoogleMaps, {
    load: {
      key: 'AIzaSyDuvnZUZmZ1ws40ZVXYw3DF2YmnEn5MCvg',
      libraries: 'places', // This is required if you use the Autocomplete plugin
      // OR: libraries: 'places,drawing'
      // OR: libraries: 'places,drawing,visualization'
      // (as you require)

      //// If you want to set the version, you can do so:
      // v: '3.26',
    },
})


Vue.component('tabs', Tabs);
Vue.component('tab', Tab);

new Vue({
    mixins: [Admin],
});


