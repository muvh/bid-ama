import AppListing from '../app-components/Listing/AppListing';

Vue.component('supervisor-listing', {
    mixins: [AppListing]
});