import AppListing from '../app-components/Listing/AppListing';

Vue.component('financial-entity-listing', {
    mixins: [AppListing]
});