import AppForm from '../app-components/Form/AppForm';

Vue.component('financial-entity-form', {
    mixins: [AppForm],
    data: function() {
        return {
            form: {
                name:  '' ,
                bank_name:  '' ,
                address:  '' ,
                cta_cte:  '' ,
                const_ruc:  '' ,
                
            }
        }
    }

});