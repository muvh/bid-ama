import AppListing from '../app-components/Listing/AppListing';

Vue.component('type-order-listing', {
    mixins: [AppListing]
});