import AppListing from '../app-components/Listing/AppListing';

Vue.component('financial-summary-listing', {
    mixins: [AppListing]
});