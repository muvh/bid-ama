import AppForm from '../app-components/Form/AppForm';

Vue.component('construction-entity-form', {
    mixins: [AppForm],
    data: function() {
        return {
            form: {
                name:  '' ,
                bank_name:  '' ,
                cta_cte:  '' ,
                const_ruc:  '' ,
                razon_social:  '' ,
                repre:  '' ,
                nro_ci:  '' ,
                
            }
        }
    }, methods: {
        onSuccess(data) {
            if(data.notify) {
                this.$notify({ type: data.notify.type, title: data.notify.title, text: data.notify.message});
               
                $('#btn-guardar').css({ 'pointer-events': 'all' });
                $('#btn-guardar').find('i').removeClass().addClass('fa fa-download');
                $('#btn-guardar').removeAttr("disabled");


            } else if (data.redirect) {
                window.location.replace(data.redirect);
            }
        },
    },

});