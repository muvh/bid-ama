import AppListing from '../app-components/Listing/AppListing';

Vue.component('construction-entity-listing', {
    mixins: [AppListing]
});