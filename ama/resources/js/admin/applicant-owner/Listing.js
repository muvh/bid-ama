import AppListing from '../app-components/Listing/AppListing';

Vue.component('applicant-owner-listing', {
    mixins: [AppListing]
});