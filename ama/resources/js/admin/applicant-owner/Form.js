import AppForm from '../app-components/Form/AppForm';

Vue.component('applicant-owner-form', {
    mixins: [AppForm],
    props:['users','applicant'],
    data: function() {
        return {
            form: {
                applicant_id:  '' ,
                admin_user_id:  '' ,
                users:  '' ,
                
            }
        }
    }, methods: {
        customLabel({ first_name, last_name }) {
            return `${first_name} ${last_name?last_name:''}`;
          },   
          executeLoader(selectID){
              console.log(selectID.id+'-------');
        this.form.admin_user_id=selectID.id.toString(); 
    }
    },
    created() {
        this.form.applicant_id=this.applicant.id.toString();
        //console.log(this.applicant.id)
    },
 

});