import AppListing from '../app-components/Listing/AppListing';

Vue.component('type-check-listing', {
    mixins: [AppListing]
});