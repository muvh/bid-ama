import { BaseListing } from 'craftable';

export default {
	mixins: [BaseListing],
    methods: {
        deleteItem: function deleteItem(url) {
            var _this7 = this;

            this.$modal.show('dialog', {
                title: 'Atención!',
                text: 'Realmente quieres eliminar este elemento?',
                buttons: [{ title: 'No, cancelar.' }, {
                    title: '<span class="btn-dialog btn-danger">Si, borrar.<span>',
                    handler: function handler() {
                        _this7.$modal.hide('dialog');
                        axios.delete(url).then(function (response) {
                            _this7.loadData();
                            _this7.$notify({ type: 'success', title: 'Éxito!', text: response.data.message ? response.data.message : 'Elemento eliminado con éxito.' });
                        }, function (error) {
                            _this7.$notify({ type: 'error', title: 'Error!', text: error.response.data.message ? error.response.data.message : 'Un error ha ocurrido.' });
                        });
                    }
                }]
            });
        },
        toggleSwitch: function toggleSwitch(url, col, row) {
            var _this8 = this;

            axios.post(url, row).then(function (response) {
                _this8.$notify({ type: 'success', title: 'Success!', text: response.data.message ? response.data.message : 'Item successfully changed.' });
            }, function (error) {
                row[col] = !row[col];
                _this8.$notify({ type: 'error', title: 'Error!', text: error.response.data.message ? error.response.data.message : 'An error has occured.' });
            });
        }
    }
};
