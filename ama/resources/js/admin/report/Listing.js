import AppListing from '../app-components/Listing/AppListing';

Vue.component('reoprt-listing', {
    mixins: [AppListing]
});

Vue.component('export-button', {
    props: ['url','textButton','resolution'],
    template: '<a style="margin: 0px 20px 0px 20px" class="btn btn-primary btn-sm pull-right m-b-0" v-bind:href="url + resolution" role="button"><i class="fa fa-download"></i>&nbsp;{{ textButton }}</a>'
});
Vue.component('export-button-social', {
    props: ['url','textButton'],
    template: '<a style="margin: 0mpx 20px 0px 20px" class="btn btn-primary btn-sm pull-right m-b-0" v-bind:href="url" role="button"><i class="fa fa-download"></i>&nbsp;{{ textButton }}</a>'
});
Vue.component('export-seccion', {
    props: ['url','textButton'],
    template: '<a style="margin: 0mpx 20px 0px 20px" class="btn btn-primary btn-sm pull-right m-b-0" v-bind:href="url" role="button"><i class="fa fa-download"></i>&nbsp;{{ textButton }}</a>'
});

Vue.component('applicant-listing', {
    mixins: [AppListing],
    data: function() {
        return {
            form: {
                date_from: '',
                date_to: '',
                status:'',
                
            },
            datePickerConfig: {
                dateFormat: 'd-m-Y',
                altFormat: 'Y-m-d',
            },
            socialLists:[]
        }
    },
    methods:{
        getWidth(index){
            //console.log(index.applicant_relationship)
            if(index === null) {
                return "";
              }else{
                return "background-color:yellow "
              }
        },
        onSearchforParams() {
          /*   let loader = this.$loading.show({
                canCancel: false,
            }); */
            
            var statusVar= (this.form.status=='') ? 0 : this.form.status;
            var datetoVar= (this.form.date_to=='') ? 0 : this.form.date_to;
            var datefromVar= (this.form.date_from=='') ? 0 : this.form.date_from;
            var options = {
                
                 
                    status:statusVar,
                    dateto:datetoVar,
                    datefrom:datefromVar
             
            };

                axios.post('/admin/reports/applicants-by-resolution-social/', options)
                .then(response => {this.socialLists = response.data.data;
                                  this.pagination.state.total=response.data.total
                                  this.pagination.state.per_page=response.data.per_page
                                  this.pagination.state.current_page=response.data.current_page
                                  this.pagination.state.last_page=response.data.last_page
                                  this.pagination.state.from=response.data.from
                                  this.pagination.state.to=response.data.to
                                  this.pagination.state.last_page_url=response.data.last_page_url
                                  this.pagination.state.next_page_url=response.data.next_page_url
                                  this.pagination.state.path=response.data.path
                                  this.pagination.state.first_page_url=response.data.first_page_url
                                  //var response.data.total
                    //console.log(this.response.data);
                    console.log(this.pagination.state.to);
                /* loader.hide(); */
            }).catch(error => {});

          
        },

        updateResource(data){
            this.socialLists = data
          }
    }
   
});
Vue.component('selection', {
    props: ['value'],
    template: '<select v-model="post"><option v-for="post in posts" v-bind:value="post">{{post.title}}<option></select>',
    data: function() {
        return {
          posts: [{ 
            title: "lorem ipsum 1", val: '1' }, {
            title: "lorem ipsum 2", val: '2' }, {
            title: "lorem ipsum 3", val: '3' }
          ],
          post: null
        }
      },
      watch: {
        post: function() {
          this.$emit('input', this.post);
      }
    },
    props: ['value']
});

Vue.component('atc-status', {
    mixins: [AppListing],
    data: function() {
        return {
            form: {
                entity: '',
                status: '',
                config: '',
                post: false,
                atc:false,
                coop:false,
                municipio:false,
                configfamily:false,
                radio:'',
                actualPage:1
            },
            
      
            socialLists:[],
            post: null
        }
    },
    methods:{

        compareWithToday(date1,  date2){
            //return 'HOLA';
            var currentdate= new Date()
            var date2=  currentdate.toISOString().split('T')[0];
            console.log('Fecha1> '+date1);
            console.log('Fecha2> '+date2);
            date1 = new Date(date1);
            date2 = new Date(date2);
            var diff = Math.floor((Date.parse(date1) - Date.parse(date2)) / 86400000);
            return diff;
        },
        parseDate(str) {
            var mdy = str.split('-');
            console.log('mdy[2]> '+mdy[2]);
            console.log('mdy[0]-1> '+mdy[0]-1);
            console.log('mdy[1]> '+mdy[1]);
            return new Date(mdy[2], mdy[0]-1, mdy[1]);
        },
        updatePage(url, current){
            setTimeout(this.ListsPage(url,  current), 100);
        },
        ListsPage(url){
            //this.currentPage=current;
           // console.log('Click '+this.form.actualPage)
            axios.get(url+'?page='+this.form.actualPage)
         .then(response => {this.socialLists = response.data.data;
            
            });
        },
        onSearchforParams() {
             let loader = this.$loading.show({
                canCancel: false,
            }); 
            
            console.log(this.form.config);
            if(this.form.radio==1){
            var statusVar= (this.form.status.id=='undefined') ? 0 : this.form.status.id;
            var entityVar= (this.form.entity.id=='undefined') ? 0 : this.form.entity.id;
            if(entityVar==undefined || statusVar==undefined ){
                loader.hide();
                this.showModalPostulante();
            }
                axios.get('/admin/reports/atc-by-status/'+statusVar+'/'+entityVar+'/'+this.form.radio+'/'+0)
                .then(response => {this.socialLists = response.data.data;
                   // console.log(response);
                    

           
                loader.hide();
            }).catch(error => {});
        }
        if(this.form.radio==2){
            var statusVar= (this.form.status.id=='undefined') ? 0 : this.form.status.id;
            var entityVar= (this.form.coop.id=='undefined') ? 0 : this.form.coop.id;
            if(entityVar==undefined || statusVar==undefined ){
                loader.hide();
                this.showModalPostulante();
            }
                axios.get('/admin/reports/atc-by-status/'+statusVar+'/'+entityVar+'/'+this.form.radio+'/'+0)
                .then(response => {this.socialLists = response.data.data;
                    //console.log(response.data);
            
                loader.hide();
            }).catch(error => {});
        }
        if(this.form.radio==3){
            var statusVar= (this.form.status.id=='undefined') ? 0 : this.form.status.id;
            var entityVar= (this.form.municio.id=='undefined') ? 0 : this.form.municio.id;
            if(entityVar==undefined || statusVar==undefined ){
                loader.hide();
                this.showModalPostulante();
            }
                axios.get('/admin/reports/atc-by-status/'+statusVar+'/'+entityVar+'/'+this.form.radio+'/'+0)
                .then(response => {this.socialLists = response.data.data;
                loader.hide();
            }).catch(error => {});
        }
        if(this.form.radio==4){
            var config= (this.form.config.id=='undefined') ? 0 : this.form.config.id;
                console.log('Config:'+config);
                console.log('Radio'+this.form.radio);
         
                axios.get('/admin/reports/atc-by-status/0/0/'+this.form.radio+'/'+config)
                .then(response => {this.socialLists = response.data.data;
                loader.hide();
            }).catch(error => {});
        }

          
        },
        showModalPostulante () {
            this.$modal.show('dialog', {
                title: 'Atención!',
                text: 'Algunos campos del filtro se encuentran vacio',
                buttons: [
                    { title: 'Ok' }
                ]
            });
        },
        updateResource(data){
            this.socialLists = data
          },
          showtemplate(id){
              
                if(id==1){
                    this.form.atc= true;
                    this.form.coop= false;
                    this.form.municipio= false;
                    this.form.configfamily= false;
                }
                if(id==2){
                    this.form.atc= false;
                    this.form.coop= true;
                    this.form.municipio= false;
                    this.form.configfamily= false;
                }
                if(id==3){
                    this.form.atc= false;
                    this.form.coop= false;
                    this.form.municipio= true;
                    this.form.configfamily= false;
                }
                if(id==4){
                    this.form.atc= false;
                    this.form.coop= false;
                    this.form.municipio= false;
                    this.form.configfamily= true;
                }
          

          }
    },
    filters: {
        // cut yyyy-mm-dd format and return date as string
        'yymmdd': function (date) { 
          var newDay = new Date(date),
              currentDate = ('0'+newDay.getDate()).slice(-2),
              currentMonth = ('0'+ (newDay.getMonth() + 1) ).slice(-2),
              currentYear = newDay.getFullYear();
    
          var day = '';
          day = currentYear + '-' + currentMonth + '-' + currentDate;
    
          return day;
        }
      },
   
});


Vue.component('applicant-listing-coop', {
    mixins: [AppListing],
    props: ['date_to', 'date_from'],
    data: function() {
        return {
            form: {
                date_from: '',
                date_to: '',
                is_published:  false,
                type_report:  ''
            },
           
            datePickerConfig: {
                                dateFormat: 'd-m-Y',
                                altFormat: 'Y-m-d',
            }
            
        }
        
    },
    mounted: function () {
        console.log(this.date_from);
        //var dt=new Date(this.date_to).dateFormat('DD-MM-YYYY');
        this.form.date_to=this.date_to;
        this.form.date_from=this.date_from
       // $('#btn-reportsocial').prop("disabled", true);
            
        
    },
    methods:{
        onSuccess(data) {
            if(data.notify) {
               //alert('hola')
                this.$notify({ type: data.notify.type, title: data.notify.title, text: data.notify.message});
               
                $('#btn-coop').css({ 'pointer-events': 'all' });
                $('#btn-coop').find('i').removeClass().addClass('fa fa-download');
                $('#btn-coop').removeAttr("disabled");


            } else if (data.redirect) {
                window.location.replace(data.redirect);
            }
        }


    }
});
Vue.component('applicant-by-coop', {
    mixins: [AppListing],
    props: ['date_to', 'date_from'],
    data: function() {
        return {
            form: {
                date_from: '',
                date_to: '',
                is_published:  false,
                type_report:  ''
            },
           
            datePickerConfig: {
                                dateFormat: 'd-m-Y',
                                altFormat: 'Y-m-d',
            }
            
        }
        
    },
    mounted: function () {
        console.log(this.date_from);
        //var dt=new Date(this.date_to).dateFormat('DD-MM-YYYY');
        this.form.date_to=this.date_to;
        this.form.date_from=this.date_from
       // $('#btn-reportsocial').prop("disabled", true);
            
        
    },
    methods:{
        onSuccess(data) {
            if(data.notify) {
                this.$notify({ type: data.notify.type, title: data.notify.title, text: data.notify.message});
               
                $('#btn-coop').css({ 'pointer-events': 'all' });
                $('#btn-coop').find('i').removeClass().addClass('fa fa-download');
                $('#btn-coop').removeAttr("disabled");


            } else if (data.redirect) {
                window.location.replace(data.redirect);
            }
        }


    }
});
import { Bar, Doughnut, Pie, mixins, Line  } from 'vue-chartjs'

Vue.component('line-chart-summary', {
    props:[  'afpendingcount', 'labels'],
    extends: Pie ,
    options: {
    
        responsive: true,
        maintainAspectRatio: false
      },
    mounted () {
      // Overwriting base render method with actual data.
   //console.log(labels);
      this.renderChart({
          labels:this.labels,
          //labels: ['Postulantes', 'Romero'],
          datasets: [
              {
                  label:'Resumen',
                  backgroundColor: '#008FFB',    
           
            //data: [this.afpendingcount]
            data: [40, 20, 12, 39, 10, 40, 39, 80, 40, 20, 12, 11],
      
          }
        ] 
      }, this.options)
    }
  });
  Vue.component('listing-search-ci', {
    mixins: [AppListing],
   
    data: function() {
        return {
            form: {
                txtci: '',
            },
            items:[],
            datePickerConfig: {
                dateFormat: 'd-m-Y',
                altFormat: 'Y-m-d',
            },
        }
    },
  
    mounted : function() {
       
        },
    methods:{
        onSearchCI() {
            let loader = this.$loading.show({
                canCancel: false,
            });
                axios.get('/admin/reports/searchci/'+this.form.txtci)
                .then(response => {this.items = response.data.items.data ;
                    //console.log(response.data.items.data.length);
                loader.hide();
                if(response.data.items.data.length==0){
                   
                        this.$modal.show('dialog', {
                            title: 'Atención!',
                            text: 'El numero de cedula ingresado no se ha encontrado!',
                            buttons: [
                                { title: 'Ok' }
                            ]
                        });
                        //this.form.txtci.focus();
                                    }
            }).catch(error => {});

          
        },

        onSubmit(data) {

            if(data.notify) {
                this.$notify({ type: data.notify.type, title: data.notify.title, text: data.notify.message});
                $('#btn-buscar').css({ 'pointer-events': 'all' });
                $('#btn-buscar').find('i').removeClass().addClass('fa fa-download');
                $('#btn-buscar').removeAttr("disabled");
            } else if (data.redirect) {
                window.location.replace(data.redirect);
            }
        }
    }
});
  Vue.component('listing-search-checks', {
    mixins: [AppListing],
    data: function() {
        return {
            form: {
                txtci: '',
            },
            items:[],
        }
    },
    methods:{
        onSearchCI() {
    /*         let loader = this.$loading.show({
                canCancel: false,
            }); */
                axios.get('/admin/reports/verificacion-institucional/'+this.form.txtci)
                .then(response => {this.items = response.data;
                    console.log(response.data.ama);
                //loader.hide();
                if(response.data.postulante){
                   var msg=response.data.postulante.Expe+"</br>"+ (response.data.ama?response.data.ama:'')+'</br>'+ (response.data.amaconyugue?response.data.amaconyugue:'');
                        this.$modal.show('dialog', {
                            title: 'Atención!',
                            text: msg,
                            buttons: [
                                { title: 'Ok' }
                            ]
                        });
                        //this.form.txtci.focus();
                                    }
            }).catch(error => {});
        },
        onSubmit(data) {
            if(data.notify) {
                this.$notify({ type: data.notify.type, title: data.notify.title, text: data.notify.message});
                $('#btn-buscar').css({ 'pointer-events': 'all' });
                $('#btn-buscar').find('i').removeClass().addClass('fa fa-download');
                $('#btn-buscar').removeAttr("disabled");
            } else if (data.redirect) {
                window.location.replace(data.redirect);
            }
        }
    }
});

  import moment from 'moment';
  Vue.filter('formatDate', function(value) {
    if (value) {
        return moment(String(value)).format('DD/MM/YYYY hh:mm')
    }
});

import Chart from 'chart.js';
Vue.component('report-ejecucion', {
    mixins: [AppListing],
    //extends: VueChartJs.Line,
    props: ["data", "options"],
    data: function() {
        return {
            lists:[],
            yy:'2019',
            totalAdj:'--',
            totalProyeccion:'--',
            datacollection: null,
            options: {
   
                responsive: true,
                maintainAspectRatio: false
              },
        }
        },
        created() {
            this.onRefresh()
        },
        methods: {    
            onRefresh(value='') {
               
                let loader = this.$loading.show({
                    canCancel: false,
                });
                // if tid is updated, this will trigger... I Think :-D
              if(value==''){
                  
                  axios.get('/admin/reports/listado-ejecuciones/')
                  .then(response => {this.lists = response.data.data;
                    loader.hide();
                    this.totalAdj=response.data.totalAdj
                    this.totalProyeccion=response.data.totalProyeccion
                    console.log(response.data)
                    this.datacollection = {
                        labels: response.data.labels,
                        datasets: [
                          {
                            label: 'Adjudicaciones por Mes - '+this.yy,
                            backgroundColor: ['#41B482','#41B482','#41B482', '#41B482', '#41B482', '#41B482', '#41B482', '#41B482', '#41B482', '#41B482', '#41B482', '#41B482'],
                            data: response.data.dataset
                          }   ,
                          {              
                            label: 'Metas por Mes - '+this.yy,
                            backgroundColor: [ '#FFCE56','#FFCE56','#FFCE56','#FFCE56','#FFCE56','#FFCE56','#FFCE56','#FFCE56','#FFCE56','#FFCE56','#FFCE56','#FFCE56','#FFCE56'],
                            data: response.data.dataObj
                          }
                        ]
                      }
                })
                  .catch(error => {});
                }else{
                  axios.post('/admin/reports/listado-ejecuciones/'+value)
                  .then(response => {this.lists = response.data.data ;
                    this.yy=value;
                    this.totalAdj=response.data.totalAdj
                    this.totalProyeccion=response.data.totalProyeccion
                    //console.log(response.data.labels)
                    loader.hide();
                    console.log(response.data)
                    this.datacollection = {
                        labels: response.data.labels,
                        datasets: [
                          {              
                            label: 'Adjudicaciones por Mes - '+this.yy,
                            backgroundColor: ['#41B482','#41B482','#41B482', '#41B482', '#41B482', '#41B482', '#41B482', '#41B482', '#41B482', '#41B482', '#41B482', '#41B482'],

                            data: response.data.dataset
                          }
                       ,
                          {              
                            label: 'Metas por Mes - '+this.yy,
                            backgroundColor: [ '#FFCE56','#FFCE56','#FFCE56','#FFCE56','#FFCE56','#FFCE56','#FFCE56','#FFCE56','#FFCE56','#FFCE56','#FFCE56','#FFCE56','#FFCE56'],
                            data: response.data.dataObj
                          }
                        ], 
                        
                      }
                }).catch(error => {});

              }
            }
            }
           
 
  });

  
  const { reactiveProp } = mixins
Vue.component('line-chart-adj', {
   /*  props:[  'afpendingcount', 'labels'], */
   mixins: [reactiveProp],
    extends: Bar ,

      data(){
        return{
            datacollection: null
        }
      }

  });


