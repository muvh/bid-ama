import AppForm from '../app-components/Form/AppForm';

Vue.component('works-type-check-form', {
    mixins: [AppForm],
    data: function() {
        return {
            form: {
                work_id:  '' ,
                applicant_id:  '' ,
                type_check_id:  '' ,
                
            }
        }
    }

});