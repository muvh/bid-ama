import AppListing from '../app-components/Listing/AppListing';

Vue.component('works-type-check-listing', {
    mixins: [AppListing]
});