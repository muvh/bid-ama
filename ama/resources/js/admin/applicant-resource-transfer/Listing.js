import AppListing from '../app-components/Listing/AppListing';

Vue.component('applicant-resource-transfer-listing', {
    mixins: [AppListing]
});