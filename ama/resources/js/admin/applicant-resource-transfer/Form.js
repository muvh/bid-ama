import AppForm from '../app-components/Form/AppForm';

Vue.component('applicant-resource-transfer-form', {
    mixins: [AppForm],
    data: function() {
        return {
            form: {
                applicant_id:  '' ,
                resource_transfer_id:  '' ,
                
            }
        }
    }

});