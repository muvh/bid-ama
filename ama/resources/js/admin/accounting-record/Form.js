import AppForm from '../app-components/Form/AppForm';

Vue.component('accounting-record-form', {
    mixins: [AppForm],
    props: ["type_movements"],
    data: function() {
        return {
            form: {
                summary:  '' ,
                type_movements:  '' ,
                amount:  '' ,
                balance:  '' ,
                confirmed:  false ,
                resolution:  '' ,
                registration_date:  '' ,

            }
        }
    }

});
