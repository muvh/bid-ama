import AppListing from '../app-components/Listing/AppListing';

Vue.component('accounting-record-listing', {
    mixins: [AppListing],
    methods: {
        formatPrice : function (value) {
        let val = (value/1).toFixed(0).replace('.', ',')
        return val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")
    }
    },
});
