import AppForm from '../app-components/Form/AppForm';

Vue.component('insurance-policy-form', {
    mixins: [AppForm],
    props: [],
    data: function() {
        return {
            form: {
                nro_poliza:  '' ,
                concepto:  '' ,
                conceptoid:  '' ,
                fecha_vigencia:  '' ,
                fecha_vencimiento:  '' ,
                fecha_desembolso:  '' ,
                fecha_envio_cac:  '' ,
                importe:  '' ,
                applicant_id:  '' ,
                applicant:  '' ,
                diferenia:  '' ,
                insurance_entity_id:  '' ,
                construction_entity_id:  '' ,
                insuranceentity:'',
                government_id:'',
                
            }, 
            conceptOption: [
                { text: 'Anticipo Financiero (100%)', value: 1 },
                { text: 'Fiel Cumplimiento (10%)', value: 2 },
            ],
            datePickerConfig: {
                dateFormat: 'Y-m-d',    
                //altInput: false, 
                //noCalendar: true,
                altFormat: 'd-m-Y',
                //locale: 'es.js',
            },
        }
    },
 
   mounted(){

    
       
            
            }, 
            methods: {
                searchApplicant(government_id){
                    console.log('government_id '+government_id.id)
                    // this.form.insuranceentity=this.insuranceentity

                    axios.get('/admin/insurance-policies/'+government_id.id+'/find-applicant').then(
                        (response) => {
                           // console.log('findApplicant '+response.data)
                            if(response.data.cod==0){
                                this.$notify({ group: 'auth', 
                                type:'warn',
                                text: 'El numero de cedula ingresado no se ha podido encontrar!!' })
                            }else{
                                console.log(response.data) 
                                //this.form.last_names=response.data.data.last_names
                                
                                //console.log('construccion name'+response.data.data[0].financial.name) 
                                this.form.construction_entity_id=response.data.data[0].construction_entity
                                this.form.construction_entity_name=response.data.data[0].construction.name
                                this.form.financial_entity_name=response.data.data[0].financial.name
                                this.form.applicant_id=response.data.data[0].id
                               // this.polizalists=response.data.data.insurance_policies
                            }
                        });
                },
                searchSeguro(seguro){
                        //console.log('seguro:'+seguro.id);
                        this.form.insurance_entity_id=seguro.id
                },
                executeLoader(selectID){
                    console.log(selectID.id);
                    //let concepto = [];
                    if(selectID.id==1){
                        this.form.importe='';
                        this.form.importe='28.000.000';
                        this.form.concepto=selectID.text;
                    }else{
                        this.form.importe='';
                        this.form.importe='3.500.000';
                        //this.conceptoid=selectID.text;
                        this.form.concepto=selectID.text;
                    }
                   // this.form.concepto = concepto;
                    

                },
                updateConcepto(users) {
                    console.log(users);
                    let approvers = [];
    
                    users.forEach((user) => {
                        approvers.push(user.text);
                    });
    
                    this.concepto = approvers;
                },
                onSuccess(data) {
                    console.log(data);
                    if(data.notify) {
                        this.$notify({ type: data.notify.type, title: data.notify.title, text: data.notify.message});
                       
                        $('#btn-guardar').css({ 'pointer-events': 'all' });
                        $('#btn-guardar').find('i').removeClass().addClass('fa fa-download');
                        $('#btn-guardar').removeAttr("disabled");
        
        
                    } else if (data.redirect) {
                        window.location.replace(data.redirect);
                    }
                },
                onUpdated(data) {
                    if(data.notify) {
                        this.$notify({ type: data.notify.type, title: data.notify.title, text: data.notify.message});
                       
                        $('#btn-guardar').css({ 'pointer-events': 'all' });
                        $('#btn-guardar').find('i').removeClass().addClass('fa fa-download');
                        $('#btn-guardar').removeAttr("disabled");
        
        
                    } else if (data.redirect) {
                        window.location.replace(data.redirect);
                    }
                },
            },


  

});

import moment from 'moment';
Vue.filter('formatDate', function(value) {
  if (value) {
      return moment(String(value)).format('DD/MM/YYYY hh:mm')
  }
});