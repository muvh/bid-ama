import AppListing from '../app-components/Listing/AppListing';

Vue.component('type-movement-listing', {
    mixins: [AppListing]
});