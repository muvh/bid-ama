import AppListing from '../app-components/Listing/AppListing';

Vue.component('feedback-listing', {
    mixins: [AppListing],
    
    methods: {
        getWidth(index){
            console.log(index)
            if(index === false) {
                return "background-color:#F79992;font-weight:bold";
              }else{
                return "background-color:#88FFB3;font-weight:bold"
              }
        },
    },
});