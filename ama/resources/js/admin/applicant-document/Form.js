import AppForm from '../app-components/Form/AppForm';

Vue.component('applicant-document-form', {
    mixins: [AppForm],
    props: ['documents', 'applicantid'],
    data: function() {
        return {
            form: {
                applicant_id:  '' ,
                document:  '' ,
                required: '',
                received_at: '',
            },
            datePickerConfig: {
                dateFormat: 'Y-m-d',
                altFormat: 'd/m/Y',
            },
            mediaCollections: ['supporting-documents']
        }
    },
    mounted: function () {
        this.form.applicant_id = this.applicantid;
        if(!!this.form.received_at){
            if(this.form.received_at.length>0){
                var date = (this.form.received_at).split(' ');
                date = moment(date[0], "YYYY-MM-DD").format('YYYY-MM-DD')
                this.form.received_at = date;
            }
            
        }
    },
    methods: {
    
       
    }
        
});
