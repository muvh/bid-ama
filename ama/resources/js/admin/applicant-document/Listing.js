import AppListing from '../app-components/Listing/AppListing';

Vue.component('applicant-document-listing', {
    mixins: [AppListing],
    props: ['url'],
    data: function() {
        return {
            listdoc:'',
            doc:''
    
    }
    },
    methods: {
        deleteDocument: function deleteDocument(urldelete) {
            var _this10 = this;

            this.deleteItemDocument(urldelete);
            //console.log(urldelete);
            /*const article = { title: "Vue POST Request Example" };
            axios.post(this.url, article)
                .then(response => this.articleId = response.data.id)
                .catch(error => {
                this.errorMessage = error.message;
                console.error("There was an error!", error);
                });*/
            //.then(response => this.articleId = response.data.id);
            //location.reload();
        },
        deleteItemDocument: function deleteItem(url) {
            var _this7 = this;

            this.$modal.show('dialog', {
                title: 'Atención!',
                text: 'Realmente quieres eliminar este elemento?',
                buttons: [{ title: 'No, cancelar.' }, {
                    title: '<span class="btn-dialog btn-danger">Si, borrar.<span>',
                    handler: function handler() {
                        _this7.$modal.hide('dialog');
                        axios.delete('delete/'+url).then(function (response) {
                            //console.log(url);
                            _this7.loadData();
                            _this7.$notify({ type: 'success', title: 'Éxito!', text: response.data.message ? response.data.message : 'Elemento eliminado con éxito.' });
                        }, function (error) {
                            _this7.$notify({ type: 'error', title: 'Error!', text: error.response.data.message ? error.response.data.message : 'Un error ha ocurrido.' });
                        });
                    }
                }]
            });
        },
        showlisting:function(modelid){
            //console.log('Hola mundo'+modelid)
             axios.get('http://127.0.0.1:8000/admin/applicants/'+modelid+'/showlisting').then(
                (response) => {
                        console.log(response.data)
                        this.listdoc=response.data;
                        this.doc=response.data;


                    if(response.data.error===false){
                        if(parseInt(response.data.message.cod)===1){
                            
                            this.showModal();
                        }
                    }
                }); 
        }

    }
});
