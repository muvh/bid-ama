import AppForm from '../app-components/Form/AppForm';

Vue.component('applicant-form', {
    mixins: [AppForm],
    props: ['cities', 'statusname', 'educationlevels', 'diseases', 'disabilities', 'contactmethods', 'applicantrelationships',
            'saveddiseases', 'saveddisabilities', 'savedcontactmethods', 'finddataurl','showrelationships'
    ],
    data: function() {
        return {
            form: {
                names:  '',
                last_names:  '',
                birthdate:  '',
                padron:'',
                registration_date:  '',
                gender:  '',
                nationality: '',
                government_id:  '',
                marital_status:  '',
                pregnant:  false,
                pregnancy_due_date:  null,
                parent_applicant:  null,
                cadaster:  '',
                property_id:  '',
                occupation:  '',
                ruc:  '',
                monthly_income:  '',
                address: '',
                neighborhood: '',

                familiarsetting:  '' ,
                city:  '' ,
                educationlevel:  '',
                disease: '',
                disability: '',
                contactmethod: '',
                applicantrelationship: '',

                diseasecomponent: {
                    id: '',
                },
                disabilitycomponent: {
                    id: '',
                },
                contactcomponent: {
                    id: '',
                },

                datePickerConfig: {
                    dateFormat: 'Y-m-d',    
                    altInput: false, 
                    noCalendar: true,
                    altFormat: 'd-m-Y',
                    locale: null,
                },
                diseasecomponents: [],
                disabilitycomponents: [],
                contactcomponents: [],

            },
           
            pregnantOption: [
                { text: 'Si', value: true },
                { text: 'No', value: false },
            ],

            datePickerConfig: {
                dateFormat: 'Y-m-d',   
                altFormat: 'd-m-Y',
             
            },
            datePickerConfig2: {
                dateFormat: 'Y-m-d',   
                altFormat: 'd-m-Y',
             
            },
        }   
    },
    
    created: function () {

        if(typeof this.saveddiseases !== 'undefined') {
            this.$set(this.form, 'diseasecomponents', this.saveddiseases)
        }

        if(typeof this.saveddisabilities !== 'undefined') {
            this.$set(this.form, 'disabilitycomponents', this.saveddisabilities)
        }

        if(typeof this.savedcontactmethods !== 'undefined') {
            this.$set(this.form, 'contactcomponents', this.savedcontactmethods)
        } 

        if(typeof this.form.diseasecomponents == 'undefined' || this.form.diseasecomponents.length === 0 ){
            this.$set(this.form, 'diseasecomponents', [{ id:''}])
        }
        if(typeof this.form.disabilitycomponents == 'undefined'  || this.form.disabilitycomponents.length === 0 ){
            this.$set(this.form, 'disabilitycomponents', [{ id:''}])
        }
        if(typeof this.form.contactcomponents == 'undefined' || this.form.contactcomponents.length === 0 ){
            this.$set(this.form, 'contactcomponents', [{ id:''}])
        }
        
    },
    methods: {

        addNewDisease: function () {
            this.form.diseasecomponents.push(Vue.util.extend({}, this.form.diseasecomponent));
        },

        deleteDisease: function (index) {
            Vue.delete(this.form.diseasecomponents, index);
        },
        addNewDisability: function () {
            this.form.disabilitycomponents.push(Vue.util.extend({}, this.form.disabilitycomponent))
        },
        deleteDisability: function (index) {
            Vue.delete(this.form.disabilitycomponents, index);
        },
        addNewContact: function () {
            this.form.contactcomponents.push(Vue.util.extend({}, this.form.contactcomponent))
        },
        deleteContact: function (index) {
            Vue.delete(this.form.contactcomponents, index);
        },
        showModal () {
            this.$modal.show('dialog', {
                title: '<span style="color:red;font-weight: bold;">Atención!</span>',
                group: 'auth',
                text: '<span style="color:red;font-weight: bold;">¡Verifique el Numero de Cedula! <br/> ¡Ya se encuentra cargado algún proyecto en el MUVH! </span>'
                +'<br/> Si el postulante no recibió subsidio puede seguir cargando su información',
                buttons: [
                    { title: 'Ok' }
                ]
            });
        },
        showModalPostulante () {
         this.$modal.show('dialog', {
                title: 'Atención!',
                text: 'Este solicitante ya se encuentra en la base de datos del proyecto AMA!',
                buttons: [
                    { title: 'Ok' }
                ]
            });
        },
       
        findData: function () {

         
            $('#verificando').show()
            //busca si existe en la BD de postgres
            axios.get(this.finddataurl+'/'+this.form.government_id+'/findapplicant').then(
                (response) => {
                    //console.log(response.data);
                    if(response.data.error===false){
                       
                            if(parseInt(response.data.cod)===1){
                                $('#verificando').hide();
                                this.showModalPostulante();
                                this.form.government_id = '';
                                
                            }
                        }else{

                            axios.get(this.finddataurl+'/'+this.form.government_id+'/hadbenefit').then(
                                (response) => {
                                    
                                    if(response.data.error===false){
                                        if(parseInt(response.data.message.cod)===1){
                                            $('#verificando').hide();
                                            this.showModal();
                                        }
                                    }
                                });
                          
                    //busca en identificaciones 
                    axios.get(this.finddataurl+'/'+this.form.government_id+'/identificaciones').then(
                        (response) => {
                            //console.log(response.data);
                            $('#verificando').hide();
                            if(response.data.error===false){
                                var data = response.data.message;
                                this.form.names = data.nombres;
                                this.form.last_names = data.apellido;
                                this.form.gender = data.sexo;
                                this.form.marital_status = data.estadoCivil.trim()  ;
                                this.form.nationality = data.nacionalidadBean;
                                var date = new Date(data.fechNacim);
                                this.form.birthdate = date.toISOString().split('T')[0];
                            }else{
                                this.$notify({ type: 'error', title: 'Error buscando datos', text: response.data.message});
                            }
                        })
                         $('#verificando').hide();
                    

                       
                    }
                });
         
             

                   
                
            },

            findDataIdentifi: function () {

                $('#ver').show();
                this.form.pregnant =false;
                              
                        //busca en identificaciones 
                        axios.get(this.finddataurl+'/'+this.form.government_id+'/identificaciones').then(
                        
                            (response) => {
                                //console.log(response.data);
                                if(response.data.error===false){
                                    var data = response.data.message;
                                    this.form.names = data.nombres;
                                    this.form.last_names = data.apellido;
                                    this.form.gender = data.sexo;
                                   
                                    this.form.marital_status = data.estadoCivil.trim()  ;
                                    this.form.nationality = data.nacionalidadBean;
                                    var date = new Date(data.fechNacim);
                                    this.form.birthdate = date.toISOString().split('T')[0];
                                }else{
                                    $('#ver').hide();
                                    this.$notify({ type: 'error', title: 'Error buscando datos', text: response.data.message});
                                }
                            });
                            $('#ver').hide();
                        },
                        onSuccess(data) {
                            if(data.notify) {
                                this.$notify({ type: data.notify.type, title: data.notify.title, text: data.notify.message});
                               
                                $('#btn-guardar').css({ 'pointer-events': 'all' });
                                $('#btn-guardar').find('i').removeClass().addClass('fa fa-download');
                                $('#btn-guardar').removeAttr("disabled");
                
                
                            } else if (data.redirect) {
                                window.location.replace(data.redirect);
                            }
                        },
                 
            onCancel() {
            console.log('User cancelled the loader.')
        },
        

    }
});

Vue.component('applicant-question-form', {
    mixins: [AppForm],
    props: ['questions', 'answers', 'applicant'],
    data: function() {
        return {
            form: {
                question: {
                    id: '',
                    text: '',
                    type: '',
                    template: '',
                    value : '',
                    comment : false,
                    comment_value : '',
                    values : [],
                    received_at : false,
                },
                questions:  [] ,
                booleans: [
                    { text: '--', value: '--' },
                    { text: 'Si', value: '1' },
                    { text: 'No', value: '0' },
                ],
            },
            datePickerConfig: {
                dateFormat: 'Y-m-d',
                altFormat: 'd/m/Y',
            },
            
        }
    },
    mounted: function () {

        const questions = this.questions[0].questionnaire_template_questions;

        questions.forEach(function (question){

            var answerValue = this.verifyAnswer(question.id,question.question_type);
            var commentValue = this.verifyComment(question.id);
            var dateValue = this.verifyDate(question.id);

            var values = [];
            if(question.values){
                values = JSON.parse(JSON.stringify(('--,'+question.values).split(',').map(s => s.trim())));
            }

            this.form.questions.push({
                    id: question.id,
                    text: question.question,
                    type: question.question_type,
                    template: question.questionnaire_template_id,
                    applicant: this.applicant,
                    value : answerValue,
                    comment : question.comment,
                    comment_text : commentValue,
                    comment_placeholder : question.comment_placeholder,
                    extended_placeholder : question.extended_placeholder,
                    values : values,
                    received_at : question.received_at,
                    received_at_value : dateValue,
            });

        }.bind(this));

    },
    methods: {
        onSuccess(data) {
            if(data.notify) {
                this.$notify({ type: data.notify.type, title: data.notify.title, text: data.notify.message});

                $('#questionButton').css({ 'pointer-events': 'all' });
                $('#questionButton').find('i').removeClass().addClass('fa fa-download');
                $('#questionButton').removeAttr("disabled");

                $('#finalButton').css({ 'pointer-events': 'all' });
                $('#finalButton').find('i').removeClass().addClass('fa fa-download');
                $('#finalButton').removeAttr("disabled");

            } else if (data.redirect) {
                window.location.replace(data.redirect);
            }
        },
        verifyAnswer: function (questionid,type) {

            var newArray = this.answers.filter(function (answer) {
                return answer.question_id === questionid
            });

            if (newArray.length > 0 && !!(newArray[0].answer)) {
                return newArray[0].answer;
            }

            if(type!=='T'){
                return "--";
            }else{
                return null;
            }

        },
        verifyComment: function (questionid) {

            var newArray = this.answers.filter(function (answer) {
                return answer.question_id === questionid;
            });

            if(newArray.length>0){
                return newArray[0].extended_value;
            }

            return '';

        },
        verifyDate: function (questionid) {

            var newArray = this.answers.filter(function (answer) {
                return answer.question_id === questionid;
            });

            if(newArray.length>0){
                if(!!newArray[0].received_at){
                    var date = (newArray[0].received_at).split(' ');
                    date = moment(date[0], "YYYY-MM-DD").format('YYYY-MM-DD')
                    return date;
                }
            }

            return '';
        }

    }

});

Vue.component('form-status', {
    mixins: [AppForm],
    
    data: function() {
        return {
            form: {
             
            },
            
        }
    },
    mounted: function () {
     
    },
    methods: {
        onSuccess(data) {
            if(data.notify) {
                this.$notify({ type: data.notify.type, title: data.notify.title, text: data.notify.message});
               
                $('#btn-status').css({ 'pointer-events': 'all' });
                $('#btn-status').find('i').removeClass().addClass('fa fa-download');
                $('#btn-status').removeAttr("disabled");


            } else if (data.redirect) {
                window.location.replace(data.redirect);
            }
        }
    }
    
});

Vue.component('confirm-ama-form', {
    mixins: [AppForm],
    props: ['atcs'],
    data: function() {
        return {
            form: {
                atc : '',
                file_number: '',
                resolution_number: '',
            },
        }
    },
    mounted: function () {
 
    },
    methods: {
        onSuccess(data) {
            console.log(data);
            if(data.notify) {
                this.$notify({ type: data.notify.type, title: data.notify.title, text: data.notify.message});
                //window.location.replace(data.redirect);
                $('#btn-ama').css({ 'pointer-events': 'all' });
                $('#btn-ama').find('i').removeClass().addClass('fa fa-download');
                $('#btn-ama').removeAttr("disabled"); 
                window.location.replace(data.notify.redirect);

            } else if (data.redirect) {
                window.location.replace(data.redirect);
            }
        }
    }
});
Vue.component('assig-ama-form', {
    mixins: [AppForm],
    props: ['atcs'],
    data: function() {
        return {
            form: {
           
            },
        }
    },
    mounted: function () {
 
    },
    methods: {
        onSuccess(data) {
           
            if(data.notify) {
                this.$notify({ type: data.notify.type, title: data.notify.title, text: data.notify.message});
               
                $('#btn-ama').css({ 'pointer-events': 'all' });
                $('#btn-ama').find('i').removeClass().addClass('fa fa-download');
                $('#btn-ama').removeAttr("disabled");


            } else if (data.redirect) {
                window.location.replace(data.redirect);
            }
        }
    }
});
Vue.component('decline-ama-form', {
    mixins: [AppForm],
    props: ['atcs'],
    data: function() {
        return {
            form: {
           
            },
        }
    },
    mounted: function () {
 
    },
    methods: {
        onSuccess(data) {
           
            if(data.notify) {
                this.$notify({ type: data.notify.type, title: data.notify.title, text: data.notify.message});
               
                $('#btn-ama').css({ 'pointer-events': 'all' });
                $('#btn-ama').find('i').removeClass().addClass('fa fa-download');
                $('#btn-ama').removeAttr("disabled");


            } else if (data.redirect) {
                window.location.replace(data.redirect);
            }
        }
    }
});

Vue.component('confirm-atc-form', {
    mixins: [AppForm],
    data: function() {
        return {
            form: {
            },
        }
    },
    mounted: function () {
    },
    methods: {
        onSuccess(data) {
            if(data.notify) {
                this.$notify({ type: data.notify.type, title: data.notify.title, text: data.notify.message});
               
                $('#btn-atc').css({ 'pointer-events': 'all' });
                $('#btn-atc').find('i').removeClass().addClass('fa fa-download');
                $('#btn-atc').removeAttr("disabled");


            } else if (data.redirect) {
                window.location.replace(data.redirect);
            }
        }
    }
});
Vue.component('assig-coop-form', {
    mixins: [AppForm],
    props: ['coops'],
    data: function() {
        return {
            form: {
            },
        }
    },
    mounted: function () {
    },
    methods: {
        onSuccess(data) {
            if(data.notify) {
                this.$notify({ type: data.notify.type, title: data.notify.title, text: data.notify.message});
               
                $('#btn-atc').css({ 'pointer-events': 'all' });
                $('#btn-atc').find('i').removeClass().addClass('fa fa-download');
                $('#btn-atc').removeAttr("disabled");


            } else if (data.redirect) {
                window.location.replace(data.redirect);
            }
        }
    }
});

Vue.component('assig-atc-form', {
    mixins: [AppForm],
    
    data: function() {
        return {
            form: {
                fec_atc:new Date().toISOString().slice(0,10),
                datePickerConfig: {
                dateFormat: 'Y-m-d',   
                altFormat: 'd-m-Y',
             
            }
            }
        }
    },
    mounted: function () {
    },
    methods: {
        onSuccess(data) {
            if(data.notify) {
                this.$notify({ type: data.notify.type, title: data.notify.title, text: data.notify.message});
               
                $('#btn-atc').css({ 'pointer-events': 'all' });
                $('#btn-atc').find('i').removeClass().addClass('fa fa-download');
                $('#btn-atc').removeAttr("disabled");


            } else if (data.redirect) {
                window.location.replace(data.redirect);
            }
        }
    }
});

import { Bar, Doughnut, mixins  } from 'vue-chartjs'

Vue.component('line-chart-coop', {
    props:[  'afpendingcount'],
    extends: Bar, Doughnut ,
    options: {
    
        responsive: true,
        maintainAspectRatio: false
      },
    mounted () {
      // Overwriting base render method with actual data.
   
      this.renderChart({
          //labels:[this.labels],
          labels: ['Postulantes'],
          datasets: [
              {
                  label:'Resumen',
                  backgroundColor: '#008FFB',    
           
            data: [this.afpendingcount]
            //data: [40, 20, 12, 39, 10, 40, 39, 80, 40, 20, 12, 11],
      
          }
        ] 
      }, this.options)
    }
  });
Vue.component('line-chart', {
    props:[ 'applicant', 'financial', 'construction'],
    extends: Bar, Doughnut ,
    options: {
    
        responsive: true,
        maintainAspectRatio: false
      },
    mounted () {
      // Overwriting base render method with actual data.
   
      this.renderChart({
          //labels:[this.labels],
          labels: ['Postulantes', 'Cooperativas', 'ATC'],
          datasets: [
              {
                  label:'Resumen',
                  backgroundColor: '#008FFB',    
           
            data: [this.applicant, this.financial, this.construction]
            //data: [40, 20, 12, 39, 10, 40, 39, 80, 40, 20, 12, 11],
      
          }
        ] 
      }, this.options)
    }
  });

Vue.component('line-chart-atc', {
    props:[ 'applicant', 'financial', 'construction', 'aecount', 'aependingcount'],
    extends: Bar, Doughnut ,
    mixins: [mixins],
    options: {

        responsive: true ,         
        maintainAspectRatio: true
      },
    mounted () {
      // Overwriting base render method with actual data.
        //alert(this.aependingcount)
      this.renderChart({
          //labels:[this.labels],
          labels: [ 'Total del ATC', 'Pendientes'],
          datasets: [
              {
                  label:'Resumen para ATC',
                  backgroundColor: '#008FFB',    
           
            data: [ this.aecount, this.aependingcount]
            //data: [40, 20, 12, 39, 10, 40, 39, 80, 40, 20, 12, 11],
          }
        ] 
      }, this.options)
    }
  });

  import moment from 'moment';
  Vue.filter('formatDate', function(value) {
    if (value) {
        return moment(String(value)).format('DD/MM/YYYY hh:mm')
    }
});
 

Vue.component('work-form-modal', {
    mixins: [AppForm],
    props:['typechecks'],
    data: function() {
        return {
            form: {
                fecha:  '' ,
                typechecks:  '' ,
                work_id:  '' ,
                type_check_id:  '' ,
                datePickerConfig: {
                 
            },
             dateFormat: 'Y-m-d',    
            //altInput: false, 
            //noCalendar: true,
            altFormat: 'd-m-Y'
        } 
        }
    },  methods: {
        applicantsLabel({ names, last_names, government_id }) {
          return `${government_id} - ${names} ${last_names==null?'':last_names}`;
        },
        typeLabel({ id, name}) {
          return `${id} - ${name} `;
        },
        applicantsLoader(selectID){
          //console.log(selectID.id)
          this.form.applicant_id=selectID.id
        },
        supervisorsLoader(selectID){
          //console.log(selectID.id)
          this.form.supervisor_id=selectID.id
        },
        executeLoader(selectID){
          this.form.type_check_id=selectID.id

        }, onSuccess(data) {
           if(data.notify) {
              this.$notify({ type: data.notify.type, title: data.notify.title, text: data.notify.message});
             
              $('#btn-guardar').css({ 'pointer-events': 'all' });
              $('#btn-guardar').find('i').removeClass().addClass('fa fa-download');
              $('#btn-guardar').removeAttr("disabled");


          } else if (data.redirect) {
              window.location.replace(data.redirect);
          }
      }
      },

});


  