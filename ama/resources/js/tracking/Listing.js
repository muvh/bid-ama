import AppListing from '../admin/app-components/Listing/AppListing';
import AppForm from "../admin/app-components/Form/AppForm";

Vue.component('tracking-listing', {
    mixins: [AppListing],
    data() {
        return {
            extra: null
        }
    },
    methods: {
        loadData: function loadData(resetCurrentPage) {
            var _this6 = this;

            var options = {
                params: {
                    per_page: this.pagination.state.per_page,
                    page: this.pagination.state.current_page,
                    orderBy: this.orderBy.column,
                    orderDirection: this.orderBy.direction
                }
            };

            if (resetCurrentPage === true) {
                options.params.page = 1;
            }

            Object.assign(options.params, this.filters);

            axios.get(this.url, options).then(function (response) {
                _this6.extra = response.data.applicant;
                return _this6.populateCurrentStateAndData(response.data.data);
            }, function (error) {
                // TODO handle error
            });
        },
    }
});

Vue.component('tracking-form', {
    mixins: [AppForm],
    data: function() {
        return {
            form: {
                names:  '' ,
                last_names:  '' ,
                birthdate:  '' ,
                gender:  '' ,
                nationality: '',
                government_id:  '' ,
                marital_status:  '' ,
            },
            datePickerConfig: {

                altFormat: 'd.m.Y',
                altInput: false,
                noCalendar: true,
            },

        }
    },
    mounted: function () {
        this.form.birthdate = moment(this.form.birthdate.split(' ')[0], 'YYYY-MM-DD').format('DD/MM/YYYY')
    }
});
