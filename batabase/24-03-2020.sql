--
-- PostgreSQL database dump
--

-- Dumped from database version 11.6
-- Dumped by pg_dump version 11.6

-- Started on 2020-09-24 09:19:35

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 222 (class 1259 OID 122935)
-- Name: activations; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.activations (
    email character varying(255) NOT NULL,
    token character varying(255) NOT NULL,
    used boolean DEFAULT false NOT NULL,
    created_at timestamp(0) without time zone
);


ALTER TABLE public.activations OWNER TO postgres;

--
-- TOC entry 223 (class 1259 OID 122942)
-- Name: admin_activations; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.admin_activations (
    email character varying(255) NOT NULL,
    token character varying(255) NOT NULL,
    used boolean DEFAULT false NOT NULL,
    created_at timestamp(0) without time zone
);


ALTER TABLE public.admin_activations OWNER TO postgres;

--
-- TOC entry 224 (class 1259 OID 122949)
-- Name: admin_password_resets; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.admin_password_resets (
    email character varying(255) NOT NULL,
    token character varying(255) NOT NULL,
    created_at timestamp(0) without time zone
);


ALTER TABLE public.admin_password_resets OWNER TO postgres;

--
-- TOC entry 225 (class 1259 OID 122955)
-- Name: admin_users; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.admin_users (
    id integer NOT NULL,
    first_name character varying(255),
    last_name character varying(255),
    email character varying(255) NOT NULL,
    password character varying(255) NOT NULL,
    remember_token character varying(100),
    activated boolean DEFAULT false NOT NULL,
    forbidden boolean DEFAULT false NOT NULL,
    language character varying(2) DEFAULT 'en'::character varying NOT NULL,
    deleted_at timestamp(0) without time zone,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    construction_entity integer,
    financial_entity integer,
    phone character varying(20)
);


ALTER TABLE public.admin_users OWNER TO postgres;

--
-- TOC entry 196 (class 1259 OID 122883)
-- Name: admin_users_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.admin_users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 2147483647
    CACHE 1;


ALTER TABLE public.admin_users_id_seq OWNER TO postgres;

--
-- TOC entry 3245 (class 0 OID 0)
-- Dependencies: 196
-- Name: admin_users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.admin_users_id_seq OWNED BY public.admin_users.id;


--
-- TOC entry 226 (class 1259 OID 122965)
-- Name: applicant_answers; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.applicant_answers (
    answer character varying(255),
    extended_value character varying(255),
    created_at timestamp(6) without time zone,
    updated_at timestamp(6) without time zone,
    question_id smallint,
    received_at timestamp(6) without time zone,
    questionnaire_template_id smallint,
    applicant_id bigint
);


ALTER TABLE public.applicant_answers OWNER TO postgres;

--
-- TOC entry 227 (class 1259 OID 122971)
-- Name: applicant_contact_methods; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.applicant_contact_methods (
    applicant_id bigint NOT NULL,
    contact_method_id smallint NOT NULL,
    description character varying(255),
    created_at timestamp(6) without time zone,
    updated_at timestamp(6) without time zone
);


ALTER TABLE public.applicant_contact_methods OWNER TO postgres;

--
-- TOC entry 228 (class 1259 OID 122974)
-- Name: applicant_disabilities; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.applicant_disabilities (
    applicant_id bigint NOT NULL,
    disability_id smallint NOT NULL,
    created_at timestamp(6) without time zone,
    updated_at timestamp(6) without time zone
);


ALTER TABLE public.applicant_disabilities OWNER TO postgres;

--
-- TOC entry 229 (class 1259 OID 122977)
-- Name: applicant_diseases; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.applicant_diseases (
    applicant_id bigint NOT NULL,
    disease_id smallint NOT NULL,
    created_at timestamp(6) without time zone,
    updated_at timestamp(6) without time zone
);


ALTER TABLE public.applicant_diseases OWNER TO postgres;

--
-- TOC entry 230 (class 1259 OID 122980)
-- Name: applicant_documents; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.applicant_documents (
    id integer NOT NULL,
    applicant_id bigint,
    document_id smallint,
    created_at timestamp(6) without time zone,
    updated_at timestamp(6) without time zone,
    received_at timestamp(6) without time zone
);


ALTER TABLE public.applicant_documents OWNER TO postgres;

--
-- TOC entry 197 (class 1259 OID 122885)
-- Name: applicant_documents_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.applicant_documents_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 2147483647
    CACHE 1;


ALTER TABLE public.applicant_documents_id_seq OWNER TO postgres;

--
-- TOC entry 3246 (class 0 OID 0)
-- Dependencies: 197
-- Name: applicant_documents_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.applicant_documents_id_seq OWNED BY public.applicant_documents.id;


--
-- TOC entry 231 (class 1259 OID 122984)
-- Name: applicant_relationships; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.applicant_relationships (
    id smallint NOT NULL,
    name character varying(255),
    created_at timestamp(6) without time zone,
    updated_at timestamp(6) without time zone,
    compute_income boolean DEFAULT false
);


ALTER TABLE public.applicant_relationships OWNER TO postgres;

--
-- TOC entry 198 (class 1259 OID 122887)
-- Name: applicant_relationships_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.applicant_relationships_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 32767
    CACHE 1;


ALTER TABLE public.applicant_relationships_id_seq OWNER TO postgres;

--
-- TOC entry 3247 (class 0 OID 0)
-- Dependencies: 198
-- Name: applicant_relationships_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.applicant_relationships_id_seq OWNED BY public.applicant_relationships.id;


--
-- TOC entry 232 (class 1259 OID 122989)
-- Name: applicant_statuses; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.applicant_statuses (
    id smallint NOT NULL,
    applicant_id bigint,
    status_id smallint,
    user_id integer,
    created_at timestamp(6) without time zone,
    updated_at timestamp(6) without time zone,
    description character varying(255)
);


ALTER TABLE public.applicant_statuses OWNER TO postgres;

--
-- TOC entry 199 (class 1259 OID 122889)
-- Name: applicant_statuses_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.applicant_statuses_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 32767
    CACHE 1;


ALTER TABLE public.applicant_statuses_id_seq OWNER TO postgres;

--
-- TOC entry 3248 (class 0 OID 0)
-- Dependencies: 199
-- Name: applicant_statuses_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.applicant_statuses_id_seq OWNED BY public.applicant_statuses.id;


--
-- TOC entry 233 (class 1259 OID 122993)
-- Name: applicants; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.applicants (
    id bigint NOT NULL,
    names character varying(255),
    last_names character varying(255),
    birthdate timestamp(6) without time zone,
    gender character(1),
    state_id smallint,
    city_id bigint,
    education_level smallint,
    government_id character varying(20),
    marital_status character(5),
    pregnant boolean,
    pregnancy_due_date smallint,
    parent_applicant bigint,
    applicant_relationship smallint,
    cadaster character varying(255),
    property_id character varying(255),
    occupation character varying(255),
    monthly_income numeric(10,0),
    nationality character varying(255),
    address character varying(255),
    neighborhood character varying(255),
    created_at timestamp(6) without time zone,
    updated_at timestamp(6) without time zone,
    construction_entity integer,
    financial_entity integer,
    resolution_number character varying(20),
    file_number character varying(20),
    financial_entity_eval boolean DEFAULT false,
    construction_entity_eval boolean DEFAULT false,
    receiving_file_date date
);


ALTER TABLE public.applicants OWNER TO postgres;

--
-- TOC entry 200 (class 1259 OID 122891)
-- Name: applicants_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.applicants_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.applicants_id_seq OWNER TO postgres;

--
-- TOC entry 3249 (class 0 OID 0)
-- Dependencies: 200
-- Name: applicants_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.applicants_id_seq OWNED BY public.applicants.id;


--
-- TOC entry 260 (class 1259 OID 147482)
-- Name: assignment_histories_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.assignment_histories_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 99999
    CACHE 1;


ALTER TABLE public.assignment_histories_seq OWNER TO postgres;

--
-- TOC entry 259 (class 1259 OID 147456)
-- Name: assignment_histories; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.assignment_histories (
    entity_id integer NOT NULL,
    applicant_id bigint NOT NULL,
    assignment_date date,
    observations character varying(255),
    created_at timestamp(6) without time zone,
    updated_at timestamp(6) without time zone,
    id smallint DEFAULT nextval('public.assignment_histories_seq'::regclass) NOT NULL,
    user_id bigint NOT NULL
);


ALTER TABLE public.assignment_histories OWNER TO postgres;

--
-- TOC entry 234 (class 1259 OID 123002)
-- Name: cities; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.cities (
    id bigint NOT NULL,
    name character varying(255),
    state_id smallint,
    created_at timestamp(6) without time zone,
    updated_at timestamp(6) without time zone
);


ALTER TABLE public.cities OWNER TO postgres;

--
-- TOC entry 201 (class 1259 OID 122893)
-- Name: cities_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.cities_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cities_id_seq OWNER TO postgres;

--
-- TOC entry 3250 (class 0 OID 0)
-- Dependencies: 201
-- Name: cities_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.cities_id_seq OWNED BY public.cities.id;


--
-- TOC entry 235 (class 1259 OID 123006)
-- Name: construction_entities; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.construction_entities (
    id integer NOT NULL,
    name character varying(255),
    created_at timestamp(6) without time zone,
    updated_at timestamp(6) without time zone
);


ALTER TABLE public.construction_entities OWNER TO postgres;

--
-- TOC entry 202 (class 1259 OID 122895)
-- Name: construction_entities_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.construction_entities_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 2147483647
    CACHE 1;


ALTER TABLE public.construction_entities_id_seq OWNER TO postgres;

--
-- TOC entry 3251 (class 0 OID 0)
-- Dependencies: 202
-- Name: construction_entities_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.construction_entities_id_seq OWNED BY public.construction_entities.id;


--
-- TOC entry 236 (class 1259 OID 123010)
-- Name: contact_methods; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.contact_methods (
    id smallint NOT NULL,
    name character varying(255),
    created_at timestamp(6) without time zone,
    updated_at timestamp(6) without time zone
);


ALTER TABLE public.contact_methods OWNER TO postgres;

--
-- TOC entry 203 (class 1259 OID 122897)
-- Name: contact_methods_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.contact_methods_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 32767
    CACHE 1;


ALTER TABLE public.contact_methods_id_seq OWNER TO postgres;

--
-- TOC entry 3252 (class 0 OID 0)
-- Dependencies: 203
-- Name: contact_methods_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.contact_methods_id_seq OWNED BY public.contact_methods.id;


--
-- TOC entry 237 (class 1259 OID 123014)
-- Name: disabilities; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.disabilities (
    id smallint NOT NULL,
    name character varying(255),
    created_at timestamp(6) without time zone,
    updated_at timestamp(6) without time zone
);


ALTER TABLE public.disabilities OWNER TO postgres;

--
-- TOC entry 204 (class 1259 OID 122899)
-- Name: disabilities_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.disabilities_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 32767
    CACHE 1;


ALTER TABLE public.disabilities_id_seq OWNER TO postgres;

--
-- TOC entry 3253 (class 0 OID 0)
-- Dependencies: 204
-- Name: disabilities_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.disabilities_id_seq OWNED BY public.disabilities.id;


--
-- TOC entry 238 (class 1259 OID 123018)
-- Name: diseases; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.diseases (
    id smallint NOT NULL,
    name character varying(255),
    created_at timestamp(6) without time zone,
    updated_at timestamp(6) without time zone
);


ALTER TABLE public.diseases OWNER TO postgres;

--
-- TOC entry 205 (class 1259 OID 122901)
-- Name: diseases_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.diseases_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 32767
    CACHE 1;


ALTER TABLE public.diseases_id_seq OWNER TO postgres;

--
-- TOC entry 3254 (class 0 OID 0)
-- Dependencies: 205
-- Name: diseases_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.diseases_id_seq OWNED BY public.diseases.id;


--
-- TOC entry 239 (class 1259 OID 123022)
-- Name: document_types; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.document_types (
    id integer NOT NULL,
    name character varying(255),
    enabled boolean DEFAULT true,
    type character(1),
    created_at timestamp(6) without time zone,
    updated_at timestamp(6) without time zone
);


ALTER TABLE public.document_types OWNER TO postgres;

--
-- TOC entry 206 (class 1259 OID 122903)
-- Name: document_types_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.document_types_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 2147483647
    CACHE 1;


ALTER TABLE public.document_types_id_seq OWNER TO postgres;

--
-- TOC entry 3255 (class 0 OID 0)
-- Dependencies: 206
-- Name: document_types_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.document_types_id_seq OWNED BY public.document_types.id;


--
-- TOC entry 240 (class 1259 OID 123027)
-- Name: education_levels; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.education_levels (
    id smallint NOT NULL,
    name character varying(255),
    created_at timestamp(6) without time zone,
    updated_at timestamp(6) without time zone
);


ALTER TABLE public.education_levels OWNER TO postgres;

--
-- TOC entry 207 (class 1259 OID 122905)
-- Name: education_levels_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.education_levels_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 32767
    CACHE 1;


ALTER TABLE public.education_levels_id_seq OWNER TO postgres;

--
-- TOC entry 3256 (class 0 OID 0)
-- Dependencies: 207
-- Name: education_levels_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.education_levels_id_seq OWNED BY public.education_levels.id;


--
-- TOC entry 241 (class 1259 OID 123031)
-- Name: failed_jobs; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.failed_jobs (
    id bigint NOT NULL,
    connection text NOT NULL,
    queue text NOT NULL,
    payload text NOT NULL,
    exception text NOT NULL,
    failed_at timestamp(0) without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL
);


ALTER TABLE public.failed_jobs OWNER TO postgres;

--
-- TOC entry 208 (class 1259 OID 122907)
-- Name: failed_jobs_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.failed_jobs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.failed_jobs_id_seq OWNER TO postgres;

--
-- TOC entry 3257 (class 0 OID 0)
-- Dependencies: 208
-- Name: failed_jobs_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.failed_jobs_id_seq OWNED BY public.failed_jobs.id;


--
-- TOC entry 242 (class 1259 OID 123039)
-- Name: financial_entities; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.financial_entities (
    id integer NOT NULL,
    name character varying(255),
    created_at timestamp(6) without time zone,
    updated_at timestamp(6) without time zone
);


ALTER TABLE public.financial_entities OWNER TO postgres;

--
-- TOC entry 209 (class 1259 OID 122909)
-- Name: financial_entities_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.financial_entities_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 2147483647
    CACHE 1;


ALTER TABLE public.financial_entities_id_seq OWNER TO postgres;

--
-- TOC entry 3258 (class 0 OID 0)
-- Dependencies: 209
-- Name: financial_entities_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.financial_entities_id_seq OWNED BY public.financial_entities.id;


--
-- TOC entry 243 (class 1259 OID 123043)
-- Name: media; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.media (
    id bigint NOT NULL,
    model_type character varying(255) NOT NULL,
    model_id bigint NOT NULL,
    collection_name character varying(255) NOT NULL,
    name character varying(255) NOT NULL,
    file_name character varying(255) NOT NULL,
    mime_type character varying(255),
    disk character varying(255) NOT NULL,
    size bigint NOT NULL,
    manipulations json NOT NULL,
    custom_properties json NOT NULL,
    responsive_images json NOT NULL,
    order_column integer,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.media OWNER TO postgres;

--
-- TOC entry 210 (class 1259 OID 122911)
-- Name: media_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.media_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.media_id_seq OWNER TO postgres;

--
-- TOC entry 3259 (class 0 OID 0)
-- Dependencies: 210
-- Name: media_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.media_id_seq OWNED BY public.media.id;


--
-- TOC entry 244 (class 1259 OID 123050)
-- Name: migrations; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.migrations (
    id integer NOT NULL,
    migration character varying(255) NOT NULL,
    batch integer NOT NULL
);


ALTER TABLE public.migrations OWNER TO postgres;

--
-- TOC entry 211 (class 1259 OID 122913)
-- Name: migrations_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.migrations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 2147483647
    CACHE 1;


ALTER TABLE public.migrations_id_seq OWNER TO postgres;

--
-- TOC entry 3260 (class 0 OID 0)
-- Dependencies: 211
-- Name: migrations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.migrations_id_seq OWNED BY public.migrations.id;


--
-- TOC entry 245 (class 1259 OID 123054)
-- Name: model_has_permissions; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.model_has_permissions (
    permission_id bigint NOT NULL,
    model_type character varying(255) NOT NULL,
    model_id bigint NOT NULL
);


ALTER TABLE public.model_has_permissions OWNER TO postgres;

--
-- TOC entry 246 (class 1259 OID 123057)
-- Name: model_has_roles; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.model_has_roles (
    role_id bigint NOT NULL,
    model_type character varying(255) NOT NULL,
    model_id bigint NOT NULL
);


ALTER TABLE public.model_has_roles OWNER TO postgres;

--
-- TOC entry 247 (class 1259 OID 123060)
-- Name: password_resets; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.password_resets (
    email character varying(255) NOT NULL,
    token character varying(255) NOT NULL,
    created_at timestamp(0) without time zone
);


ALTER TABLE public.password_resets OWNER TO postgres;

--
-- TOC entry 248 (class 1259 OID 123066)
-- Name: permissions; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.permissions (
    id bigint NOT NULL,
    name character varying(255) NOT NULL,
    guard_name character varying(255) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.permissions OWNER TO postgres;

--
-- TOC entry 212 (class 1259 OID 122915)
-- Name: permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.permissions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.permissions_id_seq OWNER TO postgres;

--
-- TOC entry 3261 (class 0 OID 0)
-- Dependencies: 212
-- Name: permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.permissions_id_seq OWNED BY public.permissions.id;


--
-- TOC entry 249 (class 1259 OID 123073)
-- Name: questionnaire_template_questions; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.questionnaire_template_questions (
    id smallint NOT NULL,
    questionnaire_template_id smallint,
    question character varying(255),
    question_type character(1),
    "values" character varying(255),
    extended_placeholder character varying(255),
    "order" smallint,
    created_at timestamp(6) without time zone,
    updated_at timestamp(6) without time zone,
    comment boolean DEFAULT true,
    comment_placeholder character varying(255),
    received_at boolean DEFAULT false
);


ALTER TABLE public.questionnaire_template_questions OWNER TO postgres;

--
-- TOC entry 213 (class 1259 OID 122917)
-- Name: questionnaire_template_questions_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.questionnaire_template_questions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 32767
    CACHE 1;


ALTER TABLE public.questionnaire_template_questions_id_seq OWNER TO postgres;

--
-- TOC entry 3262 (class 0 OID 0)
-- Dependencies: 213
-- Name: questionnaire_template_questions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.questionnaire_template_questions_id_seq OWNED BY public.questionnaire_template_questions.id;


--
-- TOC entry 250 (class 1259 OID 123082)
-- Name: questionnaire_templates; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.questionnaire_templates (
    id smallint NOT NULL,
    name character varying(255),
    enabled boolean DEFAULT true,
    questionnaire_type_id smallint,
    created_at timestamp(6) without time zone,
    updated_at timestamp(6) without time zone
);


ALTER TABLE public.questionnaire_templates OWNER TO postgres;

--
-- TOC entry 214 (class 1259 OID 122919)
-- Name: questionnaire_templates_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.questionnaire_templates_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 32767
    CACHE 1;


ALTER TABLE public.questionnaire_templates_id_seq OWNER TO postgres;

--
-- TOC entry 3263 (class 0 OID 0)
-- Dependencies: 214
-- Name: questionnaire_templates_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.questionnaire_templates_id_seq OWNED BY public.questionnaire_templates.id;


--
-- TOC entry 251 (class 1259 OID 123087)
-- Name: questionnaire_types; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.questionnaire_types (
    id smallint NOT NULL,
    name character varying(255),
    created_at timestamp(6) without time zone,
    updated_at timestamp(6) without time zone
);


ALTER TABLE public.questionnaire_types OWNER TO postgres;

--
-- TOC entry 215 (class 1259 OID 122921)
-- Name: questionnaire_types_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.questionnaire_types_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 32767
    CACHE 1;


ALTER TABLE public.questionnaire_types_id_seq OWNER TO postgres;

--
-- TOC entry 3264 (class 0 OID 0)
-- Dependencies: 215
-- Name: questionnaire_types_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.questionnaire_types_id_seq OWNED BY public.questionnaire_types.id;


--
-- TOC entry 252 (class 1259 OID 123091)
-- Name: role_has_permissions; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.role_has_permissions (
    permission_id bigint NOT NULL,
    role_id bigint NOT NULL
);


ALTER TABLE public.role_has_permissions OWNER TO postgres;

--
-- TOC entry 253 (class 1259 OID 123094)
-- Name: roles; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.roles (
    id bigint NOT NULL,
    name character varying(255) NOT NULL,
    guard_name character varying(255) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.roles OWNER TO postgres;

--
-- TOC entry 216 (class 1259 OID 122923)
-- Name: roles_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.roles_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.roles_id_seq OWNER TO postgres;

--
-- TOC entry 3265 (class 0 OID 0)
-- Dependencies: 216
-- Name: roles_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.roles_id_seq OWNED BY public.roles.id;


--
-- TOC entry 254 (class 1259 OID 123101)
-- Name: states; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.states (
    id smallint NOT NULL,
    name character varying(255),
    created_at timestamp(6) without time zone,
    updated_at timestamp(6) without time zone
);


ALTER TABLE public.states OWNER TO postgres;

--
-- TOC entry 217 (class 1259 OID 122925)
-- Name: states_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.states_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 32767
    CACHE 1;


ALTER TABLE public.states_id_seq OWNER TO postgres;

--
-- TOC entry 3266 (class 0 OID 0)
-- Dependencies: 217
-- Name: states_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.states_id_seq OWNED BY public.states.id;


--
-- TOC entry 255 (class 1259 OID 123105)
-- Name: statuses; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.statuses (
    id smallint NOT NULL,
    name character varying(255),
    created_at timestamp(6) without time zone,
    updated_at timestamp(6) without time zone,
    applicant boolean DEFAULT false
);


ALTER TABLE public.statuses OWNER TO postgres;

--
-- TOC entry 218 (class 1259 OID 122927)
-- Name: statuses_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.statuses_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 32767
    CACHE 1;


ALTER TABLE public.statuses_id_seq OWNER TO postgres;

--
-- TOC entry 3267 (class 0 OID 0)
-- Dependencies: 218
-- Name: statuses_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.statuses_id_seq OWNED BY public.statuses.id;


--
-- TOC entry 256 (class 1259 OID 123110)
-- Name: translations; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.translations (
    id integer NOT NULL,
    namespace character varying(255) DEFAULT '*'::character varying NOT NULL,
    "group" character varying(255) NOT NULL,
    key text NOT NULL,
    text jsonb NOT NULL,
    metadata jsonb,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    deleted_at timestamp(0) without time zone
);


ALTER TABLE public.translations OWNER TO postgres;

--
-- TOC entry 219 (class 1259 OID 122929)
-- Name: translations_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.translations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 2147483647
    CACHE 1;


ALTER TABLE public.translations_id_seq OWNER TO postgres;

--
-- TOC entry 3268 (class 0 OID 0)
-- Dependencies: 219
-- Name: translations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.translations_id_seq OWNED BY public.translations.id;


--
-- TOC entry 257 (class 1259 OID 123118)
-- Name: users; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.users (
    id bigint NOT NULL,
    name character varying(255) NOT NULL,
    email character varying(255) NOT NULL,
    email_verified_at timestamp(0) without time zone,
    password character varying(255) NOT NULL,
    remember_token character varying(100),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    construction_entity integer
);


ALTER TABLE public.users OWNER TO postgres;

--
-- TOC entry 220 (class 1259 OID 122931)
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.users_id_seq OWNER TO postgres;

--
-- TOC entry 3269 (class 0 OID 0)
-- Dependencies: 220
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.users_id_seq OWNED BY public.users.id;


--
-- TOC entry 258 (class 1259 OID 123125)
-- Name: wysiwyg_media; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.wysiwyg_media (
    id integer NOT NULL,
    file_path character varying(255) NOT NULL,
    wysiwygable_id integer,
    wysiwygable_type character varying(255),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.wysiwyg_media OWNER TO postgres;

--
-- TOC entry 221 (class 1259 OID 122933)
-- Name: wysiwyg_media_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.wysiwyg_media_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 2147483647
    CACHE 1;


ALTER TABLE public.wysiwyg_media_id_seq OWNER TO postgres;

--
-- TOC entry 3270 (class 0 OID 0)
-- Dependencies: 221
-- Name: wysiwyg_media_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.wysiwyg_media_id_seq OWNED BY public.wysiwyg_media.id;


--
-- TOC entry 2902 (class 2604 OID 122958)
-- Name: admin_users id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.admin_users ALTER COLUMN id SET DEFAULT nextval('public.admin_users_id_seq'::regclass);


--
-- TOC entry 2906 (class 2604 OID 122983)
-- Name: applicant_documents id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.applicant_documents ALTER COLUMN id SET DEFAULT nextval('public.applicant_documents_id_seq'::regclass);


--
-- TOC entry 2907 (class 2604 OID 122987)
-- Name: applicant_relationships id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.applicant_relationships ALTER COLUMN id SET DEFAULT nextval('public.applicant_relationships_id_seq'::regclass);


--
-- TOC entry 2909 (class 2604 OID 122992)
-- Name: applicant_statuses id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.applicant_statuses ALTER COLUMN id SET DEFAULT nextval('public.applicant_statuses_id_seq'::regclass);


--
-- TOC entry 2910 (class 2604 OID 122996)
-- Name: applicants id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.applicants ALTER COLUMN id SET DEFAULT nextval('public.applicants_id_seq'::regclass);


--
-- TOC entry 2913 (class 2604 OID 123005)
-- Name: cities id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cities ALTER COLUMN id SET DEFAULT nextval('public.cities_id_seq'::regclass);


--
-- TOC entry 2914 (class 2604 OID 123009)
-- Name: construction_entities id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.construction_entities ALTER COLUMN id SET DEFAULT nextval('public.construction_entities_id_seq'::regclass);


--
-- TOC entry 2915 (class 2604 OID 123013)
-- Name: contact_methods id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.contact_methods ALTER COLUMN id SET DEFAULT nextval('public.contact_methods_id_seq'::regclass);


--
-- TOC entry 2916 (class 2604 OID 123017)
-- Name: disabilities id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.disabilities ALTER COLUMN id SET DEFAULT nextval('public.disabilities_id_seq'::regclass);


--
-- TOC entry 2917 (class 2604 OID 123021)
-- Name: diseases id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.diseases ALTER COLUMN id SET DEFAULT nextval('public.diseases_id_seq'::regclass);


--
-- TOC entry 2918 (class 2604 OID 123025)
-- Name: document_types id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.document_types ALTER COLUMN id SET DEFAULT nextval('public.document_types_id_seq'::regclass);


--
-- TOC entry 2920 (class 2604 OID 123030)
-- Name: education_levels id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.education_levels ALTER COLUMN id SET DEFAULT nextval('public.education_levels_id_seq'::regclass);


--
-- TOC entry 2921 (class 2604 OID 123034)
-- Name: failed_jobs id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.failed_jobs ALTER COLUMN id SET DEFAULT nextval('public.failed_jobs_id_seq'::regclass);


--
-- TOC entry 2923 (class 2604 OID 123042)
-- Name: financial_entities id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.financial_entities ALTER COLUMN id SET DEFAULT nextval('public.financial_entities_id_seq'::regclass);


--
-- TOC entry 2924 (class 2604 OID 123046)
-- Name: media id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.media ALTER COLUMN id SET DEFAULT nextval('public.media_id_seq'::regclass);


--
-- TOC entry 2925 (class 2604 OID 123053)
-- Name: migrations id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.migrations ALTER COLUMN id SET DEFAULT nextval('public.migrations_id_seq'::regclass);


--
-- TOC entry 2926 (class 2604 OID 123069)
-- Name: permissions id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.permissions ALTER COLUMN id SET DEFAULT nextval('public.permissions_id_seq'::regclass);


--
-- TOC entry 2927 (class 2604 OID 123076)
-- Name: questionnaire_template_questions id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.questionnaire_template_questions ALTER COLUMN id SET DEFAULT nextval('public.questionnaire_template_questions_id_seq'::regclass);


--
-- TOC entry 2930 (class 2604 OID 123085)
-- Name: questionnaire_templates id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.questionnaire_templates ALTER COLUMN id SET DEFAULT nextval('public.questionnaire_templates_id_seq'::regclass);


--
-- TOC entry 2932 (class 2604 OID 123090)
-- Name: questionnaire_types id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.questionnaire_types ALTER COLUMN id SET DEFAULT nextval('public.questionnaire_types_id_seq'::regclass);


--
-- TOC entry 2933 (class 2604 OID 123097)
-- Name: roles id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.roles ALTER COLUMN id SET DEFAULT nextval('public.roles_id_seq'::regclass);


--
-- TOC entry 2934 (class 2604 OID 123104)
-- Name: states id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.states ALTER COLUMN id SET DEFAULT nextval('public.states_id_seq'::regclass);


--
-- TOC entry 2935 (class 2604 OID 123108)
-- Name: statuses id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.statuses ALTER COLUMN id SET DEFAULT nextval('public.statuses_id_seq'::regclass);


--
-- TOC entry 2937 (class 2604 OID 123113)
-- Name: translations id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.translations ALTER COLUMN id SET DEFAULT nextval('public.translations_id_seq'::regclass);


--
-- TOC entry 2939 (class 2604 OID 123121)
-- Name: users id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users ALTER COLUMN id SET DEFAULT nextval('public.users_id_seq'::regclass);


--
-- TOC entry 2940 (class 2604 OID 123128)
-- Name: wysiwyg_media id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wysiwyg_media ALTER COLUMN id SET DEFAULT nextval('public.wysiwyg_media_id_seq'::regclass);


--
-- TOC entry 3201 (class 0 OID 122935)
-- Dependencies: 222
-- Data for Name: activations; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 3202 (class 0 OID 122942)
-- Dependencies: 223
-- Data for Name: admin_activations; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 3203 (class 0 OID 122949)
-- Dependencies: 224
-- Data for Name: admin_password_resets; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 3204 (class 0 OID 122955)
-- Dependencies: 225
-- Data for Name: admin_users; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.admin_users (id, first_name, last_name, email, password, remember_token, activated, forbidden, language, deleted_at, created_at, updated_at, construction_entity, financial_entity, phone) VALUES (1, 'Javier', 'Mendoza', 'jmendoza@muvh.gov.py', '$2y$10$MmyS5lax0GhZhWdnEv5Im.g3BpZJDQfQZr1h0LtGNSnCNK2ujYw7y', 'bpamwTXM066e5T6is8efJQUmXgAuHWr0JyvyLVZpm9KCObcQJ3hNv3SPOJjf', true, false, 'es', NULL, '2020-05-03 19:15:28', '2020-05-03 19:18:42', NULL, NULL, NULL);
INSERT INTO public.admin_users (id, first_name, last_name, email, password, remember_token, activated, forbidden, language, deleted_at, created_at, updated_at, construction_entity, financial_entity, phone) VALUES (5, 'Javi', NULL, 'jmendozaf@gmail.com', '$2y$10$dKh3XQV2GIEfmZCv9QqW0eGiDnA2uUbLi7qZuPAimrIXhEULttQIy', 'pINccHJlXz8Fbetwlzngy4PcLcbdP6xprMTIj8SZPuNpG8aN6FDbl3pV9yBd', false, false, 'en', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.admin_users (id, first_name, last_name, email, password, remember_token, activated, forbidden, language, deleted_at, created_at, updated_at, construction_entity, financial_entity, phone) VALUES (2, 'Cooperativas', ' ', 'coop@muvh.gov.py', '$2y$10$dKh3XQV2GIEfmZCv9QqW0eGiDnA2uUbLi7qZuPAimrIXhEULttQIy', 'kLYffJkX9FRNM6zHPVTtNxNHn3v3eGUtJWLJlfE4KWYsxa9MThIOUsyYOp7H', true, false, 'es', NULL, NULL, NULL, NULL, 2, NULL);
INSERT INTO public.admin_users (id, first_name, last_name, email, password, remember_token, activated, forbidden, language, deleted_at, created_at, updated_at, construction_entity, financial_entity, phone) VALUES (3, 'AMA', NULL, 'ama@muvh.gov.py', '$2y$10$dKh3XQV2GIEfmZCv9QqW0eGiDnA2uUbLi7qZuPAimrIXhEULttQIy', 'lzM5VPcRebjwJgVd5pRy97IhOxGgEd0LPHZ1aid12QBh3QJzrmNJLdcjMzWK', true, false, 'es', NULL, NULL, '2020-05-26 07:51:58', NULL, NULL, '0971331331');
INSERT INTO public.admin_users (id, first_name, last_name, email, password, remember_token, activated, forbidden, language, deleted_at, created_at, updated_at, construction_entity, financial_entity, phone) VALUES (4, 'ATC', ' ', 'atc@muvh.gov.py', '$2y$10$dKh3XQV2GIEfmZCv9QqW0eGiDnA2uUbLi7qZuPAimrIXhEULttQIy', 'rYbqv4SDGOOvgbKBHeqFgEgRXxebdVKzXDyXu1sLHDpBuWKrr4HWE3mSSs48', true, false, 'es', NULL, NULL, NULL, 2, NULL, NULL);


--
-- TOC entry 3205 (class 0 OID 122965)
-- Dependencies: 226
-- Data for Name: applicant_answers; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.applicant_answers (answer, extended_value, created_at, updated_at, question_id, received_at, questionnaire_template_id, applicant_id) VALUES ('--', NULL, '2020-07-02 07:33:29', '2020-07-02 07:33:29', 7, NULL, 2, 47);
INSERT INTO public.applicant_answers (answer, extended_value, created_at, updated_at, question_id, received_at, questionnaire_template_id, applicant_id) VALUES ('--', NULL, '2020-07-02 07:33:29', '2020-07-02 07:33:29', 8, NULL, 2, 47);
INSERT INTO public.applicant_answers (answer, extended_value, created_at, updated_at, question_id, received_at, questionnaire_template_id, applicant_id) VALUES ('--', NULL, '2020-07-02 07:33:29', '2020-07-02 07:33:29', 9, NULL, 2, 47);
INSERT INTO public.applicant_answers (answer, extended_value, created_at, updated_at, question_id, received_at, questionnaire_template_id, applicant_id) VALUES ('1', 'final', '2020-07-02 07:33:29', '2020-07-02 07:33:29', 15, '2020-07-03 00:00:00', 2, 47);
INSERT INTO public.applicant_answers (answer, extended_value, created_at, updated_at, question_id, received_at, questionnaire_template_id, applicant_id) VALUES ('--', NULL, '2020-07-02 11:54:33', '2020-07-02 11:54:33', 1, NULL, 1, 47);
INSERT INTO public.applicant_answers (answer, extended_value, created_at, updated_at, question_id, received_at, questionnaire_template_id, applicant_id) VALUES ('0', 'sdasdasda', '2020-07-02 11:54:33', '2020-07-02 11:54:33', 2, NULL, 1, 47);
INSERT INTO public.applicant_answers (answer, extended_value, created_at, updated_at, question_id, received_at, questionnaire_template_id, applicant_id) VALUES ('--', NULL, '2020-07-02 11:54:33', '2020-07-02 11:54:33', 3, NULL, 1, 47);
INSERT INTO public.applicant_answers (answer, extended_value, created_at, updated_at, question_id, received_at, questionnaire_template_id, applicant_id) VALUES ('--', NULL, '2020-07-02 11:54:33', '2020-07-02 11:54:33', 4, NULL, 1, 47);
INSERT INTO public.applicant_answers (answer, extended_value, created_at, updated_at, question_id, received_at, questionnaire_template_id, applicant_id) VALUES ('--', NULL, '2020-07-02 11:54:33', '2020-07-02 11:54:33', 5, NULL, 1, 47);
INSERT INTO public.applicant_answers (answer, extended_value, created_at, updated_at, question_id, received_at, questionnaire_template_id, applicant_id) VALUES ('--', NULL, '2020-07-02 11:54:33', '2020-07-02 11:54:33', 6, NULL, 1, 47);
INSERT INTO public.applicant_answers (answer, extended_value, created_at, updated_at, question_id, received_at, questionnaire_template_id, applicant_id) VALUES ('1', NULL, '2020-07-02 11:54:33', '2020-07-02 11:54:33', 13, NULL, 1, 47);
INSERT INTO public.applicant_answers (answer, extended_value, created_at, updated_at, question_id, received_at, questionnaire_template_id, applicant_id) VALUES ('1', NULL, '2020-07-02 11:54:33', '2020-07-02 11:54:33', 14, NULL, 1, 47);
INSERT INTO public.applicant_answers (answer, extended_value, created_at, updated_at, question_id, received_at, questionnaire_template_id, applicant_id) VALUES ('0', 'asdasdasd', '2020-07-02 11:54:33', '2020-07-02 11:54:33', 16, NULL, 1, 47);
INSERT INTO public.applicant_answers (answer, extended_value, created_at, updated_at, question_id, received_at, questionnaire_template_id, applicant_id) VALUES ('1', NULL, '2020-07-02 14:43:20', '2020-07-02 14:43:20', 1, NULL, 1, 53);
INSERT INTO public.applicant_answers (answer, extended_value, created_at, updated_at, question_id, received_at, questionnaire_template_id, applicant_id) VALUES ('1', NULL, '2020-07-02 14:43:20', '2020-07-02 14:43:20', 2, NULL, 1, 53);
INSERT INTO public.applicant_answers (answer, extended_value, created_at, updated_at, question_id, received_at, questionnaire_template_id, applicant_id) VALUES ('0', NULL, '2020-07-02 14:43:20', '2020-07-02 14:43:20', 3, NULL, 1, 53);
INSERT INTO public.applicant_answers (answer, extended_value, created_at, updated_at, question_id, received_at, questionnaire_template_id, applicant_id) VALUES ('1', NULL, '2020-07-02 14:43:20', '2020-07-02 14:43:20', 4, NULL, 1, 53);
INSERT INTO public.applicant_answers (answer, extended_value, created_at, updated_at, question_id, received_at, questionnaire_template_id, applicant_id) VALUES ('1', NULL, '2020-07-02 14:43:20', '2020-07-02 14:43:20', 5, NULL, 1, 53);
INSERT INTO public.applicant_answers (answer, extended_value, created_at, updated_at, question_id, received_at, questionnaire_template_id, applicant_id) VALUES ('0', NULL, '2020-07-02 14:43:20', '2020-07-02 14:43:20', 6, NULL, 1, 53);
INSERT INTO public.applicant_answers (answer, extended_value, created_at, updated_at, question_id, received_at, questionnaire_template_id, applicant_id) VALUES ('1', NULL, '2020-07-02 14:43:20', '2020-07-02 14:43:20', 13, NULL, 1, 53);
INSERT INTO public.applicant_answers (answer, extended_value, created_at, updated_at, question_id, received_at, questionnaire_template_id, applicant_id) VALUES ('1', NULL, '2020-07-02 14:43:20', '2020-07-02 14:43:20', 14, NULL, 1, 53);
INSERT INTO public.applicant_answers (answer, extended_value, created_at, updated_at, question_id, received_at, questionnaire_template_id, applicant_id) VALUES ('--', NULL, '2020-07-02 14:43:20', '2020-07-02 14:43:20', 16, NULL, 1, 53);
INSERT INTO public.applicant_answers (answer, extended_value, created_at, updated_at, question_id, received_at, questionnaire_template_id, applicant_id) VALUES ('1', NULL, '2020-07-02 14:55:02', '2020-07-02 14:55:02', 7, NULL, 2, 53);
INSERT INTO public.applicant_answers (answer, extended_value, created_at, updated_at, question_id, received_at, questionnaire_template_id, applicant_id) VALUES ('1', NULL, '2020-07-02 14:55:02', '2020-07-02 14:55:02', 8, NULL, 2, 53);
INSERT INTO public.applicant_answers (answer, extended_value, created_at, updated_at, question_id, received_at, questionnaire_template_id, applicant_id) VALUES ('1', NULL, '2020-07-02 14:55:02', '2020-07-02 14:55:02', 9, NULL, 2, 53);
INSERT INTO public.applicant_answers (answer, extended_value, created_at, updated_at, question_id, received_at, questionnaire_template_id, applicant_id) VALUES ('1', NULL, '2020-07-02 14:55:02', '2020-07-02 14:55:02', 15, NULL, 2, 53);
INSERT INTO public.applicant_answers (answer, extended_value, created_at, updated_at, question_id, received_at, questionnaire_template_id, applicant_id) VALUES ('1', 'fsdfsd', '2020-07-06 09:21:04', '2020-07-06 09:21:04', 1, NULL, 1, 56);
INSERT INTO public.applicant_answers (answer, extended_value, created_at, updated_at, question_id, received_at, questionnaire_template_id, applicant_id) VALUES ('1', 'sdfsdf', '2020-07-06 09:21:04', '2020-07-06 09:21:04', 2, NULL, 1, 56);
INSERT INTO public.applicant_answers (answer, extended_value, created_at, updated_at, question_id, received_at, questionnaire_template_id, applicant_id) VALUES ('1', 'sdfsf', '2020-07-06 09:21:04', '2020-07-06 09:21:04', 3, NULL, 1, 56);
INSERT INTO public.applicant_answers (answer, extended_value, created_at, updated_at, question_id, received_at, questionnaire_template_id, applicant_id) VALUES ('1', 'sdfsd', '2020-07-06 09:21:04', '2020-07-06 09:21:04', 4, NULL, 1, 56);
INSERT INTO public.applicant_answers (answer, extended_value, created_at, updated_at, question_id, received_at, questionnaire_template_id, applicant_id) VALUES ('1', 'sdfsf', '2020-07-06 09:21:04', '2020-07-06 09:21:04', 5, NULL, 1, 56);
INSERT INTO public.applicant_answers (answer, extended_value, created_at, updated_at, question_id, received_at, questionnaire_template_id, applicant_id) VALUES ('1', 'sdfsdf', '2020-07-06 09:21:04', '2020-07-06 09:21:04', 6, NULL, 1, 56);
INSERT INTO public.applicant_answers (answer, extended_value, created_at, updated_at, question_id, received_at, questionnaire_template_id, applicant_id) VALUES ('1', 'sdfsdf', '2020-07-06 09:21:04', '2020-07-06 09:21:04', 13, NULL, 1, 56);
INSERT INTO public.applicant_answers (answer, extended_value, created_at, updated_at, question_id, received_at, questionnaire_template_id, applicant_id) VALUES ('1', 'dfsdfsdf', '2020-07-06 09:21:04', '2020-07-06 09:21:04', 14, NULL, 1, 56);
INSERT INTO public.applicant_answers (answer, extended_value, created_at, updated_at, question_id, received_at, questionnaire_template_id, applicant_id) VALUES ('1', 'dasdasd', '2020-07-06 09:29:12', '2020-07-06 09:29:12', 7, '2020-07-15 00:00:00', 2, 56);
INSERT INTO public.applicant_answers (answer, extended_value, created_at, updated_at, question_id, received_at, questionnaire_template_id, applicant_id) VALUES ('1', 'asddas', '2020-07-06 09:29:12', '2020-07-06 09:29:12', 8, '2020-07-16 00:00:00', 2, 56);
INSERT INTO public.applicant_answers (answer, extended_value, created_at, updated_at, question_id, received_at, questionnaire_template_id, applicant_id) VALUES ('1', 'asdasd', '2020-07-06 09:29:12', '2020-07-06 09:29:12', 9, '2020-07-17 00:00:00', 2, 56);
INSERT INTO public.applicant_answers (answer, extended_value, created_at, updated_at, question_id, received_at, questionnaire_template_id, applicant_id) VALUES ('1', 'asdasd', '2020-07-06 09:29:12', '2020-07-06 09:29:12', 15, '2020-07-18 00:00:00', 2, 56);
INSERT INTO public.applicant_answers (answer, extended_value, created_at, updated_at, question_id, received_at, questionnaire_template_id, applicant_id) VALUES ('1', 'ok', '2020-08-26 11:23:47', '2020-08-26 11:23:47', 7, NULL, 2, 64);
INSERT INTO public.applicant_answers (answer, extended_value, created_at, updated_at, question_id, received_at, questionnaire_template_id, applicant_id) VALUES ('0', 'ok', '2020-08-26 11:23:47', '2020-08-26 11:23:47', 8, NULL, 2, 64);
INSERT INTO public.applicant_answers (answer, extended_value, created_at, updated_at, question_id, received_at, questionnaire_template_id, applicant_id) VALUES ('1', 'ok', '2020-08-26 11:23:47', '2020-08-26 11:23:47', 9, NULL, 2, 64);
INSERT INTO public.applicant_answers (answer, extended_value, created_at, updated_at, question_id, received_at, questionnaire_template_id, applicant_id) VALUES ('1', 'Aprobada por ama', '2020-08-26 11:23:47', '2020-08-26 11:23:47', 15, '2020-08-26 00:00:00', 2, 64);
INSERT INTO public.applicant_answers (answer, extended_value, created_at, updated_at, question_id, received_at, questionnaire_template_id, applicant_id) VALUES ('1', 'ok', '2020-08-26 12:25:53', '2020-08-26 12:25:53', 1, NULL, 1, 64);
INSERT INTO public.applicant_answers (answer, extended_value, created_at, updated_at, question_id, received_at, questionnaire_template_id, applicant_id) VALUES ('1', NULL, '2020-08-26 12:25:53', '2020-08-26 12:25:53', 2, NULL, 1, 64);
INSERT INTO public.applicant_answers (answer, extended_value, created_at, updated_at, question_id, received_at, questionnaire_template_id, applicant_id) VALUES ('1', NULL, '2020-08-26 12:25:53', '2020-08-26 12:25:53', 3, NULL, 1, 64);
INSERT INTO public.applicant_answers (answer, extended_value, created_at, updated_at, question_id, received_at, questionnaire_template_id, applicant_id) VALUES ('1', NULL, '2020-08-26 12:25:53', '2020-08-26 12:25:53', 4, NULL, 1, 64);
INSERT INTO public.applicant_answers (answer, extended_value, created_at, updated_at, question_id, received_at, questionnaire_template_id, applicant_id) VALUES ('1', NULL, '2020-08-26 12:25:53', '2020-08-26 12:25:53', 5, NULL, 1, 64);
INSERT INTO public.applicant_answers (answer, extended_value, created_at, updated_at, question_id, received_at, questionnaire_template_id, applicant_id) VALUES ('1', NULL, '2020-08-26 12:25:53', '2020-08-26 12:25:53', 6, NULL, 1, 64);
INSERT INTO public.applicant_answers (answer, extended_value, created_at, updated_at, question_id, received_at, questionnaire_template_id, applicant_id) VALUES ('1', NULL, '2020-08-26 12:25:53', '2020-08-26 12:25:53', 13, NULL, 1, 64);
INSERT INTO public.applicant_answers (answer, extended_value, created_at, updated_at, question_id, received_at, questionnaire_template_id, applicant_id) VALUES ('1', NULL, '2020-08-26 12:25:53', '2020-08-26 12:25:53', 14, NULL, 1, 64);


--
-- TOC entry 3206 (class 0 OID 122971)
-- Dependencies: 227
-- Data for Name: applicant_contact_methods; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.applicant_contact_methods (applicant_id, contact_method_id, description, created_at, updated_at) VALUES (51, 3, '0971331331', '2020-06-26 11:25:19', '2020-06-26 11:25:19');
INSERT INTO public.applicant_contact_methods (applicant_id, contact_method_id, description, created_at, updated_at) VALUES (54, 4, 'dhkashdk@gmail,com', '2020-07-02 14:28:58', '2020-07-02 14:28:58');
INSERT INTO public.applicant_contact_methods (applicant_id, contact_method_id, description, created_at, updated_at) VALUES (57, 3, '0981456786', '2020-07-06 09:34:56', '2020-07-06 09:34:56');
INSERT INTO public.applicant_contact_methods (applicant_id, contact_method_id, description, created_at, updated_at) VALUES (56, 1, '21341231', '2020-07-22 14:50:34', '2020-07-22 14:50:34');
INSERT INTO public.applicant_contact_methods (applicant_id, contact_method_id, description, created_at, updated_at) VALUES (55, 4, 'adri@gmail.com', '2020-07-23 11:09:44', '2020-07-23 11:09:44');
INSERT INTO public.applicant_contact_methods (applicant_id, contact_method_id, description, created_at, updated_at) VALUES (55, 3, '981254789', '2020-07-23 11:09:44', '2020-07-23 11:09:44');
INSERT INTO public.applicant_contact_methods (applicant_id, contact_method_id, description, created_at, updated_at) VALUES (53, 3, '93115362', '2020-07-23 11:42:11', '2020-07-23 11:42:11');
INSERT INTO public.applicant_contact_methods (applicant_id, contact_method_id, description, created_at, updated_at) VALUES (53, 4, 'vjensenb@gmail.com', '2020-07-23 11:42:11', '2020-07-23 11:42:11');


--
-- TOC entry 3207 (class 0 OID 122974)
-- Dependencies: 228
-- Data for Name: applicant_disabilities; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.applicant_disabilities (applicant_id, disability_id, created_at, updated_at) VALUES (54, 1, '2020-07-02 14:28:58', '2020-07-02 14:28:58');
INSERT INTO public.applicant_disabilities (applicant_id, disability_id, created_at, updated_at) VALUES (55, 1, '2020-07-23 11:09:44', '2020-07-23 11:09:44');


--
-- TOC entry 3208 (class 0 OID 122977)
-- Dependencies: 229
-- Data for Name: applicant_diseases; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.applicant_diseases (applicant_id, disease_id, created_at, updated_at) VALUES (51, 2, '2020-06-26 11:25:19', '2020-06-26 11:25:19');
INSERT INTO public.applicant_diseases (applicant_id, disease_id, created_at, updated_at) VALUES (51, 3, '2020-06-26 11:25:19', '2020-06-26 11:25:19');


--
-- TOC entry 3209 (class 0 OID 122980)
-- Dependencies: 230
-- Data for Name: applicant_documents; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.applicant_documents (id, applicant_id, document_id, created_at, updated_at, received_at) VALUES (17, 47, 4, '2020-07-01 14:23:57', '2020-07-02 06:54:13', '2020-07-15 00:00:00');
INSERT INTO public.applicant_documents (id, applicant_id, document_id, created_at, updated_at, received_at) VALUES (18, 47, 14, '2020-07-01 14:26:03', '2020-07-02 06:54:26', '2020-07-08 00:00:00');
INSERT INTO public.applicant_documents (id, applicant_id, document_id, created_at, updated_at, received_at) VALUES (19, 47, 9, '2020-07-02 11:20:13', '2020-07-02 11:20:13', '2020-07-09 00:00:00');
INSERT INTO public.applicant_documents (id, applicant_id, document_id, created_at, updated_at, received_at) VALUES (22, 53, 1, '2020-07-02 14:51:44', '2020-07-02 14:51:44', '2020-07-02 00:00:00');
INSERT INTO public.applicant_documents (id, applicant_id, document_id, created_at, updated_at, received_at) VALUES (23, 53, 5, '2020-07-02 14:52:45', '2020-07-02 14:52:45', '2020-07-02 00:00:00');
INSERT INTO public.applicant_documents (id, applicant_id, document_id, created_at, updated_at, received_at) VALUES (25, 56, 3, '2020-07-06 09:23:02', '2020-07-06 09:23:18', '2020-07-07 00:00:00');
INSERT INTO public.applicant_documents (id, applicant_id, document_id, created_at, updated_at, received_at) VALUES (26, 56, 15, '2020-07-06 09:23:43', '2020-07-06 09:23:43', '2020-07-08 00:00:00');
INSERT INTO public.applicant_documents (id, applicant_id, document_id, created_at, updated_at, received_at) VALUES (27, 56, 11, '2020-07-06 09:30:51', '2020-07-06 09:30:51', '2020-07-07 00:00:00');
INSERT INTO public.applicant_documents (id, applicant_id, document_id, created_at, updated_at, received_at) VALUES (29, 61, 2, '2020-07-08 13:51:51', '2020-07-08 13:51:51', '2020-07-08 00:00:00');
INSERT INTO public.applicant_documents (id, applicant_id, document_id, created_at, updated_at, received_at) VALUES (31, 61, 1, '2020-07-10 10:24:51', '2020-07-10 10:24:51', '2020-07-10 00:00:00');
INSERT INTO public.applicant_documents (id, applicant_id, document_id, created_at, updated_at, received_at) VALUES (32, 61, 3, '2020-07-10 10:25:18', '2020-07-10 10:25:18', '2020-07-10 00:00:00');
INSERT INTO public.applicant_documents (id, applicant_id, document_id, created_at, updated_at, received_at) VALUES (33, 61, 5, '2020-07-10 10:25:43', '2020-07-10 10:25:43', '2020-07-10 00:00:00');
INSERT INTO public.applicant_documents (id, applicant_id, document_id, created_at, updated_at, received_at) VALUES (34, 61, 6, '2020-07-10 10:26:21', '2020-07-10 10:26:21', '2020-07-10 00:00:00');
INSERT INTO public.applicant_documents (id, applicant_id, document_id, created_at, updated_at, received_at) VALUES (38, 61, 7, '2020-07-10 11:05:33', '2020-07-10 11:05:33', '2020-07-10 00:00:00');
INSERT INTO public.applicant_documents (id, applicant_id, document_id, created_at, updated_at, received_at) VALUES (39, 53, 13, '2020-07-13 10:42:06', '2020-07-13 10:42:06', '2020-07-13 00:00:00');
INSERT INTO public.applicant_documents (id, applicant_id, document_id, created_at, updated_at, received_at) VALUES (40, 53, 15, '2020-07-23 11:12:35', '2020-07-23 11:12:35', '2020-07-01 00:00:00');
INSERT INTO public.applicant_documents (id, applicant_id, document_id, created_at, updated_at, received_at) VALUES (41, 64, 4, '2020-07-23 11:44:05', '2020-07-23 11:44:05', '2020-07-23 00:00:00');
INSERT INTO public.applicant_documents (id, applicant_id, document_id, created_at, updated_at, received_at) VALUES (42, 64, 6, '2020-07-23 11:44:53', '2020-07-23 11:44:53', '2020-07-23 00:00:00');
INSERT INTO public.applicant_documents (id, applicant_id, document_id, created_at, updated_at, received_at) VALUES (43, 64, 5, '2020-07-23 11:45:33', '2020-07-23 11:45:33', '2020-07-23 00:00:00');
INSERT INTO public.applicant_documents (id, applicant_id, document_id, created_at, updated_at, received_at) VALUES (44, 64, 1, '2020-07-23 11:45:53', '2020-07-23 11:45:53', '2020-07-23 00:00:00');
INSERT INTO public.applicant_documents (id, applicant_id, document_id, created_at, updated_at, received_at) VALUES (45, 64, 7, '2020-07-23 11:46:10', '2020-07-23 11:46:10', '2020-07-23 00:00:00');
INSERT INTO public.applicant_documents (id, applicant_id, document_id, created_at, updated_at, received_at) VALUES (46, 64, 15, '2020-07-23 11:46:53', '2020-07-23 11:46:53', '2020-07-23 00:00:00');
INSERT INTO public.applicant_documents (id, applicant_id, document_id, created_at, updated_at, received_at) VALUES (48, 64, 8, '2020-07-23 11:47:27', '2020-07-23 11:47:27', '2020-07-22 00:00:00');
INSERT INTO public.applicant_documents (id, applicant_id, document_id, created_at, updated_at, received_at) VALUES (49, 64, 13, '2020-07-23 11:47:43', '2020-07-23 11:47:43', '2020-07-23 00:00:00');
INSERT INTO public.applicant_documents (id, applicant_id, document_id, created_at, updated_at, received_at) VALUES (50, 64, 11, '2020-07-23 11:47:58', '2020-07-23 11:47:58', '2020-07-23 00:00:00');
INSERT INTO public.applicant_documents (id, applicant_id, document_id, created_at, updated_at, received_at) VALUES (51, 64, 10, '2020-07-23 11:48:14', '2020-07-23 11:48:14', '2020-07-23 00:00:00');
INSERT INTO public.applicant_documents (id, applicant_id, document_id, created_at, updated_at, received_at) VALUES (54, 64, 9, '2020-07-23 13:35:47', '2020-07-23 13:35:47', '2020-07-23 00:00:00');
INSERT INTO public.applicant_documents (id, applicant_id, document_id, created_at, updated_at, received_at) VALUES (55, 64, 12, '2020-07-23 13:36:05', '2020-07-23 13:36:05', '2020-07-23 00:00:00');
INSERT INTO public.applicant_documents (id, applicant_id, document_id, created_at, updated_at, received_at) VALUES (56, 56, 4, '2020-09-07 09:53:57', '2020-09-07 09:53:57', '2020-09-07 00:00:00');
INSERT INTO public.applicant_documents (id, applicant_id, document_id, created_at, updated_at, received_at) VALUES (57, 56, 1, '2020-09-07 09:54:19', '2020-09-07 09:54:19', '2020-09-07 00:00:00');
INSERT INTO public.applicant_documents (id, applicant_id, document_id, created_at, updated_at, received_at) VALUES (58, 56, 5, '2020-09-07 09:54:35', '2020-09-07 09:54:35', '2020-09-07 00:00:00');
INSERT INTO public.applicant_documents (id, applicant_id, document_id, created_at, updated_at, received_at) VALUES (59, 56, 7, '2020-09-07 09:54:53', '2020-09-07 09:54:53', '2020-09-22 00:00:00');
INSERT INTO public.applicant_documents (id, applicant_id, document_id, created_at, updated_at, received_at) VALUES (60, 56, 6, '2020-09-07 09:55:06', '2020-09-07 09:55:06', '2020-09-07 00:00:00');
INSERT INTO public.applicant_documents (id, applicant_id, document_id, created_at, updated_at, received_at) VALUES (61, 56, 14, '2020-09-07 09:55:35', '2020-09-07 09:55:35', '2020-09-07 00:00:00');
INSERT INTO public.applicant_documents (id, applicant_id, document_id, created_at, updated_at, received_at) VALUES (62, 56, 8, '2020-09-07 09:56:09', '2020-09-07 09:56:09', '2020-09-07 00:00:00');


--
-- TOC entry 3210 (class 0 OID 122984)
-- Dependencies: 231
-- Data for Name: applicant_relationships; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.applicant_relationships (id, name, created_at, updated_at, compute_income) VALUES (1, 'Cónyuge / Pareja', NULL, NULL, true);
INSERT INTO public.applicant_relationships (id, name, created_at, updated_at, compute_income) VALUES (2, 'Hijo / Hija', NULL, NULL, false);
INSERT INTO public.applicant_relationships (id, name, created_at, updated_at, compute_income) VALUES (3, 'Suegro / Suegra', NULL, NULL, false);
INSERT INTO public.applicant_relationships (id, name, created_at, updated_at, compute_income) VALUES (4, 'Sobrino / Sobrina', NULL, NULL, false);
INSERT INTO public.applicant_relationships (id, name, created_at, updated_at, compute_income) VALUES (5, 'Abuelo / Abuela', NULL, NULL, false);
INSERT INTO public.applicant_relationships (id, name, created_at, updated_at, compute_income) VALUES (6, 'Primo / Prima', NULL, NULL, false);
INSERT INTO public.applicant_relationships (id, name, created_at, updated_at, compute_income) VALUES (7, 'Hermano / Hermana', NULL, NULL, false);
INSERT INTO public.applicant_relationships (id, name, created_at, updated_at, compute_income) VALUES (8, 'Tío / Tía', NULL, NULL, false);
INSERT INTO public.applicant_relationships (id, name, created_at, updated_at, compute_income) VALUES (9, 'Hermanastro / Hermanastra', NULL, NULL, false);
INSERT INTO public.applicant_relationships (id, name, created_at, updated_at, compute_income) VALUES (10, 'Padrastro / Madastra', NULL, NULL, false);
INSERT INTO public.applicant_relationships (id, name, created_at, updated_at, compute_income) VALUES (11, 'Hijastro / Hijastra', NULL, NULL, false);
INSERT INTO public.applicant_relationships (id, name, created_at, updated_at, compute_income) VALUES (12, 'Padre / Madre', NULL, NULL, false);
INSERT INTO public.applicant_relationships (id, name, created_at, updated_at, compute_income) VALUES (13, 'Hijo/a Sostén', NULL, NULL, true);


--
-- TOC entry 3211 (class 0 OID 122989)
-- Dependencies: 232
-- Data for Name: applicant_statuses; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.applicant_statuses (id, applicant_id, status_id, user_id, created_at, updated_at, description) VALUES (17, 47, 1, 2, '2020-06-26 05:44:31', '2020-06-26 05:44:31', NULL);
INSERT INTO public.applicant_statuses (id, applicant_id, status_id, user_id, created_at, updated_at, description) VALUES (20, 53, 1, 2, '2020-07-02 14:27:58', '2020-07-02 14:27:58', NULL);
INSERT INTO public.applicant_statuses (id, applicant_id, status_id, user_id, created_at, updated_at, description) VALUES (21, 56, 1, 2, '2020-07-06 09:19:09', '2020-07-06 09:19:09', NULL);
INSERT INTO public.applicant_statuses (id, applicant_id, status_id, user_id, created_at, updated_at, description) VALUES (24, 64, 1, 3, '2020-07-06 14:56:27', '2020-07-06 14:56:27', NULL);
INSERT INTO public.applicant_statuses (id, applicant_id, status_id, user_id, created_at, updated_at, description) VALUES (26, 64, 2, 3, '2020-08-26 12:12:48', '2020-08-26 12:12:48', NULL);
INSERT INTO public.applicant_statuses (id, applicant_id, status_id, user_id, created_at, updated_at, description) VALUES (27, 64, 2, 3, '2020-08-26 12:25:53', '2020-08-26 12:25:53', NULL);


--
-- TOC entry 3212 (class 0 OID 122993)
-- Dependencies: 233
-- Data for Name: applicants; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.applicants (id, names, last_names, birthdate, gender, state_id, city_id, education_level, government_id, marital_status, pregnant, pregnancy_due_date, parent_applicant, applicant_relationship, cadaster, property_id, occupation, monthly_income, nationality, address, neighborhood, created_at, updated_at, construction_entity, financial_entity, resolution_number, file_number, financial_entity_eval, construction_entity_eval, receiving_file_date) VALUES (47, 'Javier', 'Mendoza', '1982-02-03 00:00:00', 'M', NULL, 1, 4, '2038893', 'S    ', false, NULL, NULL, NULL, '232323', '423424', 'Análisis de sistema', 4000000, 'PARAGUAYA', 'Tte Molinas', 'San Vicente', '2020-06-26 05:44:31', '2020-07-02 14:01:34', NULL, 2, '456', '123', true, true, NULL);
INSERT INTO public.applicants (id, names, last_names, birthdate, gender, state_id, city_id, education_level, government_id, marital_status, pregnant, pregnancy_due_date, parent_applicant, applicant_relationship, cadaster, property_id, occupation, monthly_income, nationality, address, neighborhood, created_at, updated_at, construction_entity, financial_entity, resolution_number, file_number, financial_entity_eval, construction_entity_eval, receiving_file_date) VALUES (54, 'WILDO DANIEL', 'GONZALEZ PORTILLO', '1977-12-26 00:00:00', 'M', NULL, NULL, 4, '2188216', 'CA   ', false, NULL, 53, 1, NULL, NULL, 'economista', 2000000, 'PARAGUAYA', NULL, NULL, '2020-07-02 14:28:58', '2020-07-02 14:28:58', NULL, NULL, NULL, NULL, false, false, NULL);
INSERT INTO public.applicants (id, names, last_names, birthdate, gender, state_id, city_id, education_level, government_id, marital_status, pregnant, pregnancy_due_date, parent_applicant, applicant_relationship, cadaster, property_id, occupation, monthly_income, nationality, address, neighborhood, created_at, updated_at, construction_entity, financial_entity, resolution_number, file_number, financial_entity_eval, construction_entity_eval, receiving_file_date) VALUES (57, 'PEDRO ALEJANDRO', 'ACOSTA MELO', '1982-05-10 00:00:00', 'M', NULL, NULL, 4, '3496101', 'SO   ', false, NULL, 56, 1, NULL, NULL, 'informático', 3600000, 'PARAGUAYA', NULL, NULL, '2020-07-06 09:19:56', '2020-07-06 09:34:56', NULL, NULL, NULL, NULL, false, false, NULL);
INSERT INTO public.applicants (id, names, last_names, birthdate, gender, state_id, city_id, education_level, government_id, marital_status, pregnant, pregnancy_due_date, parent_applicant, applicant_relationship, cadaster, property_id, occupation, monthly_income, nationality, address, neighborhood, created_at, updated_at, construction_entity, financial_entity, resolution_number, file_number, financial_entity_eval, construction_entity_eval, receiving_file_date) VALUES (63, 'EDILBERTO EULALIO', 'QUINTANA OVIEDO', '1966-02-12 00:00:00', 'M', NULL, NULL, 3, '2021430', 'SO   ', false, NULL, 61, 2, NULL, NULL, 'aaaaa', 2500000, 'PARAGUAYA', NULL, NULL, '2020-07-10 09:18:46', '2020-07-23 14:00:39', NULL, NULL, NULL, NULL, false, false, NULL);
INSERT INTO public.applicants (id, names, last_names, birthdate, gender, state_id, city_id, education_level, government_id, marital_status, pregnant, pregnancy_due_date, parent_applicant, applicant_relationship, cadaster, property_id, occupation, monthly_income, nationality, address, neighborhood, created_at, updated_at, construction_entity, financial_entity, resolution_number, file_number, financial_entity_eval, construction_entity_eval, receiving_file_date) VALUES (62, 'PEDRO GABRIEL', 'CENTURION GONZALEZ', '1981-09-17 00:00:00', 'M', NULL, NULL, 6, '3462421', 'SO   ', false, NULL, 61, 2, NULL, NULL, 'Cobrador', 0, 'PARAGUAYA', NULL, NULL, '2020-07-09 14:55:58', '2020-07-23 14:01:35', NULL, NULL, NULL, NULL, false, false, NULL);
INSERT INTO public.applicants (id, names, last_names, birthdate, gender, state_id, city_id, education_level, government_id, marital_status, pregnant, pregnancy_due_date, parent_applicant, applicant_relationship, cadaster, property_id, occupation, monthly_income, nationality, address, neighborhood, created_at, updated_at, construction_entity, financial_entity, resolution_number, file_number, financial_entity_eval, construction_entity_eval, receiving_file_date) VALUES (61, 'PEDRO ALEJANDRO', 'ACOSTA MELO', '1982-05-10 00:00:00', 'M', NULL, 1, 4, '3496101', 'SO   ', false, NULL, NULL, NULL, '231', '333', 'aaaaa', 2500000, 'PARAGUAYA', 'Asara 365 c/ Av Eusebio Ayala', 'centro', '2020-07-08 12:14:45', '2020-07-23 14:02:37', NULL, 2, NULL, NULL, false, false, NULL);
INSERT INTO public.applicants (id, names, last_names, birthdate, gender, state_id, city_id, education_level, government_id, marital_status, pregnant, pregnancy_due_date, parent_applicant, applicant_relationship, cadaster, property_id, occupation, monthly_income, nationality, address, neighborhood, created_at, updated_at, construction_entity, financial_entity, resolution_number, file_number, financial_entity_eval, construction_entity_eval, receiving_file_date) VALUES (51, 'MARIA DE LOS MILAGROS', 'ACOSTA LATORRE', '1984-06-05 00:00:00', 'F', NULL, NULL, 4, '1810486', 'SO   ', false, NULL, 47, 1, NULL, NULL, 'Administradora', 3500000, 'PARAGUAYA', NULL, NULL, '2020-06-26 10:59:30', '2020-06-26 11:25:19', NULL, 8, NULL, NULL, false, false, NULL);
INSERT INTO public.applicants (id, names, last_names, birthdate, gender, state_id, city_id, education_level, government_id, marital_status, pregnant, pregnancy_due_date, parent_applicant, applicant_relationship, cadaster, property_id, occupation, monthly_income, nationality, address, neighborhood, created_at, updated_at, construction_entity, financial_entity, resolution_number, file_number, financial_entity_eval, construction_entity_eval, receiving_file_date) VALUES (64, 'FABIO RICARDO', 'CARDOZO BRITEZ', '1985-07-25 00:00:00', 'M', NULL, 2, 4, '3448843', 'ME   ', false, NULL, NULL, NULL, '321868556', '5487', 'informático', 1500000, 'PARAGUAYA', 'rosas 23', 'mburucuja', '2020-07-15 14:00:05', '2020-09-17 10:09:30', 3, 2, '30004', '1234', true, false, '2020-09-07');
INSERT INTO public.applicants (id, names, last_names, birthdate, gender, state_id, city_id, education_level, government_id, marital_status, pregnant, pregnancy_due_date, parent_applicant, applicant_relationship, cadaster, property_id, occupation, monthly_income, nationality, address, neighborhood, created_at, updated_at, construction_entity, financial_entity, resolution_number, file_number, financial_entity_eval, construction_entity_eval, receiving_file_date) VALUES (55, 'ADRIANA LUZ MARINA', 'RODRIGUEZ VERA', '1991-07-27 00:00:00', 'F', NULL, NULL, 3, '5049176', 'SO   ', true, 25, 53, 13, NULL, NULL, 'asistente', 1000000, 'PARAGUAYA', NULL, NULL, '2020-07-02 14:30:17', '2020-07-23 11:09:44', NULL, NULL, NULL, NULL, false, false, NULL);
INSERT INTO public.applicants (id, names, last_names, birthdate, gender, state_id, city_id, education_level, government_id, marital_status, pregnant, pregnancy_due_date, parent_applicant, applicant_relationship, cadaster, property_id, occupation, monthly_income, nationality, address, neighborhood, created_at, updated_at, construction_entity, financial_entity, resolution_number, file_number, financial_entity_eval, construction_entity_eval, receiving_file_date) VALUES (53, 'VALERIA INGRID', 'JENSEN BUHL DE GONZALEZ', '1978-01-16 00:00:00', 'F', NULL, 1, 4, '8355096', 'CA   ', false, NULL, NULL, NULL, '654321', '123456', 'directora', 2000000, 'CHILENA', 'los laureles', 'los laureles', '2020-07-02 14:27:58', '2020-07-23 11:42:11', 2, 2, '1245', '12345', true, false, NULL);
INSERT INTO public.applicants (id, names, last_names, birthdate, gender, state_id, city_id, education_level, government_id, marital_status, pregnant, pregnancy_due_date, parent_applicant, applicant_relationship, cadaster, property_id, occupation, monthly_income, nationality, address, neighborhood, created_at, updated_at, construction_entity, financial_entity, resolution_number, file_number, financial_entity_eval, construction_entity_eval, receiving_file_date) VALUES (56, 'ADRIANA LUZ MARINA', 'RODRIGUEZ VERA', '1991-07-27 00:00:00', 'F', NULL, 1, 4, '5049176', 'DI   ', false, NULL, NULL, NULL, '3123', '123123', 'Asistente', 200000, 'BRASILERA', 'España 830', 'Mburicao', '2020-07-06 09:19:09', '2020-09-07 09:56:30', 2, 2, '2344233', '23424', true, true, '2020-09-07');


--
-- TOC entry 3238 (class 0 OID 147456)
-- Dependencies: 259
-- Data for Name: assignment_histories; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.assignment_histories (entity_id, applicant_id, assignment_date, observations, created_at, updated_at, id, user_id) VALUES (3, 64, '2020-09-17', 'Primera asignacion', '2020-09-17 10:15:41', '2020-09-17 10:15:41', 16, 3);


--
-- TOC entry 3213 (class 0 OID 123002)
-- Dependencies: 234
-- Data for Name: cities; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.cities (id, name, state_id, created_at, updated_at) VALUES (1, 'Asuncion', 1, NULL, NULL);
INSERT INTO public.cities (id, name, state_id, created_at, updated_at) VALUES (2, 'Capiatá', NULL, NULL, NULL);
INSERT INTO public.cities (id, name, state_id, created_at, updated_at) VALUES (3, ' Fernando de la Mora', NULL, NULL, NULL);
INSERT INTO public.cities (id, name, state_id, created_at, updated_at) VALUES (4, 'Luque', NULL, NULL, NULL);
INSERT INTO public.cities (id, name, state_id, created_at, updated_at) VALUES (5, 'Limpio', NULL, NULL, NULL);
INSERT INTO public.cities (id, name, state_id, created_at, updated_at) VALUES (6, 'Lambaré', NULL, NULL, NULL);
INSERT INTO public.cities (id, name, state_id, created_at, updated_at) VALUES (7, 'Mariano Roque Alonso', NULL, NULL, NULL);
INSERT INTO public.cities (id, name, state_id, created_at, updated_at) VALUES (8, 'Ñemby', NULL, NULL, NULL);
INSERT INTO public.cities (id, name, state_id, created_at, updated_at) VALUES (9, 'Villa Elisa', NULL, NULL, NULL);
INSERT INTO public.cities (id, name, state_id, created_at, updated_at) VALUES (10, 'San Antonio', NULL, NULL, NULL);
INSERT INTO public.cities (id, name, state_id, created_at, updated_at) VALUES (11, 'San Lorenzo', NULL, NULL, NULL);


--
-- TOC entry 3214 (class 0 OID 123006)
-- Dependencies: 235
-- Data for Name: construction_entities; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.construction_entities (id, name, created_at, updated_at) VALUES (1, 'Humberto Cañete', NULL, NULL);
INSERT INTO public.construction_entities (id, name, created_at, updated_at) VALUES (2, 'Nuova', NULL, NULL);
INSERT INTO public.construction_entities (id, name, created_at, updated_at) VALUES (3, 'Arq. Lourdes Legal', NULL, NULL);
INSERT INTO public.construction_entities (id, name, created_at, updated_at) VALUES (4, 'Contaconsult', NULL, NULL);
INSERT INTO public.construction_entities (id, name, created_at, updated_at) VALUES (5, 'Tekovera', NULL, NULL);
INSERT INTO public.construction_entities (id, name, created_at, updated_at) VALUES (6, 'Silvio FariñA', NULL, NULL);
INSERT INTO public.construction_entities (id, name, created_at, updated_at) VALUES (7, 'Ferrehogar', NULL, NULL);
INSERT INTO public.construction_entities (id, name, created_at, updated_at) VALUES (8, 'Cardonaro', NULL, NULL);
INSERT INTO public.construction_entities (id, name, created_at, updated_at) VALUES (9, 'Cotep', NULL, NULL);
INSERT INTO public.construction_entities (id, name, created_at, updated_at) VALUES (10, 'Arquitectonica', NULL, NULL);
INSERT INTO public.construction_entities (id, name, created_at, updated_at) VALUES (11, 'Las Cumbres', NULL, NULL);
INSERT INTO public.construction_entities (id, name, created_at, updated_at) VALUES (12, 'Cesarina Barreto', NULL, NULL);
INSERT INTO public.construction_entities (id, name, created_at, updated_at) VALUES (13, 'Ing.Hernan Gimenez', NULL, NULL);
INSERT INTO public.construction_entities (id, name, created_at, updated_at) VALUES (14, 'Ing. Alicia Gimenez', NULL, NULL);
INSERT INTO public.construction_entities (id, name, created_at, updated_at) VALUES (15, 'Fe Grande', NULL, NULL);
INSERT INTO public.construction_entities (id, name, created_at, updated_at) VALUES (16, 'CVI', NULL, NULL);
INSERT INTO public.construction_entities (id, name, created_at, updated_at) VALUES (17, 'Consultora SG', NULL, NULL);
INSERT INTO public.construction_entities (id, name, created_at, updated_at) VALUES (18, 'Trazo', NULL, NULL);
INSERT INTO public.construction_entities (id, name, created_at, updated_at) VALUES (19, 'Ing. Ubaldo M. Britez', NULL, NULL);
INSERT INTO public.construction_entities (id, name, created_at, updated_at) VALUES (20, 'Ing. Civil Jose Dominguez', NULL, NULL);
INSERT INTO public.construction_entities (id, name, created_at, updated_at) VALUES (21, 'Arq. Olga Butlerov', NULL, NULL);
INSERT INTO public.construction_entities (id, name, created_at, updated_at) VALUES (22, 'Ing. Brenda Centurión', NULL, NULL);
INSERT INTO public.construction_entities (id, name, created_at, updated_at) VALUES (23, 'Arq. Marcos Antonio López', NULL, NULL);
INSERT INTO public.construction_entities (id, name, created_at, updated_at) VALUES (24, 'Arq. Ruth L. Irala', NULL, NULL);
INSERT INTO public.construction_entities (id, name, created_at, updated_at) VALUES (25, 'Ing. Angel Miranda', NULL, NULL);
INSERT INTO public.construction_entities (id, name, created_at, updated_at) VALUES (26, 'Arq. Francisco Báez', NULL, NULL);


--
-- TOC entry 3215 (class 0 OID 123010)
-- Dependencies: 236
-- Data for Name: contact_methods; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.contact_methods (id, name, created_at, updated_at) VALUES (1, 'Teléfono particular', NULL, NULL);
INSERT INTO public.contact_methods (id, name, created_at, updated_at) VALUES (2, 'Teléfono laboral', NULL, NULL);
INSERT INTO public.contact_methods (id, name, created_at, updated_at) VALUES (3, 'Celular', NULL, NULL);
INSERT INTO public.contact_methods (id, name, created_at, updated_at) VALUES (4, 'Email', NULL, NULL);


--
-- TOC entry 3216 (class 0 OID 123014)
-- Dependencies: 237
-- Data for Name: disabilities; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.disabilities (id, name, created_at, updated_at) VALUES (2, 'Mental', NULL, NULL);
INSERT INTO public.disabilities (id, name, created_at, updated_at) VALUES (3, 'Sensorial', NULL, NULL);
INSERT INTO public.disabilities (id, name, created_at, updated_at) VALUES (1, 'Motora', NULL, NULL);


--
-- TOC entry 3217 (class 0 OID 123018)
-- Dependencies: 238
-- Data for Name: diseases; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.diseases (id, name, created_at, updated_at) VALUES (1, 'Motora', NULL, NULL);
INSERT INTO public.diseases (id, name, created_at, updated_at) VALUES (2, 'Sensorial', NULL, NULL);
INSERT INTO public.diseases (id, name, created_at, updated_at) VALUES (3, 'Mental', NULL, NULL);


--
-- TOC entry 3218 (class 0 OID 123022)
-- Dependencies: 239
-- Data for Name: document_types; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.document_types (id, name, enabled, type, created_at, updated_at) VALUES (1, 'Fotocopia de Cédula de Identidad (solicitante, cónyuge/pareja)', true, 'S', NULL, NULL);
INSERT INTO public.document_types (id, name, enabled, type, created_at, updated_at) VALUES (2, 'Certificado de nacimiento original o copia autenticada, de los hijos/as menores de edad', true, 'S', NULL, NULL);
INSERT INTO public.document_types (id, name, enabled, type, created_at, updated_at) VALUES (3, 'Documentos que acrediten la tenencia legal del inmueble (a nombre del solicitante)', true, 'S', NULL, NULL);
INSERT INTO public.document_types (id, name, enabled, type, created_at, updated_at) VALUES (4, 'Justificación de ingreso', true, 'S', NULL, NULL);
INSERT INTO public.document_types (id, name, enabled, type, created_at, updated_at) VALUES (5, 'Nota para postulación al Aporte Estatal (Subsidio)', true, 'S', NULL, NULL);
INSERT INTO public.document_types (id, name, enabled, type, created_at, updated_at) VALUES (6, 'Formulario de Inscripción (firmado por el solicitante)', true, 'S', NULL, NULL);
INSERT INTO public.document_types (id, name, enabled, type, created_at, updated_at) VALUES (7, 'Fotos de la vivienda a ser refaccionada o ampliada', true, 'S', NULL, NULL);
INSERT INTO public.document_types (id, name, enabled, type, created_at, updated_at) VALUES (8, 'Presentación del Proyecto
', true, 'T', NULL, NULL);
INSERT INTO public.document_types (id, name, enabled, type, created_at, updated_at) VALUES (9, 'Cómputo métrico y presupuestario
', true, 'T', NULL, NULL);
INSERT INTO public.document_types (id, name, enabled, type, created_at, updated_at) VALUES (13, 'Planos', true, 'T', NULL, NULL);
INSERT INTO public.document_types (id, name, enabled, type, created_at, updated_at) VALUES (14, 'Constancia de ahorro expedido por la IF/CAC o de aporte en especie según el caso', true, 'F', NULL, NULL);
INSERT INTO public.document_types (id, name, enabled, type, created_at, updated_at) VALUES (15, 'Constancia de aprobación microcrédito', true, 'F', NULL, NULL);
INSERT INTO public.document_types (id, name, enabled, type, created_at, updated_at) VALUES (16, 'Copia autenticada del acta de certificado de matrimonio o copia autenticada de libreta de familia', true, 'S', NULL, NULL);
INSERT INTO public.document_types (id, name, enabled, type, created_at, updated_at) VALUES (17, 'Constancia de Unión de Hecho, emitida por el Juzgado de paz o declaración jurada de unión de hecho', true, 'S', NULL, NULL);
INSERT INTO public.document_types (id, name, enabled, type, created_at, updated_at) VALUES (18, 'Sentencia Definitiva (S.D.) de divorcio o separación de bienes o constancia de haber iniciado tramite de divorcio', true, 'S', NULL, NULL);
INSERT INTO public.document_types (id, name, enabled, type, created_at, updated_at) VALUES (19, 'Certificado del Acta de defunción (En caso de no contar con C.I. Actualizada)', true, 'S', NULL, NULL);
INSERT INTO public.document_types (id, name, enabled, type, created_at, updated_at) VALUES (20, 'Fotocopia autenticada de Cédula Paraguaya o radicación permanente', true, 'S', NULL, NULL);
INSERT INTO public.document_types (id, name, enabled, type, created_at, updated_at) VALUES (22, 'Certificado de Discapacidad expedido por SENADIS', true, 'S', NULL, NULL);
INSERT INTO public.document_types (id, name, enabled, type, created_at, updated_at) VALUES (23, 'Certificado Médico, expedido por MSPYBS', true, 'S', NULL, NULL);
INSERT INTO public.document_types (id, name, enabled, type, created_at, updated_at) VALUES (10, 'Planilla Diagnóstico déficit cualitativo', true, 'T', NULL, NULL);
INSERT INTO public.document_types (id, name, enabled, type, created_at, updated_at) VALUES (11, 'Cronograma', true, 'T', NULL, NULL);
INSERT INTO public.document_types (id, name, enabled, type, created_at, updated_at) VALUES (12, 'Resumen Ejecutivo', true, 'T', NULL, NULL);
INSERT INTO public.document_types (id, name, enabled, type, created_at, updated_at) VALUES (21, 'Familiar Sostén- Constancia emitida por el Juzgado de paz o declaracion jurada', true, 'S', NULL, NULL);
INSERT INTO public.document_types (id, name, enabled, type, created_at, updated_at) VALUES (24, 'Embarazo - Certificado del médico tratante', true, 'S', NULL, NULL);


--
-- TOC entry 3219 (class 0 OID 123027)
-- Dependencies: 240
-- Data for Name: education_levels; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.education_levels (id, name, created_at, updated_at) VALUES (1, 'Primaria', NULL, NULL);
INSERT INTO public.education_levels (id, name, created_at, updated_at) VALUES (2, 'Secundaria', NULL, NULL);
INSERT INTO public.education_levels (id, name, created_at, updated_at) VALUES (3, 'Técnica', NULL, NULL);
INSERT INTO public.education_levels (id, name, created_at, updated_at) VALUES (4, 'Universitaria', NULL, NULL);
INSERT INTO public.education_levels (id, name, created_at, updated_at) VALUES (5, 'Especial', NULL, NULL);
INSERT INTO public.education_levels (id, name, created_at, updated_at) VALUES (6, 'Ninguna', NULL, NULL);


--
-- TOC entry 3220 (class 0 OID 123031)
-- Dependencies: 241
-- Data for Name: failed_jobs; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 3221 (class 0 OID 123039)
-- Dependencies: 242
-- Data for Name: financial_entities; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.financial_entities (id, name, created_at, updated_at) VALUES (15, 'Coop. Mult. de Ahorro y Crédito Capiata Ltda.', NULL, NULL);
INSERT INTO public.financial_entities (id, name, created_at, updated_at) VALUES (2, 'Coop. Luque Ltda.', NULL, NULL);
INSERT INTO public.financial_entities (id, name, created_at, updated_at) VALUES (1, 'Coop. de Ahorro Crédito y Consumo 24 de Octubre Ltda.', NULL, NULL);
INSERT INTO public.financial_entities (id, name, created_at, updated_at) VALUES (3, 'Coop. Mult. de Ahorro y Crédito Consumo Producción y Servicios 8 De Marzo Ltda.', NULL, NULL);
INSERT INTO public.financial_entities (id, name, created_at, updated_at) VALUES (4, 'Coop. Mult. de Ahorro y Crédito Consumo Producción y Servicios Ñemby Ltda.', NULL, NULL);
INSERT INTO public.financial_entities (id, name, created_at, updated_at) VALUES (5, 'Coop. Mult. de Ahorro y Crédito Consumo Producción y Servicios Mercado No.4 Ltda.', NULL, NULL);
INSERT INTO public.financial_entities (id, name, created_at, updated_at) VALUES (8, 'Coop. Mult. Nazareth Ltda.', NULL, NULL);
INSERT INTO public.financial_entities (id, name, created_at, updated_at) VALUES (7, 'Coop. de Ahorro y Crédito Producción y Servicios Mburicao
', NULL, NULL);
INSERT INTO public.financial_entities (id, name, created_at, updated_at) VALUES (9, 'Coop. de Ahorro Crédito y Servicios San Lorenzo Ltda.', NULL, NULL);
INSERT INTO public.financial_entities (id, name, created_at, updated_at) VALUES (10, 'Coop. Mult. de Consumo Ahorro y Crédito Yoayu Ltda.', NULL, NULL);
INSERT INTO public.financial_entities (id, name, created_at, updated_at) VALUES (11, 'Coop. Paraguaya de la ConstruccióN Ltda. Copacons
', NULL, NULL);
INSERT INTO public.financial_entities (id, name, created_at, updated_at) VALUES (12, 'Coop. Mult. de Ahorro y Crédito Consumo Producción y Servicios Reducto Ltda.
', NULL, NULL);
INSERT INTO public.financial_entities (id, name, created_at, updated_at) VALUES (13, 'Coop. Mult. de Ahorro Crédito y Servicios del Personal de la Sanidad Militar Coopersam Ltda.', NULL, NULL);
INSERT INTO public.financial_entities (id, name, created_at, updated_at) VALUES (14, 'Coop. de Ahorro Crédito Consumo y Servicios Exa San Jose Ltda.', NULL, NULL);
INSERT INTO public.financial_entities (id, name, created_at, updated_at) VALUES (6, 'Coop. Mult. de Ahorro Crédito y Servicios Medalla Milagrosa Ltda.', NULL, NULL);
INSERT INTO public.financial_entities (id, name, created_at, updated_at) VALUES (16, 'Coop. Mult. de Ahorro y Crédito Sagrados Corazones  Ltda.', NULL, NULL);


--
-- TOC entry 3222 (class 0 OID 123043)
-- Dependencies: 243
-- Data for Name: media; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.media (id, model_type, model_id, collection_name, name, file_name, mime_type, disk, size, manipulations, custom_properties, responsive_images, order_column, created_at, updated_at) VALUES (1, 'Brackets\AdminAuth\Models\AdminUser', 1, 'avatar', 'avatar', 'avatar.png', 'image/png', 'media', 23924, '[]', '{"generated_conversions":{"thumb_200":true,"thumb_75":true,"thumb_150":true}}', '[]', 1, '2020-05-03 19:15:28', '2020-05-03 19:15:28');
INSERT INTO public.media (id, model_type, model_id, collection_name, name, file_name, mime_type, disk, size, manipulations, custom_properties, responsive_images, order_column, created_at, updated_at) VALUES (2, 'App\Models\ApplicantDocument', 5, 'supporting-documents', 'nyglRcnL6AtLWuOQN87noRVBIEgjGAlwD83gHvUd', 'nyglRcnL6AtLWuOQN87noRVBIEgjGAlwD83gHvUd.png', 'image/png', 'supporting-documents', 157457, '[]', '{"name":"postulante.png","file_name":"postulante.png","width":1440,"height":1624,"generated_conversions":{"thumb_200":true,"thumb_150":true}}', '[]', 2, '2020-05-07 04:35:13', '2020-05-07 04:35:14');
INSERT INTO public.media (id, model_type, model_id, collection_name, name, file_name, mime_type, disk, size, manipulations, custom_properties, responsive_images, order_column, created_at, updated_at) VALUES (3, 'App\Models\ApplicantDocument', 9, 'supporting-documents', 'bZ60A0S8FEwhiv7C9QPavSGwS2TZKk5HtN9DZYuw', 'bZ60A0S8FEwhiv7C9QPavSGwS2TZKk5HtN9DZYuw.png', 'image/png', 'supporting-documents', 157457, '[]', '{"name":"postulante.png","file_name":"postulante.png","width":1440,"height":1624,"generated_conversions":{"thumb_200":true,"thumb_150":true}}', '[]', 3, '2020-05-07 10:31:48', '2020-05-07 10:31:48');
INSERT INTO public.media (id, model_type, model_id, collection_name, name, file_name, mime_type, disk, size, manipulations, custom_properties, responsive_images, order_column, created_at, updated_at) VALUES (4, 'App\Models\ApplicantDocument', 6, 'supporting-documents', 'QUPUemiSK8iGOfxagYWS6V9mAtsgI7VBxdo5Z4Sg', 'QUPUemiSK8iGOfxagYWS6V9mAtsgI7VBxdo5Z4Sg.png', 'image/png', 'supporting-documents', 157457, '[]', '{"name":"postulante.png","file_name":"postulante.png","width":1440,"height":1624,"generated_conversions":{"thumb_200":true,"thumb_150":true}}', '[]', 4, '2020-05-07 10:34:25', '2020-05-07 10:34:26');
INSERT INTO public.media (id, model_type, model_id, collection_name, name, file_name, mime_type, disk, size, manipulations, custom_properties, responsive_images, order_column, created_at, updated_at) VALUES (10, 'App\Models\ApplicantDocument', 15, 'supporting-documents', 'SQBDxhYTpPB4Qar3uq0Srm3Zfkz3Jh5aXHIE4MDy', 'SQBDxhYTpPB4Qar3uq0Srm3Zfkz3Jh5aXHIE4MDy.jpeg', 'image/jpeg', 'supporting-documents', 24237, '[]', '{"name":"0000101832.jpg","file_name":"0000101832.jpg","width":611,"height":273,"generated_conversions":{"thumb_200":true,"thumb_150":true}}', '[]', 5, '2020-06-25 11:14:50', '2020-06-25 11:14:51');
INSERT INTO public.media (id, model_type, model_id, collection_name, name, file_name, mime_type, disk, size, manipulations, custom_properties, responsive_images, order_column, created_at, updated_at) VALUES (13, 'App\Models\ApplicantDocument', 17, 'supporting-documents', 'ZUrET6lqDu0WmCecNYto6SX0WrIVJ5ybbNGEmkUr', 'ZUrET6lqDu0WmCecNYto6SX0WrIVJ5ybbNGEmkUr.png', 'image/png', 'supporting-documents', 80123, '[]', '{"name":"logo.png","file_name":"logo.png","width":1024,"height":1024,"generated_conversions":{"thumb_200":true,"thumb_150":true}}', '[]', 6, '2020-07-01 14:23:57', '2020-07-01 14:23:58');
INSERT INTO public.media (id, model_type, model_id, collection_name, name, file_name, mime_type, disk, size, manipulations, custom_properties, responsive_images, order_column, created_at, updated_at) VALUES (14, 'App\Models\ApplicantDocument', 18, 'supporting-documents', 'g6ySxJ99c1YXMCbBbMANMsTpOzTi8VyjuFgG27c0', 'g6ySxJ99c1YXMCbBbMANMsTpOzTi8VyjuFgG27c0.jpeg', 'image/jpeg', 'supporting-documents', 716621, '[]', '{"name":"191018184405-cnndinero-cocacola-101819-dinero-only-full-169.jpg","file_name":"191018184405-cnndinero-cocacola-101819-dinero-only-full-169.jpg","width":1600,"height":900,"generated_conversions":{"thumb_200":true,"thumb_150":true}}', '[]', 7, '2020-07-01 14:26:03', '2020-07-01 14:26:03');
INSERT INTO public.media (id, model_type, model_id, collection_name, name, file_name, mime_type, disk, size, manipulations, custom_properties, responsive_images, order_column, created_at, updated_at) VALUES (15, 'App\Models\ApplicantDocument', 19, 'supporting-documents', 'ImEHG2I8B6vEeSu9ClRLHGSjWQtiiK7MPjYRjEn3', 'ImEHG2I8B6vEeSu9ClRLHGSjWQtiiK7MPjYRjEn3.png', 'image/png', 'supporting-documents', 80123, '[]', '{"name":"logo.png","file_name":"logo.png","width":1024,"height":1024,"generated_conversions":{"thumb_200":true,"thumb_150":true}}', '[]', 8, '2020-07-02 11:20:13', '2020-07-02 11:20:13');
INSERT INTO public.media (id, model_type, model_id, collection_name, name, file_name, mime_type, disk, size, manipulations, custom_properties, responsive_images, order_column, created_at, updated_at) VALUES (20, 'App\Models\ApplicantDocument', 25, 'supporting-documents', '1A0j2n1l1OGUHlNfClSZkJCtWWN7eON4B5nlkgFn', '1A0j2n1l1OGUHlNfClSZkJCtWWN7eON4B5nlkgFn.jpeg', 'image/jpeg', 'supporting-documents', 7208, '[]', '{"name":"download.jpeg","file_name":"download.jpeg","width":259,"height":194,"generated_conversions":{"thumb_200":true,"thumb_150":true}}', '[]', 11, '2020-07-06 09:23:02', '2020-07-06 09:23:03');
INSERT INTO public.media (id, model_type, model_id, collection_name, name, file_name, mime_type, disk, size, manipulations, custom_properties, responsive_images, order_column, created_at, updated_at) VALUES (18, 'App\Models\ApplicantDocument', 22, 'supporting-documents', 'uyMuXDPuGZtsDsMC37k2HO9oYWCOJ3zMMRqweTqk', 'uyMuXDPuGZtsDsMC37k2HO9oYWCOJ3zMMRqweTqk.jpeg', 'image/jpeg', 'supporting-documents', 76373, '[]', '{"name":"9497e6882c5a0aecf46e5399dd6591ca.jpg","file_name":"9497e6882c5a0aecf46e5399dd6591ca.jpg","width":564,"height":752,"generated_conversions":{"thumb_200":true,"thumb_150":true}}', '[]', 9, '2020-07-02 14:51:45', '2020-07-02 14:51:45');
INSERT INTO public.media (id, model_type, model_id, collection_name, name, file_name, mime_type, disk, size, manipulations, custom_properties, responsive_images, order_column, created_at, updated_at) VALUES (21, 'App\Models\ApplicantDocument', 26, 'supporting-documents', 'S23P8HUOnk4E9y1N19Xq5QaojkOO3bdsw8veJda5', 'S23P8HUOnk4E9y1N19Xq5QaojkOO3bdsw8veJda5.jpeg', 'image/jpeg', 'supporting-documents', 7208, '[]', '{"name":"download.jpeg","file_name":"download.jpeg","width":259,"height":194,"generated_conversions":{"thumb_200":true,"thumb_150":true}}', '[]', 12, '2020-07-06 09:23:43', '2020-07-06 09:23:43');
INSERT INTO public.media (id, model_type, model_id, collection_name, name, file_name, mime_type, disk, size, manipulations, custom_properties, responsive_images, order_column, created_at, updated_at) VALUES (19, 'App\Models\ApplicantDocument', 23, 'supporting-documents', 'a4qeXXUQw9VHANQXdiX4PWobqrRwXEahEFAW7Oyy', 'a4qeXXUQw9VHANQXdiX4PWobqrRwXEahEFAW7Oyy.jpeg', 'image/jpeg', 'supporting-documents', 110676, '[]', '{"name":"69205731dbd826b64b2f7eb3dc9c335e.jpg","file_name":"69205731dbd826b64b2f7eb3dc9c335e.jpg","width":564,"height":1111,"generated_conversions":{"thumb_200":true,"thumb_150":true}}', '[]', 10, '2020-07-02 14:52:45', '2020-07-02 14:52:46');
INSERT INTO public.media (id, model_type, model_id, collection_name, name, file_name, mime_type, disk, size, manipulations, custom_properties, responsive_images, order_column, created_at, updated_at) VALUES (22, 'App\Models\ApplicantDocument', 27, 'supporting-documents', 'dCvoqShT4OclXkqm9Ugr6DSSt4CtBe6i8ViAuqDk', 'dCvoqShT4OclXkqm9Ugr6DSSt4CtBe6i8ViAuqDk.jpeg', 'image/jpeg', 'supporting-documents', 7208, '[]', '{"name":"download.jpeg","file_name":"download.jpeg","width":259,"height":194,"generated_conversions":{"thumb_200":true,"thumb_150":true}}', '[]', 13, '2020-07-06 09:30:51', '2020-07-06 09:30:51');
INSERT INTO public.media (id, model_type, model_id, collection_name, name, file_name, mime_type, disk, size, manipulations, custom_properties, responsive_images, order_column, created_at, updated_at) VALUES (24, 'App\Models\ApplicantDocument', 29, 'supporting-documents', 'uO1Qu1ynIMwL5Ti4fPmUJ9AjejVXHrFZx3jsnA0Y', 'uO1Qu1ynIMwL5Ti4fPmUJ9AjejVXHrFZx3jsnA0Y.jpeg', 'image/jpeg', 'supporting-documents', 4996, '[]', '{"name":"images.jpg","file_name":"images.jpg","width":220,"height":229,"generated_conversions":{"thumb_200":true,"thumb_150":true}}', '[]', 14, '2020-07-08 13:51:51', '2020-07-08 13:51:53');
INSERT INTO public.media (id, model_type, model_id, collection_name, name, file_name, mime_type, disk, size, manipulations, custom_properties, responsive_images, order_column, created_at, updated_at) VALUES (26, 'App\Models\ApplicantDocument', 31, 'supporting-documents', 'xyvYX3L3djeAXdZaC0WkY6ktvOHe0DmvCr4AsfJr', 'xyvYX3L3djeAXdZaC0WkY6ktvOHe0DmvCr4AsfJr.jpeg', 'image/jpeg', 'supporting-documents', 4996, '[]', '{"name":"images.jpg","file_name":"images.jpg","width":220,"height":229,"generated_conversions":{"thumb_200":true,"thumb_150":true}}', '[]', 16, '2020-07-10 10:24:52', '2020-07-10 10:24:54');
INSERT INTO public.media (id, model_type, model_id, collection_name, name, file_name, mime_type, disk, size, manipulations, custom_properties, responsive_images, order_column, created_at, updated_at) VALUES (27, 'App\Models\ApplicantDocument', 32, 'supporting-documents', 'ZnzPClsg1TNvhrvlHaBHHFddLhk16rhJnEMRwjBh', 'ZnzPClsg1TNvhrvlHaBHHFddLhk16rhJnEMRwjBh.jpeg', 'image/jpeg', 'supporting-documents', 123714, '[]', '{"name":"20897385-t\u00edo-sam-de-dibujos-animados-que-quiere.jpg","file_name":"20897385-t\u00edo-sam-de-dibujos-animados-que-quiere.jpg","width":964,"height":1300,"generated_conversions":{"thumb_200":true,"thumb_150":true}}', '[]', 17, '2020-07-10 10:25:18', '2020-07-10 10:25:19');
INSERT INTO public.media (id, model_type, model_id, collection_name, name, file_name, mime_type, disk, size, manipulations, custom_properties, responsive_images, order_column, created_at, updated_at) VALUES (28, 'App\Models\ApplicantDocument', 33, 'supporting-documents', 'JZkC28ckCOTMkwqz1Wqer19s4RkSCxllHGIEpiwn', 'JZkC28ckCOTMkwqz1Wqer19s4RkSCxllHGIEpiwn.jpeg', 'image/jpeg', 'supporting-documents', 65214, '[]', '{"name":"cara-hombre-de-dibujos-animados-jb47n6.jpg","file_name":"cara-hombre-de-dibujos-animados-jb47n6.jpg","width":1201,"height":1390,"generated_conversions":{"thumb_200":true,"thumb_150":true}}', '[]', 18, '2020-07-10 10:25:43', '2020-07-10 10:25:44');
INSERT INTO public.media (id, model_type, model_id, collection_name, name, file_name, mime_type, disk, size, manipulations, custom_properties, responsive_images, order_column, created_at, updated_at) VALUES (29, 'App\Models\ApplicantDocument', 34, 'supporting-documents', 'TYiTZgQqZdKHZvBWVh62NEZr3IL9pCTpn4ccmeYZ', 'TYiTZgQqZdKHZvBWVh62NEZr3IL9pCTpn4ccmeYZ.png', 'image/png', 'supporting-documents', 157457, '[]', '{"name":"postulante.png","file_name":"postulante.png","width":1440,"height":1624,"generated_conversions":{"thumb_200":true,"thumb_150":true}}', '[]', 19, '2020-07-10 10:26:21', '2020-07-10 10:26:22');
INSERT INTO public.media (id, model_type, model_id, collection_name, name, file_name, mime_type, disk, size, manipulations, custom_properties, responsive_images, order_column, created_at, updated_at) VALUES (35, 'App\Models\ApplicantDocument', 40, 'supporting-documents', 'EmWdocOq6TkosPID8SOFTpTK5NJnqLWqmnCDiXtB', 'EmWdocOq6TkosPID8SOFTpTK5NJnqLWqmnCDiXtB.jpeg', 'image/jpeg', 'supporting-documents', 65214, '[]', '{"name":"cara-hombre-de-dibujos-animados-jb47n6.jpg","file_name":"cara-hombre-de-dibujos-animados-jb47n6.jpg","width":1201,"height":1390,"generated_conversions":{"thumb_200":true,"thumb_150":true}}', '[]', 23, '2020-07-23 11:12:35', '2020-07-23 11:12:38');
INSERT INTO public.media (id, model_type, model_id, collection_name, name, file_name, mime_type, disk, size, manipulations, custom_properties, responsive_images, order_column, created_at, updated_at) VALUES (40, 'App\Models\ApplicantDocument', 45, 'supporting-documents', 'FVfKYpjfcNcL0qm1KaawF9Py9i6iWdALdD0WTS5U', 'FVfKYpjfcNcL0qm1KaawF9Py9i6iWdALdD0WTS5U.jpeg', 'image/jpeg', 'supporting-documents', 123714, '[]', '{"name":"20897385-t\u00edo-sam-de-dibujos-animados-que-quiere.jpg","file_name":"20897385-t\u00edo-sam-de-dibujos-animados-que-quiere.jpg","width":964,"height":1300,"generated_conversions":{"thumb_200":true,"thumb_150":true}}', '[]', 28, '2020-07-23 11:46:10', '2020-07-23 11:46:11');
INSERT INTO public.media (id, model_type, model_id, collection_name, name, file_name, mime_type, disk, size, manipulations, custom_properties, responsive_images, order_column, created_at, updated_at) VALUES (36, 'App\Models\ApplicantDocument', 41, 'supporting-documents', 's32Za3x4HB1qoZBoOGMKM9ZcljwororSiKMVBVD9', 's32Za3x4HB1qoZBoOGMKM9ZcljwororSiKMVBVD9.jpeg', 'image/jpeg', 'supporting-documents', 4996, '[]', '{"name":"images.jpg","file_name":"images.jpg","width":220,"height":229,"generated_conversions":{"thumb_200":true,"thumb_150":true}}', '[]', 24, '2020-07-23 11:44:05', '2020-07-23 11:44:06');
INSERT INTO public.media (id, model_type, model_id, collection_name, name, file_name, mime_type, disk, size, manipulations, custom_properties, responsive_images, order_column, created_at, updated_at) VALUES (37, 'App\Models\ApplicantDocument', 42, 'supporting-documents', '9DHQiOduYk0t6VUJLIUrG0G6DZNLG4p3DMPDgF81', '9DHQiOduYk0t6VUJLIUrG0G6DZNLG4p3DMPDgF81.jpeg', 'image/jpeg', 'supporting-documents', 4996, '[]', '{"name":"images.jpg","file_name":"images.jpg","width":220,"height":229,"generated_conversions":{"thumb_200":true,"thumb_150":true}}', '[]', 25, '2020-07-23 11:44:53', '2020-07-23 11:44:53');
INSERT INTO public.media (id, model_type, model_id, collection_name, name, file_name, mime_type, disk, size, manipulations, custom_properties, responsive_images, order_column, created_at, updated_at) VALUES (33, 'App\Models\ApplicantDocument', 38, 'supporting-documents', 'UxrgdwUurwtPcmgcOzQq6tmbWDvfkpszSQNS8UuL', 'UxrgdwUurwtPcmgcOzQq6tmbWDvfkpszSQNS8UuL.jpeg', 'image/jpeg', 'supporting-documents', 12075, '[]', '{"name":"descarga.jpg","file_name":"descarga.jpg","width":302,"height":167,"generated_conversions":{"thumb_200":true,"thumb_150":true}}', '[]', 21, '2020-07-10 11:05:33', '2020-07-10 11:05:34');
INSERT INTO public.media (id, model_type, model_id, collection_name, name, file_name, mime_type, disk, size, manipulations, custom_properties, responsive_images, order_column, created_at, updated_at) VALUES (34, 'App\Models\ApplicantDocument', 39, 'supporting-documents', 'Zq3cT7YG3my9h7VhZ5rC44MO8wpzmYAaWQTajsQD', 'Zq3cT7YG3my9h7VhZ5rC44MO8wpzmYAaWQTajsQD.jpeg', 'image/jpeg', 'supporting-documents', 65214, '[]', '{"name":"cara-hombre-de-dibujos-animados-jb47n6.jpg","file_name":"cara-hombre-de-dibujos-animados-jb47n6.jpg","width":1201,"height":1390,"generated_conversions":{"thumb_200":true,"thumb_150":true}}', '[]', 22, '2020-07-13 10:42:06', '2020-07-13 10:42:07');
INSERT INTO public.media (id, model_type, model_id, collection_name, name, file_name, mime_type, disk, size, manipulations, custom_properties, responsive_images, order_column, created_at, updated_at) VALUES (41, 'App\Models\ApplicantDocument', 46, 'supporting-documents', '9FZC4bRnQdewXDU5gEUw4z6zDyvls5rKayIag18A', '9FZC4bRnQdewXDU5gEUw4z6zDyvls5rKayIag18A.jpeg', 'image/jpeg', 'supporting-documents', 65214, '[]', '{"name":"cara-hombre-de-dibujos-animados-jb47n6.jpg","file_name":"cara-hombre-de-dibujos-animados-jb47n6.jpg","width":1201,"height":1390,"generated_conversions":{"thumb_200":true,"thumb_150":true}}', '[]', 29, '2020-07-23 11:46:53', '2020-07-23 11:46:54');
INSERT INTO public.media (id, model_type, model_id, collection_name, name, file_name, mime_type, disk, size, manipulations, custom_properties, responsive_images, order_column, created_at, updated_at) VALUES (38, 'App\Models\ApplicantDocument', 43, 'supporting-documents', '7yzXj1oJvHpHf2XlmiNH9pNzgjOopUKFsHi8GFJ8', '7yzXj1oJvHpHf2XlmiNH9pNzgjOopUKFsHi8GFJ8.jpeg', 'image/jpeg', 'supporting-documents', 4996, '[]', '{"name":"images.jpg","file_name":"images.jpg","width":220,"height":229,"generated_conversions":{"thumb_200":true,"thumb_150":true}}', '[]', 26, '2020-07-23 11:45:33', '2020-07-23 11:45:33');
INSERT INTO public.media (id, model_type, model_id, collection_name, name, file_name, mime_type, disk, size, manipulations, custom_properties, responsive_images, order_column, created_at, updated_at) VALUES (39, 'App\Models\ApplicantDocument', 44, 'supporting-documents', 'qgywt3JTFWTx2vN1xrhnwUdDZsl1v9TnCdLtO4Tf', 'qgywt3JTFWTx2vN1xrhnwUdDZsl1v9TnCdLtO4Tf.jpeg', 'image/jpeg', 'supporting-documents', 4996, '[]', '{"name":"images.jpg","file_name":"images.jpg","width":220,"height":229,"generated_conversions":{"thumb_200":true,"thumb_150":true}}', '[]', 27, '2020-07-23 11:45:53', '2020-07-23 11:45:54');
INSERT INTO public.media (id, model_type, model_id, collection_name, name, file_name, mime_type, disk, size, manipulations, custom_properties, responsive_images, order_column, created_at, updated_at) VALUES (43, 'App\Models\ApplicantDocument', 48, 'supporting-documents', 'vztp7naqEkMvfM98cMgmEGmlfuTwDhqYAhcsNGX4', 'vztp7naqEkMvfM98cMgmEGmlfuTwDhqYAhcsNGX4.jpeg', 'image/jpeg', 'supporting-documents', 65214, '[]', '{"name":"cara-hombre-de-dibujos-animados-jb47n6.jpg","file_name":"cara-hombre-de-dibujos-animados-jb47n6.jpg","width":1201,"height":1390,"generated_conversions":{"thumb_200":true,"thumb_150":true}}', '[]', 31, '2020-07-23 11:47:27', '2020-07-23 11:47:28');
INSERT INTO public.media (id, model_type, model_id, collection_name, name, file_name, mime_type, disk, size, manipulations, custom_properties, responsive_images, order_column, created_at, updated_at) VALUES (45, 'App\Models\ApplicantDocument', 50, 'supporting-documents', '98s3BDVs0QanuEohZrdTYodidkCgIImn1Lg21PX2', '98s3BDVs0QanuEohZrdTYodidkCgIImn1Lg21PX2.jpeg', 'image/jpeg', 'supporting-documents', 123714, '[]', '{"name":"20897385-t\u00edo-sam-de-dibujos-animados-que-quiere.jpg","file_name":"20897385-t\u00edo-sam-de-dibujos-animados-que-quiere.jpg","width":964,"height":1300,"generated_conversions":{"thumb_200":true,"thumb_150":true}}', '[]', 33, '2020-07-23 11:47:58', '2020-07-23 11:47:59');
INSERT INTO public.media (id, model_type, model_id, collection_name, name, file_name, mime_type, disk, size, manipulations, custom_properties, responsive_images, order_column, created_at, updated_at) VALUES (44, 'App\Models\ApplicantDocument', 49, 'supporting-documents', 'LzcePy5bw0XawLQGRb8c5IcHMPTAdELgqXxD1OgZ', 'LzcePy5bw0XawLQGRb8c5IcHMPTAdELgqXxD1OgZ.jpeg', 'image/jpeg', 'supporting-documents', 12075, '[]', '{"name":"descarga.jpg","file_name":"descarga.jpg","width":302,"height":167,"generated_conversions":{"thumb_200":true,"thumb_150":true}}', '[]', 32, '2020-07-23 11:47:43', '2020-07-23 11:47:44');
INSERT INTO public.media (id, model_type, model_id, collection_name, name, file_name, mime_type, disk, size, manipulations, custom_properties, responsive_images, order_column, created_at, updated_at) VALUES (46, 'App\Models\ApplicantDocument', 51, 'supporting-documents', 'mnQJhuBlbOHSCQNxsgME0eAPf9EmD1LcTrlHGqt0', 'mnQJhuBlbOHSCQNxsgME0eAPf9EmD1LcTrlHGqt0.jpeg', 'image/jpeg', 'supporting-documents', 71249, '[]', '{"name":"coleccion-avatares-ilustrados-personas_23-2148468903.jpg","file_name":"coleccion-avatares-ilustrados-personas_23-2148468903.jpg","width":626,"height":626,"generated_conversions":{"thumb_200":true,"thumb_150":true}}', '[]', 34, '2020-07-23 11:48:14', '2020-07-23 11:48:15');
INSERT INTO public.media (id, model_type, model_id, collection_name, name, file_name, mime_type, disk, size, manipulations, custom_properties, responsive_images, order_column, created_at, updated_at) VALUES (49, 'App\Models\ApplicantDocument', 54, 'supporting-documents', 'QdgGkHPNSAUcLISYRA8zZyGHSXDe73TyAcjjhvIO', 'QdgGkHPNSAUcLISYRA8zZyGHSXDe73TyAcjjhvIO.jpeg', 'image/jpeg', 'supporting-documents', 4996, '[]', '{"name":"images.jpg","file_name":"images.jpg","width":220,"height":229,"generated_conversions":{"thumb_200":true,"thumb_150":true}}', '[]', 35, '2020-07-23 13:35:47', '2020-07-23 13:35:48');
INSERT INTO public.media (id, model_type, model_id, collection_name, name, file_name, mime_type, disk, size, manipulations, custom_properties, responsive_images, order_column, created_at, updated_at) VALUES (50, 'App\Models\ApplicantDocument', 55, 'supporting-documents', '5lAlf6zC6yRPzOVZFBq5OQX7kM1PVOYAuexyuljg', '5lAlf6zC6yRPzOVZFBq5OQX7kM1PVOYAuexyuljg.jpeg', 'image/jpeg', 'supporting-documents', 123714, '[]', '{"name":"20897385-t\u00edo-sam-de-dibujos-animados-que-quiere.jpg","file_name":"20897385-t\u00edo-sam-de-dibujos-animados-que-quiere.jpg","width":964,"height":1300,"generated_conversions":{"thumb_200":true,"thumb_150":true}}', '[]', 36, '2020-07-23 13:36:05', '2020-07-23 13:36:06');
INSERT INTO public.media (id, model_type, model_id, collection_name, name, file_name, mime_type, disk, size, manipulations, custom_properties, responsive_images, order_column, created_at, updated_at) VALUES (51, 'App\Models\ApplicantDocument', 56, 'supporting-documents', '8IvXJleyVxE81QMBGmFJVpYelOOZJKD5yxVVlbl9', '8IvXJleyVxE81QMBGmFJVpYelOOZJKD5yxVVlbl9.jpeg', 'image/jpeg', 'supporting-documents', 57301, '[]', '{"name":"calificacion-final.jpg","file_name":"calificacion-final.jpg","width":1280,"height":768,"generated_conversions":{"thumb_200":true,"thumb_150":true}}', '[]', 37, '2020-09-07 09:53:57', '2020-09-07 09:54:00');
INSERT INTO public.media (id, model_type, model_id, collection_name, name, file_name, mime_type, disk, size, manipulations, custom_properties, responsive_images, order_column, created_at, updated_at) VALUES (57, 'App\Models\ApplicantDocument', 62, 'supporting-documents', 'piKYPnlRbVukEPmEEKAZM3pIuuMuUcUdFvGSJ6js', 'piKYPnlRbVukEPmEEKAZM3pIuuMuUcUdFvGSJ6js.png', 'image/png', 'supporting-documents', 42730, '[]', '{"name":"listado vista AMA.PNG","file_name":"listado vista AMA.PNG","width":1597,"height":853,"generated_conversions":{"thumb_200":true,"thumb_150":true}}', '[]', 43, '2020-09-07 09:56:09', '2020-09-07 09:56:10');
INSERT INTO public.media (id, model_type, model_id, collection_name, name, file_name, mime_type, disk, size, manipulations, custom_properties, responsive_images, order_column, created_at, updated_at) VALUES (52, 'App\Models\ApplicantDocument', 57, 'supporting-documents', 'fwbQMLLEaF29s0uDDEWQL4YaXXptI5Tr57IJdHzV', 'fwbQMLLEaF29s0uDDEWQL4YaXXptI5Tr57IJdHzV.png', 'image/png', 'supporting-documents', 84517, '[]', '{"name":"documentos-sociales.png","file_name":"documentos-sociales.png","width":1595,"height":897,"generated_conversions":{"thumb_200":true,"thumb_150":true}}', '[]', 38, '2020-09-07 09:54:19', '2020-09-07 09:54:20');
INSERT INTO public.media (id, model_type, model_id, collection_name, name, file_name, mime_type, disk, size, manipulations, custom_properties, responsive_images, order_column, created_at, updated_at) VALUES (53, 'App\Models\ApplicantDocument', 58, 'supporting-documents', 'COdx1smyCNMILI1A6G4bPJcORgUJS21GjvtVbS43', 'COdx1smyCNMILI1A6G4bPJcORgUJS21GjvtVbS43.png', 'image/png', 'supporting-documents', 43436, '[]', '{"name":"seguimiento-cargado.png","file_name":"seguimiento-cargado.png","width":1589,"height":818,"generated_conversions":{"thumb_200":true,"thumb_150":true}}', '[]', 39, '2020-09-07 09:54:35', '2020-09-07 09:54:35');
INSERT INTO public.media (id, model_type, model_id, collection_name, name, file_name, mime_type, disk, size, manipulations, custom_properties, responsive_images, order_column, created_at, updated_at) VALUES (54, 'App\Models\ApplicantDocument', 59, 'supporting-documents', 'yLLxCR1bvydkALDnPonXUSHiP3H9DYZ58YQB8PHd', 'yLLxCR1bvydkALDnPonXUSHiP3H9DYZ58YQB8PHd.png', 'image/png', 'supporting-documents', 30457, '[]', '{"name":"seguimiento1.png","file_name":"seguimiento1.png","width":1585,"height":715,"generated_conversions":{"thumb_200":true,"thumb_150":true}}', '[]', 40, '2020-09-07 09:54:53', '2020-09-07 09:54:54');
INSERT INTO public.media (id, model_type, model_id, collection_name, name, file_name, mime_type, disk, size, manipulations, custom_properties, responsive_images, order_column, created_at, updated_at) VALUES (55, 'App\Models\ApplicantDocument', 60, 'supporting-documents', '8L28gHhjnpDqk6bkj23PZaPGW5pKqDC9Ts51a3pb', '8L28gHhjnpDqk6bkj23PZaPGW5pKqDC9Ts51a3pb.png', 'image/png', 'supporting-documents', 41015, '[]', '{"name":"listado-ATC.png","file_name":"listado-ATC.png","width":1591,"height":853,"generated_conversions":{"thumb_200":true,"thumb_150":true}}', '[]', 41, '2020-09-07 09:55:06', '2020-09-07 09:55:07');
INSERT INTO public.media (id, model_type, model_id, collection_name, name, file_name, mime_type, disk, size, manipulations, custom_properties, responsive_images, order_column, created_at, updated_at) VALUES (56, 'App\Models\ApplicantDocument', 61, 'supporting-documents', 'GnSuysYxGxGXKp4ZiFBlYhZabs1JK1o1EYUxTqbw', 'GnSuysYxGxGXKp4ZiFBlYhZabs1JK1o1EYUxTqbw.png', 'image/png', 'supporting-documents', 43243, '[]', '{"name":"documentos-tecnicos.png","file_name":"documentos-tecnicos.png","width":1587,"height":855,"generated_conversions":{"thumb_200":true,"thumb_150":true}}', '[]', 42, '2020-09-07 09:55:35', '2020-09-07 09:55:36');


--
-- TOC entry 3223 (class 0 OID 123050)
-- Dependencies: 244
-- Data for Name: migrations; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.migrations (id, migration, batch) VALUES (24, '2014_10_12_000000_create_users_table', 1);
INSERT INTO public.migrations (id, migration, batch) VALUES (25, '2014_10_12_100000_create_password_resets_table', 1);
INSERT INTO public.migrations (id, migration, batch) VALUES (26, '2017_08_24_000000_create_activations_table', 1);
INSERT INTO public.migrations (id, migration, batch) VALUES (27, '2017_08_24_000000_create_admin_activations_table', 1);
INSERT INTO public.migrations (id, migration, batch) VALUES (28, '2017_08_24_000000_create_admin_password_resets_table', 1);
INSERT INTO public.migrations (id, migration, batch) VALUES (29, '2017_08_24_000000_create_admin_users_table', 1);
INSERT INTO public.migrations (id, migration, batch) VALUES (30, '2018_07_18_000000_create_wysiwyg_media_table', 1);
INSERT INTO public.migrations (id, migration, batch) VALUES (31, '2019_08_19_000000_create_failed_jobs_table', 1);
INSERT INTO public.migrations (id, migration, batch) VALUES (32, '2020_01_10_100027_create_media_table', 1);
INSERT INTO public.migrations (id, migration, batch) VALUES (33, '2020_01_10_100027_create_permission_tables', 1);
INSERT INTO public.migrations (id, migration, batch) VALUES (34, '2020_01_10_100027_create_translations_table', 1);
INSERT INTO public.migrations (id, migration, batch) VALUES (35, '2020_01_10_100032_fill_default_admin_user_and_permissions', 1);
INSERT INTO public.migrations (id, migration, batch) VALUES (36, '2020_01_10_105830_fill_permissions_for_city', 1);
INSERT INTO public.migrations (id, migration, batch) VALUES (37, '2020_01_10_110020_fill_permissions_for_contact-method', 1);
INSERT INTO public.migrations (id, migration, batch) VALUES (38, '2020_01_10_110114_fill_permissions_for_disability', 1);
INSERT INTO public.migrations (id, migration, batch) VALUES (39, '2020_01_10_110215_fill_permissions_for_disease', 1);
INSERT INTO public.migrations (id, migration, batch) VALUES (40, '2020_01_10_110259_fill_permissions_for_education-level', 1);
INSERT INTO public.migrations (id, migration, batch) VALUES (41, '2020_01_10_111122_fill_permissions_for_state', 1);
INSERT INTO public.migrations (id, migration, batch) VALUES (42, '2020_01_10_111312_fill_permissions_for_status', 1);
INSERT INTO public.migrations (id, migration, batch) VALUES (43, '2020_01_10_111405_fill_permissions_for_questionnaire-type', 1);
INSERT INTO public.migrations (id, migration, batch) VALUES (44, '2020_01_10_140717_fill_permissions_for_applicant-relationship', 1);
INSERT INTO public.migrations (id, migration, batch) VALUES (45, '2020_01_10_142738_fill_permissions_for_applicant', 1);
INSERT INTO public.migrations (id, migration, batch) VALUES (46, '2020_01_10_143547_fill_permissions_for_questionnaire-template', 1);
INSERT INTO public.migrations (id, migration, batch) VALUES (47, '2020_05_05_065934_fill_permissions_for_document-type', 2);
INSERT INTO public.migrations (id, migration, batch) VALUES (48, '2020_05_05_074407_fill_permissions_for_document-type', 3);
INSERT INTO public.migrations (id, migration, batch) VALUES (49, '2020_05_05_083252_fill_permissions_for_applicant-document', 4);
INSERT INTO public.migrations (id, migration, batch) VALUES (50, '2020_05_18_235927_fill_permissions_for_construction-entity', 5);
INSERT INTO public.migrations (id, migration, batch) VALUES (51, '2020_05_18_235947_fill_permissions_for_financial-entity', 6);
INSERT INTO public.migrations (id, migration, batch) VALUES (52, '2020_06_04_140449_fill_permissions_for_status', 7);


--
-- TOC entry 3224 (class 0 OID 123054)
-- Dependencies: 245
-- Data for Name: model_has_permissions; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 3225 (class 0 OID 123057)
-- Dependencies: 246
-- Data for Name: model_has_roles; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.model_has_roles (role_id, model_type, model_id) VALUES (1, 'App\Models\AdminUser', 1);
INSERT INTO public.model_has_roles (role_id, model_type, model_id) VALUES (2, 'App\Models\AdminUser', 2);
INSERT INTO public.model_has_roles (role_id, model_type, model_id) VALUES (3, 'App\Models\AdminUser', 3);
INSERT INTO public.model_has_roles (role_id, model_type, model_id) VALUES (4, 'App\Models\AdminUser', 4);
INSERT INTO public.model_has_roles (role_id, model_type, model_id) VALUES (1, 'App\Models\AdminUser', 2);
INSERT INTO public.model_has_roles (role_id, model_type, model_id) VALUES (1, 'App\Models\AdminUser', 3);
INSERT INTO public.model_has_roles (role_id, model_type, model_id) VALUES (1, 'App\Models\AdminUser', 4);
INSERT INTO public.model_has_roles (role_id, model_type, model_id) VALUES (1, 'App\Models\AdminUser', 5);


--
-- TOC entry 3226 (class 0 OID 123060)
-- Dependencies: 247
-- Data for Name: password_resets; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 3227 (class 0 OID 123066)
-- Dependencies: 248
-- Data for Name: permissions; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.permissions (id, name, guard_name, created_at, updated_at) VALUES (1, 'admin', 'admin', '2020-05-03 19:15:28', '2020-05-03 19:15:28');
INSERT INTO public.permissions (id, name, guard_name, created_at, updated_at) VALUES (2, 'admin.translation.index', 'admin', '2020-05-03 19:15:28', '2020-05-03 19:15:28');
INSERT INTO public.permissions (id, name, guard_name, created_at, updated_at) VALUES (3, 'admin.translation.edit', 'admin', '2020-05-03 19:15:28', '2020-05-03 19:15:28');
INSERT INTO public.permissions (id, name, guard_name, created_at, updated_at) VALUES (4, 'admin.translation.rescan', 'admin', '2020-05-03 19:15:28', '2020-05-03 19:15:28');
INSERT INTO public.permissions (id, name, guard_name, created_at, updated_at) VALUES (5, 'admin.admin-user.index', 'admin', '2020-05-03 19:15:28', '2020-05-03 19:15:28');
INSERT INTO public.permissions (id, name, guard_name, created_at, updated_at) VALUES (6, 'admin.admin-user.create', 'admin', '2020-05-03 19:15:28', '2020-05-03 19:15:28');
INSERT INTO public.permissions (id, name, guard_name, created_at, updated_at) VALUES (7, 'admin.admin-user.edit', 'admin', '2020-05-03 19:15:28', '2020-05-03 19:15:28');
INSERT INTO public.permissions (id, name, guard_name, created_at, updated_at) VALUES (8, 'admin.admin-user.delete', 'admin', '2020-05-03 19:15:28', '2020-05-03 19:15:28');
INSERT INTO public.permissions (id, name, guard_name, created_at, updated_at) VALUES (9, 'admin.upload', 'admin', '2020-05-03 19:15:28', '2020-05-03 19:15:28');
INSERT INTO public.permissions (id, name, guard_name, created_at, updated_at) VALUES (10, 'admin.city', 'admin', '2020-05-03 19:15:28', '2020-05-03 19:15:28');
INSERT INTO public.permissions (id, name, guard_name, created_at, updated_at) VALUES (11, 'admin.city.index', 'admin', '2020-05-03 19:15:28', '2020-05-03 19:15:28');
INSERT INTO public.permissions (id, name, guard_name, created_at, updated_at) VALUES (12, 'admin.city.create', 'admin', '2020-05-03 19:15:28', '2020-05-03 19:15:28');
INSERT INTO public.permissions (id, name, guard_name, created_at, updated_at) VALUES (13, 'admin.city.show', 'admin', '2020-05-03 19:15:28', '2020-05-03 19:15:28');
INSERT INTO public.permissions (id, name, guard_name, created_at, updated_at) VALUES (14, 'admin.city.edit', 'admin', '2020-05-03 19:15:28', '2020-05-03 19:15:28');
INSERT INTO public.permissions (id, name, guard_name, created_at, updated_at) VALUES (15, 'admin.city.delete', 'admin', '2020-05-03 19:15:28', '2020-05-03 19:15:28');
INSERT INTO public.permissions (id, name, guard_name, created_at, updated_at) VALUES (16, 'admin.city.bulk-delete', 'admin', '2020-05-03 19:15:28', '2020-05-03 19:15:28');
INSERT INTO public.permissions (id, name, guard_name, created_at, updated_at) VALUES (17, 'admin.contact-method', 'admin', '2020-05-03 19:15:28', '2020-05-03 19:15:28');
INSERT INTO public.permissions (id, name, guard_name, created_at, updated_at) VALUES (18, 'admin.contact-method.index', 'admin', '2020-05-03 19:15:28', '2020-05-03 19:15:28');
INSERT INTO public.permissions (id, name, guard_name, created_at, updated_at) VALUES (19, 'admin.contact-method.create', 'admin', '2020-05-03 19:15:28', '2020-05-03 19:15:28');
INSERT INTO public.permissions (id, name, guard_name, created_at, updated_at) VALUES (20, 'admin.contact-method.show', 'admin', '2020-05-03 19:15:28', '2020-05-03 19:15:28');
INSERT INTO public.permissions (id, name, guard_name, created_at, updated_at) VALUES (21, 'admin.contact-method.edit', 'admin', '2020-05-03 19:15:28', '2020-05-03 19:15:28');
INSERT INTO public.permissions (id, name, guard_name, created_at, updated_at) VALUES (22, 'admin.contact-method.delete', 'admin', '2020-05-03 19:15:28', '2020-05-03 19:15:28');
INSERT INTO public.permissions (id, name, guard_name, created_at, updated_at) VALUES (23, 'admin.contact-method.bulk-delete', 'admin', '2020-05-03 19:15:28', '2020-05-03 19:15:28');
INSERT INTO public.permissions (id, name, guard_name, created_at, updated_at) VALUES (24, 'admin.disability', 'admin', '2020-05-03 19:15:28', '2020-05-03 19:15:28');
INSERT INTO public.permissions (id, name, guard_name, created_at, updated_at) VALUES (25, 'admin.disability.index', 'admin', '2020-05-03 19:15:28', '2020-05-03 19:15:28');
INSERT INTO public.permissions (id, name, guard_name, created_at, updated_at) VALUES (26, 'admin.disability.create', 'admin', '2020-05-03 19:15:28', '2020-05-03 19:15:28');
INSERT INTO public.permissions (id, name, guard_name, created_at, updated_at) VALUES (27, 'admin.disability.show', 'admin', '2020-05-03 19:15:28', '2020-05-03 19:15:28');
INSERT INTO public.permissions (id, name, guard_name, created_at, updated_at) VALUES (28, 'admin.disability.edit', 'admin', '2020-05-03 19:15:28', '2020-05-03 19:15:28');
INSERT INTO public.permissions (id, name, guard_name, created_at, updated_at) VALUES (29, 'admin.disability.delete', 'admin', '2020-05-03 19:15:28', '2020-05-03 19:15:28');
INSERT INTO public.permissions (id, name, guard_name, created_at, updated_at) VALUES (30, 'admin.disability.bulk-delete', 'admin', '2020-05-03 19:15:28', '2020-05-03 19:15:28');
INSERT INTO public.permissions (id, name, guard_name, created_at, updated_at) VALUES (31, 'admin.disease', 'admin', '2020-05-03 19:15:28', '2020-05-03 19:15:28');
INSERT INTO public.permissions (id, name, guard_name, created_at, updated_at) VALUES (32, 'admin.disease.index', 'admin', '2020-05-03 19:15:28', '2020-05-03 19:15:28');
INSERT INTO public.permissions (id, name, guard_name, created_at, updated_at) VALUES (33, 'admin.disease.create', 'admin', '2020-05-03 19:15:28', '2020-05-03 19:15:28');
INSERT INTO public.permissions (id, name, guard_name, created_at, updated_at) VALUES (34, 'admin.disease.show', 'admin', '2020-05-03 19:15:28', '2020-05-03 19:15:28');
INSERT INTO public.permissions (id, name, guard_name, created_at, updated_at) VALUES (35, 'admin.disease.edit', 'admin', '2020-05-03 19:15:28', '2020-05-03 19:15:28');
INSERT INTO public.permissions (id, name, guard_name, created_at, updated_at) VALUES (36, 'admin.disease.delete', 'admin', '2020-05-03 19:15:28', '2020-05-03 19:15:28');
INSERT INTO public.permissions (id, name, guard_name, created_at, updated_at) VALUES (37, 'admin.disease.bulk-delete', 'admin', '2020-05-03 19:15:28', '2020-05-03 19:15:28');
INSERT INTO public.permissions (id, name, guard_name, created_at, updated_at) VALUES (38, 'admin.education-level', 'admin', '2020-05-03 19:15:28', '2020-05-03 19:15:28');
INSERT INTO public.permissions (id, name, guard_name, created_at, updated_at) VALUES (39, 'admin.education-level.index', 'admin', '2020-05-03 19:15:28', '2020-05-03 19:15:28');
INSERT INTO public.permissions (id, name, guard_name, created_at, updated_at) VALUES (40, 'admin.education-level.create', 'admin', '2020-05-03 19:15:28', '2020-05-03 19:15:28');
INSERT INTO public.permissions (id, name, guard_name, created_at, updated_at) VALUES (41, 'admin.education-level.show', 'admin', '2020-05-03 19:15:28', '2020-05-03 19:15:28');
INSERT INTO public.permissions (id, name, guard_name, created_at, updated_at) VALUES (42, 'admin.education-level.edit', 'admin', '2020-05-03 19:15:28', '2020-05-03 19:15:28');
INSERT INTO public.permissions (id, name, guard_name, created_at, updated_at) VALUES (43, 'admin.education-level.delete', 'admin', '2020-05-03 19:15:28', '2020-05-03 19:15:28');
INSERT INTO public.permissions (id, name, guard_name, created_at, updated_at) VALUES (44, 'admin.education-level.bulk-delete', 'admin', '2020-05-03 19:15:28', '2020-05-03 19:15:28');
INSERT INTO public.permissions (id, name, guard_name, created_at, updated_at) VALUES (45, 'admin.state', 'admin', '2020-05-03 19:15:28', '2020-05-03 19:15:28');
INSERT INTO public.permissions (id, name, guard_name, created_at, updated_at) VALUES (46, 'admin.state.index', 'admin', '2020-05-03 19:15:28', '2020-05-03 19:15:28');
INSERT INTO public.permissions (id, name, guard_name, created_at, updated_at) VALUES (47, 'admin.state.create', 'admin', '2020-05-03 19:15:28', '2020-05-03 19:15:28');
INSERT INTO public.permissions (id, name, guard_name, created_at, updated_at) VALUES (48, 'admin.state.show', 'admin', '2020-05-03 19:15:28', '2020-05-03 19:15:28');
INSERT INTO public.permissions (id, name, guard_name, created_at, updated_at) VALUES (49, 'admin.state.edit', 'admin', '2020-05-03 19:15:28', '2020-05-03 19:15:28');
INSERT INTO public.permissions (id, name, guard_name, created_at, updated_at) VALUES (50, 'admin.state.delete', 'admin', '2020-05-03 19:15:28', '2020-05-03 19:15:28');
INSERT INTO public.permissions (id, name, guard_name, created_at, updated_at) VALUES (51, 'admin.state.bulk-delete', 'admin', '2020-05-03 19:15:28', '2020-05-03 19:15:28');
INSERT INTO public.permissions (id, name, guard_name, created_at, updated_at) VALUES (52, 'admin.status', 'admin', '2020-05-03 19:15:28', '2020-05-03 19:15:28');
INSERT INTO public.permissions (id, name, guard_name, created_at, updated_at) VALUES (53, 'admin.status.index', 'admin', '2020-05-03 19:15:28', '2020-05-03 19:15:28');
INSERT INTO public.permissions (id, name, guard_name, created_at, updated_at) VALUES (54, 'admin.status.create', 'admin', '2020-05-03 19:15:28', '2020-05-03 19:15:28');
INSERT INTO public.permissions (id, name, guard_name, created_at, updated_at) VALUES (55, 'admin.status.show', 'admin', '2020-05-03 19:15:28', '2020-05-03 19:15:28');
INSERT INTO public.permissions (id, name, guard_name, created_at, updated_at) VALUES (56, 'admin.status.edit', 'admin', '2020-05-03 19:15:28', '2020-05-03 19:15:28');
INSERT INTO public.permissions (id, name, guard_name, created_at, updated_at) VALUES (57, 'admin.status.delete', 'admin', '2020-05-03 19:15:28', '2020-05-03 19:15:28');
INSERT INTO public.permissions (id, name, guard_name, created_at, updated_at) VALUES (58, 'admin.status.bulk-delete', 'admin', '2020-05-03 19:15:28', '2020-05-03 19:15:28');
INSERT INTO public.permissions (id, name, guard_name, created_at, updated_at) VALUES (59, 'admin.questionnaire-type', 'admin', '2020-05-03 19:15:28', '2020-05-03 19:15:28');
INSERT INTO public.permissions (id, name, guard_name, created_at, updated_at) VALUES (60, 'admin.questionnaire-type.index', 'admin', '2020-05-03 19:15:28', '2020-05-03 19:15:28');
INSERT INTO public.permissions (id, name, guard_name, created_at, updated_at) VALUES (61, 'admin.questionnaire-type.create', 'admin', '2020-05-03 19:15:28', '2020-05-03 19:15:28');
INSERT INTO public.permissions (id, name, guard_name, created_at, updated_at) VALUES (62, 'admin.questionnaire-type.show', 'admin', '2020-05-03 19:15:28', '2020-05-03 19:15:28');
INSERT INTO public.permissions (id, name, guard_name, created_at, updated_at) VALUES (63, 'admin.questionnaire-type.edit', 'admin', '2020-05-03 19:15:28', '2020-05-03 19:15:28');
INSERT INTO public.permissions (id, name, guard_name, created_at, updated_at) VALUES (64, 'admin.questionnaire-type.delete', 'admin', '2020-05-03 19:15:28', '2020-05-03 19:15:28');
INSERT INTO public.permissions (id, name, guard_name, created_at, updated_at) VALUES (65, 'admin.questionnaire-type.bulk-delete', 'admin', '2020-05-03 19:15:28', '2020-05-03 19:15:28');
INSERT INTO public.permissions (id, name, guard_name, created_at, updated_at) VALUES (66, 'admin.applicant-relationship', 'admin', '2020-05-03 19:15:28', '2020-05-03 19:15:28');
INSERT INTO public.permissions (id, name, guard_name, created_at, updated_at) VALUES (67, 'admin.applicant-relationship.index', 'admin', '2020-05-03 19:15:28', '2020-05-03 19:15:28');
INSERT INTO public.permissions (id, name, guard_name, created_at, updated_at) VALUES (68, 'admin.applicant-relationship.create', 'admin', '2020-05-03 19:15:28', '2020-05-03 19:15:28');
INSERT INTO public.permissions (id, name, guard_name, created_at, updated_at) VALUES (69, 'admin.applicant-relationship.show', 'admin', '2020-05-03 19:15:28', '2020-05-03 19:15:28');
INSERT INTO public.permissions (id, name, guard_name, created_at, updated_at) VALUES (70, 'admin.applicant-relationship.edit', 'admin', '2020-05-03 19:15:28', '2020-05-03 19:15:28');
INSERT INTO public.permissions (id, name, guard_name, created_at, updated_at) VALUES (71, 'admin.applicant-relationship.delete', 'admin', '2020-05-03 19:15:28', '2020-05-03 19:15:28');
INSERT INTO public.permissions (id, name, guard_name, created_at, updated_at) VALUES (72, 'admin.applicant-relationship.bulk-delete', 'admin', '2020-05-03 19:15:28', '2020-05-03 19:15:28');
INSERT INTO public.permissions (id, name, guard_name, created_at, updated_at) VALUES (73, 'admin.applicant', 'admin', '2020-05-03 19:15:28', '2020-05-03 19:15:28');
INSERT INTO public.permissions (id, name, guard_name, created_at, updated_at) VALUES (74, 'admin.applicant.index', 'admin', '2020-05-03 19:15:28', '2020-05-03 19:15:28');
INSERT INTO public.permissions (id, name, guard_name, created_at, updated_at) VALUES (75, 'admin.applicant.create', 'admin', '2020-05-03 19:15:28', '2020-05-03 19:15:28');
INSERT INTO public.permissions (id, name, guard_name, created_at, updated_at) VALUES (76, 'admin.applicant.show', 'admin', '2020-05-03 19:15:28', '2020-05-03 19:15:28');
INSERT INTO public.permissions (id, name, guard_name, created_at, updated_at) VALUES (77, 'admin.applicant.edit', 'admin', '2020-05-03 19:15:28', '2020-05-03 19:15:28');
INSERT INTO public.permissions (id, name, guard_name, created_at, updated_at) VALUES (78, 'admin.applicant.delete', 'admin', '2020-05-03 19:15:28', '2020-05-03 19:15:28');
INSERT INTO public.permissions (id, name, guard_name, created_at, updated_at) VALUES (79, 'admin.applicant.bulk-delete', 'admin', '2020-05-03 19:15:28', '2020-05-03 19:15:28');
INSERT INTO public.permissions (id, name, guard_name, created_at, updated_at) VALUES (80, 'admin.questionnaire-template', 'admin', '2020-05-03 19:15:28', '2020-05-03 19:15:28');
INSERT INTO public.permissions (id, name, guard_name, created_at, updated_at) VALUES (81, 'admin.questionnaire-template.index', 'admin', '2020-05-03 19:15:28', '2020-05-03 19:15:28');
INSERT INTO public.permissions (id, name, guard_name, created_at, updated_at) VALUES (82, 'admin.questionnaire-template.create', 'admin', '2020-05-03 19:15:28', '2020-05-03 19:15:28');
INSERT INTO public.permissions (id, name, guard_name, created_at, updated_at) VALUES (83, 'admin.questionnaire-template.show', 'admin', '2020-05-03 19:15:28', '2020-05-03 19:15:28');
INSERT INTO public.permissions (id, name, guard_name, created_at, updated_at) VALUES (84, 'admin.questionnaire-template.edit', 'admin', '2020-05-03 19:15:28', '2020-05-03 19:15:28');
INSERT INTO public.permissions (id, name, guard_name, created_at, updated_at) VALUES (85, 'admin.questionnaire-template.delete', 'admin', '2020-05-03 19:15:28', '2020-05-03 19:15:28');
INSERT INTO public.permissions (id, name, guard_name, created_at, updated_at) VALUES (86, 'admin.questionnaire-template.bulk-delete', 'admin', '2020-05-03 19:15:28', '2020-05-03 19:15:28');
INSERT INTO public.permissions (id, name, guard_name, created_at, updated_at) VALUES (87, 'admin.document-type', 'admin', '2020-05-05 06:59:34', '2020-05-05 06:59:34');
INSERT INTO public.permissions (id, name, guard_name, created_at, updated_at) VALUES (88, 'admin.document-type.index', 'admin', '2020-05-05 06:59:34', '2020-05-05 06:59:34');
INSERT INTO public.permissions (id, name, guard_name, created_at, updated_at) VALUES (89, 'admin.document-type.create', 'admin', '2020-05-05 06:59:34', '2020-05-05 06:59:34');
INSERT INTO public.permissions (id, name, guard_name, created_at, updated_at) VALUES (90, 'admin.document-type.show', 'admin', '2020-05-05 06:59:34', '2020-05-05 06:59:34');
INSERT INTO public.permissions (id, name, guard_name, created_at, updated_at) VALUES (91, 'admin.document-type.edit', 'admin', '2020-05-05 06:59:34', '2020-05-05 06:59:34');
INSERT INTO public.permissions (id, name, guard_name, created_at, updated_at) VALUES (92, 'admin.document-type.delete', 'admin', '2020-05-05 06:59:34', '2020-05-05 06:59:34');
INSERT INTO public.permissions (id, name, guard_name, created_at, updated_at) VALUES (93, 'admin.document-type.bulk-delete', 'admin', '2020-05-05 06:59:34', '2020-05-05 06:59:34');
INSERT INTO public.permissions (id, name, guard_name, created_at, updated_at) VALUES (94, 'admin.applicant-document', 'admin', '2020-05-05 08:32:58', '2020-05-05 08:32:58');
INSERT INTO public.permissions (id, name, guard_name, created_at, updated_at) VALUES (95, 'admin.applicant-document.index', 'admin', '2020-05-05 08:32:58', '2020-05-05 08:32:58');
INSERT INTO public.permissions (id, name, guard_name, created_at, updated_at) VALUES (96, 'admin.applicant-document.create', 'admin', '2020-05-05 08:32:58', '2020-05-05 08:32:58');
INSERT INTO public.permissions (id, name, guard_name, created_at, updated_at) VALUES (97, 'admin.applicant-document.show', 'admin', '2020-05-05 08:32:58', '2020-05-05 08:32:58');
INSERT INTO public.permissions (id, name, guard_name, created_at, updated_at) VALUES (98, 'admin.applicant-document.edit', 'admin', '2020-05-05 08:32:58', '2020-05-05 08:32:58');
INSERT INTO public.permissions (id, name, guard_name, created_at, updated_at) VALUES (99, 'admin.applicant-document.delete', 'admin', '2020-05-05 08:32:58', '2020-05-05 08:32:58');
INSERT INTO public.permissions (id, name, guard_name, created_at, updated_at) VALUES (100, 'admin.applicant-document.bulk-delete', 'admin', '2020-05-05 08:32:58', '2020-05-05 08:32:58');
INSERT INTO public.permissions (id, name, guard_name, created_at, updated_at) VALUES (101, 'admin.construction-entity', 'admin', '2020-05-18 23:59:30', '2020-05-18 23:59:30');
INSERT INTO public.permissions (id, name, guard_name, created_at, updated_at) VALUES (102, 'admin.construction-entity.index', 'admin', '2020-05-18 23:59:30', '2020-05-18 23:59:30');
INSERT INTO public.permissions (id, name, guard_name, created_at, updated_at) VALUES (103, 'admin.construction-entity.create', 'admin', '2020-05-18 23:59:30', '2020-05-18 23:59:30');
INSERT INTO public.permissions (id, name, guard_name, created_at, updated_at) VALUES (104, 'admin.construction-entity.show', 'admin', '2020-05-18 23:59:30', '2020-05-18 23:59:30');
INSERT INTO public.permissions (id, name, guard_name, created_at, updated_at) VALUES (105, 'admin.construction-entity.edit', 'admin', '2020-05-18 23:59:30', '2020-05-18 23:59:30');
INSERT INTO public.permissions (id, name, guard_name, created_at, updated_at) VALUES (106, 'admin.construction-entity.delete', 'admin', '2020-05-18 23:59:30', '2020-05-18 23:59:30');
INSERT INTO public.permissions (id, name, guard_name, created_at, updated_at) VALUES (107, 'admin.construction-entity.bulk-delete', 'admin', '2020-05-18 23:59:30', '2020-05-18 23:59:30');
INSERT INTO public.permissions (id, name, guard_name, created_at, updated_at) VALUES (108, 'admin.financial-entity', 'admin', '2020-05-18 23:59:49', '2020-05-18 23:59:49');
INSERT INTO public.permissions (id, name, guard_name, created_at, updated_at) VALUES (109, 'admin.financial-entity.index', 'admin', '2020-05-18 23:59:49', '2020-05-18 23:59:49');
INSERT INTO public.permissions (id, name, guard_name, created_at, updated_at) VALUES (110, 'admin.financial-entity.create', 'admin', '2020-05-18 23:59:49', '2020-05-18 23:59:49');
INSERT INTO public.permissions (id, name, guard_name, created_at, updated_at) VALUES (111, 'admin.financial-entity.show', 'admin', '2020-05-18 23:59:49', '2020-05-18 23:59:49');
INSERT INTO public.permissions (id, name, guard_name, created_at, updated_at) VALUES (112, 'admin.financial-entity.edit', 'admin', '2020-05-18 23:59:49', '2020-05-18 23:59:49');
INSERT INTO public.permissions (id, name, guard_name, created_at, updated_at) VALUES (113, 'admin.financial-entity.delete', 'admin', '2020-05-18 23:59:49', '2020-05-18 23:59:49');
INSERT INTO public.permissions (id, name, guard_name, created_at, updated_at) VALUES (114, 'admin.financial-entity.bulk-delete', 'admin', '2020-05-18 23:59:49', '2020-05-18 23:59:49');
INSERT INTO public.permissions (id, name, guard_name, created_at, updated_at) VALUES (115, 'admin.report.index', 'admin', '2020-05-29 09:25:23', '2020-05-29 09:25:25');


--
-- TOC entry 3228 (class 0 OID 123073)
-- Dependencies: 249
-- Data for Name: questionnaire_template_questions; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.questionnaire_template_questions (id, questionnaire_template_id, question, question_type, "values", extended_placeholder, "order", created_at, updated_at, comment, comment_placeholder, received_at) VALUES (2, 1, 'La casa donde vive, ¿cuenta con titulo de propiedad a su nombre?', 'B', NULL, NULL, 2, NULL, NULL, true, 'Observación', false);
INSERT INTO public.questionnaire_template_questions (id, questionnaire_template_id, question, question_type, "values", extended_placeholder, "order", created_at, updated_at, comment, comment_placeholder, received_at) VALUES (6, 1, 'Recibio alguna vez su conyuge/pareja subsidio habitacional del estado', 'B', NULL, NULL, 6, NULL, NULL, true, 'Observación', false);
INSERT INTO public.questionnaire_template_questions (id, questionnaire_template_id, question, question_type, "values", extended_placeholder, "order", created_at, updated_at, comment, comment_placeholder, received_at) VALUES (1, 1, 'Cuenta con casa propia?', 'B', NULL, NULL, 1, NULL, NULL, true, 'Observación', false);
INSERT INTO public.questionnaire_template_questions (id, questionnaire_template_id, question, question_type, "values", extended_placeholder, "order", created_at, updated_at, comment, comment_placeholder, received_at) VALUES (15, 2, 'Calificación Final', 'B', NULL, 'Calificación Final', NULL, NULL, NULL, true, 'Observación', true);
INSERT INTO public.questionnaire_template_questions (id, questionnaire_template_id, question, question_type, "values", extended_placeholder, "order", created_at, updated_at, comment, comment_placeholder, received_at) VALUES (4, 1, '¿Podría Contar con un Ahorro Previo de 700.000 Gs?', 'B', NULL, NULL, 4, NULL, NULL, true, 'Observación', false);
INSERT INTO public.questionnaire_template_questions (id, questionnaire_template_id, question, question_type, "values", extended_placeholder, "order", created_at, updated_at, comment, comment_placeholder, received_at) VALUES (5, 1, '¿Es sujeto de crédito en su cooperativa por 3.500.000 Gs?', 'B', NULL, NULL, 5, NULL, NULL, true, 'Observación', false);
INSERT INTO public.questionnaire_template_questions (id, questionnaire_template_id, question, question_type, "values", extended_placeholder, "order", created_at, updated_at, comment, comment_placeholder, received_at) VALUES (3, 1, 'Recibio alguna vez subsidio habitacional del estado', 'B', NULL, NULL, 3, NULL, NULL, true, 'Observación', false);
INSERT INTO public.questionnaire_template_questions (id, questionnaire_template_id, question, question_type, "values", extended_placeholder, "order", created_at, updated_at, comment, comment_placeholder, received_at) VALUES (14, 1, '¿Tiene RUC activo?', 'B', NULL, NULL, 8, NULL, NULL, true, 'Observación', false);
INSERT INTO public.questionnaire_template_questions (id, questionnaire_template_id, question, question_type, "values", extended_placeholder, "order", created_at, updated_at, comment, comment_placeholder, received_at) VALUES (13, 1, '¿Tiene pagado al menos el 70% de su propiedad?', 'B', NULL, NULL, 7, NULL, NULL, true, 'Observación', false);
INSERT INTO public.questionnaire_template_questions (id, questionnaire_template_id, question, question_type, "values", extended_placeholder, "order", created_at, updated_at, comment, comment_placeholder, received_at) VALUES (7, 2, 'Social', 'B', NULL, NULL, 1, NULL, NULL, true, 'Observación', true);
INSERT INTO public.questionnaire_template_questions (id, questionnaire_template_id, question, question_type, "values", extended_placeholder, "order", created_at, updated_at, comment, comment_placeholder, received_at) VALUES (8, 2, 'Financiero', 'B', NULL, NULL, 2, NULL, NULL, true, 'Observación', true);
INSERT INTO public.questionnaire_template_questions (id, questionnaire_template_id, question, question_type, "values", extended_placeholder, "order", created_at, updated_at, comment, comment_placeholder, received_at) VALUES (9, 2, 'Técnico', 'B', NULL, NULL, 3, NULL, NULL, true, 'Observación', true);


--
-- TOC entry 3229 (class 0 OID 123082)
-- Dependencies: 250
-- Data for Name: questionnaire_templates; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.questionnaire_templates (id, name, enabled, questionnaire_type_id, created_at, updated_at) VALUES (1, 'Social', true, 1, NULL, NULL);
INSERT INTO public.questionnaire_templates (id, name, enabled, questionnaire_type_id, created_at, updated_at) VALUES (2, 'Evaluación Final', true, 2, NULL, NULL);


--
-- TOC entry 3230 (class 0 OID 123087)
-- Dependencies: 251
-- Data for Name: questionnaire_types; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.questionnaire_types (id, name, created_at, updated_at) VALUES (1, 'coop-cuestionario', NULL, NULL);
INSERT INTO public.questionnaire_types (id, name, created_at, updated_at) VALUES (2, 'ama-evaluación-final', NULL, NULL);


--
-- TOC entry 3231 (class 0 OID 123091)
-- Dependencies: 252
-- Data for Name: role_has_permissions; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (1, 2);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (2, 2);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (3, 2);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (4, 2);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (5, 2);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (6, 2);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (7, 2);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (8, 2);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (9, 2);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (10, 2);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (11, 2);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (12, 2);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (13, 2);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (14, 2);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (15, 2);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (16, 2);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (17, 2);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (18, 2);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (19, 2);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (20, 2);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (21, 2);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (22, 2);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (23, 2);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (24, 2);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (25, 2);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (26, 2);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (27, 2);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (28, 2);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (29, 2);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (30, 2);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (31, 2);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (32, 2);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (33, 2);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (34, 2);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (35, 2);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (36, 2);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (37, 2);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (38, 2);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (39, 2);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (40, 2);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (41, 2);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (42, 2);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (43, 2);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (44, 2);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (45, 2);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (46, 2);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (47, 2);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (48, 2);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (49, 2);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (50, 2);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (51, 2);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (52, 2);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (53, 2);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (54, 2);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (55, 2);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (56, 2);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (57, 2);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (58, 2);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (59, 2);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (60, 2);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (61, 2);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (62, 2);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (63, 2);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (64, 2);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (65, 2);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (66, 2);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (67, 2);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (68, 2);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (69, 2);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (70, 2);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (71, 2);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (72, 2);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (73, 2);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (74, 2);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (75, 2);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (76, 2);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (77, 2);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (78, 2);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (79, 2);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (80, 2);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (81, 2);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (82, 2);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (83, 2);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (84, 2);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (85, 2);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (86, 2);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (87, 2);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (88, 2);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (89, 2);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (90, 2);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (91, 2);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (92, 2);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (93, 2);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (94, 2);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (95, 2);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (96, 2);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (97, 2);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (98, 2);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (99, 2);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (100, 2);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (1, 3);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (2, 3);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (3, 3);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (4, 3);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (5, 3);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (6, 3);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (7, 3);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (8, 3);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (9, 3);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (10, 3);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (11, 3);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (12, 3);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (13, 3);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (14, 3);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (15, 3);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (16, 3);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (17, 3);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (18, 3);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (19, 3);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (20, 3);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (21, 3);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (22, 3);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (23, 3);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (24, 3);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (25, 3);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (26, 3);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (27, 3);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (28, 3);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (29, 3);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (30, 3);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (31, 3);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (32, 3);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (33, 3);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (34, 3);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (35, 3);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (36, 3);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (37, 3);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (38, 3);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (39, 3);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (40, 3);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (41, 3);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (42, 3);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (43, 3);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (44, 3);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (45, 3);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (46, 3);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (47, 3);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (48, 3);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (49, 3);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (50, 3);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (51, 3);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (52, 3);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (53, 3);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (54, 3);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (55, 3);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (56, 3);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (57, 3);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (58, 3);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (59, 3);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (60, 3);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (61, 3);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (62, 3);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (63, 3);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (64, 3);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (65, 3);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (66, 3);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (67, 3);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (68, 3);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (69, 3);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (70, 3);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (71, 3);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (72, 3);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (73, 3);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (74, 3);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (75, 3);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (76, 3);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (77, 3);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (78, 3);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (79, 3);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (80, 3);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (81, 3);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (82, 3);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (83, 3);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (84, 3);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (85, 3);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (86, 3);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (87, 3);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (88, 3);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (89, 3);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (90, 3);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (91, 3);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (92, 3);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (93, 3);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (94, 3);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (95, 3);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (96, 3);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (97, 3);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (98, 3);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (99, 3);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (100, 3);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (1, 4);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (2, 4);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (3, 4);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (4, 4);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (5, 4);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (6, 4);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (7, 4);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (8, 4);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (9, 4);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (10, 4);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (11, 4);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (12, 4);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (13, 4);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (14, 4);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (15, 4);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (16, 4);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (17, 4);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (18, 4);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (19, 4);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (20, 4);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (21, 4);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (22, 4);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (23, 4);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (24, 4);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (25, 4);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (26, 4);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (27, 4);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (28, 4);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (29, 4);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (30, 4);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (31, 4);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (32, 4);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (33, 4);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (34, 4);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (35, 4);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (36, 4);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (37, 4);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (38, 4);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (39, 4);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (40, 4);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (41, 4);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (42, 4);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (43, 4);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (44, 4);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (45, 4);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (46, 4);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (47, 4);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (48, 4);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (49, 4);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (50, 4);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (51, 4);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (52, 4);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (53, 4);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (54, 4);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (55, 4);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (56, 4);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (57, 4);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (58, 4);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (59, 4);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (60, 4);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (61, 4);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (62, 4);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (63, 4);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (64, 4);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (65, 4);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (66, 4);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (67, 4);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (68, 4);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (69, 4);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (70, 4);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (71, 4);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (72, 4);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (73, 4);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (74, 4);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (75, 4);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (76, 4);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (77, 4);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (78, 4);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (79, 4);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (80, 4);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (81, 4);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (82, 4);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (83, 4);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (84, 4);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (85, 4);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (86, 4);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (87, 4);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (88, 4);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (89, 4);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (90, 4);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (91, 4);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (92, 4);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (93, 4);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (94, 4);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (95, 4);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (96, 4);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (97, 4);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (98, 4);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (99, 4);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (100, 4);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (101, 1);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (102, 1);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (103, 1);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (104, 1);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (105, 1);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (106, 1);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (107, 1);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (108, 1);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (109, 1);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (110, 1);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (111, 1);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (112, 1);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (113, 1);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (114, 1);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (115, 2);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (115, 3);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (115, 4);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (115, 1);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (52, 1);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (53, 1);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (54, 1);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (55, 1);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (56, 1);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (57, 1);
INSERT INTO public.role_has_permissions (permission_id, role_id) VALUES (58, 1);


--
-- TOC entry 3232 (class 0 OID 123094)
-- Dependencies: 253
-- Data for Name: roles; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.roles (id, name, guard_name, created_at, updated_at) VALUES (1, 'Administrator', 'admin', '2020-05-03 19:15:28', '2020-05-03 19:15:28');
INSERT INTO public.roles (id, name, guard_name, created_at, updated_at) VALUES (2, 'coop', 'admin', NULL, NULL);
INSERT INTO public.roles (id, name, guard_name, created_at, updated_at) VALUES (3, 'ama', 'admin', NULL, NULL);
INSERT INTO public.roles (id, name, guard_name, created_at, updated_at) VALUES (4, 'atc', 'admin', NULL, NULL);


--
-- TOC entry 3233 (class 0 OID 123101)
-- Dependencies: 254
-- Data for Name: states; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.states (id, name, created_at, updated_at) VALUES (1, 'Test', NULL, NULL);


--
-- TOC entry 3234 (class 0 OID 123105)
-- Dependencies: 255
-- Data for Name: statuses; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.statuses (id, name, created_at, updated_at, applicant) VALUES (3, 'Documento Ingresado', NULL, NULL, false);
INSERT INTO public.statuses (id, name, created_at, updated_at, applicant) VALUES (4, 'Solicitud completa', NULL, NULL, false);
INSERT INTO public.statuses (id, name, created_at, updated_at, applicant) VALUES (2, 'Actualización de Cuestionario', NULL, NULL, false);
INSERT INTO public.statuses (id, name, created_at, updated_at, applicant) VALUES (7, 'Asignación de Nro de Expediente', NULL, NULL, false);
INSERT INTO public.statuses (id, name, created_at, updated_at, applicant) VALUES (8, 'Asignación de ATC', NULL, NULL, false);
INSERT INTO public.statuses (id, name, created_at, updated_at, applicant) VALUES (9, 'Asiganción de Resolución', NULL, NULL, false);
INSERT INTO public.statuses (id, name, created_at, updated_at, applicant) VALUES (6, 'Calificación Final', NULL, NULL, false);
INSERT INTO public.statuses (id, name, created_at, updated_at, applicant) VALUES (5, 'Actualizacion de Calificación', NULL, NULL, false);
INSERT INTO public.statuses (id, name, created_at, updated_at, applicant) VALUES (10, 'Análisis ATC', NULL, NULL, false);
INSERT INTO public.statuses (id, name, created_at, updated_at, applicant) VALUES (1, 'Solicitud ingresada', NULL, '2020-06-04 14:16:53', true);


--
-- TOC entry 3235 (class 0 OID 123110)
-- Dependencies: 256
-- Data for Name: translations; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (284, '*', 'admin', 'applicant-processed.title', '{"en": "admin.applicant-processed.title", "es": "Procesados"}', NULL, '2020-09-09 11:51:52', '2020-09-09 11:52:20', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (267, '*', 'admin', 'driver.columns.received_at', '{"en": "Received at", "es": "Recepcionado"}', NULL, '2020-05-27 21:53:35', '2020-05-27 21:55:52', '2020-05-27 21:55:52');
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (63, '*', 'admin', 'applicant.columns.state_id', '{"en": "State", "es": "Departamento"}', NULL, '2020-05-03 19:18:29', '2020-05-19 00:09:27', '2020-05-19 00:09:27');
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (240, '*', 'admin', 'applicant.columns.beneficiaries', '{"en": "Beneficiaries", "es": "Beneficiarios"}', NULL, '2020-05-13 07:31:23', '2020-05-26 17:53:53', '2020-05-26 17:53:53');
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (273, '*', 'admin', 'sidebar.reports.general', '{"en": "General", "es": "General"}', NULL, '2020-05-29 10:12:59', '2020-05-29 10:16:20', '2020-05-29 10:16:20');
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (239, '*', 'admin', 'applicant.columns.comment', '{"en": "Comment", "es": "Observación"}', NULL, '2020-05-13 07:31:23', '2020-05-21 08:08:11', '2020-05-21 08:08:11');
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (281, '*', 'admin', 'reports.actions.applicants-coop-status', '{"en": "admin.reports.actions.applicants-coop-status", "es": "Reporte estados de postulantes"}', NULL, '2020-07-30 14:31:30', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (282, '*', 'admin', 'reports.actions.applicants-by-resolution-social', '{"en": "admin.reports.actions.applicants-by-resolution-social", "es": "Reporte del Área Social"}', NULL, '2020-07-30 14:31:30', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (206, '*', '*', 'Reset Password', '{"en": "Reset Password", "es": "Restablecer la contraseña"}', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (207, '*', '*', 'Send Password Reset Link', '{"en": "Send Password Reset Link", "es": "Enviar enlace de restablecimiento de contraseña"}', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (209, '*', '*', 'Name', '{"en": "Name", "es": "Nombre"}', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (210, '*', '*', 'Verify Your Email Address', '{"en": "Verify Your Email Address", "es": "Verifique su dirección de correo electrónico"}', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (211, '*', '*', 'A fresh verification link has been sent to your email address.', '{"en": "A fresh verification link has been sent to your email address.", "es": "Se ha enviado un nuevo enlace de verificación a su dirección de correo electrónico."}', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (212, '*', '*', 'Before proceeding, please check your email for a verification link.', '{"en": "Before proceeding, please check your email for a verification link.", "es": "Antes de continuar, revise su correo electrónico para obtener un enlace de verificación."}', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (213, '*', '*', 'If you did not receive the email', '{"en": "If you did not receive the email", "es": "Si no recibiste el correo electrónico"}', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (214, '*', '*', 'click here to request another', '{"en": "click here to request another", "es": "haga clic aquí para solicitar"}', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (215, '*', '*', 'Toggle navigation', '{"en": "Toggle navigation", "es": "Navegación"}', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (216, '*', '*', 'Logout', '{"en": "Logout", "es": "Salir"}', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (217, '*', '*', 'Close', '{"en": "Close", "es": "Cerrar"}', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (54, '*', 'admin', 'applicant.columns.parent_applicant', '{"en": "Parent applicant", "es": "Postulante relacionado"}', NULL, '2020-05-03 19:18:29', '2020-05-19 02:15:58', '2020-05-19 02:15:58');
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (223, '*', 'admin', 'applicant-document.columns.id', '[]', NULL, '2020-05-06 09:18:13', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (49, '*', 'admin', 'applicant.columns.gender', '{"en": "Gender", "es": "Género"}', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (92, '*', 'admin', 'education-level.columns.id', '{"en": "ID", "es": "ID"}', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (279, 'brackets/admin-ui', 'admin', 'placeholder.search-by-document', '{"en": "Search by document or file number", "es": "Buscar por documento o nro. de expediente"}', NULL, '2020-06-05 10:07:07', '2020-07-30 14:31:29', '2020-07-30 14:31:29');
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (1, 'brackets/admin-ui', 'admin', 'operation.succeeded', '{"en": "Operation successful", "es": "Operación exitosa!"}', NULL, '2020-05-03 19:18:28', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (2, 'brackets/admin-ui', 'admin', 'operation.failed', '{"en": "Operation failed", "es": "Operación fallida"}', NULL, '2020-05-03 19:18:28', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (3, 'brackets/admin-ui', 'admin', 'operation.not_allowed', '{"en": "Operation not allowed", "es": "Operación no permitida"}', NULL, '2020-05-03 19:18:28', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (18, '*', 'admin', 'admin-user.actions.index', '{"en": "Users", "es": "Usuarios"}', NULL, '2020-05-03 19:18:28', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (4, '*', 'admin', 'admin-user.columns.first_name', '{"en": "First name", "es": "Nombre"}', NULL, '2020-05-03 19:18:28', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (5, '*', 'admin', 'admin-user.columns.last_name', '{"en": "Last name", "es": "Apellido"}', NULL, '2020-05-03 19:18:28', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (6, '*', 'admin', 'admin-user.columns.email', '[]', NULL, '2020-05-03 19:18:28', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (7, '*', 'admin', 'admin-user.columns.password', '{"en": "Password", "es": "Contraseña"}', NULL, '2020-05-03 19:18:28', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (8, '*', 'admin', 'admin-user.columns.password_repeat', '{"en": "Password Confirmation", "es": "Confirmación de contraseña"}', NULL, '2020-05-03 19:18:28', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (9, '*', 'admin', 'admin-user.columns.activated', '{"en": "Activated", "es": "Activado"}', NULL, '2020-05-03 19:18:28', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (10, '*', 'admin', 'admin-user.columns.forbidden', '{"en": "Forbidden", "es": "Prohibido"}', NULL, '2020-05-03 19:18:28', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (11, '*', 'admin', 'admin-user.columns.language', '{"en": "Language", "es": "Idioma"}', NULL, '2020-05-03 19:18:28', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (12, 'brackets/admin-ui', 'admin', 'forms.select_an_option', '{"en": "Select an option", "es": "Selecciona una opción"}', NULL, '2020-05-03 19:18:28', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (13, '*', 'admin', 'admin-user.columns.roles', '[]', NULL, '2020-05-03 19:18:28', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (14, 'brackets/admin-ui', 'admin', 'forms.select_options', '{"en": "Select options", "es": "Seleccionar opción"}', NULL, '2020-05-03 19:18:28', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (15, '*', 'admin', 'admin-user.actions.create', '{"en": "New User", "es": "Nuevo Usuario"}', NULL, '2020-05-03 19:18:28', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (16, 'brackets/admin-ui', 'admin', 'btn.save', '{"en": "Save", "es": "Grabar"}', NULL, '2020-05-03 19:18:28', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (17, '*', 'admin', 'admin-user.actions.edit', '{"en": "Edit :name", "es": "Editar :name"}', NULL, '2020-05-03 19:18:28', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (19, 'brackets/admin-ui', 'admin', 'placeholder.search', '{"en": "Search", "es": "Buscar"}', NULL, '2020-05-03 19:18:28', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (20, 'brackets/admin-ui', 'admin', 'btn.search', '{"en": "Search", "es": "Buscar"}', NULL, '2020-05-03 19:18:28', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (21, '*', 'admin', 'admin-user.columns.id', '[]', NULL, '2020-05-03 19:18:28', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (22, 'brackets/admin-ui', 'admin', 'btn.edit', '{"en": "Edit", "es": "Editar"}', NULL, '2020-05-03 19:18:28', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (42, '*', 'admin', 'applicant.columns.disability', '{"en": "Disability", "es": "Discapacidades"}', NULL, '2020-05-03 19:18:29', '2020-07-30 14:31:29', '2020-07-30 14:31:29');
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (44, '*', 'admin', 'applicant.columns.disease', '{"en": "Disease", "es": "Enfermedad"}', NULL, '2020-05-03 19:18:29', '2020-07-30 14:31:29', '2020-07-30 14:31:29');
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (23, 'brackets/admin-ui', 'admin', 'btn.delete', '{"en": "Delete", "es": "Borrar"}', NULL, '2020-05-03 19:18:28', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (24, 'brackets/admin-ui', 'admin', 'pagination.overview', '{"en": "Displaying items from {{ pagination.state.from }} to {{ pagination.state.to }} of total {{ pagination.state.total }} items.", "es": "Registros desde {{ pagination.state.from }} a {{ pagination.state.to }} de un total de {{ pagination.state.total }} registros."}', NULL, '2020-05-03 19:18:28', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (25, 'brackets/admin-ui', 'admin', 'index.no_items', '{"en": "Could not find any items", "es": "No se pudo encontrar ningún registro"}', NULL, '2020-05-03 19:18:28', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (26, 'brackets/admin-ui', 'admin', 'index.try_changing_items', '{"en": "Try changing the filters or add a new one", "es": "Cambia los filtros o agrega un registro nuevo"}', NULL, '2020-05-03 19:18:28', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (27, 'brackets/admin-ui', 'admin', 'btn.new', '{"en": "New", "es": "Nuevo"}', NULL, '2020-05-03 19:18:28', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (218, '*', 'admin', 'applicant-document.columns.applicant_id', '{"en": "Applicant", "es": "Postulante"}', NULL, '2020-05-06 09:18:13', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (219, '*', 'admin', 'applicant-document.columns.document_id', '{"en": "Document", "es": "Documento"}', NULL, '2020-05-06 09:18:13', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (238, '*', 'admin', 'documents.actions.search', '{"en": "Search", "es": "Buscar"}', NULL, '2020-05-13 07:31:23', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (268, '*', 'admin', 'applicant-document.columns.received_at', '{"en": "Received at", "es": "Recepcionado en"}', NULL, '2020-05-27 21:55:52', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (51, 'brackets/admin-ui', 'admin', 'forms.select_date_and_time', '{"en": "Select date and time", "es": "Selecciona fecha y hora"}', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (220, '*', 'admin', 'applicant-document.actions.create', '{"en": "New Document", "es": "Nuevo Documento"}', NULL, '2020-05-06 09:18:13', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (221, '*', 'admin', 'applicant-document.actions.edit', '{"en": "Edit :name", "es": "Editar :name"}', NULL, '2020-05-06 09:18:13', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (222, '*', 'admin', 'applicant-document.actions.index', '{"en": "Applicants Supporing Documents", "es": "Documentos de Postulantes"}', NULL, '2020-05-06 09:18:13', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (33, 'brackets/admin-ui', 'admin', 'listing.selected_items', '{"en": "Selected items", "es": "Items seleccionados"}', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (34, 'brackets/admin-ui', 'admin', 'listing.check_all_items', '{"en": "Check all items", "es": "Selecciona todo"}', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (35, 'brackets/admin-ui', 'admin', 'listing.uncheck_all_items', '{"en": "Uncheck all items", "es": "Deselecciona todo"}', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (28, '*', 'admin', 'applicant-relationship.columns.name', '{"en": "Name", "es": "Nombre"}', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (29, '*', 'admin', 'applicant-relationship.actions.create', '{"en": "New relationship", "es": "Nueva relación"}', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (30, '*', 'admin', 'applicant-relationship.actions.edit', '{"en": "Editar relationship", "es": "Editar relación"}', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (31, '*', 'admin', 'applicant-relationship.actions.index', '{"en": "Relationships", "es": "Relaciones"}', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (32, '*', 'admin', 'applicant-relationship.columns.id', '{"en": "ID", "es": "ID"}', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (37, '*', 'admin', 'applicant.actions.search', '{"en": "Search", "es": "Buscar"}', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (55, '*', 'admin', 'applicant.columns.applicant_relationship', '{"en": "Relationship type", "es": "Tipo de relación"}', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (47, '*', 'admin', 'applicant.columns.government_id', '{"en": "Document", "es": "Documento"}', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (45, '*', 'admin', 'applicant.columns.names', '{"en": "Name", "es": "Nombre"}', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (46, '*', 'admin', 'applicant.columns.last_names', '{"en": "Last name", "es": "Apellido"}', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (226, '*', 'admin', 'applicant.columns.nationality', '{"en": "Nationality", "es": "Nacionalidad"}', NULL, '2020-05-06 09:18:13', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (48, '*', 'admin', 'applicant.columns.marital_status', '{"en": "Marital status", "es": "Estado civil"}', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (50, '*', 'admin', 'applicant.columns.birthdate', '{"en": "Birthdate", "es": "Nacimiento"}', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (65, '*', 'admin', 'applicant.columns.pregnant', '{"en": "Pregnant?", "es": "Embarazada?"}', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (66, '*', 'admin', 'applicant.columns.pregnancy_due_date', '{"en": "Pregnancy due date", "es": "Tiempo embarazo (semanas)"}', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (56, '*', 'admin', 'applicant.columns.education_level', '{"en": "Education level", "es": "Nivel de educación"}', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (57, '*', 'admin', 'applicant.columns.occupation', '{"en": "Occupation", "es": "Profesión"}', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (58, '*', 'admin', 'applicant.columns.monthly_income', '{"en": "Monthly income", "es": "Ingreso mensual"}', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (36, '*', 'admin', 'contact-method.title', '{"en": "Contact methods", "es": "Métodos de contacto"}', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (38, '*', 'admin', 'applicant.columns.contact-method', '{"en": "Contact method", "es": "Método de contacto"}', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (39, '*', 'admin', 'applicant.columns.contact-description', '{"en": "Description", "es": "Descripción"}', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (40, 'brackets/admin-ui', 'admin', 'btn.add', '{"en": "Add", "es": "Agregar"}', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (41, '*', 'admin', 'disability.title', '{"en": "Disabilities", "es": "Discapacidades"}', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (43, '*', 'admin', 'disease.title', '{"en": "Diseases", "es": "Enfermedades"}', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (224, '*', 'admin', 'applicant.columns.address', '{"en": "Address", "es": "Dirección"}', NULL, '2020-05-06 09:18:13', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (225, '*', 'admin', 'applicant.columns.neighborhood', '{"en": "Neighborhood", "es": "Barrio"}', NULL, '2020-05-06 09:18:13', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (64, '*', 'admin', 'applicant.columns.city_id', '{"en": "City", "es": "Ciudad"}', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (52, '*', 'admin', 'applicant.columns.property_id', '{"en": "Property id", "es": "Finca"}', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (53, '*', 'admin', 'applicant.columns.cadaster', '{"en": "Cadaster", "es": "Cta Catastral"}', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (59, '*', 'admin', 'applicant.actions.create', '{"en": "New applicant", "es": "Nuevo postulante"}', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (228, '*', 'admin', 'beneficiary.actions.create', '{"en": "New beneficiary", "es": "Nuevo Beneficiario"}', NULL, '2020-05-06 09:18:13', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (260, '*', 'admin', 'beneficiary.actions.edit', '{"en": "Beneficiary edit", "es": "Editar beneficiario"}', NULL, '2020-05-19 02:15:58', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (60, '*', 'admin', 'applicant.actions.edit', '{"en": "Edit applicant", "es": "Editar postulante"}', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (227, '*', 'admin', 'beneficiary.actions.index', '{"en": "Beneficiaries", "es": "Beneficiarios"}', NULL, '2020-05-06 09:18:13', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (62, '*', 'admin', 'applicant.columns.id', '{"en": "ID", "es": "ID"}', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (229, '*', 'admin', 'applicant.actions.questions', '{"en": "Questionnaire", "es": "Cuestionario"}', NULL, '2020-05-06 09:18:13', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (230, '*', 'admin', 'document-type.columns.name', '{"en": "Name", "es": "Nombre"}', NULL, '2020-05-06 09:18:13', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (276, 'brackets/admin-ui', 'admin', 'btn.document-view', '{"en": "View document", "es": "Ver documento"}', NULL, '2020-06-01 20:08:36', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (261, '*', 'admin', 'applicant.actions.final', '{"en": "Final Evaluation", "es": "Calificación Final"}', NULL, '2020-05-21 08:08:12', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (262, '*', 'admin', 'applicant.actions.finalizar', '{"en": "Confirm", "es": "Finalizar"}', NULL, '2020-05-21 08:53:21', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (61, '*', 'admin', 'applicant.actions.index', '{"en": "Applicants", "es": "Postulantes"}', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (247, '*', 'admin', 'applicant.actions.export', '{"en": "Export", "es": "Exportar"}', NULL, '2020-05-13 19:16:34', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (266, '*', 'admin', 'applicant.columns.beneficiaries_short', '{"en": "Benef.", "es": "Benef."}', NULL, '2020-05-26 17:53:53', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (241, '*', 'admin', 'applicant.columns.sum', '{"en": "Family Income", "es": "Ingreso familiar"}', NULL, '2020-05-13 07:31:23', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (263, '*', 'admin', 'applicant.columns.financial_entity', '{"en": "Financial", "es": "Cooperativa"}', NULL, '2020-05-26 10:07:00', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (264, '*', 'admin', 'applicant.columns.construction_entity', '{"en": "Constructor", "es": "ATC"}', NULL, '2020-05-26 10:07:00', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (67, '*', 'admin', 'city.columns.name', '{"en": "Name", "es": "Nombre"}', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (68, '*', 'admin', 'city.columns.state_id', '{"en": "State", "es": "Departamento"}', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (69, '*', 'admin', 'city.actions.create', '{"en": "New city", "es": "Nueva ciudad"}', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (70, '*', 'admin', 'city.actions.edit', '{"en": "Edit city", "es": "Editar ciudad"}', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (71, '*', 'admin', 'city.actions.index', '{"en": "Cities", "es": "Ciudades"}', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (72, '*', 'admin', 'city.columns.id', '{"en": "ID", "es": "ID"}', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (248, '*', 'admin', 'construction-entity.columns.name', '{"en": "Name", "es": "Nombre"}', NULL, '2020-05-19 00:09:27', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (249, '*', 'admin', 'construction-entity.actions.create', '{"en": "New ATC", "es": "Nueva ATC"}', NULL, '2020-05-19 00:09:27', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (250, '*', 'admin', 'construction-entity.actions.edit', '{"en": "Edit :name", "es": "Editar :name"}', NULL, '2020-05-19 00:09:27', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (251, '*', 'admin', 'construction-entity.actions.index', '{"en": "ATC", "es": "ATC"}', NULL, '2020-05-19 00:09:27', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (252, '*', 'admin', 'construction-entity.columns.id', '[]', NULL, '2020-05-19 00:09:27', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (73, '*', 'admin', 'contact-method.columns.name', '{"en": "Name", "es": "Nombre"}', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (74, '*', 'admin', 'contact-method.actions.create', '{"en": "New contact method", "es": "Nuevo método de contacto"}', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (75, '*', 'admin', 'contact-method.actions.edit', '{"en": "Edit contact method", "es": "Editar método de contacto"}', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (76, '*', 'admin', 'contact-method.actions.index', '{"en": "Contact methods", "es": "Métodos de contacto"}', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (77, '*', 'admin', 'contact-method.columns.id', '{"en": "ID", "es": "ID"}', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (78, '*', 'admin', 'disability.columns.name', '{"en": "Name", "es": "Nombre"}', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (79, '*', 'admin', 'disability.actions.create', '{"en": "New disability", "es": "Nueva discapacidad"}', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (80, '*', 'admin', 'disability.actions.edit', '{"en": "Edit disability", "es": "Editar discapacidad"}', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (81, '*', 'admin', 'disability.actions.index', '{"en": "Disabilities", "es": "Discapacidades"}', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (82, '*', 'admin', 'disability.columns.id', '{"en": "ID", "es": "ID"}', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (83, '*', 'admin', 'disease.columns.name', '{"en": "Name", "es": "Nombre"}', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (84, '*', 'admin', 'disease.actions.create', '{"en": "New disease", "es": "Nueva enfermedad"}', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (85, '*', 'admin', 'disease.actions.edit', '{"en": "Edit disease", "es": "Editar enfermedad"}', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (86, '*', 'admin', 'disease.actions.index', '{"en": "Diseases", "es": "Enfermedades"}', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (87, '*', 'admin', 'disease.columns.id', '{"en": "ID", "es": "ID"}', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (231, '*', 'admin', 'document-type.columns.enabled', '{"en": "Enabled", "es": "Habilitado"}', NULL, '2020-05-06 09:18:13', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (232, '*', 'admin', 'document-type.columns.type', '{"en": "Type", "es": "Tipo"}', NULL, '2020-05-06 09:18:13', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (233, '*', 'admin', 'document-type.actions.create', '{"en": "New Document Type", "es": "Nuevo tipo de documento"}', NULL, '2020-05-06 09:18:13', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (234, '*', 'admin', 'document-type.actions.edit', '{"en": "Edit :name", "es": "Editar :name"}', NULL, '2020-05-06 09:18:13', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (235, '*', 'admin', 'document-type.actions.index', '{"en": "Document Types", "es": "Tipos de documentos"}', NULL, '2020-05-06 09:18:13', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (236, '*', 'admin', 'document-type.columns.id', '[]', NULL, '2020-05-06 09:18:13', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (88, '*', 'admin', 'education-level.columns.name', '{"en": "Name", "es": "Nombre"}', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (89, '*', 'admin', 'education-level.actions.create', '{"en": "New education level", "es": "Nuevo nivel de educación"}', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (90, '*', 'admin', 'education-level.actions.edit', '{"en": "Edit education level", "es": "Editar nuevo nivel de educación"}', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (91, '*', 'admin', 'education-level.actions.index', '{"en": "Education levels", "es": "Niveles de Educación"}', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (253, '*', 'admin', 'financial-entity.columns.name', '{"en": "Name", "es": "Nombre"}', NULL, '2020-05-19 00:09:27', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (254, '*', 'admin', 'financial-entity.actions.create', '{"en": "New Financial Entity", "es": "Nueva Cooperativa"}', NULL, '2020-05-19 00:09:27', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (255, '*', 'admin', 'financial-entity.actions.edit', '{"en": "Edit :name", "es": "Editar :name"}', NULL, '2020-05-19 00:09:27', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (256, '*', 'admin', 'financial-entity.actions.index', '{"en": "Financial Entities", "es": "Cooperativas"}', NULL, '2020-05-19 00:09:27', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (257, '*', 'admin', 'financial-entity.columns.id', '[]', NULL, '2020-05-19 00:09:27', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (93, 'brackets/admin-ui', 'admin', 'profile_dropdown.account', '{"en": "Account", "es": "Cuenta"}', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (94, 'brackets/admin-auth', 'admin', 'profile_dropdown.logout', '{"en": "Logout", "es": "Salir"}', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (95, 'brackets/admin-ui', 'admin', 'sidebar.content', '{"en": "Content", "es": "Contenido"}', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (96, '*', 'admin', 'applicant.title', '{"en": "Applicants", "es": "Postulantes"}', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (272, '*', 'admin', 'sidebar.reports', '{"en": "Reports", "es": "Reportes"}', NULL, '2020-05-29 10:12:59', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (274, '*', 'admin', 'sidebar.general', '{"en": "General", "es": "General"}', NULL, '2020-05-29 10:16:20', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (269, '*', 'admin', 'reports.actions.applicants-by-resolution', '{"en": "Final Qualification List", "es": "Listado de Calificación Final"}', NULL, '2020-05-29 10:05:18', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (97, 'brackets/admin-ui', 'admin', 'sidebar.settings', '{"en": "Settings", "es": "Configuraciones"}', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (98, '*', 'admin', 'sidebar.settings.general', '{"en": "General", "es": "General"}', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (258, '*', 'admin', 'construction-entity.title', '{"en": "ATC", "es": "ATC"}', NULL, '2020-05-19 00:09:27', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (259, '*', 'admin', 'financial-entity.title', '{"en": "Financial Entities", "es": "Cooperativas"}', NULL, '2020-05-19 00:09:27', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (99, '*', 'admin', 'state.title', '{"en": "States", "es": "Departamentos"}', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (100, '*', 'admin', 'city.title', '{"en": "Cities", "es": "Ciudades"}', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (237, '*', 'admin', 'document-type.title', '{"en": "Document Types", "es": "Tipos de documentos"}', NULL, '2020-05-06 09:18:13', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (101, '*', 'admin', 'education-level.title', '{"en": "Education levels", "es": "Niveles de educación"}', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (102, '*', 'admin', 'applicant-relationship.title', '{"en": "Relationships", "es": "Relaciones"}', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (103, '*', 'admin', 'status.title', '{"en": "Statuses", "es": "Estados"}', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (104, '*', 'admin', 'questionnaire-type.title', '{"en": "Questionnaire types", "es": "Tipos de cuestionario"}', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (105, '*', 'admin', 'questionnaire-template.title', '{"en": "Questionnaire templates", "es": "Cuestionarios"}', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (106, 'brackets/admin-ui', 'admin', 'sidebar.access', '{"en": "Access", "es": "Accesos"}', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (107, '*', 'admin', 'admin-user.actions.edit_password', '{"en": "Edit Password", "es": "Editar contraseña"}', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (108, '*', 'admin', 'admin-user.actions.edit_profile', '{"en": "Edit Profile", "es": "Editar Perfil"}', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (265, '*', 'admin', 'admin-user.columns.phone', '{"en": "Phone", "es": "Teléfono"}', NULL, '2020-05-26 10:07:00', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (109, '*', 'admin', 'questionnaire-template.columns.name', '{"en": "Name", "es": "Nombre"}', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (110, '*', 'admin', 'questionnaire-template.columns.enabled', '{"en": "Enabled", "es": "Habilitado"}', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (111, '*', 'admin', 'questionnaire-template.columns.questionnaire_type_id', '{"en": "ID", "es": "ID"}', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (112, '*', 'admin', 'questionnaire-template.actions.create', '{"en": "New questionnaire template", "es": "Nuevo cuestionario"}', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (113, '*', 'admin', 'questionnaire-template.actions.edit', '{"en": "Edit questionnaire template", "es": "Editar cuestionario"}', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (114, '*', 'admin', 'questionnaire-template.actions.index', '{"en": "Questionnaire templates", "es": "Cuestionarios"}', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (115, '*', 'admin', 'questionnaire-template.columns.id', '{"en": "ID", "es": "ID"}', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (116, '*', 'admin', 'questionnaire-type.columns.name', '{"en": "Name", "es": "Nombre"}', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (117, '*', 'admin', 'questionnaire-type.actions.create', '{"en": "New questionnaire type", "es": "Nuevo tipo de cuestionario"}', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (118, '*', 'admin', 'questionnaire-type.actions.edit', '{"en": "Edit questionnaire type", "es": "Editar tipo de cuestionario"}', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (119, '*', 'admin', 'questionnaire-type.actions.index', '{"en": "Questionnaire type", "es": "Tipos de cuestionario"}', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (120, '*', 'admin', 'questionnaire-type.columns.id', '{"en": "id", "es": "id"}', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (283, '*', 'admin', 'applicant.actions.social', '[]', NULL, '2020-07-30 14:31:31', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (275, 'brackets/admin-ui', 'admin', 'placeholder.search-by-resolution', '{"en": "Search by resolution", "es": "Buscar por resolución"}', NULL, '2020-05-29 10:42:16', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (121, '*', 'admin', 'state.columns.name', '{"en": "Name", "es": "Nombre"}', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (122, '*', 'admin', 'state.actions.create', '{"en": "New state", "es": "Nuevo departamento"}', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (123, '*', 'admin', 'state.actions.edit', '{"en": "Edit state", "es": "Editar departamento"}', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (124, '*', 'admin', 'state.actions.index', '{"en": "States", "es": "Departamentos"}', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (125, '*', 'admin', 'state.columns.id', '{"en": "ID", "es": "ID"}', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (126, '*', 'admin', 'status.columns.name', '{"en": "Name", "es": "Nombre"}', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (277, '*', 'admin', 'status.columns.applicant', '{"en": "Show to applicant", "es": "Mostrar al postulante"}', NULL, '2020-06-04 14:19:10', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (127, '*', 'admin', 'status.actions.create', '{"en": "Statuses", "es": "Estados"}', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (128, '*', 'admin', 'status.actions.edit', '{"en": "Status", "es": "Estados"}', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (129, '*', 'admin', 'status.actions.index', '{"en": "Statuses", "es": "Estados"}', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (130, '*', 'admin', 'status.columns.id', '{"en": "ID", "es": "ID"}', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (278, '*', 'admin', 'applicant.actions.trakings', '{"en": "Application Tracking", "es": "Seguimiento de Postulación"}', NULL, '2020-06-04 19:30:24', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (131, 'brackets/admin-auth', 'admin', 'login.title', '{"en": "Login", "es": "Iniciar sesión"}', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (132, 'brackets/admin-auth', 'admin', 'login.sign_in_text', '{"en": "Sign In to your account", "es": "Iniciar sesión con su cuenta"}', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (133, 'brackets/admin-auth', 'admin', 'auth_global.email', '{"en": "Your e-mail", "es": "Tu correo electrónico"}', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (134, 'brackets/admin-auth', 'admin', 'auth_global.password', '{"en": "Password", "es": "Contraseña"}', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (135, 'brackets/admin-auth', 'admin', 'login.button', '{"en": "Login", "es": "Iniciar sesión"}', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (136, 'brackets/admin-auth', 'admin', 'login.forgot_password', '{"en": "Forgot password?", "es": "¿Se te olvidó tu contraseña?"}', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (137, 'brackets/admin-ui', 'admin', 'page_title_suffix', '{"en": "AMA", "es": "AMA"}', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (138, 'brackets/admin-auth', 'activations', 'email.line', '{"en": "You are receiving this email because we received an activation request for your account.", "es": "Recibió este correo electrónico porque recibimos una solicitud de activación para su cuenta."}', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (139, 'brackets/admin-auth', 'activations', 'email.action', '{"en": "Activate your account", "es": "Activa tu cuenta"}', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (140, 'brackets/admin-auth', 'activations', 'email.notRequested', '{"en": "If you did not request an activation, no further action is required.", "es": "Si no solicitó una activación, no se requiere ninguna otra acción."}', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (141, 'brackets/admin-auth', 'admin', 'activations.activated', '{"en": "Your account was activated!", "es": "Su cuenta fue activada!"}', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (142, 'brackets/admin-auth', 'admin', 'activations.invalid_request', '{"en": "The request failed.", "es": "La solicitud falló."}', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (143, 'brackets/admin-auth', 'admin', 'activations.disabled', '{"en": "Activation is disabled.", "es": "La activación está deshabilitada."}', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (144, 'brackets/admin-auth', 'admin', 'activations.sent', '{"en": "We have sent you an activation link!", "es": "Te hemos enviado un enlace de activación!"}', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (145, 'brackets/admin-auth', 'admin', 'passwords.sent', '{"en": "We have sent you a password reset link!", "es": "Le hemos enviado un enlace para restablecer la contraseña!"}', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (146, 'brackets/admin-auth', 'admin', 'passwords.reset', '{"en": "Your password has been reset!", "es": "Tu contraseña ha sido restablecida!"}', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (147, 'brackets/admin-auth', 'admin', 'passwords.invalid_token', '{"en": "The password reset token is invalid.", "es": "El token de restablecimiento de contraseña no es válido."}', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (148, 'brackets/admin-auth', 'admin', 'passwords.invalid_user', '{"en": "We can''t find a user with this e-mail address.", "es": "No podemos encontrar un usuario con esta dirección de correo electrónico."}', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (149, 'brackets/admin-auth', 'admin', 'passwords.invalid_password', '{"en": "Password must be at least six characters long and match the confirmation.", "es": "La contraseña debe tener al menos seis caracteres y coincidir con la confirmación."}', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (242, 'brackets/admin-auth', 'resets', 'email.line', '{"en": "You are receiving this email because we received a password reset request for your account.", "es": "Recibió este correo electrónico porque recibimos una solicitud de restablecimiento de contraseña para su cuenta."}', NULL, '2020-05-13 07:31:23', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (243, 'brackets/admin-auth', 'resets', 'email.action', '{"en": "Reset Password", "es": "Restablecimiento de contraseña"}', NULL, '2020-05-13 07:31:23', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (244, 'brackets/admin-auth', 'resets', 'email.notRequested', '{"en": "If you did not request a password reset, no further action is required.", "es": "Si no solicitó un restablecimiento de contraseña, no se requiere ninguna otra acción."}', NULL, '2020-05-13 07:31:23', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (245, '*', 'auth', 'failed', '{"en": "These credentials do not match our records.", "es": "Las credenciales no coinciden con nuestros registros"}', NULL, '2020-05-13 07:31:23', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (246, '*', 'auth', 'throttle', '{"en": "Too many login attempts. Please try again in :seconds seconds.", "es": "Se han realizado demasiados intentos de inicio de sesión. Pruebe de nuevo en :seconds segundos."}', NULL, '2020-05-13 07:31:23', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (150, 'brackets/admin-auth', 'admin', 'activation_form.title', '{"en": "Activate account", "es": "Activar la cuenta"}', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (151, 'brackets/admin-auth', 'admin', 'activation_form.note', '{"en": "Send activation link to e-mail", "es": "Enviar enlace de activación al correo electrónico"}', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (152, 'brackets/admin-auth', 'admin', 'activation_form.button', '{"en": "Send Activation Link", "es": "Enviar enlace de activación"}', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (153, 'brackets/admin-auth', 'admin', 'forgot_password.title', '{"en": "Reset Password", "es": "Restablecer la contraseña"}', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (154, 'brackets/admin-auth', 'admin', 'forgot_password.note', '{"en": "Send password reset e-mail", "es": "Enviar contraseña restablecer correo electrónico"}', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (155, 'brackets/admin-auth', 'admin', 'forgot_password.button', '{"en": "Send Password Reset Link", "es": "Enviar enlace de restablecimiento de contraseña"}', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (156, 'brackets/admin-auth', 'admin', 'password_reset.title', '{"en": "Reset Password", "es": "Restablecer la contraseña"}', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (157, 'brackets/admin-auth', 'admin', 'password_reset.note', '{"en": "Reset forgotten password", "es": "Restablecer contraseña olvidada"}', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (158, 'brackets/admin-auth', 'admin', 'auth_global.password_confirm', '{"en": "Password confirmation", "es": "Confirmación de contraseña"}', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (159, 'brackets/admin-auth', 'admin', 'password_reset.button', '{"en": "Reset password", "es": "Restablecer la contraseña"}', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (160, 'brackets/admin-ui', 'admin', 'media_uploader.max_number_of_files', '{"en": "(max no. of files: :maxNumberOfFiles files)", "es": "(Máximo nro de archivos: :maxNumberOfFiles archivos)"}', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (161, 'brackets/admin-ui', 'admin', 'media_uploader.max_size_pre_file', '{"en": "(max size per file: :maxFileSize MB)", "es": "(Máximo tamaño por archivo: :maxFileSize MB)"}', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (162, 'brackets/admin-ui', 'admin', 'media_uploader.private_title', '{"en": "Files are not accessible for public", "es": "Archivos no públicos"}', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (163, 'brackets/admin-ui', 'admin', 'footer.powered_by', '[]', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (164, 'brackets/admin-translations', 'admin', 'title', '{"en": "Translations", "es": "Traducciones"}', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (165, 'brackets/admin-translations', 'admin', 'index.all_groups', '{"en": "All groups", "es": "Todos los grupos"}', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (166, 'brackets/admin-translations', 'admin', 'index.edit', '{"en": "Edit translation", "es": "Editar traducción"}', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (167, 'brackets/admin-translations', 'admin', 'index.default_text', '{"en": "Default text", "es": "Texto predeterminado"}', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (168, 'brackets/admin-translations', 'admin', 'index.translation', '{"en": "translation", "es": "traducción"}', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (169, 'brackets/admin-translations', 'admin', 'index.translation_for_language', '{"en": "Type a translation for :locale language.", "es": "Escriba una traducción para :locale idioma."}', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (170, 'brackets/admin-translations', 'admin', 'import.title', '{"en": "Translations import", "es": "Importación de traducciones"}', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (171, 'brackets/admin-translations', 'admin', 'import.notice', '{"en": "You can import translations of a selected language from the .xslx file. Imported file must have identical structure as generated in Translations export.", "es": "Puede importar traducciones de un idioma seleccionado del archivo .xlsx. El archivo importado debe tener una estructura idéntica a la generada en la exportación de Traducciones."}', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (172, 'brackets/admin-translations', 'admin', 'import.upload_file', '{"en": "Upload File", "es": "Subir archivo"}', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (173, 'brackets/admin-translations', 'admin', 'import.choose_file', '{"en": "Choose file", "es": "Elija el archivo"}', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (174, 'brackets/admin-translations', 'admin', 'import.language_to_import', '{"en": "Language to import", "es": "Idioma para importar"}', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (175, 'brackets/admin-translations', 'admin', 'fields.select_language', '{"en": "Select language", "es": "Seleccione el idioma"}', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (176, 'brackets/admin-translations', 'admin', 'import.do_not_override', '{"en": "Do not override existing translations", "es": "No anule las traducciones existentes"}', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (177, 'brackets/admin-translations', 'admin', 'import.conflict_notice_we_have_found', '{"en": "We have found", "es": "No hemos encontrado"}', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (178, 'brackets/admin-translations', 'admin', 'import.conflict_notice_translations_to_be_imported', '{"en": "translations in total to be imported. Please review", "es": "traducciones en total a importar. Por favor revise"}', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (179, 'brackets/admin-translations', 'admin', 'import.conflict_notice_differ', '{"en": "translations that differs before continuing.", "es": "traducciones que difieren antes de continuar."}', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (180, 'brackets/admin-translations', 'admin', 'fields.group', '{"en": "Group", "es": "Grupo"}', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (181, 'brackets/admin-translations', 'admin', 'fields.default', '{"en": "Default", "es": "Por defecto"}', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (182, 'brackets/admin-translations', 'admin', 'fields.current_value', '{"en": "Current value", "es": "Valor actual"}', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (183, 'brackets/admin-translations', 'admin', 'fields.imported_value', '{"en": "Imported value", "es": "Valor importado"}', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (184, 'brackets/admin-translations', 'admin', 'import.sucesfully_notice', '{"en": "translations sucesfully imported", "es": "traducciones importadas con éxito"}', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (185, 'brackets/admin-translations', 'admin', 'import.sucesfully_notice_update', '{"en": "translations sucesfully updated.", "es": "Traducciones actualizadas con éxito."}', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (186, 'brackets/admin-translations', 'admin', 'index.export', '{"en": "Translations export", "es": "Exportación de traducciones"}', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (196, '*', '*', 'Translations', '{"en": "Translations", "es": "Traducciones"}', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (197, '*', '*', 'Manage access', '{"en": "Manage access", "es": "Administrar acceso"}', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (198, '*', '*', 'Configuration', '{"en": "Configuration", "es": "Configuración"}', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (199, '*', '*', 'Login', '{"en": "Login", "es": "Iniciar sesión"}', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (200, '*', '*', 'E-Mail Address', '{"en": "E-Mail Address", "es": "Dirección de correo electrónico"}', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (201, '*', '*', 'Password', '{"en": "Password", "es": "Contraseña"}', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (202, '*', '*', 'Remember Me', '{"en": "Remember Me", "es": "Recuérdame"}', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (203, '*', '*', 'Forgot Your Password?', '{"en": "Forgot Your Password?", "es": "¿Olvidaste tu contraseña?"}', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (204, '*', '*', 'Confirm Password', '{"en": "Confirm Password", "es": "Confirmar contraseña"}', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (205, '*', '*', 'Please confirm your password before continuing.', '{"en": "Please confirm your password before continuing.", "es": "Por favor confirme su contraseña antes de continuar."}', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (208, '*', '*', 'Register', '{"en": "Register", "es": "Registrarse"}', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (187, 'brackets/admin-translations', 'admin', 'export.notice', '{"en": "You can export translations of a selected language as .xslx file.", "es": "Puede exportar traducciones de un idioma seleccionado como archivo .xlsx."}', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (188, 'brackets/admin-translations', 'admin', 'export.language_to_export', '[]', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (189, 'brackets/admin-translations', 'admin', 'btn.export', '{"en": "Export", "es": "Exportar"}', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (190, 'brackets/admin-translations', 'admin', 'index.title', '{"en": "Translations list", "es": "Lista de traducciones"}', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (191, 'brackets/admin-translations', 'admin', 'btn.import', '{"en": "Import", "es": "importar"}', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (192, 'brackets/admin-translations', 'admin', 'btn.re_scan', '{"en": "Re-scan translations", "es": "Volver a escanear traducciones"}', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (193, 'brackets/admin-translations', 'admin', 'fields.created_at', '{"en": "Created at", "es": "Creado en:"}', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (194, 'brackets/admin-translations', 'admin', 'index.no_items', '{"en": "Could not find any translations", "es": "No se pudo encontrar ninguna traducción."}', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);
INSERT INTO public.translations (id, namespace, "group", key, text, metadata, created_at, updated_at, deleted_at) VALUES (195, 'brackets/admin-translations', 'admin', 'index.try_changing_items', '{"en": "Try changing the filters or re-scan", "es": "Intente cambiar los filtros o vuelva a escanear"}', NULL, '2020-05-03 19:18:29', '2020-09-09 11:51:52', NULL);


--
-- TOC entry 3236 (class 0 OID 123118)
-- Dependencies: 257
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 3237 (class 0 OID 123125)
-- Dependencies: 258
-- Data for Name: wysiwyg_media; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 3271 (class 0 OID 0)
-- Dependencies: 196
-- Name: admin_users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.admin_users_id_seq', 4, true);


--
-- TOC entry 3272 (class 0 OID 0)
-- Dependencies: 197
-- Name: applicant_documents_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.applicant_documents_id_seq', 62, true);


--
-- TOC entry 3273 (class 0 OID 0)
-- Dependencies: 198
-- Name: applicant_relationships_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.applicant_relationships_id_seq', 4, true);


--
-- TOC entry 3274 (class 0 OID 0)
-- Dependencies: 199
-- Name: applicant_statuses_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.applicant_statuses_id_seq', 27, true);


--
-- TOC entry 3275 (class 0 OID 0)
-- Dependencies: 200
-- Name: applicants_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.applicants_id_seq', 63, true);


--
-- TOC entry 3276 (class 0 OID 0)
-- Dependencies: 260
-- Name: assignment_histories_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.assignment_histories_seq', 16, true);


--
-- TOC entry 3277 (class 0 OID 0)
-- Dependencies: 201
-- Name: cities_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.cities_id_seq', 12, true);


--
-- TOC entry 3278 (class 0 OID 0)
-- Dependencies: 202
-- Name: construction_entities_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.construction_entities_id_seq', 29, true);


--
-- TOC entry 3279 (class 0 OID 0)
-- Dependencies: 203
-- Name: contact_methods_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.contact_methods_id_seq', 7, true);


--
-- TOC entry 3280 (class 0 OID 0)
-- Dependencies: 204
-- Name: disabilities_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.disabilities_id_seq', 5, true);


--
-- TOC entry 3281 (class 0 OID 0)
-- Dependencies: 205
-- Name: diseases_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.diseases_id_seq', 5, true);


--
-- TOC entry 3282 (class 0 OID 0)
-- Dependencies: 206
-- Name: document_types_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.document_types_id_seq', 2, false);


--
-- TOC entry 3283 (class 0 OID 0)
-- Dependencies: 207
-- Name: education_levels_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.education_levels_id_seq', 7, true);


--
-- TOC entry 3284 (class 0 OID 0)
-- Dependencies: 208
-- Name: failed_jobs_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.failed_jobs_id_seq', 2, false);


--
-- TOC entry 3285 (class 0 OID 0)
-- Dependencies: 209
-- Name: financial_entities_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.financial_entities_id_seq', 18, true);


--
-- TOC entry 3286 (class 0 OID 0)
-- Dependencies: 210
-- Name: media_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.media_id_seq', 57, true);


--
-- TOC entry 3287 (class 0 OID 0)
-- Dependencies: 211
-- Name: migrations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.migrations_id_seq', 53, true);


--
-- TOC entry 3288 (class 0 OID 0)
-- Dependencies: 212
-- Name: permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.permissions_id_seq', 116, true);


--
-- TOC entry 3289 (class 0 OID 0)
-- Dependencies: 213
-- Name: questionnaire_template_questions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.questionnaire_template_questions_id_seq', 2, true);


--
-- TOC entry 3290 (class 0 OID 0)
-- Dependencies: 214
-- Name: questionnaire_templates_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.questionnaire_templates_id_seq', 2, true);


--
-- TOC entry 3291 (class 0 OID 0)
-- Dependencies: 215
-- Name: questionnaire_types_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.questionnaire_types_id_seq', 2, true);


--
-- TOC entry 3292 (class 0 OID 0)
-- Dependencies: 216
-- Name: roles_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.roles_id_seq', 2, true);


--
-- TOC entry 3293 (class 0 OID 0)
-- Dependencies: 217
-- Name: states_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.states_id_seq', 2, true);


--
-- TOC entry 3294 (class 0 OID 0)
-- Dependencies: 218
-- Name: statuses_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.statuses_id_seq', 11, true);


--
-- TOC entry 3295 (class 0 OID 0)
-- Dependencies: 219
-- Name: translations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.translations_id_seq', 284, true);


--
-- TOC entry 3296 (class 0 OID 0)
-- Dependencies: 220
-- Name: users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.users_id_seq', 2, false);


--
-- TOC entry 3297 (class 0 OID 0)
-- Dependencies: 221
-- Name: wysiwyg_media_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.wysiwyg_media_id_seq', 2, false);


--
-- TOC entry 2959 (class 2606 OID 123147)
-- Name: applicant_relationships _copy_3; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.applicant_relationships
    ADD CONSTRAINT _copy_3 PRIMARY KEY (id);


--
-- TOC entry 2963 (class 2606 OID 123151)
-- Name: applicants _copy_4; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.applicants
    ADD CONSTRAINT _copy_4 PRIMARY KEY (id);


--
-- TOC entry 2965 (class 2606 OID 123153)
-- Name: cities _copy_5; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cities
    ADD CONSTRAINT _copy_5 PRIMARY KEY (id);


--
-- TOC entry 2977 (class 2606 OID 123165)
-- Name: education_levels _copy_6; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.education_levels
    ADD CONSTRAINT _copy_6 PRIMARY KEY (id);


--
-- TOC entry 2946 (class 2606 OID 123208)
-- Name: admin_users admin_users_email_deleted_at_unique; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.admin_users
    ADD CONSTRAINT admin_users_email_deleted_at_unique UNIQUE (email, deleted_at);


--
-- TOC entry 2949 (class 2606 OID 123136)
-- Name: admin_users admin_users_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.admin_users
    ADD CONSTRAINT admin_users_pkey PRIMARY KEY (id);


--
-- TOC entry 2951 (class 2606 OID 123139)
-- Name: applicant_contact_methods applicant_contact_methods_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.applicant_contact_methods
    ADD CONSTRAINT applicant_contact_methods_pkey PRIMARY KEY (applicant_id, contact_method_id);


--
-- TOC entry 2953 (class 2606 OID 123141)
-- Name: applicant_disabilities applicant_disabilities_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.applicant_disabilities
    ADD CONSTRAINT applicant_disabilities_pkey PRIMARY KEY (applicant_id, disability_id);


--
-- TOC entry 2955 (class 2606 OID 123143)
-- Name: applicant_diseases applicant_diseases_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.applicant_diseases
    ADD CONSTRAINT applicant_diseases_pkey PRIMARY KEY (applicant_id, disease_id);


--
-- TOC entry 2957 (class 2606 OID 123145)
-- Name: applicant_documents applicant_documents_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.applicant_documents
    ADD CONSTRAINT applicant_documents_pkey PRIMARY KEY (id);


--
-- TOC entry 2961 (class 2606 OID 123149)
-- Name: applicant_statuses applicant_statuses_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.applicant_statuses
    ADD CONSTRAINT applicant_statuses_pkey PRIMARY KEY (id);


--
-- TOC entry 3022 (class 2606 OID 147481)
-- Name: assignment_histories assignment_history_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.assignment_histories
    ADD CONSTRAINT assignment_history_pkey PRIMARY KEY (id);


--
-- TOC entry 2967 (class 2606 OID 123155)
-- Name: construction_entities construction_entities_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.construction_entities
    ADD CONSTRAINT construction_entities_pkey PRIMARY KEY (id);


--
-- TOC entry 2969 (class 2606 OID 123157)
-- Name: contact_methods contact_methods_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.contact_methods
    ADD CONSTRAINT contact_methods_pkey PRIMARY KEY (id);


--
-- TOC entry 2971 (class 2606 OID 123159)
-- Name: disabilities disabilities_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.disabilities
    ADD CONSTRAINT disabilities_pkey PRIMARY KEY (id);


--
-- TOC entry 2973 (class 2606 OID 123161)
-- Name: diseases diseases_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.diseases
    ADD CONSTRAINT diseases_pkey PRIMARY KEY (id);


--
-- TOC entry 2975 (class 2606 OID 123163)
-- Name: document_types document_types_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.document_types
    ADD CONSTRAINT document_types_pkey PRIMARY KEY (id);


--
-- TOC entry 2979 (class 2606 OID 123167)
-- Name: failed_jobs failed_jobs_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.failed_jobs
    ADD CONSTRAINT failed_jobs_pkey PRIMARY KEY (id);


--
-- TOC entry 2981 (class 2606 OID 123169)
-- Name: financial_entities financial_entities_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.financial_entities
    ADD CONSTRAINT financial_entities_pkey PRIMARY KEY (id);


--
-- TOC entry 2984 (class 2606 OID 123171)
-- Name: media media_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.media
    ADD CONSTRAINT media_pkey PRIMARY KEY (id);


--
-- TOC entry 2986 (class 2606 OID 123174)
-- Name: migrations migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.migrations
    ADD CONSTRAINT migrations_pkey PRIMARY KEY (id);


--
-- TOC entry 2989 (class 2606 OID 123176)
-- Name: model_has_permissions model_has_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.model_has_permissions
    ADD CONSTRAINT model_has_permissions_pkey PRIMARY KEY (permission_id, model_id, model_type);


--
-- TOC entry 2992 (class 2606 OID 123179)
-- Name: model_has_roles model_has_roles_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.model_has_roles
    ADD CONSTRAINT model_has_roles_pkey PRIMARY KEY (role_id, model_id, model_type);


--
-- TOC entry 2995 (class 2606 OID 123183)
-- Name: permissions permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.permissions
    ADD CONSTRAINT permissions_pkey PRIMARY KEY (id);


--
-- TOC entry 2997 (class 2606 OID 123185)
-- Name: questionnaire_template_questions questionnaire_template_questions_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.questionnaire_template_questions
    ADD CONSTRAINT questionnaire_template_questions_pkey PRIMARY KEY (id);


--
-- TOC entry 2999 (class 2606 OID 123187)
-- Name: questionnaire_templates questionnaire_templates_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.questionnaire_templates
    ADD CONSTRAINT questionnaire_templates_pkey PRIMARY KEY (id);


--
-- TOC entry 3001 (class 2606 OID 123189)
-- Name: questionnaire_types questionnaire_types_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.questionnaire_types
    ADD CONSTRAINT questionnaire_types_pkey PRIMARY KEY (id);


--
-- TOC entry 3003 (class 2606 OID 123191)
-- Name: role_has_permissions role_has_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.role_has_permissions
    ADD CONSTRAINT role_has_permissions_pkey PRIMARY KEY (permission_id, role_id);


--
-- TOC entry 3005 (class 2606 OID 123193)
-- Name: roles roles_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.roles
    ADD CONSTRAINT roles_pkey PRIMARY KEY (id);


--
-- TOC entry 3007 (class 2606 OID 123195)
-- Name: states states_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.states
    ADD CONSTRAINT states_pkey PRIMARY KEY (id);


--
-- TOC entry 3009 (class 2606 OID 123197)
-- Name: statuses statuses_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.statuses
    ADD CONSTRAINT statuses_pkey PRIMARY KEY (id);


--
-- TOC entry 3013 (class 2606 OID 123199)
-- Name: translations translations_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.translations
    ADD CONSTRAINT translations_pkey PRIMARY KEY (id);


--
-- TOC entry 3015 (class 2606 OID 123350)
-- Name: users users_email_unique; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_email_unique UNIQUE (email);


--
-- TOC entry 3017 (class 2606 OID 123203)
-- Name: users users_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- TOC entry 3019 (class 2606 OID 123205)
-- Name: wysiwyg_media wysiwyg_media_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wysiwyg_media
    ADD CONSTRAINT wysiwyg_media_pkey PRIMARY KEY (id);


--
-- TOC entry 2942 (class 1259 OID 123132)
-- Name: activations_email_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX activations_email_index ON public.activations USING btree (email);


--
-- TOC entry 2943 (class 1259 OID 123133)
-- Name: admin_activations_email_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX admin_activations_email_index ON public.admin_activations USING btree (email);


--
-- TOC entry 2944 (class 1259 OID 123134)
-- Name: admin_password_resets_email_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX admin_password_resets_email_index ON public.admin_password_resets USING btree (email);


--
-- TOC entry 2947 (class 1259 OID 123137)
-- Name: admin_users_email_null_deleted_at; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX admin_users_email_null_deleted_at ON public.admin_users USING btree (email) WHERE (deleted_at IS NULL);


--
-- TOC entry 2982 (class 1259 OID 123172)
-- Name: media_model_type_model_id_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX media_model_type_model_id_index ON public.media USING btree (model_type, model_id);


--
-- TOC entry 2987 (class 1259 OID 123177)
-- Name: model_has_permissions_model_id_model_type_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX model_has_permissions_model_id_model_type_index ON public.model_has_permissions USING btree (model_id, model_type);


--
-- TOC entry 2990 (class 1259 OID 123180)
-- Name: model_has_roles_model_id_model_type_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX model_has_roles_model_id_model_type_index ON public.model_has_roles USING btree (model_id, model_type);


--
-- TOC entry 2993 (class 1259 OID 123181)
-- Name: password_resets_email_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX password_resets_email_index ON public.password_resets USING btree (email);


--
-- TOC entry 3010 (class 1259 OID 123200)
-- Name: translations_group_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX translations_group_index ON public.translations USING btree ("group");


--
-- TOC entry 3011 (class 1259 OID 123201)
-- Name: translations_namespace_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX translations_namespace_index ON public.translations USING btree (namespace);


--
-- TOC entry 3020 (class 1259 OID 123206)
-- Name: wysiwyg_media_wysiwygable_id_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wysiwyg_media_wysiwygable_id_index ON public.wysiwyg_media USING btree (wysiwygable_id);


--
-- TOC entry 3023 (class 2606 OID 123209)
-- Name: admin_users fk_admin_users_construction_entity; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.admin_users
    ADD CONSTRAINT fk_admin_users_construction_entity FOREIGN KEY (construction_entity) REFERENCES public.construction_entities(id);


--
-- TOC entry 3024 (class 2606 OID 123214)
-- Name: admin_users fk_admin_users_financial_entity; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.admin_users
    ADD CONSTRAINT fk_admin_users_financial_entity FOREIGN KEY (financial_entity) REFERENCES public.financial_entities(id);


--
-- TOC entry 3037 (class 2606 OID 123279)
-- Name: applicants fk_admin_users_financial_entity; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.applicants
    ADD CONSTRAINT fk_admin_users_financial_entity FOREIGN KEY (financial_entity) REFERENCES public.financial_entities(id);


--
-- TOC entry 3025 (class 2606 OID 123219)
-- Name: applicant_answers fk_applicant_answers_applicants; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.applicant_answers
    ADD CONSTRAINT fk_applicant_answers_applicants FOREIGN KEY (applicant_id) REFERENCES public.applicants(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3026 (class 2606 OID 123224)
-- Name: applicant_answers fk_applicant_answers_questionnaire_templates; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.applicant_answers
    ADD CONSTRAINT fk_applicant_answers_questionnaire_templates FOREIGN KEY (questionnaire_template_id) REFERENCES public.questionnaire_templates(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 3027 (class 2606 OID 123229)
-- Name: applicant_contact_methods fk_applicant_contact_methods_applicants_1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.applicant_contact_methods
    ADD CONSTRAINT fk_applicant_contact_methods_applicants_1 FOREIGN KEY (applicant_id) REFERENCES public.applicants(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3028 (class 2606 OID 123234)
-- Name: applicant_contact_methods fk_applicant_contact_methods_contact_methods_1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.applicant_contact_methods
    ADD CONSTRAINT fk_applicant_contact_methods_contact_methods_1 FOREIGN KEY (contact_method_id) REFERENCES public.contact_methods(id);


--
-- TOC entry 3029 (class 2606 OID 123239)
-- Name: applicant_disabilities fk_applicant_disabilities_applicants_1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.applicant_disabilities
    ADD CONSTRAINT fk_applicant_disabilities_applicants_1 FOREIGN KEY (applicant_id) REFERENCES public.applicants(id) ON DELETE CASCADE;


--
-- TOC entry 3030 (class 2606 OID 123244)
-- Name: applicant_disabilities fk_applicant_disabilities_disabilities_1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.applicant_disabilities
    ADD CONSTRAINT fk_applicant_disabilities_disabilities_1 FOREIGN KEY (disability_id) REFERENCES public.disabilities(id);


--
-- TOC entry 3031 (class 2606 OID 123249)
-- Name: applicant_diseases fk_applicant_diseases_applicants_1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.applicant_diseases
    ADD CONSTRAINT fk_applicant_diseases_applicants_1 FOREIGN KEY (applicant_id) REFERENCES public.applicants(id) ON DELETE CASCADE;


--
-- TOC entry 3032 (class 2606 OID 123254)
-- Name: applicant_diseases fk_applicant_diseases_diseases_1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.applicant_diseases
    ADD CONSTRAINT fk_applicant_diseases_diseases_1 FOREIGN KEY (disease_id) REFERENCES public.diseases(id);


--
-- TOC entry 3033 (class 2606 OID 123259)
-- Name: applicant_documents fk_applicant_documents_applicants_1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.applicant_documents
    ADD CONSTRAINT fk_applicant_documents_applicants_1 FOREIGN KEY (applicant_id) REFERENCES public.applicants(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3034 (class 2606 OID 123264)
-- Name: applicant_documents fk_applicant_documents_document_types_1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.applicant_documents
    ADD CONSTRAINT fk_applicant_documents_document_types_1 FOREIGN KEY (document_id) REFERENCES public.document_types(id);


--
-- TOC entry 3035 (class 2606 OID 123269)
-- Name: applicant_statuses fk_applicant_statuses_applicants_1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.applicant_statuses
    ADD CONSTRAINT fk_applicant_statuses_applicants_1 FOREIGN KEY (applicant_id) REFERENCES public.applicants(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3036 (class 2606 OID 123274)
-- Name: applicant_statuses fk_applicant_statuses_statuses_1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.applicant_statuses
    ADD CONSTRAINT fk_applicant_statuses_statuses_1 FOREIGN KEY (status_id) REFERENCES public.statuses(id);


--
-- TOC entry 3038 (class 2606 OID 123284)
-- Name: applicants fk_applicants_applicant_relationships_1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.applicants
    ADD CONSTRAINT fk_applicants_applicant_relationships_1 FOREIGN KEY (applicant_relationship) REFERENCES public.applicant_relationships(id);


--
-- TOC entry 3039 (class 2606 OID 123289)
-- Name: applicants fk_applicants_applicants_1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.applicants
    ADD CONSTRAINT fk_applicants_applicants_1 FOREIGN KEY (parent_applicant) REFERENCES public.applicants(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3040 (class 2606 OID 123294)
-- Name: applicants fk_applicants_cities_1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.applicants
    ADD CONSTRAINT fk_applicants_cities_1 FOREIGN KEY (city_id) REFERENCES public.cities(id);


--
-- TOC entry 3041 (class 2606 OID 123299)
-- Name: applicants fk_applicants_construction_entity; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.applicants
    ADD CONSTRAINT fk_applicants_construction_entity FOREIGN KEY (construction_entity) REFERENCES public.construction_entities(id);


--
-- TOC entry 3042 (class 2606 OID 123304)
-- Name: applicants fk_applicants_education_levels_1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.applicants
    ADD CONSTRAINT fk_applicants_education_levels_1 FOREIGN KEY (education_level) REFERENCES public.education_levels(id);


--
-- TOC entry 3043 (class 2606 OID 123309)
-- Name: applicants fk_applicants_states_1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.applicants
    ADD CONSTRAINT fk_applicants_states_1 FOREIGN KEY (state_id) REFERENCES public.states(id);


--
-- TOC entry 3052 (class 2606 OID 147466)
-- Name: assignment_histories fk_assignment_history_applicants; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.assignment_histories
    ADD CONSTRAINT fk_assignment_history_applicants FOREIGN KEY (applicant_id) REFERENCES public.applicants(id);


--
-- TOC entry 3051 (class 2606 OID 147461)
-- Name: assignment_histories fk_assignment_history_construction_entity; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.assignment_histories
    ADD CONSTRAINT fk_assignment_history_construction_entity FOREIGN KEY (entity_id) REFERENCES public.construction_entities(id);


--
-- TOC entry 3053 (class 2606 OID 155658)
-- Name: assignment_histories fk_assignment_history_users; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.assignment_histories
    ADD CONSTRAINT fk_assignment_history_users FOREIGN KEY (user_id) REFERENCES public.admin_users(id);


--
-- TOC entry 3044 (class 2606 OID 123314)
-- Name: cities fk_cities_states_1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cities
    ADD CONSTRAINT fk_cities_states_1 FOREIGN KEY (state_id) REFERENCES public.states(id);


--
-- TOC entry 3047 (class 2606 OID 123329)
-- Name: questionnaire_template_questions fk_questionnaire_template_questions_questionnaire_templates_1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.questionnaire_template_questions
    ADD CONSTRAINT fk_questionnaire_template_questions_questionnaire_templates_1 FOREIGN KEY (questionnaire_template_id) REFERENCES public.questionnaire_templates(id);


--
-- TOC entry 3048 (class 2606 OID 123334)
-- Name: questionnaire_templates fk_questionnaire_templates_questionnaire_types_1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.questionnaire_templates
    ADD CONSTRAINT fk_questionnaire_templates_questionnaire_types_1 FOREIGN KEY (questionnaire_type_id) REFERENCES public.questionnaire_types(id);


--
-- TOC entry 3045 (class 2606 OID 123319)
-- Name: model_has_permissions model_has_permissions_permission_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.model_has_permissions
    ADD CONSTRAINT model_has_permissions_permission_id_foreign FOREIGN KEY (permission_id) REFERENCES public.permissions(id) ON DELETE CASCADE;


--
-- TOC entry 3046 (class 2606 OID 123324)
-- Name: model_has_roles model_has_roles_role_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.model_has_roles
    ADD CONSTRAINT model_has_roles_role_id_foreign FOREIGN KEY (role_id) REFERENCES public.roles(id) ON DELETE CASCADE;


--
-- TOC entry 3049 (class 2606 OID 123339)
-- Name: role_has_permissions role_has_permissions_permission_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.role_has_permissions
    ADD CONSTRAINT role_has_permissions_permission_id_foreign FOREIGN KEY (permission_id) REFERENCES public.permissions(id) ON DELETE CASCADE;


--
-- TOC entry 3050 (class 2606 OID 123344)
-- Name: role_has_permissions role_has_permissions_role_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.role_has_permissions
    ADD CONSTRAINT role_has_permissions_role_id_foreign FOREIGN KEY (role_id) REFERENCES public.roles(id) ON DELETE CASCADE;


-- Completed on 2020-09-24 09:19:37

--
-- PostgreSQL database dump complete
--

